<?php

return [
    'url' => env('VIX_VERIFY_URL'),
    'account_id' => env('VIX_VERIFY_ACCOUNTID'),
    'password' => env('VIX_VERIFY_PASSWORD'),
    'apikey' => env('VIX_VERIFY_APIKEY')
];