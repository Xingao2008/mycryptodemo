<?php
return [
    'available_currency' => [
        'btc',
        'eth',
        'ltc',
        'xrp',
        'pwr'
    ],
    'holding' => [
        'btc' => getenv('BTC_HOLDINGWALLET'),
        'eth' => env('ETH_HOLDINGWALLET'),
        'ltc' => getenv('LTC_HOLDINGWALLET'),
        'xrp' => env('XRP_HOLDINGWALLET'),
        'ethkey' => env('ETH_HOLDING_TRANSFERKEY'),
    ],
    'transfer_key' => [
        'btc' => env('BTC_TRANSFERKEY'),
        'eth' => env('ETH_TRANSFERKEY'),
        'ltc' => env('LTC_TRANSFERKEY'),
        'xrp' => env('XRP_TRANSFERKEY'),
    ],
    // 'btcmarket' => [
    //     'btc' => getenv('BTC_BTCMARKETS_WALLET'),
    //     'eth' => getenv('ETH_BTCMARKETS_WALLET'),
    //     'ltc' => getenv('LTC_BTCMARKETS_WALLET'),
    //     'xrp' => getenv('XRP_BTCMARKETS_WALLET'),
    //     'secret_key' => env('SK_BTCMARKETS'),
    //     'private_key' => env('PK_BTCMARKETS'),
    // ],
    'bittrex' => [
        'btc' => getenv('BTC_BITTREX_WALLET'),
        'eth' => getenv('ETH_BITTREX_WALLET'),
        'ltc' => getenv('LTC_BITTREX_WALLET'),
        'xrp' => getenv('XRP_BITTREX_WALLET'),
        'xrp_dt' => getenv('XRP_BITTREX_DT'),
        'secret_key' => env('SK_BITTREX'),
        'private_key' => env('PK_BITTREX'),
    ],
//    'miner_sell_fee' => [
//        'btc'=> 0.005,
//        'ltc'=> 0.01,
//        'eth'=> 0.001,
//        'xrp'=> 0.15,
//        'pwr'=> 1.0
//    ],
//    'miner_buy_fee' => [
//        'btc'=> 0.001,
//        'ltc'=> 0.01,
//        'eth'=> 0.006,
//        'xrp'=> 1.0,
//        'pwr'=> 1.0
//    ]

];
