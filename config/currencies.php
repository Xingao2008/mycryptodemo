<?php

return [
 /*
  |--------------------------------------------------------------------------
  | Currency settings
  |--------------------------------------------------------------------------
  |
  | Return currently available currencies on the platform
  | Please update country record and set available = 1
  |
  */
  36 => 'AUD',
  554 => 'NZD',
  840 => 'USD',
  'allAvailableCurrencies' => 
    [
      "btc", 
      "ltc", 
      "eth", 
      "xrp", 
      "aud", 
      "pwr",
      "usd",
      'nzd'
    ],
    'fiatCurrencies' => [
      'aud',
      'nzd',
      'usd'
    ],
    'crypto' => [
      "btc", 
      "ltc", 
      "eth", 
      "xrp", 
      "pwr"
    ]
];
