<?php
return [
    /**
     * We charge user in different nuance
     */
    'exchange' => [
        [
            'min' => 0,
            'max' => 5000,
            'exchange_tier' => 1,
            'fee' => 1
        ],
        [
            'min' => 5000,
            'max' => 25000,
            'exchange_tier' => 1,
            'fee' => 0.75
        ],
        [
            'min' => 25000,
            'max' => 9223372036854775807, //max size of integer
            'exchange_tier' => 1,
            'fee' => 0.5
        ],
        [
            'min' => 0,
            'max' => 5000,
            'exchange_tier' => 99,
            'fee' => 0
        ],
        [
            'min' => 5000,
            'max' => 25000,
            'exchange_tier' => 99,
            'fee' => 0
        ],
        [
            'min' => 25000,
            'max' => 9223372036854775807, //max size of integer
            'exchange_tier' => 99,
            'fee' => 0
        ],
    ],
    'fiat_convert' => [
        'min_convert_fee' => 50
    ],

];
