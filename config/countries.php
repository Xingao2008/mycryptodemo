<?php

return [
 /*
  |--------------------------------------------------------------------------
  | Database settings
  |--------------------------------------------------------------------------
  |
  | The name of the table to create in the database
  |
  */
  'table_name' => 'countries',
  'support_countries' => [
    36, //'Australia',
    554,// => 'New Zealand',
    840,// => 'United States of America',
    826,// => 'United Kingdom',
    124,// => 'Canada',
    392,// => 'Japan'
  ],
  'phone_prefix' => [
    61 => 36, //Australia
    64 => 554, //New Zealand
    1 => 840,  //USA & Canada
    44 => 826, //UK
    81 => 392,  //Japan
  ]
];
