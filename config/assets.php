<?php

/*
|---------------------------------------------------------------------------
| Here are SOME of the available configuration options with suitable values.
| Uncomment and customize those you want to override or remove them to
| use their default values. For a FULL list of options please visit
| //github.com/Stolz/Assets/blob/master/API.md#assets
|---------------------------------------------------------------------------
*/

return [

    // Configuration for the default group. Feel free to add more groups.
    // Each group can have different settings.
    'default' => [
        //'asset_regex' => '/.\.(css|js)$/i',
        //'css_regex' => '/.\.css$/i',
        //'js_regex' => '/.\.js$/i',
        'no_minification_regex' => '/.[-.]min\.(css|js)$/i',
        //'public_dir' => (function_exists('public_path')) ? public_path() : '/var/www/localhost/htdocs',
        'css_dir' => 'css',
        'js_dir' => 'js',
        //'packages_dir' => 'packages',
        'pipeline' => false,
        //'pipeline_dir' => 'min',
        'pipeline_gzip' => true,
        //'fetch_command' => function ($asset) {return preprocess(file_get_contents($asset));},
        'collections' => [
            'default' => [
                'font-awesome.min.css',
                "dataTables.bootstrap.min.css",
                //"jquery.dataTables.min.js",
                //"dataTables.bootstrap.min.js",
                'sweetalert.min.css',
                'sweetalert.min.js',
                'vixverify-greenid.css',

                'vendor.js',
                'vendor.css',
                'app.js'
            ],
            'charts' => [
                'charts/app.js',
                'charts/canvasjs.min.js',
            ],
            'quill' => [
                'quill/quill.js',
                'quill/app.js',
                'quill.snow.css',
            ]
        ],
        'autoload' => ['default'],

    ], // End of default group

    'admin' => [
        //'asset_regex' => '/.\.(css|js)$/i',
        //'css_regex' => '/.\.css$/i',
        //'js_regex' => '/.\.js$/i',
        //'no_minification_regex' => '/.[-.]min\.(css|js)$/i',
        //'public_dir' => (function_exists('public_path')) ? public_path() : '/var/www/localhost/htdocs',
        'css_dir' => 'css',
        'js_dir' => 'js',
        'packages_dir' => 'packages',
        'pipeline' => false,
        'pipeline_dir' => '',
        'pipeline_gzip' => true,
        'collections' => [
            'default' => [
                'font-awesome.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
                'admin/app.js',
                'admin/app.css',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                'admin.js',
                'clipboard.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js',
            ],
            'icheck' => [
                "icheck.min.js",
                "icheck/all.css",
            ],
            'toggle' => [
                "bootstrap-toggle.min.css",
                "bootstrap-toggle.min.js",
            ],
            'datatables' => [
                "dataTables.bootstrap.min.css",
                "jquery.dataTables.min.js",
                "dataTables.bootstrap.min.js",
            ],
            'fileupload' => [
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.6/css/fileinput.css",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.6/themes/explorer/theme.css",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.6/js/fileinput.js",
                "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.6/themes/explorer/theme.js",
            ]
        ],
        'autoload' => ['default'],

    ], // End of admin group
];

