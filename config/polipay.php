<?php

return [
    'merchant_code' => env('POLI_MERCHANT_CODE'),
    'auth_code' => env('POLI_AUTH_CODE')
];