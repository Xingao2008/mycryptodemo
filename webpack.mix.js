const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.browserSync('http://localhost');

mix.js('resources/assets/js/app.js', 'public/js');
mix.sass('resources/assets/sass/app.scss', 'public/css').version();


mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/jquery-ui-dist/jquery-ui.js',
    'node_modules/vue/dist/vue.js',
], 'public/js/vendor.js');

mix.styles([
    'node_modules/jquery-ui-dist/jquery-ui.css',
], 'public/css/vendor.css');

// Admin
mix.styles([
    'node_modules/select2/dist/css/select2.css',
    'node_modules/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css',
    'node_modules/adminlte/dist/css/AdminLTE.css',
    'node_modules/adminlte/dist/css/skins/skin-blue.css',
], 'public/css/admin/app.css');

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.js',
    'node_modules/select2/dist/js/select2.js',
    'node_modules/vue/dist/vue.js',
    'node_modules/axios/dist/axios.js',
], 'public/js/admin/app.js');

//mix.copyDirectory('node_modules/bootstrap/fonts', 'public/css/fonts');

//mix.sass('node_modules/ionicons/dist/scss/ionicons.scss', 'public/css/ionicons.css');
//mix.copyDirectory('node_modules/ionicons/dist/fonts', 'public/css/fonts');


// EXCHANGE
mix.js('resources/assets/js/Exchange/main.js', 'public/js/exchange.js').version();
mix.sass('resources/assets/sass/exchange/app.scss', 'public/css/exchange.css').version();

// new Register Flow
mix.js('resources/assets/js/Register/main.js', 'public/js/register_flow.js').version();
mix.sass('resources/assets/sass/register/app.scss', 'public/css/register_flow.css').version();

// new Dashboard widgets
mix.js('resources/assets/js/dashboard/main.js', 'public/js/dashboard-widgets.js').version();