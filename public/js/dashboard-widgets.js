/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/axios/index.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/axios/lib/axios.js");

/***/ }),

/***/ "./node_modules/axios/lib/adapters/xhr.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");
var settle = __webpack_require__("./node_modules/axios/lib/core/settle.js");
var buildURL = __webpack_require__("./node_modules/axios/lib/helpers/buildURL.js");
var parseHeaders = __webpack_require__("./node_modules/axios/lib/helpers/parseHeaders.js");
var isURLSameOrigin = __webpack_require__("./node_modules/axios/lib/helpers/isURLSameOrigin.js");
var createError = __webpack_require__("./node_modules/axios/lib/core/createError.js");
var btoa = (typeof window !== 'undefined' && window.btoa && window.btoa.bind(window)) || __webpack_require__("./node_modules/axios/lib/helpers/btoa.js");

module.exports = function xhrAdapter(config) {
  return new Promise(function dispatchXhrRequest(resolve, reject) {
    var requestData = config.data;
    var requestHeaders = config.headers;

    if (utils.isFormData(requestData)) {
      delete requestHeaders['Content-Type']; // Let the browser set it
    }

    var request = new XMLHttpRequest();
    var loadEvent = 'onreadystatechange';
    var xDomain = false;

    // For IE 8/9 CORS support
    // Only supports POST and GET calls and doesn't returns the response headers.
    // DON'T do this for testing b/c XMLHttpRequest is mocked, not XDomainRequest.
    if ("development" !== 'test' &&
        typeof window !== 'undefined' &&
        window.XDomainRequest && !('withCredentials' in request) &&
        !isURLSameOrigin(config.url)) {
      request = new window.XDomainRequest();
      loadEvent = 'onload';
      xDomain = true;
      request.onprogress = function handleProgress() {};
      request.ontimeout = function handleTimeout() {};
    }

    // HTTP basic authentication
    if (config.auth) {
      var username = config.auth.username || '';
      var password = config.auth.password || '';
      requestHeaders.Authorization = 'Basic ' + btoa(username + ':' + password);
    }

    request.open(config.method.toUpperCase(), buildURL(config.url, config.params, config.paramsSerializer), true);

    // Set the request timeout in MS
    request.timeout = config.timeout;

    // Listen for ready state
    request[loadEvent] = function handleLoad() {
      if (!request || (request.readyState !== 4 && !xDomain)) {
        return;
      }

      // The request errored out and we didn't get a response, this will be
      // handled by onerror instead
      // With one exception: request that using file: protocol, most browsers
      // will return status as 0 even though it's a successful request
      if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf('file:') === 0)) {
        return;
      }

      // Prepare the response
      var responseHeaders = 'getAllResponseHeaders' in request ? parseHeaders(request.getAllResponseHeaders()) : null;
      var responseData = !config.responseType || config.responseType === 'text' ? request.responseText : request.response;
      var response = {
        data: responseData,
        // IE sends 1223 instead of 204 (https://github.com/axios/axios/issues/201)
        status: request.status === 1223 ? 204 : request.status,
        statusText: request.status === 1223 ? 'No Content' : request.statusText,
        headers: responseHeaders,
        config: config,
        request: request
      };

      settle(resolve, reject, response);

      // Clean up request
      request = null;
    };

    // Handle low level network errors
    request.onerror = function handleError() {
      // Real errors are hidden from us by the browser
      // onerror should only fire if it's a network error
      reject(createError('Network Error', config, null, request));

      // Clean up request
      request = null;
    };

    // Handle timeout
    request.ontimeout = function handleTimeout() {
      reject(createError('timeout of ' + config.timeout + 'ms exceeded', config, 'ECONNABORTED',
        request));

      // Clean up request
      request = null;
    };

    // Add xsrf header
    // This is only done if running in a standard browser environment.
    // Specifically not if we're in a web worker, or react-native.
    if (utils.isStandardBrowserEnv()) {
      var cookies = __webpack_require__("./node_modules/axios/lib/helpers/cookies.js");

      // Add xsrf header
      var xsrfValue = (config.withCredentials || isURLSameOrigin(config.url)) && config.xsrfCookieName ?
          cookies.read(config.xsrfCookieName) :
          undefined;

      if (xsrfValue) {
        requestHeaders[config.xsrfHeaderName] = xsrfValue;
      }
    }

    // Add headers to the request
    if ('setRequestHeader' in request) {
      utils.forEach(requestHeaders, function setRequestHeader(val, key) {
        if (typeof requestData === 'undefined' && key.toLowerCase() === 'content-type') {
          // Remove Content-Type if data is undefined
          delete requestHeaders[key];
        } else {
          // Otherwise add header to the request
          request.setRequestHeader(key, val);
        }
      });
    }

    // Add withCredentials to request if needed
    if (config.withCredentials) {
      request.withCredentials = true;
    }

    // Add responseType to request if needed
    if (config.responseType) {
      try {
        request.responseType = config.responseType;
      } catch (e) {
        // Expected DOMException thrown by browsers not compatible XMLHttpRequest Level 2.
        // But, this can be suppressed for 'json' type as it can be parsed by default 'transformResponse' function.
        if (config.responseType !== 'json') {
          throw e;
        }
      }
    }

    // Handle progress if needed
    if (typeof config.onDownloadProgress === 'function') {
      request.addEventListener('progress', config.onDownloadProgress);
    }

    // Not all browsers support upload events
    if (typeof config.onUploadProgress === 'function' && request.upload) {
      request.upload.addEventListener('progress', config.onUploadProgress);
    }

    if (config.cancelToken) {
      // Handle cancellation
      config.cancelToken.promise.then(function onCanceled(cancel) {
        if (!request) {
          return;
        }

        request.abort();
        reject(cancel);
        // Clean up request
        request = null;
      });
    }

    if (requestData === undefined) {
      requestData = null;
    }

    // Send the request
    request.send(requestData);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");
var bind = __webpack_require__("./node_modules/axios/lib/helpers/bind.js");
var Axios = __webpack_require__("./node_modules/axios/lib/core/Axios.js");
var defaults = __webpack_require__("./node_modules/axios/lib/defaults.js");

/**
 * Create an instance of Axios
 *
 * @param {Object} defaultConfig The default config for the instance
 * @return {Axios} A new instance of Axios
 */
function createInstance(defaultConfig) {
  var context = new Axios(defaultConfig);
  var instance = bind(Axios.prototype.request, context);

  // Copy axios.prototype to instance
  utils.extend(instance, Axios.prototype, context);

  // Copy context to instance
  utils.extend(instance, context);

  return instance;
}

// Create the default instance to be exported
var axios = createInstance(defaults);

// Expose Axios class to allow class inheritance
axios.Axios = Axios;

// Factory for creating new instances
axios.create = function create(instanceConfig) {
  return createInstance(utils.merge(defaults, instanceConfig));
};

// Expose Cancel & CancelToken
axios.Cancel = __webpack_require__("./node_modules/axios/lib/cancel/Cancel.js");
axios.CancelToken = __webpack_require__("./node_modules/axios/lib/cancel/CancelToken.js");
axios.isCancel = __webpack_require__("./node_modules/axios/lib/cancel/isCancel.js");

// Expose all/spread
axios.all = function all(promises) {
  return Promise.all(promises);
};
axios.spread = __webpack_require__("./node_modules/axios/lib/helpers/spread.js");

module.exports = axios;

// Allow use of default import syntax in TypeScript
module.exports.default = axios;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/Cancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * A `Cancel` is an object that is thrown when an operation is canceled.
 *
 * @class
 * @param {string=} message The message.
 */
function Cancel(message) {
  this.message = message;
}

Cancel.prototype.toString = function toString() {
  return 'Cancel' + (this.message ? ': ' + this.message : '');
};

Cancel.prototype.__CANCEL__ = true;

module.exports = Cancel;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/CancelToken.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Cancel = __webpack_require__("./node_modules/axios/lib/cancel/Cancel.js");

/**
 * A `CancelToken` is an object that can be used to request cancellation of an operation.
 *
 * @class
 * @param {Function} executor The executor function.
 */
function CancelToken(executor) {
  if (typeof executor !== 'function') {
    throw new TypeError('executor must be a function.');
  }

  var resolvePromise;
  this.promise = new Promise(function promiseExecutor(resolve) {
    resolvePromise = resolve;
  });

  var token = this;
  executor(function cancel(message) {
    if (token.reason) {
      // Cancellation has already been requested
      return;
    }

    token.reason = new Cancel(message);
    resolvePromise(token.reason);
  });
}

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
CancelToken.prototype.throwIfRequested = function throwIfRequested() {
  if (this.reason) {
    throw this.reason;
  }
};

/**
 * Returns an object that contains a new `CancelToken` and a function that, when called,
 * cancels the `CancelToken`.
 */
CancelToken.source = function source() {
  var cancel;
  var token = new CancelToken(function executor(c) {
    cancel = c;
  });
  return {
    token: token,
    cancel: cancel
  };
};

module.exports = CancelToken;


/***/ }),

/***/ "./node_modules/axios/lib/cancel/isCancel.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function isCancel(value) {
  return !!(value && value.__CANCEL__);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/Axios.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defaults = __webpack_require__("./node_modules/axios/lib/defaults.js");
var utils = __webpack_require__("./node_modules/axios/lib/utils.js");
var InterceptorManager = __webpack_require__("./node_modules/axios/lib/core/InterceptorManager.js");
var dispatchRequest = __webpack_require__("./node_modules/axios/lib/core/dispatchRequest.js");

/**
 * Create a new instance of Axios
 *
 * @param {Object} instanceConfig The default config for the instance
 */
function Axios(instanceConfig) {
  this.defaults = instanceConfig;
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager()
  };
}

/**
 * Dispatch a request
 *
 * @param {Object} config The config specific for this request (merged with this.defaults)
 */
Axios.prototype.request = function request(config) {
  /*eslint no-param-reassign:0*/
  // Allow for axios('example/url'[, config]) a la fetch API
  if (typeof config === 'string') {
    config = utils.merge({
      url: arguments[0]
    }, arguments[1]);
  }

  config = utils.merge(defaults, {method: 'get'}, this.defaults, config);
  config.method = config.method.toLowerCase();

  // Hook up interceptors middleware
  var chain = [dispatchRequest, undefined];
  var promise = Promise.resolve(config);

  this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
    chain.unshift(interceptor.fulfilled, interceptor.rejected);
  });

  this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
    chain.push(interceptor.fulfilled, interceptor.rejected);
  });

  while (chain.length) {
    promise = promise.then(chain.shift(), chain.shift());
  }

  return promise;
};

// Provide aliases for supported request methods
utils.forEach(['delete', 'get', 'head', 'options'], function forEachMethodNoData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url
    }));
  };
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  /*eslint func-names:0*/
  Axios.prototype[method] = function(url, data, config) {
    return this.request(utils.merge(config || {}, {
      method: method,
      url: url,
      data: data
    }));
  };
});

module.exports = Axios;


/***/ }),

/***/ "./node_modules/axios/lib/core/InterceptorManager.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

function InterceptorManager() {
  this.handlers = [];
}

/**
 * Add a new interceptor to the stack
 *
 * @param {Function} fulfilled The function to handle `then` for a `Promise`
 * @param {Function} rejected The function to handle `reject` for a `Promise`
 *
 * @return {Number} An ID used to remove interceptor later
 */
InterceptorManager.prototype.use = function use(fulfilled, rejected) {
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected
  });
  return this.handlers.length - 1;
};

/**
 * Remove an interceptor from the stack
 *
 * @param {Number} id The ID that was returned by `use`
 */
InterceptorManager.prototype.eject = function eject(id) {
  if (this.handlers[id]) {
    this.handlers[id] = null;
  }
};

/**
 * Iterate over all the registered interceptors
 *
 * This method is particularly useful for skipping over any
 * interceptors that may have become `null` calling `eject`.
 *
 * @param {Function} fn The function to call for each interceptor
 */
InterceptorManager.prototype.forEach = function forEach(fn) {
  utils.forEach(this.handlers, function forEachHandler(h) {
    if (h !== null) {
      fn(h);
    }
  });
};

module.exports = InterceptorManager;


/***/ }),

/***/ "./node_modules/axios/lib/core/createError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var enhanceError = __webpack_require__("./node_modules/axios/lib/core/enhanceError.js");

/**
 * Create an Error with the specified message, config, error code, request and response.
 *
 * @param {string} message The error message.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The created error.
 */
module.exports = function createError(message, config, code, request, response) {
  var error = new Error(message);
  return enhanceError(error, config, code, request, response);
};


/***/ }),

/***/ "./node_modules/axios/lib/core/dispatchRequest.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");
var transformData = __webpack_require__("./node_modules/axios/lib/core/transformData.js");
var isCancel = __webpack_require__("./node_modules/axios/lib/cancel/isCancel.js");
var defaults = __webpack_require__("./node_modules/axios/lib/defaults.js");
var isAbsoluteURL = __webpack_require__("./node_modules/axios/lib/helpers/isAbsoluteURL.js");
var combineURLs = __webpack_require__("./node_modules/axios/lib/helpers/combineURLs.js");

/**
 * Throws a `Cancel` if cancellation has been requested.
 */
function throwIfCancellationRequested(config) {
  if (config.cancelToken) {
    config.cancelToken.throwIfRequested();
  }
}

/**
 * Dispatch a request to the server using the configured adapter.
 *
 * @param {object} config The config that is to be used for the request
 * @returns {Promise} The Promise to be fulfilled
 */
module.exports = function dispatchRequest(config) {
  throwIfCancellationRequested(config);

  // Support baseURL config
  if (config.baseURL && !isAbsoluteURL(config.url)) {
    config.url = combineURLs(config.baseURL, config.url);
  }

  // Ensure headers exist
  config.headers = config.headers || {};

  // Transform request data
  config.data = transformData(
    config.data,
    config.headers,
    config.transformRequest
  );

  // Flatten headers
  config.headers = utils.merge(
    config.headers.common || {},
    config.headers[config.method] || {},
    config.headers || {}
  );

  utils.forEach(
    ['delete', 'get', 'head', 'post', 'put', 'patch', 'common'],
    function cleanHeaderConfig(method) {
      delete config.headers[method];
    }
  );

  var adapter = config.adapter || defaults.adapter;

  return adapter(config).then(function onAdapterResolution(response) {
    throwIfCancellationRequested(config);

    // Transform response data
    response.data = transformData(
      response.data,
      response.headers,
      config.transformResponse
    );

    return response;
  }, function onAdapterRejection(reason) {
    if (!isCancel(reason)) {
      throwIfCancellationRequested(config);

      // Transform response data
      if (reason && reason.response) {
        reason.response.data = transformData(
          reason.response.data,
          reason.response.headers,
          config.transformResponse
        );
      }
    }

    return Promise.reject(reason);
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/core/enhanceError.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Update an Error with the specified config, error code, and response.
 *
 * @param {Error} error The error to update.
 * @param {Object} config The config.
 * @param {string} [code] The error code (for example, 'ECONNABORTED').
 * @param {Object} [request] The request.
 * @param {Object} [response] The response.
 * @returns {Error} The error.
 */
module.exports = function enhanceError(error, config, code, request, response) {
  error.config = config;
  if (code) {
    error.code = code;
  }
  error.request = request;
  error.response = response;
  return error;
};


/***/ }),

/***/ "./node_modules/axios/lib/core/settle.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var createError = __webpack_require__("./node_modules/axios/lib/core/createError.js");

/**
 * Resolve or reject a Promise based on response status.
 *
 * @param {Function} resolve A function that resolves the promise.
 * @param {Function} reject A function that rejects the promise.
 * @param {object} response The response.
 */
module.exports = function settle(resolve, reject, response) {
  var validateStatus = response.config.validateStatus;
  // Note: status is not exposed by XDomainRequest
  if (!response.status || !validateStatus || validateStatus(response.status)) {
    resolve(response);
  } else {
    reject(createError(
      'Request failed with status code ' + response.status,
      response.config,
      null,
      response.request,
      response
    ));
  }
};


/***/ }),

/***/ "./node_modules/axios/lib/core/transformData.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

/**
 * Transform the data for a request or a response
 *
 * @param {Object|String} data The data to be transformed
 * @param {Array} headers The headers for the request or response
 * @param {Array|Function} fns A single function or Array of functions
 * @returns {*} The resulting transformed data
 */
module.exports = function transformData(data, headers, fns) {
  /*eslint no-param-reassign:0*/
  utils.forEach(fns, function transform(fn) {
    data = fn(data, headers);
  });

  return data;
};


/***/ }),

/***/ "./node_modules/axios/lib/defaults.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var utils = __webpack_require__("./node_modules/axios/lib/utils.js");
var normalizeHeaderName = __webpack_require__("./node_modules/axios/lib/helpers/normalizeHeaderName.js");

var DEFAULT_CONTENT_TYPE = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

function setContentTypeIfUnset(headers, value) {
  if (!utils.isUndefined(headers) && utils.isUndefined(headers['Content-Type'])) {
    headers['Content-Type'] = value;
  }
}

function getDefaultAdapter() {
  var adapter;
  if (typeof XMLHttpRequest !== 'undefined') {
    // For browsers use XHR adapter
    adapter = __webpack_require__("./node_modules/axios/lib/adapters/xhr.js");
  } else if (typeof process !== 'undefined') {
    // For node use HTTP adapter
    adapter = __webpack_require__("./node_modules/axios/lib/adapters/xhr.js");
  }
  return adapter;
}

var defaults = {
  adapter: getDefaultAdapter(),

  transformRequest: [function transformRequest(data, headers) {
    normalizeHeaderName(headers, 'Content-Type');
    if (utils.isFormData(data) ||
      utils.isArrayBuffer(data) ||
      utils.isBuffer(data) ||
      utils.isStream(data) ||
      utils.isFile(data) ||
      utils.isBlob(data)
    ) {
      return data;
    }
    if (utils.isArrayBufferView(data)) {
      return data.buffer;
    }
    if (utils.isURLSearchParams(data)) {
      setContentTypeIfUnset(headers, 'application/x-www-form-urlencoded;charset=utf-8');
      return data.toString();
    }
    if (utils.isObject(data)) {
      setContentTypeIfUnset(headers, 'application/json;charset=utf-8');
      return JSON.stringify(data);
    }
    return data;
  }],

  transformResponse: [function transformResponse(data) {
    /*eslint no-param-reassign:0*/
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (e) { /* Ignore */ }
    }
    return data;
  }],

  /**
   * A timeout in milliseconds to abort a request. If set to 0 (default) a
   * timeout is not created.
   */
  timeout: 0,

  xsrfCookieName: 'XSRF-TOKEN',
  xsrfHeaderName: 'X-XSRF-TOKEN',

  maxContentLength: -1,

  validateStatus: function validateStatus(status) {
    return status >= 200 && status < 300;
  }
};

defaults.headers = {
  common: {
    'Accept': 'application/json, text/plain, */*'
  }
};

utils.forEach(['delete', 'get', 'head'], function forEachMethodNoData(method) {
  defaults.headers[method] = {};
});

utils.forEach(['post', 'put', 'patch'], function forEachMethodWithData(method) {
  defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
});

module.exports = defaults;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/axios/lib/helpers/bind.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function bind(fn, thisArg) {
  return function wrap() {
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }
    return fn.apply(thisArg, args);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/btoa.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// btoa polyfill for IE<10 courtesy https://github.com/davidchambers/Base64.js

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

function E() {
  this.message = 'String contains an invalid character';
}
E.prototype = new Error;
E.prototype.code = 5;
E.prototype.name = 'InvalidCharacterError';

function btoa(input) {
  var str = String(input);
  var output = '';
  for (
    // initialize result and counter
    var block, charCode, idx = 0, map = chars;
    // if the next str index does not exist:
    //   change the mapping table to "="
    //   check if d has no fractional digits
    str.charAt(idx | 0) || (map = '=', idx % 1);
    // "8 - idx % 1 * 8" generates the sequence 2, 4, 6, 8
    output += map.charAt(63 & block >> 8 - idx % 1 * 8)
  ) {
    charCode = str.charCodeAt(idx += 3 / 4);
    if (charCode > 0xFF) {
      throw new E();
    }
    block = block << 8 | charCode;
  }
  return output;
}

module.exports = btoa;


/***/ }),

/***/ "./node_modules/axios/lib/helpers/buildURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

function encode(val) {
  return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%20/g, '+').
    replace(/%5B/gi, '[').
    replace(/%5D/gi, ']');
}

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
module.exports = function buildURL(url, params, paramsSerializer) {
  /*eslint no-param-reassign:0*/
  if (!params) {
    return url;
  }

  var serializedParams;
  if (paramsSerializer) {
    serializedParams = paramsSerializer(params);
  } else if (utils.isURLSearchParams(params)) {
    serializedParams = params.toString();
  } else {
    var parts = [];

    utils.forEach(params, function serialize(val, key) {
      if (val === null || typeof val === 'undefined') {
        return;
      }

      if (utils.isArray(val)) {
        key = key + '[]';
      } else {
        val = [val];
      }

      utils.forEach(val, function parseValue(v) {
        if (utils.isDate(v)) {
          v = v.toISOString();
        } else if (utils.isObject(v)) {
          v = JSON.stringify(v);
        }
        parts.push(encode(key) + '=' + encode(v));
      });
    });

    serializedParams = parts.join('&');
  }

  if (serializedParams) {
    url += (url.indexOf('?') === -1 ? '?' : '&') + serializedParams;
  }

  return url;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/combineURLs.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Creates a new URL by combining the specified URLs
 *
 * @param {string} baseURL The base URL
 * @param {string} relativeURL The relative URL
 * @returns {string} The combined URL
 */
module.exports = function combineURLs(baseURL, relativeURL) {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/cookies.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs support document.cookie
  (function standardBrowserEnv() {
    return {
      write: function write(name, value, expires, path, domain, secure) {
        var cookie = [];
        cookie.push(name + '=' + encodeURIComponent(value));

        if (utils.isNumber(expires)) {
          cookie.push('expires=' + new Date(expires).toGMTString());
        }

        if (utils.isString(path)) {
          cookie.push('path=' + path);
        }

        if (utils.isString(domain)) {
          cookie.push('domain=' + domain);
        }

        if (secure === true) {
          cookie.push('secure');
        }

        document.cookie = cookie.join('; ');
      },

      read: function read(name) {
        var match = document.cookie.match(new RegExp('(^|;\\s*)(' + name + ')=([^;]*)'));
        return (match ? decodeURIComponent(match[3]) : null);
      },

      remove: function remove(name) {
        this.write(name, '', Date.now() - 86400000);
      }
    };
  })() :

  // Non standard browser env (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return {
      write: function write() {},
      read: function read() { return null; },
      remove: function remove() {}
    };
  })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isAbsoluteURL.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Determines whether the specified URL is absolute
 *
 * @param {string} url The URL to test
 * @returns {boolean} True if the specified URL is absolute, otherwise false
 */
module.exports = function isAbsoluteURL(url) {
  // A URL is considered absolute if it begins with "<scheme>://" or "//" (protocol-relative URL).
  // RFC 3986 defines scheme name as a sequence of characters beginning with a letter and followed
  // by any combination of letters, digits, plus, period, or hyphen.
  return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(url);
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/isURLSameOrigin.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

module.exports = (
  utils.isStandardBrowserEnv() ?

  // Standard browser envs have full support of the APIs needed to test
  // whether the request URL is of the same origin as current location.
  (function standardBrowserEnv() {
    var msie = /(msie|trident)/i.test(navigator.userAgent);
    var urlParsingNode = document.createElement('a');
    var originURL;

    /**
    * Parse a URL to discover it's components
    *
    * @param {String} url The URL to be parsed
    * @returns {Object}
    */
    function resolveURL(url) {
      var href = url;

      if (msie) {
        // IE needs attribute set twice to normalize properties
        urlParsingNode.setAttribute('href', href);
        href = urlParsingNode.href;
      }

      urlParsingNode.setAttribute('href', href);

      // urlParsingNode provides the UrlUtils interface - http://url.spec.whatwg.org/#urlutils
      return {
        href: urlParsingNode.href,
        protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, '') : '',
        host: urlParsingNode.host,
        search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, '') : '',
        hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, '') : '',
        hostname: urlParsingNode.hostname,
        port: urlParsingNode.port,
        pathname: (urlParsingNode.pathname.charAt(0) === '/') ?
                  urlParsingNode.pathname :
                  '/' + urlParsingNode.pathname
      };
    }

    originURL = resolveURL(window.location.href);

    /**
    * Determine if a URL shares the same origin as the current location
    *
    * @param {String} requestURL The URL to test
    * @returns {boolean} True if URL shares the same origin, otherwise false
    */
    return function isURLSameOrigin(requestURL) {
      var parsed = (utils.isString(requestURL)) ? resolveURL(requestURL) : requestURL;
      return (parsed.protocol === originURL.protocol &&
            parsed.host === originURL.host);
    };
  })() :

  // Non standard browser envs (web workers, react-native) lack needed support.
  (function nonStandardBrowserEnv() {
    return function isURLSameOrigin() {
      return true;
    };
  })()
);


/***/ }),

/***/ "./node_modules/axios/lib/helpers/normalizeHeaderName.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

module.exports = function normalizeHeaderName(headers, normalizedName) {
  utils.forEach(headers, function processHeader(value, name) {
    if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
      headers[normalizedName] = value;
      delete headers[name];
    }
  });
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/parseHeaders.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__("./node_modules/axios/lib/utils.js");

// Headers whose duplicates are ignored by node
// c.f. https://nodejs.org/api/http.html#http_message_headers
var ignoreDuplicateOf = [
  'age', 'authorization', 'content-length', 'content-type', 'etag',
  'expires', 'from', 'host', 'if-modified-since', 'if-unmodified-since',
  'last-modified', 'location', 'max-forwards', 'proxy-authorization',
  'referer', 'retry-after', 'user-agent'
];

/**
 * Parse headers into an object
 *
 * ```
 * Date: Wed, 27 Aug 2014 08:58:49 GMT
 * Content-Type: application/json
 * Connection: keep-alive
 * Transfer-Encoding: chunked
 * ```
 *
 * @param {String} headers Headers needing to be parsed
 * @returns {Object} Headers parsed into an object
 */
module.exports = function parseHeaders(headers) {
  var parsed = {};
  var key;
  var val;
  var i;

  if (!headers) { return parsed; }

  utils.forEach(headers.split('\n'), function parser(line) {
    i = line.indexOf(':');
    key = utils.trim(line.substr(0, i)).toLowerCase();
    val = utils.trim(line.substr(i + 1));

    if (key) {
      if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
        return;
      }
      if (key === 'set-cookie') {
        parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
      } else {
        parsed[key] = parsed[key] ? parsed[key] + ', ' + val : val;
      }
    }
  });

  return parsed;
};


/***/ }),

/***/ "./node_modules/axios/lib/helpers/spread.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Syntactic sugar for invoking a function and expanding an array for arguments.
 *
 * Common use case would be to use `Function.prototype.apply`.
 *
 *  ```js
 *  function f(x, y, z) {}
 *  var args = [1, 2, 3];
 *  f.apply(null, args);
 *  ```
 *
 * With `spread` this example can be re-written.
 *
 *  ```js
 *  spread(function(x, y, z) {})([1, 2, 3]);
 *  ```
 *
 * @param {Function} callback
 * @returns {Function}
 */
module.exports = function spread(callback) {
  return function wrap(arr) {
    return callback.apply(null, arr);
  };
};


/***/ }),

/***/ "./node_modules/axios/lib/utils.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__("./node_modules/axios/lib/helpers/bind.js");
var isBuffer = __webpack_require__("./node_modules/is-buffer/index.js");

/*global toString:true*/

// utils is a library of generic helper functions non-specific to axios

var toString = Object.prototype.toString;

/**
 * Determine if a value is an Array
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Array, otherwise false
 */
function isArray(val) {
  return toString.call(val) === '[object Array]';
}

/**
 * Determine if a value is an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an ArrayBuffer, otherwise false
 */
function isArrayBuffer(val) {
  return toString.call(val) === '[object ArrayBuffer]';
}

/**
 * Determine if a value is a FormData
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an FormData, otherwise false
 */
function isFormData(val) {
  return (typeof FormData !== 'undefined') && (val instanceof FormData);
}

/**
 * Determine if a value is a view on an ArrayBuffer
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a view on an ArrayBuffer, otherwise false
 */
function isArrayBufferView(val) {
  var result;
  if ((typeof ArrayBuffer !== 'undefined') && (ArrayBuffer.isView)) {
    result = ArrayBuffer.isView(val);
  } else {
    result = (val) && (val.buffer) && (val.buffer instanceof ArrayBuffer);
  }
  return result;
}

/**
 * Determine if a value is a String
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a String, otherwise false
 */
function isString(val) {
  return typeof val === 'string';
}

/**
 * Determine if a value is a Number
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Number, otherwise false
 */
function isNumber(val) {
  return typeof val === 'number';
}

/**
 * Determine if a value is undefined
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if the value is undefined, otherwise false
 */
function isUndefined(val) {
  return typeof val === 'undefined';
}

/**
 * Determine if a value is an Object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is an Object, otherwise false
 */
function isObject(val) {
  return val !== null && typeof val === 'object';
}

/**
 * Determine if a value is a Date
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Date, otherwise false
 */
function isDate(val) {
  return toString.call(val) === '[object Date]';
}

/**
 * Determine if a value is a File
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a File, otherwise false
 */
function isFile(val) {
  return toString.call(val) === '[object File]';
}

/**
 * Determine if a value is a Blob
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Blob, otherwise false
 */
function isBlob(val) {
  return toString.call(val) === '[object Blob]';
}

/**
 * Determine if a value is a Function
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Function, otherwise false
 */
function isFunction(val) {
  return toString.call(val) === '[object Function]';
}

/**
 * Determine if a value is a Stream
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a Stream, otherwise false
 */
function isStream(val) {
  return isObject(val) && isFunction(val.pipe);
}

/**
 * Determine if a value is a URLSearchParams object
 *
 * @param {Object} val The value to test
 * @returns {boolean} True if value is a URLSearchParams object, otherwise false
 */
function isURLSearchParams(val) {
  return typeof URLSearchParams !== 'undefined' && val instanceof URLSearchParams;
}

/**
 * Trim excess whitespace off the beginning and end of a string
 *
 * @param {String} str The String to trim
 * @returns {String} The String freed of excess whitespace
 */
function trim(str) {
  return str.replace(/^\s*/, '').replace(/\s*$/, '');
}

/**
 * Determine if we're running in a standard browser environment
 *
 * This allows axios to run in a web worker, and react-native.
 * Both environments support XMLHttpRequest, but not fully standard globals.
 *
 * web workers:
 *  typeof window -> undefined
 *  typeof document -> undefined
 *
 * react-native:
 *  navigator.product -> 'ReactNative'
 */
function isStandardBrowserEnv() {
  if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    return false;
  }
  return (
    typeof window !== 'undefined' &&
    typeof document !== 'undefined'
  );
}

/**
 * Iterate over an Array or an Object invoking a function for each item.
 *
 * If `obj` is an Array callback will be called passing
 * the value, index, and complete array for each item.
 *
 * If 'obj' is an Object callback will be called passing
 * the value, key, and complete object for each property.
 *
 * @param {Object|Array} obj The object to iterate
 * @param {Function} fn The callback to invoke for each item
 */
function forEach(obj, fn) {
  // Don't bother if no value provided
  if (obj === null || typeof obj === 'undefined') {
    return;
  }

  // Force an array if not already something iterable
  if (typeof obj !== 'object') {
    /*eslint no-param-reassign:0*/
    obj = [obj];
  }

  if (isArray(obj)) {
    // Iterate over array values
    for (var i = 0, l = obj.length; i < l; i++) {
      fn.call(null, obj[i], i, obj);
    }
  } else {
    // Iterate over object keys
    for (var key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        fn.call(null, obj[key], key, obj);
      }
    }
  }
}

/**
 * Accepts varargs expecting each argument to be an object, then
 * immutably merges the properties of each object and returns result.
 *
 * When multiple objects contain the same key the later object in
 * the arguments list will take precedence.
 *
 * Example:
 *
 * ```js
 * var result = merge({foo: 123}, {foo: 456});
 * console.log(result.foo); // outputs 456
 * ```
 *
 * @param {Object} obj1 Object to merge
 * @returns {Object} Result of all merge properties
 */
function merge(/* obj1, obj2, obj3, ... */) {
  var result = {};
  function assignValue(val, key) {
    if (typeof result[key] === 'object' && typeof val === 'object') {
      result[key] = merge(result[key], val);
    } else {
      result[key] = val;
    }
  }

  for (var i = 0, l = arguments.length; i < l; i++) {
    forEach(arguments[i], assignValue);
  }
  return result;
}

/**
 * Extends object a by mutably adding to it the properties of object b.
 *
 * @param {Object} a The object to be extended
 * @param {Object} b The object to copy properties from
 * @param {Object} thisArg The object to bind function to
 * @return {Object} The resulting value of object a
 */
function extend(a, b, thisArg) {
  forEach(b, function assignValue(val, key) {
    if (thisArg && typeof val === 'function') {
      a[key] = bind(val, thisArg);
    } else {
      a[key] = val;
    }
  });
  return a;
}

module.exports = {
  isArray: isArray,
  isArrayBuffer: isArrayBuffer,
  isBuffer: isBuffer,
  isFormData: isFormData,
  isArrayBufferView: isArrayBufferView,
  isString: isString,
  isNumber: isNumber,
  isObject: isObject,
  isUndefined: isUndefined,
  isDate: isDate,
  isFile: isFile,
  isBlob: isBlob,
  isFunction: isFunction,
  isStream: isStream,
  isURLSearchParams: isURLSearchParams,
  isStandardBrowserEnv: isStandardBrowserEnv,
  forEach: forEach,
  merge: merge,
  extend: extend,
  trim: trim
};


/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/piechart.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts__ = __webpack_require__("./node_modules/highcharts/highcharts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_highcharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__("./node_modules/axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            // prices: '',
            spinner: true,
            percent: {
                btc: 0,
                eth: 0,
                ltc: 0,
                xrp: 0,
                pwr: 0
            }
        };
    },

    methods: {
        loadData: function loadData() {
            var vm = this;
            __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/api/balance').then(function (response) {
                vm.percent.btc = response.data.btc;
                vm.percent.eth = response.data.eth;
                vm.percent.ltc = response.data.ltc;
                vm.percent.xrp = response.data.xrp;
                vm.percent.pwr = response.data.pwr;

                vm.spinner = false;
                vm.applyChart();
            }).catch(function (error) {});
        },
        applyChart: function applyChart() {
            var pieColors = function () {
                var colors = [],

                // base = Highcharts.getOptions().colors[0],
                base = __WEBPACK_IMPORTED_MODULE_0_highcharts___default.a.theme && __WEBPACK_IMPORTED_MODULE_0_highcharts___default.a.theme.contrastTextColor || 'black',
                    i;

                for (i = 0; i < 10; i += 1) {
                    // Start out with a darkened base color (negative brighten), and end
                    // up with a much brighter color
                    colors.push(__WEBPACK_IMPORTED_MODULE_0_highcharts___default.a.Color(base).brighten((i - 3) / 7).get());
                }

                colors = ['#29a7de', '#f98f2a', '#f2f2f2', '#2f2f2f', '#6fecbc'];
                // console.log(colors);

                return colors;
            }();

            __WEBPACK_IMPORTED_MODULE_0_highcharts___default.a.chart('piechart-container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    backgroundColor: null
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        colors: pieColors,
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                            distance: -50,
                            filter: {
                                property: 'percentage',
                                operator: '>',
                                value: 4
                            }
                        }
                    }
                },
                series: [{
                    name: 'Share',
                    data: [{ name: 'BTC', y: this.percent.btc }, { name: 'ETH', y: this.percent.eth }, { name: 'LTC', y: this.percent.ltc }, { name: 'XRP', y: this.percent.xrp }, { name: 'PWR', y: this.percent.pwr }]
                }]
            });
        }
    },

    mounted: function mounted() {
        this.loadData();
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/radialbar.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_radial_progress_dist_vue_radial_progress_min_js__ = __webpack_require__("./node_modules/vue-radial-progress/dist/vue-radial-progress.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_radial_progress_dist_vue_radial_progress_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue_radial_progress_dist_vue_radial_progress_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__("./node_modules/axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            gain: 0,
            total: 100,
            animateSpeed: 1000,
            diameter: 250,
            strokeWidth: 10,
            startColor: '#22d68e',
            stopColor: '#21d28a',
            innerStrokeColor: '#323232',
            timingFunc: 'linear',
            updatePhrase: 'loading'
        };
    },


    methods: {
        loadAsset: function loadAsset() {
            var vm = this;
            __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/api/assetUpdate').then(function (response) {
                vm.gain = response.data;
                vm.spinner = false;
                vm.gain = 92;

                if (vm.gain < 0) {
                    vm.startColor = '#e86465';
                    vm.stopColor = '#af5156';
                    vm.updatePhrase = 'lost in 24hrs';
                } else {
                    vm.updatePhrase = 'gained in 24hrs';
                }
            }).catch(function (error) {});
        },
        timingFuncChanged: function timingFuncChanged(e) {
            this.timingFunc = e.target.value;
        },
        innerStrokeColorChanged: function innerStrokeColorChanged(e) {
            this.innerStrokeColor = e.target.value;
        },
        stopColorChanged: function stopColorChanged(e) {
            this.stopColor = e.target.value;
        },
        startColorChanged: function startColorChanged(e) {
            this.startColor = e.target.value;
        },
        strokeWidthChanged: function strokeWidthChanged(e) {
            e.preventDefault();
            var val = e.target.value;

            if (!val || isNaN(val)) {
                return false;
            }

            this.strokeWidth = parseInt(val);
        },
        diameterChanged: function diameterChanged(e) {
            e.preventDefault();
            var val = e.target.value;

            if (!val || isNaN(val)) {
                return false;
            }

            this.diameter = parseInt(val);
        },
        animateSpeedChanged: function animateSpeedChanged(e) {
            e.preventDefault();
            var val = e.target.value;

            if (!val || isNaN(val)) {
                return false;
            }

            this.animateSpeed = parseInt(val);
        },
        totalStepsChanged: function totalStepsChanged(e) {
            e.preventDefault();
            var val = e.target.value;

            if (!val || isNaN(val)) {
                return false;
            }

            this.totalSteps = parseInt(val);
        }
    },

    mounted: function mounted() {
        this.loadAsset();
    },


    components: {
        RadialProgressBar: __WEBPACK_IMPORTED_MODULE_0_vue_radial_progress_dist_vue_radial_progress_min_js___default.a
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/stockchart.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts_highstock__ = __webpack_require__("./node_modules/highcharts/highstock.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_highcharts_highstock___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_highcharts_highstock__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__("./node_modules/axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            prices: '',
            spinner: true
        };
    },
    methods: {
        loadPrice: function loadPrice(coin) {
            console.log("loading " + coin + " from the sever.....");
            var vm = this;
            __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('/api/prices/' + coin).then(function (response) {

                vm.prices = response.data.original;
                vm.applyChart();
                vm.spinner = false;
            }).catch(function (error) {

                vm.prices = [0, 0]; // draw a line to avoid confusion
                vm.applyChart();
                vm.spinner = false;
                console.log("loading ETH from bottom failed......");
            });
        },
        applyChart: function applyChart() {
            __WEBPACK_IMPORTED_MODULE_0_highcharts_highstock___default.a.theme = {
                colors: ['#3399d4'], // color of line and border of tooltip
                chart: { // chart background color
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [[0, 'rgb(13, 51, 80)'], // useful
                        [1, 'rgb(15, 33, 55)'] // useful
                        ]
                    }
                },
                title: { // top title style
                    style: {
                        color: '#FFFFFF',
                        font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                    }
                },
                xAxis: {
                    gridLineColor: '#1c3e66',
                    labels: {
                        style: {
                            color: '#9ac7e3'
                        }
                    },
                    lineColor: '#1c3e66',
                    minorGridLineColor: '#1c3e66',
                    tickColor: '#1c3e66',
                    title: {
                        style: {
                            color: '#9ac7e3'
                        }
                    }
                },
                yAxis: {
                    gridLineColor: '#1c3e66',
                    labels: {
                        style: {
                            color: '#9ac7e3'
                        }
                    },
                    lineColor: '#1c3e66',
                    minorGridLineColor: '#1c3e66',
                    tickColor: '#1c3e66',
                    tickWidth: 1,
                    title: {
                        style: {
                            color: '#9ac7e3'
                        }
                    }
                },
                tooltip: { //the point
                    backgroundColor: 'rgba(51, 153, 212, 0.8)',
                    style: {
                        color: '#FFFFFF'
                    }
                },

                // top right navigator
                navigation: {
                    buttonOptions: {
                        symbolStroke: '#FFFFFF',
                        theme: {
                            fill: '#3399d4'
                        }
                    }
                },

                // zoom and select style
                rangeSelector: {
                    buttonTheme: {
                        fill: '#0f2137',
                        stroke: '#3399d4',
                        style: {
                            color: 'white'
                        },
                        states: {
                            hover: {
                                fill: '#3399d4',
                                stroke: '#000000',
                                style: {
                                    color: 'white'
                                }
                            },
                            select: {
                                fill: '#3399d4',
                                stroke: '#3399d4',
                                style: {
                                    color: 'white'
                                }
                            }
                        }
                    },
                    inputBoxBorderColor: '#1c3e66',
                    inputStyle: {
                        backgroundColor: '#0f2137',
                        color: 'white'
                    },
                    labelStyle: {
                        color: 'white'
                    }
                },

                navigator: { // bottom navigator
                    handles: {
                        backgroundColor: 'rgb(15, 33, 55)',
                        borderColor: '#9ac7e3'
                    },
                    outlineColor: '#1c3e66',
                    maskFill: 'rgba(255,255,255,0.1)',
                    series: {
                        color: '#FFF',
                        lineColor: '#3399d4'
                    },
                    xAxis: {
                        gridLineColor: '#1c3e66'
                    }
                },

                // bottom scroll bar
                scrollbar: {
                    barBackgroundColor: '#3399d4',
                    barBorderColor: '#9ac7e3',
                    buttonArrowColor: '#9ac7e3',
                    buttonBackgroundColor: '#0f2137',
                    buttonBorderColor: '#9ac7e3',
                    rifleColor: '#FFF',
                    trackBackgroundColor: '#3399d4',
                    trackBorderColor: '#3399d4'
                }
            };

            // Apply the theme
            __WEBPACK_IMPORTED_MODULE_0_highcharts_highstock___default.a.setOptions(__WEBPACK_IMPORTED_MODULE_0_highcharts_highstock___default.a.theme);

            __WEBPACK_IMPORTED_MODULE_0_highcharts_highstock___default.a.stockChart('stockChart-container', {

                rangeSelector: {
                    selected: 1
                },

                title: {
                    text: 'myCryptoWallet Market Price'
                },

                exporting: {
                    enabled: false
                },

                series: [{
                    name: 'Market Price',
                    data: this.prices,
                    tooltip: {
                        valueDecimals: 2
                    }
                }]
            });
        }
    },
    mounted: function mounted() {
        this.loadPrice('btc');
    }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/dashboard/components/radialbar.vue":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.radial-progress-display {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n.radial-progress-controls {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./node_modules/highcharts/highcharts.js":
/***/ (function(module, exports) {

/*
 Highcharts JS v6.0.7 (2018-02-16)

 (c) 2009-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(S,K){"object"===typeof module&&module.exports?module.exports=S.document?K(S):K:S.Highcharts=K(S)})("undefined"!==typeof window?window:this,function(S){var K=function(){var a="undefined"===typeof S?window:S,B=a.document,H=a.navigator&&a.navigator.userAgent||"",E=B&&B.createElementNS&&!!B.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,q=/(edge|msie|trident)/i.test(H)&&!a.opera,f=-1!==H.indexOf("Firefox"),l=-1!==H.indexOf("Chrome"),t=f&&4>parseInt(H.split("Firefox/")[1],
10);return a.Highcharts?a.Highcharts.error(16,!0):{product:"Highcharts",version:"6.0.7",deg2rad:2*Math.PI/360,doc:B,hasBidiBug:t,hasTouch:B&&void 0!==B.documentElement.ontouchstart,isMS:q,isWebKit:-1!==H.indexOf("AppleWebKit"),isFirefox:f,isChrome:l,isSafari:!l&&-1!==H.indexOf("Safari"),isTouchDevice:/(Mobile|Android|Windows Phone)/.test(H),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:E,win:a,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},
charts:[]}}();(function(a){a.timers=[];var B=a.charts,H=a.doc,E=a.win;a.error=function(q,f){q=a.isNumber(q)?"Highcharts error #"+q+": www.highcharts.com/errors/"+q:q;if(f)throw Error(q);E.console&&console.log(q)};a.Fx=function(a,f,l){this.options=f;this.elem=a;this.prop=l};a.Fx.prototype={dSetter:function(){var a=this.paths[0],f=this.paths[1],l=[],t=this.now,n=a.length,v;if(1===t)l=this.toD;else if(n===f.length&&1>t)for(;n--;)v=parseFloat(a[n]),l[n]=isNaN(v)?f[n]:t*parseFloat(f[n]-v)+v;else l=f;this.elem.attr("d",
l,null,!0)},update:function(){var a=this.elem,f=this.prop,l=this.now,t=this.options.step;if(this[f+"Setter"])this[f+"Setter"]();else a.attr?a.element&&a.attr(f,l,null,!0):a.style[f]=l+this.unit;t&&t.call(a,l,this)},run:function(q,f,l){var t=this,n=t.options,v=function(a){return v.stopped?!1:t.step(a)},u=E.requestAnimationFrame||function(a){setTimeout(a,13)},c=function(){for(var b=0;b<a.timers.length;b++)a.timers[b]()||a.timers.splice(b--,1);a.timers.length&&u(c)};q===f?(delete n.curAnim[this.prop],
n.complete&&0===a.keys(n.curAnim).length&&n.complete.call(this.elem)):(this.startTime=+new Date,this.start=q,this.end=f,this.unit=l,this.now=this.start,this.pos=0,v.elem=this.elem,v.prop=this.prop,v()&&1===a.timers.push(v)&&u(c))},step:function(q){var f=+new Date,l,t=this.options,n=this.elem,v=t.complete,u=t.duration,c=t.curAnim;n.attr&&!n.element?q=!1:q||f>=u+this.startTime?(this.now=this.end,this.pos=1,this.update(),l=c[this.prop]=!0,a.objectEach(c,function(a){!0!==a&&(l=!1)}),l&&v&&v.call(n),q=
!1):(this.pos=t.easing((f-this.startTime)/u),this.now=this.start+(this.end-this.start)*this.pos,this.update(),q=!0);return q},initPath:function(q,f,l){function t(a){var c,h;for(e=a.length;e--;)c="M"===a[e]||"L"===a[e],h=/[a-zA-Z]/.test(a[e+3]),c&&h&&a.splice(e+1,0,a[e+1],a[e+2],a[e+1],a[e+2])}function n(a,c){for(;a.length<h;){a[0]=c[h-a.length];var b=a.slice(0,d);[].splice.apply(a,[0,0].concat(b));p&&(b=a.slice(a.length-d),[].splice.apply(a,[a.length,0].concat(b)),e--)}a[0]="M"}function v(a,c){for(var b=
(h-a.length)/d;0<b&&b--;)k=a.slice().splice(a.length/r-d,d*r),k[0]=c[h-d-b*d],m&&(k[d-6]=k[d-2],k[d-5]=k[d-1]),[].splice.apply(a,[a.length/r,0].concat(k)),p&&b--}f=f||"";var u,c=q.startX,b=q.endX,m=-1<f.indexOf("C"),d=m?7:3,h,k,e;f=f.split(" ");l=l.slice();var p=q.isArea,r=p?2:1,I;m&&(t(f),t(l));if(c&&b){for(e=0;e<c.length;e++)if(c[e]===b[0]){u=e;break}else if(c[0]===b[b.length-c.length+e]){u=e;I=!0;break}void 0===u&&(f=[])}f.length&&a.isNumber(u)&&(h=l.length+u*r*d,I?(n(f,l),v(l,f)):(n(l,f),v(f,
l)));return[f,l]}};a.Fx.prototype.fillSetter=a.Fx.prototype.strokeSetter=function(){this.elem.attr(this.prop,a.color(this.start).tweenTo(a.color(this.end),this.pos),null,!0)};a.merge=function(){var q,f=arguments,l,t={},n=function(l,q){"object"!==typeof l&&(l={});a.objectEach(q,function(c,b){!a.isObject(c,!0)||a.isClass(c)||a.isDOMElement(c)?l[b]=q[b]:l[b]=n(l[b]||{},c)});return l};!0===f[0]&&(t=f[1],f=Array.prototype.slice.call(f,2));l=f.length;for(q=0;q<l;q++)t=n(t,f[q]);return t};a.pInt=function(a,
f){return parseInt(a,f||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(q,f){return!!q&&"object"===typeof q&&(!f||!a.isArray(q))};a.isDOMElement=function(q){return a.isObject(q)&&"number"===typeof q.nodeType};a.isClass=function(q){var f=q&&q.constructor;return!(!a.isObject(q,!0)||a.isDOMElement(q)||!f||!f.name||"Object"===f.name)};a.isNumber=function(a){return"number"===
typeof a&&!isNaN(a)&&Infinity>a&&-Infinity<a};a.erase=function(a,f){for(var l=a.length;l--;)if(a[l]===f){a.splice(l,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(q,f,l){var t;a.isString(f)?a.defined(l)?q.setAttribute(f,l):q&&q.getAttribute&&(t=q.getAttribute(f)):a.defined(f)&&a.isObject(f)&&a.objectEach(f,function(a,l){q.setAttribute(l,a)});return t};a.splat=function(q){return a.isArray(q)?q:[q]};a.syncTimeout=function(a,f,l){if(f)return setTimeout(a,f,l);a.call(0,
l)};a.extend=function(a,f){var l;a||(a={});for(l in f)a[l]=f[l];return a};a.pick=function(){var a=arguments,f,l,t=a.length;for(f=0;f<t;f++)if(l=a[f],void 0!==l&&null!==l)return l};a.css=function(q,f){a.isMS&&!a.svg&&f&&void 0!==f.opacity&&(f.filter="alpha(opacity\x3d"+100*f.opacity+")");a.extend(q.style,f)};a.createElement=function(q,f,l,t,n){q=H.createElement(q);var v=a.css;f&&a.extend(q,f);n&&v(q,{padding:0,border:"none",margin:0});l&&v(q,l);t&&t.appendChild(q);return q};a.extendClass=function(q,
f){var l=function(){};l.prototype=new q;a.extend(l.prototype,f);return l};a.pad=function(a,f,l){return Array((f||2)+1-String(a).length).join(l||0)+a};a.relativeLength=function(a,f,l){return/%$/.test(a)?f*parseFloat(a)/100+(l||0):parseFloat(a)};a.wrap=function(a,f,l){var t=a[f];a[f]=function(){var a=Array.prototype.slice.call(arguments),v=arguments,u=this;u.proceed=function(){t.apply(u,arguments.length?arguments:v)};a.unshift(t);a=l.apply(this,a);u.proceed=null;return a}};a.formatSingle=function(q,
f,l){var t=/\.([0-9])/,n=a.defaultOptions.lang;/f$/.test(q)?(l=(l=q.match(t))?l[1]:-1,null!==f&&(f=a.numberFormat(f,l,n.decimalPoint,-1<q.indexOf(",")?n.thousandsSep:""))):f=(l||a.time).dateFormat(q,f);return f};a.format=function(q,f,l){for(var t="{",n=!1,v,u,c,b,m=[],d;q;){t=q.indexOf(t);if(-1===t)break;v=q.slice(0,t);if(n){v=v.split(":");u=v.shift().split(".");b=u.length;d=f;for(c=0;c<b;c++)d&&(d=d[u[c]]);v.length&&(d=a.formatSingle(v.join(":"),d,l));m.push(d)}else m.push(v);q=q.slice(t+1);t=(n=
!n)?"}":"{"}m.push(q);return m.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(q,f,l,t,n){var v,u=q;l=a.pick(l,1);v=q/l;f||(f=n?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===t&&(1===l?f=a.grep(f,function(a){return 0===a%1}):.1>=l&&(f=[1/l])));for(t=0;t<f.length&&!(u=f[t],n&&u*l>=q||!n&&v<=(f[t]+(f[t+1]||f[t]))/2);t++);return u=a.correctFloat(u*l,-Math.round(Math.log(.001)/Math.LN10))};a.stableSort=function(a,f){var l=
a.length,t,n;for(n=0;n<l;n++)a[n].safeI=n;a.sort(function(a,l){t=f(a,l);return 0===t?a.safeI-l.safeI:t});for(n=0;n<l;n++)delete a[n].safeI};a.arrayMin=function(a){for(var f=a.length,l=a[0];f--;)a[f]<l&&(l=a[f]);return l};a.arrayMax=function(a){for(var f=a.length,l=a[0];f--;)a[f]>l&&(l=a[f]);return l};a.destroyObjectProperties=function(q,f){a.objectEach(q,function(a,t){a&&a!==f&&a.destroy&&a.destroy();delete q[t]})};a.discardElement=function(q){var f=a.garbageBin;f||(f=a.createElement("div"));q&&f.appendChild(q);
f.innerHTML=""};a.correctFloat=function(a,f){return parseFloat(a.toPrecision(f||14))};a.setAnimation=function(q,f){f.renderer.globalAnimation=a.pick(q,f.options.chart.animation,!0)};a.animObject=function(q){return a.isObject(q)?a.merge(q):{duration:q?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=function(q,f,l,t){q=+q||0;f=+f;var n=a.defaultOptions.lang,v=(q.toString().split(".")[1]||"").split("e")[0].length,u,
c,b=q.toString().split("e");-1===f?f=Math.min(v,20):a.isNumber(f)?f&&b[1]&&0>b[1]&&(u=f+ +b[1],0<=u?(b[0]=(+b[0]).toExponential(u).split("e")[0],f=u):(b[0]=b[0].split(".")[0]||0,q=20>f?(b[0]*Math.pow(10,b[1])).toFixed(f):0,b[1]=0)):f=2;c=(Math.abs(b[1]?b[0]:q)+Math.pow(10,-Math.max(f,v)-1)).toFixed(f);v=String(a.pInt(c));u=3<v.length?v.length%3:0;l=a.pick(l,n.decimalPoint);t=a.pick(t,n.thousandsSep);q=(0>q?"-":"")+(u?v.substr(0,u)+t:"");q+=v.substr(u).replace(/(\d{3})(?=\d)/g,"$1"+t);f&&(q+=l+c.slice(-f));
b[1]&&0!==+q&&(q+="e"+b[1]);return q};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*a)-1)};a.getStyle=function(q,f,l){if("width"===f)return Math.min(q.offsetWidth,q.scrollWidth)-a.getStyle(q,"padding-left")-a.getStyle(q,"padding-right");if("height"===f)return Math.min(q.offsetHeight,q.scrollHeight)-a.getStyle(q,"padding-top")-a.getStyle(q,"padding-bottom");E.getComputedStyle||a.error(27,!0);if(q=E.getComputedStyle(q,void 0))q=q.getPropertyValue(f),a.pick(l,"opacity"!==f)&&(q=a.pInt(q));
return q};a.inArray=function(q,f){return(a.indexOfPolyfill||Array.prototype.indexOf).call(f,q)};a.grep=function(q,f){return(a.filterPolyfill||Array.prototype.filter).call(q,f)};a.find=Array.prototype.find?function(a,f){return a.find(f)}:function(a,f){var l,t=a.length;for(l=0;l<t;l++)if(f(a[l],l))return a[l]};a.map=function(a,f){for(var l=[],t=0,n=a.length;t<n;t++)l[t]=f.call(a[t],a[t],t,a);return l};a.keys=function(q){return(a.keysPolyfill||Object.keys).call(void 0,q)};a.reduce=function(q,f,l){return(a.reducePolyfill||
Array.prototype.reduce).call(q,f,l)};a.offset=function(a){var f=H.documentElement;a=a.parentElement?a.getBoundingClientRect():{top:0,left:0};return{top:a.top+(E.pageYOffset||f.scrollTop)-(f.clientTop||0),left:a.left+(E.pageXOffset||f.scrollLeft)-(f.clientLeft||0)}};a.stop=function(q,f){for(var l=a.timers.length;l--;)a.timers[l].elem!==q||f&&f!==a.timers[l].prop||(a.timers[l].stopped=!0)};a.each=function(q,f,l){return(a.forEachPolyfill||Array.prototype.forEach).call(q,f,l)};a.objectEach=function(a,
f,l){for(var t in a)a.hasOwnProperty(t)&&f.call(l,a[t],t,a)};a.isPrototype=function(q){return q===a.Axis.prototype||q===a.Chart.prototype||q===a.Point.prototype||q===a.Series.prototype||q===a.Tick.prototype};a.addEvent=function(q,f,l){var t,n=q.addEventListener||a.addEventListenerPolyfill;t=a.isPrototype(q)?"protoEvents":"hcEvents";t=q[t]=q[t]||{};n&&n.call(q,f,l,!1);t[f]||(t[f]=[]);t[f].push(l);return function(){a.removeEvent(q,f,l)}};a.removeEvent=function(q,f,l){function t(c,b){var m=q.removeEventListener||
a.removeEventListenerPolyfill;m&&m.call(q,c,b,!1)}function n(c){var b,m;q.nodeName&&(f?(b={},b[f]=!0):b=c,a.objectEach(b,function(a,h){if(c[h])for(m=c[h].length;m--;)t(h,c[h][m])}))}var v,u;a.each(["protoEvents","hcEvents"],function(c){var b=q[c];b&&(f?(v=b[f]||[],l?(u=a.inArray(l,v),-1<u&&(v.splice(u,1),b[f]=v),t(f,l)):(n(b),b[f]=[])):(n(b),q[c]={}))})};a.fireEvent=function(q,f,l,t){var n,v,u,c,b;l=l||{};H.createEvent&&(q.dispatchEvent||q.fireEvent)?(n=H.createEvent("Events"),n.initEvent(f,!0,!0),
a.extend(n,l),q.dispatchEvent?q.dispatchEvent(n):q.fireEvent(f,n)):a.each(["protoEvents","hcEvents"],function(m){if(q[m])for(v=q[m][f]||[],u=v.length,l.target||a.extend(l,{preventDefault:function(){l.defaultPrevented=!0},target:q,type:f}),c=0;c<u;c++)(b=v[c])&&!1===b.call(q,l)&&l.preventDefault()});t&&!l.defaultPrevented&&t(l)};a.animate=function(q,f,l){var t,n="",v,u,c;a.isObject(l)||(c=arguments,l={duration:c[2],easing:c[3],complete:c[4]});a.isNumber(l.duration)||(l.duration=400);l.easing="function"===
typeof l.easing?l.easing:Math[l.easing]||Math.easeInOutSine;l.curAnim=a.merge(f);a.objectEach(f,function(c,m){a.stop(q,m);u=new a.Fx(q,l,m);v=null;"d"===m?(u.paths=u.initPath(q,q.d,f.d),u.toD=f.d,t=0,v=1):q.attr?t=q.attr(m):(t=parseFloat(a.getStyle(q,m))||0,"opacity"!==m&&(n="px"));v||(v=c);v&&v.match&&v.match("px")&&(v=v.replace(/px/g,""));u.run(t,v,n)})};a.seriesType=function(q,f,l,t,n){var v=a.getOptions(),u=a.seriesTypes;v.plotOptions[q]=a.merge(v.plotOptions[f],l);u[q]=a.extendClass(u[f]||function(){},
t);u[q].prototype.type=q;n&&(u[q].prototype.pointClass=a.extendClass(a.Point,n));return u[q]};a.uniqueKey=function(){var a=Math.random().toString(36).substring(2,9),f=0;return function(){return"highcharts-"+a+"-"+f++}}();E.jQuery&&(E.jQuery.fn.highcharts=function(){var q=[].slice.call(arguments);if(this[0])return q[0]?(new (a[a.isString(q[0])?q.shift():"Chart"])(this[0],q[0],q[1]),this):B[a.attr(this[0],"data-highcharts-chart")]})})(K);(function(a){var B=a.each,H=a.isNumber,E=a.map,q=a.merge,f=a.pInt;
a.Color=function(l){if(!(this instanceof a.Color))return new a.Color(l);this.init(l)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[f(a[1]),f(a[2]),f(a[3]),parseFloat(a[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[f(a[1]),f(a[2]),f(a[3]),1]}}],names:{none:"rgba(255,255,255,0)",white:"#ffffff",black:"#000000"},init:function(l){var f,
n,v,u;if((this.input=l=this.names[l&&l.toLowerCase?l.toLowerCase():""]||l)&&l.stops)this.stops=E(l.stops,function(c){return new a.Color(c[1])});else if(l&&l.charAt&&"#"===l.charAt()&&(f=l.length,l=parseInt(l.substr(1),16),7===f?n=[(l&16711680)>>16,(l&65280)>>8,l&255,1]:4===f&&(n=[(l&3840)>>4|(l&3840)>>8,(l&240)>>4|l&240,(l&15)<<4|l&15,1])),!n)for(v=this.parsers.length;v--&&!n;)u=this.parsers[v],(f=u.regex.exec(l))&&(n=u.parse(f));this.rgba=n||[]},get:function(a){var l=this.input,n=this.rgba,v;this.stops?
(v=q(l),v.stops=[].concat(v.stops),B(this.stops,function(n,c){v.stops[c]=[v.stops[c][0],n.get(a)]})):v=n&&H(n[0])?"rgb"===a||!a&&1===n[3]?"rgb("+n[0]+","+n[1]+","+n[2]+")":"a"===a?n[3]:"rgba("+n.join(",")+")":l;return v},brighten:function(a){var l,n=this.rgba;if(this.stops)B(this.stops,function(n){n.brighten(a)});else if(H(a)&&0!==a)for(l=0;3>l;l++)n[l]+=f(255*a),0>n[l]&&(n[l]=0),255<n[l]&&(n[l]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this},tweenTo:function(a,f){var n=this.rgba,
l=a.rgba;l.length&&n&&n.length?(a=1!==l[3]||1!==n[3],f=(a?"rgba(":"rgb(")+Math.round(l[0]+(n[0]-l[0])*(1-f))+","+Math.round(l[1]+(n[1]-l[1])*(1-f))+","+Math.round(l[2]+(n[2]-l[2])*(1-f))+(a?","+(l[3]+(n[3]-l[3])*(1-f)):"")+")"):f=a.input||"none";return f}};a.color=function(l){return new a.Color(l)}})(K);(function(a){var B,H,E=a.addEvent,q=a.animate,f=a.attr,l=a.charts,t=a.color,n=a.css,v=a.createElement,u=a.defined,c=a.deg2rad,b=a.destroyObjectProperties,m=a.doc,d=a.each,h=a.extend,k=a.erase,e=a.grep,
p=a.hasTouch,r=a.inArray,I=a.isArray,z=a.isFirefox,M=a.isMS,D=a.isObject,C=a.isString,x=a.isWebKit,F=a.merge,A=a.noop,J=a.objectEach,G=a.pick,g=a.pInt,w=a.removeEvent,L=a.stop,P=a.svg,N=a.SVG_NS,O=a.symbolSizes,R=a.win;B=a.SVGElement=function(){return this};h(B.prototype,{opacity:1,SVG_NS:N,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline".split(" "),init:function(a,g){this.element="span"===g?v(g):m.createElementNS(this.SVG_NS,
g);this.renderer=a},animate:function(y,g,c){g=a.animObject(G(g,this.renderer.globalAnimation,!0));0!==g.duration?(c&&(g.complete=c),q(this,y,g)):(this.attr(y,null,c),g.step&&g.step.call(this));return this},colorGradient:function(y,g,c){var w=this.renderer,h,b,e,k,p,Q,x,N,m,A,r=[],P;y.radialGradient?b="radialGradient":y.linearGradient&&(b="linearGradient");b&&(e=y[b],p=w.gradients,x=y.stops,A=c.radialReference,I(e)&&(y[b]=e={x1:e[0],y1:e[1],x2:e[2],y2:e[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===
b&&A&&!u(e.gradientUnits)&&(k=e,e=F(e,w.getRadialAttr(A,k),{gradientUnits:"userSpaceOnUse"})),J(e,function(a,y){"id"!==y&&r.push(y,a)}),J(x,function(a){r.push(a)}),r=r.join(","),p[r]?A=p[r].attr("id"):(e.id=A=a.uniqueKey(),p[r]=Q=w.createElement(b).attr(e).add(w.defs),Q.radAttr=k,Q.stops=[],d(x,function(y){0===y[1].indexOf("rgba")?(h=a.color(y[1]),N=h.get("rgb"),m=h.get("a")):(N=y[1],m=1);y=w.createElement("stop").attr({offset:y[0],"stop-color":N,"stop-opacity":m}).add(Q);Q.stops.push(y)})),P="url("+
w.url+"#"+A+")",c.setAttribute(g,P),c.gradient=r,y.toString=function(){return P})},applyTextOutline:function(y){var g=this.element,c,w,h,e,b;-1!==y.indexOf("contrast")&&(y=y.replace(/contrast/g,this.renderer.getContrast(g.style.fill)));y=y.split(" ");w=y[y.length-1];if((h=y[0])&&"none"!==h&&a.svg){this.fakeTS=!0;y=[].slice.call(g.getElementsByTagName("tspan"));this.ySetter=this.xSetter;h=h.replace(/(^[\d\.]+)(.*?)$/g,function(a,y,g){return 2*y+g});for(b=y.length;b--;)c=y[b],"highcharts-text-outline"===
c.getAttribute("class")&&k(y,g.removeChild(c));e=g.firstChild;d(y,function(a,y){0===y&&(a.setAttribute("x",g.getAttribute("x")),y=g.getAttribute("y"),a.setAttribute("y",y||0),null===y&&g.setAttribute("y",0));a=a.cloneNode(1);f(a,{"class":"highcharts-text-outline",fill:w,stroke:w,"stroke-width":h,"stroke-linejoin":"round"});g.insertBefore(a,e)})}},attr:function(a,g,c,w){var y,h=this.element,b,e=this,d,k;"string"===typeof a&&void 0!==g&&(y=a,a={},a[y]=g);"string"===typeof a?e=(this[a+"Getter"]||this._defaultGetter).call(this,
a,h):(J(a,function(y,g){d=!1;w||L(this,g);this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)$/.test(g)&&(b||(this.symbolAttr(a),b=!0),d=!0);!this.rotation||"x"!==g&&"y"!==g||(this.doTransform=!0);d||(k=this[g+"Setter"]||this._defaultSetter,k.call(this,y,g,h),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(g)&&this.updateShadows(g,y,k))},this),this.afterSetters());c&&c.call(this);return e},afterSetters:function(){this.doTransform&&(this.updateTransform(),
this.doTransform=!1)},updateShadows:function(a,g,c){for(var y=this.shadows,w=y.length;w--;)c.call(y[w],"height"===a?Math.max(g-(y[w].cutHeight||0),0):"d"===a?this.d:g,a,y[w])},addClass:function(a,g){var y=this.attr("class")||"";-1===y.indexOf(a)&&(g||(a=(y+(y?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==r(a,(this.attr("class")||"").split(" "))},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(a,""))},symbolAttr:function(a){var y=
this;d("x y r start end width height innerR anchorX anchorY".split(" "),function(g){y[g]=G(a[g],y[g])});y.attr({d:y.renderer.symbols[y.symbolName](y.x,y.y,y.width,y.height,y)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,g){var y;g=g||a.strokeWidth||0;y=Math.round(g)%2/2;a.x=Math.floor(a.x||this.x||0)+y;a.y=Math.floor(a.y||this.y||0)+y;a.width=Math.floor((a.width||this.width||0)-2*y);a.height=Math.floor((a.height||this.height||0)-
2*y);u(a.strokeWidth)&&(a.strokeWidth=g);return a},css:function(a){var y=this.styles,c={},w=this.element,b,e="",d,k=!y,p=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);y&&J(a,function(a,g){a!==y[g]&&(c[g]=a,k=!0)});k&&(y&&(a=h(y,c)),b=this.textWidth=a&&a.width&&"auto"!==a.width&&"text"===w.nodeName.toLowerCase()&&g(a.width),this.styles=a,b&&!P&&this.renderer.forExport&&delete a.width,w.namespaceURI===this.SVG_NS?(d=function(a,y){return"-"+y.toLowerCase()},J(a,function(a,y){-1===
r(y,p)&&(e+=y.replace(/([A-Z])/g,d)+":"+a+";")}),e&&f(w,"style",e)):n(w,a),this.added&&("text"===this.element.nodeName&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline)));return this},strokeWidth:function(){return this["stroke-width"]||0},on:function(a,g){var y=this,c=y.element;p&&"click"===a?(c.ontouchstart=function(a){y.touchEventFired=Date.now();a.preventDefault();g.call(c,a)},c.onclick=function(a){(-1===R.navigator.userAgent.indexOf("Android")||1100<Date.now()-
(y.touchEventFired||0))&&g.call(c,a)}):c["on"+a]=g;return this},setRadialReference:function(a){var y=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;y&&y.radAttr&&y.animate(this.renderer.getRadialAttr(a,y.radAttr));return this},translate:function(a,g){return this.attr({translateX:a,translateY:g})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,g=this.translateY||0,c=this.scaleX,w=this.scaleY,
h=this.inverted,e=this.rotation,b=this.matrix,d=this.element;h&&(a+=this.width,g+=this.height);a=["translate("+a+","+g+")"];u(b)&&a.push("matrix("+b.join(",")+")");h?a.push("rotate(90) scale(-1,1)"):e&&a.push("rotate("+e+" "+G(this.rotationOriginX,d.getAttribute("x"),0)+" "+G(this.rotationOriginY,d.getAttribute("y")||0)+")");(u(c)||u(w))&&a.push("scale("+G(c,1)+" "+G(w,1)+")");a.length&&d.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},
align:function(a,g,c){var y,w,h,e,d={};w=this.renderer;h=w.alignedObjects;var b,p;if(a){if(this.alignOptions=a,this.alignByTranslate=g,!c||C(c))this.alignTo=y=c||"renderer",k(h,this),h.push(this),c=null}else a=this.alignOptions,g=this.alignByTranslate,y=this.alignTo;c=G(c,w[y],w);y=a.align;w=a.verticalAlign;h=(c.x||0)+(a.x||0);e=(c.y||0)+(a.y||0);"right"===y?b=1:"center"===y&&(b=2);b&&(h+=(c.width-(a.width||0))/b);d[g?"translateX":"x"]=Math.round(h);"bottom"===w?p=1:"middle"===w&&(p=2);p&&(e+=(c.height-
(a.height||0))/p);d[g?"translateY":"y"]=Math.round(e);this[this.placed?"animate":"attr"](d);this.placed=!0;this.alignAttr=d;return this},getBBox:function(a,g){var y,w=this.renderer,b,e=this.element,k=this.styles,p,x=this.textStr,N,m=w.cache,A=w.cacheKeys,F;g=G(g,this.rotation);b=g*c;p=k&&k.fontSize;u(x)&&(F=x.toString(),-1===F.indexOf("\x3c")&&(F=F.replace(/[0-9]/g,"0")),F+=["",g||0,p,k&&k.width,k&&k.textOverflow].join());F&&!a&&(y=m[F]);if(!y){if(e.namespaceURI===this.SVG_NS||w.forExport){try{(N=
this.fakeTS&&function(a){d(e.querySelectorAll(".highcharts-text-outline"),function(y){y.style.display=a})})&&N("none"),y=e.getBBox?h({},e.getBBox()):{width:e.offsetWidth,height:e.offsetHeight},N&&N("")}catch(W){}if(!y||0>y.width)y={width:0,height:0}}else y=this.htmlGetBBox();w.isSVG&&(a=y.width,w=y.height,k&&"11px"===k.fontSize&&17===Math.round(w)&&(y.height=w=14),g&&(y.width=Math.abs(w*Math.sin(b))+Math.abs(a*Math.cos(b)),y.height=Math.abs(w*Math.cos(b))+Math.abs(a*Math.sin(b))));if(F&&0<y.height){for(;250<
A.length;)delete m[A.shift()];m[F]||A.push(F);m[F]=y}}return y},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var y=this;y.animate({opacity:0},{duration:a||150,complete:function(){y.attr({y:-9999})}})},add:function(a){var y=this.renderer,g=this.element,c;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&y.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)c=
this.zIndexSetter();c||(a?a.element:y.box).appendChild(g);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var y=a.parentNode;y&&y.removeChild(a)},destroy:function(){var a=this,g=a.element||{},c=a.renderer.isSVG&&"SPAN"===g.nodeName&&a.parentGroup,w=g.ownerSVGElement,h=a.clipPath;g.onclick=g.onmouseout=g.onmouseover=g.onmousemove=g.point=null;L(a);h&&w&&(d(w.querySelectorAll("[clip-path],[CLIP-PATH]"),function(a){var g=a.getAttribute("clip-path"),y=h.element.id;(-1<g.indexOf("(#"+
y+")")||-1<g.indexOf('("#'+y+'")'))&&a.removeAttribute("clip-path")}),a.clipPath=h.destroy());if(a.stops){for(w=0;w<a.stops.length;w++)a.stops[w]=a.stops[w].destroy();a.stops=null}a.safeRemoveChild(g);for(a.destroyShadows();c&&c.div&&0===c.div.childNodes.length;)g=c.parentGroup,a.safeRemoveChild(c.div),delete c.div,c=g;a.alignTo&&k(a.renderer.alignedObjects,a);J(a,function(g,y){delete a[y]});return null},shadow:function(a,g,c){var y=[],w,h,e=this.element,b,d,k,p;if(!a)this.destroyShadows();else if(!this.shadows){d=
G(a.width,3);k=(a.opacity||.15)/d;p=this.parentInverted?"(-1,-1)":"("+G(a.offsetX,1)+", "+G(a.offsetY,1)+")";for(w=1;w<=d;w++)h=e.cloneNode(0),b=2*d+1-2*w,f(h,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":k*w,"stroke-width":b,transform:"translate"+p,fill:"none"}),c&&(f(h,"height",Math.max(f(h,"height")-b,0)),h.cutHeight=b),g?g.element.appendChild(h):e.parentNode&&e.parentNode.insertBefore(h,e),y.push(h);this.shadows=y}return this},destroyShadows:function(){d(this.shadows||[],function(a){this.safeRemoveChild(a)},
this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=G(this[a+"Value"],this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,g,c){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[g]!==a&&(c.setAttribute(g,a),this[g]=a)},dashstyleSetter:function(a){var y,c=this["stroke-width"];"inherit"===
c&&(c=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(y=a.length;y--;)a[y]=g(a[y])*c;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.alignValue=a;this.element.setAttribute("text-anchor",{left:"start",center:"middle",
right:"end"}[a])},opacitySetter:function(a,g,c){this[g]=a;c.setAttribute(g,a)},titleSetter:function(a){var g=this.element.getElementsByTagName("title")[0];g||(g=m.createElementNS(this.SVG_NS,"title"),this.element.appendChild(g));g.firstChild&&g.removeChild(g.firstChild);g.appendChild(m.createTextNode(String(G(a),"").replace(/<[^>]*>/g,"").replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},
fillSetter:function(a,g,c){"string"===typeof a?c.setAttribute(g,a):a&&this.colorGradient(a,g,c)},visibilitySetter:function(a,g,c){"inherit"===a?c.removeAttribute(g):this[g]!==a&&c.setAttribute(g,a);this[g]=a},zIndexSetter:function(a,c){var w=this.renderer,y=this.parentGroup,h=(y||w).element||w.box,e,b=this.element,d,k,w=h===w.box;e=this.added;var p;u(a)&&(b.zIndex=a,a=+a,this[c]===a&&(e=!1),this[c]=a);if(e){(a=this.zIndex)&&y&&(y.handleZ=!0);c=h.childNodes;for(p=c.length-1;0<=p&&!d;p--)if(y=c[p],
e=y.zIndex,k=!u(e),y!==b)if(0>a&&k&&!w&&!p)h.insertBefore(b,c[p]),d=!0;else if(g(e)<=a||k&&(!u(a)||0<=a))h.insertBefore(b,c[p+1]||null),d=!0;d||(h.insertBefore(b,c[w?3:0]||null),d=!0)}return d},_defaultSetter:function(a,g,c){c.setAttribute(g,a)}});B.prototype.yGetter=B.prototype.xGetter;B.prototype.translateXSetter=B.prototype.translateYSetter=B.prototype.rotationSetter=B.prototype.verticalAlignSetter=B.prototype.rotationOriginXSetter=B.prototype.rotationOriginYSetter=B.prototype.scaleXSetter=B.prototype.scaleYSetter=
B.prototype.matrixSetter=function(a,g){this[g]=a;this.doTransform=!0};B.prototype["stroke-widthSetter"]=B.prototype.strokeSetter=function(a,g,c){this[g]=a;this.stroke&&this["stroke-width"]?(B.prototype.fillSetter.call(this,this.stroke,"stroke",c),c.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===g&&0===a&&this.hasStroke&&(c.removeAttribute("stroke"),this.hasStroke=!1)};H=a.SVGRenderer=function(){this.init.apply(this,arguments)};h(H.prototype,{Element:B,SVG_NS:N,
init:function(a,g,c,w,h,e){var y;w=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(w));y=w.element;a.appendChild(y);f(a,"dir","ltr");-1===a.innerHTML.indexOf("xmlns")&&f(y,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=y;this.boxWrapper=w;this.alignedObjects=[];this.url=(z||x)&&m.getElementsByTagName("base").length?R.location.href.replace(/#.*?$/,"").replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(m.createTextNode("Created with Highcharts 6.0.7"));
this.defs=this.createElement("defs").add();this.allowHTML=e;this.forExport=h;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(g,c,!1);var b;z&&a.getBoundingClientRect&&(g=function(){n(a,{left:0,top:0});b=a.getBoundingClientRect();n(a,{left:Math.ceil(b.left)-b.left+"px",top:Math.ceil(b.top)-b.top+"px"})},g(),this.unSubPixelFix=E(R,"resize",g))},getStyle:function(a){return this.style=h({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},
a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();b(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var g=new this.Element;g.init(this,a);return g},draw:A,getRadialAttr:function(a,g){return{cx:a[0]-a[2]/2+g.cx*a[2],cy:a[1]-
a[2]/2+g.cy*a[2],r:g.r*a[2]}},getSpanWidth:function(a){return a.getBBox(!0).width},applyEllipsis:function(a,g,c,w){var h=a.rotation,y=c,b,e=0,d=c.length,k=function(a){g.removeChild(g.firstChild);a&&g.appendChild(m.createTextNode(a))},p;a.rotation=0;y=this.getSpanWidth(a,g);if(p=y>w){for(;e<=d;)b=Math.ceil((e+d)/2),y=c.substring(0,b)+"\u2026",k(y),y=this.getSpanWidth(a,g),e===d?e=d+1:y>w?d=b-1:e=b;0===d&&k("")}a.rotation=h;return p},escapes:{"\x26":"\x26amp;","\x3c":"\x26lt;","\x3e":"\x26gt;","'":"\x26#39;",
'"':"\x26quot;"},buildText:function(a){var c=a.element,w=this,h=w.forExport,b=G(a.textStr,"").toString(),y=-1!==b.indexOf("\x3c"),k=c.childNodes,p,x,A,F,z=f(c,"x"),L=a.styles,O=a.textWidth,l=L&&L.lineHeight,D=L&&L.textOutline,C=L&&"ellipsis"===L.textOverflow,v=L&&"nowrap"===L.whiteSpace,u=L&&L.fontSize,M,t,I=k.length,L=O&&!a.added&&this.box,R=function(a){var h;h=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:u||w.style.fontSize||12;return l?g(l):w.fontMetrics(h,a.getAttribute("style")?a:c).h},
q=function(a,g){J(w.escapes,function(c,w){g&&-1!==r(c,g)||(a=a.toString().replace(new RegExp(c,"g"),w))});return a};M=[b,C,v,l,D,u,O].join();if(M!==a.textCache){for(a.textCache=M;I--;)c.removeChild(k[I]);y||D||C||O||-1!==b.indexOf(" ")?(p=/<.*class="([^"]+)".*>/,x=/<.*style="([^"]+)".*>/,A=/<.*href="([^"]+)".*>/,L&&L.appendChild(c),b=y?b.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,
"\x3c/span\x3e").split(/<br.*?>/g):[b],b=e(b,function(a){return""!==a}),d(b,function(g,b){var e,y=0;g=g.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");e=g.split("|||");d(e,function(g){if(""!==g||1===e.length){var d={},k=m.createElementNS(w.SVG_NS,"tspan"),r,L;p.test(g)&&(r=g.match(p)[1],f(k,"class",r));x.test(g)&&(L=g.match(x)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),f(k,"style",L));A.test(g)&&!h&&(f(k,"onclick",'location.href\x3d"'+g.match(A)[1]+
'"'),f(k,"class","highcharts-anchor"),n(k,{cursor:"pointer"}));g=q(g.replace(/<[a-zA-Z\/](.|\n)*?>/g,"")||" ");if(" "!==g){k.appendChild(m.createTextNode(g));y?d.dx=0:b&&null!==z&&(d.x=z);f(k,d);c.appendChild(k);!y&&t&&(!P&&h&&n(k,{display:"block"}),f(k,"dy",R(k)));if(O){d=g.replace(/([^\^])-/g,"$1- ").split(" ");r=1<e.length||b||1<d.length&&!v;var l=[],D,J=R(k),G=a.rotation;for(C&&(F=w.applyEllipsis(a,k,g,O));!C&&r&&(d.length||l.length);)a.rotation=0,D=w.getSpanWidth(a,k),g=D>O,void 0===F&&(F=g),
g&&1!==d.length?(k.removeChild(k.firstChild),l.unshift(d.pop())):(d=l,l=[],d.length&&!v&&(k=m.createElementNS(N,"tspan"),f(k,{dy:J,x:z}),L&&f(k,"style",L),c.appendChild(k)),D>O&&(O=D)),d.length&&k.appendChild(m.createTextNode(d.join(" ").replace(/- /g,"-")));a.rotation=G}y++}}});t=t||c.childNodes.length}),F&&a.attr("title",q(a.textStr,["\x26lt;","\x26gt;"])),L&&L.removeChild(c),D&&a.applyTextOutline&&a.applyTextOutline(D)):c.appendChild(m.createTextNode(q(b)))}},getContrast:function(a){a=t(a).rgba;
return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,g,c,w,b,d,e,k,p){var y=this.label(a,g,c,p,null,null,null,null,"button"),x=0;y.attr(F({padding:8,r:2},b));var N,m,A,r;b=F({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",cursor:"pointer",fontWeight:"normal"}},b);N=b.style;delete b.style;d=F(b,{fill:"#e6e6e6"},d);m=d.style;delete d.style;e=F(b,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},e);A=e.style;delete e.style;k=F(b,{style:{color:"#cccccc"}},
k);r=k.style;delete k.style;E(y.element,M?"mouseover":"mouseenter",function(){3!==x&&y.setState(1)});E(y.element,M?"mouseout":"mouseleave",function(){3!==x&&y.setState(x)});y.setState=function(a){1!==a&&(y.state=x=a);y.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+["normal","hover","pressed","disabled"][a||0]);y.attr([b,d,e,k][a||0]).css([N,m,A,r][a||0])};y.attr(b).css(h({cursor:"default"},N));return y.on("click",function(a){3!==x&&w.call(y,a)})},crispLine:function(a,
g){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-g%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+g%2/2);return a},path:function(a){var g={fill:"none"};I(a)?g.d=a:D(a)&&h(g,a);return this.createElement("path").attr(g)},circle:function(a,g,c){a=D(a)?a:{x:a,y:g,r:c};g=this.createElement("circle");g.xSetter=g.ySetter=function(a,g,c){c.setAttribute("c"+g,a)};return g.attr(a)},arc:function(a,g,c,w,b,h){D(a)?(w=a,g=w.y,c=w.r,a=w.x):w={innerR:w,start:b,end:h};a=this.symbol("arc",a,g,c,c,w);a.r=c;return a},rect:function(a,
g,c,w,b,h){b=D(a)?a.r:b;var d=this.createElement("rect");a=D(a)?a:void 0===a?{}:{x:a,y:g,width:Math.max(c,0),height:Math.max(w,0)};void 0!==h&&(a.strokeWidth=h,a=d.crisp(a));a.fill="none";b&&(a.r=b);d.rSetter=function(a,g,c){f(c,{rx:a,ry:a})};return d.attr(a)},setSize:function(a,g,c){var w=this.alignedObjects,b=w.length;this.width=a;this.height=g;for(this.boxWrapper.animate({width:a,height:g},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:G(c,!0)?
void 0:0});b--;)w[b].align()},g:function(a){var g=this.createElement("g");return a?g.attr({"class":"highcharts-"+a}):g},image:function(a,g,c,w,b){var d={preserveAspectRatio:"none"};1<arguments.length&&h(d,{x:g,y:c,width:w,height:b});d=this.createElement("image").attr(d);d.element.setAttributeNS?d.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):d.element.setAttribute("hc-svg-href",a);return d},symbol:function(a,g,c,w,b,e){var k=this,p,y=/^url\((.*?)\)$/,x=y.test(a),N=!x&&(this.symbols[a]?
a:"circle"),A=N&&this.symbols[N],F=u(g)&&A&&A.call(this.symbols,Math.round(g),Math.round(c),w,b,e),r,L;A?(p=this.path(F),p.attr("fill","none"),h(p,{symbolName:N,x:g,y:c,width:w,height:b}),e&&h(p,e)):x&&(r=a.match(y)[1],p=this.image(r),p.imgwidth=G(O[r]&&O[r].width,e&&e.width),p.imgheight=G(O[r]&&O[r].height,e&&e.height),L=function(){p.attr({width:p.width,height:p.height})},d(["width","height"],function(a){p[a+"Setter"]=function(a,g){var c={},w=this["img"+g],b="width"===g?"translateX":"translateY";
this[g]=a;u(w)&&(this.element&&this.element.setAttribute(g,w),this.alignByTranslate||(c[b]=((this[g]||0)-w)/2,this.attr(c)))}}),u(g)&&p.attr({x:g,y:c}),p.isImg=!0,u(p.imgwidth)&&u(p.imgheight)?L():(p.attr({width:0,height:0}),v("img",{onload:function(){var a=l[k.chartIndex];0===this.width&&(n(this,{position:"absolute",top:"-999em"}),m.body.appendChild(this));O[r]={width:this.width,height:this.height};p.imgwidth=this.width;p.imgheight=this.height;p.element&&L();this.parentNode&&this.parentNode.removeChild(this);
k.imgCount--;if(!k.imgCount&&a&&a.onload)a.onload()},src:r}),this.imgCount++));return p},symbols:{circle:function(a,g,c,w){return this.arc(a+c/2,g+w/2,c/2,w/2,{start:0,end:2*Math.PI,open:!1})},square:function(a,g,c,w){return["M",a,g,"L",a+c,g,a+c,g+w,a,g+w,"Z"]},triangle:function(a,g,c,w){return["M",a+c/2,g,"L",a+c,g+w,a,g+w,"Z"]},"triangle-down":function(a,g,c,w){return["M",a,g,"L",a+c,g,a+c/2,g+w,"Z"]},diamond:function(a,g,c,w){return["M",a+c/2,g,"L",a+c,g+w/2,a+c/2,g+w,a,g+w/2,"Z"]},arc:function(a,
g,c,w,b){var h=b.start,d=b.r||c,e=b.r||w||c,k=b.end-.001;c=b.innerR;w=G(b.open,.001>Math.abs(b.end-b.start-2*Math.PI));var p=Math.cos(h),y=Math.sin(h),x=Math.cos(k),k=Math.sin(k);b=.001>b.end-h-Math.PI?0:1;d=["M",a+d*p,g+e*y,"A",d,e,0,b,1,a+d*x,g+e*k];u(c)&&d.push(w?"M":"L",a+c*x,g+c*k,"A",c,c,0,b,0,a+c*p,g+c*y);d.push(w?"":"Z");return d},callout:function(a,g,c,w,b){var h=Math.min(b&&b.r||0,c,w),d=h+6,e=b&&b.anchorX;b=b&&b.anchorY;var k;k=["M",a+h,g,"L",a+c-h,g,"C",a+c,g,a+c,g,a+c,g+h,"L",a+c,g+w-
h,"C",a+c,g+w,a+c,g+w,a+c-h,g+w,"L",a+h,g+w,"C",a,g+w,a,g+w,a,g+w-h,"L",a,g+h,"C",a,g,a,g,a+h,g];e&&e>c?b>g+d&&b<g+w-d?k.splice(13,3,"L",a+c,b-6,a+c+6,b,a+c,b+6,a+c,g+w-h):k.splice(13,3,"L",a+c,w/2,e,b,a+c,w/2,a+c,g+w-h):e&&0>e?b>g+d&&b<g+w-d?k.splice(33,3,"L",a,b+6,a-6,b,a,b-6,a,g+h):k.splice(33,3,"L",a,w/2,e,b,a,w/2,a,g+h):b&&b>w&&e>a+d&&e<a+c-d?k.splice(23,3,"L",e+6,g+w,e,g+w+6,e-6,g+w,a+h,g+w):b&&0>b&&e>a+d&&e<a+c-d&&k.splice(3,3,"L",e-6,g,e,g-6,e+6,g,c-h,g);return k}},clipRect:function(g,c,w,
b){var h=a.uniqueKey(),d=this.createElement("clipPath").attr({id:h}).add(this.defs);g=this.rect(g,c,w,b,0).add(d);g.id=h;g.clipPath=d;g.count=0;return g},text:function(a,g,c,w){var b={};if(w&&(this.allowHTML||!this.forExport))return this.html(a,g,c);b.x=Math.round(g||0);c&&(b.y=Math.round(c));if(a||0===a)b.text=a;a=this.createElement("text").attr(b);w||(a.xSetter=function(a,g,c){var w=c.getElementsByTagName("tspan"),b,h=c.getAttribute(g),d;for(d=0;d<w.length;d++)b=w[d],b.getAttribute(g)===h&&b.setAttribute(g,
a);c.setAttribute(g,a)});return a},fontMetrics:function(a,c){a=a||c&&c.style&&c.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?g(a):/em/.test(a)?parseFloat(a)*(c?this.fontMetrics(null,c.parentNode).f:16):12;c=24>a?a+3:Math.round(1.2*a);return{h:c,b:Math.round(.8*c),f:a}},rotCorr:function(a,g,w){var b=a;g&&w&&(b=Math.max(b*Math.cos(g*c),4));return{x:-a/3*Math.sin(g*c),y:b}},label:function(g,c,b,e,k,p,x,N,m){var A=this,r=A.g("button"!==m&&"label"),y=r.text=A.text("",0,0,x).attr({zIndex:1}),
L,P,z=0,O=3,l=0,n,D,f,J,C,G={},v,M,t=/^url\((.*?)\)$/.test(e),I=t,R,q,Q,T;m&&r.addClass("highcharts-"+m);I=t;R=function(){return(v||0)%2/2};q=function(){var a=y.element.style,g={};P=(void 0===n||void 0===D||C)&&u(y.textStr)&&y.getBBox();r.width=(n||P.width||0)+2*O+l;r.height=(D||P.height||0)+2*O;M=O+A.fontMetrics(a&&a.fontSize,y).b;I&&(L||(r.box=L=A.symbols[e]||t?A.symbol(e):A.rect(),L.addClass(("button"===m?"":"highcharts-label-box")+(m?" highcharts-"+m+"-box":"")),L.add(r),a=R(),g.x=a,g.y=(N?-M:
0)+a),g.width=Math.round(r.width),g.height=Math.round(r.height),L.attr(h(g,G)),G={})};Q=function(){var a=l+O,g;g=N?0:M;u(n)&&P&&("center"===C||"right"===C)&&(a+={center:.5,right:1}[C]*(n-P.width));if(a!==y.x||g!==y.y)y.attr("x",a),void 0!==g&&y.attr("y",g);y.x=a;y.y=g};T=function(a,g){L?L.attr(a,g):G[a]=g};r.onAdd=function(){y.add(r);r.attr({text:g||0===g?g:"",x:c,y:b});L&&u(k)&&r.attr({anchorX:k,anchorY:p})};r.widthSetter=function(g){n=a.isNumber(g)?g:null};r.heightSetter=function(a){D=a};r["text-alignSetter"]=
function(a){C=a};r.paddingSetter=function(a){u(a)&&a!==O&&(O=r.padding=a,Q())};r.paddingLeftSetter=function(a){u(a)&&a!==l&&(l=a,Q())};r.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==z&&(z=a,P&&r.attr({x:f}))};r.textSetter=function(a){void 0!==a&&y.textSetter(a);q();Q()};r["stroke-widthSetter"]=function(a,g){a&&(I=!0);v=this["stroke-width"]=a;T(g,a)};r.strokeSetter=r.fillSetter=r.rSetter=function(a,g){"r"!==g&&("fill"===g&&a&&(I=!0),r[g]=a);T(g,a)};r.anchorXSetter=function(a,g){k=r.anchorX=
a;T(g,Math.round(a)-R()-f)};r.anchorYSetter=function(a,g){p=r.anchorY=a;T(g,a-J)};r.xSetter=function(a){r.x=a;z&&(a-=z*((n||P.width)+2*O));f=Math.round(a);r.attr("translateX",f)};r.ySetter=function(a){J=r.y=Math.round(a);r.attr("translateY",J)};var U=r.css;return h(r,{css:function(a){if(a){var g={};a=F(a);d(r.textProps,function(c){void 0!==a[c]&&(g[c]=a[c],delete a[c])});y.css(g)}return U.call(r,a)},getBBox:function(){return{width:P.width+2*O,height:P.height+2*O,x:P.x-O,y:P.y-O}},shadow:function(a){a&&
(q(),L&&L.shadow(a));return r},destroy:function(){w(r.element,"mouseenter");w(r.element,"mouseleave");y&&(y=y.destroy());L&&(L=L.destroy());B.prototype.destroy.call(r);r=A=q=Q=T=null}})}});a.Renderer=H})(K);(function(a){var B=a.attr,H=a.createElement,E=a.css,q=a.defined,f=a.each,l=a.extend,t=a.isFirefox,n=a.isMS,v=a.isWebKit,u=a.pick,c=a.pInt,b=a.SVGRenderer,m=a.win,d=a.wrap;l(a.SVGElement.prototype,{htmlCss:function(a){var c=this.element;if(c=a&&"SPAN"===c.tagName&&a.width)delete a.width,this.textWidth=
c,this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=l(this.styles,a);E(this.element,a);return this},htmlGetBBox:function(){var a=this.element;return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=this.renderer,b=this.element,d=this.translateX||0,p=this.translateY||0,r=this.x||0,m=this.y||0,z=this.textAlign||"left",l={left:0,center:.5,right:1}[z],n=this.styles,
C=n&&n.whiteSpace;E(b,{marginLeft:d,marginTop:p});this.shadows&&f(this.shadows,function(a){E(a,{marginLeft:d+1,marginTop:p+1})});this.inverted&&f(b.childNodes,function(c){a.invertChild(c,b)});if("SPAN"===b.tagName){var n=this.rotation,x=this.textWidth&&c(this.textWidth),F=[n,z,b.innerHTML,this.textWidth,this.textAlign].join(),A;(A=x!==this.oldTextWidth)&&!(A=x>this.oldTextWidth)&&((A=this.textPxLength)||(E(b,{width:"",whiteSpace:C||"nowrap"}),A=b.offsetWidth),A=A>x);A&&/[ \-]/.test(b.textContent||
b.innerText)&&(E(b,{width:x+"px",display:"block",whiteSpace:C||"normal"}),this.oldTextWidth=x);F!==this.cTT&&(C=a.fontMetrics(b.style.fontSize).b,q(n)&&n!==(this.oldRotation||0)&&this.setSpanRotation(n,l,C),this.getSpanCorrection(this.textPxLength||b.offsetWidth,C,l,n,z));E(b,{left:r+(this.xCorr||0)+"px",top:m+(this.yCorr||0)+"px"});this.cTT=F;this.oldRotation=n}}else this.alignOnAdd=!0},setSpanRotation:function(a,c,b){var d={},e=this.renderer.getTransformKey();d[e]=d.transform="rotate("+a+"deg)";
d[e+(t?"Origin":"-origin")]=d.transformOrigin=100*c+"% "+b+"px";E(this.element,d)},getSpanCorrection:function(a,c,b){this.xCorr=-a*b;this.yCorr=-c}});l(b.prototype,{getTransformKey:function(){return n&&!/Edge/.test(m.navigator.userAgent)?"-ms-transform":v?"-webkit-transform":t?"MozTransform":m.opera?"-o-transform":""},html:function(a,c,b){var e=this.createElement("span"),h=e.element,k=e.renderer,m=k.isSVG,n=function(a,c){f(["opacity","visibility"],function(b){d(a,b+"Setter",function(a,b,e,d){a.call(this,
b,e,d);c[e]=b})})};e.textSetter=function(a){a!==h.innerHTML&&delete this.bBox;this.textStr=a;h.innerHTML=u(a,"");e.doTransform=!0};m&&n(e,e.element.style);e.xSetter=e.ySetter=e.alignSetter=e.rotationSetter=function(a,c){"align"===c&&(c="textAlign");e[c]=a;e.doTransform=!0};e.afterSetters=function(){this.doTransform&&(this.htmlUpdateTransform(),this.doTransform=!1)};e.attr({text:a,x:Math.round(c),y:Math.round(b)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});
h.style.whiteSpace="nowrap";e.css=e.htmlCss;m&&(e.add=function(a){var c,b=k.box.parentNode,d=[];if(this.parentGroup=a){if(c=a.div,!c){for(;a;)d.push(a),a=a.parentGroup;f(d.reverse(),function(a){function h(g,c){a[c]=g;"translateX"===c?k.left=g+"px":k.top=g+"px";a.doTransform=!0}var k,g=B(a.element,"class");g&&(g={className:g});c=a.div=a.div||H("div",g,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},
c||b);k=c.style;l(a,{classSetter:function(a){return function(g){this.element.setAttribute("class",g);a.className=g}}(c),on:function(){d[0].div&&e.on.apply({element:d[0].div},arguments);return a},translateXSetter:h,translateYSetter:h});n(a,k)})}}else c=b;c.appendChild(h);e.added=!0;e.alignOnAdd&&e.htmlUpdateTransform();return e});return e}})})(K);(function(a){var B=a.defined,H=a.each,E=a.extend,q=a.merge,f=a.pick,l=a.timeUnits,t=a.win;a.Time=function(a){this.update(a,!1)};a.Time.prototype={defaultOptions:{},
update:function(n){var l=f(n&&n.useUTC,!0),u=this;this.options=n=q(!0,this.options||{},n);this.Date=n.Date||t.Date;this.timezoneOffset=(this.useUTC=l)&&n.timezoneOffset;this.getTimezoneOffset=this.timezoneOffsetFunction();(this.variableTimezone=!(l&&!n.getTimezoneOffset&&!n.timezone))||this.timezoneOffset?(this.get=function(a,b){var c=b.getTime(),d=c-u.getTimezoneOffset(b);b.setTime(d);a=b["getUTC"+a]();b.setTime(c);return a},this.set=function(c,b,m){var d;if(-1!==a.inArray(c,["Milliseconds","Seconds",
"Minutes"]))b["set"+c](m);else d=u.getTimezoneOffset(b),d=b.getTime()-d,b.setTime(d),b["setUTC"+c](m),c=u.getTimezoneOffset(b),d=b.getTime()+c,b.setTime(d)}):l?(this.get=function(a,b){return b["getUTC"+a]()},this.set=function(a,b,m){return b["setUTC"+a](m)}):(this.get=function(a,b){return b["get"+a]()},this.set=function(a,b,m){return b["set"+a](m)})},makeTime:function(l,v,u,c,b,m){var d,h,k;this.useUTC?(d=this.Date.UTC.apply(0,arguments),h=this.getTimezoneOffset(d),d+=h,k=this.getTimezoneOffset(d),
h!==k?d+=k-h:h-36E5!==this.getTimezoneOffset(d-36E5)||a.isSafari||(d-=36E5)):d=(new this.Date(l,v,f(u,1),f(c,0),f(b,0),f(m,0))).getTime();return d},timezoneOffsetFunction:function(){var l=this,f=this.options,u=t.moment;if(!this.useUTC)return function(a){return 6E4*(new Date(a)).getTimezoneOffset()};if(f.timezone){if(u)return function(a){return 6E4*-u.tz(a,f.timezone).utcOffset()};a.error(25)}return this.useUTC&&f.getTimezoneOffset?function(a){return 6E4*f.getTimezoneOffset(a)}:function(){return 6E4*
(l.timezoneOffset||0)}},dateFormat:function(l,f,u){if(!a.defined(f)||isNaN(f))return a.defaultOptions.lang.invalidDate||"";l=a.pick(l,"%Y-%m-%d %H:%M:%S");var c=this,b=new this.Date(f),m=this.get("Hours",b),d=this.get("Day",b),h=this.get("Date",b),k=this.get("Month",b),e=this.get("FullYear",b),p=a.defaultOptions.lang,r=p.weekdays,n=p.shortWeekdays,z=a.pad,b=a.extend({a:n?n[d]:r[d].substr(0,3),A:r[d],d:z(h),e:z(h,2," "),w:d,b:p.shortMonths[k],B:p.months[k],m:z(k+1),y:e.toString().substr(2,2),Y:e,H:z(m),
k:m,I:z(m%12||12),l:m%12||12,M:z(c.get("Minutes",b)),p:12>m?"AM":"PM",P:12>m?"am":"pm",S:z(b.getSeconds()),L:z(Math.round(f%1E3),3)},a.dateFormats);a.objectEach(b,function(a,b){for(;-1!==l.indexOf("%"+b);)l=l.replace("%"+b,"function"===typeof a?a.call(c,f):a)});return u?l.substr(0,1).toUpperCase()+l.substr(1):l},getTimeTicks:function(a,v,u,c){var b=this,m=[],d={},h,k=new b.Date(v),e=a.unitRange,p=a.count||1,r;if(B(v)){b.set("Milliseconds",k,e>=l.second?0:p*Math.floor(b.get("Milliseconds",k)/p));e>=
l.second&&b.set("Seconds",k,e>=l.minute?0:p*Math.floor(b.get("Seconds",k)/p));e>=l.minute&&b.set("Minutes",k,e>=l.hour?0:p*Math.floor(b.get("Minutes",k)/p));e>=l.hour&&b.set("Hours",k,e>=l.day?0:p*Math.floor(b.get("Hours",k)/p));e>=l.day&&b.set("Date",k,e>=l.month?1:p*Math.floor(b.get("Date",k)/p));e>=l.month&&(b.set("Month",k,e>=l.year?0:p*Math.floor(b.get("Month",k)/p)),h=b.get("FullYear",k));e>=l.year&&b.set("FullYear",k,h-h%p);e===l.week&&b.set("Date",k,b.get("Date",k)-b.get("Day",k)+f(c,1));
h=b.get("FullYear",k);c=b.get("Month",k);var n=b.get("Date",k),z=b.get("Hours",k);v=k.getTime();b.variableTimezone&&(r=u-v>4*l.month||b.getTimezoneOffset(v)!==b.getTimezoneOffset(u));k=k.getTime();for(v=1;k<u;)m.push(k),k=e===l.year?b.makeTime(h+v*p,0):e===l.month?b.makeTime(h,c+v*p):!r||e!==l.day&&e!==l.week?r&&e===l.hour&&1<p?b.makeTime(h,c,n,z+v*p):k+e*p:b.makeTime(h,c,n+v*p*(e===l.day?1:7)),v++;m.push(k);e<=l.hour&&1E4>m.length&&H(m,function(a){0===a%18E5&&"000000000"===b.dateFormat("%H%M%S%L",
a)&&(d[a]="day")})}m.info=E(a,{higherRanks:d,totalRange:e*p});return m}}})(K);(function(a){var B=a.color,H=a.merge;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{},time:a.Time.prototype.defaultOptions,chart:{borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:6},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},
title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},
shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",
day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:B("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",
whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(B){a.defaultOptions=H(!0,a.defaultOptions,B);a.time.update(H(a.defaultOptions.global,a.defaultOptions.time),!1);return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;a.time=new a.Time(H(a.defaultOptions.global,
a.defaultOptions.time));a.dateFormat=function(B,q,f){return a.time.dateFormat(B,q,f)}})(K);(function(a){var B=a.correctFloat,H=a.defined,E=a.destroyObjectProperties,q=a.isNumber,f=a.merge,l=a.pick,t=a.deg2rad;a.Tick=function(a,l,f,c){this.axis=a;this.pos=l;this.type=f||"";this.isNewLabel=this.isNew=!0;f||c||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,v=a.options,u=a.chart,c=a.categories,b=a.names,m=this.pos,d=v.labels,h=a.tickPositions,k=m===h[0],e=m===h[h.length-1],b=c?
l(c[m],b[m],m):m,c=this.label,h=h.info,p;a.isDatetimeAxis&&h&&(p=v.dateTimeLabelFormats[h.higherRanks[m]||h.unitName]);this.isFirst=k;this.isLast=e;v=a.labelFormatter.call({axis:a,chart:u,isFirst:k,isLast:e,dateTimeLabelFormat:p,value:a.isLog?B(a.lin2log(b)):b,pos:m});if(H(c))c&&c.attr({text:v});else{if(this.label=c=H(v)&&d.enabled?u.renderer.text(v,0,0,d.useHTML).css(f(d.style)).add(a.labelGroup):null)c.textPxLength=c.getBBox().width;this.rotation=0}},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?
"height":"width"]:0},handleOverflow:function(a){var f=this.axis,n=f.options.labels,c=a.x,b=f.chart.chartWidth,m=f.chart.spacing,d=l(f.labelLeft,Math.min(f.pos,m[3])),m=l(f.labelRight,Math.max(f.isRadial?0:f.pos+f.len,b-m[1])),h=this.label,k=this.rotation,e={left:0,center:.5,right:1}[f.labelAlign||h.attr("align")],p=h.getBBox().width,r=f.getSlotWidth(),I=r,z=1,M,D={};if(k||!1===n.overflow)0>k&&c-e*p<d?M=Math.round(c/Math.cos(k*t)-d):0<k&&c+e*p>m&&(M=Math.round((b-c)/Math.cos(k*t)));else if(b=c+(1-
e)*p,c-e*p<d?I=a.x+I*(1-e)-d:b>m&&(I=m-a.x+I*e,z=-1),I=Math.min(r,I),I<r&&"center"===f.labelAlign&&(a.x+=z*(r-I-e*(r-Math.min(p,I)))),p>I||f.autoRotation&&(h.styles||{}).width)M=I;M&&(D.width=M,(n.style||{}).textOverflow||(D.textOverflow="ellipsis"),h.css(D))},getPosition:function(l,f,u,c){var b=this.axis,m=b.chart,d=c&&m.oldChartHeight||m.chartHeight;return{x:l?a.correctFloat(b.translate(f+u,null,null,c)+b.transB):b.left+b.offset+(b.opposite?(c&&m.oldChartWidth||m.chartWidth)-b.right-b.left:0),y:l?
d-b.bottom+b.offset-(b.opposite?b.height:0):a.correctFloat(d-b.translate(f+u,null,null,c)-b.transB)}},getLabelPosition:function(a,l,f,c,b,m,d,h){var k=this.axis,e=k.transA,p=k.reversed,r=k.staggerLines,n=k.tickRotCorr||{x:0,y:0},z=b.y,u=c||k.reserveSpaceDefault?0:-k.labelOffset*("center"===k.labelAlign?.5:1);H(z)||(z=0===k.side?f.rotation?-8:-f.getBBox().height:2===k.side?n.y+8:Math.cos(f.rotation*t)*(n.y-f.getBBox(!1,0).height/2));a=a+b.x+u+n.x-(m&&c?m*e*(p?-1:1):0);l=l+z-(m&&!c?m*e*(p?1:-1):0);
r&&(f=d/(h||1)%r,k.opposite&&(f=r-f-1),l+=k.labelOffset/r*f);return{x:a,y:Math.round(l)}},getMarkPath:function(a,l,f,c,b,m){return m.crispLine(["M",a,l,"L",a+(b?0:-f),l+(b?f:0)],c)},renderGridLine:function(a,l,f){var c=this.axis,b=c.options,m=this.gridLine,d={},h=this.pos,k=this.type,e=c.tickmarkOffset,p=c.chart.renderer,r=k?k+"Grid":"grid",n=b[r+"LineWidth"],z=b[r+"LineColor"],b=b[r+"LineDashStyle"];m||(d.stroke=z,d["stroke-width"]=n,b&&(d.dashstyle=b),k||(d.zIndex=1),a&&(d.opacity=0),this.gridLine=
m=p.path().attr(d).addClass("highcharts-"+(k?k+"-":"")+"grid-line").add(c.gridGroup));if(!a&&m&&(a=c.getPlotLinePath(h+e,m.strokeWidth()*f,a,!0)))m[this.isNew?"attr":"animate"]({d:a,opacity:l})},renderMark:function(a,f,u){var c=this.axis,b=c.options,m=c.chart.renderer,d=this.type,h=d?d+"Tick":"tick",k=c.tickSize(h),e=this.mark,p=!e,r=a.x;a=a.y;var n=l(b[h+"Width"],!d&&c.isXAxis?1:0),b=b[h+"Color"];k&&(c.opposite&&(k[0]=-k[0]),p&&(this.mark=e=m.path().addClass("highcharts-"+(d?d+"-":"")+"tick").add(c.axisGroup),
e.attr({stroke:b,"stroke-width":n})),e[p?"attr":"animate"]({d:this.getMarkPath(r,a,k[0],e.strokeWidth()*u,c.horiz,m),opacity:f}))},renderLabel:function(a,f,u,c){var b=this.axis,m=b.horiz,d=b.options,h=this.label,k=d.labels,e=k.step,b=b.tickmarkOffset,p=!0,r=a.x;a=a.y;h&&q(r)&&(h.xy=a=this.getLabelPosition(r,a,h,m,k,b,c,e),this.isFirst&&!this.isLast&&!l(d.showFirstLabel,1)||this.isLast&&!this.isFirst&&!l(d.showLastLabel,1)?p=!1:!m||k.step||k.rotation||f||0===u||this.handleOverflow(a),e&&c%e&&(p=!1),
p&&q(a.y)?(a.opacity=u,h[this.isNewLabel?"attr":"animate"](a),this.isNewLabel=!1):(h.attr("y",-9999),this.isNewLabel=!0))},render:function(f,t,u){var c=this.axis,b=c.horiz,m=this.getPosition(b,this.pos,c.tickmarkOffset,t),d=m.x,h=m.y,c=b&&d===c.pos+c.len||!b&&h===c.pos?-1:1;u=l(u,1);this.isActive=!0;this.renderGridLine(t,u,c);this.renderMark(m,u,c);this.renderLabel(m,t,u,f);this.isNew=!1;a.fireEvent(this,"afterRender")},destroy:function(){E(this,this.axis)}}})(K);var V=function(a){var B=a.addEvent,
H=a.animObject,E=a.arrayMax,q=a.arrayMin,f=a.color,l=a.correctFloat,t=a.defaultOptions,n=a.defined,v=a.deg2rad,u=a.destroyObjectProperties,c=a.each,b=a.extend,m=a.fireEvent,d=a.format,h=a.getMagnitude,k=a.grep,e=a.inArray,p=a.isArray,r=a.isNumber,I=a.isString,z=a.merge,M=a.normalizeTickInterval,D=a.objectEach,C=a.pick,x=a.removeEvent,F=a.splat,A=a.syncTimeout,J=a.Tick,G=function(){this.init.apply(this,arguments)};a.extend(G.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",
second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},x:0},maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",minPadding:.01,startOfWeek:1,startOnTick:!1,tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",
lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,formatter:function(){return a.numberFormat(this.total,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},
defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},init:function(a,c){var g=c.isX,b=this;b.chart=a;b.horiz=a.inverted&&!b.isZAxis?!g:g;b.isXAxis=g;b.coll=b.coll||(g?"xAxis":"yAxis");b.opposite=c.opposite;b.side=c.side||(b.horiz?b.opposite?0:2:b.opposite?1:3);b.setOptions(c);var w=this.options,d=w.type;b.labelFormatter=w.labels.formatter||
b.defaultLabelFormatter;b.userOptions=c;b.minPixelPadding=0;b.reversed=w.reversed;b.visible=!1!==w.visible;b.zoomEnabled=!1!==w.zoomEnabled;b.hasNames="category"===d||!0===w.categories;b.categories=w.categories||b.hasNames;b.names||(b.names=[],b.names.keys={});b.plotLinesAndBandsGroups={};b.isLog="logarithmic"===d;b.isDatetimeAxis="datetime"===d;b.positiveValuesOnly=b.isLog&&!b.allowNegativeLog;b.isLinked=n(w.linkedTo);b.ticks={};b.labelEdge=[];b.minorTicks={};b.plotLinesAndBands=[];b.alternateBands=
{};b.len=0;b.minRange=b.userMinRange=w.minRange||w.maxZoom;b.range=w.range;b.offset=w.offset||0;b.stacks={};b.oldStacks={};b.stacksTouched=0;b.max=null;b.min=null;b.crosshair=C(w.crosshair,F(a.options.tooltip.crosshairs)[g?0:1],!1);c=b.options.events;-1===e(b,a.axes)&&(g?a.axes.splice(a.xAxis.length,0,b):a.axes.push(b),a[b.coll].push(b));b.series=b.series||[];a.inverted&&!b.isZAxis&&g&&void 0===b.reversed&&(b.reversed=!0);D(c,function(a,g){B(b,g,a)});b.lin2log=w.linearToLogConverter||b.lin2log;b.isLog&&
(b.val2lin=b.log2lin,b.lin2val=b.lin2log)},setOptions:function(a){this.options=z(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],z(t[this.coll],a))},defaultLabelFormatter:function(){var g=this.axis,c=this.value,b=g.chart.time,e=g.categories,h=this.dateTimeLabelFormat,k=t.lang,p=k.numericSymbols,k=k.numericSymbolMagnitude||1E3,x=p&&p.length,r,m=g.options.labels.format,
g=g.isLog?Math.abs(c):g.tickInterval;if(m)r=d(m,this,b);else if(e)r=c;else if(h)r=b.dateFormat(h,c);else if(x&&1E3<=g)for(;x--&&void 0===r;)b=Math.pow(k,x+1),g>=b&&0===10*c%b&&null!==p[x]&&0!==c&&(r=a.numberFormat(c/b,-1)+p[x]);void 0===r&&(r=1E4<=Math.abs(c)?a.numberFormat(c,-1):a.numberFormat(c,-1,void 0,""));return r},getSeriesExtremes:function(){var a=this,b=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();c(a.series,
function(g){if(g.visible||!b.options.chart.ignoreHiddenSeries){var c=g.options,w=c.threshold,d;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=w&&(w=null);if(a.isXAxis)c=g.xData,c.length&&(g=q(c),d=E(c),r(g)||g instanceof Date||(c=k(c,r),g=q(c),d=E(c)),c.length&&(a.dataMin=Math.min(C(a.dataMin,c[0],g),g),a.dataMax=Math.max(C(a.dataMax,c[0],d),d)));else if(g.getExtremes(),d=g.dataMax,g=g.dataMin,n(g)&&n(d)&&(a.dataMin=Math.min(C(a.dataMin,g),g),a.dataMax=Math.max(C(a.dataMax,d),d)),n(w)&&(a.threshold=
w),!c.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})},translate:function(a,c,b,d,e,h){var g=this.linkedParent||this,w=1,k=0,p=d?g.oldTransA:g.transA;d=d?g.oldMin:g.min;var x=g.minPixelPadding;e=(g.isOrdinal||g.isBroken||g.isLog&&e)&&g.lin2val;p||(p=g.transA);b&&(w*=-1,k=g.len);g.reversed&&(w*=-1,k-=w*(g.sector||g.len));c?(a=(a*w+k-x)/p+d,e&&(a=g.lin2val(a))):(e&&(a=g.val2lin(a)),a=r(d)?w*(a-d)*p+k+w*x+(r(h)?p*h:0):void 0);return a},toPixels:function(a,c){return this.translate(a,!1,!this.horiz,
null,!0)+(c?0:this.pos)},toValue:function(a,c){return this.translate(a-(c?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,c,b,d,e){var g=this.chart,w=this.left,h=this.top,k,p,x=b&&g.oldChartHeight||g.chartHeight,m=b&&g.oldChartWidth||g.chartWidth,N;k=this.transB;var A=function(a,g,c){if(a<g||a>c)d?a=Math.min(Math.max(g,a),c):N=!0;return a};e=C(e,this.translate(a,null,null,b));e=Math.min(Math.max(-1E5,e),1E5);a=b=Math.round(e+k);k=p=Math.round(x-e-k);r(e)?this.horiz?(k=h,p=x-this.bottom,
a=b=A(a,w,w+this.width)):(a=w,b=m-this.right,k=p=A(k,h,h+this.height)):(N=!0,d=!1);return N&&!d?null:g.renderer.crispLine(["M",a,k,"L",b,p],c||1)},getLinearTickPositions:function(a,c,b){var g,w=l(Math.floor(c/a)*a);b=l(Math.ceil(b/a)*a);var d=[],e;l(w+a)===w&&(e=20);if(this.single)return[c];for(c=w;c<=b;){d.push(c);c=l(c+a,e);if(c===g)break;g=c}return d},getMinorTickInterval:function(){var a=this.options;return!0===a.minorTicks?C(a.minorTickInterval,"auto"):!1===a.minorTicks?null:a.minorTickInterval},
getMinorTickPositions:function(){var a=this,b=a.options,d=a.tickPositions,e=a.minorTickInterval,h=[],k=a.pointRangePadding||0,p=a.min-k,k=a.max+k,x=k-p;if(x&&x/e<a.len/3)if(a.isLog)c(this.paddedTicks,function(g,c,b){c&&h.push.apply(h,a.getLogTickPositions(e,b[c-1],b[c],!0))});else if(a.isDatetimeAxis&&"auto"===this.getMinorTickInterval())h=h.concat(a.getTimeTicks(a.normalizeTimeTickInterval(e),p,k,b.startOfWeek));else for(b=p+(d[0]-p)%e;b<=k&&b!==h[0];b+=e)h.push(b);0!==h.length&&a.trimTicks(h);return h},
adjustForMinRange:function(){var a=this.options,b=this.min,d=this.max,e,h,k,p,x,r,m,A;this.isXAxis&&void 0===this.minRange&&!this.isLog&&(n(a.min)||n(a.max)?this.minRange=null:(c(this.series,function(a){r=a.xData;for(p=m=a.xIncrement?1:r.length-1;0<p;p--)if(x=r[p]-r[p-1],void 0===k||x<k)k=x}),this.minRange=Math.min(5*k,this.dataMax-this.dataMin)));d-b<this.minRange&&(h=this.dataMax-this.dataMin>=this.minRange,A=this.minRange,e=(A-d+b)/2,e=[b-e,C(a.min,b-e)],h&&(e[2]=this.isLog?this.log2lin(this.dataMin):
this.dataMin),b=E(e),d=[b+A,C(a.max,b+A)],h&&(d[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),d=q(d),d-b<A&&(e[0]=d-A,e[1]=C(a.min,d-A),b=E(e)));this.min=b;this.max=d},getClosest:function(){var a;this.categories?a=1:c(this.series,function(g){var c=g.closestPointRange,b=g.visible||!g.chart.options.chart.ignoreHiddenSeries;!g.noSharedTooltip&&n(c)&&b&&(a=n(a)?Math.min(a,c):c)});return a},nameToX:function(a){var g=p(this.categories),c=g?this.categories:this.names,b=a.options.x,d;a.series.requireSorting=
!1;n(b)||(b=!1===this.options.uniqueNames?a.series.autoIncrement():g?e(a.name,c):C(c.keys[a.name],-1));-1===b?g||(d=c.length):d=b;void 0!==d&&(this.names[d]=a.name,this.names.keys[a.name]=d);return d},updateNames:function(){var g=this,b=this.names;0<b.length&&(c(a.keys(b.keys),function(a){delete b.keys[a]}),b.length=0,this.minRange=this.userMinRange,c(this.series||[],function(a){a.xIncrement=null;if(!a.points||a.isDirtyData)a.processData(),a.generatePoints();c(a.points,function(c,b){var d;c.options&&
(d=g.nameToX(c),void 0!==d&&d!==c.x&&(c.x=d,a.xData[b]=d))})}))},setAxisTranslation:function(a){var g=this,b=g.max-g.min,d=g.axisPointRange||0,e,h=0,k=0,p=g.linkedParent,x=!!g.categories,r=g.transA,m=g.isXAxis;if(m||x||d)e=g.getClosest(),p?(h=p.minPointOffset,k=p.pointRangePadding):c(g.series,function(a){var c=x?1:m?C(a.options.pointRange,e,0):g.axisPointRange||0;a=a.options.pointPlacement;d=Math.max(d,c);g.single||(h=Math.max(h,I(a)?0:c/2),k=Math.max(k,"on"===a?0:c))}),p=g.ordinalSlope&&e?g.ordinalSlope/
e:1,g.minPointOffset=h*=p,g.pointRangePadding=k*=p,g.pointRange=Math.min(d,b),m&&(g.closestPointRange=e);a&&(g.oldTransA=r);g.translationSlope=g.transA=r=g.options.staticScale||g.len/(b+k||1);g.transB=g.horiz?g.left:g.bottom;g.minPixelPadding=r*h},minFromRange:function(){return this.max-this.range},setTickInterval:function(g){var b=this,d=b.chart,e=b.options,k=b.isLog,p=b.log2lin,x=b.isDatetimeAxis,A=b.isXAxis,F=b.isLinked,f=e.maxPadding,z=e.minPadding,D=e.tickInterval,J=e.tickPixelInterval,G=b.categories,
u=b.threshold,t=b.softThreshold,v,q,I,B;x||G||F||this.getTickAmount();I=C(b.userMin,e.min);B=C(b.userMax,e.max);F?(b.linkedParent=d[b.coll][e.linkedTo],d=b.linkedParent.getExtremes(),b.min=C(d.min,d.dataMin),b.max=C(d.max,d.dataMax),e.type!==b.linkedParent.options.type&&a.error(11,1)):(!t&&n(u)&&(b.dataMin>=u?(v=u,z=0):b.dataMax<=u&&(q=u,f=0)),b.min=C(I,v,b.dataMin),b.max=C(B,q,b.dataMax));k&&(b.positiveValuesOnly&&!g&&0>=Math.min(b.min,C(b.dataMin,b.min))&&a.error(10,1),b.min=l(p(b.min),15),b.max=
l(p(b.max),15));b.range&&n(b.max)&&(b.userMin=b.min=I=Math.max(b.dataMin,b.minFromRange()),b.userMax=B=b.max,b.range=null);m(b,"foundExtremes");b.beforePadding&&b.beforePadding();b.adjustForMinRange();!(G||b.axisPointRange||b.usePercentage||F)&&n(b.min)&&n(b.max)&&(p=b.max-b.min)&&(!n(I)&&z&&(b.min-=p*z),!n(B)&&f&&(b.max+=p*f));r(e.softMin)&&!r(b.userMin)&&(b.min=Math.min(b.min,e.softMin));r(e.softMax)&&!r(b.userMax)&&(b.max=Math.max(b.max,e.softMax));r(e.floor)&&(b.min=Math.max(b.min,e.floor));r(e.ceiling)&&
(b.max=Math.min(b.max,e.ceiling));t&&n(b.dataMin)&&(u=u||0,!n(I)&&b.min<u&&b.dataMin>=u?b.min=u:!n(B)&&b.max>u&&b.dataMax<=u&&(b.max=u));b.tickInterval=b.min===b.max||void 0===b.min||void 0===b.max?1:F&&!D&&J===b.linkedParent.options.tickPixelInterval?D=b.linkedParent.tickInterval:C(D,this.tickAmount?(b.max-b.min)/Math.max(this.tickAmount-1,1):void 0,G?1:(b.max-b.min)*J/Math.max(b.len,J));A&&!g&&c(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&
b.beforeSetTickPositions();b.postProcessTickInterval&&(b.tickInterval=b.postProcessTickInterval(b.tickInterval));b.pointRange&&!D&&(b.tickInterval=Math.max(b.pointRange,b.tickInterval));g=C(e.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);!D&&b.tickInterval<g&&(b.tickInterval=g);x||k||D||(b.tickInterval=M(b.tickInterval,null,h(b.tickInterval),C(e.allowDecimals,!(.5<b.tickInterval&&5>b.tickInterval&&1E3<b.max&&9999>b.max)),!!this.tickAmount));this.tickAmount||(b.tickInterval=b.unsquish());
this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions;b=this.getMinorTickInterval();var d=a.tickPositioner,e=a.startOnTick,h=a.endOnTick;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===b&&this.tickInterval?this.tickInterval/5:b;this.single=this.min===this.max&&n(this.min)&&!this.tickAmount&&(parseInt(this.min,10)===this.min||!1!==a.allowDecimals);this.tickPositions=b=c&&c.slice();
!b&&(b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),b.length>this.len&&(b=[b[0],b.pop()],b[0]===b[1]&&(b.length=1)),this.tickPositions=b,d&&(d=d.apply(this,[this.min,this.max])))&&(this.tickPositions=b=d);this.paddedTicks=b.slice(0);
this.trimTicks(b,e,h);this.isLinked||(this.single&&2>b.length&&(this.min-=.5,this.max+=.5),c||d||this.adjustTickAmount())},trimTicks:function(a,b,c){var g=a[0],d=a[a.length-1],e=this.minPointOffset||0;if(!this.isLinked){if(b&&-Infinity!==g)this.min=g;else for(;this.min-e>a[0];)a.shift();if(c)this.max=d;else for(;this.max+e<a[a.length-1];)a.pop();0===a.length&&n(g)&&!this.options.tickPositions&&a.push((d+g)/2)}},alignToOthers:function(){var a={},b,d=this.options;!1===this.chart.options.chart.alignTicks||
!1===d.alignTicks||this.isLog||c(this.chart[this.coll],function(g){var c=g.options,c=[g.horiz?c.left:c.top,c.width,c.height,c.pane].join();g.series.length&&(a[c]?b=!0:a[c]=1)});return b},getTickAmount:function(){var a=this.options,b=a.tickAmount,c=a.tickPixelInterval;!n(a.tickInterval)&&this.len<c&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(b=2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/c)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=
this.tickInterval,b=this.tickPositions,c=this.tickAmount,d=this.finalTickAmt,e=b&&b.length,h=C(this.threshold,this.softThreshold?0:null);if(this.hasData()){if(e<c){for(;b.length<c;)b.length%2||this.min===h?b.push(l(b[b.length-1]+a)):b.unshift(l(b[0]-a));this.transA*=(e-1)/(c-1);this.min=b[0];this.max=b[b.length-1]}else e>c&&(this.tickInterval*=2,this.setTickPositions());if(n(d)){for(a=c=b.length;a--;)(3===d&&1===a%2||2>=d&&0<a&&a<c-1)&&b.splice(a,1);this.finalTickAmt=void 0}}},setScale:function(){var a,
b;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();b=this.len!==this.oldAxisLength;c(this.series,function(b){if(b.isDirtyData||b.isDirty||b.xAxis.isDirty)a=!0});b||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||
(this.isDirty=b||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks();m(this,"afterSetScale")},setExtremes:function(a,d,e,h,k){var g=this,p=g.chart;e=C(e,!0);c(g.series,function(a){delete a.kdTree});k=b(k,{min:a,max:d});m(g,"setExtremes",k,function(){g.userMin=a;g.userMax=d;g.eventArgs=k;e&&p.redraw(h)})},zoom:function(a,b){var g=this.dataMin,c=this.dataMax,d=this.options,e=Math.min(g,C(d.min,g)),d=Math.max(c,C(d.max,c));if(a!==this.min||b!==this.max)this.allowZoomOutside||
(n(g)&&(a<e&&(a=e),a>d&&(a=d)),n(c)&&(b<e&&(b=e),b>d&&(b=d))),this.displayBtn=void 0!==a||void 0!==b,this.setExtremes(a,b,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var b=this.chart,c=this.options,d=c.offsets||[0,0,0,0],e=this.horiz,h=this.width=Math.round(a.relativeLength(C(c.width,b.plotWidth-d[3]+d[1]),b.plotWidth)),k=this.height=Math.round(a.relativeLength(C(c.height,b.plotHeight-d[0]+d[2]),b.plotHeight)),p=this.top=Math.round(a.relativeLength(C(c.top,b.plotTop+d[0]),b.plotHeight,
b.plotTop)),c=this.left=Math.round(a.relativeLength(C(c.left,b.plotLeft+d[3]),b.plotWidth,b.plotLeft));this.bottom=b.chartHeight-k-p;this.right=b.chartWidth-h-c;this.len=Math.max(e?h:k,0);this.pos=e?c:p},getExtremes:function(){var a=this.isLog,b=this.lin2log;return{min:a?l(b(this.min)):this.min,max:a?l(b(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,c=this.lin2log,g=b?c(this.min):this.min,b=b?c(this.max):
this.max;null===a?a=g:g>a?a=g:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(C(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var b=this.options,c=b[a+"Length"],g=C(b[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(g&&c)return"inside"===b[a+"Position"]&&(c=-c),[c,g]},labelMetrics:function(){var a=this.tickPositions&&this.tickPositions[0]||0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,
this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,b=this.horiz,d=this.tickInterval,e=d,h=this.len/(((this.categories?1:0)+this.max-this.min)/d),k,p=a.rotation,x=this.labelMetrics(),r,m=Number.MAX_VALUE,A,F=function(a){a/=h||1;a=1<a?Math.ceil(a):1;return a*d};b?(A=!a.staggerLines&&!a.step&&(n(p)?[p]:h<C(a.autoRotationLimit,80)&&a.autoRotation))&&c(A,function(a){var b;if(a===p||a&&-90<=a&&90>=a)r=F(Math.abs(x.h/Math.sin(v*a))),b=r+Math.abs(a/360),b<m&&(m=b,k=a,e=r)}):
a.step||(e=F(x.h));this.autoRotation=A;this.labelRotation=C(k,p);return e},getSlotWidth:function(){var a=this.chart,b=this.horiz,c=this.options.labels,d=Math.max(this.tickPositions.length-(this.categories?0:1),1),e=a.margin[3];return b&&2>(c.step||0)&&!c.rotation&&(this.staggerLines||1)*this.len/d||!b&&(c.style&&parseInt(c.style.width,10)||e&&e-a.spacing[3]||.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,b=a.renderer,d=this.tickPositions,e=this.ticks,h=this.options.labels,k=this.horiz,
p=this.getSlotWidth(),x=Math.max(1,Math.round(p-2*(h.padding||5))),r={},m=this.labelMetrics(),A=h.style&&h.style.textOverflow,F,l,f=0,z;I(h.rotation)||(r.rotation=h.rotation||0);c(d,function(a){(a=e[a])&&a.label&&a.label.textPxLength>f&&(f=a.label.textPxLength)});this.maxLabelLength=f;if(this.autoRotation)f>x&&f>m.h?r.rotation=this.labelRotation:this.labelRotation=0;else if(p&&(F=x,!A))for(l="clip",x=d.length;!k&&x--;)if(z=d[x],z=e[z].label)z.styles&&"ellipsis"===z.styles.textOverflow?z.css({textOverflow:"clip"}):
z.textPxLength>p&&z.css({width:p+"px"}),z.getBBox().height>this.len/d.length-(m.h-m.f)&&(z.specificTextOverflow="ellipsis");r.rotation&&(F=f>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight,A||(l="ellipsis"));if(this.labelAlign=h.align||this.autoLabelAlign(this.labelRotation))r.align=this.labelAlign;c(d,function(a){var b=(a=e[a])&&a.label;b&&(b.attr(r),!F||h.style&&h.style.width||!(F<b.textPxLength||"SPAN"===b.element.tagName)||b.css({width:F,textOverflow:b.specificTextOverflow||l}),delete b.specificTextOverflow,
a.rotation=r.rotation)});this.tickRotCorr=b.rotCorr(m.b,this.labelRotation||0,0!==this.side)},hasData:function(){return this.hasVisibleSeries||n(this.min)&&n(this.max)&&this.tickPositions&&0<this.tickPositions.length},addTitle:function(a){var b=this.chart.renderer,c=this.horiz,g=this.opposite,d=this.options.title,e;this.axisTitle||((e=d.textAlign)||(e=(c?{low:"left",middle:"center",high:"right"}:{low:g?"right":"left",middle:"center",high:g?"left":"right"})[d.align]),this.axisTitle=b.text(d.text,0,
0,d.useHTML).attr({zIndex:7,rotation:d.rotation||0,align:e}).addClass("highcharts-axis-title").css(z(d.style)).add(this.axisGroup),this.axisTitle.isNew=!0);d.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](!0)},generateTick:function(a){var b=this.ticks;b[a]?b[a].addLabel():b[a]=new J(this,a)},getOffset:function(){var a=this,b=a.chart,d=b.renderer,e=a.options,h=a.tickPositions,k=a.ticks,p=a.horiz,x=a.side,r=b.inverted&&!a.isZAxis?[1,0,3,2][x]:x,m,A,
F=0,l,f=0,z=e.title,J=e.labels,G=0,u=b.axisOffset,b=b.clipOffset,t=[-1,1,1,-1][x],v=e.className,M=a.axisParent,q=this.tickSize("tick");m=a.hasData();a.showAxis=A=m||C(e.showEmpty,!0);a.staggerLines=a.horiz&&J.staggerLines;a.axisGroup||(a.gridGroup=d.g("grid").attr({zIndex:e.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(v||"")).add(M),a.axisGroup=d.g("axis").attr({zIndex:e.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+" "+(v||"")).add(M),a.labelGroup=d.g("axis-labels").attr({zIndex:J.zIndex||
7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(v||"")).add(M));m||a.isLinked?(c(h,function(b,c){a.generateTick(b,c)}),a.renderUnsquish(),a.reserveSpaceDefault=0===x||2===x||{1:"left",3:"right"}[x]===a.labelAlign,C(J.reserveSpace,"center"===a.labelAlign?!0:null,a.reserveSpaceDefault)&&c(h,function(a){G=Math.max(k[a].getLabelSize(),G)}),a.staggerLines&&(G*=a.staggerLines),a.labelOffset=G*(a.opposite?-1:1)):D(k,function(a,b){a.destroy();delete k[b]});z&&z.text&&!1!==z.enabled&&(a.addTitle(A),
A&&!1!==z.reserveSpace&&(a.titleOffset=F=a.axisTitle.getBBox()[p?"height":"width"],l=z.offset,f=n(l)?0:C(z.margin,p?5:10)));a.renderLine();a.offset=t*C(e.offset,u[x]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};d=0===x?-a.labelMetrics().h:2===x?a.tickRotCorr.y:0;f=Math.abs(G)+f;G&&(f=f-d+t*(p?C(J.y,a.tickRotCorr.y+8*t):J.x));a.axisTitleMargin=C(l,f);u[x]=Math.max(u[x],a.axisTitleMargin+F+t*a.offset,f,m&&h.length&&q?q[0]+t*a.offset:0);e=e.offset?0:2*Math.floor(a.axisLine.strokeWidth()/2);b[r]=Math.max(b[r],
e)},getLinePath:function(a){var b=this.chart,c=this.opposite,g=this.offset,d=this.horiz,e=this.left+(c?this.width:0)+g,g=b.chartHeight-this.bottom-(c?this.height:0)+g;c&&(a*=-1);return b.renderer.crispLine(["M",d?this.left:e,d?g:this.top,"L",d?b.chartWidth-this.right:e,d?g:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,
zIndex:7}))},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,d=this.len,e=this.options.title,h=a?b:c,k=this.opposite,p=this.offset,x=e.x||0,r=e.y||0,m=this.axisTitle,A=this.chart.renderer.fontMetrics(e.style&&e.style.fontSize,m),m=Math.max(m.getBBox(null,0).height-A.h-1,0),d={low:h+(a?0:d),middle:h+d/2,high:h+(a?d:0)}[e.align],b=(a?c+this.height:b)+(a?1:-1)*(k?-1:1)*this.axisTitleMargin+[-m,m,A.f,-m][this.side];return{x:a?d+x:b+(k?this.width:0)+p+x,y:a?b+r-(k?this.height:0)+p:
d+r}},renderMinorTick:function(a){var b=this.chart.hasRendered&&r(this.oldMin),c=this.minorTicks;c[a]||(c[a]=new J(this,a,"minor"));b&&c[a].isNew&&c[a].render(null,!0);c[a].render(null,!1,1)},renderTick:function(a,b){var c=this.isLinked,g=this.ticks,d=this.chart.hasRendered&&r(this.oldMin);if(!c||a>=this.min&&a<=this.max)g[a]||(g[a]=new J(this,a)),d&&g[a].isNew&&g[a].render(b,!0,.1),g[a].render(b)},render:function(){var b=this,d=b.chart,e=b.options,h=b.isLog,k=b.lin2log,p=b.isLinked,x=b.tickPositions,
m=b.axisTitle,F=b.ticks,l=b.minorTicks,f=b.alternateBands,z=e.stackLabels,G=e.alternateGridColor,n=b.tickmarkOffset,u=b.axisLine,C=b.showAxis,t=H(d.renderer.globalAnimation),v,M;b.labelEdge.length=0;b.overlap=!1;c([F,l,f],function(a){D(a,function(a){a.isActive=!1})});if(b.hasData()||p)b.minorTickInterval&&!b.categories&&c(b.getMinorTickPositions(),function(a){b.renderMinorTick(a)}),x.length&&(c(x,function(a,c){b.renderTick(a,c)}),n&&(0===b.min||b.single)&&(F[-1]||(F[-1]=new J(b,-1,null,!0)),F[-1].render(-1))),
G&&c(x,function(c,e){M=void 0!==x[e+1]?x[e+1]+n:b.max-n;0===e%2&&c<b.max&&M<=b.max+(d.polar?-n:n)&&(f[c]||(f[c]=new a.PlotLineOrBand(b)),v=c+n,f[c].options={from:h?k(v):v,to:h?k(M):M,color:G},f[c].render(),f[c].isActive=!0)}),b._addedPlotLB||(c((e.plotLines||[]).concat(e.plotBands||[]),function(a){b.addPlotBandOrLine(a)}),b._addedPlotLB=!0);c([F,l,f],function(a){var b,c=[],e=t.duration;D(a,function(a,b){a.isActive||(a.render(b,!1,0),a.isActive=!1,c.push(b))});A(function(){for(b=c.length;b--;)a[c[b]]&&
!a[c[b]].isActive&&(a[c[b]].destroy(),delete a[c[b]])},a!==f&&d.hasRendered&&e?e:0)});u&&(u[u.isPlaced?"animate":"attr"]({d:this.getLinePath(u.strokeWidth())}),u.isPlaced=!0,u[C?"show":"hide"](!0));m&&C&&(e=b.getTitlePosition(),r(e.y)?(m[m.isNew?"attr":"animate"](e),m.isNew=!1):(m.attr("y",-9999),m.isNew=!0));z&&z.enabled&&b.renderStackTotals();b.isDirty=!1},redraw:function(){this.visible&&(this.render(),c(this.plotLinesAndBands,function(a){a.render()}));c(this.series,function(a){a.isDirty=!0})},
keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var b=this,d=b.stacks,g=b.plotLinesAndBands,h;a||x(b);D(d,function(a,b){u(a);d[b]=null});c([b.ticks,b.minorTicks,b.alternateBands],function(a){u(a)});if(g)for(a=g.length;a--;)g[a].destroy();c("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),function(a){b[a]&&(b[a]=b[a].destroy())});for(h in b.plotLinesAndBandsGroups)b.plotLinesAndBandsGroups[h]=b.plotLinesAndBandsGroups[h].destroy();
D(b,function(a,c){-1===e(c,b.keepProps)&&delete b[c]})},drawCrosshair:function(a,b){var c,d=this.crosshair,e=C(d.snap,!0),g,h=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(n(b)||!e)?(e?n(b)&&(g=this.isXAxis?b.plotX:this.len-b.plotY):g=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),n(g)&&(c=this.getPlotLinePath(b&&(this.isXAxis?b.x:C(b.stackY,b.y)),null,null,null,g)||null),n(c)?(b=this.categories&&!this.isRadial,h||(this.cross=h=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+
(b?"category ":"thin ")+d.className).attr({zIndex:C(d.zIndex,2)}).add(),h.attr({stroke:d.color||(b?f("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":C(d.width,1)}).css({"pointer-events":"none"}),d.dashStyle&&h.attr({dashstyle:d.dashStyle})),h.show().attr({d:c}),b&&!d.width&&h.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&this.cross.hide()}});return a.Axis=G}(K);(function(a){var B=a.Axis,H=a.getMagnitude,
E=a.normalizeTickInterval,q=a.timeUnits;B.prototype.getTimeTicks=function(){return this.chart.time.getTimeTicks.apply(this.chart.time,arguments)};B.prototype.normalizeTimeTickInterval=function(a,l){var f=l||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];l=f[f.length-1];var n=q[l[0]],v=l[1],u;for(u=0;u<f.length&&!(l=f[u],n=q[l[0]],v=l[1],f[u+1]&&a<=(n*
v[v.length-1]+q[f[u+1][0]])/2);u++);n===q.year&&a<5*n&&(v=[1,2,5]);a=E(a/n,v,"year"===l[0]?Math.max(H(a/n),1):1);return{unitRange:n,count:a,unitName:l[0]}}})(K);(function(a){var B=a.Axis,H=a.getMagnitude,E=a.map,q=a.normalizeTickInterval,f=a.pick;B.prototype.getLogTickPositions=function(a,t,n,v){var l=this.options,c=this.len,b=this.lin2log,m=this.log2lin,d=[];v||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),d=this.getLinearTickPositions(a,t,n);else if(.08<=a)for(var c=Math.floor(t),h,k,
e,p,r,l=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];c<n+1&&!r;c++)for(k=l.length,h=0;h<k&&!r;h++)e=m(b(c)*l[h]),e>t&&(!v||p<=n)&&void 0!==p&&d.push(p),p>n&&(r=!0),p=e;else t=b(t),n=b(n),a=v?this.getMinorTickInterval():l.tickInterval,a=f("auto"===a?null:a,this._minorAutoInterval,l.tickPixelInterval/(v?5:1)*(n-t)/((v?c/this.tickPositions.length:c)||1)),a=q(a,null,H(a)),d=E(this.getLinearTickPositions(a,t,n),m),v||(this._minorAutoInterval=a/5);v||(this.tickInterval=a);return d};B.prototype.log2lin=
function(a){return Math.log(a)/Math.LN10};B.prototype.lin2log=function(a){return Math.pow(10,a)}})(K);(function(a,B){var H=a.arrayMax,E=a.arrayMin,q=a.defined,f=a.destroyObjectProperties,l=a.each,t=a.erase,n=a.merge,v=a.pick;a.PlotLineOrBand=function(a,c){this.axis=a;c&&(this.options=c,this.id=c.id)};a.PlotLineOrBand.prototype={render:function(){var f=this,c=f.axis,b=c.horiz,m=f.options,d=m.label,h=f.label,k=m.to,e=m.from,p=m.value,r=q(e)&&q(k),l=q(p),z=f.svgElem,t=!z,D=[],C=m.color,x=v(m.zIndex,
0),F=m.events,D={"class":"highcharts-plot-"+(r?"band ":"line ")+(m.className||"")},A={},J=c.chart.renderer,G=r?"bands":"lines",g=c.log2lin;c.isLog&&(e=g(e),k=g(k),p=g(p));l?(D={stroke:C,"stroke-width":m.width},m.dashStyle&&(D.dashstyle=m.dashStyle)):r&&(C&&(D.fill=C),m.borderWidth&&(D.stroke=m.borderColor,D["stroke-width"]=m.borderWidth));A.zIndex=x;G+="-"+x;(C=c.plotLinesAndBandsGroups[G])||(c.plotLinesAndBandsGroups[G]=C=J.g("plot-"+G).attr(A).add());t&&(f.svgElem=z=J.path().attr(D).add(C));if(l)D=
c.getPlotLinePath(p,z.strokeWidth());else if(r)D=c.getPlotBandPath(e,k,m);else return;t&&D&&D.length?(z.attr({d:D}),F&&a.objectEach(F,function(a,b){z.on(b,function(a){F[b].apply(f,[a])})})):z&&(D?(z.show(),z.animate({d:D})):(z.hide(),h&&(f.label=h=h.destroy())));d&&q(d.text)&&D&&D.length&&0<c.width&&0<c.height&&!D.flat?(d=n({align:b&&r&&"center",x:b?!r&&4:10,verticalAlign:!b&&r&&"middle",y:b?r?16:10:r?6:-4,rotation:b&&!r&&90},d),this.renderLabel(d,D,r,x)):h&&h.hide();return f},renderLabel:function(a,
c,b,m){var d=this.label,h=this.axis.chart.renderer;d||(d={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(b?"band":"line")+"-label "+(a.className||"")},d.zIndex=m,this.label=d=h.text(a.text,0,0,a.useHTML).attr(d).add(),d.css(a.style));m=c.xBounds||[c[1],c[4],b?c[6]:c[1]];c=c.yBounds||[c[2],c[5],b?c[7]:c[2]];b=E(m);h=E(c);d.align(a,!1,{x:b,y:h,width:H(m)-b,height:H(c)-h});d.show()},destroy:function(){t(this.axis.plotLinesAndBands,this);delete this.axis;f(this)}};a.extend(B.prototype,
{getPlotBandPath:function(a,c){var b=this.getPlotLinePath(c,null,null,!0),m=this.getPlotLinePath(a,null,null,!0),d=[],h=this.horiz,k=1,e;a=a<this.min&&c<this.min||a>this.max&&c>this.max;if(m&&b)for(a&&(e=m.toString()===b.toString(),k=0),a=0;a<m.length;a+=6)h&&b[a+1]===m[a+1]?(b[a+1]+=k,b[a+4]+=k):h||b[a+2]!==m[a+2]||(b[a+2]+=k,b[a+5]+=k),d.push("M",m[a+1],m[a+2],"L",m[a+4],m[a+5],b[a+4],b[a+5],b[a+1],b[a+2],"z"),d.flat=e;return d},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},
addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(f,c){var b=(new a.PlotLineOrBand(this,f)).render(),m=this.userOptions;b&&(c&&(m[c]=m[c]||[],m[c].push(f)),this.plotLinesAndBands.push(b));return b},removePlotBandOrLine:function(a){for(var c=this.plotLinesAndBands,b=this.options,m=this.userOptions,d=c.length;d--;)c[d].id===a&&c[d].destroy();l([b.plotLines||[],m.plotLines||[],b.plotBands||[],m.plotBands||[]],function(b){for(d=b.length;d--;)b[d].id===a&&
t(b,b[d])})},removePlotBand:function(a){this.removePlotBandOrLine(a)},removePlotLine:function(a){this.removePlotBandOrLine(a)}})})(K,V);(function(a){var B=a.each,H=a.extend,E=a.format,q=a.isNumber,f=a.map,l=a.merge,t=a.pick,n=a.splat,v=a.syncTimeout,u=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,b){this.chart=a;this.options=b;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=b.split&&!a.inverted;this.shared=b.shared||this.split},
cleanSplit:function(a){B(this.chart.series,function(b){var c=b&&b.tt;c&&(!c.isActive||a?b.tt=c.destroy():c.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,b=this.options;this.label||(this.split?this.label=a.g("tooltip"):(this.label=a.label("",0,0,b.shape||"callout",null,null,b.useHTML,null,"tooltip").attr({padding:b.padding,r:b.borderRadius}),this.label.attr({fill:b.backgroundColor,"stroke-width":b.borderWidth}).css(b.style).shadow(b.shadow)),this.label.attr({zIndex:8}).add());return this.label},
update:function(a){this.destroy();l(!0,this.chart.options.tooltip.userOptions,a);this.init(this.chart,l(!0,this.options,a))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,b,m,d){var c=this,k=c.now,e=!1!==c.options.animation&&!c.isHidden&&(1<Math.abs(a-k.x)||1<Math.abs(b-k.y)),p=c.followPointer||1<c.len;H(k,{x:e?(2*k.x+a)/
3:a,y:e?(k.y+b)/2:b,anchorX:p?void 0:e?(2*k.anchorX+m)/3:m,anchorY:p?void 0:e?(k.anchorY+d)/2:d});c.getLabel().attr(k);e&&(clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){c&&c.move(a,b,m,d)},32))},hide:function(a){var b=this;clearTimeout(this.hideTimer);a=t(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=v(function(){b.getLabel()[a?"fadeOut":"hide"]();b.isHidden=!0},a))},getAnchor:function(a,b){var c,d=this.chart,h=d.inverted,k=d.plotTop,e=d.plotLeft,p=0,r=
0,l,z;a=n(a);c=a[0].tooltipPos;this.followPointer&&b&&(void 0===b.chartX&&(b=d.pointer.normalize(b)),c=[b.chartX-d.plotLeft,b.chartY-k]);c||(B(a,function(a){l=a.series.yAxis;z=a.series.xAxis;p+=a.plotX+(!h&&z?z.left-e:0);r+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!h&&l?l.top-k:0)}),p/=a.length,r/=a.length,c=[h?d.plotWidth-r:p,this.shared&&!h&&1<a.length&&b?b.chartY-k:h?d.plotHeight-p:r]);return f(c,Math.round)},getPosition:function(a,b,m){var c=this.chart,h=this.distance,k={},e=c.inverted&&
m.h||0,p,r=["y",c.chartHeight,b,m.plotY+c.plotTop,c.plotTop,c.plotTop+c.plotHeight],f=["x",c.chartWidth,a,m.plotX+c.plotLeft,c.plotLeft,c.plotLeft+c.plotWidth],l=!this.followPointer&&t(m.ttBelow,!c.inverted===!!m.negative),n=function(a,b,c,d,g,p){var x=c<d-h,r=d+h+c<b,m=d-h-c;d+=h;if(l&&r)k[a]=d;else if(!l&&x)k[a]=m;else if(x)k[a]=Math.min(p-c,0>m-e?m:m-e);else if(r)k[a]=Math.max(g,d+e+c>b?d:d+e);else return!1},D=function(a,b,c,d){var e;d<h||d>b-h?e=!1:k[a]=d<c/2?1:d>b-c/2?b-c-2:d-c/2;return e},C=
function(a){var b=r;r=f;f=b;p=a},x=function(){!1!==n.apply(0,r)?!1!==D.apply(0,f)||p||(C(!0),x()):p?k.x=k.y=0:(C(!0),x())};(c.inverted||1<this.len)&&C();x();return k},defaultFormatter:function(a){var b=this.points||n(this),c;c=[a.tooltipFooterHeaderFormatter(b[0])];c=c.concat(a.bodyFormatter(b));c.push(a.tooltipFooterHeaderFormatter(b[0],!0));return c},refresh:function(a,b){var c,d=this.options,h,k=a,e,p={},r=[];c=d.formatter||this.defaultFormatter;var p=this.shared,f;d.enabled&&(clearTimeout(this.hideTimer),
this.followPointer=n(k)[0].series.tooltipOptions.followPointer,e=this.getAnchor(k,b),b=e[0],h=e[1],!p||k.series&&k.series.noSharedTooltip?p=k.getLabelConfig():(B(k,function(a){a.setState("hover");r.push(a.getLabelConfig())}),p={x:k[0].category,y:k[0].y},p.points=r,k=k[0]),this.len=r.length,p=c.call(p,this),f=k.series,this.distance=t(f.tooltipOptions.distance,16),!1===p?this.hide():(c=this.getLabel(),this.isHidden&&c.attr({opacity:1}).show(),this.split?this.renderSplit(p,n(a)):(d.style.width||c.css({width:this.chart.spacingBox.width}),
c.attr({text:p&&p.join?p.join(""):p}),c.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+t(k.colorIndex,f.colorIndex)),c.attr({stroke:d.borderColor||k.color||f.color||"#666666"}),this.updatePosition({plotX:b,plotY:h,negative:k.negative,ttBelow:k.ttBelow,h:e[2]||0})),this.isHidden=!1))},renderSplit:function(c,b){var m=this,d=[],h=this.chart,k=h.renderer,e=!0,p=this.options,r=0,f=this.getLabel();a.isString(c)&&(c=[!1,c]);B(c.slice(0,b.length+1),function(a,c){if(!1!==a){c=b[c-1]||
{isHeader:!0,plotX:b[0].plotX};var l=c.series||m,z=l.tt,x=c.series||{},F="highcharts-color-"+t(c.colorIndex,x.colorIndex,"none");z||(l.tt=z=k.label(null,null,null,"callout",null,null,p.useHTML).addClass("highcharts-tooltip-box "+F).attr({padding:p.padding,r:p.borderRadius,fill:p.backgroundColor,stroke:p.borderColor||c.color||x.color||"#333333","stroke-width":p.borderWidth}).add(f));z.isActive=!0;z.attr({text:a});z.css(p.style).shadow(p.shadow);a=z.getBBox();x=a.width+z.strokeWidth();c.isHeader?(r=
a.height,x=Math.max(0,Math.min(c.plotX+h.plotLeft-x/2,h.chartWidth-x))):x=c.plotX+h.plotLeft-t(p.distance,16)-x;0>x&&(e=!1);a=(c.series&&c.series.yAxis&&c.series.yAxis.pos)+(c.plotY||0);a-=h.plotTop;d.push({target:c.isHeader?h.plotHeight+r:a,rank:c.isHeader?1:0,size:l.tt.getBBox().height+1,point:c,x:x,tt:z})}});this.cleanSplit();a.distribute(d,h.plotHeight+r);B(d,function(a){var b=a.point,c=b.series;a.tt.attr({visibility:void 0===a.pos?"hidden":"inherit",x:e||b.isHeader?a.x:b.plotX+h.plotLeft+t(p.distance,
16),y:a.pos+h.plotTop,anchorX:b.isHeader?b.plotX+h.plotLeft:b.plotX+c.xAxis.pos,anchorY:b.isHeader?a.pos+h.plotTop-15:b.plotY+c.yAxis.pos})})},updatePosition:function(a){var b=this.chart,c=this.getLabel(),c=(this.options.positioner||this.getPosition).call(this,c.width,c.height,a);this.move(Math.round(c.x),Math.round(c.y||0),a.plotX+b.plotLeft,a.plotY+b.plotTop)},getDateFormat:function(a,b,m,d){var c=this.chart.time,k=c.dateFormat("%m-%d %H:%M:%S.%L",b),e,p,r={millisecond:15,second:12,minute:9,hour:6,
day:3},f="millisecond";for(p in u){if(a===u.week&&+c.dateFormat("%w",b)===m&&"00:00:00.000"===k.substr(6)){p="week";break}if(u[p]>a){p=f;break}if(r[p]&&k.substr(r[p])!=="01-01 00:00:00.000".substr(r[p]))break;"week"!==p&&(f=p)}p&&(e=d[p]);return e},getXDateFormat:function(a,b,m){b=b.dateTimeLabelFormats;var c=m&&m.closestPointRange;return(c?this.getDateFormat(c,a.x,m.options.startOfWeek,b):b.day)||b.year},tooltipFooterHeaderFormatter:function(a,b){b=b?"footer":"header";var c=a.series,d=c.tooltipOptions,
h=d.xDateFormat,k=c.xAxis,e=k&&"datetime"===k.options.type&&q(a.key),p=d[b+"Format"];e&&!h&&(h=this.getXDateFormat(a,d,k));e&&h&&B(a.point&&a.point.tooltipDateKeys||["key"],function(a){p=p.replace("{point."+a+"}","{point."+a+":"+h+"}")});return E(p,{point:a,series:c},this.chart.time)},bodyFormatter:function(a){return f(a,function(a){var b=a.series.tooltipOptions;return(b[(a.point.formatPrefix||"point")+"Formatter"]||a.point.tooltipFormatter).call(a.point,b[(a.point.formatPrefix||"point")+"Format"])})}}})(K);
(function(a){var B=a.addEvent,H=a.attr,E=a.charts,q=a.color,f=a.css,l=a.defined,t=a.each,n=a.extend,v=a.find,u=a.fireEvent,c=a.isNumber,b=a.isObject,m=a.offset,d=a.pick,h=a.splat,k=a.Tooltip;a.Pointer=function(a,b){this.init(a,b)};a.Pointer.prototype={init:function(a,b){this.options=b;this.chart=a;this.runChartClick=b.chart.events&&!!b.chart.events.click;this.pinchDown=[];this.lastValidTouch={};k&&(a.tooltip=new k(a,b.tooltip),this.followTouchMove=d(b.tooltip.followTouchMove,!0));this.setDOMEvents()},
zoomOption:function(a){var b=this.chart,c=b.options.chart,e=c.zoomType||"",b=b.inverted;/touch/.test(a.type)&&(e=d(c.pinchType,e));this.zoomX=a=/x/.test(e);this.zoomY=e=/y/.test(e);this.zoomHor=a&&!b||e&&b;this.zoomVert=e&&!b||a&&b;this.hasZoom=a||e},normalize:function(a,b){var c;c=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;b||(this.chartPosition=b=m(this.chart.container));return n(a,{chartX:Math.round(c.pageX-b.left),chartY:Math.round(c.pageY-b.top)})},getCoordinates:function(a){var b=
{xAxis:[],yAxis:[]};t(this.chart.axes,function(c){b[c.isXAxis?"xAxis":"yAxis"].push({axis:c,value:c.toValue(a[c.horiz?"chartX":"chartY"])})});return b},findNearestKDPoint:function(a,c,d){var e;t(a,function(a){var h=!(a.noSharedTooltip&&c)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(d,h);if((h=b(a,!0))&&!(h=!b(e,!0)))var h=e.distX-a.distX,k=e.dist-a.dist,p=(a.series.group&&a.series.group.zIndex)-(e.series.group&&e.series.group.zIndex),h=0<(0!==h&&c?h:0!==k?k:0!==p?p:e.series.index>
a.series.index?-1:1);h&&(e=a)});return e},getPointFromEvent:function(a){a=a.target;for(var b;a&&!b;)b=a.point,a=a.parentNode;return b},getChartCoordinatesFromPoint:function(a,b){var c=a.series,e=c.xAxis,c=c.yAxis,h=d(a.clientX,a.plotX);if(e&&c)return b?{chartX:e.len+e.pos-h,chartY:c.len+c.pos-a.plotY}:{chartX:h+e.pos,chartY:a.plotY+c.pos}},getHoverData:function(c,h,k,f,m,l,n){var e,x=[],p=n&&n.isBoosting;f=!(!f||!c);n=h&&!h.stickyTracking?[h]:a.grep(k,function(a){return a.visible&&!(!m&&a.directTouch)&&
d(a.options.enableMouseTracking,!0)&&a.stickyTracking});h=(e=f?c:this.findNearestKDPoint(n,m,l))&&e.series;e&&(m&&!h.noSharedTooltip?(n=a.grep(k,function(a){return a.visible&&!(!m&&a.directTouch)&&d(a.options.enableMouseTracking,!0)&&!a.noSharedTooltip}),t(n,function(a){var c=v(a.points,function(a){return a.x===e.x&&!a.isNull});b(c)&&(p&&(c=a.getPoint(c)),x.push(c))})):x.push(e));return{hoverPoint:e,hoverSeries:h,hoverPoints:x}},runPointActions:function(b,c){var e=this.chart,h=e.tooltip&&e.tooltip.options.enabled?
e.tooltip:void 0,k=h?h.shared:!1,p=c||e.hoverPoint,f=p&&p.series||e.hoverSeries,f=this.getHoverData(p,f,e.series,!!c||f&&f.directTouch&&this.isDirectTouch,k,b,{isBoosting:e.isBoosting}),m,p=f.hoverPoint;m=f.hoverPoints;c=(f=f.hoverSeries)&&f.tooltipOptions.followPointer;k=k&&f&&!f.noSharedTooltip;if(p&&(p!==e.hoverPoint||h&&h.isHidden)){t(e.hoverPoints||[],function(b){-1===a.inArray(b,m)&&b.setState()});t(m||[],function(a){a.setState("hover")});if(e.hoverSeries!==f)f.onMouseOver();e.hoverPoint&&e.hoverPoint.firePointEvent("mouseOut");
if(!p.series)return;p.firePointEvent("mouseOver");e.hoverPoints=m;e.hoverPoint=p;h&&h.refresh(k?m:p,b)}else c&&h&&!h.isHidden&&(p=h.getAnchor([{}],b),h.updatePosition({plotX:p[0],plotY:p[1]}));this.unDocMouseMove||(this.unDocMouseMove=B(e.container.ownerDocument,"mousemove",function(b){var c=E[a.hoverChartIndex];if(c)c.pointer.onDocumentMouseMove(b)}));t(e.axes,function(c){var e=d(c.crosshair.snap,!0),h=e?a.find(m,function(a){return a.series[c.coll]===c}):void 0;h||!e?c.drawCrosshair(b,h):c.hideCrosshair()})},
reset:function(a,b){var c=this.chart,d=c.hoverSeries,e=c.hoverPoint,k=c.hoverPoints,p=c.tooltip,f=p&&p.shared?k:e;a&&f&&t(h(f),function(b){b.series.isCartesian&&void 0===b.plotX&&(a=!1)});if(a)p&&f&&(p.refresh(f),e&&(e.setState(e.state,!0),t(c.axes,function(a){a.crosshair&&a.drawCrosshair(null,e)})));else{if(e)e.onMouseOut();k&&t(k,function(a){a.setState()});if(d)d.onMouseOut();p&&p.hide(b);this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());t(c.axes,function(a){a.hideCrosshair()});this.hoverX=
c.hoverPoints=c.hoverPoint=null}},scaleGroups:function(a,b){var c=this.chart,d;t(c.series,function(e){d=a||e.getPlotBox();e.xAxis&&e.xAxis.zoomEnabled&&e.group&&(e.group.attr(d),e.markerGroup&&(e.markerGroup.attr(d),e.markerGroup.clip(b?c.clipRect:null)),e.dataLabelsGroup&&e.dataLabelsGroup.attr(d))});c.clipRect.attr(b||c.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=
this.chart,c=b.options.chart,d=a.chartX,e=a.chartY,h=this.zoomHor,k=this.zoomVert,f=b.plotLeft,x=b.plotTop,m=b.plotWidth,A=b.plotHeight,l,n=this.selectionMarker,g=this.mouseDownX,w=this.mouseDownY,t=c.panKey&&a[c.panKey+"Key"];n&&n.touch||(d<f?d=f:d>f+m&&(d=f+m),e<x?e=x:e>x+A&&(e=x+A),this.hasDragged=Math.sqrt(Math.pow(g-d,2)+Math.pow(w-e,2)),10<this.hasDragged&&(l=b.isInsidePlot(g-f,w-x),b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&l&&!t&&!n&&(this.selectionMarker=n=b.renderer.rect(f,x,h?1:m,
k?1:A,0).attr({fill:c.selectionMarkerFill||q("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",zIndex:7}).add()),n&&h&&(d-=g,n.attr({width:Math.abs(d),x:(0<d?0:d)+g})),n&&k&&(d=e-w,n.attr({height:Math.abs(d),y:(0<d?0:d)+w})),l&&!n&&c.panning&&b.pan(a,c.panning)))},drop:function(a){var b=this,d=this.chart,e=this.hasPinched;if(this.selectionMarker){var h={originalEvent:a,xAxis:[],yAxis:[]},k=this.selectionMarker,m=k.attr?k.attr("x"):k.x,C=k.attr?k.attr("y"):k.y,x=k.attr?k.attr("width"):
k.width,F=k.attr?k.attr("height"):k.height,A;if(this.hasDragged||e)t(d.axes,function(c){if(c.zoomEnabled&&l(c.min)&&(e||b[{xAxis:"zoomX",yAxis:"zoomY"}[c.coll]])){var d=c.horiz,g="touchend"===a.type?c.minPixelPadding:0,k=c.toValue((d?m:C)+g),d=c.toValue((d?m+x:C+F)-g);h[c.coll].push({axis:c,min:Math.min(k,d),max:Math.max(k,d)});A=!0}}),A&&u(d,"selection",h,function(a){d.zoom(n(a,e?{animation:!1}:null))});c(d.index)&&(this.selectionMarker=this.selectionMarker.destroy());e&&this.scaleGroups()}d&&c(d.index)&&
(f(d.container,{cursor:d._cursor}),d.cancelClick=10<this.hasDragged,d.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){a=this.normalize(a);2!==a.button&&(this.zoomOption(a),a.preventDefault&&a.preventDefault(),this.dragStart(a))},onDocumentMouseUp:function(b){E[a.hoverChartIndex]&&E[a.hoverChartIndex].pointer.drop(b)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition;a=this.normalize(a,c);!c||this.inClass(a.target,"highcharts-tracker")||
b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},onContainerMouseLeave:function(b){var c=E[a.hoverChartIndex];c&&(b.relatedTarget||b.toElement)&&(c.pointer.reset(),c.pointer.chartPosition=null)},onContainerMouseMove:function(b){var c=this.chart;l(a.hoverChartIndex)&&E[a.hoverChartIndex]&&E[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=c.index);b=this.normalize(b);b.returnValue=!1;"mousedown"===c.mouseIsDown&&this.drag(b);!this.inClass(b.target,"highcharts-tracker")&&!c.isInsidePlot(b.chartX-
c.plotLeft,b.chartY-c.plotTop)||c.openMenu||this.runPointActions(b)},inClass:function(a,b){for(var c;a;){if(c=H(a,"class")){if(-1!==c.indexOf(b))return!0;if(-1!==c.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;this.isDirectTouch=!1;if(!(!b||!a||b.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},
onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,d=b.plotLeft,e=b.plotTop;a=this.normalize(a);b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(u(c.series,"click",n(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",a)):(n(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-d,a.chartY-e)&&u(b,"click",a)))},setDOMEvents:function(){var b=this,c=b.chart.container,d=c.ownerDocument;c.onmousedown=function(a){b.onContainerMouseDown(a)};c.onmousemove=function(a){b.onContainerMouseMove(a)};
c.onclick=function(a){b.onContainerClick(a)};this.unbindContainerMouseLeave=B(c,"mouseleave",b.onContainerMouseLeave);a.unbindDocumentMouseUp||(a.unbindDocumentMouseUp=B(d,"mouseup",b.onDocumentMouseUp));a.hasTouch&&(c.ontouchstart=function(a){b.onContainerTouchStart(a)},c.ontouchmove=function(a){b.onContainerTouchMove(a)},a.unbindDocumentTouchEnd||(a.unbindDocumentTouchEnd=B(d,"touchend",b.onDocumentTouchEnd)))},destroy:function(){var b=this;b.unDocMouseMove&&b.unDocMouseMove();this.unbindContainerMouseLeave();
a.chartCount||(a.unbindDocumentMouseUp&&(a.unbindDocumentMouseUp=a.unbindDocumentMouseUp()),a.unbindDocumentTouchEnd&&(a.unbindDocumentTouchEnd=a.unbindDocumentTouchEnd()));clearInterval(b.tooltipTimeout);a.objectEach(b,function(a,c){b[c]=null})}}})(K);(function(a){var B=a.charts,H=a.each,E=a.extend,q=a.map,f=a.noop,l=a.pick;E(a.Pointer.prototype,{pinchTranslate:function(a,f,l,q,c,b){this.zoomHor&&this.pinchTranslateDirection(!0,a,f,l,q,c,b);this.zoomVert&&this.pinchTranslateDirection(!1,a,f,l,q,
c,b)},pinchTranslateDirection:function(a,f,l,q,c,b,m,d){var h=this.chart,k=a?"x":"y",e=a?"X":"Y",p="chart"+e,r=a?"width":"height",n=h["plot"+(a?"Left":"Top")],z,t,D=d||1,C=h.inverted,x=h.bounds[a?"h":"v"],F=1===f.length,A=f[0][p],J=l[0][p],G=!F&&f[1][p],g=!F&&l[1][p],w;l=function(){!F&&20<Math.abs(A-G)&&(D=d||Math.abs(J-g)/Math.abs(A-G));t=(n-J)/D+A;z=h["plot"+(a?"Width":"Height")]/D};l();f=t;f<x.min?(f=x.min,w=!0):f+z>x.max&&(f=x.max-z,w=!0);w?(J-=.8*(J-m[k][0]),F||(g-=.8*(g-m[k][1])),l()):m[k]=
[J,g];C||(b[k]=t-n,b[r]=z);b=C?1/D:D;c[r]=z;c[k]=f;q[C?a?"scaleY":"scaleX":"scale"+e]=D;q["translate"+e]=b*n+(J-b*A)},pinch:function(a){var n=this,t=n.chart,u=n.pinchDown,c=a.touches,b=c.length,m=n.lastValidTouch,d=n.hasZoom,h=n.selectionMarker,k={},e=1===b&&(n.inClass(a.target,"highcharts-tracker")&&t.runTrackerClick||n.runChartClick),p={};1<b&&(n.initiated=!0);d&&n.initiated&&!e&&a.preventDefault();q(c,function(a){return n.normalize(a)});"touchstart"===a.type?(H(c,function(a,b){u[b]={chartX:a.chartX,
chartY:a.chartY}}),m.x=[u[0].chartX,u[1]&&u[1].chartX],m.y=[u[0].chartY,u[1]&&u[1].chartY],H(t.axes,function(a){if(a.zoomEnabled){var b=t.bounds[a.horiz?"h":"v"],c=a.minPixelPadding,d=a.toPixels(l(a.options.min,a.dataMin)),e=a.toPixels(l(a.options.max,a.dataMax)),h=Math.max(d,e);b.min=Math.min(a.pos,Math.min(d,e)-c);b.max=Math.max(a.pos+a.len,h+c)}}),n.res=!0):n.followTouchMove&&1===b?this.runPointActions(n.normalize(a)):u.length&&(h||(n.selectionMarker=h=E({destroy:f,touch:!0},t.plotBox)),n.pinchTranslate(u,
c,k,h,p,m),n.hasPinched=d,n.scaleGroups(k,p),n.res&&(n.res=!1,this.reset(!1,0)))},touch:function(f,n){var t=this.chart,q,c;if(t.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});a.hoverChartIndex=t.index;1===f.touches.length?(f=this.normalize(f),(c=t.isInsidePlot(f.chartX-t.plotLeft,f.chartY-t.plotTop))&&!t.openMenu?(n&&this.runPointActions(f),"touchmove"===f.type&&(n=this.pinchDown,q=n[0]?4<=Math.sqrt(Math.pow(n[0].chartX-f.chartX,2)+Math.pow(n[0].chartY-f.chartY,2)):!1),l(q,
!0)&&this.pinch(f)):n&&this.reset()):2===f.touches.length&&this.pinch(f)},onContainerTouchStart:function(a){this.zoomOption(a);this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(f){B[a.hoverChartIndex]&&B[a.hoverChartIndex].pointer.drop(f)}})})(K);(function(a){var B=a.addEvent,H=a.charts,E=a.css,q=a.doc,f=a.extend,l=a.noop,t=a.Pointer,n=a.removeEvent,v=a.win,u=a.wrap;if(!a.hasTouch&&(v.PointerEvent||v.MSPointerEvent)){var c={},b=!!v.PointerEvent,m=function(){var b=
[];b.item=function(a){return this[a]};a.objectEach(c,function(a){b.push({pageX:a.pageX,pageY:a.pageY,target:a.target})});return b},d=function(b,c,d,f){"touch"!==b.pointerType&&b.pointerType!==b.MSPOINTER_TYPE_TOUCH||!H[a.hoverChartIndex]||(f(b),f=H[a.hoverChartIndex].pointer,f[c]({type:d,target:b.currentTarget,preventDefault:l,touches:m()}))};f(t.prototype,{onContainerPointerDown:function(a){d(a,"onContainerTouchStart","touchstart",function(a){c[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},
onContainerPointerMove:function(a){d(a,"onContainerTouchMove","touchmove",function(a){c[a.pointerId]={pageX:a.pageX,pageY:a.pageY};c[a.pointerId].target||(c[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){d(a,"onDocumentTouchEnd","touchend",function(a){delete c[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,b?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,b?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(q,b?
"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});u(t.prototype,"init",function(a,b,c){a.call(this,b,c);this.hasZoom&&E(b.container,{"-ms-touch-action":"none","touch-action":"none"})});u(t.prototype,"setDOMEvents",function(a){a.apply(this);(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(B)});u(t.prototype,"destroy",function(a){this.batchMSEvents(n);a.call(this)})}})(K);(function(a){var B=a.addEvent,H=a.css,E=a.discardElement,q=a.defined,f=a.each,l=a.isFirefox,t=a.marginNames,n=a.merge,
v=a.pick,u=a.setAnimation,c=a.stableSort,b=a.win,m=a.wrap;a.Legend=function(a,b){this.init(a,b)};a.Legend.prototype={init:function(a,b){this.chart=a;this.setOptions(b);b.enabled&&(this.render(),B(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},setOptions:function(a){var b=v(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=n(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.padding=b;this.initialItemY=b-5;this.itemHeight=
this.maxItemWidth=0;this.symbolWidth=v(a.symbolWidth,16);this.pages=[]},update:function(a,b){var c=this.chart;this.setOptions(n(!0,this.options,a));this.destroy();c.isDirtyLegend=c.isDirtyBox=!0;v(b,!0)&&c.redraw()},colorizeItem:function(a,b){a.legendGroup[b?"removeClass":"addClass"]("highcharts-legend-item-hidden");var c=this.options,d=a.legendItem,h=a.legendLine,f=a.legendSymbol,m=this.itemHiddenStyle.color,c=b?c.itemStyle.color:m,l=b?a.color||m:m,n=a.options&&a.options.marker,D={fill:l};d&&d.css({fill:c,
color:c});h&&h.attr({stroke:l});f&&(n&&f.isMarker&&(D=a.pointAttribs(),b||(D.stroke=D.fill=m)),f.attr(D))},positionItem:function(a){var b=this.options,c=b.symbolPadding,b=!b.rtl,d=a._legendItemPos,f=d[0],d=d[1],m=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(b?f:this.legendWidth-f-2*c-4,d);m&&(m.x=f,m.y=d)},destroyItem:function(a){var b=a.checkbox;f(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});b&&E(a.checkbox)},destroy:function(){function a(a){this[a]&&
(this[a]=this[a].destroy())}f(this.getAllItems(),function(b){f(["legendItem","legendGroup"],a,b)});f("clipRect up down pager nav box title group".split(" "),a,this);this.display=null},positionCheckboxes:function(){var a=this.group&&this.group.alignAttr,b,c=this.clipHeight||this.legendHeight,e=this.titleHeight;a&&(b=a.translateY,f(this.allItems,function(d){var h=d.checkbox,k;h&&(k=b+e+h.y+(this.scrollOffset||0)+3,H(h,{left:a.translateX+d.checkboxOffset+h.x-20+"px",top:k+"px",display:k>b-6&&k<b+c-6?
"":"none"}))},this))},renderTitle:function(){var a=this.options,b=this.padding,c=a.title,e=0;c.text&&(this.title||(this.title=this.chart.renderer.label(c.text,b-3,b-4,null,null,null,a.useHTML,null,"legend-title").attr({zIndex:1}).css(c.style).add(this.group)),a=this.title.getBBox(),e=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:e}));this.titleHeight=e},setText:function(b){var c=this.options;b.legendItem.attr({text:c.labelFormat?a.format(c.labelFormat,b,this.chart.time):c.labelFormatter.call(b)})},
renderItem:function(a){var b=this.chart,c=b.renderer,d=this.options,f="horizontal"===d.layout,m=this.symbolWidth,l=d.symbolPadding,z=this.itemStyle,q=this.itemHiddenStyle,D=this.padding,C=f?v(d.itemDistance,20):0,x=!d.rtl,F=d.width,A=d.itemMarginBottom||0,J=this.itemMarginTop,G=a.legendItem,g=!a.series,w=!g&&a.series.drawLegendSymbol?a.series:a,t=w.options,u=this.createCheckboxForItem&&t&&t.showCheckbox,t=m+l+C+(u?20:0),N=d.useHTML,O=a.options.className;G||(a.legendGroup=c.g("legend-item").addClass("highcharts-"+
w.type+"-series highcharts-color-"+a.colorIndex+(O?" "+O:"")+(g?" highcharts-series-"+a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=G=c.text("",x?m+l:-l,this.baseline||0,N).css(n(a.visible?z:q)).attr({align:x?"left":"right",zIndex:2}).add(a.legendGroup),this.baseline||(m=z.fontSize,this.fontMetrics=c.fontMetrics(m,G),this.baseline=this.fontMetrics.f+3+J,G.attr("y",this.baseline)),this.symbolHeight=d.symbolHeight||this.fontMetrics.f,w.drawLegendSymbol(this,a),this.setItemEvents&&
this.setItemEvents(a,G,N),u&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);z.width||G.css({width:(d.itemWidth||d.width||b.spacingBox.width)-t});this.setText(a);c=G.getBBox();z=a.checkboxOffset=d.itemWidth||a.legendItemWidth||c.width+t;this.itemHeight=c=Math.round(a.legendItemHeight||c.height||this.symbolHeight);f&&this.itemX-D+z>(F||b.spacingBox.width-2*D-d.x)&&(this.itemX=D,this.itemY+=J+this.lastLineHeight+A,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,z);
this.lastItemY=J+this.itemY+A;this.lastLineHeight=Math.max(c,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];f?this.itemX+=z:(this.itemY+=J+c+A,this.lastLineHeight=c);this.offsetWidth=F||Math.max((f?this.itemX-D-(a.checkbox?0:C):z)+D,this.offsetWidth)},getAllItems:function(){var a=[];f(this.chart.series,function(b){var c=b&&b.options;b&&v(c.showInLegend,q(c.linkedTo)?!1:void 0,!0)&&(a=a.concat(b.legendItems||("point"===c.legendType?b.data:b)))});return a},getAlignment:function(){var a=
this.options;return a.floating?"":a.align.charAt(0)+a.verticalAlign.charAt(0)+a.layout.charAt(0)},adjustMargins:function(a,b){var c=this.chart,d=this.options,h=this.getAlignment();h&&f([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(e,k){e.test(h)&&!q(a[k])&&(c[t[k]]=Math.max(c[t[k]],c.legend[(k+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][k]*d[k%2?"x":"y"]+v(d.margin,12)+b[k]+(0===k?c.titleOffset+c.options.title.margin:0)))})},render:function(){var a=this,b=a.chart,k=b.renderer,
e=a.group,m,l,t,z,q=a.box,D=a.options,C=a.padding;a.itemX=C;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;e||(a.group=e=k.g("legend").attr({zIndex:7}).add(),a.contentGroup=k.g().attr({zIndex:1}).add(e),a.scrollGroup=k.g().add(a.contentGroup));a.renderTitle();m=a.getAllItems();c(m,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});D.reversed&&m.reverse();a.allItems=m;a.display=l=!!m.length;a.lastLineHeight=0;f(m,function(b){a.renderItem(b)});t=
(D.width||a.offsetWidth)+C;z=a.lastItemY+a.lastLineHeight+a.titleHeight;z=a.handleOverflow(z);z+=C;q||(a.box=q=k.rect().addClass("highcharts-legend-box").attr({r:D.borderRadius}).add(e),q.isNew=!0);q.attr({stroke:D.borderColor,"stroke-width":D.borderWidth||0,fill:D.backgroundColor||"none"}).shadow(D.shadow);0<t&&0<z&&(q[q.isNew?"attr":"animate"](q.crisp.call({},{x:0,y:0,width:t,height:z},q.strokeWidth())),q.isNew=!1);q[l?"show":"hide"]();a.legendWidth=t;a.legendHeight=z;f(m,function(b){a.positionItem(b)});
l&&(k=b.spacingBox,/(lth|ct|rth)/.test(a.getAlignment())&&(k=n(k,{y:k.y+b.titleOffset+b.options.title.margin})),e.align(n(D,{width:t,height:z}),!0,k));b.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var b=this,c=this.chart,d=c.renderer,m=this.options,l=m.y,n=this.padding,c=c.spacingBox.height+("top"===m.verticalAlign?-l:l)-n,l=m.maxHeight,z,q=this.clipRect,D=m.navigation,t=v(D.animation,!0),x=D.arrowSize||12,F=this.nav,A=this.pages,J,G=this.allItems,g=function(a){"number"===typeof a?
q.attr({height:a}):q&&(b.clipRect=q.destroy(),b.contentGroup.clip());b.contentGroup.div&&(b.contentGroup.div.style.clip=a?"rect("+n+"px,9999px,"+(n+a)+"px,0)":"auto")};"horizontal"!==m.layout||"middle"===m.verticalAlign||m.floating||(c/=2);l&&(c=Math.min(c,l));A.length=0;a>c&&!1!==D.enabled?(this.clipHeight=z=Math.max(c-20-this.titleHeight-n,0),this.currentPage=v(this.currentPage,1),this.fullHeight=a,f(G,function(a,b){var c=a._legendItemPos[1],d=Math.round(a.legendItem.getBBox().height),g=A.length;
if(!g||c-A[g-1]>z&&(J||c)!==A[g-1])A.push(J||c),g++;a.pageIx=g-1;J&&(G[b-1].pageIx=g-1);b===G.length-1&&c+d-A[g-1]>z&&(A.push(c),a.pageIx=g);c!==J&&(J=c)}),q||(q=b.clipRect=d.clipRect(0,n,9999,0),b.contentGroup.clip(q)),g(z),F||(this.nav=F=d.g().attr({zIndex:1}).add(this.group),this.up=d.symbol("triangle",0,0,x,x).on("click",function(){b.scroll(-1,t)}).add(F),this.pager=d.text("",15,10).addClass("highcharts-legend-navigation").css(D.style).add(F),this.down=d.symbol("triangle-down",0,0,x,x).on("click",
function(){b.scroll(1,t)}).add(F)),b.scroll(0),a=c):F&&(g(),this.nav=F.destroy(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0);return a},scroll:function(a,b){var c=this.pages,d=c.length;a=this.currentPage+a;var h=this.clipHeight,f=this.options.navigation,m=this.pager,l=this.padding;a>d&&(a=d);0<a&&(void 0!==b&&u(b,this.chart),this.nav.attr({translateX:l,translateY:h+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),
m.attr({text:a+"/"+d}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===d?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?f.inactiveColor:f.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===d?f.inactiveColor:f.activeColor}).css({cursor:a===d?"default":"pointer"}),this.scrollOffset=-c[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:this.scrollOffset}),this.currentPage=a,this.positionCheckboxes())}};a.LegendSymbolMixin=
{drawRectangle:function(a,b){var c=a.symbolHeight,d=a.options.squareSymbol;b.legendSymbol=this.chart.renderer.rect(d?(a.symbolWidth-c)/2:0,a.baseline-c+1,d?c:a.symbolWidth,c,v(a.options.symbolRadius,c/2)).addClass("highcharts-point").attr({zIndex:3}).add(b.legendGroup)},drawLineMarker:function(a){var b=this.options,c=b.marker,d=a.symbolWidth,f=a.symbolHeight,m=f/2,l=this.chart.renderer,z=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var q;q={"stroke-width":b.lineWidth||0};b.dashStyle&&
(q.dashstyle=b.dashStyle);this.legendLine=l.path(["M",0,a,"L",d,a]).addClass("highcharts-graph").attr(q).add(z);c&&!1!==c.enabled&&(b=Math.min(v(c.radius,m),m),0===this.symbol.indexOf("url")&&(c=n(c,{width:f,height:f}),b=0),this.legendSymbol=c=l.symbol(this.symbol,d/2-b,a-b,2*b,2*b,c).addClass("highcharts-point").add(z),c.isMarker=!0)}};(/Trident\/7\.0/.test(b.navigator.userAgent)||l)&&m(a.Legend.prototype,"positionItem",function(a,b){var c=this,d=function(){b._legendItemPos&&a.call(c,b)};d();setTimeout(d)})})(K);
(function(a){var B=a.addEvent,H=a.animate,E=a.animObject,q=a.attr,f=a.doc,l=a.Axis,t=a.createElement,n=a.defaultOptions,v=a.discardElement,u=a.charts,c=a.css,b=a.defined,m=a.each,d=a.extend,h=a.find,k=a.fireEvent,e=a.grep,p=a.isNumber,r=a.isObject,I=a.isString,z=a.Legend,M=a.marginNames,D=a.merge,C=a.objectEach,x=a.Pointer,F=a.pick,A=a.pInt,J=a.removeEvent,G=a.seriesTypes,g=a.splat,w=a.syncTimeout,L=a.win,P=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,b,c){return new P(a,
b,c)};d(P.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);if(I(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(b,c){var g,d,e=b.series,h=b.plotOptions||{};b.series=null;g=D(n,b);for(d in g.plotOptions)g.plotOptions[d].tooltip=h[d]&&D(h[d].tooltip)||void 0;g.tooltip.userOptions=b.chart&&b.chart.forExport&&b.tooltip.userOptions||b.tooltip;g.series=b.series=e;this.userOptions=b;d=g.chart;e=d.events;this.margin=[];this.spacing=[];this.bounds=
{h:{},v:{}};this.labelCollectors=[];this.callback=c;this.isResizing=0;this.options=g;this.axes=[];this.series=[];this.time=b.time&&a.keys(b.time).length?new a.Time(b.time):a.time;this.hasCartesianSeries=d.showAxes;var k=this;k.index=u.length;u.push(k);a.chartCount++;e&&C(e,function(a,b){B(k,b,a)});k.xAxis=[];k.yAxis=[];k.pointCount=k.colorCounter=k.symbolCounter=0;k.firstRender()},initSeries:function(b){var c=this.options.chart;(c=G[b.type||c.type||c.defaultSeriesType])||a.error(17,!0);c=new c;c.init(this,
b);return c},orderSeries:function(a){var b=this.series;for(a=a||0;a<b.length;a++)b[a]&&(b[a].index=a,b[a].name=b[a].getName())},isInsidePlot:function(a,b,c){var g=c?b:a;a=c?a:b;return 0<=g&&g<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(b){var c=this.axes,g=this.series,e=this.pointer,h=this.legend,f=this.isDirtyLegend,x,l,p=this.hasCartesianSeries,A=this.isDirtyBox,F,n=this.renderer,w=n.isHidden(),r=[];this.setResponsive&&this.setResponsive(!1);a.setAnimation(b,this);w&&this.temporaryDisplay();
this.layOutTitles();for(b=g.length;b--;)if(F=g[b],F.options.stacking&&(x=!0,F.isDirty)){l=!0;break}if(l)for(b=g.length;b--;)F=g[b],F.options.stacking&&(F.isDirty=!0);m(g,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),f=!0);a.isDirtyData&&k(a,"updatedData")});f&&h.options.enabled&&(h.render(),this.isDirtyLegend=!1);x&&this.getStacks();p&&m(c,function(a){a.updateNames();a.setScale()});this.getMargins();p&&(m(c,function(a){a.isDirty&&(A=!0)}),m(c,function(a){var b=
a.min+","+a.max;a.extKey!==b&&(a.extKey=b,r.push(function(){k(a,"afterSetExtremes",d(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(A||x)&&a.redraw()}));A&&this.drawChartBox();k(this,"predraw");m(g,function(a){(A||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});e&&e.reset(!0);n.draw();k(this,"redraw");k(this,"render");w&&this.temporaryDisplay(!0);m(r,function(a){a.call()})},get:function(a){function b(b){return b.id===a||b.options&&b.options.id===a}var c,g=this.series,d;c=h(this.axes,b)||
h(this.series,b);for(d=0;!c&&d<g.length;d++)c=h(g[d].points||[],b);return c},getAxes:function(){var a=this,b=this.options,c=b.xAxis=g(b.xAxis||{}),b=b.yAxis=g(b.yAxis||{});k(this,"beforeGetAxes");m(c,function(a,b){a.index=b;a.isX=!0});m(b,function(a,b){a.index=b});c=c.concat(b);m(c,function(b){new l(a,b)})},getSelectedPoints:function(){var a=[];m(this.series,function(b){a=a.concat(e(b.data||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return e(this.series,function(a){return a.selected})},
setTitle:function(a,b,c){var g=this,d=g.options,e;e=d.title=D({style:{color:"#333333",fontSize:d.isStock?"16px":"18px"}},d.title,a);d=d.subtitle=D({style:{color:"#666666"}},d.subtitle,b);m([["title",a,e],["subtitle",b,d]],function(a,b){var c=a[0],d=g[c],e=a[1];a=a[2];d&&e&&(g[c]=d=d.destroy());a&&!d&&(g[c]=g.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+c,zIndex:a.zIndex||4}).add(),g[c].update=function(a){g.setTitle(!b&&a,b&&a)},g[c].css(a.style))});g.layOutTitles(c)},
layOutTitles:function(a){var b=0,c,g=this.renderer,e=this.spacingBox;m(["title","subtitle"],function(a){var c=this[a],h=this.options[a];a="title"===a?-3:h.verticalAlign?0:b+2;var k;c&&(k=h.style.fontSize,k=g.fontMetrics(k,c).b,c.css({width:(h.width||e.width+h.widthAdjust)+"px"}).align(d({y:a+k},h),!1,"spacingBox"),h.floating||h.verticalAlign||(b=Math.ceil(b+c.getBBox(h.useHTML).height)))},this);c=this.titleOffset!==b;this.titleOffset=b;!this.isDirtyBox&&c&&(this.isDirtyBox=c,this.hasRendered&&F(a,
!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var c=this.options.chart,g=c.width,c=c.height,d=this.renderTo;b(g)||(this.containerWidth=a.getStyle(d,"width"));b(c)||(this.containerHeight=a.getStyle(d,"height"));this.chartWidth=Math.max(0,g||this.containerWidth||600);this.chartHeight=Math.max(0,a.relativeLength(c,this.chartWidth)||(1<this.containerHeight?this.containerHeight:400))},temporaryDisplay:function(b){var c=this.renderTo;if(b)for(;c&&c.style;)c.hcOrigStyle&&(a.css(c,c.hcOrigStyle),
delete c.hcOrigStyle),c.hcOrigDetached&&(f.body.removeChild(c),c.hcOrigDetached=!1),c=c.parentNode;else for(;c&&c.style;){f.body.contains(c)||c.parentNode||(c.hcOrigDetached=!0,f.body.appendChild(c));if("none"===a.getStyle(c,"display",!1)||c.hcOricDetached)c.hcOrigStyle={display:c.style.display,height:c.style.height,overflow:c.style.overflow},b={display:"block",overflow:"hidden"},c!==this.renderTo&&(b.height=0),a.css(c,b),c.offsetWidth||c.style.setProperty("display","block","important");c=c.parentNode;
if(c===f.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var b,c=this.options,g=c.chart,e,h;b=this.renderTo;var k=a.uniqueKey(),x;b||(this.renderTo=b=g.renderTo);I(b)&&(this.renderTo=b=f.getElementById(b));b||a.error(13,!0);e=A(q(b,"data-highcharts-chart"));p(e)&&u[e]&&u[e].hasRendered&&u[e].destroy();q(b,"data-highcharts-chart",this.index);b.innerHTML="";g.skipClone||b.offsetWidth||this.temporaryDisplay();this.getChartSize();
e=this.chartWidth;h=this.chartHeight;x=d({position:"relative",overflow:"hidden",width:e+"px",height:h+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},g.style);this.container=b=t("div",{id:k},x,b);this._cursor=b.style.cursor;this.renderer=new (a[g.renderer]||a.Renderer)(b,e,h,null,g.forExport,c.exporting&&c.exporting.allowHTML);this.setClassName(g.className);this.renderer.setStyle(g.style);this.renderer.chartIndex=this.index},getMargins:function(a){var c=
this.spacing,g=this.margin,d=this.titleOffset;this.resetMargins();d&&!b(g[0])&&(this.plotTop=Math.max(this.plotTop,d+this.options.title.margin+c[0]));this.legend&&this.legend.display&&this.legend.adjustMargins(g,c);this.extraMargin&&(this[this.extraMargin.type]=(this[this.extraMargin.type]||0)+this.extraMargin.value);this.adjustPlotArea&&this.adjustPlotArea();a||this.getAxisMargins()},getAxisMargins:function(){var a=this,c=a.axisOffset=[0,0,0,0],g=a.margin;a.hasCartesianSeries&&m(a.axes,function(a){a.visible&&
a.getOffset()});m(M,function(d,e){b(g[e])||(a[d]+=c[e])});a.setChartSize()},reflow:function(c){var g=this,d=g.options.chart,e=g.renderTo,h=b(d.width)&&b(d.height),k=d.width||a.getStyle(e,"width"),d=d.height||a.getStyle(e,"height"),e=c?c.target:L;if(!h&&!g.isPrinting&&k&&d&&(e===L||e===f)){if(k!==g.containerWidth||d!==g.containerHeight)clearTimeout(g.reflowTimeout),g.reflowTimeout=w(function(){g.container&&g.setSize(void 0,void 0,!1)},c?100:0);g.containerWidth=k;g.containerHeight=d}},initReflow:function(){var a=
this,b;b=B(L,"resize",function(b){a.reflow(b)});B(a,"destroy",b)},setSize:function(b,g,d){var e=this,h=e.renderer;e.isResizing+=1;a.setAnimation(d,e);e.oldChartHeight=e.chartHeight;e.oldChartWidth=e.chartWidth;void 0!==b&&(e.options.chart.width=b);void 0!==g&&(e.options.chart.height=g);e.getChartSize();b=h.globalAnimation;(b?H:c)(e.container,{width:e.chartWidth+"px",height:e.chartHeight+"px"},b);e.setChartSize(!0);h.setSize(e.chartWidth,e.chartHeight,d);m(e.axes,function(a){a.isDirty=!0;a.setScale()});
e.isDirtyLegend=!0;e.isDirtyBox=!0;e.layOutTitles();e.getMargins();e.redraw(d);e.oldChartHeight=null;k(e,"resize");w(function(){e&&k(e,"endResize",null,function(){--e.isResizing})},E(b).duration)},setChartSize:function(a){var b=this.inverted,c=this.renderer,g=this.chartWidth,d=this.chartHeight,e=this.options.chart,h=this.spacing,k=this.clipOffset,f,x,l,p;this.plotLeft=f=Math.round(this.plotLeft);this.plotTop=x=Math.round(this.plotTop);this.plotWidth=l=Math.max(0,Math.round(g-f-this.marginRight));
this.plotHeight=p=Math.max(0,Math.round(d-x-this.marginBottom));this.plotSizeX=b?p:l;this.plotSizeY=b?l:p;this.plotBorderWidth=e.plotBorderWidth||0;this.spacingBox=c.spacingBox={x:h[3],y:h[0],width:g-h[3]-h[1],height:d-h[0]-h[2]};this.plotBox=c.plotBox={x:f,y:x,width:l,height:p};g=2*Math.floor(this.plotBorderWidth/2);b=Math.ceil(Math.max(g,k[3])/2);c=Math.ceil(Math.max(g,k[0])/2);this.clipBox={x:b,y:c,width:Math.floor(this.plotSizeX-Math.max(g,k[1])/2-b),height:Math.max(0,Math.floor(this.plotSizeY-
Math.max(g,k[2])/2-c))};a||m(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,b=a.options.chart;m(["margin","spacing"],function(c){var g=b[c],d=r(g)?g:[g,g,g,g];m(["Top","Right","Bottom","Left"],function(g,e){a[c][e]=F(b[c+g],d[e])})});m(M,function(b,c){a[b]=F(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,g=this.chartHeight,d=this.chartBackground,
e=this.plotBackground,h=this.plotBorder,f,x=this.plotBGImage,m=a.backgroundColor,l=a.plotBackgroundColor,p=a.plotBackgroundImage,A,F=this.plotLeft,n=this.plotTop,w=this.plotWidth,r=this.plotHeight,z=this.plotBox,G=this.clipRect,J=this.clipBox,q="animate";d||(this.chartBackground=d=b.rect().addClass("highcharts-background").add(),q="attr");f=a.borderWidth||0;A=f+(a.shadow?8:0);m={fill:m||"none"};if(f||d["stroke-width"])m.stroke=a.borderColor,m["stroke-width"]=f;d.attr(m).shadow(a.shadow);d[q]({x:A/
2,y:A/2,width:c-A-f%2,height:g-A-f%2,r:a.borderRadius});q="animate";e||(q="attr",this.plotBackground=e=b.rect().addClass("highcharts-plot-background").add());e[q](z);e.attr({fill:l||"none"}).shadow(a.plotShadow);p&&(x?x.animate(z):this.plotBGImage=b.image(p,F,n,w,r).add());G?G.animate({width:J.width,height:J.height}):this.clipRect=b.clipRect(J);q="animate";h||(q="attr",this.plotBorder=h=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());h.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||
0,fill:"none"});h[q](h.crisp({x:F,y:n,width:w,height:r},-h.strokeWidth()));this.isDirtyBox=!1;k(this,"afterDrawChartBox")},propFromSeries:function(){var a=this,b=a.options.chart,c,g=a.options.series,d,e;m(["inverted","angular","polar"],function(h){c=G[b.type||b.defaultSeriesType];e=b[h]||c&&c.prototype[h];for(d=g&&g.length;!e&&d--;)(c=G[g[d].type])&&c.prototype[h]&&(e=!0);a[h]=e})},linkSeries:function(){var a=this,b=a.series;m(b,function(a){a.linkedSeries.length=0});m(b,function(b){var c=b.options.linkedTo;
I(c)&&(c=":previous"===c?a.series[b.index-1]:a.get(c))&&c.linkedParent!==b&&(c.linkedSeries.push(b),b.linkedParent=c,b.visible=F(b.options.visible,c.options.visible,b.visible))})},renderSeries:function(){m(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=this,b=a.options.labels;b.items&&m(b.items,function(c){var g=d(b.style,c.style),e=A(g.left)+a.plotLeft,h=A(g.top)+a.plotTop+12;delete g.left;delete g.top;a.renderer.text(c.html,e,h).attr({zIndex:2}).css(g).add()})},
render:function(){var a=this.axes,b=this.renderer,c=this.options,g,d,e;this.setTitle();this.legend=new z(this,c.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();c=this.plotWidth;g=this.plotHeight=Math.max(this.plotHeight-21,0);m(a,function(a){a.setScale()});this.getAxisMargins();d=1.1<c/this.plotWidth;e=1.05<g/this.plotHeight;if(d||e)m(a,function(a){(a.horiz&&d||!a.horiz&&e)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&m(a,
function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=b.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var b=this;a=D(!0,this.options.credits,a);a.enabled&&!this.credits&&(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(L.location.href=a.href)}).attr({align:a.position.align,
zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},destroy:function(){var b=this,c=b.axes,g=b.series,d=b.container,e,h=d&&d.parentNode;k(b,"destroy");b.renderer.forExport?a.erase(u,b):u[b.index]=void 0;a.chartCount--;b.renderTo.removeAttribute("data-highcharts-chart");J(b);for(e=c.length;e--;)c[e]=c[e].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(e=g.length;e--;)g[e]=g[e].destroy();m("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),
function(a){var c=b[a];c&&c.destroy&&(b[a]=c.destroy())});d&&(d.innerHTML="",J(d),h&&v(d));C(b,function(a,c){delete b[c]})},firstRender:function(){var a=this,b=a.options;if(!a.isReadyToRender||a.isReadyToRender()){a.getContainer();k(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();m(b.series||[],function(b){a.initSeries(b)});a.linkSeries();k(a,"beforeRender");x&&(a.pointer=new x(a,b));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();a.temporaryDisplay(!0)}},onload:function(){m([this.callback].concat(this.callbacks),
function(a){a&&void 0!==this.index&&a.apply(this,[this])},this);k(this,"load");k(this,"render");b(this.index)&&!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}})})(K);(function(a){var B,H=a.each,E=a.extend,q=a.erase,f=a.fireEvent,l=a.format,t=a.isArray,n=a.isNumber,v=a.pick,u=a.removeEvent;a.Point=B=function(){};a.Point.prototype={init:function(a,b,m){this.series=a;this.color=a.color;this.applyOptions(b,m);a.options.colorByPoint?(b=a.options.colors||a.chart.options.colors,this.color=
this.color||b[a.colorCounter],b=b.length,m=a.colorCounter,a.colorCounter++,a.colorCounter===b&&(a.colorCounter=0)):m=a.colorIndex;this.colorIndex=v(this.colorIndex,m);a.chart.pointCount++;f(this,"afterInit");return this},applyOptions:function(a,b){var c=this.series,d=c.options.pointValKey||c.pointValKey;a=B.prototype.optionsToObject.call(this,a);E(this,a);this.options=this.options?E(this.options,a):a;a.group&&delete this.group;d&&(this.y=this[d]);this.isNull=v(this.isValid&&!this.isValid(),null===
this.x||!n(this.y,!0));this.selected&&(this.state="select");"name"in this&&void 0===b&&c.xAxis&&c.xAxis.hasNames&&(this.x=c.xAxis.nameToX(this));void 0===this.x&&c&&(this.x=void 0===b?c.autoIncrement(this):b);return this},optionsToObject:function(a){var b={},c=this.series,d=c.options.keys,h=d||c.pointArrayMap||["y"],k=h.length,e=0,f=0;if(n(a)||null===a)b[h[0]]=a;else if(t(a))for(!d&&a.length>k&&(c=typeof a[0],"string"===c?b.name=a[0]:"number"===c&&(b.x=a[0]),e++);f<k;)d&&void 0===a[e]||(b[h[f]]=a[e]),
e++,f++;else"object"===typeof a&&(b=a,a.dataLabels&&(c._hasPointLabels=!0),a.marker&&(c._hasPointMarkers=!0));return b},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?" "+this.zone.className.replace("highcharts-negative",
""):"")},getZone:function(){var a=this.series,b=a.zones,a=a.zoneAxis||"y",f=0,d;for(d=b[f];this[a]>=d.value;)d=b[++f];d&&d.color&&!this.options.color&&(this.color=d.color);return d},destroy:function(){var a=this.series.chart,b=a.hoverPoints,f;a.pointCount--;b&&(this.setState(),q(b,this),b.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)u(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);for(f in this)this[f]=null},destroyElements:function(){for(var a=
["graphic","dataLabel","dataLabelUpper","connector","shadowGroup"],b,f=6;f--;)b=a[f],this[b]&&(this[b]=this[b].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var b=this.series,c=b.tooltipOptions,d=v(c.valueDecimals,""),h=c.valuePrefix||"",k=c.valueSuffix||"";H(b.pointArrayMap||["y"],
function(b){b="{point."+b;if(h||k)a=a.replace(b+"}",h+b+"}"+k);a=a.replace(b+"}",b+":,."+d+"f}")});return l(a,{point:this,series:this.series},b.chart.time)},firePointEvent:function(a,b,m){var c=this,h=this.series.options;(h.point.events[a]||c.options&&c.options.events&&c.options.events[a])&&this.importEvents();"click"===a&&h.allowPointSelect&&(m=function(a){c.select&&c.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});f(this,a,b,m)},visible:!0}})(K);(function(a){var B=a.addEvent,H=a.animObject,E=a.arrayMax,
q=a.arrayMin,f=a.correctFloat,l=a.defaultOptions,t=a.defaultPlotOptions,n=a.defined,v=a.each,u=a.erase,c=a.extend,b=a.fireEvent,m=a.grep,d=a.isArray,h=a.isNumber,k=a.isString,e=a.merge,p=a.objectEach,r=a.pick,I=a.removeEvent,z=a.splat,M=a.SVGElement,D=a.syncTimeout,C=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",enabledThreshold:2,radius:4,states:{normal:{animation:!0},hover:{animation:{duration:50},
enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{normal:{animation:!0},hover:{animation:{duration:50},lineWidthPlus:1,marker:{},halo:{size:10,opacity:.25}},
select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var d=this,e,h=a.series,g;d.chart=a;d.options=b=d.setOptions(b);d.linkedSeries=[];d.bindAxes();c(d,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});e=b.events;p(e,function(a,b){B(d,b,a)});if(e&&e.click||b.point&&b.point.events&&
b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;d.getColor();d.getSymbol();v(d.parallelArrays,function(a){d[a+"Data"]=[]});d.setData(b.data,!1);d.isCartesian&&(a.hasCartesianSeries=!0);h.length&&(g=h[h.length-1]);d._i=r(g&&g._i,-1)+1;a.orderSeries(this.insert(h))},insert:function(a){var b=this.options.index,c;if(h(b)){for(c=a.length;c--;)if(b>=r(a[c].options.index,a[c]._i)){a.splice(c+1,0,this);break}-1===c&&a.unshift(this);c+=1}else a.push(this);return r(c,a.length-1)},bindAxes:function(){var b=
this,c=b.options,d=b.chart,e;v(b.axisTypes||[],function(h){v(d[h],function(a){e=a.options;if(c[h]===e.index||void 0!==c[h]&&c[h]===e.id||void 0===c[h]&&0===e.index)b.insert(a.series),b[h]=a,a.isDirty=!0});b[h]||b.optionalAxis===h||a.error(18,!0)})},updateParallelArrays:function(a,b){var c=a.series,d=arguments,e=h(b)?function(g){var d="y"===g&&c.toYData?c.toYData(a):a[g];c[g+"Data"][b]=d}:function(a){Array.prototype[b].apply(c[a+"Data"],Array.prototype.slice.call(d,2))};v(c.parallelArrays,e)},autoIncrement:function(){var a=
this.options,b=this.xIncrement,c,d=a.pointIntervalUnit,e=this.chart.time,b=r(b,a.pointStart,0);this.pointInterval=c=r(this.pointInterval,a.pointInterval,1);d&&(a=new e.Date(b),"day"===d?e.set("Date",a,e.get("Date",a)+c):"month"===d?e.set("Month",a,e.get("Month",a)+c):"year"===d&&e.set("FullYear",a,e.get("FullYear",a)+c),c=a.getTime()-b);this.xIncrement=b+c;return b},setOptions:function(a){var b=this.chart,c=b.options,d=c.plotOptions,h=(b.userOptions||{}).plotOptions||{},g=d[this.type];this.userOptions=
a;b=e(g,d.series,a);this.tooltipOptions=e(l.tooltip,l.plotOptions.series&&l.plotOptions.series.tooltip,l.plotOptions[this.type].tooltip,c.tooltip.userOptions,d.series&&d.series.tooltip,d[this.type].tooltip,a.tooltip);this.stickyTracking=r(a.stickyTracking,h[this.type]&&h[this.type].stickyTracking,h.series&&h.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:b.stickyTracking);null===g.marker&&delete b.marker;this.zoneAxis=b.zoneAxis;a=this.zones=(b.zones||[]).slice();!b.negativeColor&&
!b.negativeFillColor||b.zones||a.push({value:b[this.zoneAxis+"Threshold"]||b.threshold||0,className:"highcharts-negative",color:b.negativeColor,fillColor:b.negativeFillColor});a.length&&n(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return b},getName:function(){return this.name||"Series "+(this.index+1)},getCyclic:function(a,b,c){var d,e=this.chart,g=this.userOptions,h=a+"Index",k=a+"Counter",f=c?c.length:r(e.options.chart[a+"Count"],e[a+"Count"]);b||(d=r(g[h],g["_"+h]),
n(d)||(e.series.length||(e[k]=0),g["_"+h]=d=e[k]%f,e[k]+=1),c&&(b=c[d]));void 0!==d&&(this[h]=d);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||t[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(b,c,e,f){var m=this,g=m.points,l=g&&g.length||0,x,p=m.options,F=
m.chart,n=null,A=m.xAxis,z=p.turboThreshold,q=this.xData,D=this.yData,t=(x=m.pointArrayMap)&&x.length;b=b||[];x=b.length;c=r(c,!0);if(!1!==f&&x&&l===x&&!m.cropped&&!m.hasGroupedData&&m.visible)v(b,function(a,b){g[b].update&&a!==p.data[b]&&g[b].update(a,!1,null,!1)});else{m.xIncrement=null;m.colorCounter=0;v(this.parallelArrays,function(a){m[a+"Data"].length=0});if(z&&x>z){for(e=0;null===n&&e<x;)n=b[e],e++;if(h(n))for(e=0;e<x;e++)q[e]=this.autoIncrement(),D[e]=b[e];else if(d(n))if(t)for(e=0;e<x;e++)n=
b[e],q[e]=n[0],D[e]=n.slice(1,t+1);else for(e=0;e<x;e++)n=b[e],q[e]=n[0],D[e]=n[1];else a.error(12)}else for(e=0;e<x;e++)void 0!==b[e]&&(n={series:m},m.pointClass.prototype.applyOptions.apply(n,[b[e]]),m.updateParallelArrays(n,e));D&&k(D[0])&&a.error(14,!0);m.data=[];m.options.data=m.userOptions.data=b;for(e=l;e--;)g[e]&&g[e].destroy&&g[e].destroy();A&&(A.minRange=A.userMinRange);m.isDirty=F.isDirtyBox=!0;m.isDirtyData=!!g;e=!1}"point"===p.legendType&&(this.processData(),this.generatePoints());c&&
F.redraw(e)},processData:function(b){var c=this.xData,d=this.yData,e=c.length,h;h=0;var g,k,f=this.xAxis,m,l=this.options;m=l.cropThreshold;var x=this.getExtremesFromAll||l.getExtremesFromAll,p=this.isCartesian,l=f&&f.val2lin,n=f&&f.isLog,r=this.requireSorting,z,q;if(p&&!this.isDirty&&!f.isDirty&&!this.yAxis.isDirty&&!b)return!1;f&&(b=f.getExtremes(),z=b.min,q=b.max);if(p&&this.sorted&&!x&&(!m||e>m||this.forceCrop))if(c[e-1]<z||c[0]>q)c=[],d=[];else if(c[0]<z||c[e-1]>q)h=this.cropData(this.xData,
this.yData,z,q),c=h.xData,d=h.yData,h=h.start,g=!0;for(m=c.length||1;--m;)e=n?l(c[m])-l(c[m-1]):c[m]-c[m-1],0<e&&(void 0===k||e<k)?k=e:0>e&&r&&(a.error(15),r=!1);this.cropped=g;this.cropStart=h;this.processedXData=c;this.processedYData=d;this.closestPointRange=k},cropData:function(a,b,c,d){var e=a.length,g=0,h=e,k=r(this.cropShoulder,1),f;for(f=0;f<e;f++)if(a[f]>=c){g=Math.max(0,f-k);break}for(c=f;c<e;c++)if(a[c]>d){h=c+k;break}return{xData:a.slice(g,h),yData:b.slice(g,h),start:g,end:h}},generatePoints:function(){var a=
this.options,b=a.data,c=this.data,d,e=this.processedXData,g=this.processedYData,h=this.pointClass,k=e.length,f=this.cropStart||0,m,l=this.hasGroupedData,a=a.keys,p,n=[],r;c||l||(c=[],c.length=b.length,c=this.data=c);a&&l&&(this.options.keys=!1);for(r=0;r<k;r++)m=f+r,l?(p=(new h).init(this,[e[r]].concat(z(g[r]))),p.dataGroup=this.groupMap[r]):(p=c[m])||void 0===b[m]||(c[m]=p=(new h).init(this,b[m],e[r])),p&&(p.index=m,n[r]=p);this.options.keys=a;if(c&&(k!==(d=c.length)||l))for(r=0;r<d;r++)r!==f||l||
(r+=k),c[r]&&(c[r].destroyElements(),c[r].plotX=void 0);this.data=c;this.points=n},getExtremes:function(a){var b=this.yAxis,c=this.processedXData,e,k=[],g=0;e=this.xAxis.getExtremes();var f=e.min,m=e.max,l,p,x,n;a=a||this.stackedYData||this.processedYData||[];e=a.length;for(n=0;n<e;n++)if(p=c[n],x=a[n],l=(h(x,!0)||d(x))&&(!b.positiveValuesOnly||x.length||0<x),p=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(c[n+1]||p)>=f&&(c[n-1]||p)<=m,l&&p)if(l=x.length)for(;l--;)"number"===
typeof x[l]&&(k[g++]=x[l]);else k[g++]=x;this.dataMin=q(k);this.dataMax=E(k)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,c=a.stacking,e=this.xAxis,d=e.categories,k=this.yAxis,g=this.points,m=g.length,l=!!this.modifyValue,p=a.pointPlacement,z="between"===p||h(p),q=a.threshold,D=a.startFromThreshold?q:0,t,C,v,u,M=Number.MAX_VALUE;"between"===p&&(p=.5);h(p)&&(p*=r(a.pointRange||e.pointRange));for(a=0;a<m;a++){var I=g[a],B=I.x,E=I.y;C=I.low;var H=
c&&k.stacks[(this.negStacks&&E<(D?0:q)?"-":"")+this.stackKey],K;k.positiveValuesOnly&&null!==E&&0>=E&&(I.isNull=!0);I.plotX=t=f(Math.min(Math.max(-1E5,e.translate(B,0,0,0,1,p,"flags"===this.type)),1E5));c&&this.visible&&!I.isNull&&H&&H[B]&&(u=this.getStackIndicator(u,B,this.index),K=H[B],E=K.points[u.key],C=E[0],E=E[1],C===D&&u.key===H[B].base&&(C=r(q,k.min)),k.positiveValuesOnly&&0>=C&&(C=null),I.total=I.stackTotal=K.total,I.percentage=K.total&&I.y/K.total*100,I.stackY=E,K.setOffset(this.pointXOffset||
0,this.barW||0));I.yBottom=n(C)?Math.min(Math.max(-1E5,k.translate(C,0,1,0,1)),1E5):null;l&&(E=this.modifyValue(E,I));I.plotY=C="number"===typeof E&&Infinity!==E?Math.min(Math.max(-1E5,k.translate(E,0,1,0,1)),1E5):void 0;I.isInside=void 0!==C&&0<=C&&C<=k.len&&0<=t&&t<=e.len;I.clientX=z?f(e.translate(B,0,0,0,1,p)):t;I.negative=I.y<(q||0);I.category=d&&void 0!==d[I.x]?d[I.x]:I.x;I.isNull||(void 0!==v&&(M=Math.min(M,Math.abs(t-v))),v=t);I.zone=this.zones.length&&I.getZone()}this.closestPointRangePx=
M;b(this,"afterTranslate")},getValidPoints:function(a,b){var c=this.chart;return m(a||this.points||[],function(a){return b&&!c.isInsidePlot(a.plotX,a.plotY,c.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,c=this.options,e=b.renderer,d=b.inverted,g=this.clipBox,h=g||b.clipBox,k=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,h.height,c.xAxis,c.yAxis].join(),f=b[k],m=b[k+"m"];f||(a&&(h.width=0,d&&(h.x=b.plotSizeX),b[k+"m"]=m=e.clipRect(d?b.plotSizeX+99:-99,d?-b.plotLeft:
-b.plotTop,99,d?b.chartWidth:b.chartHeight)),b[k]=f=e.clipRect(h),f.count={length:0});a&&!f.count[this.index]&&(f.count[this.index]=!0,f.count.length+=1);!1!==c.clip&&(this.group.clip(a||g?f:b.clipRect),this.markerGroup.clip(m),this.sharedClipKey=k);a||(f.count[this.index]&&(delete f.count[this.index],--f.count.length),0===f.count.length&&k&&b[k]&&(g||(b[k]=b[k].destroy()),b[k+"m"]&&(b[k+"m"]=b[k+"m"].destroy())))},animate:function(a){var b=this.chart,c=H(this.options.animation),e;a?this.setClip(c):
(e=this.sharedClipKey,(a=b[e])&&a.animate({width:b.plotSizeX,x:0},c),b[e+"m"]&&b[e+"m"].animate({width:b.plotSizeX+99,x:0},c),this.animate=null)},afterAnimate:function(){this.setClip();b(this,"afterAnimate");this.finishedAnimating=!0},drawPoints:function(){var a=this.points,b=this.chart,c,e,d,g,h=this.options.marker,k,f,m,l=this[this.specialGroup]||this.markerGroup,p,n=r(h.enabled,this.xAxis.isRadial?!0:null,this.closestPointRangePx>=h.enabledThreshold*h.radius);if(!1!==h.enabled||this._hasPointMarkers)for(c=
0;c<a.length;c++)e=a[c],g=e.graphic,k=e.marker||{},f=!!e.marker,d=n&&void 0===k.enabled||k.enabled,m=e.isInside,d&&!e.isNull?(d=r(k.symbol,this.symbol),p=this.markerAttribs(e,e.selected&&"select"),g?g[m?"show":"hide"](!0).animate(p):m&&(0<p.width||e.hasImage)&&(e.graphic=g=b.renderer.symbol(d,p.x,p.y,p.width,p.height,f?k:h).add(l)),g&&g.attr(this.pointAttribs(e,e.selected&&"select")),g&&g.addClass(e.getClassName(),!0)):g&&(e.graphic=g.destroy())},markerAttribs:function(a,b){var c=this.options.marker,
e=a.marker||{},d=e.symbol||c.symbol,g=r(e.radius,c.radius);b&&(c=c.states[b],b=e.states&&e.states[b],g=r(b&&b.radius,c&&c.radius,g+(c&&c.radiusPlus||0)));a.hasImage=d&&0===d.indexOf("url");a.hasImage&&(g=0);a={x:Math.floor(a.plotX)-g,y:a.plotY-g};g&&(a.width=a.height=2*g);return a},pointAttribs:function(a,b){var c=this.options.marker,e=a&&a.options,d=e&&e.marker||{},g=this.color,h=e&&e.color,k=a&&a.color,e=r(d.lineWidth,c.lineWidth);a=a&&a.zone&&a.zone.color;g=h||a||k||g;a=d.fillColor||c.fillColor||
g;g=d.lineColor||c.lineColor||g;b&&(c=c.states[b],b=d.states&&d.states[b]||{},e=r(b.lineWidth,c.lineWidth,e+r(b.lineWidthPlus,c.lineWidthPlus,0)),a=b.fillColor||c.fillColor||a,g=b.lineColor||c.lineColor||g);return{stroke:g,"stroke-width":e,fill:a}},destroy:function(){var a=this,c=a.chart,e=/AppleWebKit\/533/.test(C.navigator.userAgent),d,h,g=a.data||[],k,f;b(a,"destroy");I(a);v(a.axisTypes||[],function(b){(f=a[b])&&f.series&&(u(f.series,a),f.isDirty=f.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);
for(h=g.length;h--;)(k=g[h])&&k.destroy&&k.destroy();a.points=null;clearTimeout(a.animationTimeout);p(a,function(a,b){a instanceof M&&!a.survive&&(d=e&&"group"===b?"hide":"destroy",a[d]())});c.hoverSeries===a&&(c.hoverSeries=null);u(c.series,a);c.orderSeries();p(a,function(b,c){delete a[c]})},getGraphPath:function(a,b,c){var e=this,d=e.options,g=d.step,h,k=[],f=[],m;a=a||e.points;(h=a.reversed)&&a.reverse();(g={right:1,center:2}[g]||g&&3)&&h&&(g=4-g);!d.connectNulls||b||c||(a=this.getValidPoints(a));
v(a,function(h,l){var p=h.plotX,x=h.plotY,r=a[l-1];(h.leftCliff||r&&r.rightCliff)&&!c&&(m=!0);h.isNull&&!n(b)&&0<l?m=!d.connectNulls:h.isNull&&!b?m=!0:(0===l||m?l=["M",h.plotX,h.plotY]:e.getPointSpline?l=e.getPointSpline(a,h,l):g?(l=1===g?["L",r.plotX,x]:2===g?["L",(r.plotX+p)/2,r.plotY,"L",(r.plotX+p)/2,x]:["L",p,r.plotY],l.push("L",p,x)):l=["L",p,x],f.push(h.x),g&&f.push(h.x),k.push.apply(k,l),m=!1)});k.xMap=f;return e.graphPath=k},drawGraph:function(){var a=this,b=this.options,c=(this.gappedPath||
this.getGraphPath).call(this),e=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];v(this.zones,function(c,d){e.push(["zone-graph-"+d,"highcharts-graph highcharts-zone-graph-"+d+" "+(c.className||""),c.color||a.color,c.dashStyle||b.dashStyle])});v(e,function(e,d){var g=e[0],h=a[g];h?(h.endX=a.preventGraphAnimation?null:c.xMap,h.animate({d:c})):c.length&&(a[g]=a.chart.renderer.path(c).addClass(e[1]).attr({zIndex:1}).add(a.group),h={stroke:e[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&
a.color||"none"},e[3]?h.dashstyle=e[3]:"square"!==b.linecap&&(h["stroke-linecap"]=h["stroke-linejoin"]="round"),h=a[g].attr(h).shadow(2>d&&b.shadow));h&&(h.startX=c.xMap,h.isArea=c.isArea)})},applyZones:function(){var a=this,b=this.chart,c=b.renderer,e=this.zones,d,g,h=this.clips||[],k,f=this.graph,m=this.area,l=Math.max(b.chartWidth,b.chartHeight),p=this[(this.zoneAxis||"y")+"Axis"],n,z,q=b.inverted,D,t,C,u,M=!1;e.length&&(f||m)&&p&&void 0!==p.min&&(z=p.reversed,D=p.horiz,f&&f.hide(),m&&m.hide(),
n=p.getExtremes(),v(e,function(e,x){d=z?D?b.plotWidth:0:D?0:p.toPixels(n.min);d=Math.min(Math.max(r(g,d),0),l);g=Math.min(Math.max(Math.round(p.toPixels(r(e.value,n.max),!0)),0),l);M&&(d=g=p.toPixels(n.max));t=Math.abs(d-g);C=Math.min(d,g);u=Math.max(d,g);p.isXAxis?(k={x:q?u:C,y:0,width:t,height:l},D||(k.x=b.plotHeight-k.x)):(k={x:0,y:q?u:C,width:l,height:t},D&&(k.y=b.plotWidth-k.y));q&&c.isVML&&(k=p.isXAxis?{x:0,y:z?C:u,height:k.width,width:b.chartWidth}:{x:k.y-b.plotLeft-b.spacingBox.x,y:0,width:k.height,
height:b.chartHeight});h[x]?h[x].animate(k):(h[x]=c.clipRect(k),f&&a["zone-graph-"+x].clip(h[x]),m&&a["zone-area-"+x].clip(h[x]));M=e.value>n.max}),this.clips=h)},invertGroups:function(a){function b(){v(["group","markerGroup"],function(b){c[b]&&(e.renderer.isVML&&c[b].attr({width:c.yAxis.len,height:c.xAxis.len}),c[b].width=c.yAxis.len,c[b].height=c.xAxis.len,c[b].invert(a))})}var c=this,e=c.chart,d;c.xAxis&&(d=B(e,"resize",b),B(c,"destroy",d),b(a),c.invertGroups=b)},plotGroup:function(a,b,c,e,d){var g=
this[a],h=!g;h&&(this[a]=g=this.chart.renderer.g().attr({zIndex:e||.1}).add(d));g.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series "+(n(this.colorIndex)?"highcharts-color-"+this.colorIndex+" ":"")+(this.options.className||"")+(g.hasClass("highcharts-tracker")?" highcharts-tracker":""),!0);g.attr({visibility:c})[h?"attr":"animate"](this.getPlotBox());return g},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;a.inverted&&(b=c,c=this.xAxis);
return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,c=a.chart,e,d=a.options,h=!!a.animate&&c.renderer.isSVG&&H(d.animation).duration,g=a.visible?"inherit":"hidden",k=d.zIndex,f=a.hasRendered,m=c.seriesGroup,l=c.inverted;e=a.plotGroup("group","series",g,k,m);a.markerGroup=a.plotGroup("markerGroup","markers",g,k,m);h&&a.animate(!0);e.inverted=a.isCartesian?l:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();
a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(l);!1===d.clip||a.sharedClipKey||f||e.clip(c.clipRect);h&&a.animate();f||(a.animationTimeout=D(function(){a.afterAnimate()},h));a.isDirty=!1;a.hasRendered=!0;b(a,"afterRender")},redraw:function(){var a=this.chart,b=this.isDirty||this.isDirtyData,c=this.group,e=this.xAxis,d=this.yAxis;c&&(a.inverted&&c.attr({width:a.plotWidth,height:a.plotHeight}),c.animate({translateX:r(e&&e.left,a.plotLeft),
translateY:r(d&&d.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var c=this.xAxis,e=this.yAxis,d=this.chart.inverted;return this.searchKDTree({clientX:d?c.len-a.chartY+c.pos:a.chartX-c.pos,plotY:d?e.len-a.chartX+e.pos:a.chartY-e.pos},b)},buildKDTree:function(){function a(c,e,d){var g,h;if(h=c&&c.length)return g=b.kdAxisArray[e%d],c.sort(function(a,b){return a[g]-b[g]}),h=Math.floor(h/2),{point:c[h],left:a(c.slice(0,
h),e+1,d),right:a(c.slice(h+1),e+1,d)}}this.buildingKdTree=!0;var b=this,c=-1<b.options.findNearestPointBy.indexOf("y")?2:1;delete b.kdTree;D(function(){b.kdTree=a(b.getValidPoints(null,!b.directTouch),c,c);b.buildingKdTree=!1},b.options.kdNow?0:1)},searchKDTree:function(a,b){function c(a,b,k,f){var m=b.point,l=e.kdAxisArray[k%f],p,r,z=m;r=n(a[d])&&n(m[d])?Math.pow(a[d]-m[d],2):null;p=n(a[g])&&n(m[g])?Math.pow(a[g]-m[g],2):null;p=(r||0)+(p||0);m.dist=n(p)?Math.sqrt(p):Number.MAX_VALUE;m.distX=n(r)?
Math.sqrt(r):Number.MAX_VALUE;l=a[l]-m[l];p=0>l?"left":"right";r=0>l?"right":"left";b[p]&&(p=c(a,b[p],k+1,f),z=p[h]<z[h]?p:m);b[r]&&Math.sqrt(l*l)<z[h]&&(a=c(a,b[r],k+1,f),z=a[h]<z[h]?a:z);return z}var e=this,d=this.kdAxisArray[0],g=this.kdAxisArray[1],h=b?"distX":"dist";b=-1<e.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||this.buildingKdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,b,b)}})})(K);(function(a){var B=a.Axis,H=a.Chart,E=a.correctFloat,q=a.defined,f=a.destroyObjectProperties,
l=a.each,t=a.format,n=a.objectEach,v=a.pick,u=a.Series;a.StackItem=function(a,b,f,d,h){var c=a.chart.inverted;this.axis=a;this.isNegative=f;this.options=b;this.x=d;this.total=null;this.points={};this.stack=h;this.rightCliff=this.leftCliff=0;this.alignOptions={align:b.align||(c?f?"left":"right":"center"),verticalAlign:b.verticalAlign||(c?"middle":f?"bottom":"top"),y:v(b.y,c?4:f?14:-6),x:v(b.x,c?f?-6:6:0)};this.textAlign=b.textAlign||(c?f?"right":"left":"center")};a.StackItem.prototype={destroy:function(){f(this,
this.axis)},render:function(a){var b=this.axis.chart,c=this.options,d=c.format,d=d?t(d,this,b.time):c.formatter.call(this);this.label?this.label.attr({text:d,visibility:"hidden"}):this.label=b.renderer.text(d,null,null,c.useHTML).css(c.style).attr({align:this.textAlign,rotation:c.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,b){var c=this.axis,d=c.chart,h=c.translate(c.usePercentage?100:this.total,0,0,0,1),c=c.translate(0),c=Math.abs(h-c);a=d.xAxis[0].translate(this.x)+a;h=this.getStackBox(d,
this,a,h,b,c);if(b=this.label)b.align(this.alignOptions,null,h),h=b.alignAttr,b[!1===this.options.crop||d.isInsidePlot(h.x,h.y)?"show":"hide"](!0)},getStackBox:function(a,b,f,d,h,k){var c=b.axis.reversed,m=a.inverted;a=a.plotHeight;b=b.isNegative&&!c||!b.isNegative&&c;return{x:m?b?d:d-k:f,y:m?a-f-h:b?a-d-k:a-d,width:m?k:h,height:m?h:k}}};H.prototype.getStacks=function(){var a=this;l(a.yAxis,function(a){a.stacks&&a.hasVisibleSeries&&(a.oldStacks=a.stacks)});l(a.series,function(b){!b.options.stacking||
!0!==b.visible&&!1!==a.options.chart.ignoreHiddenSeries||(b.stackKey=b.type+v(b.options.stack,""))})};B.prototype.buildStacks=function(){var a=this.series,b=v(this.options.reversedStacks,!0),f=a.length,d;if(!this.isXAxis){this.usePercentage=!1;for(d=f;d--;)a[b?d:f-d-1].setStackedPoints();for(d=0;d<f;d++)a[d].modifyStacks()}};B.prototype.renderStackTotals=function(){var a=this.chart,b=a.renderer,f=this.stacks,d=this.stackTotalGroup;d||(this.stackTotalGroup=d=b.g("stack-labels").attr({visibility:"visible",
zIndex:6}).add());d.translate(a.plotLeft,a.plotTop);n(f,function(a){n(a,function(a){a.render(d)})})};B.prototype.resetStacks=function(){var a=this,b=a.stacks;a.isXAxis||n(b,function(b){n(b,function(c,h){c.touched<a.stacksTouched?(c.destroy(),delete b[h]):(c.total=null,c.cumulative=null)})})};B.prototype.cleanStacks=function(){var a;this.isXAxis||(this.oldStacks&&(a=this.stacks=this.oldStacks),n(a,function(a){n(a,function(a){a.cumulative=a.total})}))};u.prototype.setStackedPoints=function(){if(this.options.stacking&&
(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var c=this.processedXData,b=this.processedYData,f=[],d=b.length,h=this.options,k=h.threshold,e=v(h.startFromThreshold&&k,0),l=h.stack,h=h.stacking,n=this.stackKey,t="-"+n,z=this.negStacks,u=this.yAxis,D=u.stacks,C=u.oldStacks,x,F,A,J,G,g,w;u.stacksTouched+=1;for(G=0;G<d;G++)g=c[G],w=b[G],x=this.getStackIndicator(x,g,this.index),J=x.key,A=(F=z&&w<(e?0:k))?t:n,D[A]||(D[A]={}),D[A][g]||(C[A]&&C[A][g]?(D[A][g]=C[A][g],D[A][g].total=
null):D[A][g]=new a.StackItem(u,u.options.stackLabels,F,g,l)),A=D[A][g],null!==w?(A.points[J]=A.points[this.index]=[v(A.cumulative,e)],q(A.cumulative)||(A.base=J),A.touched=u.stacksTouched,0<x.index&&!1===this.singleStacks&&(A.points[J][0]=A.points[this.index+","+g+",0"][0])):A.points[J]=A.points[this.index]=null,"percent"===h?(F=F?n:t,z&&D[F]&&D[F][g]?(F=D[F][g],A.total=F.total=Math.max(F.total,A.total)+Math.abs(w)||0):A.total=E(A.total+(Math.abs(w)||0))):A.total=E(A.total+(w||0)),A.cumulative=v(A.cumulative,
e)+(w||0),null!==w&&(A.points[J].push(A.cumulative),f[G]=A.cumulative);"percent"===h&&(u.usePercentage=!0);this.stackedYData=f;u.oldStacks={}}};u.prototype.modifyStacks=function(){var a=this,b=a.stackKey,f=a.yAxis.stacks,d=a.processedXData,h,k=a.options.stacking;a[k+"Stacker"]&&l([b,"-"+b],function(b){for(var c=d.length,e,l;c--;)if(e=d[c],h=a.getStackIndicator(h,e,a.index,b),l=(e=f[b]&&f[b][e])&&e.points[h.key])a[k+"Stacker"](l,e,c)})};u.prototype.percentStacker=function(a,b,f){b=b.total?100/b.total:
0;a[0]=E(a[0]*b);a[1]=E(a[1]*b);this.stackedYData[f]=a[1]};u.prototype.getStackIndicator=function(a,b,f,d){!q(a)||a.x!==b||d&&a.key!==d?a={x:b,index:0,key:d}:a.index++;a.key=[f,b,a.index].join();return a}})(K);(function(a){var B=a.addEvent,H=a.animate,E=a.Axis,q=a.createElement,f=a.css,l=a.defined,t=a.each,n=a.erase,v=a.extend,u=a.fireEvent,c=a.inArray,b=a.isNumber,m=a.isObject,d=a.isArray,h=a.merge,k=a.objectEach,e=a.pick,p=a.Point,r=a.Series,I=a.seriesTypes,z=a.setAnimation,M=a.splat;v(a.Chart.prototype,
{addSeries:function(a,b,c){var d,h=this;a&&(b=e(b,!0),u(h,"addSeries",{options:a},function(){d=h.initSeries(a);h.isDirtyLegend=!0;h.linkSeries();b&&h.redraw(c)}));return d},addAxis:function(a,b,c,d){var k=b?"xAxis":"yAxis",f=this.options;a=h(a,{index:this[k].length,isX:b});b=new E(this,a);f[k]=M(f[k]||{});f[k].push(a);e(c,!0)&&this.redraw(d);return b},showLoading:function(a){var b=this,c=b.options,e=b.loadingDiv,d=c.loading,h=function(){e&&f(e,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+
"px",height:b.plotHeight+"px"})};e||(b.loadingDiv=e=q("div",{className:"highcharts-loading highcharts-loading-hidden"},null,b.container),b.loadingSpan=q("span",{className:"highcharts-loading-inner"},null,e),B(b,"redraw",h));e.className="highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;f(e,v(d.style,{zIndex:10}));f(b.loadingSpan,d.labelStyle);b.loadingShown||(f(e,{opacity:0,display:""}),H(e,{opacity:d.style.opacity||.5},{duration:d.showDuration||0}));b.loadingShown=!0;h()},hideLoading:function(){var a=
this.options,b=this.loadingDiv;b&&(b.className="highcharts-loading highcharts-loading-hidden",H(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){f(b,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions time tooltip".split(" "),update:function(a,d,f){var p=this,m={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},n=a.chart,r,g,z=[];if(n){h(!0,p.options.chart,n);"className"in n&&p.setClassName(n.className);if("inverted"in n||"polar"in n)p.propFromSeries(),r=!0;"alignTicks"in n&&(r=!0);k(n,function(a,b){-1!==c("chart."+b,p.propsRequireUpdateSeries)&&(g=!0);-1!==c(b,p.propsRequireDirtyBox)&&
(p.isDirtyBox=!0)});"style"in n&&p.renderer.setStyle(n.style)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&h(!0,this.options.plotOptions,a.plotOptions);k(a,function(a,b){if(p[b]&&"function"===typeof p[b].update)p[b].update(a,!1);else if("function"===typeof p[m[b]])p[m[b]](a);"chart"!==b&&-1!==c(b,p.propsRequireUpdateSeries)&&(g=!0)});t("xAxis yAxis zAxis series colorAxis pane".split(" "),function(b){a[b]&&(t(M(a[b]),function(a,c){(c=l(a.id)&&p.get(a.id)||p[b][c])&&c.coll===b&&(c.update(a,
!1),f&&(c.touched=!0));if(!c&&f)if("series"===b)p.addSeries(a,!1).touched=!0;else if("xAxis"===b||"yAxis"===b)p.addAxis(a,"xAxis"===b,!1).touched=!0}),f&&t(p[b],function(a){a.touched?delete a.touched:z.push(a)}))});t(z,function(a){a.remove(!1)});r&&t(p.axes,function(a){a.update({},!1)});g&&t(p.series,function(a){a.update({},!1)});a.loading&&h(!0,p.options.loading,a.loading);r=n&&n.width;n=n&&n.height;b(r)&&r!==p.chartWidth||b(n)&&n!==p.chartHeight?p.setSize(r,n):e(d,!0)&&p.redraw()},setSubtitle:function(a){this.setTitle(void 0,
a)}});v(p.prototype,{update:function(a,b,c,d){function h(){k.applyOptions(a);null===k.y&&g&&(k.graphic=g.destroy());m(a,!0)&&(g&&g.element&&a&&a.marker&&void 0!==a.marker.symbol&&(k.graphic=g.destroy()),a&&a.dataLabels&&k.dataLabel&&(k.dataLabel=k.dataLabel.destroy()),k.connector&&(k.connector=k.connector.destroy()));p=k.index;f.updateParallelArrays(k,p);n.data[p]=m(n.data[p],!0)||m(a,!0)?k.options:a;f.isDirty=f.isDirtyData=!0;!f.fixedBox&&f.hasCartesianSeries&&(l.isDirtyBox=!0);"point"===n.legendType&&
(l.isDirtyLegend=!0);b&&l.redraw(c)}var k=this,f=k.series,g=k.graphic,p,l=f.chart,n=f.options;b=e(b,!0);!1===d?h():k.firePointEvent("update",{options:a},h)},remove:function(a,b){this.series.removePoint(c(this,this.series.data),a,b)}});v(r.prototype,{addPoint:function(a,b,c,d){var h=this.options,k=this.data,f=this.chart,g=this.xAxis,g=g&&g.hasNames&&g.names,p=h.data,l,m,n=this.xData,r,z;b=e(b,!0);l={series:this};this.pointClass.prototype.applyOptions.apply(l,[a]);z=l.x;r=n.length;if(this.requireSorting&&
z<n[r-1])for(m=!0;r&&n[r-1]>z;)r--;this.updateParallelArrays(l,"splice",r,0,0);this.updateParallelArrays(l,r);g&&l.name&&(g[z]=l.name);p.splice(r,0,a);m&&(this.data.splice(r,0,null),this.processData());"point"===h.legendType&&this.generatePoints();c&&(k[0]&&k[0].remove?k[0].remove(!1):(k.shift(),this.updateParallelArrays(l,"shift"),p.shift()));this.isDirtyData=this.isDirty=!0;b&&f.redraw(d)},removePoint:function(a,b,c){var d=this,h=d.data,k=h[a],f=d.points,g=d.chart,p=function(){f&&f.length===h.length&&
f.splice(a,1);h.splice(a,1);d.options.data.splice(a,1);d.updateParallelArrays(k||{series:d},"splice",a,1);k&&k.destroy();d.isDirty=!0;d.isDirtyData=!0;b&&g.redraw()};z(c,g);b=e(b,!0);k?k.firePointEvent("remove",null,p):p()},remove:function(a,b,c){function d(){h.destroy();k.isDirtyLegend=k.isDirtyBox=!0;k.linkSeries();e(a,!0)&&k.redraw(b)}var h=this,k=h.chart;!1!==c?u(h,"remove",null,d):d()},update:function(a,b){var c=this,d=c.chart,k=c.userOptions,f=c.oldType||c.type,p=a.type||k.type||d.options.chart.type,
g=I[f].prototype,l,m=["group","markerGroup","dataLabelsGroup"],n=["navigatorSeries","baseSeries"],r=c.finishedAnimating&&{animation:!1};if(Object.keys&&"data"===Object.keys(a).toString())return this.setData(a.data,b);n=m.concat(n);t(n,function(a){n[a]=c[a];delete c[a]});a=h(k,r,{index:c.index,pointStart:c.xData[0]},{data:c.options.data},a);c.remove(!1,null,!1);for(l in g)c[l]=void 0;v(c,I[p||f].prototype);t(n,function(a){c[a]=n[a]});c.init(d,a);a.zIndex!==k.zIndex&&t(m,function(b){c[b]&&c[b].attr({zIndex:a.zIndex})});
c.oldType=f;d.linkSeries();e(b,!0)&&d.redraw(!1)}});v(E.prototype,{update:function(a,b){var c=this.chart;a=c.options[this.coll][this.options.index]=h(this.userOptions,a);this.destroy(!0);this.init(c,v(a,{events:void 0}));c.isDirtyBox=!0;e(b,!0)&&c.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,h=this.series,k=h.length;k--;)h[k]&&h[k].remove(!1);n(b.axes,this);n(b[c],this);d(b.options[c])?b.options[c].splice(this.options.index,1):delete b.options[c];t(b[c],function(a,b){a.options.index=
b});this.destroy();b.isDirtyBox=!0;e(a,!0)&&b.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})})(K);(function(a){var B=a.color,H=a.each,E=a.map,q=a.pick,f=a.Series,l=a.seriesType;l("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(f){var l=[],t=[],u=this.xAxis,c=this.yAxis,b=c.stacks[this.stackKey],m={},d=this.index,h=c.series,k=h.length,e,p=q(c.options.reversedStacks,!0)?1:-1,r;f=f||this.points;
if(this.options.stacking){for(r=0;r<f.length;r++)f[r].leftNull=f[r].rightNull=null,m[f[r].x]=f[r];a.objectEach(b,function(a,b){null!==a.total&&t.push(b)});t.sort(function(a,b){return a-b});e=E(h,function(){return this.visible});H(t,function(a,h){var f=0,n,z;if(m[a]&&!m[a].isNull)l.push(m[a]),H([-1,1],function(c){var f=1===c?"rightNull":"leftNull",l=0,q=b[t[h+c]];if(q)for(r=d;0<=r&&r<k;)n=q.points[r],n||(r===d?m[a][f]=!0:e[r]&&(z=b[a].points[r])&&(l-=z[1]-z[0])),r+=p;m[a][1===c?"rightCliff":"leftCliff"]=
l});else{for(r=d;0<=r&&r<k;){if(n=b[a].points[r]){f=n[1];break}r+=p}f=c.translate(f,0,1,0,1);l.push({isNull:!0,plotX:u.translate(a,0,0,0,1),x:a,plotY:f,yBottom:f})}})}return l},getGraphPath:function(a){var l=f.prototype.getGraphPath,t=this.options,u=t.stacking,c=this.yAxis,b,m,d=[],h=[],k=this.index,e,p=c.stacks[this.stackKey],r=t.threshold,I=c.getThreshold(t.threshold),z,t=t.connectNulls||"percent"===u,M=function(b,f,l){var m=a[b];b=u&&p[m.x].points[k];var n=m[l+"Null"]||0;l=m[l+"Cliff"]||0;var z,
q,m=!0;l||n?(z=(n?b[0]:b[1])+l,q=b[0]+l,m=!!n):!u&&a[f]&&a[f].isNull&&(z=q=r);void 0!==z&&(h.push({plotX:e,plotY:null===z?I:c.getThreshold(z),isNull:m,isCliff:!0}),d.push({plotX:e,plotY:null===q?I:c.getThreshold(q),doCurve:!1}))};a=a||this.points;u&&(a=this.getStackPoints(a));for(b=0;b<a.length;b++)if(m=a[b].isNull,e=q(a[b].rectPlotX,a[b].plotX),z=q(a[b].yBottom,I),!m||t)t||M(b,b-1,"left"),m&&!u&&t||(h.push(a[b]),d.push({x:b,plotX:e,plotY:z})),t||M(b,b+1,"right");b=l.call(this,h,!0,!0);d.reversed=
!0;m=l.call(this,d,!0,!0);m.length&&(m[0]="L");m=b.concat(m);l=l.call(this,h,!1,t);m.xMap=b.xMap;this.areaPath=m;return l},drawGraph:function(){this.areaPath=[];f.prototype.drawGraph.apply(this);var a=this,l=this.areaPath,v=this.options,u=[["area","highcharts-area",this.color,v.fillColor]];H(this.zones,function(c,b){u.push(["zone-area-"+b,"highcharts-area highcharts-zone-area-"+b+" "+c.className,c.color||a.color,c.fillColor||v.fillColor])});H(u,function(c){var b=c[0],f=a[b];f?(f.endX=a.preventGraphAnimation?
null:l.xMap,f.animate({d:l})):(f=a[b]=a.chart.renderer.path(l).addClass(c[1]).attr({fill:q(c[3],B(c[2]).setOpacity(q(v.fillOpacity,.75)).get()),zIndex:0}).add(a.group),f.isArea=!0);f.startX=l.xMap;f.shiftUnit=v.step?2:1})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(K);(function(a){var B=a.pick;a=a.seriesType;a("spline","line",{},{getPointSpline:function(a,E,q){var f=E.plotX,l=E.plotY,t=a[q-1];q=a[q+1];var n,v,u,c;if(t&&!t.isNull&&!1!==t.doCurve&&!E.isCliff&&q&&!q.isNull&&!1!==q.doCurve&&
!E.isCliff){a=t.plotY;u=q.plotX;q=q.plotY;var b=0;n=(1.5*f+t.plotX)/2.5;v=(1.5*l+a)/2.5;u=(1.5*f+u)/2.5;c=(1.5*l+q)/2.5;u!==n&&(b=(c-v)*(u-f)/(u-n)+l-c);v+=b;c+=b;v>a&&v>l?(v=Math.max(a,l),c=2*l-v):v<a&&v<l&&(v=Math.min(a,l),c=2*l-v);c>q&&c>l?(c=Math.max(q,l),v=2*l-c):c<q&&c<l&&(c=Math.min(q,l),v=2*l-c);E.rightContX=u;E.rightContY=c}E=["C",B(t.rightContX,t.plotX),B(t.rightContY,t.plotY),B(n,f),B(v,l),f,l];t.rightContX=t.rightContY=null;return E}})})(K);(function(a){var B=a.seriesTypes.area.prototype,
H=a.seriesType;H("areaspline","spline",a.defaultPlotOptions.area,{getStackPoints:B.getStackPoints,getGraphPath:B.getGraphPath,drawGraph:B.drawGraph,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(K);(function(a){var B=a.animObject,H=a.color,E=a.each,q=a.extend,f=a.isNumber,l=a.merge,t=a.pick,n=a.Series,v=a.seriesType,u=a.svg;v("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1},
select:{color:"#cccccc",borderColor:"#000000"}},dataLabels:{align:null,verticalAlign:null,y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){n.prototype.init.apply(this,arguments);var a=this,b=a.chart;b.hasRendered&&E(b.series,function(b){b.type===a.type&&(b.isDirty=!0)})},getColumnMetrics:function(){var a=this,b=a.options,f=a.xAxis,
d=a.yAxis,h=f.reversed,k,e={},l=0;!1===b.grouping?l=1:E(a.chart.series,function(b){var c=b.options,h=b.yAxis,f;b.type!==a.type||!b.visible&&a.chart.options.chart.ignoreHiddenSeries||d.len!==h.len||d.pos!==h.pos||(c.stacking?(k=b.stackKey,void 0===e[k]&&(e[k]=l++),f=e[k]):!1!==c.grouping&&(f=l++),b.columnIndex=f)});var n=Math.min(Math.abs(f.transA)*(f.ordinalSlope||b.pointRange||f.closestPointRange||f.tickInterval||1),f.len),q=n*b.groupPadding,z=(n-2*q)/(l||1),b=Math.min(b.maxPointWidth||f.len,t(b.pointWidth,
z*(1-2*b.pointPadding)));a.columnMetrics={width:b,offset:(z-b)/2+(q+((a.columnIndex||0)+(h?1:0))*z-n/2)*(h?-1:1)};return a.columnMetrics},crispCol:function(a,b,f,d){var c=this.chart,k=this.borderWidth,e=-(k%2?.5:0),k=k%2?.5:1;c.inverted&&c.renderer.isVML&&(k+=1);this.options.crisp&&(f=Math.round(a+f)+e,a=Math.round(a)+e,f-=a);d=Math.round(b+d)+k;e=.5>=Math.abs(b)&&.5<d;b=Math.round(b)+k;d-=b;e&&d&&(--b,d+=1);return{x:a,y:b,width:f,height:d}},translate:function(){var a=this,b=a.chart,f=a.options,d=
a.dense=2>a.closestPointRange*a.xAxis.transA,d=a.borderWidth=t(f.borderWidth,d?0:1),h=a.yAxis,k=f.threshold,e=a.translatedThreshold=h.getThreshold(k),l=t(f.minPointLength,5),r=a.getColumnMetrics(),q=r.width,z=a.barW=Math.max(q,1+2*d),u=a.pointXOffset=r.offset;b.inverted&&(e-=.5);f.pointPadding&&(z=Math.ceil(z));n.prototype.translate.apply(a);E(a.points,function(c){var d=t(c.yBottom,e),f=999+Math.abs(d),f=Math.min(Math.max(-f,c.plotY),h.len+f),p=c.plotX+u,m=z,n=Math.min(f,d),r,g=Math.max(f,d)-n;l&&
Math.abs(g)<l&&(g=l,r=!h.reversed&&!c.negative||h.reversed&&c.negative,c.y===k&&a.dataMax<=k&&h.min<k&&(r=!r),n=Math.abs(n-e)>l?d-l:e-(r?l:0));c.barX=p;c.pointWidth=q;c.tooltipPos=b.inverted?[h.len+h.pos-b.plotLeft-f,a.xAxis.len-p-m/2,g]:[p+m/2,f+h.pos-b.plotTop,g];c.shapeType="rect";c.shapeArgs=a.crispCol.apply(a,c.isNull?[p,e,m,0]:[p,n,m,g])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},
pointAttribs:function(a,b){var c=this.options,d,h=this.pointAttrToOptions||{};d=h.stroke||"borderColor";var f=h["stroke-width"]||"borderWidth",e=a&&a.color||this.color,p=a&&a[d]||c[d]||this.color||e,n=a&&a[f]||c[f]||this[f]||0,h=c.dashStyle;a&&this.zones.length&&(e=a.getZone(),e=a.options.color||e&&e.color||this.color);b&&(a=l(c.states[b],a.options.states&&a.options.states[b]||{}),b=a.brightness,e=a.color||void 0!==b&&H(e).brighten(a.brightness).get()||e,p=a[d]||p,n=a[f]||n,h=a.dashStyle||h);d={fill:e,
stroke:p,"stroke-width":n};h&&(d.dashstyle=h);return d},drawPoints:function(){var a=this,b=this.chart,m=a.options,d=b.renderer,h=m.animationLimit||250,k;E(a.points,function(c){var e=c.graphic;if(f(c.plotY)&&null!==c.y){k=c.shapeArgs;if(e)e[b.pointCount<h?"animate":"attr"](l(k));else c.graphic=e=d[c.shapeType](k).add(c.group||a.group);m.borderRadius&&e.attr({r:m.borderRadius});e.attr(a.pointAttribs(c,c.selected&&"select")).shadow(m.shadow,null,m.stacking&&!m.borderRadius);e.addClass(c.getClassName(),
!0)}else e&&(c.graphic=e.destroy())})},animate:function(a){var b=this,c=this.yAxis,d=b.options,h=this.chart.inverted,f={},e=h?"translateX":"translateY",l;u&&(a?(f.scaleY=.001,a=Math.min(c.pos+c.len,Math.max(c.pos,c.toPixels(d.threshold))),h?f.translateX=a-c.len:f.translateY=a,b.group.attr(f)):(l=b.group.attr(e),b.group.animate({scaleY:1},q(B(b.options.animation),{step:function(a,d){f[e]=l+d.pos*(c.pos-l);b.group.attr(f)}})),b.animate=null))},remove:function(){var a=this,b=a.chart;b.hasRendered&&E(b.series,
function(b){b.type===a.type&&(b.isDirty=!0)});n.prototype.remove.apply(a,arguments)}})})(K);(function(a){a=a.seriesType;a("bar","column",null,{inverted:!0})})(K);(function(a){var B=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,findNearestPointBy:"xy",marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},
{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&B.prototype.drawGraph.call(this)}})})(K);(function(a){var B=a.deg2rad,H=a.isNumber,E=a.pick,q=a.relativeLength;a.CenteredSeriesMixin={getCenter:function(){var a=this.options,l=this.chart,t=2*(a.slicedOffset||0),n=l.plotWidth-2*t,l=l.plotHeight-2*t,v=a.center,v=[E(v[0],"50%"),E(v[1],"50%"),a.size||"100%",a.innerSize||0],u=Math.min(n,
l),c,b;for(c=0;4>c;++c)b=v[c],a=2>c||2===c&&/%$/.test(b),v[c]=q(b,[n,l,u,v[2]][c])+(a?t:0);v[3]>v[2]&&(v[3]=v[2]);return v},getStartAndEndRadians:function(a,l){a=H(a)?a:0;l=H(l)&&l>a&&360>l-a?l:a+360;return{start:B*(a+-90),end:B*(l+-90)}}}})(K);(function(a){var B=a.addEvent,H=a.CenteredSeriesMixin,E=a.defined,q=a.each,f=a.extend,l=H.getStartAndEndRadians,t=a.inArray,n=a.noop,v=a.pick,u=a.Point,c=a.Series,b=a.seriesType,m=a.setAnimation;b("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,
enabled:!0,formatter:function(){return this.point.isNull?void 0:this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,states:{hover:{brightness:.1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:a.seriesTypes.column.prototype.pointAttribs,animate:function(a){var b=this,
c=b.points,d=b.startAngleRad;a||(q(c,function(a){var c=a.graphic,e=a.shapeArgs;c&&(c.attr({r:a.startR||b.center[3]/2,start:d,end:d}),c.animate({r:e.r,start:e.start,end:e.end},b.options.animation))}),b.animate=null)},updateTotals:function(){var a,b=0,c=this.points,e=c.length,f,l=this.options.ignoreHiddenPoint;for(a=0;a<e;a++)f=c[a],b+=l&&!f.visible?0:f.isNull?0:f.y;this.total=b;for(a=0;a<e;a++)f=c[a],f.percentage=0<b&&(f.visible||!l)?f.y/b*100:0,f.total=b},generatePoints:function(){c.prototype.generatePoints.call(this);
this.updateTotals()},translate:function(a){this.generatePoints();var b=0,c=this.options,e=c.slicedOffset,d=e+(c.borderWidth||0),f,m,n,q=l(c.startAngle,c.endAngle),t=this.startAngleRad=q.start,q=(this.endAngleRad=q.end)-t,u=this.points,x,F=c.dataLabels.distance,c=c.ignoreHiddenPoint,A,B=u.length,G;a||(this.center=a=this.getCenter());this.getX=function(b,c,e){n=Math.asin(Math.min((b-a[1])/(a[2]/2+e.labelDistance),1));return a[0]+(c?-1:1)*Math.cos(n)*(a[2]/2+e.labelDistance)};for(A=0;A<B;A++){G=u[A];
G.labelDistance=v(G.options.dataLabels&&G.options.dataLabels.distance,F);this.maxLabelDistance=Math.max(this.maxLabelDistance||0,G.labelDistance);f=t+b*q;if(!c||G.visible)b+=G.percentage/100;m=t+b*q;G.shapeType="arc";G.shapeArgs={x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:Math.round(1E3*f)/1E3,end:Math.round(1E3*m)/1E3};n=(m+f)/2;n>1.5*Math.PI?n-=2*Math.PI:n<-Math.PI/2&&(n+=2*Math.PI);G.slicedTranslation={translateX:Math.round(Math.cos(n)*e),translateY:Math.round(Math.sin(n)*e)};m=Math.cos(n)*a[2]/
2;x=Math.sin(n)*a[2]/2;G.tooltipPos=[a[0]+.7*m,a[1]+.7*x];G.half=n<-Math.PI/2||n>Math.PI/2?1:0;G.angle=n;f=Math.min(d,G.labelDistance/5);G.labelPos=[a[0]+m+Math.cos(n)*G.labelDistance,a[1]+x+Math.sin(n)*G.labelDistance,a[0]+m+Math.cos(n)*f,a[1]+x+Math.sin(n)*f,a[0]+m,a[1]+x,0>G.labelDistance?"center":G.half?"right":"left",n]}},drawGraph:null,drawPoints:function(){var a=this,b=a.chart.renderer,c,e,l,m,n=a.options.shadow;n&&!a.shadowGroup&&(a.shadowGroup=b.g("shadow").add(a.group));q(a.points,function(d){e=
d.graphic;if(d.isNull)e&&(d.graphic=e.destroy());else{m=d.shapeArgs;c=d.getTranslate();var h=d.shadowGroup;n&&!h&&(h=d.shadowGroup=b.g("shadow").add(a.shadowGroup));h&&h.attr(c);l=a.pointAttribs(d,d.selected&&"select");e?e.setRadialReference(a.center).attr(l).animate(f(m,c)):(d.graphic=e=b[d.shapeType](m).setRadialReference(a.center).attr(c).add(a.group),d.visible||e.attr({visibility:"hidden"}),e.attr(l).attr({"stroke-linejoin":"round"}).shadow(n,h));e.addClass(d.getClassName())}})},searchPoint:n,
sortByAngle:function(a,b){a.sort(function(a,c){return void 0!==a.angle&&(c.angle-a.angle)*b})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,getCenter:H.getCenter,getSymbol:n},{init:function(){u.prototype.init.apply(this,arguments);var a=this,b;a.name=v(a.name,"Slice");b=function(b){a.slice("select"===b.type)};B(a,"select",b);B(a,"unselect",b);return a},isValid:function(){return a.isNumber(this.y,!0)&&0<=this.y},setVisible:function(a,b){var c=this,e=c.series,d=e.chart,h=e.options.ignoreHiddenPoint;
b=v(b,h);a!==c.visible&&(c.visible=c.options.visible=a=void 0===a?!c.visible:a,e.options.data[t(c,e.data)]=c.options,q(["graphic","dataLabel","connector","shadowGroup"],function(b){if(c[b])c[b][a?"show":"hide"](!0)}),c.legendItem&&d.legend.colorizeItem(c,a),a||"hover"!==c.state||c.setState(""),h&&(e.isDirty=!0),b&&d.redraw())},slice:function(a,b,c){var e=this.series;m(c,e.chart);v(b,!0);this.sliced=this.options.sliced=E(a)?a:!this.sliced;e.options.data[t(this,e.data)]=this.options;this.graphic.animate(this.getTranslate());
this.shadowGroup&&this.shadowGroup.animate(this.getTranslate())},getTranslate:function(){return this.sliced?this.slicedTranslation:{translateX:0,translateY:0}},haloPath:function(a){var b=this.shapeArgs;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(b.x,b.y,b.r+a,b.r+a,{innerR:this.shapeArgs.r-1,start:b.start,end:b.end})}})})(K);(function(a){var B=a.addEvent,H=a.arrayMax,E=a.defined,q=a.each,f=a.extend,l=a.format,t=a.map,n=a.merge,v=a.noop,u=a.pick,c=a.relativeLength,b=
a.Series,m=a.seriesTypes,d=a.stableSort;a.distribute=function(a,b){function c(a,b){return a.target-b.target}var f,h=!0,k=a,l=[],m;m=0;for(f=a.length;f--;)m+=a[f].size;if(m>b){d(a,function(a,b){return(b.rank||0)-(a.rank||0)});for(m=f=0;m<=b;)m+=a[f].size,f++;l=a.splice(f-1,a.length)}d(a,c);for(a=t(a,function(a){return{size:a.size,targets:[a.target],align:u(a.align,.5)}});h;){for(f=a.length;f--;)h=a[f],m=(Math.min.apply(0,h.targets)+Math.max.apply(0,h.targets))/2,h.pos=Math.min(Math.max(0,m-h.size*
h.align),b-h.size);f=a.length;for(h=!1;f--;)0<f&&a[f-1].pos+a[f-1].size>a[f].pos&&(a[f-1].size+=a[f].size,a[f-1].targets=a[f-1].targets.concat(a[f].targets),a[f-1].align=.5,a[f-1].pos+a[f-1].size>b&&(a[f-1].pos=b-a[f-1].size),a.splice(f,1),h=!0)}f=0;q(a,function(a){var b=0;q(a.targets,function(){k[f].pos=a.pos+b;b+=k[f].size;f++})});k.push.apply(k,l);d(k,c)};b.prototype.drawDataLabels=function(){function b(a,b){var c=b.filter;return c?(b=c.operator,a=a[c.property],c=c.value,"\x3e"===b&&a>c||"\x3c"===
b&&a<c||"\x3e\x3d"===b&&a>=c||"\x3c\x3d"===b&&a<=c||"\x3d\x3d"===b&&a==c||"\x3d\x3d\x3d"===b&&a===c?!0:!1):!0}var c=this,e=c.chart,d=c.options,f=d.dataLabels,m=c.points,z,t,v=c.hasRendered||0,C,x,F=u(f.defer,!!d.animation),A=e.renderer;if(f.enabled||c._hasPointLabels)c.dlProcessOptions&&c.dlProcessOptions(f),x=c.plotGroup("dataLabelsGroup","data-labels",F&&!v?"hidden":"visible",f.zIndex||6),F&&(x.attr({opacity:+v}),v||B(c,"afterAnimate",function(){c.visible&&x.show(!0);x[d.animation?"animate":"attr"]({opacity:1},
{duration:200})})),t=f,q(m,function(h){var k,g=h.dataLabel,m,p,q=h.connector,r=!g,v;z=h.dlOptions||h.options&&h.options.dataLabels;(k=u(z&&z.enabled,t.enabled)&&!h.isNull)&&(k=!0===b(h,z||f));k&&(f=n(t,z),m=h.getLabelConfig(),v=f[h.formatPrefix+"Format"]||f.format,C=E(v)?l(v,m,e.time):(f[h.formatPrefix+"Formatter"]||f.formatter).call(m,f),v=f.style,m=f.rotation,v.color=u(f.color,v.color,c.color,"#000000"),"contrast"===v.color&&(h.contrastColor=A.getContrast(h.color||c.color),v.color=f.inside||0>u(h.labelDistance,
f.distance)||d.stacking?h.contrastColor:"#000000"),d.cursor&&(v.cursor=d.cursor),p={fill:f.backgroundColor,stroke:f.borderColor,"stroke-width":f.borderWidth,r:f.borderRadius||0,rotation:m,padding:f.padding,zIndex:1},a.objectEach(p,function(a,b){void 0===a&&delete p[b]}));!g||k&&E(C)?k&&E(C)&&(g?p.text=C:(g=h.dataLabel=m?A.text(C,0,-9999).addClass("highcharts-data-label"):A.label(C,0,-9999,f.shape,null,null,f.useHTML,null,"data-label"),g.addClass(" highcharts-data-label-color-"+h.colorIndex+" "+(f.className||
"")+(f.useHTML?"highcharts-tracker":""))),g.attr(p),g.css(v).shadow(f.shadow),g.added||g.add(x),c.alignDataLabel(h,g,f,null,r)):(h.dataLabel=g=g.destroy(),q&&(h.connector=q.destroy()))});a.fireEvent(this,"afterDrawDataLabels")};b.prototype.alignDataLabel=function(a,b,c,d,l){var e=this.chart,h=e.inverted,k=u(a.dlBox&&a.dlBox.centerX,a.plotX,-9999),m=u(a.plotY,-9999),n=b.getBBox(),p,q=c.rotation,r=c.align,t=this.visible&&(a.series.forceDL||e.isInsidePlot(k,Math.round(m),h)||d&&e.isInsidePlot(k,h?d.x+
1:d.y+d.height-1,h)),v="justify"===u(c.overflow,"justify");if(t&&(p=c.style.fontSize,p=e.renderer.fontMetrics(p,b).b,d=f({x:h?this.yAxis.len-m:k,y:Math.round(h?this.xAxis.len-k:m),width:0,height:0},d),f(c,{width:n.width,height:n.height}),q?(v=!1,k=e.renderer.rotCorr(p,q),k={x:d.x+c.x+d.width/2+k.x,y:d.y+c.y+{top:0,middle:.5,bottom:1}[c.verticalAlign]*d.height},b[l?"attr":"animate"](k).attr({align:r}),m=(q+720)%360,m=180<m&&360>m,"left"===r?k.y-=m?n.height:0:"center"===r?(k.x-=n.width/2,k.y-=n.height/
2):"right"===r&&(k.x-=n.width,k.y-=m?0:n.height)):(b.align(c,null,d),k=b.alignAttr),v?a.isLabelJustified=this.justifyDataLabel(b,c,k,n,d,l):u(c.crop,!0)&&(t=e.isInsidePlot(k.x,k.y)&&e.isInsidePlot(k.x+n.width,k.y+n.height)),c.shape&&!q))b[l?"attr":"animate"]({anchorX:h?e.plotWidth-a.plotY:a.plotX,anchorY:h?e.plotHeight-a.plotX:a.plotY});t||(b.attr({y:-9999}),b.placed=!1)};b.prototype.justifyDataLabel=function(a,b,c,d,f,l){var e=this.chart,h=b.align,k=b.verticalAlign,m,n,p=a.box?0:a.padding||0;m=c.x+
p;0>m&&("right"===h?b.align="left":b.x=-m,n=!0);m=c.x+d.width-p;m>e.plotWidth&&("left"===h?b.align="right":b.x=e.plotWidth-m,n=!0);m=c.y+p;0>m&&("bottom"===k?b.verticalAlign="top":b.y=-m,n=!0);m=c.y+d.height-p;m>e.plotHeight&&("top"===k?b.verticalAlign="bottom":b.y=e.plotHeight-m,n=!0);n&&(a.placed=!l,a.align(b,null,f));return n};m.pie&&(m.pie.prototype.drawDataLabels=function(){var c=this,d=c.data,e,f=c.chart,l=c.options.dataLabels,m=u(l.connectorPadding,10),n=u(l.connectorWidth,1),t=f.plotWidth,
v=f.plotHeight,C,x=c.center,F=x[2]/2,A=x[1],B,G,g,w,L=[[],[]],P,N,O,K,y=[0,0,0,0];c.visible&&(l.enabled||c._hasPointLabels)&&(q(d,function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),b.prototype.drawDataLabels.apply(c),q(d,function(a){a.dataLabel&&a.visible&&(L[a.half].push(a),a.dataLabel._pos=null)}),q(L,function(b,d){var h,k,n=b.length,p=[],r;if(n)for(c.sortByAngle(b,d-.5),0<c.maxLabelDistance&&
(h=Math.max(0,A-F-c.maxLabelDistance),k=Math.min(A+F+c.maxLabelDistance,f.plotHeight),q(b,function(a){0<a.labelDistance&&a.dataLabel&&(a.top=Math.max(0,A-F-a.labelDistance),a.bottom=Math.min(A+F+a.labelDistance,f.plotHeight),r=a.dataLabel.getBBox().height||21,a.positionsIndex=p.push({target:a.labelPos[1]-a.top+r/2,size:r,rank:a.y})-1)}),a.distribute(p,k+r-h)),K=0;K<n;K++)e=b[K],k=e.positionsIndex,g=e.labelPos,B=e.dataLabel,O=!1===e.visible?"hidden":"inherit",N=h=g[1],p&&E(p[k])&&(void 0===p[k].pos?
O="hidden":(w=p[k].size,N=e.top+p[k].pos)),delete e.positionIndex,P=l.justify?x[0]+(d?-1:1)*(F+e.labelDistance):c.getX(N<e.top+2||N>e.bottom-2?h:N,d,e),B._attr={visibility:O,align:g[6]},B._pos={x:P+l.x+({left:m,right:-m}[g[6]]||0),y:N+l.y-10},g.x=P,g.y=N,u(l.crop,!0)&&(G=B.getBBox().width,h=null,P-G<m?(h=Math.round(G-P+m),y[3]=Math.max(h,y[3])):P+G>t-m&&(h=Math.round(P+G-t+m),y[1]=Math.max(h,y[1])),0>N-w/2?y[0]=Math.max(Math.round(-N+w/2),y[0]):N+w/2>v&&(y[2]=Math.max(Math.round(N+w/2-v),y[2])),B.sideOverflow=
h)}),0===H(y)||this.verifyDataLabelOverflow(y))&&(this.placeDataLabels(),n&&q(this.points,function(a){var b;C=a.connector;if((B=a.dataLabel)&&B._pos&&a.visible&&0<a.labelDistance){O=B._attr.visibility;if(b=!C)a.connector=C=f.renderer.path().addClass("highcharts-data-label-connector  highcharts-color-"+a.colorIndex).add(c.dataLabelsGroup),C.attr({"stroke-width":n,stroke:l.connectorColor||a.color||"#666666"});C[b?"attr":"animate"]({d:c.connectorPath(a.labelPos)});C.attr("visibility",O)}else C&&(a.connector=
C.destroy())}))},m.pie.prototype.connectorPath=function(a){var b=a.x,c=a.y;return u(this.options.dataLabels.softConnector,!0)?["M",b+("left"===a[6]?5:-5),c,"C",b,c,2*a[2]-a[4],2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",b+("left"===a[6]?5:-5),c,"L",a[2],a[3],"L",a[4],a[5]]},m.pie.prototype.placeDataLabels=function(){q(this.points,function(a){var b=a.dataLabel;b&&a.visible&&((a=b._pos)?(b.sideOverflow&&(b._attr.width=b.getBBox().width-b.sideOverflow,b.css({width:b._attr.width+"px",textOverflow:"ellipsis"}),
b.shortened=!0),b.attr(b._attr),b[b.moved?"animate":"attr"](a),b.moved=!0):b&&b.attr({y:-9999}))},this)},m.pie.prototype.alignDataLabel=v,m.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,e=this.options,d=e.center,f=e.minSize||80,h,l=null!==e.size;l||(null!==d[0]?h=Math.max(b[2]-Math.max(a[1],a[3]),f):(h=Math.max(b[2]-a[1]-a[3],f),b[0]+=(a[3]-a[1])/2),null!==d[1]?h=Math.max(Math.min(h,b[2]-Math.max(a[0],a[2])),f):(h=Math.max(Math.min(h,b[2]-a[0]-a[2]),f),b[1]+=(a[0]-a[2])/2),h<
b[2]?(b[2]=h,b[3]=Math.min(c(e.innerSize||0,h),h),this.translate(b),this.drawDataLabels&&this.drawDataLabels()):l=!0);return l});m.column&&(m.column.prototype.alignDataLabel=function(a,c,d,f,l){var e=this.chart.inverted,h=a.series,k=a.dlBox||a.shapeArgs,m=u(a.below,a.plotY>u(this.translatedThreshold,h.yAxis.len)),p=u(d.inside,!!this.options.stacking);k&&(f=n(k),0>f.y&&(f.height+=f.y,f.y=0),k=f.y+f.height-h.yAxis.len,0<k&&(f.height-=k),e&&(f={x:h.yAxis.len-f.y-f.height,y:h.xAxis.len-f.x-f.width,width:f.height,
height:f.width}),p||(e?(f.x+=m?0:f.width,f.width=0):(f.y+=m?f.height:0,f.height=0)));d.align=u(d.align,!e||p?"center":m?"right":"left");d.verticalAlign=u(d.verticalAlign,e||p?"middle":m?"top":"bottom");b.prototype.alignDataLabel.call(this,a,c,d,f,l);a.isLabelJustified&&a.contrastColor&&a.dataLabel.css({color:a.contrastColor})})})(K);(function(a){var B=a.Chart,H=a.each,E=a.objectEach,q=a.pick;a=a.addEvent;a(B.prototype,"render",function(){var a=[];H(this.labelCollectors||[],function(f){a=a.concat(f())});
H(this.yAxis||[],function(f){f.options.stackLabels&&!f.options.stackLabels.allowOverlap&&E(f.stacks,function(f){E(f,function(f){a.push(f.label)})})});H(this.series||[],function(f){var l=f.options.dataLabels,n=f.dataLabelCollections||["dataLabel"];(l.enabled||f._hasPointLabels)&&!l.allowOverlap&&f.visible&&H(n,function(l){H(f.points,function(f){f[l]&&(f[l].labelrank=q(f.labelrank,f.shapeArgs&&f.shapeArgs.height),a.push(f[l]))})})});this.hideOverlappingLabels(a)});B.prototype.hideOverlappingLabels=
function(a){var f=a.length,q,n,v,u,c,b,m,d,h,k=function(a,b,c,d,f,h,k,l){return!(f>a+c||f+k<a||h>b+d||h+l<b)};for(n=0;n<f;n++)if(q=a[n])q.oldOpacity=q.opacity,q.newOpacity=1,q.width||(v=q.getBBox(),q.width=v.width,q.height=v.height);a.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(n=0;n<f;n++)for(v=a[n],q=n+1;q<f;++q)if(u=a[q],v&&u&&v!==u&&v.placed&&u.placed&&0!==v.newOpacity&&0!==u.newOpacity&&(c=v.alignAttr,b=u.alignAttr,m=v.parentGroup,d=u.parentGroup,h=2*(v.box?0:v.padding||
0),c=k(c.x+m.translateX,c.y+m.translateY,v.width-h,v.height-h,b.x+d.translateX,b.y+d.translateY,u.width-h,u.height-h)))(v.labelrank<u.labelrank?v:u).newOpacity=0;H(a,function(a){var b,c;a&&(c=a.newOpacity,a.oldOpacity!==c&&a.placed&&(c?a.show(!0):b=function(){a.hide()},a.alignAttr.opacity=c,a[a.isOld?"animate":"attr"](a.alignAttr,null,b)),a.isOld=!0)})}})(K);(function(a){var B=a.addEvent,H=a.Chart,E=a.createElement,q=a.css,f=a.defaultOptions,l=a.defaultPlotOptions,t=a.each,n=a.extend,v=a.fireEvent,
u=a.hasTouch,c=a.inArray,b=a.isObject,m=a.Legend,d=a.merge,h=a.pick,k=a.Point,e=a.Series,p=a.seriesTypes,r=a.svg,I;I=a.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart.pointer,c=function(a){var c=b.getPointFromEvent(a);void 0!==c&&(b.isDirectTouch=!0,c.onMouseOver(a))};t(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(t(a.trackerGroups,function(d){if(a[d]){a[d].addClass("highcharts-tracker").on("mouseover",
c).on("mouseout",function(a){b.onTrackerMouseOut(a)});if(u)a[d].on("touchstart",c);a.options.cursor&&a[d].css(q).css({cursor:a.options.cursor})}}),a._hasTracking=!0);v(this,"afterDrawTracker")},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,d=[].concat(c?a.areaPath:a.graphPath),e=d.length,f=a.chart,h=f.pointer,k=f.renderer,l=f.options.tooltip.snap,g=a.tracker,m,n=function(){if(f.hoverSeries!==a)a.onMouseOver()},p="rgba(192,192,192,"+(r?.0001:.002)+")";if(e&&!c)for(m=e+1;m--;)"M"===
d[m]&&d.splice(m+1,0,d[m+1]-l,d[m+2],"L"),(m&&"M"===d[m]||m===e)&&d.splice(m,0,"L",d[m-2]+l,d[m-1]);g?g.attr({d:d}):a.graph&&(a.tracker=k.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:p,fill:c?p:"none","stroke-width":a.graph.strokeWidth()+(c?0:2*l),zIndex:2}).add(a.group),t([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",n).on("mouseout",function(a){h.onTrackerMouseOut(a)});b.cursor&&a.css({cursor:b.cursor});if(u)a.on("touchstart",
n)}));v(this,"afterDrawTracker")}};p.column&&(p.column.prototype.drawTracker=I.drawTrackerPoint);p.pie&&(p.pie.prototype.drawTracker=I.drawTrackerPoint);p.scatter&&(p.scatter.prototype.drawTracker=I.drawTrackerPoint);n(m.prototype,{setItemEvents:function(a,b,c){var e=this,f=e.chart.renderer.boxWrapper,h="highcharts-legend-"+(a instanceof k?"point":"series")+"-active";(c?b:a.legendGroup).on("mouseover",function(){a.setState("hover");f.addClass(h);b.css(e.options.itemHoverStyle)}).on("mouseout",function(){b.css(d(a.visible?
e.itemStyle:e.itemHiddenStyle));f.removeClass(h);a.setState()}).on("click",function(b){var c=function(){a.setVisible&&a.setVisible()};f.removeClass(h);b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):v(a,"legendItemClick",b,c)})},createCheckboxForItem:function(a){a.checkbox=E("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);B(a.checkbox,"click",function(b){v(a.series||a,"checkboxClick",{checked:b.target.checked,
item:a},function(){a.select()})})}});f.legend.itemStyle.cursor="pointer";n(H.prototype,{showResetZoom:function(){function a(){b.zoomOut()}var b=this,c=f.lang,d=b.options.chart.resetZoomButton,e=d.theme,h=e.states,k="chart"===d.relativeTo?null:"plotBox";v(this,"beforeShowResetZoom",null,function(){b.resetZoomButton=b.renderer.button(c.resetZoom,null,null,a,e,h&&h.hover).attr({align:d.position.align,title:c.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(d.position,!1,k)})},zoomOut:function(){var a=
this;v(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var c,d=this.pointer,e=!1,f;!a||a.resetSelection?(t(this.axes,function(a){c=a.zoom()}),d.initiated=!1):t(a.xAxis.concat(a.yAxis),function(a){var b=a.axis;d[b.isXAxis?"zoomX":"zoomY"]&&(c=b.zoom(a.min,a.max),b.displayBtn&&(e=!0))});f=this.resetZoomButton;e&&!f?this.showResetZoom():!e&&b(f)&&(this.resetZoomButton=f.destroy());c&&this.redraw(h(this.options.chart.animation,a&&a.animation,100>this.pointCount))},pan:function(a,
b){var c=this,d=c.hoverPoints,e;d&&t(d,function(a){a.setState()});t("xy"===b?[1,0]:[1],function(b){b=c[b?"xAxis":"yAxis"][0];var d=b.horiz,f=a[d?"chartX":"chartY"],d=d?"mouseDownX":"mouseDownY",h=c[d],g=(b.pointRange||0)/2,k=b.getExtremes(),l=b.toValue(h-f,!0)+g,m=b.toValue(h+b.len-f,!0)-g,n=m<l,h=n?m:l,l=n?l:m,m=Math.min(k.dataMin,g?k.min:b.toValue(b.toPixels(k.min)-b.minPixelPadding)),g=Math.max(k.dataMax,g?k.max:b.toValue(b.toPixels(k.max)+b.minPixelPadding)),n=m-h;0<n&&(l+=n,h=m);n=l-g;0<n&&(l=
g,h-=n);b.series.length&&h!==k.min&&l!==k.max&&(b.setExtremes(h,l,!1,!1,{trigger:"pan"}),e=!0);c[d]=f});e&&c.redraw(!1);q(c.container,{cursor:"move"})}});n(k.prototype,{select:function(a,b){var d=this,e=d.series,f=e.chart;a=h(a,!d.selected);d.firePointEvent(a?"select":"unselect",{accumulate:b},function(){d.selected=d.options.selected=a;e.options.data[c(d,e.data)]=d.options;d.setState(a&&"select");b||t(f.getSelectedPoints(),function(a){a.selected&&a!==d&&(a.selected=a.options.selected=!1,e.options.data[c(a,
e.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a){var b=this.series.chart,c=b.pointer;a=a?c.normalize(a):c.getChartCoordinatesFromPoint(this,b.inverted);c.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");t(a.hoverPoints||[],function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var b=this,c=d(b.series.options.point,b.options).events;b.events=c;a.objectEach(c,
function(a,c){B(b,c,a)});this.hasImportedEvents=!0}},setState:function(a,b){var c=Math.floor(this.plotX),d=this.plotY,e=this.series,f=e.options.states[a||"normal"]||{},k=l[e.type].marker&&e.options.marker,m=k&&!1===k.enabled,p=k&&k.states&&k.states[a||"normal"]||{},g=!1===p.enabled,q=e.stateMarkerGraphic,r=this.marker||{},t=e.chart,u=e.halo,z,B=k&&e.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===f.enabled||a&&(g||m&&!1===p.enabled)||a&&r.states&&r.states[a]&&!1===
r.states[a].enabled)){B&&(z=e.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.animate(e.pointAttribs(this,a),h(t.options.chart.animation,f.animation)),z&&this.graphic.animate(z,h(t.options.chart.animation,p.animation,k.animation)),q&&q.hide();else{if(a&&p){k=r.symbol||e.symbol;q&&q.currentSymbol!==k&&(q=q.destroy());if(q)q[b?"animate":"attr"]({x:z.x,y:z.y});else k&&(e.stateMarkerGraphic=
q=t.renderer.symbol(k,z.x,z.y,z.width,z.height).add(e.markerGroup),q.currentSymbol=k);q&&q.attr(e.pointAttribs(this,a))}q&&(q[a&&t.isInsidePlot(c,d,t.inverted)?"show":"hide"](),q.element.point=this)}(c=f.halo)&&c.size?(u||(e.halo=u=t.renderer.path().add((this.graphic||q).parentGroup)),u.show()[b?"animate":"attr"]({d:this.haloPath(c.size)}),u.attr({"class":"highcharts-halo highcharts-color-"+h(this.colorIndex,e.colorIndex)}),u.point=this,u.attr(n({fill:this.color||e.color,"fill-opacity":c.opacity,
zIndex:-1},c.attributes))):u&&u.point&&u.point.haloPath&&u.animate({d:u.point.haloPath(0)},null,u.hide);this.state=a;v(this,"afterSetState")}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});n(e.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&v(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=
this.chart,c=b.tooltip,d=b.hoverPoint;b.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&v(this,"mouseOut");!c||this.stickyTracking||c.shared&&!this.noSharedTooltip||c.hide();this.setState()},setState:function(a){var b=this,c=b.options,d=b.graph,e=c.states,f=c.lineWidth,c=0;a=a||"";if(b.state!==a&&(t([b.group,b.markerGroup,b.dataLabelsGroup],function(c){c&&(b.state&&c.removeClass("highcharts-series-"+b.state),a&&c.addClass("highcharts-series-"+a))}),b.state=a,!e[a]||!1!==e[a].enabled)&&
(a&&(f=e[a].lineWidth||f+(e[a].lineWidthPlus||0)),d&&!d.dashstyle))for(f={"stroke-width":f},d.animate(f,h(e[a||"normal"]&&e[a||"normal"].animation,b.chart.options.chart.animation));b["zone-graph-"+c];)b["zone-graph-"+c].attr(f),c+=1},setVisible:function(a,b){var c=this,d=c.chart,e=c.legendItem,f,h=d.options.chart.ignoreHiddenSeries,k=c.visible;f=(c.visible=a=c.options.visible=c.userOptions.visible=void 0===a?!k:a)?"show":"hide";t(["group","dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(c[a])c[a][f]()});
if(d.hoverSeries===c||(d.hoverPoint&&d.hoverPoint.series)===c)c.onMouseOut();e&&d.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&t(d.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});t(c.linkedSeries,function(b){b.setVisible(a,!1)});h&&(d.isDirtyBox=!0);!1!==b&&d.redraw();v(c,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);v(this,a?"select":
"unselect")},drawTracker:I.drawTrackerGraph})})(K);(function(a){var B=a.Chart,H=a.each,E=a.inArray,q=a.isArray,f=a.isObject,l=a.pick,t=a.splat;B.prototype.setResponsive=function(f){var l=this.options.responsive,n=[],c=this.currentResponsive;l&&l.rules&&H(l.rules,function(b){void 0===b._id&&(b._id=a.uniqueKey());this.matchResponsiveRule(b,n,f)},this);var b=a.merge.apply(0,a.map(n,function(b){return a.find(l.rules,function(a){return a._id===b}).chartOptions})),n=n.toString()||void 0;n!==(c&&c.ruleIds)&&
(c&&this.update(c.undoOptions,f),n?(this.currentResponsive={ruleIds:n,mergedOptions:b,undoOptions:this.currentOptions(b)},this.update(b,f)):this.currentResponsive=void 0)};B.prototype.matchResponsiveRule=function(a,f){var n=a.condition;(n.callback||function(){return this.chartWidth<=l(n.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=l(n.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=l(n.minWidth,0)&&this.chartHeight>=l(n.minHeight,0)}).call(this)&&f.push(a._id)};B.prototype.currentOptions=function(l){function n(c,
b,l,d){var h;a.objectEach(c,function(a,c){if(!d&&-1<E(c,["series","xAxis","yAxis"]))for(a=t(a),l[c]=[],h=0;h<a.length;h++)b[c][h]&&(l[c][h]={},n(a[h],b[c][h],l[c][h],d+1));else f(a)?(l[c]=q(a)?[]:{},n(a,b[c]||{},l[c],d+1)):l[c]=b[c]||null})}var u={};n(l,this.options,u,0);return u}})(K);return K});


/***/ }),

/***/ "./node_modules/highcharts/highstock.js":
/***/ (function(module, exports) {

/*
 Highstock JS v6.0.7 (2018-02-16)

 (c) 2009-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(T,L){"object"===typeof module&&module.exports?module.exports=T.document?L(T):L:T.Highcharts=L(T)})("undefined"!==typeof window?window:this,function(T){var L=function(){var a="undefined"===typeof T?window:T,C=a.document,E=a.navigator&&a.navigator.userAgent||"",F=C&&C.createElementNS&&!!C.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,r=/(edge|msie|trident)/i.test(E)&&!a.opera,m=-1!==E.indexOf("Firefox"),l=-1!==E.indexOf("Chrome"),w=m&&4>parseInt(E.split("Firefox/")[1],
10);return a.Highcharts?a.Highcharts.error(16,!0):{product:"Highstock",version:"6.0.7",deg2rad:2*Math.PI/360,doc:C,hasBidiBug:w,hasTouch:C&&void 0!==C.documentElement.ontouchstart,isMS:r,isWebKit:-1!==E.indexOf("AppleWebKit"),isFirefox:m,isChrome:l,isSafari:!l&&-1!==E.indexOf("Safari"),isTouchDevice:/(Mobile|Android|Windows Phone)/.test(E),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:F,win:a,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},
charts:[]}}();(function(a){a.timers=[];var C=a.charts,E=a.doc,F=a.win;a.error=function(r,m){r=a.isNumber(r)?"Highcharts error #"+r+": www.highcharts.com/errors/"+r:r;if(m)throw Error(r);F.console&&console.log(r)};a.Fx=function(a,m,l){this.options=m;this.elem=a;this.prop=l};a.Fx.prototype={dSetter:function(){var a=this.paths[0],m=this.paths[1],l=[],w=this.now,p=a.length,v;if(1===w)l=this.toD;else if(p===m.length&&1>w)for(;p--;)v=parseFloat(a[p]),l[p]=isNaN(v)?m[p]:w*parseFloat(m[p]-v)+v;else l=m;this.elem.attr("d",
l,null,!0)},update:function(){var a=this.elem,m=this.prop,l=this.now,w=this.options.step;if(this[m+"Setter"])this[m+"Setter"]();else a.attr?a.element&&a.attr(m,l,null,!0):a.style[m]=l+this.unit;w&&w.call(a,l,this)},run:function(r,m,l){var w=this,p=w.options,v=function(a){return v.stopped?!1:w.step(a)},B=F.requestAnimationFrame||function(a){setTimeout(a,13)},e=function(){for(var c=0;c<a.timers.length;c++)a.timers[c]()||a.timers.splice(c--,1);a.timers.length&&B(e)};r===m?(delete p.curAnim[this.prop],
p.complete&&0===a.keys(p.curAnim).length&&p.complete.call(this.elem)):(this.startTime=+new Date,this.start=r,this.end=m,this.unit=l,this.now=this.start,this.pos=0,v.elem=this.elem,v.prop=this.prop,v()&&1===a.timers.push(v)&&B(e))},step:function(r){var m=+new Date,l,w=this.options,p=this.elem,v=w.complete,B=w.duration,e=w.curAnim;p.attr&&!p.element?r=!1:r||m>=B+this.startTime?(this.now=this.end,this.pos=1,this.update(),l=e[this.prop]=!0,a.objectEach(e,function(a){!0!==a&&(l=!1)}),l&&v&&v.call(p),r=
!1):(this.pos=w.easing((m-this.startTime)/B),this.now=this.start+(this.end-this.start)*this.pos,this.update(),r=!0);return r},initPath:function(r,m,l){function w(a){var g,b;for(t=a.length;t--;)g="M"===a[t]||"L"===a[t],b=/[a-zA-Z]/.test(a[t+3]),g&&b&&a.splice(t+1,0,a[t+1],a[t+2],a[t+1],a[t+2])}function p(a,c){for(;a.length<g;){a[0]=c[g-a.length];var e=a.slice(0,b);[].splice.apply(a,[0,0].concat(e));f&&(e=a.slice(a.length-b),[].splice.apply(a,[a.length,0].concat(e)),t--)}a[0]="M"}function v(a,c){for(var e=
(g-a.length)/b;0<e&&e--;)n=a.slice().splice(a.length/u-b,b*u),n[0]=c[g-b-e*b],k&&(n[b-6]=n[b-2],n[b-5]=n[b-1]),[].splice.apply(a,[a.length/u,0].concat(n)),f&&e--}m=m||"";var B,e=r.startX,c=r.endX,k=-1<m.indexOf("C"),b=k?7:3,g,n,t;m=m.split(" ");l=l.slice();var f=r.isArea,u=f?2:1,D;k&&(w(m),w(l));if(e&&c){for(t=0;t<e.length;t++)if(e[t]===c[0]){B=t;break}else if(e[0]===c[c.length-e.length+t]){B=t;D=!0;break}void 0===B&&(m=[])}m.length&&a.isNumber(B)&&(g=l.length+B*u*b,D?(p(m,l),v(l,m)):(p(l,m),v(m,
l)));return[m,l]}};a.Fx.prototype.fillSetter=a.Fx.prototype.strokeSetter=function(){this.elem.attr(this.prop,a.color(this.start).tweenTo(a.color(this.end),this.pos),null,!0)};a.merge=function(){var r,m=arguments,l,w={},p=function(l,r){"object"!==typeof l&&(l={});a.objectEach(r,function(e,c){!a.isObject(e,!0)||a.isClass(e)||a.isDOMElement(e)?l[c]=r[c]:l[c]=p(l[c]||{},e)});return l};!0===m[0]&&(w=m[1],m=Array.prototype.slice.call(m,2));l=m.length;for(r=0;r<l;r++)w=p(w,m[r]);return w};a.pInt=function(a,
m){return parseInt(a,m||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(r,m){return!!r&&"object"===typeof r&&(!m||!a.isArray(r))};a.isDOMElement=function(r){return a.isObject(r)&&"number"===typeof r.nodeType};a.isClass=function(r){var m=r&&r.constructor;return!(!a.isObject(r,!0)||a.isDOMElement(r)||!m||!m.name||"Object"===m.name)};a.isNumber=function(a){return"number"===
typeof a&&!isNaN(a)&&Infinity>a&&-Infinity<a};a.erase=function(a,m){for(var l=a.length;l--;)if(a[l]===m){a.splice(l,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(r,m,l){var w;a.isString(m)?a.defined(l)?r.setAttribute(m,l):r&&r.getAttribute&&(w=r.getAttribute(m)):a.defined(m)&&a.isObject(m)&&a.objectEach(m,function(a,l){r.setAttribute(l,a)});return w};a.splat=function(r){return a.isArray(r)?r:[r]};a.syncTimeout=function(a,m,l){if(m)return setTimeout(a,m,l);a.call(0,
l)};a.extend=function(a,m){var l;a||(a={});for(l in m)a[l]=m[l];return a};a.pick=function(){var a=arguments,m,l,w=a.length;for(m=0;m<w;m++)if(l=a[m],void 0!==l&&null!==l)return l};a.css=function(r,m){a.isMS&&!a.svg&&m&&void 0!==m.opacity&&(m.filter="alpha(opacity\x3d"+100*m.opacity+")");a.extend(r.style,m)};a.createElement=function(r,m,l,w,p){r=E.createElement(r);var v=a.css;m&&a.extend(r,m);p&&v(r,{padding:0,border:"none",margin:0});l&&v(r,l);w&&w.appendChild(r);return r};a.extendClass=function(r,
m){var l=function(){};l.prototype=new r;a.extend(l.prototype,m);return l};a.pad=function(a,m,l){return Array((m||2)+1-String(a).length).join(l||0)+a};a.relativeLength=function(a,m,l){return/%$/.test(a)?m*parseFloat(a)/100+(l||0):parseFloat(a)};a.wrap=function(a,m,l){var w=a[m];a[m]=function(){var a=Array.prototype.slice.call(arguments),v=arguments,B=this;B.proceed=function(){w.apply(B,arguments.length?arguments:v)};a.unshift(w);a=l.apply(this,a);B.proceed=null;return a}};a.formatSingle=function(r,
m,l){var w=/\.([0-9])/,p=a.defaultOptions.lang;/f$/.test(r)?(l=(l=r.match(w))?l[1]:-1,null!==m&&(m=a.numberFormat(m,l,p.decimalPoint,-1<r.indexOf(",")?p.thousandsSep:""))):m=(l||a.time).dateFormat(r,m);return m};a.format=function(r,m,l){for(var w="{",p=!1,v,B,e,c,k=[],b;r;){w=r.indexOf(w);if(-1===w)break;v=r.slice(0,w);if(p){v=v.split(":");B=v.shift().split(".");c=B.length;b=m;for(e=0;e<c;e++)b&&(b=b[B[e]]);v.length&&(b=a.formatSingle(v.join(":"),b,l));k.push(b)}else k.push(v);r=r.slice(w+1);w=(p=
!p)?"}":"{"}k.push(r);return k.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(r,m,l,w,p){var v,B=r;l=a.pick(l,1);v=r/l;m||(m=p?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===w&&(1===l?m=a.grep(m,function(a){return 0===a%1}):.1>=l&&(m=[1/l])));for(w=0;w<m.length&&!(B=m[w],p&&B*l>=r||!p&&v<=(m[w]+(m[w+1]||m[w]))/2);w++);return B=a.correctFloat(B*l,-Math.round(Math.log(.001)/Math.LN10))};a.stableSort=function(a,m){var l=
a.length,w,p;for(p=0;p<l;p++)a[p].safeI=p;a.sort(function(a,p){w=m(a,p);return 0===w?a.safeI-p.safeI:w});for(p=0;p<l;p++)delete a[p].safeI};a.arrayMin=function(a){for(var m=a.length,l=a[0];m--;)a[m]<l&&(l=a[m]);return l};a.arrayMax=function(a){for(var m=a.length,l=a[0];m--;)a[m]>l&&(l=a[m]);return l};a.destroyObjectProperties=function(r,m){a.objectEach(r,function(a,w){a&&a!==m&&a.destroy&&a.destroy();delete r[w]})};a.discardElement=function(r){var m=a.garbageBin;m||(m=a.createElement("div"));r&&m.appendChild(r);
m.innerHTML=""};a.correctFloat=function(a,m){return parseFloat(a.toPrecision(m||14))};a.setAnimation=function(r,m){m.renderer.globalAnimation=a.pick(r,m.options.chart.animation,!0)};a.animObject=function(r){return a.isObject(r)?a.merge(r):{duration:r?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=function(r,m,l,w){r=+r||0;m=+m;var p=a.defaultOptions.lang,v=(r.toString().split(".")[1]||"").split("e")[0].length,B,
e,c=r.toString().split("e");-1===m?m=Math.min(v,20):a.isNumber(m)?m&&c[1]&&0>c[1]&&(B=m+ +c[1],0<=B?(c[0]=(+c[0]).toExponential(B).split("e")[0],m=B):(c[0]=c[0].split(".")[0]||0,r=20>m?(c[0]*Math.pow(10,c[1])).toFixed(m):0,c[1]=0)):m=2;e=(Math.abs(c[1]?c[0]:r)+Math.pow(10,-Math.max(m,v)-1)).toFixed(m);v=String(a.pInt(e));B=3<v.length?v.length%3:0;l=a.pick(l,p.decimalPoint);w=a.pick(w,p.thousandsSep);r=(0>r?"-":"")+(B?v.substr(0,B)+w:"");r+=v.substr(B).replace(/(\d{3})(?=\d)/g,"$1"+w);m&&(r+=l+e.slice(-m));
c[1]&&0!==+r&&(r+="e"+c[1]);return r};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*a)-1)};a.getStyle=function(r,m,l){if("width"===m)return Math.min(r.offsetWidth,r.scrollWidth)-a.getStyle(r,"padding-left")-a.getStyle(r,"padding-right");if("height"===m)return Math.min(r.offsetHeight,r.scrollHeight)-a.getStyle(r,"padding-top")-a.getStyle(r,"padding-bottom");F.getComputedStyle||a.error(27,!0);if(r=F.getComputedStyle(r,void 0))r=r.getPropertyValue(m),a.pick(l,"opacity"!==m)&&(r=a.pInt(r));
return r};a.inArray=function(r,m){return(a.indexOfPolyfill||Array.prototype.indexOf).call(m,r)};a.grep=function(r,m){return(a.filterPolyfill||Array.prototype.filter).call(r,m)};a.find=Array.prototype.find?function(a,m){return a.find(m)}:function(a,m){var l,w=a.length;for(l=0;l<w;l++)if(m(a[l],l))return a[l]};a.map=function(a,m){for(var l=[],w=0,p=a.length;w<p;w++)l[w]=m.call(a[w],a[w],w,a);return l};a.keys=function(r){return(a.keysPolyfill||Object.keys).call(void 0,r)};a.reduce=function(r,m,l){return(a.reducePolyfill||
Array.prototype.reduce).call(r,m,l)};a.offset=function(a){var m=E.documentElement;a=a.parentElement?a.getBoundingClientRect():{top:0,left:0};return{top:a.top+(F.pageYOffset||m.scrollTop)-(m.clientTop||0),left:a.left+(F.pageXOffset||m.scrollLeft)-(m.clientLeft||0)}};a.stop=function(r,m){for(var l=a.timers.length;l--;)a.timers[l].elem!==r||m&&m!==a.timers[l].prop||(a.timers[l].stopped=!0)};a.each=function(r,m,l){return(a.forEachPolyfill||Array.prototype.forEach).call(r,m,l)};a.objectEach=function(a,
m,l){for(var w in a)a.hasOwnProperty(w)&&m.call(l,a[w],w,a)};a.isPrototype=function(r){return r===a.Axis.prototype||r===a.Chart.prototype||r===a.Point.prototype||r===a.Series.prototype||r===a.Tick.prototype};a.addEvent=function(r,m,l){var w,p=r.addEventListener||a.addEventListenerPolyfill;w=a.isPrototype(r)?"protoEvents":"hcEvents";w=r[w]=r[w]||{};p&&p.call(r,m,l,!1);w[m]||(w[m]=[]);w[m].push(l);return function(){a.removeEvent(r,m,l)}};a.removeEvent=function(r,m,l){function w(e,c){var k=r.removeEventListener||
a.removeEventListenerPolyfill;k&&k.call(r,e,c,!1)}function p(e){var c,k;r.nodeName&&(m?(c={},c[m]=!0):c=e,a.objectEach(c,function(a,g){if(e[g])for(k=e[g].length;k--;)w(g,e[g][k])}))}var v,B;a.each(["protoEvents","hcEvents"],function(e){var c=r[e];c&&(m?(v=c[m]||[],l?(B=a.inArray(l,v),-1<B&&(v.splice(B,1),c[m]=v),w(m,l)):(p(c),c[m]=[])):(p(c),r[e]={}))})};a.fireEvent=function(r,m,l,w){var p,v,B,e,c;l=l||{};E.createEvent&&(r.dispatchEvent||r.fireEvent)?(p=E.createEvent("Events"),p.initEvent(m,!0,!0),
a.extend(p,l),r.dispatchEvent?r.dispatchEvent(p):r.fireEvent(m,p)):a.each(["protoEvents","hcEvents"],function(k){if(r[k])for(v=r[k][m]||[],B=v.length,l.target||a.extend(l,{preventDefault:function(){l.defaultPrevented=!0},target:r,type:m}),e=0;e<B;e++)(c=v[e])&&!1===c.call(r,l)&&l.preventDefault()});w&&!l.defaultPrevented&&w(l)};a.animate=function(r,m,l){var w,p="",v,B,e;a.isObject(l)||(e=arguments,l={duration:e[2],easing:e[3],complete:e[4]});a.isNumber(l.duration)||(l.duration=400);l.easing="function"===
typeof l.easing?l.easing:Math[l.easing]||Math.easeInOutSine;l.curAnim=a.merge(m);a.objectEach(m,function(c,e){a.stop(r,e);B=new a.Fx(r,l,e);v=null;"d"===e?(B.paths=B.initPath(r,r.d,m.d),B.toD=m.d,w=0,v=1):r.attr?w=r.attr(e):(w=parseFloat(a.getStyle(r,e))||0,"opacity"!==e&&(p="px"));v||(v=c);v&&v.match&&v.match("px")&&(v=v.replace(/px/g,""));B.run(w,v,p)})};a.seriesType=function(r,m,l,w,p){var v=a.getOptions(),B=a.seriesTypes;v.plotOptions[r]=a.merge(v.plotOptions[m],l);B[r]=a.extendClass(B[m]||function(){},
w);B[r].prototype.type=r;p&&(B[r].prototype.pointClass=a.extendClass(a.Point,p));return B[r]};a.uniqueKey=function(){var a=Math.random().toString(36).substring(2,9),m=0;return function(){return"highcharts-"+a+"-"+m++}}();F.jQuery&&(F.jQuery.fn.highcharts=function(){var r=[].slice.call(arguments);if(this[0])return r[0]?(new (a[a.isString(r[0])?r.shift():"Chart"])(this[0],r[0],r[1]),this):C[a.attr(this[0],"data-highcharts-chart")]})})(L);(function(a){var C=a.each,E=a.isNumber,F=a.map,r=a.merge,m=a.pInt;
a.Color=function(l){if(!(this instanceof a.Color))return new a.Color(l);this.init(l)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[m(a[1]),m(a[2]),m(a[3]),parseFloat(a[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[m(a[1]),m(a[2]),m(a[3]),1]}}],names:{none:"rgba(255,255,255,0)",white:"#ffffff",black:"#000000"},init:function(l){var m,
p,v,B;if((this.input=l=this.names[l&&l.toLowerCase?l.toLowerCase():""]||l)&&l.stops)this.stops=F(l.stops,function(e){return new a.Color(e[1])});else if(l&&l.charAt&&"#"===l.charAt()&&(m=l.length,l=parseInt(l.substr(1),16),7===m?p=[(l&16711680)>>16,(l&65280)>>8,l&255,1]:4===m&&(p=[(l&3840)>>4|(l&3840)>>8,(l&240)>>4|l&240,(l&15)<<4|l&15,1])),!p)for(v=this.parsers.length;v--&&!p;)B=this.parsers[v],(m=B.regex.exec(l))&&(p=B.parse(m));this.rgba=p||[]},get:function(a){var l=this.input,p=this.rgba,v;this.stops?
(v=r(l),v.stops=[].concat(v.stops),C(this.stops,function(p,e){v.stops[e]=[v.stops[e][0],p.get(a)]})):v=p&&E(p[0])?"rgb"===a||!a&&1===p[3]?"rgb("+p[0]+","+p[1]+","+p[2]+")":"a"===a?p[3]:"rgba("+p.join(",")+")":l;return v},brighten:function(a){var l,p=this.rgba;if(this.stops)C(this.stops,function(p){p.brighten(a)});else if(E(a)&&0!==a)for(l=0;3>l;l++)p[l]+=m(255*a),0>p[l]&&(p[l]=0),255<p[l]&&(p[l]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this},tweenTo:function(a,m){var p=this.rgba,
l=a.rgba;l.length&&p&&p.length?(a=1!==l[3]||1!==p[3],m=(a?"rgba(":"rgb(")+Math.round(l[0]+(p[0]-l[0])*(1-m))+","+Math.round(l[1]+(p[1]-l[1])*(1-m))+","+Math.round(l[2]+(p[2]-l[2])*(1-m))+(a?","+(l[3]+(p[3]-l[3])*(1-m)):"")+")"):m=a.input||"none";return m}};a.color=function(l){return new a.Color(l)}})(L);(function(a){var C,E,F=a.addEvent,r=a.animate,m=a.attr,l=a.charts,w=a.color,p=a.css,v=a.createElement,B=a.defined,e=a.deg2rad,c=a.destroyObjectProperties,k=a.doc,b=a.each,g=a.extend,n=a.erase,t=a.grep,
f=a.hasTouch,u=a.inArray,D=a.isArray,q=a.isFirefox,A=a.isMS,y=a.isObject,H=a.isString,I=a.isWebKit,J=a.merge,d=a.noop,z=a.objectEach,G=a.pick,h=a.pInt,x=a.removeEvent,N=a.stop,O=a.svg,P=a.SVG_NS,M=a.symbolSizes,Q=a.win;C=a.SVGElement=function(){return this};g(C.prototype,{opacity:1,SVG_NS:P,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline".split(" "),init:function(a,h){this.element="span"===h?v(h):k.createElementNS(this.SVG_NS,
h);this.renderer=a},animate:function(h,d,x){d=a.animObject(G(d,this.renderer.globalAnimation,!0));0!==d.duration?(x&&(d.complete=x),r(this,h,d)):(this.attr(h,null,x),d.step&&d.step.call(this));return this},colorGradient:function(h,d,x){var K=this.renderer,g,f,c,e,n,q,t,u,A,y,G=[],k;h.radialGradient?f="radialGradient":h.linearGradient&&(f="linearGradient");f&&(c=h[f],n=K.gradients,t=h.stops,y=x.radialReference,D(c)&&(h[f]=c={x1:c[0],y1:c[1],x2:c[2],y2:c[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===
f&&y&&!B(c.gradientUnits)&&(e=c,c=J(c,K.getRadialAttr(y,e),{gradientUnits:"userSpaceOnUse"})),z(c,function(a,h){"id"!==h&&G.push(h,a)}),z(t,function(a){G.push(a)}),G=G.join(","),n[G]?y=n[G].attr("id"):(c.id=y=a.uniqueKey(),n[G]=q=K.createElement(f).attr(c).add(K.defs),q.radAttr=e,q.stops=[],b(t,function(h){0===h[1].indexOf("rgba")?(g=a.color(h[1]),u=g.get("rgb"),A=g.get("a")):(u=h[1],A=1);h=K.createElement("stop").attr({offset:h[0],"stop-color":u,"stop-opacity":A}).add(q);q.stops.push(h)})),k="url("+
K.url+"#"+y+")",x.setAttribute(d,k),x.gradient=G,h.toString=function(){return k})},applyTextOutline:function(h){var K=this.element,d,x,g,f,c;-1!==h.indexOf("contrast")&&(h=h.replace(/contrast/g,this.renderer.getContrast(K.style.fill)));h=h.split(" ");x=h[h.length-1];if((g=h[0])&&"none"!==g&&a.svg){this.fakeTS=!0;h=[].slice.call(K.getElementsByTagName("tspan"));this.ySetter=this.xSetter;g=g.replace(/(^[\d\.]+)(.*?)$/g,function(a,h,K){return 2*h+K});for(c=h.length;c--;)d=h[c],"highcharts-text-outline"===
d.getAttribute("class")&&n(h,K.removeChild(d));f=K.firstChild;b(h,function(a,h){0===h&&(a.setAttribute("x",K.getAttribute("x")),h=K.getAttribute("y"),a.setAttribute("y",h||0),null===h&&K.setAttribute("y",0));a=a.cloneNode(1);m(a,{"class":"highcharts-text-outline",fill:x,stroke:x,"stroke-width":g,"stroke-linejoin":"round"});K.insertBefore(a,f)})}},attr:function(a,h,d,x){var K,g=this.element,b,f=this,c,e;"string"===typeof a&&void 0!==h&&(K=a,a={},a[K]=h);"string"===typeof a?f=(this[a+"Getter"]||this._defaultGetter).call(this,
a,g):(z(a,function(h,d){c=!1;x||N(this,d);this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)$/.test(d)&&(b||(this.symbolAttr(a),b=!0),c=!0);!this.rotation||"x"!==d&&"y"!==d||(this.doTransform=!0);c||(e=this[d+"Setter"]||this._defaultSetter,e.call(this,h,d,g),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(d)&&this.updateShadows(d,h,e))},this),this.afterSetters());d&&d.call(this);return f},afterSetters:function(){this.doTransform&&(this.updateTransform(),
this.doTransform=!1)},updateShadows:function(a,h,d){for(var K=this.shadows,x=K.length;x--;)d.call(K[x],"height"===a?Math.max(h-(K[x].cutHeight||0),0):"d"===a?this.d:h,a,K[x])},addClass:function(a,h){var d=this.attr("class")||"";-1===d.indexOf(a)&&(h||(a=(d+(d?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==u(a,(this.attr("class")||"").split(" "))},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(a,""))},symbolAttr:function(a){var h=
this;b("x y r start end width height innerR anchorX anchorY".split(" "),function(d){h[d]=G(a[d],h[d])});h.attr({d:h.renderer.symbols[h.symbolName](h.x,h.y,h.width,h.height,h)})},clip:function(a){return this.attr("clip-path",a?"url("+this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,h){var d;h=h||a.strokeWidth||0;d=Math.round(h)%2/2;a.x=Math.floor(a.x||this.x||0)+d;a.y=Math.floor(a.y||this.y||0)+d;a.width=Math.floor((a.width||this.width||0)-2*d);a.height=Math.floor((a.height||this.height||0)-
2*d);B(a.strokeWidth)&&(a.strokeWidth=h);return a},css:function(a){var d=this.styles,x={},K=this.element,b,f="",c,e=!d,n=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);d&&z(a,function(a,h){a!==d[h]&&(x[h]=a,e=!0)});e&&(d&&(a=g(d,x)),b=this.textWidth=a&&a.width&&"auto"!==a.width&&"text"===K.nodeName.toLowerCase()&&h(a.width),this.styles=a,b&&!O&&this.renderer.forExport&&delete a.width,K.namespaceURI===this.SVG_NS?(c=function(a,h){return"-"+h.toLowerCase()},z(a,function(a,h){-1===
u(h,n)&&(f+=h.replace(/([A-Z])/g,c)+":"+a+";")}),f&&m(K,"style",f)):p(K,a),this.added&&("text"===this.element.nodeName&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline)));return this},strokeWidth:function(){return this["stroke-width"]||0},on:function(a,h){var d=this,x=d.element;f&&"click"===a?(x.ontouchstart=function(a){d.touchEventFired=Date.now();a.preventDefault();h.call(x,a)},x.onclick=function(a){(-1===Q.navigator.userAgent.indexOf("Android")||1100<Date.now()-
(d.touchEventFired||0))&&h.call(x,a)}):x["on"+a]=h;return this},setRadialReference:function(a){var h=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;h&&h.radAttr&&h.animate(this.renderer.getRadialAttr(a,h.radAttr));return this},translate:function(a,h){return this.attr({translateX:a,translateY:h})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,h=this.translateY||0,d=this.scaleX,x=this.scaleY,
g=this.inverted,b=this.rotation,f=this.matrix,c=this.element;g&&(a+=this.width,h+=this.height);a=["translate("+a+","+h+")"];B(f)&&a.push("matrix("+f.join(",")+")");g?a.push("rotate(90) scale(-1,1)"):b&&a.push("rotate("+b+" "+G(this.rotationOriginX,c.getAttribute("x"),0)+" "+G(this.rotationOriginY,c.getAttribute("y")||0)+")");(B(d)||B(x))&&a.push("scale("+G(d,1)+" "+G(x,1)+")");a.length&&c.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},
align:function(a,h,d){var x,g,b,f,K={};g=this.renderer;b=g.alignedObjects;var c,e;if(a){if(this.alignOptions=a,this.alignByTranslate=h,!d||H(d))this.alignTo=x=d||"renderer",n(b,this),b.push(this),d=null}else a=this.alignOptions,h=this.alignByTranslate,x=this.alignTo;d=G(d,g[x],g);x=a.align;g=a.verticalAlign;b=(d.x||0)+(a.x||0);f=(d.y||0)+(a.y||0);"right"===x?c=1:"center"===x&&(c=2);c&&(b+=(d.width-(a.width||0))/c);K[h?"translateX":"x"]=Math.round(b);"bottom"===g?e=1:"middle"===g&&(e=2);e&&(f+=(d.height-
(a.height||0))/e);K[h?"translateY":"y"]=Math.round(f);this[this.placed?"animate":"attr"](K);this.placed=!0;this.alignAttr=K;return this},getBBox:function(a,h){var d,x=this.renderer,f,c=this.element,K=this.styles,n,z=this.textStr,q,t=x.cache,u=x.cacheKeys,A;h=G(h,this.rotation);f=h*e;n=K&&K.fontSize;B(z)&&(A=z.toString(),-1===A.indexOf("\x3c")&&(A=A.replace(/[0-9]/g,"0")),A+=["",h||0,n,K&&K.width,K&&K.textOverflow].join());A&&!a&&(d=t[A]);if(!d){if(c.namespaceURI===this.SVG_NS||x.forExport){try{(q=
this.fakeTS&&function(a){b(c.querySelectorAll(".highcharts-text-outline"),function(h){h.style.display=a})})&&q("none"),d=c.getBBox?g({},c.getBBox()):{width:c.offsetWidth,height:c.offsetHeight},q&&q("")}catch(fa){}if(!d||0>d.width)d={width:0,height:0}}else d=this.htmlGetBBox();x.isSVG&&(a=d.width,x=d.height,K&&"11px"===K.fontSize&&17===Math.round(x)&&(d.height=x=14),h&&(d.width=Math.abs(x*Math.sin(f))+Math.abs(a*Math.cos(f)),d.height=Math.abs(x*Math.cos(f))+Math.abs(a*Math.sin(f))));if(A&&0<d.height){for(;250<
u.length;)delete t[u.shift()];t[A]||u.push(A);t[A]=d}}return d},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var h=this;h.animate({opacity:0},{duration:a||150,complete:function(){h.attr({y:-9999})}})},add:function(a){var h=this.renderer,d=this.element,x;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&h.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)x=
this.zIndexSetter();x||(a?a.element:h.box).appendChild(d);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var h=a.parentNode;h&&h.removeChild(a)},destroy:function(){var a=this,h=a.element||{},d=a.renderer.isSVG&&"SPAN"===h.nodeName&&a.parentGroup,x=h.ownerSVGElement,g=a.clipPath;h.onclick=h.onmouseout=h.onmouseover=h.onmousemove=h.point=null;N(a);g&&x&&(b(x.querySelectorAll("[clip-path],[CLIP-PATH]"),function(a){var h=a.getAttribute("clip-path"),d=g.element.id;(-1<h.indexOf("(#"+
d+")")||-1<h.indexOf('("#'+d+'")'))&&a.removeAttribute("clip-path")}),a.clipPath=g.destroy());if(a.stops){for(x=0;x<a.stops.length;x++)a.stops[x]=a.stops[x].destroy();a.stops=null}a.safeRemoveChild(h);for(a.destroyShadows();d&&d.div&&0===d.div.childNodes.length;)h=d.parentGroup,a.safeRemoveChild(d.div),delete d.div,d=h;a.alignTo&&n(a.renderer.alignedObjects,a);z(a,function(h,d){delete a[d]});return null},shadow:function(a,h,d){var x=[],g,b,f=this.element,c,e,K,n;if(!a)this.destroyShadows();else if(!this.shadows){e=
G(a.width,3);K=(a.opacity||.15)/e;n=this.parentInverted?"(-1,-1)":"("+G(a.offsetX,1)+", "+G(a.offsetY,1)+")";for(g=1;g<=e;g++)b=f.cloneNode(0),c=2*e+1-2*g,m(b,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":K*g,"stroke-width":c,transform:"translate"+n,fill:"none"}),d&&(m(b,"height",Math.max(m(b,"height")-c,0)),b.cutHeight=c),h?h.element.appendChild(b):f.parentNode&&f.parentNode.insertBefore(b,f),x.push(b);this.shadows=x}return this},destroyShadows:function(){b(this.shadows||[],function(a){this.safeRemoveChild(a)},
this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=G(this[a+"Value"],this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,h,d){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[h]!==a&&(d.setAttribute(h,a),this[h]=a)},dashstyleSetter:function(a){var d,x=this["stroke-width"];"inherit"===
x&&(x=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(d=a.length;d--;)a[d]=h(a[d])*x;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.alignValue=a;this.element.setAttribute("text-anchor",{left:"start",center:"middle",
right:"end"}[a])},opacitySetter:function(a,h,d){this[h]=a;d.setAttribute(h,a)},titleSetter:function(a){var h=this.element.getElementsByTagName("title")[0];h||(h=k.createElementNS(this.SVG_NS,"title"),this.element.appendChild(h));h.firstChild&&h.removeChild(h.firstChild);h.appendChild(k.createTextNode(String(G(a),"").replace(/<[^>]*>/g,"").replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},
fillSetter:function(a,h,d){"string"===typeof a?d.setAttribute(h,a):a&&this.colorGradient(a,h,d)},visibilitySetter:function(a,h,d){"inherit"===a?d.removeAttribute(h):this[h]!==a&&d.setAttribute(h,a);this[h]=a},zIndexSetter:function(a,d){var x=this.renderer,g=this.parentGroup,b=(g||x).element||x.box,f,c=this.element,e,n,x=b===x.box;f=this.added;var z;B(a)&&(c.zIndex=a,a=+a,this[d]===a&&(f=!1),this[d]=a);if(f){(a=this.zIndex)&&g&&(g.handleZ=!0);d=b.childNodes;for(z=d.length-1;0<=z&&!e;z--)if(g=d[z],
f=g.zIndex,n=!B(f),g!==c)if(0>a&&n&&!x&&!z)b.insertBefore(c,d[z]),e=!0;else if(h(f)<=a||n&&(!B(a)||0<=a))b.insertBefore(c,d[z+1]||null),e=!0;e||(b.insertBefore(c,d[x?3:0]||null),e=!0)}return e},_defaultSetter:function(a,h,d){d.setAttribute(h,a)}});C.prototype.yGetter=C.prototype.xGetter;C.prototype.translateXSetter=C.prototype.translateYSetter=C.prototype.rotationSetter=C.prototype.verticalAlignSetter=C.prototype.rotationOriginXSetter=C.prototype.rotationOriginYSetter=C.prototype.scaleXSetter=C.prototype.scaleYSetter=
C.prototype.matrixSetter=function(a,h){this[h]=a;this.doTransform=!0};C.prototype["stroke-widthSetter"]=C.prototype.strokeSetter=function(a,h,d){this[h]=a;this.stroke&&this["stroke-width"]?(C.prototype.fillSetter.call(this,this.stroke,"stroke",d),d.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===h&&0===a&&this.hasStroke&&(d.removeAttribute("stroke"),this.hasStroke=!1)};E=a.SVGRenderer=function(){this.init.apply(this,arguments)};g(E.prototype,{Element:C,SVG_NS:P,
init:function(a,h,d,x,g,b){var f;x=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(x));f=x.element;a.appendChild(f);m(a,"dir","ltr");-1===a.innerHTML.indexOf("xmlns")&&m(f,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=f;this.boxWrapper=x;this.alignedObjects=[];this.url=(q||I)&&k.getElementsByTagName("base").length?Q.location.href.replace(/#.*?$/,"").replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(k.createTextNode("Created with Highstock 6.0.7"));
this.defs=this.createElement("defs").add();this.allowHTML=b;this.forExport=g;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(h,d,!1);var c;q&&a.getBoundingClientRect&&(h=function(){p(a,{left:0,top:0});c=a.getBoundingClientRect();p(a,{left:Math.ceil(c.left)-c.left+"px",top:Math.ceil(c.top)-c.top+"px"})},h(),this.unSubPixelFix=F(Q,"resize",h))},getStyle:function(a){return this.style=g({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},
a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();c(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var h=new this.Element;h.init(this,a);return h},draw:d,getRadialAttr:function(a,h){return{cx:a[0]-a[2]/2+h.cx*a[2],cy:a[1]-
a[2]/2+h.cy*a[2],r:h.r*a[2]}},getSpanWidth:function(a){return a.getBBox(!0).width},applyEllipsis:function(a,h,d,x){var g=a.rotation,b=d,f,c=0,e=d.length,n=function(a){h.removeChild(h.firstChild);a&&h.appendChild(k.createTextNode(a))},z;a.rotation=0;b=this.getSpanWidth(a,h);if(z=b>x){for(;c<=e;)f=Math.ceil((c+e)/2),b=d.substring(0,f)+"\u2026",n(b),b=this.getSpanWidth(a,h),c===e?c=e+1:b>x?e=f-1:c=f;0===e&&n("")}a.rotation=g;return z},escapes:{"\x26":"\x26amp;","\x3c":"\x26lt;","\x3e":"\x26gt;","'":"\x26#39;",
'"':"\x26quot;"},buildText:function(a){var d=a.element,x=this,g=x.forExport,f=G(a.textStr,"").toString(),c=-1!==f.indexOf("\x3c"),e=d.childNodes,n,q,A,y,I=m(d,"x"),D=a.styles,K=a.textWidth,J=D&&D.lineHeight,N=D&&D.textOutline,H=D&&"ellipsis"===D.textOverflow,M=D&&"nowrap"===D.whiteSpace,l=D&&D.fontSize,v,B,Q=e.length,D=K&&!a.added&&this.box,w=function(a){var g;g=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:l||x.style.fontSize||12;return J?h(J):x.fontMetrics(g,a.getAttribute("style")?a:d).h},
r=function(a,h){z(x.escapes,function(d,x){h&&-1!==u(d,h)||(a=a.toString().replace(new RegExp(d,"g"),x))});return a};v=[f,H,M,J,N,l,K].join();if(v!==a.textCache){for(a.textCache=v;Q--;)d.removeChild(e[Q]);c||N||H||K||-1!==f.indexOf(" ")?(n=/<.*class="([^"]+)".*>/,q=/<.*style="([^"]+)".*>/,A=/<.*href="([^"]+)".*>/,D&&D.appendChild(d),f=c?f.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,
"\x3c/span\x3e").split(/<br.*?>/g):[f],f=t(f,function(a){return""!==a}),b(f,function(h,f){var c,e=0;h=h.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");c=h.split("|||");b(c,function(h){if(""!==h||1===c.length){var b={},z=k.createElementNS(x.SVG_NS,"tspan"),t,u;n.test(h)&&(t=h.match(n)[1],m(z,"class",t));q.test(h)&&(u=h.match(q)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),m(z,"style",u));A.test(h)&&!g&&(m(z,"onclick",'location.href\x3d"'+h.match(A)[1]+
'"'),m(z,"class","highcharts-anchor"),p(z,{cursor:"pointer"}));h=r(h.replace(/<[a-zA-Z\/](.|\n)*?>/g,"")||" ");if(" "!==h){z.appendChild(k.createTextNode(h));e?b.dx=0:f&&null!==I&&(b.x=I);m(z,b);d.appendChild(z);!e&&B&&(!O&&g&&p(z,{display:"block"}),m(z,"dy",w(z)));if(K){b=h.replace(/([^\^])-/g,"$1- ").split(" ");t=1<c.length||f||1<b.length&&!M;var G=[],D,J=w(z),N=a.rotation;for(H&&(y=x.applyEllipsis(a,z,h,K));!H&&t&&(b.length||G.length);)a.rotation=0,D=x.getSpanWidth(a,z),h=D>K,void 0===y&&(y=h),
h&&1!==b.length?(z.removeChild(z.firstChild),G.unshift(b.pop())):(b=G,G=[],b.length&&!M&&(z=k.createElementNS(P,"tspan"),m(z,{dy:J,x:I}),u&&m(z,"style",u),d.appendChild(z)),D>K&&(K=D)),b.length&&z.appendChild(k.createTextNode(b.join(" ").replace(/- /g,"-")));a.rotation=N}e++}}});B=B||d.childNodes.length}),y&&a.attr("title",r(a.textStr,["\x26lt;","\x26gt;"])),D&&D.removeChild(d),N&&a.applyTextOutline&&a.applyTextOutline(N)):d.appendChild(k.createTextNode(r(f)))}},getContrast:function(a){a=w(a).rgba;
return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,h,d,x,b,f,c,e,z){var n=this.label(a,h,d,z,null,null,null,null,"button"),q=0;n.attr(J({padding:8,r:2},b));var t,u,G,y;b=J({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,style:{color:"#333333",cursor:"pointer",fontWeight:"normal"}},b);t=b.style;delete b.style;f=J(b,{fill:"#e6e6e6"},f);u=f.style;delete f.style;c=J(b,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},c);G=c.style;delete c.style;e=J(b,{style:{color:"#cccccc"}},
e);y=e.style;delete e.style;F(n.element,A?"mouseover":"mouseenter",function(){3!==q&&n.setState(1)});F(n.element,A?"mouseout":"mouseleave",function(){3!==q&&n.setState(q)});n.setState=function(a){1!==a&&(n.state=q=a);n.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+["normal","hover","pressed","disabled"][a||0]);n.attr([b,f,c,e][a||0]).css([t,u,G,y][a||0])};n.attr(b).css(g({cursor:"default"},t));return n.on("click",function(a){3!==q&&x.call(n,a)})},crispLine:function(a,
h){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-h%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+h%2/2);return a},path:function(a){var h={fill:"none"};D(a)?h.d=a:y(a)&&g(h,a);return this.createElement("path").attr(h)},circle:function(a,h,d){a=y(a)?a:{x:a,y:h,r:d};h=this.createElement("circle");h.xSetter=h.ySetter=function(a,h,d){d.setAttribute("c"+h,a)};return h.attr(a)},arc:function(a,h,d,x,b,g){y(a)?(x=a,h=x.y,d=x.r,a=x.x):x={innerR:x,start:b,end:g};a=this.symbol("arc",a,h,d,d,x);a.r=d;return a},rect:function(a,
h,d,x,b,g){b=y(a)?a.r:b;var f=this.createElement("rect");a=y(a)?a:void 0===a?{}:{x:a,y:h,width:Math.max(d,0),height:Math.max(x,0)};void 0!==g&&(a.strokeWidth=g,a=f.crisp(a));a.fill="none";b&&(a.r=b);f.rSetter=function(a,h,d){m(d,{rx:a,ry:a})};return f.attr(a)},setSize:function(a,h,d){var x=this.alignedObjects,b=x.length;this.width=a;this.height=h;for(this.boxWrapper.animate({width:a,height:h},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:G(d,!0)?
void 0:0});b--;)x[b].align()},g:function(a){var h=this.createElement("g");return a?h.attr({"class":"highcharts-"+a}):h},image:function(a,h,d,x,b){var f={preserveAspectRatio:"none"};1<arguments.length&&g(f,{x:h,y:d,width:x,height:b});f=this.createElement("image").attr(f);f.element.setAttributeNS?f.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):f.element.setAttribute("hc-svg-href",a);return f},symbol:function(a,h,d,x,f,c){var e=this,z,n=/^url\((.*?)\)$/,q=n.test(a),t=!q&&(this.symbols[a]?
a:"circle"),A=t&&this.symbols[t],u=B(h)&&A&&A.call(this.symbols,Math.round(h),Math.round(d),x,f,c),y,I;A?(z=this.path(u),z.attr("fill","none"),g(z,{symbolName:t,x:h,y:d,width:x,height:f}),c&&g(z,c)):q&&(y=a.match(n)[1],z=this.image(y),z.imgwidth=G(M[y]&&M[y].width,c&&c.width),z.imgheight=G(M[y]&&M[y].height,c&&c.height),I=function(){z.attr({width:z.width,height:z.height})},b(["width","height"],function(a){z[a+"Setter"]=function(a,h){var d={},x=this["img"+h],b="width"===h?"translateX":"translateY";
this[h]=a;B(x)&&(this.element&&this.element.setAttribute(h,x),this.alignByTranslate||(d[b]=((this[h]||0)-x)/2,this.attr(d)))}}),B(h)&&z.attr({x:h,y:d}),z.isImg=!0,B(z.imgwidth)&&B(z.imgheight)?I():(z.attr({width:0,height:0}),v("img",{onload:function(){var a=l[e.chartIndex];0===this.width&&(p(this,{position:"absolute",top:"-999em"}),k.body.appendChild(this));M[y]={width:this.width,height:this.height};z.imgwidth=this.width;z.imgheight=this.height;z.element&&I();this.parentNode&&this.parentNode.removeChild(this);
e.imgCount--;if(!e.imgCount&&a&&a.onload)a.onload()},src:y}),this.imgCount++));return z},symbols:{circle:function(a,h,d,x){return this.arc(a+d/2,h+x/2,d/2,x/2,{start:0,end:2*Math.PI,open:!1})},square:function(a,h,d,x){return["M",a,h,"L",a+d,h,a+d,h+x,a,h+x,"Z"]},triangle:function(a,h,d,x){return["M",a+d/2,h,"L",a+d,h+x,a,h+x,"Z"]},"triangle-down":function(a,h,d,x){return["M",a,h,"L",a+d,h,a+d/2,h+x,"Z"]},diamond:function(a,h,d,x){return["M",a+d/2,h,"L",a+d,h+x/2,a+d/2,h+x,a,h+x/2,"Z"]},arc:function(a,
h,d,x,b){var f=b.start,g=b.r||d,c=b.r||x||d,e=b.end-.001;d=b.innerR;x=G(b.open,.001>Math.abs(b.end-b.start-2*Math.PI));var z=Math.cos(f),n=Math.sin(f),q=Math.cos(e),e=Math.sin(e);b=.001>b.end-f-Math.PI?0:1;g=["M",a+g*z,h+c*n,"A",g,c,0,b,1,a+g*q,h+c*e];B(d)&&g.push(x?"M":"L",a+d*q,h+d*e,"A",d,d,0,b,0,a+d*z,h+d*n);g.push(x?"":"Z");return g},callout:function(a,h,d,x,b){var f=Math.min(b&&b.r||0,d,x),g=f+6,c=b&&b.anchorX;b=b&&b.anchorY;var e;e=["M",a+f,h,"L",a+d-f,h,"C",a+d,h,a+d,h,a+d,h+f,"L",a+d,h+x-
f,"C",a+d,h+x,a+d,h+x,a+d-f,h+x,"L",a+f,h+x,"C",a,h+x,a,h+x,a,h+x-f,"L",a,h+f,"C",a,h,a,h,a+f,h];c&&c>d?b>h+g&&b<h+x-g?e.splice(13,3,"L",a+d,b-6,a+d+6,b,a+d,b+6,a+d,h+x-f):e.splice(13,3,"L",a+d,x/2,c,b,a+d,x/2,a+d,h+x-f):c&&0>c?b>h+g&&b<h+x-g?e.splice(33,3,"L",a,b+6,a-6,b,a,b-6,a,h+f):e.splice(33,3,"L",a,x/2,c,b,a,x/2,a,h+f):b&&b>x&&c>a+g&&c<a+d-g?e.splice(23,3,"L",c+6,h+x,c,h+x+6,c-6,h+x,a+f,h+x):b&&0>b&&c>a+g&&c<a+d-g&&e.splice(3,3,"L",c-6,h,c,h-6,c+6,h,d-f,h);return e}},clipRect:function(h,d,x,
b){var f=a.uniqueKey(),g=this.createElement("clipPath").attr({id:f}).add(this.defs);h=this.rect(h,d,x,b,0).add(g);h.id=f;h.clipPath=g;h.count=0;return h},text:function(a,h,d,x){var b={};if(x&&(this.allowHTML||!this.forExport))return this.html(a,h,d);b.x=Math.round(h||0);d&&(b.y=Math.round(d));if(a||0===a)b.text=a;a=this.createElement("text").attr(b);x||(a.xSetter=function(a,h,d){var x=d.getElementsByTagName("tspan"),b,f=d.getAttribute(h),g;for(g=0;g<x.length;g++)b=x[g],b.getAttribute(h)===f&&b.setAttribute(h,
a);d.setAttribute(h,a)});return a},fontMetrics:function(a,d){a=a||d&&d.style&&d.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?h(a):/em/.test(a)?parseFloat(a)*(d?this.fontMetrics(null,d.parentNode).f:16):12;d=24>a?a+3:Math.round(1.2*a);return{h:d,b:Math.round(.8*d),f:a}},rotCorr:function(a,h,d){var x=a;h&&d&&(x=Math.max(x*Math.cos(h*e),4));return{x:-a/3*Math.sin(h*e),y:x}},label:function(h,d,f,c,e,z,n,q,t){var A=this,u=A.g("button"!==t&&"label"),y=u.text=A.text("",0,0,n).attr({zIndex:1}),
G,I,k=0,P=3,O=0,D,N,H,M,p,l={},v,m,K=/^url\((.*?)\)$/.test(c),Q=K,w,r,Y,V;t&&u.addClass("highcharts-"+t);Q=K;w=function(){return(v||0)%2/2};r=function(){var a=y.element.style,h={};I=(void 0===D||void 0===N||p)&&B(y.textStr)&&y.getBBox();u.width=(D||I.width||0)+2*P+O;u.height=(N||I.height||0)+2*P;m=P+A.fontMetrics(a&&a.fontSize,y).b;Q&&(G||(u.box=G=A.symbols[c]||K?A.symbol(c):A.rect(),G.addClass(("button"===t?"":"highcharts-label-box")+(t?" highcharts-"+t+"-box":"")),G.add(u),a=w(),h.x=a,h.y=(q?-m:
0)+a),h.width=Math.round(u.width),h.height=Math.round(u.height),G.attr(g(h,l)),l={})};Y=function(){var a=O+P,h;h=q?0:m;B(D)&&I&&("center"===p||"right"===p)&&(a+={center:.5,right:1}[p]*(D-I.width));if(a!==y.x||h!==y.y)y.attr("x",a),void 0!==h&&y.attr("y",h);y.x=a;y.y=h};V=function(a,h){G?G.attr(a,h):l[a]=h};u.onAdd=function(){y.add(u);u.attr({text:h||0===h?h:"",x:d,y:f});G&&B(e)&&u.attr({anchorX:e,anchorY:z})};u.widthSetter=function(h){D=a.isNumber(h)?h:null};u.heightSetter=function(a){N=a};u["text-alignSetter"]=
function(a){p=a};u.paddingSetter=function(a){B(a)&&a!==P&&(P=u.padding=a,Y())};u.paddingLeftSetter=function(a){B(a)&&a!==O&&(O=a,Y())};u.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==k&&(k=a,I&&u.attr({x:H}))};u.textSetter=function(a){void 0!==a&&y.textSetter(a);r();Y()};u["stroke-widthSetter"]=function(a,h){a&&(Q=!0);v=this["stroke-width"]=a;V(h,a)};u.strokeSetter=u.fillSetter=u.rSetter=function(a,h){"r"!==h&&("fill"===h&&a&&(Q=!0),u[h]=a);V(h,a)};u.anchorXSetter=function(a,h){e=u.anchorX=
a;V(h,Math.round(a)-w()-H)};u.anchorYSetter=function(a,h){z=u.anchorY=a;V(h,a-M)};u.xSetter=function(a){u.x=a;k&&(a-=k*((D||I.width)+2*P));H=Math.round(a);u.attr("translateX",H)};u.ySetter=function(a){M=u.y=Math.round(a);u.attr("translateY",M)};var ea=u.css;return g(u,{css:function(a){if(a){var h={};a=J(a);b(u.textProps,function(d){void 0!==a[d]&&(h[d]=a[d],delete a[d])});y.css(h)}return ea.call(u,a)},getBBox:function(){return{width:I.width+2*P,height:I.height+2*P,x:I.x-P,y:I.y-P}},shadow:function(a){a&&
(r(),G&&G.shadow(a));return u},destroy:function(){x(u.element,"mouseenter");x(u.element,"mouseleave");y&&(y=y.destroy());G&&(G=G.destroy());C.prototype.destroy.call(u);u=A=r=Y=V=null}})}});a.Renderer=E})(L);(function(a){var C=a.attr,E=a.createElement,F=a.css,r=a.defined,m=a.each,l=a.extend,w=a.isFirefox,p=a.isMS,v=a.isWebKit,B=a.pick,e=a.pInt,c=a.SVGRenderer,k=a.win,b=a.wrap;l(a.SVGElement.prototype,{htmlCss:function(a){var b=this.element;if(b=a&&"SPAN"===b.tagName&&a.width)delete a.width,this.textWidth=
b,this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=l(this.styles,a);F(this.element,a);return this},htmlGetBBox:function(){var a=this.element;return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=this.renderer,b=this.element,c=this.translateX||0,f=this.translateY||0,u=this.x||0,k=this.y||0,q=this.textAlign||"left",A={left:0,center:.5,right:1}[q],y=this.styles,
H=y&&y.whiteSpace;F(b,{marginLeft:c,marginTop:f});this.shadows&&m(this.shadows,function(a){F(a,{marginLeft:c+1,marginTop:f+1})});this.inverted&&m(b.childNodes,function(d){a.invertChild(d,b)});if("SPAN"===b.tagName){var y=this.rotation,I=this.textWidth&&e(this.textWidth),J=[y,q,b.innerHTML,this.textWidth,this.textAlign].join(),d;(d=I!==this.oldTextWidth)&&!(d=I>this.oldTextWidth)&&((d=this.textPxLength)||(F(b,{width:"",whiteSpace:H||"nowrap"}),d=b.offsetWidth),d=d>I);d&&/[ \-]/.test(b.textContent||
b.innerText)&&(F(b,{width:I+"px",display:"block",whiteSpace:H||"normal"}),this.oldTextWidth=I);J!==this.cTT&&(H=a.fontMetrics(b.style.fontSize).b,r(y)&&y!==(this.oldRotation||0)&&this.setSpanRotation(y,A,H),this.getSpanCorrection(this.textPxLength||b.offsetWidth,H,A,y,q));F(b,{left:u+(this.xCorr||0)+"px",top:k+(this.yCorr||0)+"px"});this.cTT=J;this.oldRotation=y}}else this.alignOnAdd=!0},setSpanRotation:function(a,b,c){var f={},g=this.renderer.getTransformKey();f[g]=f.transform="rotate("+a+"deg)";
f[g+(w?"Origin":"-origin")]=f.transformOrigin=100*b+"% "+c+"px";F(this.element,f)},getSpanCorrection:function(a,b,c){this.xCorr=-a*c;this.yCorr=-b}});l(c.prototype,{getTransformKey:function(){return p&&!/Edge/.test(k.navigator.userAgent)?"-ms-transform":v?"-webkit-transform":w?"MozTransform":k.opera?"-o-transform":""},html:function(a,c,e){var f=this.createElement("span"),g=f.element,n=f.renderer,q=n.isSVG,t=function(a,f){m(["opacity","visibility"],function(c){b(a,c+"Setter",function(a,d,b,c){a.call(this,
d,b,c);f[b]=d})})};f.textSetter=function(a){a!==g.innerHTML&&delete this.bBox;this.textStr=a;g.innerHTML=B(a,"");f.doTransform=!0};q&&t(f,f.element.style);f.xSetter=f.ySetter=f.alignSetter=f.rotationSetter=function(a,b){"align"===b&&(b="textAlign");f[b]=a;f.doTransform=!0};f.afterSetters=function(){this.doTransform&&(this.htmlUpdateTransform(),this.doTransform=!1)};f.attr({text:a,x:Math.round(c),y:Math.round(e)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});
g.style.whiteSpace="nowrap";f.css=f.htmlCss;q&&(f.add=function(a){var b,c=n.box.parentNode,e=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)e.push(a),a=a.parentGroup;m(e.reverse(),function(a){function d(h,d){a[d]=h;"translateX"===d?g.left=h+"px":g.top=h+"px";a.doTransform=!0}var g,h=C(a.element,"class");h&&(h={className:h});b=a.div=a.div||E("div",h,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},
b||c);g=b.style;l(a,{classSetter:function(a){return function(h){this.element.setAttribute("class",h);a.className=h}}(b),on:function(){e[0].div&&f.on.apply({element:e[0].div},arguments);return a},translateXSetter:d,translateYSetter:d});t(a,g)})}}else b=c;b.appendChild(g);f.added=!0;f.alignOnAdd&&f.htmlUpdateTransform();return f});return f}})})(L);(function(a){var C=a.defined,E=a.each,F=a.extend,r=a.merge,m=a.pick,l=a.timeUnits,w=a.win;a.Time=function(a){this.update(a,!1)};a.Time.prototype={defaultOptions:{},
update:function(p){var l=m(p&&p.useUTC,!0),B=this;this.options=p=r(!0,this.options||{},p);this.Date=p.Date||w.Date;this.timezoneOffset=(this.useUTC=l)&&p.timezoneOffset;this.getTimezoneOffset=this.timezoneOffsetFunction();(this.variableTimezone=!(l&&!p.getTimezoneOffset&&!p.timezone))||this.timezoneOffset?(this.get=function(a,c){var e=c.getTime(),b=e-B.getTimezoneOffset(c);c.setTime(b);a=c["getUTC"+a]();c.setTime(e);return a},this.set=function(e,c,k){var b;if(-1!==a.inArray(e,["Milliseconds","Seconds",
"Minutes"]))c["set"+e](k);else b=B.getTimezoneOffset(c),b=c.getTime()-b,c.setTime(b),c["setUTC"+e](k),e=B.getTimezoneOffset(c),b=c.getTime()+e,c.setTime(b)}):l?(this.get=function(a,c){return c["getUTC"+a]()},this.set=function(a,c,k){return c["setUTC"+a](k)}):(this.get=function(a,c){return c["get"+a]()},this.set=function(a,c,k){return c["set"+a](k)})},makeTime:function(p,l,B,e,c,k){var b,g,n;this.useUTC?(b=this.Date.UTC.apply(0,arguments),g=this.getTimezoneOffset(b),b+=g,n=this.getTimezoneOffset(b),
g!==n?b+=n-g:g-36E5!==this.getTimezoneOffset(b-36E5)||a.isSafari||(b-=36E5)):b=(new this.Date(p,l,m(B,1),m(e,0),m(c,0),m(k,0))).getTime();return b},timezoneOffsetFunction:function(){var p=this,l=this.options,m=w.moment;if(!this.useUTC)return function(a){return 6E4*(new Date(a)).getTimezoneOffset()};if(l.timezone){if(m)return function(a){return 6E4*-m.tz(a,l.timezone).utcOffset()};a.error(25)}return this.useUTC&&l.getTimezoneOffset?function(a){return 6E4*l.getTimezoneOffset(a)}:function(){return 6E4*
(p.timezoneOffset||0)}},dateFormat:function(p,l,m){if(!a.defined(l)||isNaN(l))return a.defaultOptions.lang.invalidDate||"";p=a.pick(p,"%Y-%m-%d %H:%M:%S");var e=this,c=new this.Date(l),k=this.get("Hours",c),b=this.get("Day",c),g=this.get("Date",c),n=this.get("Month",c),t=this.get("FullYear",c),f=a.defaultOptions.lang,u=f.weekdays,D=f.shortWeekdays,q=a.pad,c=a.extend({a:D?D[b]:u[b].substr(0,3),A:u[b],d:q(g),e:q(g,2," "),w:b,b:f.shortMonths[n],B:f.months[n],m:q(n+1),y:t.toString().substr(2,2),Y:t,H:q(k),
k:k,I:q(k%12||12),l:k%12||12,M:q(e.get("Minutes",c)),p:12>k?"AM":"PM",P:12>k?"am":"pm",S:q(c.getSeconds()),L:q(Math.round(l%1E3),3)},a.dateFormats);a.objectEach(c,function(a,b){for(;-1!==p.indexOf("%"+b);)p=p.replace("%"+b,"function"===typeof a?a.call(e,l):a)});return m?p.substr(0,1).toUpperCase()+p.substr(1):p},getTimeTicks:function(a,v,B,e){var c=this,k=[],b={},g,n=new c.Date(v),t=a.unitRange,f=a.count||1,u;if(C(v)){c.set("Milliseconds",n,t>=l.second?0:f*Math.floor(c.get("Milliseconds",n)/f));t>=
l.second&&c.set("Seconds",n,t>=l.minute?0:f*Math.floor(c.get("Seconds",n)/f));t>=l.minute&&c.set("Minutes",n,t>=l.hour?0:f*Math.floor(c.get("Minutes",n)/f));t>=l.hour&&c.set("Hours",n,t>=l.day?0:f*Math.floor(c.get("Hours",n)/f));t>=l.day&&c.set("Date",n,t>=l.month?1:f*Math.floor(c.get("Date",n)/f));t>=l.month&&(c.set("Month",n,t>=l.year?0:f*Math.floor(c.get("Month",n)/f)),g=c.get("FullYear",n));t>=l.year&&c.set("FullYear",n,g-g%f);t===l.week&&c.set("Date",n,c.get("Date",n)-c.get("Day",n)+m(e,1));
g=c.get("FullYear",n);e=c.get("Month",n);var D=c.get("Date",n),q=c.get("Hours",n);v=n.getTime();c.variableTimezone&&(u=B-v>4*l.month||c.getTimezoneOffset(v)!==c.getTimezoneOffset(B));n=n.getTime();for(v=1;n<B;)k.push(n),n=t===l.year?c.makeTime(g+v*f,0):t===l.month?c.makeTime(g,e+v*f):!u||t!==l.day&&t!==l.week?u&&t===l.hour&&1<f?c.makeTime(g,e,D,q+v*f):n+t*f:c.makeTime(g,e,D+v*f*(t===l.day?1:7)),v++;k.push(n);t<=l.hour&&1E4>k.length&&E(k,function(a){0===a%18E5&&"000000000"===c.dateFormat("%H%M%S%L",
a)&&(b[a]="day")})}k.info=F(a,{higherRanks:b,totalRange:t*f});return k}}})(L);(function(a){var C=a.color,E=a.merge;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{},time:a.Time.prototype.defaultOptions,chart:{borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:6},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},
title:{text:"Chart title",align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},
shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",
day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:C("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",
whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(C){a.defaultOptions=E(!0,a.defaultOptions,C);a.time.update(E(a.defaultOptions.global,a.defaultOptions.time),!1);return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;a.time=new a.Time(E(a.defaultOptions.global,
a.defaultOptions.time));a.dateFormat=function(C,r,m){return a.time.dateFormat(C,r,m)}})(L);(function(a){var C=a.correctFloat,E=a.defined,F=a.destroyObjectProperties,r=a.isNumber,m=a.merge,l=a.pick,w=a.deg2rad;a.Tick=function(a,l,m,e){this.axis=a;this.pos=l;this.type=m||"";this.isNewLabel=this.isNew=!0;m||e||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,v=a.options,B=a.chart,e=a.categories,c=a.names,k=this.pos,b=v.labels,g=a.tickPositions,n=k===g[0],t=k===g[g.length-1],c=e?
l(e[k],c[k],k):k,e=this.label,g=g.info,f;a.isDatetimeAxis&&g&&(f=v.dateTimeLabelFormats[g.higherRanks[k]||g.unitName]);this.isFirst=n;this.isLast=t;v=a.labelFormatter.call({axis:a,chart:B,isFirst:n,isLast:t,dateTimeLabelFormat:f,value:a.isLog?C(a.lin2log(c)):c,pos:k});if(E(e))e&&e.attr({text:v});else{if(this.label=e=E(v)&&b.enabled?B.renderer.text(v,0,0,b.useHTML).css(m(b.style)).add(a.labelGroup):null)e.textPxLength=e.getBBox().width;this.rotation=0}},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?
"height":"width"]:0},handleOverflow:function(a){var p=this.axis,m=p.options.labels,e=a.x,c=p.chart.chartWidth,k=p.chart.spacing,b=l(p.labelLeft,Math.min(p.pos,k[3])),k=l(p.labelRight,Math.max(p.isRadial?0:p.pos+p.len,c-k[1])),g=this.label,n=this.rotation,t={left:0,center:.5,right:1}[p.labelAlign||g.attr("align")],f=g.getBBox().width,u=p.getSlotWidth(),D=u,q=1,A,y={};if(n||!1===m.overflow)0>n&&e-t*f<b?A=Math.round(e/Math.cos(n*w)-b):0<n&&e+t*f>k&&(A=Math.round((c-e)/Math.cos(n*w)));else if(c=e+(1-
t)*f,e-t*f<b?D=a.x+D*(1-t)-b:c>k&&(D=k-a.x+D*t,q=-1),D=Math.min(u,D),D<u&&"center"===p.labelAlign&&(a.x+=q*(u-D-t*(u-Math.min(f,D)))),f>D||p.autoRotation&&(g.styles||{}).width)A=D;A&&(y.width=A,(m.style||{}).textOverflow||(y.textOverflow="ellipsis"),g.css(y))},getPosition:function(l,m,B,e){var c=this.axis,k=c.chart,b=e&&k.oldChartHeight||k.chartHeight;return{x:l?a.correctFloat(c.translate(m+B,null,null,e)+c.transB):c.left+c.offset+(c.opposite?(e&&k.oldChartWidth||k.chartWidth)-c.right-c.left:0),y:l?
b-c.bottom+c.offset-(c.opposite?c.height:0):a.correctFloat(b-c.translate(m+B,null,null,e)-c.transB)}},getLabelPosition:function(a,l,m,e,c,k,b,g){var n=this.axis,t=n.transA,f=n.reversed,u=n.staggerLines,D=n.tickRotCorr||{x:0,y:0},q=c.y,A=e||n.reserveSpaceDefault?0:-n.labelOffset*("center"===n.labelAlign?.5:1);E(q)||(q=0===n.side?m.rotation?-8:-m.getBBox().height:2===n.side?D.y+8:Math.cos(m.rotation*w)*(D.y-m.getBBox(!1,0).height/2));a=a+c.x+A+D.x-(k&&e?k*t*(f?-1:1):0);l=l+q-(k&&!e?k*t*(f?1:-1):0);
u&&(m=b/(g||1)%u,n.opposite&&(m=u-m-1),l+=n.labelOffset/u*m);return{x:a,y:Math.round(l)}},getMarkPath:function(a,l,m,e,c,k){return k.crispLine(["M",a,l,"L",a+(c?0:-m),l+(c?m:0)],e)},renderGridLine:function(a,l,m){var e=this.axis,c=e.options,k=this.gridLine,b={},g=this.pos,n=this.type,t=e.tickmarkOffset,f=e.chart.renderer,u=n?n+"Grid":"grid",D=c[u+"LineWidth"],q=c[u+"LineColor"],c=c[u+"LineDashStyle"];k||(b.stroke=q,b["stroke-width"]=D,c&&(b.dashstyle=c),n||(b.zIndex=1),a&&(b.opacity=0),this.gridLine=
k=f.path().attr(b).addClass("highcharts-"+(n?n+"-":"")+"grid-line").add(e.gridGroup));if(!a&&k&&(a=e.getPlotLinePath(g+t,k.strokeWidth()*m,a,!0)))k[this.isNew?"attr":"animate"]({d:a,opacity:l})},renderMark:function(a,m,B){var e=this.axis,c=e.options,k=e.chart.renderer,b=this.type,g=b?b+"Tick":"tick",n=e.tickSize(g),t=this.mark,f=!t,u=a.x;a=a.y;var D=l(c[g+"Width"],!b&&e.isXAxis?1:0),c=c[g+"Color"];n&&(e.opposite&&(n[0]=-n[0]),f&&(this.mark=t=k.path().addClass("highcharts-"+(b?b+"-":"")+"tick").add(e.axisGroup),
t.attr({stroke:c,"stroke-width":D})),t[f?"attr":"animate"]({d:this.getMarkPath(u,a,n[0],t.strokeWidth()*B,e.horiz,k),opacity:m}))},renderLabel:function(a,m,B,e){var c=this.axis,k=c.horiz,b=c.options,g=this.label,n=b.labels,t=n.step,c=c.tickmarkOffset,f=!0,u=a.x;a=a.y;g&&r(u)&&(g.xy=a=this.getLabelPosition(u,a,g,k,n,c,e,t),this.isFirst&&!this.isLast&&!l(b.showFirstLabel,1)||this.isLast&&!this.isFirst&&!l(b.showLastLabel,1)?f=!1:!k||n.step||n.rotation||m||0===B||this.handleOverflow(a),t&&e%t&&(f=!1),
f&&r(a.y)?(a.opacity=B,g[this.isNewLabel?"attr":"animate"](a),this.isNewLabel=!1):(g.attr("y",-9999),this.isNewLabel=!0))},render:function(m,v,B){var e=this.axis,c=e.horiz,k=this.getPosition(c,this.pos,e.tickmarkOffset,v),b=k.x,g=k.y,e=c&&b===e.pos+e.len||!c&&g===e.pos?-1:1;B=l(B,1);this.isActive=!0;this.renderGridLine(v,B,e);this.renderMark(k,B,e);this.renderLabel(k,v,B,m);this.isNew=!1;a.fireEvent(this,"afterRender")},destroy:function(){F(this,this.axis)}}})(L);var da=function(a){var C=a.addEvent,
E=a.animObject,F=a.arrayMax,r=a.arrayMin,m=a.color,l=a.correctFloat,w=a.defaultOptions,p=a.defined,v=a.deg2rad,B=a.destroyObjectProperties,e=a.each,c=a.extend,k=a.fireEvent,b=a.format,g=a.getMagnitude,n=a.grep,t=a.inArray,f=a.isArray,u=a.isNumber,D=a.isString,q=a.merge,A=a.normalizeTickInterval,y=a.objectEach,H=a.pick,I=a.removeEvent,J=a.splat,d=a.syncTimeout,z=a.Tick,G=function(){this.init.apply(this,arguments)};a.extend(G.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",
second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},x:0},maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",minPadding:.01,startOfWeek:1,startOnTick:!1,tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",
lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,formatter:function(){return a.numberFormat(this.total,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},
defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},init:function(a,d){var h=d.isX,x=this;x.chart=a;x.horiz=a.inverted&&!x.isZAxis?!h:h;x.isXAxis=h;x.coll=x.coll||(h?"xAxis":"yAxis");x.opposite=d.opposite;x.side=d.side||(x.horiz?x.opposite?0:2:x.opposite?1:3);x.setOptions(d);var b=this.options,f=b.type;x.labelFormatter=b.labels.formatter||
x.defaultLabelFormatter;x.userOptions=d;x.minPixelPadding=0;x.reversed=b.reversed;x.visible=!1!==b.visible;x.zoomEnabled=!1!==b.zoomEnabled;x.hasNames="category"===f||!0===b.categories;x.categories=b.categories||x.hasNames;x.names||(x.names=[],x.names.keys={});x.plotLinesAndBandsGroups={};x.isLog="logarithmic"===f;x.isDatetimeAxis="datetime"===f;x.positiveValuesOnly=x.isLog&&!x.allowNegativeLog;x.isLinked=p(b.linkedTo);x.ticks={};x.labelEdge=[];x.minorTicks={};x.plotLinesAndBands=[];x.alternateBands=
{};x.len=0;x.minRange=x.userMinRange=b.minRange||b.maxZoom;x.range=b.range;x.offset=b.offset||0;x.stacks={};x.oldStacks={};x.stacksTouched=0;x.max=null;x.min=null;x.crosshair=H(b.crosshair,J(a.options.tooltip.crosshairs)[h?0:1],!1);d=x.options.events;-1===t(x,a.axes)&&(h?a.axes.splice(a.xAxis.length,0,x):a.axes.push(x),a[x.coll].push(x));x.series=x.series||[];a.inverted&&!x.isZAxis&&h&&void 0===x.reversed&&(x.reversed=!0);y(d,function(a,h){C(x,h,a)});x.lin2log=b.linearToLogConverter||x.lin2log;x.isLog&&
(x.val2lin=x.log2lin,x.lin2val=x.lin2log)},setOptions:function(a){this.options=q(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],q(w[this.coll],a))},defaultLabelFormatter:function(){var h=this.axis,d=this.value,f=h.chart.time,c=h.categories,g=this.dateTimeLabelFormat,e=w.lang,z=e.numericSymbols,e=e.numericSymbolMagnitude||1E3,n=z&&z.length,q,u=h.options.labels.format,
h=h.isLog?Math.abs(d):h.tickInterval;if(u)q=b(u,this,f);else if(c)q=d;else if(g)q=f.dateFormat(g,d);else if(n&&1E3<=h)for(;n--&&void 0===q;)f=Math.pow(e,n+1),h>=f&&0===10*d%f&&null!==z[n]&&0!==d&&(q=a.numberFormat(d/f,-1)+z[n]);void 0===q&&(q=1E4<=Math.abs(d)?a.numberFormat(d,-1):a.numberFormat(d,-1,void 0,""));return q},getSeriesExtremes:function(){var a=this,d=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();e(a.series,
function(h){if(h.visible||!d.options.chart.ignoreHiddenSeries){var x=h.options,b=x.threshold,f;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=b&&(b=null);if(a.isXAxis)x=h.xData,x.length&&(h=r(x),f=F(x),u(h)||h instanceof Date||(x=n(x,u),h=r(x),f=F(x)),x.length&&(a.dataMin=Math.min(H(a.dataMin,x[0],h),h),a.dataMax=Math.max(H(a.dataMax,x[0],f),f)));else if(h.getExtremes(),f=h.dataMax,h=h.dataMin,p(h)&&p(f)&&(a.dataMin=Math.min(H(a.dataMin,h),h),a.dataMax=Math.max(H(a.dataMax,f),f)),p(b)&&(a.threshold=
b),!x.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})},translate:function(a,d,b,f,c,g){var h=this.linkedParent||this,x=1,e=0,z=f?h.oldTransA:h.transA;f=f?h.oldMin:h.min;var q=h.minPixelPadding;c=(h.isOrdinal||h.isBroken||h.isLog&&c)&&h.lin2val;z||(z=h.transA);b&&(x*=-1,e=h.len);h.reversed&&(x*=-1,e-=x*(h.sector||h.len));d?(a=(a*x+e-q)/z+f,c&&(a=h.lin2val(a))):(c&&(a=h.val2lin(a)),a=u(f)?x*(a-f)*z+e+x*q+(u(g)?z*g:0):void 0);return a},toPixels:function(a,d){return this.translate(a,!1,!this.horiz,
null,!0)+(d?0:this.pos)},toValue:function(a,d){return this.translate(a-(d?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,d,b,f,c){var h=this.chart,x=this.left,g=this.top,e,z,q=b&&h.oldChartHeight||h.chartHeight,n=b&&h.oldChartWidth||h.chartWidth,t;e=this.transB;var A=function(a,h,d){if(a<h||a>d)f?a=Math.min(Math.max(h,a),d):t=!0;return a};c=H(c,this.translate(a,null,null,b));c=Math.min(Math.max(-1E5,c),1E5);a=b=Math.round(c+e);e=z=Math.round(q-c-e);u(c)?this.horiz?(e=g,z=q-this.bottom,
a=b=A(a,x,x+this.width)):(a=x,b=n-this.right,e=z=A(e,g,g+this.height)):(t=!0,f=!1);return t&&!f?null:h.renderer.crispLine(["M",a,e,"L",b,z],d||1)},getLinearTickPositions:function(a,d,b){var h,x=l(Math.floor(d/a)*a);b=l(Math.ceil(b/a)*a);var f=[],c;l(x+a)===x&&(c=20);if(this.single)return[d];for(d=x;d<=b;){f.push(d);d=l(d+a,c);if(d===h)break;h=d}return f},getMinorTickInterval:function(){var a=this.options;return!0===a.minorTicks?H(a.minorTickInterval,"auto"):!1===a.minorTicks?null:a.minorTickInterval},
getMinorTickPositions:function(){var a=this,d=a.options,b=a.tickPositions,f=a.minorTickInterval,c=[],g=a.pointRangePadding||0,z=a.min-g,g=a.max+g,q=g-z;if(q&&q/f<a.len/3)if(a.isLog)e(this.paddedTicks,function(h,d,x){d&&c.push.apply(c,a.getLogTickPositions(f,x[d-1],x[d],!0))});else if(a.isDatetimeAxis&&"auto"===this.getMinorTickInterval())c=c.concat(a.getTimeTicks(a.normalizeTimeTickInterval(f),z,g,d.startOfWeek));else for(d=z+(b[0]-z)%f;d<=g&&d!==c[0];d+=f)c.push(d);0!==c.length&&a.trimTicks(c);return c},
adjustForMinRange:function(){var a=this.options,d=this.min,b=this.max,f,c,g,z,q,n,u,t;this.isXAxis&&void 0===this.minRange&&!this.isLog&&(p(a.min)||p(a.max)?this.minRange=null:(e(this.series,function(a){n=a.xData;for(z=u=a.xIncrement?1:n.length-1;0<z;z--)if(q=n[z]-n[z-1],void 0===g||q<g)g=q}),this.minRange=Math.min(5*g,this.dataMax-this.dataMin)));b-d<this.minRange&&(c=this.dataMax-this.dataMin>=this.minRange,t=this.minRange,f=(t-b+d)/2,f=[d-f,H(a.min,d-f)],c&&(f[2]=this.isLog?this.log2lin(this.dataMin):
this.dataMin),d=F(f),b=[d+t,H(a.max,d+t)],c&&(b[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),b=r(b),b-d<t&&(f[0]=b-t,f[1]=H(a.min,b-t),d=F(f)));this.min=d;this.max=b},getClosest:function(){var a;this.categories?a=1:e(this.series,function(h){var d=h.closestPointRange,x=h.visible||!h.chart.options.chart.ignoreHiddenSeries;!h.noSharedTooltip&&p(d)&&x&&(a=p(a)?Math.min(a,d):d)});return a},nameToX:function(a){var h=f(this.categories),d=h?this.categories:this.names,b=a.options.x,c;a.series.requireSorting=
!1;p(b)||(b=!1===this.options.uniqueNames?a.series.autoIncrement():h?t(a.name,d):H(d.keys[a.name],-1));-1===b?h||(c=d.length):c=b;void 0!==c&&(this.names[c]=a.name,this.names.keys[a.name]=c);return c},updateNames:function(){var h=this,d=this.names;0<d.length&&(e(a.keys(d.keys),function(a){delete d.keys[a]}),d.length=0,this.minRange=this.userMinRange,e(this.series||[],function(a){a.xIncrement=null;if(!a.points||a.isDirtyData)a.processData(),a.generatePoints();e(a.points,function(d,b){var x;d.options&&
(x=h.nameToX(d),void 0!==x&&x!==d.x&&(d.x=x,a.xData[b]=x))})}))},setAxisTranslation:function(a){var h=this,d=h.max-h.min,b=h.axisPointRange||0,f,c=0,g=0,z=h.linkedParent,q=!!h.categories,n=h.transA,t=h.isXAxis;if(t||q||b)f=h.getClosest(),z?(c=z.minPointOffset,g=z.pointRangePadding):e(h.series,function(a){var d=q?1:t?H(a.options.pointRange,f,0):h.axisPointRange||0;a=a.options.pointPlacement;b=Math.max(b,d);h.single||(c=Math.max(c,D(a)?0:d/2),g=Math.max(g,"on"===a?0:d))}),z=h.ordinalSlope&&f?h.ordinalSlope/
f:1,h.minPointOffset=c*=z,h.pointRangePadding=g*=z,h.pointRange=Math.min(b,d),t&&(h.closestPointRange=f);a&&(h.oldTransA=n);h.translationSlope=h.transA=n=h.options.staticScale||h.len/(d+g||1);h.transB=h.horiz?h.left:h.bottom;h.minPixelPadding=n*c},minFromRange:function(){return this.max-this.range},setTickInterval:function(h){var d=this,b=d.chart,f=d.options,c=d.isLog,z=d.log2lin,q=d.isDatetimeAxis,n=d.isXAxis,t=d.isLinked,G=f.maxPadding,y=f.minPadding,I=f.tickInterval,D=f.tickPixelInterval,J=d.categories,
m=d.threshold,v=d.softThreshold,B,w,r,C;q||J||t||this.getTickAmount();r=H(d.userMin,f.min);C=H(d.userMax,f.max);t?(d.linkedParent=b[d.coll][f.linkedTo],b=d.linkedParent.getExtremes(),d.min=H(b.min,b.dataMin),d.max=H(b.max,b.dataMax),f.type!==d.linkedParent.options.type&&a.error(11,1)):(!v&&p(m)&&(d.dataMin>=m?(B=m,y=0):d.dataMax<=m&&(w=m,G=0)),d.min=H(r,B,d.dataMin),d.max=H(C,w,d.dataMax));c&&(d.positiveValuesOnly&&!h&&0>=Math.min(d.min,H(d.dataMin,d.min))&&a.error(10,1),d.min=l(z(d.min),15),d.max=
l(z(d.max),15));d.range&&p(d.max)&&(d.userMin=d.min=r=Math.max(d.dataMin,d.minFromRange()),d.userMax=C=d.max,d.range=null);k(d,"foundExtremes");d.beforePadding&&d.beforePadding();d.adjustForMinRange();!(J||d.axisPointRange||d.usePercentage||t)&&p(d.min)&&p(d.max)&&(z=d.max-d.min)&&(!p(r)&&y&&(d.min-=z*y),!p(C)&&G&&(d.max+=z*G));u(f.softMin)&&!u(d.userMin)&&(d.min=Math.min(d.min,f.softMin));u(f.softMax)&&!u(d.userMax)&&(d.max=Math.max(d.max,f.softMax));u(f.floor)&&(d.min=Math.max(d.min,f.floor));u(f.ceiling)&&
(d.max=Math.min(d.max,f.ceiling));v&&p(d.dataMin)&&(m=m||0,!p(r)&&d.min<m&&d.dataMin>=m?d.min=m:!p(C)&&d.max>m&&d.dataMax<=m&&(d.max=m));d.tickInterval=d.min===d.max||void 0===d.min||void 0===d.max?1:t&&!I&&D===d.linkedParent.options.tickPixelInterval?I=d.linkedParent.tickInterval:H(I,this.tickAmount?(d.max-d.min)/Math.max(this.tickAmount-1,1):void 0,J?1:(d.max-d.min)*D/Math.max(d.len,D));n&&!h&&e(d.series,function(a){a.processData(d.min!==d.oldMin||d.max!==d.oldMax)});d.setAxisTranslation(!0);d.beforeSetTickPositions&&
d.beforeSetTickPositions();d.postProcessTickInterval&&(d.tickInterval=d.postProcessTickInterval(d.tickInterval));d.pointRange&&!I&&(d.tickInterval=Math.max(d.pointRange,d.tickInterval));h=H(f.minTickInterval,d.isDatetimeAxis&&d.closestPointRange);!I&&d.tickInterval<h&&(d.tickInterval=h);q||c||I||(d.tickInterval=A(d.tickInterval,null,g(d.tickInterval),H(f.allowDecimals,!(.5<d.tickInterval&&5>d.tickInterval&&1E3<d.max&&9999>d.max)),!!this.tickAmount));this.tickAmount||(d.tickInterval=d.unsquish());
this.setTickPositions()},setTickPositions:function(){var a=this.options,d,b=a.tickPositions;d=this.getMinorTickInterval();var f=a.tickPositioner,c=a.startOnTick,g=a.endOnTick;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval="auto"===d&&this.tickInterval?this.tickInterval/5:d;this.single=this.min===this.max&&p(this.min)&&!this.tickAmount&&(parseInt(this.min,10)===this.min||!1!==a.allowDecimals);this.tickPositions=d=b&&b.slice();
!d&&(d=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,this.min,this.max),d.length>this.len&&(d=[d[0],d.pop()],d[0]===d[1]&&(d.length=1)),this.tickPositions=d,f&&(f=f.apply(this,[this.min,this.max])))&&(this.tickPositions=d=f);this.paddedTicks=d.slice(0);
this.trimTicks(d,c,g);this.isLinked||(this.single&&2>d.length&&(this.min-=.5,this.max+=.5),b||f||this.adjustTickAmount())},trimTicks:function(a,d,b){var h=a[0],f=a[a.length-1],c=this.minPointOffset||0;if(!this.isLinked){if(d&&-Infinity!==h)this.min=h;else for(;this.min-c>a[0];)a.shift();if(b)this.max=f;else for(;this.max+c<a[a.length-1];)a.pop();0===a.length&&p(h)&&!this.options.tickPositions&&a.push((f+h)/2)}},alignToOthers:function(){var a={},d,b=this.options;!1===this.chart.options.chart.alignTicks||
!1===b.alignTicks||this.isLog||e(this.chart[this.coll],function(h){var b=h.options,b=[h.horiz?b.left:b.top,b.width,b.height,b.pane].join();h.series.length&&(a[b]?d=!0:a[b]=1)});return d},getTickAmount:function(){var a=this.options,d=a.tickAmount,b=a.tickPixelInterval;!p(a.tickInterval)&&this.len<b&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(d=2);!d&&this.alignToOthers()&&(d=Math.ceil(this.len/b)+1);4>d&&(this.finalTickAmt=d,d=5);this.tickAmount=d},adjustTickAmount:function(){var a=
this.tickInterval,d=this.tickPositions,b=this.tickAmount,f=this.finalTickAmt,c=d&&d.length,g=H(this.threshold,this.softThreshold?0:null);if(this.hasData()){if(c<b){for(;d.length<b;)d.length%2||this.min===g?d.push(l(d[d.length-1]+a)):d.unshift(l(d[0]-a));this.transA*=(c-1)/(b-1);this.min=d[0];this.max=d[d.length-1]}else c>b&&(this.tickInterval*=2,this.setTickPositions());if(p(f)){for(a=b=d.length;a--;)(3===f&&1===a%2||2>=f&&0<a&&a<b-1)&&d.splice(a,1);this.finalTickAmt=void 0}}},setScale:function(){var a,
d;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();d=this.len!==this.oldAxisLength;e(this.series,function(d){if(d.isDirtyData||d.isDirty||d.xAxis.isDirty)a=!0});d||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||
(this.isDirty=d||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks();k(this,"afterSetScale")},setExtremes:function(a,d,b,f,g){var h=this,z=h.chart;b=H(b,!0);e(h.series,function(a){delete a.kdTree});g=c(g,{min:a,max:d});k(h,"setExtremes",g,function(){h.userMin=a;h.userMax=d;h.eventArgs=g;b&&z.redraw(f)})},zoom:function(a,d){var h=this.dataMin,b=this.dataMax,f=this.options,c=Math.min(h,H(f.min,h)),f=Math.max(b,H(f.max,b));if(a!==this.min||d!==this.max)this.allowZoomOutside||
(p(h)&&(a<c&&(a=c),a>f&&(a=f)),p(b)&&(d<c&&(d=c),d>f&&(d=f))),this.displayBtn=void 0!==a||void 0!==d,this.setExtremes(a,d,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var d=this.chart,b=this.options,f=b.offsets||[0,0,0,0],c=this.horiz,g=this.width=Math.round(a.relativeLength(H(b.width,d.plotWidth-f[3]+f[1]),d.plotWidth)),z=this.height=Math.round(a.relativeLength(H(b.height,d.plotHeight-f[0]+f[2]),d.plotHeight)),e=this.top=Math.round(a.relativeLength(H(b.top,d.plotTop+f[0]),d.plotHeight,
d.plotTop)),b=this.left=Math.round(a.relativeLength(H(b.left,d.plotLeft+f[3]),d.plotWidth,d.plotLeft));this.bottom=d.chartHeight-z-e;this.right=d.chartWidth-g-b;this.len=Math.max(c?g:z,0);this.pos=c?b:e},getExtremes:function(){var a=this.isLog,d=this.lin2log;return{min:a?l(d(this.min)):this.min,max:a?l(d(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var d=this.isLog,h=this.lin2log,b=d?h(this.min):this.min,d=d?h(this.max):
this.max;null===a?a=b:b>a?a=b:d<a&&(a=d);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(H(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var d=this.options,h=d[a+"Length"],b=H(d[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(b&&h)return"inside"===d[a+"Position"]&&(h=-h),[h,b]},labelMetrics:function(){var a=this.tickPositions&&this.tickPositions[0]||0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,
this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,d=this.horiz,b=this.tickInterval,f=b,c=this.len/(((this.categories?1:0)+this.max-this.min)/b),g,z=a.rotation,q=this.labelMetrics(),n,t=Number.MAX_VALUE,u,A=function(a){a/=c||1;a=1<a?Math.ceil(a):1;return a*b};d?(u=!a.staggerLines&&!a.step&&(p(z)?[z]:c<H(a.autoRotationLimit,80)&&a.autoRotation))&&e(u,function(a){var d;if(a===z||a&&-90<=a&&90>=a)n=A(Math.abs(q.h/Math.sin(v*a))),d=n+Math.abs(a/360),d<t&&(t=d,g=a,f=n)}):
a.step||(f=A(q.h));this.autoRotation=u;this.labelRotation=H(g,z);return f},getSlotWidth:function(){var a=this.chart,d=this.horiz,b=this.options.labels,f=Math.max(this.tickPositions.length-(this.categories?0:1),1),c=a.margin[3];return d&&2>(b.step||0)&&!b.rotation&&(this.staggerLines||1)*this.len/f||!d&&(b.style&&parseInt(b.style.width,10)||c&&c-a.spacing[3]||.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,d=a.renderer,b=this.tickPositions,f=this.ticks,c=this.options.labels,g=this.horiz,
z=this.getSlotWidth(),q=Math.max(1,Math.round(z-2*(c.padding||5))),n={},t=this.labelMetrics(),u=c.style&&c.style.textOverflow,A,G,y=0,I;D(c.rotation)||(n.rotation=c.rotation||0);e(b,function(a){(a=f[a])&&a.label&&a.label.textPxLength>y&&(y=a.label.textPxLength)});this.maxLabelLength=y;if(this.autoRotation)y>q&&y>t.h?n.rotation=this.labelRotation:this.labelRotation=0;else if(z&&(A=q,!u))for(G="clip",q=b.length;!g&&q--;)if(I=b[q],I=f[I].label)I.styles&&"ellipsis"===I.styles.textOverflow?I.css({textOverflow:"clip"}):
I.textPxLength>z&&I.css({width:z+"px"}),I.getBBox().height>this.len/b.length-(t.h-t.f)&&(I.specificTextOverflow="ellipsis");n.rotation&&(A=y>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight,u||(G="ellipsis"));if(this.labelAlign=c.align||this.autoLabelAlign(this.labelRotation))n.align=this.labelAlign;e(b,function(a){var d=(a=f[a])&&a.label;d&&(d.attr(n),!A||c.style&&c.style.width||!(A<d.textPxLength||"SPAN"===d.element.tagName)||d.css({width:A,textOverflow:d.specificTextOverflow||G}),delete d.specificTextOverflow,
a.rotation=n.rotation)});this.tickRotCorr=d.rotCorr(t.b,this.labelRotation||0,0!==this.side)},hasData:function(){return this.hasVisibleSeries||p(this.min)&&p(this.max)&&this.tickPositions&&0<this.tickPositions.length},addTitle:function(a){var d=this.chart.renderer,h=this.horiz,b=this.opposite,f=this.options.title,c;this.axisTitle||((c=f.textAlign)||(c=(h?{low:"left",middle:"center",high:"right"}:{low:b?"right":"left",middle:"center",high:b?"left":"right"})[f.align]),this.axisTitle=d.text(f.text,0,
0,f.useHTML).attr({zIndex:7,rotation:f.rotation||0,align:c}).addClass("highcharts-axis-title").css(q(f.style)).add(this.axisGroup),this.axisTitle.isNew=!0);f.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](!0)},generateTick:function(a){var d=this.ticks;d[a]?d[a].addLabel():d[a]=new z(this,a)},getOffset:function(){var a=this,d=a.chart,b=d.renderer,f=a.options,c=a.tickPositions,g=a.ticks,z=a.horiz,q=a.side,n=d.inverted&&!a.isZAxis?[1,0,3,2][q]:q,t,u,
A=0,G,I=0,k=f.title,D=f.labels,J=0,l=d.axisOffset,d=d.clipOffset,m=[-1,1,1,-1][q],v=f.className,B=a.axisParent,w=this.tickSize("tick");t=a.hasData();a.showAxis=u=t||H(f.showEmpty,!0);a.staggerLines=a.horiz&&D.staggerLines;a.axisGroup||(a.gridGroup=b.g("grid").attr({zIndex:f.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(v||"")).add(B),a.axisGroup=b.g("axis").attr({zIndex:f.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+" "+(v||"")).add(B),a.labelGroup=b.g("axis-labels").attr({zIndex:D.zIndex||
7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(v||"")).add(B));t||a.isLinked?(e(c,function(d,b){a.generateTick(d,b)}),a.renderUnsquish(),a.reserveSpaceDefault=0===q||2===q||{1:"left",3:"right"}[q]===a.labelAlign,H(D.reserveSpace,"center"===a.labelAlign?!0:null,a.reserveSpaceDefault)&&e(c,function(a){J=Math.max(g[a].getLabelSize(),J)}),a.staggerLines&&(J*=a.staggerLines),a.labelOffset=J*(a.opposite?-1:1)):y(g,function(a,d){a.destroy();delete g[d]});k&&k.text&&!1!==k.enabled&&(a.addTitle(u),
u&&!1!==k.reserveSpace&&(a.titleOffset=A=a.axisTitle.getBBox()[z?"height":"width"],G=k.offset,I=p(G)?0:H(k.margin,z?5:10)));a.renderLine();a.offset=m*H(f.offset,l[q]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};b=0===q?-a.labelMetrics().h:2===q?a.tickRotCorr.y:0;I=Math.abs(J)+I;J&&(I=I-b+m*(z?H(D.y,a.tickRotCorr.y+8*m):D.x));a.axisTitleMargin=H(G,I);l[q]=Math.max(l[q],a.axisTitleMargin+A+m*a.offset,I,t&&c.length&&w?w[0]+m*a.offset:0);f=f.offset?0:2*Math.floor(a.axisLine.strokeWidth()/2);d[n]=Math.max(d[n],
f)},getLinePath:function(a){var d=this.chart,b=this.opposite,h=this.offset,f=this.horiz,c=this.left+(b?this.width:0)+h,h=d.chartHeight-this.bottom-(b?this.height:0)+h;b&&(a*=-1);return d.renderer.crispLine(["M",f?this.left:c,f?h:this.top,"L",f?d.chartWidth-this.right:c,f?h:d.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,
zIndex:7}))},getTitlePosition:function(){var a=this.horiz,d=this.left,b=this.top,f=this.len,c=this.options.title,g=a?d:b,z=this.opposite,e=this.offset,q=c.x||0,n=c.y||0,t=this.axisTitle,u=this.chart.renderer.fontMetrics(c.style&&c.style.fontSize,t),t=Math.max(t.getBBox(null,0).height-u.h-1,0),f={low:g+(a?0:f),middle:g+f/2,high:g+(a?f:0)}[c.align],d=(a?b+this.height:d)+(a?1:-1)*(z?-1:1)*this.axisTitleMargin+[-t,t,u.f,-t][this.side];return{x:a?f+q:d+(z?this.width:0)+e+q,y:a?d+n-(z?this.height:0)+e:
f+n}},renderMinorTick:function(a){var d=this.chart.hasRendered&&u(this.oldMin),b=this.minorTicks;b[a]||(b[a]=new z(this,a,"minor"));d&&b[a].isNew&&b[a].render(null,!0);b[a].render(null,!1,1)},renderTick:function(a,d){var b=this.isLinked,h=this.ticks,f=this.chart.hasRendered&&u(this.oldMin);if(!b||a>=this.min&&a<=this.max)h[a]||(h[a]=new z(this,a)),f&&h[a].isNew&&h[a].render(d,!0,.1),h[a].render(d)},render:function(){var b=this,f=b.chart,c=b.options,g=b.isLog,q=b.lin2log,n=b.isLinked,t=b.tickPositions,
A=b.axisTitle,I=b.ticks,G=b.minorTicks,k=b.alternateBands,D=c.stackLabels,J=c.alternateGridColor,H=b.tickmarkOffset,l=b.axisLine,m=b.showAxis,p=E(f.renderer.globalAnimation),v,B;b.labelEdge.length=0;b.overlap=!1;e([I,G,k],function(a){y(a,function(a){a.isActive=!1})});if(b.hasData()||n)b.minorTickInterval&&!b.categories&&e(b.getMinorTickPositions(),function(a){b.renderMinorTick(a)}),t.length&&(e(t,function(a,d){b.renderTick(a,d)}),H&&(0===b.min||b.single)&&(I[-1]||(I[-1]=new z(b,-1,null,!0)),I[-1].render(-1))),
J&&e(t,function(d,h){B=void 0!==t[h+1]?t[h+1]+H:b.max-H;0===h%2&&d<b.max&&B<=b.max+(f.polar?-H:H)&&(k[d]||(k[d]=new a.PlotLineOrBand(b)),v=d+H,k[d].options={from:g?q(v):v,to:g?q(B):B,color:J},k[d].render(),k[d].isActive=!0)}),b._addedPlotLB||(e((c.plotLines||[]).concat(c.plotBands||[]),function(a){b.addPlotBandOrLine(a)}),b._addedPlotLB=!0);e([I,G,k],function(a){var b,h=[],c=p.duration;y(a,function(a,d){a.isActive||(a.render(d,!1,0),a.isActive=!1,h.push(d))});d(function(){for(b=h.length;b--;)a[h[b]]&&
!a[h[b]].isActive&&(a[h[b]].destroy(),delete a[h[b]])},a!==k&&f.hasRendered&&c?c:0)});l&&(l[l.isPlaced?"animate":"attr"]({d:this.getLinePath(l.strokeWidth())}),l.isPlaced=!0,l[m?"show":"hide"](!0));A&&m&&(c=b.getTitlePosition(),u(c.y)?(A[A.isNew?"attr":"animate"](c),A.isNew=!1):(A.attr("y",-9999),A.isNew=!0));D&&D.enabled&&b.renderStackTotals();b.isDirty=!1},redraw:function(){this.visible&&(this.render(),e(this.plotLinesAndBands,function(a){a.render()}));e(this.series,function(a){a.isDirty=!0})},
keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var d=this,b=d.stacks,h=d.plotLinesAndBands,f;a||I(d);y(b,function(a,d){B(a);b[d]=null});e([d.ticks,d.minorTicks,d.alternateBands],function(a){B(a)});if(h)for(a=h.length;a--;)h[a].destroy();e("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),function(a){d[a]&&(d[a]=d[a].destroy())});for(f in d.plotLinesAndBandsGroups)d.plotLinesAndBandsGroups[f]=d.plotLinesAndBandsGroups[f].destroy();
y(d,function(a,b){-1===t(b,d.keepProps)&&delete d[b]})},drawCrosshair:function(a,d){var b,f=this.crosshair,h=H(f.snap,!0),c,g=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(p(d)||!h)?(h?p(d)&&(c=this.isXAxis?d.plotX:this.len-d.plotY):c=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),p(c)&&(b=this.getPlotLinePath(d&&(this.isXAxis?d.x:H(d.stackY,d.y)),null,null,null,c)||null),p(b)?(d=this.categories&&!this.isRadial,g||(this.cross=g=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+
(d?"category ":"thin ")+f.className).attr({zIndex:H(f.zIndex,2)}).add(),g.attr({stroke:f.color||(d?m("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":H(f.width,1)}).css({"pointer-events":"none"}),f.dashStyle&&g.attr({dashstyle:f.dashStyle})),g.show().attr({d:b}),d&&!f.width&&g.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&this.cross.hide()}});return a.Axis=G}(L);(function(a){var C=a.Axis,E=a.getMagnitude,
F=a.normalizeTickInterval,r=a.timeUnits;C.prototype.getTimeTicks=function(){return this.chart.time.getTimeTicks.apply(this.chart.time,arguments)};C.prototype.normalizeTimeTickInterval=function(a,l){var m=l||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];l=m[m.length-1];var p=r[l[0]],v=l[1],B;for(B=0;B<m.length&&!(l=m[B],p=r[l[0]],v=l[1],m[B+1]&&a<=(p*
v[v.length-1]+r[m[B+1][0]])/2);B++);p===r.year&&a<5*p&&(v=[1,2,5]);a=F(a/p,v,"year"===l[0]?Math.max(E(a/p),1):1);return{unitRange:p,count:a,unitName:l[0]}}})(L);(function(a){var C=a.Axis,E=a.getMagnitude,F=a.map,r=a.normalizeTickInterval,m=a.pick;C.prototype.getLogTickPositions=function(a,w,p,v){var l=this.options,e=this.len,c=this.lin2log,k=this.log2lin,b=[];v||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),b=this.getLinearTickPositions(a,w,p);else if(.08<=a)for(var e=Math.floor(w),g,n,
t,f,u,l=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];e<p+1&&!u;e++)for(n=l.length,g=0;g<n&&!u;g++)t=k(c(e)*l[g]),t>w&&(!v||f<=p)&&void 0!==f&&b.push(f),f>p&&(u=!0),f=t;else w=c(w),p=c(p),a=v?this.getMinorTickInterval():l.tickInterval,a=m("auto"===a?null:a,this._minorAutoInterval,l.tickPixelInterval/(v?5:1)*(p-w)/((v?e/this.tickPositions.length:e)||1)),a=r(a,null,E(a)),b=F(this.getLinearTickPositions(a,w,p),k),v||(this._minorAutoInterval=a/5);v||(this.tickInterval=a);return b};C.prototype.log2lin=
function(a){return Math.log(a)/Math.LN10};C.prototype.lin2log=function(a){return Math.pow(10,a)}})(L);(function(a,C){var E=a.arrayMax,F=a.arrayMin,r=a.defined,m=a.destroyObjectProperties,l=a.each,w=a.erase,p=a.merge,v=a.pick;a.PlotLineOrBand=function(a,e){this.axis=a;e&&(this.options=e,this.id=e.id)};a.PlotLineOrBand.prototype={render:function(){var l=this,e=l.axis,c=e.horiz,k=l.options,b=k.label,g=l.label,n=k.to,t=k.from,f=k.value,u=r(t)&&r(n),D=r(f),q=l.svgElem,A=!q,y=[],H=k.color,I=v(k.zIndex,
0),J=k.events,y={"class":"highcharts-plot-"+(u?"band ":"line ")+(k.className||"")},d={},z=e.chart.renderer,G=u?"bands":"lines",h=e.log2lin;e.isLog&&(t=h(t),n=h(n),f=h(f));D?(y={stroke:H,"stroke-width":k.width},k.dashStyle&&(y.dashstyle=k.dashStyle)):u&&(H&&(y.fill=H),k.borderWidth&&(y.stroke=k.borderColor,y["stroke-width"]=k.borderWidth));d.zIndex=I;G+="-"+I;(H=e.plotLinesAndBandsGroups[G])||(e.plotLinesAndBandsGroups[G]=H=z.g("plot-"+G).attr(d).add());A&&(l.svgElem=q=z.path().attr(y).add(H));if(D)y=
e.getPlotLinePath(f,q.strokeWidth());else if(u)y=e.getPlotBandPath(t,n,k);else return;A&&y&&y.length?(q.attr({d:y}),J&&a.objectEach(J,function(a,d){q.on(d,function(a){J[d].apply(l,[a])})})):q&&(y?(q.show(),q.animate({d:y})):(q.hide(),g&&(l.label=g=g.destroy())));b&&r(b.text)&&y&&y.length&&0<e.width&&0<e.height&&!y.flat?(b=p({align:c&&u&&"center",x:c?!u&&4:10,verticalAlign:!c&&u&&"middle",y:c?u?16:10:u?6:-4,rotation:c&&!u&&90},b),this.renderLabel(b,y,u,I)):g&&g.hide();return l},renderLabel:function(a,
e,c,k){var b=this.label,g=this.axis.chart.renderer;b||(b={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(c?"band":"line")+"-label "+(a.className||"")},b.zIndex=k,this.label=b=g.text(a.text,0,0,a.useHTML).attr(b).add(),b.css(a.style));k=e.xBounds||[e[1],e[4],c?e[6]:e[1]];e=e.yBounds||[e[2],e[5],c?e[7]:e[2]];c=F(k);g=F(e);b.align(a,!1,{x:c,y:g,width:E(k)-c,height:E(e)-g});b.show()},destroy:function(){w(this.axis.plotLinesAndBands,this);delete this.axis;m(this)}};a.extend(C.prototype,
{getPlotBandPath:function(a,e){var c=this.getPlotLinePath(e,null,null,!0),k=this.getPlotLinePath(a,null,null,!0),b=[],g=this.horiz,n=1,t;a=a<this.min&&e<this.min||a>this.max&&e>this.max;if(k&&c)for(a&&(t=k.toString()===c.toString(),n=0),a=0;a<k.length;a+=6)g&&c[a+1]===k[a+1]?(c[a+1]+=n,c[a+4]+=n):g||c[a+2]!==k[a+2]||(c[a+2]+=n,c[a+5]+=n),b.push("M",k[a+1],k[a+2],"L",k[a+4],k[a+5],c[a+4],c[a+5],c[a+1],c[a+2],"z"),b.flat=t;return b},addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},
addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(l,e){var c=(new a.PlotLineOrBand(this,l)).render(),k=this.userOptions;c&&(e&&(k[e]=k[e]||[],k[e].push(l)),this.plotLinesAndBands.push(c));return c},removePlotBandOrLine:function(a){for(var e=this.plotLinesAndBands,c=this.options,k=this.userOptions,b=e.length;b--;)e[b].id===a&&e[b].destroy();l([c.plotLines||[],k.plotLines||[],c.plotBands||[],k.plotBands||[]],function(c){for(b=c.length;b--;)c[b].id===a&&
w(c,c[b])})},removePlotBand:function(a){this.removePlotBandOrLine(a)},removePlotLine:function(a){this.removePlotBandOrLine(a)}})})(L,da);(function(a){var C=a.each,E=a.extend,F=a.format,r=a.isNumber,m=a.map,l=a.merge,w=a.pick,p=a.splat,v=a.syncTimeout,B=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,c){this.chart=a;this.options=c;this.crosshairs=[];this.now={x:0,y:0};this.isHidden=!0;this.split=c.split&&!a.inverted;this.shared=c.shared||this.split},
cleanSplit:function(a){C(this.chart.series,function(c){var e=c&&c.tt;e&&(!e.isActive||a?c.tt=e.destroy():e.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,c=this.options;this.label||(this.split?this.label=a.g("tooltip"):(this.label=a.label("",0,0,c.shape||"callout",null,null,c.useHTML,null,"tooltip").attr({padding:c.padding,r:c.borderRadius}),this.label.attr({fill:c.backgroundColor,"stroke-width":c.borderWidth}).css(c.style).shadow(c.shadow)),this.label.attr({zIndex:8}).add());return this.label},
update:function(a){this.destroy();l(!0,this.chart.options.tooltip.userOptions,a);this.init(this.chart,l(!0,this.options,a))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,c,k,b){var g=this,e=g.now,t=!1!==g.options.animation&&!g.isHidden&&(1<Math.abs(a-e.x)||1<Math.abs(c-e.y)),f=g.followPointer||1<g.len;E(e,{x:t?(2*e.x+a)/
3:a,y:t?(e.y+c)/2:c,anchorX:f?void 0:t?(2*e.anchorX+k)/3:k,anchorY:f?void 0:t?(e.anchorY+b)/2:b});g.getLabel().attr(e);t&&(clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){g&&g.move(a,c,k,b)},32))},hide:function(a){var c=this;clearTimeout(this.hideTimer);a=w(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=v(function(){c.getLabel()[a?"fadeOut":"hide"]();c.isHidden=!0},a))},getAnchor:function(a,c){var e,b=this.chart,g=b.inverted,n=b.plotTop,t=b.plotLeft,f=0,u=
0,D,q;a=p(a);e=a[0].tooltipPos;this.followPointer&&c&&(void 0===c.chartX&&(c=b.pointer.normalize(c)),e=[c.chartX-b.plotLeft,c.chartY-n]);e||(C(a,function(a){D=a.series.yAxis;q=a.series.xAxis;f+=a.plotX+(!g&&q?q.left-t:0);u+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!g&&D?D.top-n:0)}),f/=a.length,u/=a.length,e=[g?b.plotWidth-u:f,this.shared&&!g&&1<a.length&&c?c.chartY-n:g?b.plotHeight-f:u]);return m(e,Math.round)},getPosition:function(a,c,k){var b=this.chart,g=this.distance,e={},t=b.inverted&&
k.h||0,f,u=["y",b.chartHeight,c,k.plotY+b.plotTop,b.plotTop,b.plotTop+b.plotHeight],D=["x",b.chartWidth,a,k.plotX+b.plotLeft,b.plotLeft,b.plotLeft+b.plotWidth],q=!this.followPointer&&w(k.ttBelow,!b.inverted===!!k.negative),A=function(a,d,b,f,h,c){var z=b<f-g,n=f+g+b<d,u=f-g-b;f+=g;if(q&&n)e[a]=f;else if(!q&&z)e[a]=u;else if(z)e[a]=Math.min(c-b,0>u-t?u:u-t);else if(n)e[a]=Math.max(h,f+t+b>d?f:f+t);else return!1},y=function(a,d,b,f){var h;f<g||f>d-g?h=!1:e[a]=f<b/2?1:f>d-b/2?d-b-2:f-b/2;return h},H=
function(a){var d=u;u=D;D=d;f=a},I=function(){!1!==A.apply(0,u)?!1!==y.apply(0,D)||f||(H(!0),I()):f?e.x=e.y=0:(H(!0),I())};(b.inverted||1<this.len)&&H();I();return e},defaultFormatter:function(a){var c=this.points||p(this),e;e=[a.tooltipFooterHeaderFormatter(c[0])];e=e.concat(a.bodyFormatter(c));e.push(a.tooltipFooterHeaderFormatter(c[0],!0));return e},refresh:function(a,c){var e,b=this.options,g,n=a,t,f={},u=[];e=b.formatter||this.defaultFormatter;var f=this.shared,D;b.enabled&&(clearTimeout(this.hideTimer),
this.followPointer=p(n)[0].series.tooltipOptions.followPointer,t=this.getAnchor(n,c),c=t[0],g=t[1],!f||n.series&&n.series.noSharedTooltip?f=n.getLabelConfig():(C(n,function(a){a.setState("hover");u.push(a.getLabelConfig())}),f={x:n[0].category,y:n[0].y},f.points=u,n=n[0]),this.len=u.length,f=e.call(f,this),D=n.series,this.distance=w(D.tooltipOptions.distance,16),!1===f?this.hide():(e=this.getLabel(),this.isHidden&&e.attr({opacity:1}).show(),this.split?this.renderSplit(f,p(a)):(b.style.width||e.css({width:this.chart.spacingBox.width}),
e.attr({text:f&&f.join?f.join(""):f}),e.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+w(n.colorIndex,D.colorIndex)),e.attr({stroke:b.borderColor||n.color||D.color||"#666666"}),this.updatePosition({plotX:c,plotY:g,negative:n.negative,ttBelow:n.ttBelow,h:t[2]||0})),this.isHidden=!1))},renderSplit:function(e,c){var k=this,b=[],g=this.chart,n=g.renderer,t=!0,f=this.options,u=0,D=this.getLabel();a.isString(e)&&(e=[!1,e]);C(e.slice(0,c.length+1),function(a,e){if(!1!==a){e=c[e-1]||
{isHeader:!0,plotX:c[0].plotX};var q=e.series||k,A=q.tt,I=e.series||{},J="highcharts-color-"+w(e.colorIndex,I.colorIndex,"none");A||(q.tt=A=n.label(null,null,null,"callout",null,null,f.useHTML).addClass("highcharts-tooltip-box "+J).attr({padding:f.padding,r:f.borderRadius,fill:f.backgroundColor,stroke:f.borderColor||e.color||I.color||"#333333","stroke-width":f.borderWidth}).add(D));A.isActive=!0;A.attr({text:a});A.css(f.style).shadow(f.shadow);a=A.getBBox();I=a.width+A.strokeWidth();e.isHeader?(u=
a.height,I=Math.max(0,Math.min(e.plotX+g.plotLeft-I/2,g.chartWidth-I))):I=e.plotX+g.plotLeft-w(f.distance,16)-I;0>I&&(t=!1);a=(e.series&&e.series.yAxis&&e.series.yAxis.pos)+(e.plotY||0);a-=g.plotTop;b.push({target:e.isHeader?g.plotHeight+u:a,rank:e.isHeader?1:0,size:q.tt.getBBox().height+1,point:e,x:I,tt:A})}});this.cleanSplit();a.distribute(b,g.plotHeight+u);C(b,function(a){var b=a.point,c=b.series;a.tt.attr({visibility:void 0===a.pos?"hidden":"inherit",x:t||b.isHeader?a.x:b.plotX+g.plotLeft+w(f.distance,
16),y:a.pos+g.plotTop,anchorX:b.isHeader?b.plotX+g.plotLeft:b.plotX+c.xAxis.pos,anchorY:b.isHeader?a.pos+g.plotTop-15:b.plotY+c.yAxis.pos})})},updatePosition:function(a){var c=this.chart,e=this.getLabel(),e=(this.options.positioner||this.getPosition).call(this,e.width,e.height,a);this.move(Math.round(e.x),Math.round(e.y||0),a.plotX+c.plotLeft,a.plotY+c.plotTop)},getDateFormat:function(a,c,k,b){var g=this.chart.time,e=g.dateFormat("%m-%d %H:%M:%S.%L",c),t,f,u={millisecond:15,second:12,minute:9,hour:6,
day:3},D="millisecond";for(f in B){if(a===B.week&&+g.dateFormat("%w",c)===k&&"00:00:00.000"===e.substr(6)){f="week";break}if(B[f]>a){f=D;break}if(u[f]&&e.substr(u[f])!=="01-01 00:00:00.000".substr(u[f]))break;"week"!==f&&(D=f)}f&&(t=b[f]);return t},getXDateFormat:function(a,c,k){c=c.dateTimeLabelFormats;var b=k&&k.closestPointRange;return(b?this.getDateFormat(b,a.x,k.options.startOfWeek,c):c.day)||c.year},tooltipFooterHeaderFormatter:function(a,c){c=c?"footer":"header";var e=a.series,b=e.tooltipOptions,
g=b.xDateFormat,n=e.xAxis,t=n&&"datetime"===n.options.type&&r(a.key),f=b[c+"Format"];t&&!g&&(g=this.getXDateFormat(a,b,n));t&&g&&C(a.point&&a.point.tooltipDateKeys||["key"],function(a){f=f.replace("{point."+a+"}","{point."+a+":"+g+"}")});return F(f,{point:a,series:e},this.chart.time)},bodyFormatter:function(a){return m(a,function(a){var c=a.series.tooltipOptions;return(c[(a.point.formatPrefix||"point")+"Formatter"]||a.point.tooltipFormatter).call(a.point,c[(a.point.formatPrefix||"point")+"Format"])})}}})(L);
(function(a){var C=a.addEvent,E=a.attr,F=a.charts,r=a.color,m=a.css,l=a.defined,w=a.each,p=a.extend,v=a.find,B=a.fireEvent,e=a.isNumber,c=a.isObject,k=a.offset,b=a.pick,g=a.splat,n=a.Tooltip;a.Pointer=function(a,b){this.init(a,b)};a.Pointer.prototype={init:function(a,f){this.options=f;this.chart=a;this.runChartClick=f.chart.events&&!!f.chart.events.click;this.pinchDown=[];this.lastValidTouch={};n&&(a.tooltip=new n(a,f.tooltip),this.followTouchMove=b(f.tooltip.followTouchMove,!0));this.setDOMEvents()},
zoomOption:function(a){var f=this.chart,c=f.options.chart,g=c.zoomType||"",f=f.inverted;/touch/.test(a.type)&&(g=b(c.pinchType,g));this.zoomX=a=/x/.test(g);this.zoomY=g=/y/.test(g);this.zoomHor=a&&!f||g&&f;this.zoomVert=g&&!f||a&&f;this.hasZoom=a||g},normalize:function(a,b){var f;f=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;b||(this.chartPosition=b=k(this.chart.container));return p(a,{chartX:Math.round(f.pageX-b.left),chartY:Math.round(f.pageY-b.top)})},getCoordinates:function(a){var b=
{xAxis:[],yAxis:[]};w(this.chart.axes,function(f){b[f.isXAxis?"xAxis":"yAxis"].push({axis:f,value:f.toValue(a[f.horiz?"chartX":"chartY"])})});return b},findNearestKDPoint:function(a,b,g){var f;w(a,function(a){var e=!(a.noSharedTooltip&&b)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(g,e);if((e=c(a,!0))&&!(e=!c(f,!0)))var e=f.distX-a.distX,q=f.dist-a.dist,n=(a.series.group&&a.series.group.zIndex)-(f.series.group&&f.series.group.zIndex),e=0<(0!==e&&b?e:0!==q?q:0!==n?n:f.series.index>
a.series.index?-1:1);e&&(f=a)});return f},getPointFromEvent:function(a){a=a.target;for(var b;a&&!b;)b=a.point,a=a.parentNode;return b},getChartCoordinatesFromPoint:function(a,f){var c=a.series,g=c.xAxis,c=c.yAxis,e=b(a.clientX,a.plotX);if(g&&c)return f?{chartX:g.len+g.pos-e,chartY:c.len+c.pos-a.plotY}:{chartX:e+g.pos,chartY:a.plotY+c.pos}},getHoverData:function(g,f,e,n,q,A,y){var t,u=[],k=y&&y.isBoosting;n=!(!n||!g);y=f&&!f.stickyTracking?[f]:a.grep(e,function(a){return a.visible&&!(!q&&a.directTouch)&&
b(a.options.enableMouseTracking,!0)&&a.stickyTracking});f=(t=n?g:this.findNearestKDPoint(y,q,A))&&t.series;t&&(q&&!f.noSharedTooltip?(y=a.grep(e,function(a){return a.visible&&!(!q&&a.directTouch)&&b(a.options.enableMouseTracking,!0)&&!a.noSharedTooltip}),w(y,function(a){var d=v(a.points,function(a){return a.x===t.x&&!a.isNull});c(d)&&(k&&(d=a.getPoint(d)),u.push(d))})):u.push(t));return{hoverPoint:t,hoverSeries:f,hoverPoints:u}},runPointActions:function(c,f){var g=this.chart,e=g.tooltip&&g.tooltip.options.enabled?
g.tooltip:void 0,n=e?e.shared:!1,t=f||g.hoverPoint,y=t&&t.series||g.hoverSeries,y=this.getHoverData(t,y,g.series,!!f||y&&y.directTouch&&this.isDirectTouch,n,c,{isBoosting:g.isBoosting}),k,t=y.hoverPoint;k=y.hoverPoints;f=(y=y.hoverSeries)&&y.tooltipOptions.followPointer;n=n&&y&&!y.noSharedTooltip;if(t&&(t!==g.hoverPoint||e&&e.isHidden)){w(g.hoverPoints||[],function(b){-1===a.inArray(b,k)&&b.setState()});w(k||[],function(a){a.setState("hover")});if(g.hoverSeries!==y)y.onMouseOver();g.hoverPoint&&g.hoverPoint.firePointEvent("mouseOut");
if(!t.series)return;t.firePointEvent("mouseOver");g.hoverPoints=k;g.hoverPoint=t;e&&e.refresh(n?k:t,c)}else f&&e&&!e.isHidden&&(t=e.getAnchor([{}],c),e.updatePosition({plotX:t[0],plotY:t[1]}));this.unDocMouseMove||(this.unDocMouseMove=C(g.container.ownerDocument,"mousemove",function(b){var f=F[a.hoverChartIndex];if(f)f.pointer.onDocumentMouseMove(b)}));w(g.axes,function(f){var g=b(f.crosshair.snap,!0),d=g?a.find(k,function(a){return a.series[f.coll]===f}):void 0;d||!g?f.drawCrosshair(c,d):f.hideCrosshair()})},
reset:function(a,b){var f=this.chart,c=f.hoverSeries,e=f.hoverPoint,n=f.hoverPoints,t=f.tooltip,k=t&&t.shared?n:e;a&&k&&w(g(k),function(b){b.series.isCartesian&&void 0===b.plotX&&(a=!1)});if(a)t&&k&&(t.refresh(k),e&&(e.setState(e.state,!0),w(f.axes,function(a){a.crosshair&&a.drawCrosshair(null,e)})));else{if(e)e.onMouseOut();n&&w(n,function(a){a.setState()});if(c)c.onMouseOut();t&&t.hide(b);this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());w(f.axes,function(a){a.hideCrosshair()});this.hoverX=
f.hoverPoints=f.hoverPoint=null}},scaleGroups:function(a,b){var f=this.chart,c;w(f.series,function(g){c=a||g.getPlotBox();g.xAxis&&g.xAxis.zoomEnabled&&g.group&&(g.group.attr(c),g.markerGroup&&(g.markerGroup.attr(c),g.markerGroup.clip(b?f.clipRect:null)),g.dataLabelsGroup&&g.dataLabelsGroup.attr(c))});f.clipRect.attr(b||f.clipBox)},dragStart:function(a){var b=this.chart;b.mouseIsDown=a.type;b.cancelClick=!1;b.mouseDownX=this.mouseDownX=a.chartX;b.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var b=
this.chart,c=b.options.chart,g=a.chartX,e=a.chartY,n=this.zoomHor,t=this.zoomVert,k=b.plotLeft,I=b.plotTop,J=b.plotWidth,d=b.plotHeight,z,G=this.selectionMarker,h=this.mouseDownX,x=this.mouseDownY,l=c.panKey&&a[c.panKey+"Key"];G&&G.touch||(g<k?g=k:g>k+J&&(g=k+J),e<I?e=I:e>I+d&&(e=I+d),this.hasDragged=Math.sqrt(Math.pow(h-g,2)+Math.pow(x-e,2)),10<this.hasDragged&&(z=b.isInsidePlot(h-k,x-I),b.hasCartesianSeries&&(this.zoomX||this.zoomY)&&z&&!l&&!G&&(this.selectionMarker=G=b.renderer.rect(k,I,n?1:J,
t?1:d,0).attr({fill:c.selectionMarkerFill||r("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",zIndex:7}).add()),G&&n&&(g-=h,G.attr({width:Math.abs(g),x:(0<g?0:g)+h})),G&&t&&(g=e-x,G.attr({height:Math.abs(g),y:(0<g?0:g)+x})),z&&!G&&c.panning&&b.pan(a,c.panning)))},drop:function(a){var b=this,c=this.chart,g=this.hasPinched;if(this.selectionMarker){var n={originalEvent:a,xAxis:[],yAxis:[]},t=this.selectionMarker,y=t.attr?t.attr("x"):t.x,k=t.attr?t.attr("y"):t.y,I=t.attr?t.attr("width"):
t.width,J=t.attr?t.attr("height"):t.height,d;if(this.hasDragged||g)w(c.axes,function(f){if(f.zoomEnabled&&l(f.min)&&(g||b[{xAxis:"zoomX",yAxis:"zoomY"}[f.coll]])){var c=f.horiz,h="touchend"===a.type?f.minPixelPadding:0,e=f.toValue((c?y:k)+h),c=f.toValue((c?y+I:k+J)-h);n[f.coll].push({axis:f,min:Math.min(e,c),max:Math.max(e,c)});d=!0}}),d&&B(c,"selection",n,function(a){c.zoom(p(a,g?{animation:!1}:null))});e(c.index)&&(this.selectionMarker=this.selectionMarker.destroy());g&&this.scaleGroups()}c&&e(c.index)&&
(m(c.container,{cursor:c._cursor}),c.cancelClick=10<this.hasDragged,c.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=[])},onContainerMouseDown:function(a){a=this.normalize(a);2!==a.button&&(this.zoomOption(a),a.preventDefault&&a.preventDefault(),this.dragStart(a))},onDocumentMouseUp:function(b){F[a.hoverChartIndex]&&F[a.hoverChartIndex].pointer.drop(b)},onDocumentMouseMove:function(a){var b=this.chart,c=this.chartPosition;a=this.normalize(a,c);!c||this.inClass(a.target,"highcharts-tracker")||
b.isInsidePlot(a.chartX-b.plotLeft,a.chartY-b.plotTop)||this.reset()},onContainerMouseLeave:function(b){var f=F[a.hoverChartIndex];f&&(b.relatedTarget||b.toElement)&&(f.pointer.reset(),f.pointer.chartPosition=null)},onContainerMouseMove:function(b){var f=this.chart;l(a.hoverChartIndex)&&F[a.hoverChartIndex]&&F[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=f.index);b=this.normalize(b);b.returnValue=!1;"mousedown"===f.mouseIsDown&&this.drag(b);!this.inClass(b.target,"highcharts-tracker")&&!f.isInsidePlot(b.chartX-
f.plotLeft,b.chartY-f.plotTop)||f.openMenu||this.runPointActions(b)},inClass:function(a,b){for(var f;a;){if(f=E(a,"class")){if(-1!==f.indexOf(b))return!0;if(-1!==f.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var b=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;this.isDirectTouch=!1;if(!(!b||!a||b.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+b.index)&&this.inClass(a,"highcharts-tracker")))b.onMouseOut()},
onContainerClick:function(a){var b=this.chart,c=b.hoverPoint,g=b.plotLeft,e=b.plotTop;a=this.normalize(a);b.cancelClick||(c&&this.inClass(a.target,"highcharts-tracker")?(B(c.series,"click",p(a,{point:c})),b.hoverPoint&&c.firePointEvent("click",a)):(p(a,this.getCoordinates(a)),b.isInsidePlot(a.chartX-g,a.chartY-e)&&B(b,"click",a)))},setDOMEvents:function(){var b=this,f=b.chart.container,c=f.ownerDocument;f.onmousedown=function(a){b.onContainerMouseDown(a)};f.onmousemove=function(a){b.onContainerMouseMove(a)};
f.onclick=function(a){b.onContainerClick(a)};this.unbindContainerMouseLeave=C(f,"mouseleave",b.onContainerMouseLeave);a.unbindDocumentMouseUp||(a.unbindDocumentMouseUp=C(c,"mouseup",b.onDocumentMouseUp));a.hasTouch&&(f.ontouchstart=function(a){b.onContainerTouchStart(a)},f.ontouchmove=function(a){b.onContainerTouchMove(a)},a.unbindDocumentTouchEnd||(a.unbindDocumentTouchEnd=C(c,"touchend",b.onDocumentTouchEnd)))},destroy:function(){var b=this;b.unDocMouseMove&&b.unDocMouseMove();this.unbindContainerMouseLeave();
a.chartCount||(a.unbindDocumentMouseUp&&(a.unbindDocumentMouseUp=a.unbindDocumentMouseUp()),a.unbindDocumentTouchEnd&&(a.unbindDocumentTouchEnd=a.unbindDocumentTouchEnd()));clearInterval(b.tooltipTimeout);a.objectEach(b,function(a,c){b[c]=null})}}})(L);(function(a){var C=a.charts,E=a.each,F=a.extend,r=a.map,m=a.noop,l=a.pick;F(a.Pointer.prototype,{pinchTranslate:function(a,l,m,r,e,c){this.zoomHor&&this.pinchTranslateDirection(!0,a,l,m,r,e,c);this.zoomVert&&this.pinchTranslateDirection(!1,a,l,m,r,
e,c)},pinchTranslateDirection:function(a,l,m,r,e,c,k,b){var g=this.chart,n=a?"x":"y",t=a?"X":"Y",f="chart"+t,u=a?"width":"height",D=g["plot"+(a?"Left":"Top")],q,A,y=b||1,H=g.inverted,I=g.bounds[a?"h":"v"],J=1===l.length,d=l[0][f],z=m[0][f],G=!J&&l[1][f],h=!J&&m[1][f],x;m=function(){!J&&20<Math.abs(d-G)&&(y=b||Math.abs(z-h)/Math.abs(d-G));A=(D-z)/y+d;q=g["plot"+(a?"Width":"Height")]/y};m();l=A;l<I.min?(l=I.min,x=!0):l+q>I.max&&(l=I.max-q,x=!0);x?(z-=.8*(z-k[n][0]),J||(h-=.8*(h-k[n][1])),m()):k[n]=
[z,h];H||(c[n]=A-D,c[u]=q);c=H?1/y:y;e[u]=q;e[n]=l;r[H?a?"scaleY":"scaleX":"scale"+t]=y;r["translate"+t]=c*D+(z-c*d)},pinch:function(a){var p=this,v=p.chart,w=p.pinchDown,e=a.touches,c=e.length,k=p.lastValidTouch,b=p.hasZoom,g=p.selectionMarker,n={},t=1===c&&(p.inClass(a.target,"highcharts-tracker")&&v.runTrackerClick||p.runChartClick),f={};1<c&&(p.initiated=!0);b&&p.initiated&&!t&&a.preventDefault();r(e,function(a){return p.normalize(a)});"touchstart"===a.type?(E(e,function(a,b){w[b]={chartX:a.chartX,
chartY:a.chartY}}),k.x=[w[0].chartX,w[1]&&w[1].chartX],k.y=[w[0].chartY,w[1]&&w[1].chartY],E(v.axes,function(a){if(a.zoomEnabled){var b=v.bounds[a.horiz?"h":"v"],f=a.minPixelPadding,c=a.toPixels(l(a.options.min,a.dataMin)),g=a.toPixels(l(a.options.max,a.dataMax)),e=Math.max(c,g);b.min=Math.min(a.pos,Math.min(c,g)-f);b.max=Math.max(a.pos+a.len,e+f)}}),p.res=!0):p.followTouchMove&&1===c?this.runPointActions(p.normalize(a)):w.length&&(g||(p.selectionMarker=g=F({destroy:m,touch:!0},v.plotBox)),p.pinchTranslate(w,
e,n,g,f,k),p.hasPinched=b,p.scaleGroups(n,f),p.res&&(p.res=!1,this.reset(!1,0)))},touch:function(m,p){var v=this.chart,r,e;if(v.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});a.hoverChartIndex=v.index;1===m.touches.length?(m=this.normalize(m),(e=v.isInsidePlot(m.chartX-v.plotLeft,m.chartY-v.plotTop))&&!v.openMenu?(p&&this.runPointActions(m),"touchmove"===m.type&&(p=this.pinchDown,r=p[0]?4<=Math.sqrt(Math.pow(p[0].chartX-m.chartX,2)+Math.pow(p[0].chartY-m.chartY,2)):!1),l(r,
!0)&&this.pinch(m)):p&&this.reset()):2===m.touches.length&&this.pinch(m)},onContainerTouchStart:function(a){this.zoomOption(a);this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(l){C[a.hoverChartIndex]&&C[a.hoverChartIndex].pointer.drop(l)}})})(L);(function(a){var C=a.addEvent,E=a.charts,F=a.css,r=a.doc,m=a.extend,l=a.noop,w=a.Pointer,p=a.removeEvent,v=a.win,B=a.wrap;if(!a.hasTouch&&(v.PointerEvent||v.MSPointerEvent)){var e={},c=!!v.PointerEvent,k=function(){var b=
[];b.item=function(a){return this[a]};a.objectEach(e,function(a){b.push({pageX:a.pageX,pageY:a.pageY,target:a.target})});return b},b=function(b,c,e,f){"touch"!==b.pointerType&&b.pointerType!==b.MSPOINTER_TYPE_TOUCH||!E[a.hoverChartIndex]||(f(b),f=E[a.hoverChartIndex].pointer,f[c]({type:e,target:b.currentTarget,preventDefault:l,touches:k()}))};m(w.prototype,{onContainerPointerDown:function(a){b(a,"onContainerTouchStart","touchstart",function(a){e[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},
onContainerPointerMove:function(a){b(a,"onContainerTouchMove","touchmove",function(a){e[a.pointerId]={pageX:a.pageX,pageY:a.pageY};e[a.pointerId].target||(e[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){b(a,"onDocumentTouchEnd","touchend",function(a){delete e[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,c?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,c?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(r,c?
"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});B(w.prototype,"init",function(a,b,c){a.call(this,b,c);this.hasZoom&&F(b.container,{"-ms-touch-action":"none","touch-action":"none"})});B(w.prototype,"setDOMEvents",function(a){a.apply(this);(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(C)});B(w.prototype,"destroy",function(a){this.batchMSEvents(p);a.call(this)})}})(L);(function(a){var C=a.addEvent,E=a.css,F=a.discardElement,r=a.defined,m=a.each,l=a.isFirefox,w=a.marginNames,p=a.merge,
v=a.pick,B=a.setAnimation,e=a.stableSort,c=a.win,k=a.wrap;a.Legend=function(a,c){this.init(a,c)};a.Legend.prototype={init:function(a,c){this.chart=a;this.setOptions(c);c.enabled&&(this.render(),C(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},setOptions:function(a){var b=v(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=p(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.padding=b;this.initialItemY=b-5;this.itemHeight=
this.maxItemWidth=0;this.symbolWidth=v(a.symbolWidth,16);this.pages=[]},update:function(a,c){var b=this.chart;this.setOptions(p(!0,this.options,a));this.destroy();b.isDirtyLegend=b.isDirtyBox=!0;v(c,!0)&&b.redraw()},colorizeItem:function(a,c){a.legendGroup[c?"removeClass":"addClass"]("highcharts-legend-item-hidden");var b=this.options,g=a.legendItem,f=a.legendLine,e=a.legendSymbol,k=this.itemHiddenStyle.color,b=c?b.itemStyle.color:k,q=c?a.color||k:k,A=a.options&&a.options.marker,y={fill:q};g&&g.css({fill:b,
color:b});f&&f.attr({stroke:q});e&&(A&&e.isMarker&&(y=a.pointAttribs(),c||(y.stroke=y.fill=k)),e.attr(y))},positionItem:function(a){var b=this.options,c=b.symbolPadding,b=!b.rtl,e=a._legendItemPos,f=e[0],e=e[1],k=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(b?f:this.legendWidth-f-2*c-4,e);k&&(k.x=f,k.y=e)},destroyItem:function(a){var b=a.checkbox;m(["legendItem","legendLine","legendSymbol","legendGroup"],function(b){a[b]&&(a[b]=a[b].destroy())});b&&F(a.checkbox)},destroy:function(){function a(a){this[a]&&
(this[a]=this[a].destroy())}m(this.getAllItems(),function(b){m(["legendItem","legendGroup"],a,b)});m("clipRect up down pager nav box title group".split(" "),a,this);this.display=null},positionCheckboxes:function(){var a=this.group&&this.group.alignAttr,c,e=this.clipHeight||this.legendHeight,k=this.titleHeight;a&&(c=a.translateY,m(this.allItems,function(b){var f=b.checkbox,g;f&&(g=c+k+f.y+(this.scrollOffset||0)+3,E(f,{left:a.translateX+b.checkboxOffset+f.x-20+"px",top:g+"px",display:g>c-6&&g<c+e-6?
"":"none"}))},this))},renderTitle:function(){var a=this.options,c=this.padding,e=a.title,k=0;e.text&&(this.title||(this.title=this.chart.renderer.label(e.text,c-3,c-4,null,null,null,a.useHTML,null,"legend-title").attr({zIndex:1}).css(e.style).add(this.group)),a=this.title.getBBox(),k=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:k}));this.titleHeight=k},setText:function(b){var c=this.options;b.legendItem.attr({text:c.labelFormat?a.format(c.labelFormat,b,this.chart.time):c.labelFormatter.call(b)})},
renderItem:function(a){var b=this.chart,c=b.renderer,e=this.options,f="horizontal"===e.layout,k=this.symbolWidth,l=e.symbolPadding,q=this.itemStyle,A=this.itemHiddenStyle,y=this.padding,m=f?v(e.itemDistance,20):0,I=!e.rtl,J=e.width,d=e.itemMarginBottom||0,z=this.itemMarginTop,G=a.legendItem,h=!a.series,x=!h&&a.series.drawLegendSymbol?a.series:a,r=x.options,O=this.createCheckboxForItem&&r&&r.showCheckbox,r=k+l+m+(O?20:0),P=e.useHTML,M=a.options.className;G||(a.legendGroup=c.g("legend-item").addClass("highcharts-"+
x.type+"-series highcharts-color-"+a.colorIndex+(M?" "+M:"")+(h?" highcharts-series-"+a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=G=c.text("",I?k+l:-l,this.baseline||0,P).css(p(a.visible?q:A)).attr({align:I?"left":"right",zIndex:2}).add(a.legendGroup),this.baseline||(k=q.fontSize,this.fontMetrics=c.fontMetrics(k,G),this.baseline=this.fontMetrics.f+3+z,G.attr("y",this.baseline)),this.symbolHeight=e.symbolHeight||this.fontMetrics.f,x.drawLegendSymbol(this,a),this.setItemEvents&&
this.setItemEvents(a,G,P),O&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);q.width||G.css({width:(e.itemWidth||e.width||b.spacingBox.width)-r});this.setText(a);c=G.getBBox();q=a.checkboxOffset=e.itemWidth||a.legendItemWidth||c.width+r;this.itemHeight=c=Math.round(a.legendItemHeight||c.height||this.symbolHeight);f&&this.itemX-y+q>(J||b.spacingBox.width-2*y-e.x)&&(this.itemX=y,this.itemY+=z+this.lastLineHeight+d,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,q);
this.lastItemY=z+this.itemY+d;this.lastLineHeight=Math.max(c,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];f?this.itemX+=q:(this.itemY+=z+c+d,this.lastLineHeight=c);this.offsetWidth=J||Math.max((f?this.itemX-y-(a.checkbox?0:m):q)+y,this.offsetWidth)},getAllItems:function(){var a=[];m(this.chart.series,function(b){var c=b&&b.options;b&&v(c.showInLegend,r(c.linkedTo)?!1:void 0,!0)&&(a=a.concat(b.legendItems||("point"===c.legendType?b.data:b)))});return a},getAlignment:function(){var a=
this.options;return a.floating?"":a.align.charAt(0)+a.verticalAlign.charAt(0)+a.layout.charAt(0)},adjustMargins:function(a,c){var b=this.chart,g=this.options,f=this.getAlignment();f&&m([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(e,n){e.test(f)&&!r(a[n])&&(b[w[n]]=Math.max(b[w[n]],b.legend[(n+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][n]*g[n%2?"x":"y"]+v(g.margin,12)+c[n]+(0===n?b.titleOffset+b.options.title.margin:0)))})},render:function(){var a=this,c=a.chart,n=c.renderer,
k=a.group,f,u,l,q,A=a.box,y=a.options,H=a.padding;a.itemX=H;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;k||(a.group=k=n.g("legend").attr({zIndex:7}).add(),a.contentGroup=n.g().attr({zIndex:1}).add(k),a.scrollGroup=n.g().add(a.contentGroup));a.renderTitle();f=a.getAllItems();e(f,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});y.reversed&&f.reverse();a.allItems=f;a.display=u=!!f.length;a.lastLineHeight=0;m(f,function(b){a.renderItem(b)});l=
(y.width||a.offsetWidth)+H;q=a.lastItemY+a.lastLineHeight+a.titleHeight;q=a.handleOverflow(q);q+=H;A||(a.box=A=n.rect().addClass("highcharts-legend-box").attr({r:y.borderRadius}).add(k),A.isNew=!0);A.attr({stroke:y.borderColor,"stroke-width":y.borderWidth||0,fill:y.backgroundColor||"none"}).shadow(y.shadow);0<l&&0<q&&(A[A.isNew?"attr":"animate"](A.crisp.call({},{x:0,y:0,width:l,height:q},A.strokeWidth())),A.isNew=!1);A[u?"show":"hide"]();a.legendWidth=l;a.legendHeight=q;m(f,function(b){a.positionItem(b)});
u&&(n=c.spacingBox,/(lth|ct|rth)/.test(a.getAlignment())&&(n=p(n,{y:n.y+c.titleOffset+c.options.title.margin})),k.align(p(y,{width:l,height:q}),!0,n));c.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var b=this,c=this.chart,e=c.renderer,f=this.options,k=f.y,l=this.padding,c=c.spacingBox.height+("top"===f.verticalAlign?-k:k)-l,k=f.maxHeight,q,A=this.clipRect,y=f.navigation,H=v(y.animation,!0),I=y.arrowSize||12,J=this.nav,d=this.pages,z,G=this.allItems,h=function(a){"number"===typeof a?
A.attr({height:a}):A&&(b.clipRect=A.destroy(),b.contentGroup.clip());b.contentGroup.div&&(b.contentGroup.div.style.clip=a?"rect("+l+"px,9999px,"+(l+a)+"px,0)":"auto")};"horizontal"!==f.layout||"middle"===f.verticalAlign||f.floating||(c/=2);k&&(c=Math.min(c,k));d.length=0;a>c&&!1!==y.enabled?(this.clipHeight=q=Math.max(c-20-this.titleHeight-l,0),this.currentPage=v(this.currentPage,1),this.fullHeight=a,m(G,function(a,b){var c=a._legendItemPos[1],f=Math.round(a.legendItem.getBBox().height),h=d.length;
if(!h||c-d[h-1]>q&&(z||c)!==d[h-1])d.push(z||c),h++;a.pageIx=h-1;z&&(G[b-1].pageIx=h-1);b===G.length-1&&c+f-d[h-1]>q&&(d.push(c),a.pageIx=h);c!==z&&(z=c)}),A||(A=b.clipRect=e.clipRect(0,l,9999,0),b.contentGroup.clip(A)),h(q),J||(this.nav=J=e.g().attr({zIndex:1}).add(this.group),this.up=e.symbol("triangle",0,0,I,I).on("click",function(){b.scroll(-1,H)}).add(J),this.pager=e.text("",15,10).addClass("highcharts-legend-navigation").css(y.style).add(J),this.down=e.symbol("triangle-down",0,0,I,I).on("click",
function(){b.scroll(1,H)}).add(J)),b.scroll(0),a=c):J&&(h(),this.nav=J.destroy(),this.scrollGroup.attr({translateY:1}),this.clipHeight=0);return a},scroll:function(a,c){var b=this.pages,e=b.length;a=this.currentPage+a;var f=this.clipHeight,g=this.options.navigation,k=this.pager,q=this.padding;a>e&&(a=e);0<a&&(void 0!==c&&B(c,this.chart),this.nav.attr({translateX:q,translateY:f+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),
k.attr({text:a+"/"+e}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===e?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?g.inactiveColor:g.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===e?g.inactiveColor:g.activeColor}).css({cursor:a===e?"default":"pointer"}),this.scrollOffset=-b[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:this.scrollOffset}),this.currentPage=a,this.positionCheckboxes())}};a.LegendSymbolMixin=
{drawRectangle:function(a,c){var b=a.symbolHeight,e=a.options.squareSymbol;c.legendSymbol=this.chart.renderer.rect(e?(a.symbolWidth-b)/2:0,a.baseline-b+1,e?b:a.symbolWidth,b,v(a.options.symbolRadius,b/2)).addClass("highcharts-point").attr({zIndex:3}).add(c.legendGroup)},drawLineMarker:function(a){var b=this.options,c=b.marker,e=a.symbolWidth,f=a.symbolHeight,k=f/2,l=this.chart.renderer,q=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var A;A={"stroke-width":b.lineWidth||0};b.dashStyle&&
(A.dashstyle=b.dashStyle);this.legendLine=l.path(["M",0,a,"L",e,a]).addClass("highcharts-graph").attr(A).add(q);c&&!1!==c.enabled&&(b=Math.min(v(c.radius,k),k),0===this.symbol.indexOf("url")&&(c=p(c,{width:f,height:f}),b=0),this.legendSymbol=c=l.symbol(this.symbol,e/2-b,a-b,2*b,2*b,c).addClass("highcharts-point").add(q),c.isMarker=!0)}};(/Trident\/7\.0/.test(c.navigator.userAgent)||l)&&k(a.Legend.prototype,"positionItem",function(a,c){var b=this,e=function(){c._legendItemPos&&a.call(b,c)};e();setTimeout(e)})})(L);
(function(a){var C=a.addEvent,E=a.animate,F=a.animObject,r=a.attr,m=a.doc,l=a.Axis,w=a.createElement,p=a.defaultOptions,v=a.discardElement,B=a.charts,e=a.css,c=a.defined,k=a.each,b=a.extend,g=a.find,n=a.fireEvent,t=a.grep,f=a.isNumber,u=a.isObject,D=a.isString,q=a.Legend,A=a.marginNames,y=a.merge,H=a.objectEach,I=a.Pointer,J=a.pick,d=a.pInt,z=a.removeEvent,G=a.seriesTypes,h=a.splat,x=a.syncTimeout,N=a.win,O=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,d,b){return new O(a,
d,b)};b(O.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);if(D(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(d,b){var c,f,h=d.series,e=d.plotOptions||{};d.series=null;c=y(p,d);for(f in c.plotOptions)c.plotOptions[f].tooltip=e[f]&&y(e[f].tooltip)||void 0;c.tooltip.userOptions=d.chart&&d.chart.forExport&&d.tooltip.userOptions||d.tooltip;c.series=d.series=h;this.userOptions=d;f=c.chart;h=f.events;this.margin=[];this.spacing=[];this.bounds=
{h:{},v:{}};this.labelCollectors=[];this.callback=b;this.isResizing=0;this.options=c;this.axes=[];this.series=[];this.time=d.time&&a.keys(d.time).length?new a.Time(d.time):a.time;this.hasCartesianSeries=f.showAxes;var g=this;g.index=B.length;B.push(g);a.chartCount++;h&&H(h,function(a,d){C(g,d,a)});g.xAxis=[];g.yAxis=[];g.pointCount=g.colorCounter=g.symbolCounter=0;g.firstRender()},initSeries:function(d){var b=this.options.chart;(b=G[d.type||b.type||b.defaultSeriesType])||a.error(17,!0);b=new b;b.init(this,
d);return b},orderSeries:function(a){var d=this.series;for(a=a||0;a<d.length;a++)d[a]&&(d[a].index=a,d[a].name=d[a].getName())},isInsidePlot:function(a,d,b){var c=b?d:a;a=b?a:d;return 0<=c&&c<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(d){var c=this.axes,f=this.series,h=this.pointer,e=this.legend,g=this.isDirtyLegend,z,q,x=this.hasCartesianSeries,G=this.isDirtyBox,y,I=this.renderer,A=I.isHidden(),t=[];this.setResponsive&&this.setResponsive(!1);a.setAnimation(d,this);A&&this.temporaryDisplay();
this.layOutTitles();for(d=f.length;d--;)if(y=f[d],y.options.stacking&&(z=!0,y.isDirty)){q=!0;break}if(q)for(d=f.length;d--;)y=f[d],y.options.stacking&&(y.isDirty=!0);k(f,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),g=!0);a.isDirtyData&&n(a,"updatedData")});g&&e.options.enabled&&(e.render(),this.isDirtyLegend=!1);z&&this.getStacks();x&&k(c,function(a){a.updateNames();a.setScale()});this.getMargins();x&&(k(c,function(a){a.isDirty&&(G=!0)}),k(c,function(a){var d=
a.min+","+a.max;a.extKey!==d&&(a.extKey=d,t.push(function(){n(a,"afterSetExtremes",b(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(G||z)&&a.redraw()}));G&&this.drawChartBox();n(this,"predraw");k(f,function(a){(G||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});h&&h.reset(!0);I.draw();n(this,"redraw");n(this,"render");A&&this.temporaryDisplay(!0);k(t,function(a){a.call()})},get:function(a){function d(d){return d.id===a||d.options&&d.options.id===a}var b,c=this.series,f;b=g(this.axes,d)||
g(this.series,d);for(f=0;!b&&f<c.length;f++)b=g(c[f].points||[],d);return b},getAxes:function(){var a=this,d=this.options,b=d.xAxis=h(d.xAxis||{}),d=d.yAxis=h(d.yAxis||{});n(this,"beforeGetAxes");k(b,function(a,d){a.index=d;a.isX=!0});k(d,function(a,d){a.index=d});b=b.concat(d);k(b,function(d){new l(a,d)})},getSelectedPoints:function(){var a=[];k(this.series,function(d){a=a.concat(t(d.data||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return t(this.series,function(a){return a.selected})},
setTitle:function(a,d,b){var c=this,f=c.options,h;h=f.title=y({style:{color:"#333333",fontSize:f.isStock?"16px":"18px"}},f.title,a);f=f.subtitle=y({style:{color:"#666666"}},f.subtitle,d);k([["title",a,h],["subtitle",d,f]],function(a,d){var b=a[0],f=c[b],h=a[1];a=a[2];f&&h&&(c[b]=f=f.destroy());a&&!f&&(c[b]=c.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+b,zIndex:a.zIndex||4}).add(),c[b].update=function(a){c.setTitle(!d&&a,d&&a)},c[b].css(a.style))});c.layOutTitles(b)},
layOutTitles:function(a){var d=0,c,f=this.renderer,h=this.spacingBox;k(["title","subtitle"],function(a){var c=this[a],e=this.options[a];a="title"===a?-3:e.verticalAlign?0:d+2;var g;c&&(g=e.style.fontSize,g=f.fontMetrics(g,c).b,c.css({width:(e.width||h.width+e.widthAdjust)+"px"}).align(b({y:a+g},e),!1,"spacingBox"),e.floating||e.verticalAlign||(d=Math.ceil(d+c.getBBox(e.useHTML).height)))},this);c=this.titleOffset!==d;this.titleOffset=d;!this.isDirtyBox&&c&&(this.isDirtyBox=c,this.hasRendered&&J(a,
!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var d=this.options.chart,b=d.width,d=d.height,f=this.renderTo;c(b)||(this.containerWidth=a.getStyle(f,"width"));c(d)||(this.containerHeight=a.getStyle(f,"height"));this.chartWidth=Math.max(0,b||this.containerWidth||600);this.chartHeight=Math.max(0,a.relativeLength(d,this.chartWidth)||(1<this.containerHeight?this.containerHeight:400))},temporaryDisplay:function(d){var b=this.renderTo;if(d)for(;b&&b.style;)b.hcOrigStyle&&(a.css(b,b.hcOrigStyle),
delete b.hcOrigStyle),b.hcOrigDetached&&(m.body.removeChild(b),b.hcOrigDetached=!1),b=b.parentNode;else for(;b&&b.style;){m.body.contains(b)||b.parentNode||(b.hcOrigDetached=!0,m.body.appendChild(b));if("none"===a.getStyle(b,"display",!1)||b.hcOricDetached)b.hcOrigStyle={display:b.style.display,height:b.style.height,overflow:b.style.overflow},d={display:"block",overflow:"hidden"},b!==this.renderTo&&(d.height=0),a.css(b,d),b.offsetWidth||b.style.setProperty("display","block","important");b=b.parentNode;
if(b===m.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var c,h=this.options,e=h.chart,g,z;c=this.renderTo;var q=a.uniqueKey(),k;c||(this.renderTo=c=e.renderTo);D(c)&&(this.renderTo=c=m.getElementById(c));c||a.error(13,!0);g=d(r(c,"data-highcharts-chart"));f(g)&&B[g]&&B[g].hasRendered&&B[g].destroy();r(c,"data-highcharts-chart",this.index);c.innerHTML="";e.skipClone||c.offsetWidth||this.temporaryDisplay();this.getChartSize();
g=this.chartWidth;z=this.chartHeight;k=b({position:"relative",overflow:"hidden",width:g+"px",height:z+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},e.style);this.container=c=w("div",{id:q},k,c);this._cursor=c.style.cursor;this.renderer=new (a[e.renderer]||a.Renderer)(c,g,z,null,e.forExport,h.exporting&&h.exporting.allowHTML);this.setClassName(e.className);this.renderer.setStyle(e.style);this.renderer.chartIndex=this.index},getMargins:function(a){var d=
this.spacing,b=this.margin,f=this.titleOffset;this.resetMargins();f&&!c(b[0])&&(this.plotTop=Math.max(this.plotTop,f+this.options.title.margin+d[0]));this.legend&&this.legend.display&&this.legend.adjustMargins(b,d);this.extraMargin&&(this[this.extraMargin.type]=(this[this.extraMargin.type]||0)+this.extraMargin.value);this.adjustPlotArea&&this.adjustPlotArea();a||this.getAxisMargins()},getAxisMargins:function(){var a=this,d=a.axisOffset=[0,0,0,0],b=a.margin;a.hasCartesianSeries&&k(a.axes,function(a){a.visible&&
a.getOffset()});k(A,function(f,h){c(b[h])||(a[f]+=d[h])});a.setChartSize()},reflow:function(d){var b=this,f=b.options.chart,h=b.renderTo,e=c(f.width)&&c(f.height),g=f.width||a.getStyle(h,"width"),f=f.height||a.getStyle(h,"height"),h=d?d.target:N;if(!e&&!b.isPrinting&&g&&f&&(h===N||h===m)){if(g!==b.containerWidth||f!==b.containerHeight)clearTimeout(b.reflowTimeout),b.reflowTimeout=x(function(){b.container&&b.setSize(void 0,void 0,!1)},d?100:0);b.containerWidth=g;b.containerHeight=f}},initReflow:function(){var a=
this,d;d=C(N,"resize",function(d){a.reflow(d)});C(a,"destroy",d)},setSize:function(d,b,c){var f=this,h=f.renderer;f.isResizing+=1;a.setAnimation(c,f);f.oldChartHeight=f.chartHeight;f.oldChartWidth=f.chartWidth;void 0!==d&&(f.options.chart.width=d);void 0!==b&&(f.options.chart.height=b);f.getChartSize();d=h.globalAnimation;(d?E:e)(f.container,{width:f.chartWidth+"px",height:f.chartHeight+"px"},d);f.setChartSize(!0);h.setSize(f.chartWidth,f.chartHeight,c);k(f.axes,function(a){a.isDirty=!0;a.setScale()});
f.isDirtyLegend=!0;f.isDirtyBox=!0;f.layOutTitles();f.getMargins();f.redraw(c);f.oldChartHeight=null;n(f,"resize");x(function(){f&&n(f,"endResize",null,function(){--f.isResizing})},F(d).duration)},setChartSize:function(a){var d=this.inverted,b=this.renderer,c=this.chartWidth,f=this.chartHeight,h=this.options.chart,e=this.spacing,g=this.clipOffset,z,q,x,n;this.plotLeft=z=Math.round(this.plotLeft);this.plotTop=q=Math.round(this.plotTop);this.plotWidth=x=Math.max(0,Math.round(c-z-this.marginRight));
this.plotHeight=n=Math.max(0,Math.round(f-q-this.marginBottom));this.plotSizeX=d?n:x;this.plotSizeY=d?x:n;this.plotBorderWidth=h.plotBorderWidth||0;this.spacingBox=b.spacingBox={x:e[3],y:e[0],width:c-e[3]-e[1],height:f-e[0]-e[2]};this.plotBox=b.plotBox={x:z,y:q,width:x,height:n};c=2*Math.floor(this.plotBorderWidth/2);d=Math.ceil(Math.max(c,g[3])/2);b=Math.ceil(Math.max(c,g[0])/2);this.clipBox={x:d,y:b,width:Math.floor(this.plotSizeX-Math.max(c,g[1])/2-d),height:Math.max(0,Math.floor(this.plotSizeY-
Math.max(c,g[2])/2-b))};a||k(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,d=a.options.chart;k(["margin","spacing"],function(b){var c=d[b],f=u(c)?c:[c,c,c,c];k(["Top","Right","Bottom","Left"],function(c,h){a[b][h]=J(d[b+c],f[h])})});k(A,function(d,b){a[d]=J(a.margin[b],a.spacing[b])});a.axisOffset=[0,0,0,0];a.clipOffset=[0,0,0,0]},drawChartBox:function(){var a=this.options.chart,d=this.renderer,b=this.chartWidth,c=this.chartHeight,f=this.chartBackground,
h=this.plotBackground,e=this.plotBorder,g,z=this.plotBGImage,q=a.backgroundColor,k=a.plotBackgroundColor,x=a.plotBackgroundImage,G,y=this.plotLeft,I=this.plotTop,A=this.plotWidth,t=this.plotHeight,u=this.plotBox,J=this.clipRect,l=this.clipBox,m="animate";f||(this.chartBackground=f=d.rect().addClass("highcharts-background").add(),m="attr");g=a.borderWidth||0;G=g+(a.shadow?8:0);q={fill:q||"none"};if(g||f["stroke-width"])q.stroke=a.borderColor,q["stroke-width"]=g;f.attr(q).shadow(a.shadow);f[m]({x:G/
2,y:G/2,width:b-G-g%2,height:c-G-g%2,r:a.borderRadius});m="animate";h||(m="attr",this.plotBackground=h=d.rect().addClass("highcharts-plot-background").add());h[m](u);h.attr({fill:k||"none"}).shadow(a.plotShadow);x&&(z?z.animate(u):this.plotBGImage=d.image(x,y,I,A,t).add());J?J.animate({width:l.width,height:l.height}):this.clipRect=d.clipRect(l);m="animate";e||(m="attr",this.plotBorder=e=d.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());e.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||
0,fill:"none"});e[m](e.crisp({x:y,y:I,width:A,height:t},-e.strokeWidth()));this.isDirtyBox=!1;n(this,"afterDrawChartBox")},propFromSeries:function(){var a=this,d=a.options.chart,b,c=a.options.series,f,h;k(["inverted","angular","polar"],function(e){b=G[d.type||d.defaultSeriesType];h=d[e]||b&&b.prototype[e];for(f=c&&c.length;!h&&f--;)(b=G[c[f].type])&&b.prototype[e]&&(h=!0);a[e]=h})},linkSeries:function(){var a=this,d=a.series;k(d,function(a){a.linkedSeries.length=0});k(d,function(d){var b=d.options.linkedTo;
D(b)&&(b=":previous"===b?a.series[d.index-1]:a.get(b))&&b.linkedParent!==d&&(b.linkedSeries.push(d),d.linkedParent=b,d.visible=J(d.options.visible,b.options.visible,d.visible))})},renderSeries:function(){k(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=this,c=a.options.labels;c.items&&k(c.items,function(f){var h=b(c.style,f.style),e=d(h.left)+a.plotLeft,g=d(h.top)+a.plotTop+12;delete h.left;delete h.top;a.renderer.text(f.html,e,g).attr({zIndex:2}).css(h).add()})},
render:function(){var a=this.axes,d=this.renderer,b=this.options,c,f,h;this.setTitle();this.legend=new q(this,b.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();b=this.plotWidth;c=this.plotHeight=Math.max(this.plotHeight-21,0);k(a,function(a){a.setScale()});this.getAxisMargins();f=1.1<b/this.plotWidth;h=1.05<c/this.plotHeight;if(f||h)k(a,function(a){(a.horiz&&f||!a.horiz&&h)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&k(a,
function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=d.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var d=this;a=y(!0,this.options.credits,a);a.enabled&&!this.credits&&(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(N.location.href=a.href)}).attr({align:a.position.align,
zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){d.credits=d.credits.destroy();d.addCredits(a)})},destroy:function(){var d=this,b=d.axes,c=d.series,f=d.container,h,e=f&&f.parentNode;n(d,"destroy");d.renderer.forExport?a.erase(B,d):B[d.index]=void 0;a.chartCount--;d.renderTo.removeAttribute("data-highcharts-chart");z(d);for(h=b.length;h--;)b[h]=b[h].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(h=c.length;h--;)c[h]=c[h].destroy();k("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),
function(a){var b=d[a];b&&b.destroy&&(d[a]=b.destroy())});f&&(f.innerHTML="",z(f),e&&v(f));H(d,function(a,b){delete d[b]})},firstRender:function(){var a=this,d=a.options;if(!a.isReadyToRender||a.isReadyToRender()){a.getContainer();n(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();k(d.series||[],function(d){a.initSeries(d)});a.linkSeries();n(a,"beforeRender");I&&(a.pointer=new I(a,d));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();a.temporaryDisplay(!0)}},onload:function(){k([this.callback].concat(this.callbacks),
function(a){a&&void 0!==this.index&&a.apply(this,[this])},this);n(this,"load");n(this,"render");c(this.index)&&!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}})})(L);(function(a){var C,E=a.each,F=a.extend,r=a.erase,m=a.fireEvent,l=a.format,w=a.isArray,p=a.isNumber,v=a.pick,B=a.removeEvent;a.Point=C=function(){};a.Point.prototype={init:function(a,c,k){this.series=a;this.color=a.color;this.applyOptions(c,k);a.options.colorByPoint?(c=a.options.colors||a.chart.options.colors,this.color=
this.color||c[a.colorCounter],c=c.length,k=a.colorCounter,a.colorCounter++,a.colorCounter===c&&(a.colorCounter=0)):k=a.colorIndex;this.colorIndex=v(this.colorIndex,k);a.chart.pointCount++;m(this,"afterInit");return this},applyOptions:function(a,c){var e=this.series,b=e.options.pointValKey||e.pointValKey;a=C.prototype.optionsToObject.call(this,a);F(this,a);this.options=this.options?F(this.options,a):a;a.group&&delete this.group;b&&(this.y=this[b]);this.isNull=v(this.isValid&&!this.isValid(),null===
this.x||!p(this.y,!0));this.selected&&(this.state="select");"name"in this&&void 0===c&&e.xAxis&&e.xAxis.hasNames&&(this.x=e.xAxis.nameToX(this));void 0===this.x&&e&&(this.x=void 0===c?e.autoIncrement(this):c);return this},optionsToObject:function(a){var c={},e=this.series,b=e.options.keys,g=b||e.pointArrayMap||["y"],n=g.length,t=0,f=0;if(p(a)||null===a)c[g[0]]=a;else if(w(a))for(!b&&a.length>n&&(e=typeof a[0],"string"===e?c.name=a[0]:"number"===e&&(c.x=a[0]),t++);f<n;)b&&void 0===a[t]||(c[g[f]]=a[t]),
t++,f++;else"object"===typeof a&&(c=a,a.dataLabels&&(e._hasPointLabels=!0),a.marker&&(e._hasPointMarkers=!0));return c},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?" "+this.zone.className.replace("highcharts-negative",
""):"")},getZone:function(){var a=this.series,c=a.zones,a=a.zoneAxis||"y",k=0,b;for(b=c[k];this[a]>=b.value;)b=c[++k];b&&b.color&&!this.options.color&&(this.color=b.color);return b},destroy:function(){var a=this.series.chart,c=a.hoverPoints,k;a.pointCount--;c&&(this.setState(),r(c,this),c.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)B(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);for(k in this)this[k]=null},destroyElements:function(){for(var a=
["graphic","dataLabel","dataLabelUpper","connector","shadowGroup"],c,k=6;k--;)c=a[k],this[c]&&(this[c]=this[c].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var c=this.series,e=c.tooltipOptions,b=v(e.valueDecimals,""),g=e.valuePrefix||"",n=e.valueSuffix||"";E(c.pointArrayMap||["y"],
function(c){c="{point."+c;if(g||n)a=a.replace(c+"}",g+c+"}"+n);a=a.replace(c+"}",c+":,."+b+"f}")});return l(a,{point:this,series:this.series},c.chart.time)},firePointEvent:function(a,c,k){var b=this,e=this.series.options;(e.point.events[a]||b.options&&b.options.events&&b.options.events[a])&&this.importEvents();"click"===a&&e.allowPointSelect&&(k=function(a){b.select&&b.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});m(this,a,c,k)},visible:!0}})(L);(function(a){var C=a.addEvent,E=a.animObject,F=a.arrayMax,
r=a.arrayMin,m=a.correctFloat,l=a.defaultOptions,w=a.defaultPlotOptions,p=a.defined,v=a.each,B=a.erase,e=a.extend,c=a.fireEvent,k=a.grep,b=a.isArray,g=a.isNumber,n=a.isString,t=a.merge,f=a.objectEach,u=a.pick,D=a.removeEvent,q=a.splat,A=a.SVGElement,y=a.syncTimeout,H=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",enabledThreshold:2,radius:4,states:{normal:{animation:!0},hover:{animation:{duration:50},
enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{normal:{animation:!0},hover:{animation:{duration:50},lineWidthPlus:1,marker:{},halo:{size:10,opacity:.25}},
select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var d=this,c,g=a.series,h;d.chart=a;d.options=b=d.setOptions(b);d.linkedSeries=[];d.bindAxes();e(d,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});c=b.events;f(c,function(a,b){C(d,b,a)});if(c&&c.click||b.point&&b.point.events&&
b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;d.getColor();d.getSymbol();v(d.parallelArrays,function(a){d[a+"Data"]=[]});d.setData(b.data,!1);d.isCartesian&&(a.hasCartesianSeries=!0);g.length&&(h=g[g.length-1]);d._i=u(h&&h._i,-1)+1;a.orderSeries(this.insert(g))},insert:function(a){var b=this.options.index,d;if(g(b)){for(d=a.length;d--;)if(b>=u(a[d].options.index,a[d]._i)){a.splice(d+1,0,this);break}-1===d&&a.unshift(this);d+=1}else a.push(this);return u(d,a.length-1)},bindAxes:function(){var b=
this,c=b.options,d=b.chart,f;v(b.axisTypes||[],function(e){v(d[e],function(a){f=a.options;if(c[e]===f.index||void 0!==c[e]&&c[e]===f.id||void 0===c[e]&&0===f.index)b.insert(a.series),b[e]=a,a.isDirty=!0});b[e]||b.optionalAxis===e||a.error(18,!0)})},updateParallelArrays:function(a,b){var d=a.series,c=arguments,f=g(b)?function(c){var f="y"===c&&d.toYData?d.toYData(a):a[c];d[c+"Data"][b]=f}:function(a){Array.prototype[b].apply(d[a+"Data"],Array.prototype.slice.call(c,2))};v(d.parallelArrays,f)},autoIncrement:function(){var a=
this.options,b=this.xIncrement,d,c=a.pointIntervalUnit,f=this.chart.time,b=u(b,a.pointStart,0);this.pointInterval=d=u(this.pointInterval,a.pointInterval,1);c&&(a=new f.Date(b),"day"===c?f.set("Date",a,f.get("Date",a)+d):"month"===c?f.set("Month",a,f.get("Month",a)+d):"year"===c&&f.set("FullYear",a,f.get("FullYear",a)+d),d=a.getTime()-b);this.xIncrement=b+d;return b},setOptions:function(a){var b=this.chart,d=b.options,c=d.plotOptions,f=(b.userOptions||{}).plotOptions||{},h=c[this.type];this.userOptions=
a;b=t(h,c.series,a);this.tooltipOptions=t(l.tooltip,l.plotOptions.series&&l.plotOptions.series.tooltip,l.plotOptions[this.type].tooltip,d.tooltip.userOptions,c.series&&c.series.tooltip,c[this.type].tooltip,a.tooltip);this.stickyTracking=u(a.stickyTracking,f[this.type]&&f[this.type].stickyTracking,f.series&&f.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:b.stickyTracking);null===h.marker&&delete b.marker;this.zoneAxis=b.zoneAxis;a=this.zones=(b.zones||[]).slice();!b.negativeColor&&
!b.negativeFillColor||b.zones||a.push({value:b[this.zoneAxis+"Threshold"]||b.threshold||0,className:"highcharts-negative",color:b.negativeColor,fillColor:b.negativeFillColor});a.length&&p(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return b},getName:function(){return this.name||"Series "+(this.index+1)},getCyclic:function(a,b,d){var c,f=this.chart,h=this.userOptions,e=a+"Index",g=a+"Counter",q=d?d.length:u(f.options.chart[a+"Count"],f[a+"Count"]);b||(c=u(h[e],h["_"+e]),
p(c)||(f.series.length||(f[g]=0),h["_"+e]=c=f[g]%q,f[g]+=1),d&&(b=d[c]));void 0!==c&&(this[e]=c);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||w[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(c,f,d,e){var z=this,h=z.points,q=h&&h.length||0,k,y=z.options,A=
z.chart,t=null,I=z.xAxis,l=y.turboThreshold,m=this.xData,J=this.yData,H=(k=z.pointArrayMap)&&k.length;c=c||[];k=c.length;f=u(f,!0);if(!1!==e&&k&&q===k&&!z.cropped&&!z.hasGroupedData&&z.visible)v(c,function(a,d){h[d].update&&a!==y.data[d]&&h[d].update(a,!1,null,!1)});else{z.xIncrement=null;z.colorCounter=0;v(this.parallelArrays,function(a){z[a+"Data"].length=0});if(l&&k>l){for(d=0;null===t&&d<k;)t=c[d],d++;if(g(t))for(d=0;d<k;d++)m[d]=this.autoIncrement(),J[d]=c[d];else if(b(t))if(H)for(d=0;d<k;d++)t=
c[d],m[d]=t[0],J[d]=t.slice(1,H+1);else for(d=0;d<k;d++)t=c[d],m[d]=t[0],J[d]=t[1];else a.error(12)}else for(d=0;d<k;d++)void 0!==c[d]&&(t={series:z},z.pointClass.prototype.applyOptions.apply(t,[c[d]]),z.updateParallelArrays(t,d));J&&n(J[0])&&a.error(14,!0);z.data=[];z.options.data=z.userOptions.data=c;for(d=q;d--;)h[d]&&h[d].destroy&&h[d].destroy();I&&(I.minRange=I.userMinRange);z.isDirty=A.isDirtyBox=!0;z.isDirtyData=!!h;d=!1}"point"===y.legendType&&(this.processData(),this.generatePoints());f&&
A.redraw(d)},processData:function(b){var c=this.xData,d=this.yData,f=c.length,e;e=0;var h,g,q=this.xAxis,k,n=this.options;k=n.cropThreshold;var y=this.getExtremesFromAll||n.getExtremesFromAll,A=this.isCartesian,n=q&&q.val2lin,t=q&&q.isLog,u=this.requireSorting,I,l;if(A&&!this.isDirty&&!q.isDirty&&!this.yAxis.isDirty&&!b)return!1;q&&(b=q.getExtremes(),I=b.min,l=b.max);if(A&&this.sorted&&!y&&(!k||f>k||this.forceCrop))if(c[f-1]<I||c[0]>l)c=[],d=[];else if(c[0]<I||c[f-1]>l)e=this.cropData(this.xData,
this.yData,I,l),c=e.xData,d=e.yData,e=e.start,h=!0;for(k=c.length||1;--k;)f=t?n(c[k])-n(c[k-1]):c[k]-c[k-1],0<f&&(void 0===g||f<g)?g=f:0>f&&u&&(a.error(15),u=!1);this.cropped=h;this.cropStart=e;this.processedXData=c;this.processedYData=d;this.closestPointRange=g},cropData:function(a,b,d,c){var f=a.length,h=0,e=f,g=u(this.cropShoulder,1),z;for(z=0;z<f;z++)if(a[z]>=d){h=Math.max(0,z-g);break}for(d=z;d<f;d++)if(a[d]>c){e=d+g;break}return{xData:a.slice(h,e),yData:b.slice(h,e),start:h,end:e}},generatePoints:function(){var a=
this.options,b=a.data,d=this.data,c,f=this.processedXData,h=this.processedYData,e=this.pointClass,g=f.length,k=this.cropStart||0,n,y=this.hasGroupedData,a=a.keys,A,t=[],u;d||y||(d=[],d.length=b.length,d=this.data=d);a&&y&&(this.options.keys=!1);for(u=0;u<g;u++)n=k+u,y?(A=(new e).init(this,[f[u]].concat(q(h[u]))),A.dataGroup=this.groupMap[u]):(A=d[n])||void 0===b[n]||(d[n]=A=(new e).init(this,b[n],f[u])),A&&(A.index=n,t[u]=A);this.options.keys=a;if(d&&(g!==(c=d.length)||y))for(u=0;u<c;u++)u!==k||y||
(u+=g),d[u]&&(d[u].destroyElements(),d[u].plotX=void 0);this.data=d;this.points=t},getExtremes:function(a){var c=this.yAxis,d=this.processedXData,f,e=[],h=0;f=this.xAxis.getExtremes();var q=f.min,k=f.max,n,y,A,t;a=a||this.stackedYData||this.processedYData||[];f=a.length;for(t=0;t<f;t++)if(y=d[t],A=a[t],n=(g(A,!0)||b(A))&&(!c.positiveValuesOnly||A.length||0<A),y=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(d[t+1]||y)>=q&&(d[t-1]||y)<=k,n&&y)if(n=A.length)for(;n--;)"number"===
typeof A[n]&&(e[h++]=A[n]);else e[h++]=A;this.dataMin=r(e);this.dataMax=F(e)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,b=a.stacking,d=this.xAxis,f=d.categories,e=this.yAxis,h=this.points,q=h.length,k=!!this.modifyValue,n=a.pointPlacement,y="between"===n||g(n),A=a.threshold,t=a.startFromThreshold?A:0,l,H,D,v,r=Number.MAX_VALUE;"between"===n&&(n=.5);g(n)&&(n*=u(a.pointRange||d.pointRange));for(a=0;a<q;a++){var w=h[a],B=w.x,C=w.y;H=w.low;var E=
b&&e.stacks[(this.negStacks&&C<(t?0:A)?"-":"")+this.stackKey],F;e.positiveValuesOnly&&null!==C&&0>=C&&(w.isNull=!0);w.plotX=l=m(Math.min(Math.max(-1E5,d.translate(B,0,0,0,1,n,"flags"===this.type)),1E5));b&&this.visible&&!w.isNull&&E&&E[B]&&(v=this.getStackIndicator(v,B,this.index),F=E[B],C=F.points[v.key],H=C[0],C=C[1],H===t&&v.key===E[B].base&&(H=u(A,e.min)),e.positiveValuesOnly&&0>=H&&(H=null),w.total=w.stackTotal=F.total,w.percentage=F.total&&w.y/F.total*100,w.stackY=C,F.setOffset(this.pointXOffset||
0,this.barW||0));w.yBottom=p(H)?Math.min(Math.max(-1E5,e.translate(H,0,1,0,1)),1E5):null;k&&(C=this.modifyValue(C,w));w.plotY=H="number"===typeof C&&Infinity!==C?Math.min(Math.max(-1E5,e.translate(C,0,1,0,1)),1E5):void 0;w.isInside=void 0!==H&&0<=H&&H<=e.len&&0<=l&&l<=d.len;w.clientX=y?m(d.translate(B,0,0,0,1,n)):l;w.negative=w.y<(A||0);w.category=f&&void 0!==f[w.x]?f[w.x]:w.x;w.isNull||(void 0!==D&&(r=Math.min(r,Math.abs(l-D))),D=l);w.zone=this.zones.length&&w.getZone()}this.closestPointRangePx=
r;c(this,"afterTranslate")},getValidPoints:function(a,b){var d=this.chart;return k(a||this.points||[],function(a){return b&&!d.isInsidePlot(a.plotX,a.plotY,d.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,d=this.options,c=b.renderer,f=b.inverted,h=this.clipBox,e=h||b.clipBox,g=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,e.height,d.xAxis,d.yAxis].join(),q=b[g],n=b[g+"m"];q||(a&&(e.width=0,f&&(e.x=b.plotSizeX),b[g+"m"]=n=c.clipRect(f?b.plotSizeX+99:-99,f?-b.plotLeft:
-b.plotTop,99,f?b.chartWidth:b.chartHeight)),b[g]=q=c.clipRect(e),q.count={length:0});a&&!q.count[this.index]&&(q.count[this.index]=!0,q.count.length+=1);!1!==d.clip&&(this.group.clip(a||h?q:b.clipRect),this.markerGroup.clip(n),this.sharedClipKey=g);a||(q.count[this.index]&&(delete q.count[this.index],--q.count.length),0===q.count.length&&g&&b[g]&&(h||(b[g]=b[g].destroy()),b[g+"m"]&&(b[g+"m"]=b[g+"m"].destroy())))},animate:function(a){var b=this.chart,d=E(this.options.animation),c;a?this.setClip(d):
(c=this.sharedClipKey,(a=b[c])&&a.animate({width:b.plotSizeX,x:0},d),b[c+"m"]&&b[c+"m"].animate({width:b.plotSizeX+99,x:0},d),this.animate=null)},afterAnimate:function(){this.setClip();c(this,"afterAnimate");this.finishedAnimating=!0},drawPoints:function(){var a=this.points,b=this.chart,d,c,f,h,e=this.options.marker,g,q,n,k=this[this.specialGroup]||this.markerGroup,y,A=u(e.enabled,this.xAxis.isRadial?!0:null,this.closestPointRangePx>=e.enabledThreshold*e.radius);if(!1!==e.enabled||this._hasPointMarkers)for(d=
0;d<a.length;d++)c=a[d],h=c.graphic,g=c.marker||{},q=!!c.marker,f=A&&void 0===g.enabled||g.enabled,n=c.isInside,f&&!c.isNull?(f=u(g.symbol,this.symbol),y=this.markerAttribs(c,c.selected&&"select"),h?h[n?"show":"hide"](!0).animate(y):n&&(0<y.width||c.hasImage)&&(c.graphic=h=b.renderer.symbol(f,y.x,y.y,y.width,y.height,q?g:e).add(k)),h&&h.attr(this.pointAttribs(c,c.selected&&"select")),h&&h.addClass(c.getClassName(),!0)):h&&(c.graphic=h.destroy())},markerAttribs:function(a,b){var d=this.options.marker,
c=a.marker||{},f=c.symbol||d.symbol,h=u(c.radius,d.radius);b&&(d=d.states[b],b=c.states&&c.states[b],h=u(b&&b.radius,d&&d.radius,h+(d&&d.radiusPlus||0)));a.hasImage=f&&0===f.indexOf("url");a.hasImage&&(h=0);a={x:Math.floor(a.plotX)-h,y:a.plotY-h};h&&(a.width=a.height=2*h);return a},pointAttribs:function(a,b){var d=this.options.marker,c=a&&a.options,f=c&&c.marker||{},h=this.color,e=c&&c.color,g=a&&a.color,c=u(f.lineWidth,d.lineWidth);a=a&&a.zone&&a.zone.color;h=e||a||g||h;a=f.fillColor||d.fillColor||
h;h=f.lineColor||d.lineColor||h;b&&(d=d.states[b],b=f.states&&f.states[b]||{},c=u(b.lineWidth,d.lineWidth,c+u(b.lineWidthPlus,d.lineWidthPlus,0)),a=b.fillColor||d.fillColor||a,h=b.lineColor||d.lineColor||h);return{stroke:h,"stroke-width":c,fill:a}},destroy:function(){var a=this,b=a.chart,d=/AppleWebKit\/533/.test(H.navigator.userAgent),e,g,h=a.data||[],q,n;c(a,"destroy");D(a);v(a.axisTypes||[],function(d){(n=a[d])&&n.series&&(B(n.series,a),n.isDirty=n.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);
for(g=h.length;g--;)(q=h[g])&&q.destroy&&q.destroy();a.points=null;clearTimeout(a.animationTimeout);f(a,function(a,b){a instanceof A&&!a.survive&&(e=d&&"group"===b?"hide":"destroy",a[e]())});b.hoverSeries===a&&(b.hoverSeries=null);B(b.series,a);b.orderSeries();f(a,function(d,b){delete a[b]})},getGraphPath:function(a,b,d){var c=this,f=c.options,h=f.step,e,g=[],q=[],n;a=a||c.points;(e=a.reversed)&&a.reverse();(h={right:1,center:2}[h]||h&&3)&&e&&(h=4-h);!f.connectNulls||b||d||(a=this.getValidPoints(a));
v(a,function(e,z){var k=e.plotX,y=e.plotY,A=a[z-1];(e.leftCliff||A&&A.rightCliff)&&!d&&(n=!0);e.isNull&&!p(b)&&0<z?n=!f.connectNulls:e.isNull&&!b?n=!0:(0===z||n?z=["M",e.plotX,e.plotY]:c.getPointSpline?z=c.getPointSpline(a,e,z):h?(z=1===h?["L",A.plotX,y]:2===h?["L",(A.plotX+k)/2,A.plotY,"L",(A.plotX+k)/2,y]:["L",k,A.plotY],z.push("L",k,y)):z=["L",k,y],q.push(e.x),h&&q.push(e.x),g.push.apply(g,z),n=!1)});g.xMap=q;return c.graphPath=g},drawGraph:function(){var a=this,b=this.options,d=(this.gappedPath||
this.getGraphPath).call(this),c=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];v(this.zones,function(d,f){c.push(["zone-graph-"+f,"highcharts-graph highcharts-zone-graph-"+f+" "+(d.className||""),d.color||a.color,d.dashStyle||b.dashStyle])});v(c,function(c,f){var h=c[0],e=a[h];e?(e.endX=a.preventGraphAnimation?null:d.xMap,e.animate({d:d})):d.length&&(a[h]=a.chart.renderer.path(d).addClass(c[1]).attr({zIndex:1}).add(a.group),e={stroke:c[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&
a.color||"none"},c[3]?e.dashstyle=c[3]:"square"!==b.linecap&&(e["stroke-linecap"]=e["stroke-linejoin"]="round"),e=a[h].attr(e).shadow(2>f&&b.shadow));e&&(e.startX=d.xMap,e.isArea=d.isArea)})},applyZones:function(){var a=this,b=this.chart,d=b.renderer,c=this.zones,f,h,e=this.clips||[],g,q=this.graph,n=this.area,k=Math.max(b.chartWidth,b.chartHeight),y=this[(this.zoneAxis||"y")+"Axis"],A,t,l=b.inverted,m,H,D,p,r=!1;c.length&&(q||n)&&y&&void 0!==y.min&&(t=y.reversed,m=y.horiz,q&&q.hide(),n&&n.hide(),
A=y.getExtremes(),v(c,function(c,z){f=t?m?b.plotWidth:0:m?0:y.toPixels(A.min);f=Math.min(Math.max(u(h,f),0),k);h=Math.min(Math.max(Math.round(y.toPixels(u(c.value,A.max),!0)),0),k);r&&(f=h=y.toPixels(A.max));H=Math.abs(f-h);D=Math.min(f,h);p=Math.max(f,h);y.isXAxis?(g={x:l?p:D,y:0,width:H,height:k},m||(g.x=b.plotHeight-g.x)):(g={x:0,y:l?p:D,width:k,height:H},m&&(g.y=b.plotWidth-g.y));l&&d.isVML&&(g=y.isXAxis?{x:0,y:t?D:p,height:g.width,width:b.chartWidth}:{x:g.y-b.plotLeft-b.spacingBox.x,y:0,width:g.height,
height:b.chartHeight});e[z]?e[z].animate(g):(e[z]=d.clipRect(g),q&&a["zone-graph-"+z].clip(e[z]),n&&a["zone-area-"+z].clip(e[z]));r=c.value>A.max}),this.clips=e)},invertGroups:function(a){function b(){v(["group","markerGroup"],function(b){d[b]&&(c.renderer.isVML&&d[b].attr({width:d.yAxis.len,height:d.xAxis.len}),d[b].width=d.yAxis.len,d[b].height=d.xAxis.len,d[b].invert(a))})}var d=this,c=d.chart,f;d.xAxis&&(f=C(c,"resize",b),C(d,"destroy",f),b(a),d.invertGroups=b)},plotGroup:function(a,b,d,c,f){var h=
this[a],e=!h;e&&(this[a]=h=this.chart.renderer.g().attr({zIndex:c||.1}).add(f));h.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series "+(p(this.colorIndex)?"highcharts-color-"+this.colorIndex+" ":"")+(this.options.className||"")+(h.hasClass("highcharts-tracker")?" highcharts-tracker":""),!0);h.attr({visibility:d})[e?"attr":"animate"](this.getPlotBox());return h},getPlotBox:function(){var a=this.chart,b=this.xAxis,d=this.yAxis;a.inverted&&(b=d,d=this.xAxis);
return{translateX:b?b.left:a.plotLeft,translateY:d?d.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,d,f=a.options,e=!!a.animate&&b.renderer.isSVG&&E(f.animation).duration,h=a.visible?"inherit":"hidden",g=f.zIndex,q=a.hasRendered,n=b.seriesGroup,k=b.inverted;d=a.plotGroup("group","series",h,g,n);a.markerGroup=a.plotGroup("markerGroup","markers",h,g,n);e&&a.animate(!0);d.inverted=a.isCartesian?k:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();
a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(k);!1===f.clip||a.sharedClipKey||q||d.clip(b.clipRect);e&&a.animate();q||(a.animationTimeout=y(function(){a.afterAnimate()},e));a.isDirty=!1;a.hasRendered=!0;c(a,"afterRender")},redraw:function(){var a=this.chart,b=this.isDirty||this.isDirtyData,d=this.group,c=this.xAxis,f=this.yAxis;d&&(a.inverted&&d.attr({width:a.plotWidth,height:a.plotHeight}),d.animate({translateX:u(c&&c.left,a.plotLeft),
translateY:u(f&&f.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var d=this.xAxis,c=this.yAxis,f=this.chart.inverted;return this.searchKDTree({clientX:f?d.len-a.chartY+d.pos:a.chartX-d.pos,plotY:f?c.len-a.chartX+c.pos:a.chartY-c.pos},b)},buildKDTree:function(){function a(d,c,f){var h,e;if(e=d&&d.length)return h=b.kdAxisArray[c%f],d.sort(function(a,d){return a[h]-d[h]}),e=Math.floor(e/2),{point:d[e],left:a(d.slice(0,
e),c+1,f),right:a(d.slice(e+1),c+1,f)}}this.buildingKdTree=!0;var b=this,d=-1<b.options.findNearestPointBy.indexOf("y")?2:1;delete b.kdTree;y(function(){b.kdTree=a(b.getValidPoints(null,!b.directTouch),d,d);b.buildingKdTree=!1},b.options.kdNow?0:1)},searchKDTree:function(a,b){function d(a,b,g,q){var n=b.point,k=c.kdAxisArray[g%q],z,y,A=n;y=p(a[f])&&p(n[f])?Math.pow(a[f]-n[f],2):null;z=p(a[h])&&p(n[h])?Math.pow(a[h]-n[h],2):null;z=(y||0)+(z||0);n.dist=p(z)?Math.sqrt(z):Number.MAX_VALUE;n.distX=p(y)?
Math.sqrt(y):Number.MAX_VALUE;k=a[k]-n[k];z=0>k?"left":"right";y=0>k?"right":"left";b[z]&&(z=d(a,b[z],g+1,q),A=z[e]<A[e]?z:n);b[y]&&Math.sqrt(k*k)<A[e]&&(a=d(a,b[y],g+1,q),A=a[e]<A[e]?a:A);return A}var c=this,f=this.kdAxisArray[0],h=this.kdAxisArray[1],e=b?"distX":"dist";b=-1<c.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||this.buildingKdTree||this.buildKDTree();if(this.kdTree)return d(a,this.kdTree,b,b)}})})(L);(function(a){var C=a.Axis,E=a.Chart,F=a.correctFloat,r=a.defined,m=a.destroyObjectProperties,
l=a.each,w=a.format,p=a.objectEach,v=a.pick,B=a.Series;a.StackItem=function(a,c,k,b,g){var e=a.chart.inverted;this.axis=a;this.isNegative=k;this.options=c;this.x=b;this.total=null;this.points={};this.stack=g;this.rightCliff=this.leftCliff=0;this.alignOptions={align:c.align||(e?k?"left":"right":"center"),verticalAlign:c.verticalAlign||(e?"middle":k?"bottom":"top"),y:v(c.y,e?4:k?14:-6),x:v(c.x,e?k?-6:6:0)};this.textAlign=c.textAlign||(e?k?"right":"left":"center")};a.StackItem.prototype={destroy:function(){m(this,
this.axis)},render:function(a){var c=this.axis.chart,e=this.options,b=e.format,b=b?w(b,this,c.time):e.formatter.call(this);this.label?this.label.attr({text:b,visibility:"hidden"}):this.label=c.renderer.text(b,null,null,e.useHTML).css(e.style).attr({align:this.textAlign,rotation:e.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,c){var e=this.axis,b=e.chart,g=e.translate(e.usePercentage?100:this.total,0,0,0,1),e=e.translate(0),e=Math.abs(g-e);a=b.xAxis[0].translate(this.x)+a;g=this.getStackBox(b,
this,a,g,c,e);if(c=this.label)c.align(this.alignOptions,null,g),g=c.alignAttr,c[!1===this.options.crop||b.isInsidePlot(g.x,g.y)?"show":"hide"](!0)},getStackBox:function(a,c,k,b,g,n){var e=c.axis.reversed,f=a.inverted;a=a.plotHeight;c=c.isNegative&&!e||!c.isNegative&&e;return{x:f?c?b:b-n:k,y:f?a-k-g:c?a-b-n:a-b,width:f?n:g,height:f?g:n}}};E.prototype.getStacks=function(){var a=this;l(a.yAxis,function(a){a.stacks&&a.hasVisibleSeries&&(a.oldStacks=a.stacks)});l(a.series,function(c){!c.options.stacking||
!0!==c.visible&&!1!==a.options.chart.ignoreHiddenSeries||(c.stackKey=c.type+v(c.options.stack,""))})};C.prototype.buildStacks=function(){var a=this.series,c=v(this.options.reversedStacks,!0),k=a.length,b;if(!this.isXAxis){this.usePercentage=!1;for(b=k;b--;)a[c?b:k-b-1].setStackedPoints();for(b=0;b<k;b++)a[b].modifyStacks()}};C.prototype.renderStackTotals=function(){var a=this.chart,c=a.renderer,k=this.stacks,b=this.stackTotalGroup;b||(this.stackTotalGroup=b=c.g("stack-labels").attr({visibility:"visible",
zIndex:6}).add());b.translate(a.plotLeft,a.plotTop);p(k,function(a){p(a,function(a){a.render(b)})})};C.prototype.resetStacks=function(){var a=this,c=a.stacks;a.isXAxis||p(c,function(c){p(c,function(b,e){b.touched<a.stacksTouched?(b.destroy(),delete c[e]):(b.total=null,b.cumulative=null)})})};C.prototype.cleanStacks=function(){var a;this.isXAxis||(this.oldStacks&&(a=this.stacks=this.oldStacks),p(a,function(a){p(a,function(a){a.cumulative=a.total})}))};B.prototype.setStackedPoints=function(){if(this.options.stacking&&
(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var e=this.processedXData,c=this.processedYData,k=[],b=c.length,g=this.options,n=g.threshold,t=v(g.startFromThreshold&&n,0),f=g.stack,g=g.stacking,u=this.stackKey,l="-"+u,q=this.negStacks,A=this.yAxis,y=A.stacks,m=A.oldStacks,p,J,d,z,G,h,x;A.stacksTouched+=1;for(G=0;G<b;G++)h=e[G],x=c[G],p=this.getStackIndicator(p,h,this.index),z=p.key,d=(J=q&&x<(t?0:n))?l:u,y[d]||(y[d]={}),y[d][h]||(m[d]&&m[d][h]?(y[d][h]=m[d][h],y[d][h].total=
null):y[d][h]=new a.StackItem(A,A.options.stackLabels,J,h,f)),d=y[d][h],null!==x?(d.points[z]=d.points[this.index]=[v(d.cumulative,t)],r(d.cumulative)||(d.base=z),d.touched=A.stacksTouched,0<p.index&&!1===this.singleStacks&&(d.points[z][0]=d.points[this.index+","+h+",0"][0])):d.points[z]=d.points[this.index]=null,"percent"===g?(J=J?u:l,q&&y[J]&&y[J][h]?(J=y[J][h],d.total=J.total=Math.max(J.total,d.total)+Math.abs(x)||0):d.total=F(d.total+(Math.abs(x)||0))):d.total=F(d.total+(x||0)),d.cumulative=v(d.cumulative,
t)+(x||0),null!==x&&(d.points[z].push(d.cumulative),k[G]=d.cumulative);"percent"===g&&(A.usePercentage=!0);this.stackedYData=k;A.oldStacks={}}};B.prototype.modifyStacks=function(){var a=this,c=a.stackKey,k=a.yAxis.stacks,b=a.processedXData,g,n=a.options.stacking;a[n+"Stacker"]&&l([c,"-"+c],function(c){for(var f=b.length,e,t;f--;)if(e=b[f],g=a.getStackIndicator(g,e,a.index,c),t=(e=k[c]&&k[c][e])&&e.points[g.key])a[n+"Stacker"](t,e,f)})};B.prototype.percentStacker=function(a,c,k){c=c.total?100/c.total:
0;a[0]=F(a[0]*c);a[1]=F(a[1]*c);this.stackedYData[k]=a[1]};B.prototype.getStackIndicator=function(a,c,k,b){!r(a)||a.x!==c||b&&a.key!==b?a={x:c,index:0,key:b}:a.index++;a.key=[k,c,a.index].join();return a}})(L);(function(a){var C=a.addEvent,E=a.animate,F=a.Axis,r=a.createElement,m=a.css,l=a.defined,w=a.each,p=a.erase,v=a.extend,B=a.fireEvent,e=a.inArray,c=a.isNumber,k=a.isObject,b=a.isArray,g=a.merge,n=a.objectEach,t=a.pick,f=a.Point,u=a.Series,D=a.seriesTypes,q=a.setAnimation,A=a.splat;v(a.Chart.prototype,
{addSeries:function(a,b,c){var f,d=this;a&&(b=t(b,!0),B(d,"addSeries",{options:a},function(){f=d.initSeries(a);d.isDirtyLegend=!0;d.linkSeries();b&&d.redraw(c)}));return f},addAxis:function(a,b,c,f){var d=b?"xAxis":"yAxis",e=this.options;a=g(a,{index:this[d].length,isX:b});b=new F(this,a);e[d]=A(e[d]||{});e[d].push(a);t(c,!0)&&this.redraw(f);return b},showLoading:function(a){var b=this,c=b.options,f=b.loadingDiv,d=c.loading,e=function(){f&&m(f,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+
"px",height:b.plotHeight+"px"})};f||(b.loadingDiv=f=r("div",{className:"highcharts-loading highcharts-loading-hidden"},null,b.container),b.loadingSpan=r("span",{className:"highcharts-loading-inner"},null,f),C(b,"redraw",e));f.className="highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;m(f,v(d.style,{zIndex:10}));m(b.loadingSpan,d.labelStyle);b.loadingShown||(m(f,{opacity:0,display:""}),E(f,{opacity:d.style.opacity||.5},{duration:d.showDuration||0}));b.loadingShown=!0;e()},hideLoading:function(){var a=
this.options,b=this.loadingDiv;b&&(b.className="highcharts-loading highcharts-loading-hidden",E(b,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){m(b,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions time tooltip".split(" "),update:function(a,b,f){var q=this,d={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},z=a.chart,k,h,y=[];if(z){g(!0,q.options.chart,z);"className"in z&&q.setClassName(z.className);if("inverted"in z||"polar"in z)q.propFromSeries(),k=!0;"alignTicks"in z&&(k=!0);n(z,function(a,d){-1!==e("chart."+d,q.propsRequireUpdateSeries)&&(h=!0);-1!==e(d,q.propsRequireDirtyBox)&&
(q.isDirtyBox=!0)});"style"in z&&q.renderer.setStyle(z.style)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&g(!0,this.options.plotOptions,a.plotOptions);n(a,function(a,b){if(q[b]&&"function"===typeof q[b].update)q[b].update(a,!1);else if("function"===typeof q[d[b]])q[d[b]](a);"chart"!==b&&-1!==e(b,q.propsRequireUpdateSeries)&&(h=!0)});w("xAxis yAxis zAxis series colorAxis pane".split(" "),function(d){a[d]&&(w(A(a[d]),function(a,b){(b=l(a.id)&&q.get(a.id)||q[d][b])&&b.coll===d&&(b.update(a,
!1),f&&(b.touched=!0));if(!b&&f)if("series"===d)q.addSeries(a,!1).touched=!0;else if("xAxis"===d||"yAxis"===d)q.addAxis(a,"xAxis"===d,!1).touched=!0}),f&&w(q[d],function(a){a.touched?delete a.touched:y.push(a)}))});w(y,function(a){a.remove(!1)});k&&w(q.axes,function(a){a.update({},!1)});h&&w(q.series,function(a){a.update({},!1)});a.loading&&g(!0,q.options.loading,a.loading);k=z&&z.width;z=z&&z.height;c(k)&&k!==q.chartWidth||c(z)&&z!==q.chartHeight?q.setSize(k,z):t(b,!0)&&q.redraw()},setSubtitle:function(a){this.setTitle(void 0,
a)}});v(f.prototype,{update:function(a,b,c,f){function d(){e.applyOptions(a);null===e.y&&h&&(e.graphic=h.destroy());k(a,!0)&&(h&&h.element&&a&&a.marker&&void 0!==a.marker.symbol&&(e.graphic=h.destroy()),a&&a.dataLabels&&e.dataLabel&&(e.dataLabel=e.dataLabel.destroy()),e.connector&&(e.connector=e.connector.destroy()));q=e.index;g.updateParallelArrays(e,q);A.data[q]=k(A.data[q],!0)||k(a,!0)?e.options:a;g.isDirty=g.isDirtyData=!0;!g.fixedBox&&g.hasCartesianSeries&&(n.isDirtyBox=!0);"point"===A.legendType&&
(n.isDirtyLegend=!0);b&&n.redraw(c)}var e=this,g=e.series,h=e.graphic,q,n=g.chart,A=g.options;b=t(b,!0);!1===f?d():e.firePointEvent("update",{options:a},d)},remove:function(a,b){this.series.removePoint(e(this,this.series.data),a,b)}});v(u.prototype,{addPoint:function(a,b,c,f){var d=this.options,e=this.data,g=this.chart,h=this.xAxis,h=h&&h.hasNames&&h.names,q=d.data,n,k,A=this.xData,y,u;b=t(b,!0);n={series:this};this.pointClass.prototype.applyOptions.apply(n,[a]);u=n.x;y=A.length;if(this.requireSorting&&
u<A[y-1])for(k=!0;y&&A[y-1]>u;)y--;this.updateParallelArrays(n,"splice",y,0,0);this.updateParallelArrays(n,y);h&&n.name&&(h[u]=n.name);q.splice(y,0,a);k&&(this.data.splice(y,0,null),this.processData());"point"===d.legendType&&this.generatePoints();c&&(e[0]&&e[0].remove?e[0].remove(!1):(e.shift(),this.updateParallelArrays(n,"shift"),q.shift()));this.isDirtyData=this.isDirty=!0;b&&g.redraw(f)},removePoint:function(a,b,c){var f=this,d=f.data,e=d[a],g=f.points,h=f.chart,n=function(){g&&g.length===d.length&&
g.splice(a,1);d.splice(a,1);f.options.data.splice(a,1);f.updateParallelArrays(e||{series:f},"splice",a,1);e&&e.destroy();f.isDirty=!0;f.isDirtyData=!0;b&&h.redraw()};q(c,h);b=t(b,!0);e?e.firePointEvent("remove",null,n):n()},remove:function(a,b,c){function f(){d.destroy();e.isDirtyLegend=e.isDirtyBox=!0;e.linkSeries();t(a,!0)&&e.redraw(b)}var d=this,e=d.chart;!1!==c?B(d,"remove",null,f):f()},update:function(a,b){var c=this,f=c.chart,d=c.userOptions,e=c.oldType||c.type,q=a.type||d.type||f.options.chart.type,
h=D[e].prototype,n,k=["group","markerGroup","dataLabelsGroup"],A=["navigatorSeries","baseSeries"],y=c.finishedAnimating&&{animation:!1};if(Object.keys&&"data"===Object.keys(a).toString())return this.setData(a.data,b);A=k.concat(A);w(A,function(a){A[a]=c[a];delete c[a]});a=g(d,y,{index:c.index,pointStart:c.xData[0]},{data:c.options.data},a);c.remove(!1,null,!1);for(n in h)c[n]=void 0;v(c,D[q||e].prototype);w(A,function(a){c[a]=A[a]});c.init(f,a);a.zIndex!==d.zIndex&&w(k,function(d){c[d]&&c[d].attr({zIndex:a.zIndex})});
c.oldType=e;f.linkSeries();t(b,!0)&&f.redraw(!1)}});v(F.prototype,{update:function(a,b){var c=this.chart;a=c.options[this.coll][this.options.index]=g(this.userOptions,a);this.destroy(!0);this.init(c,v(a,{events:void 0}));c.isDirtyBox=!0;t(b,!0)&&c.redraw()},remove:function(a){for(var c=this.chart,f=this.coll,e=this.series,d=e.length;d--;)e[d]&&e[d].remove(!1);p(c.axes,this);p(c[f],this);b(c.options[f])?c.options[f].splice(this.options.index,1):delete c.options[f];w(c[f],function(a,d){a.options.index=
d});this.destroy();c.isDirtyBox=!0;t(a,!0)&&c.redraw()},setTitle:function(a,b){this.update({title:a},b)},setCategories:function(a,b){this.update({categories:a},b)}})})(L);(function(a){var C=a.color,E=a.each,F=a.map,r=a.pick,m=a.Series,l=a.seriesType;l("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(l){var m=[],v=[],w=this.xAxis,e=this.yAxis,c=e.stacks[this.stackKey],k={},b=this.index,g=e.series,n=g.length,t,f=r(e.options.reversedStacks,!0)?1:-1,u;l=l||this.points;
if(this.options.stacking){for(u=0;u<l.length;u++)l[u].leftNull=l[u].rightNull=null,k[l[u].x]=l[u];a.objectEach(c,function(a,b){null!==a.total&&v.push(b)});v.sort(function(a,b){return a-b});t=F(g,function(){return this.visible});E(v,function(a,g){var q=0,y,l;if(k[a]&&!k[a].isNull)m.push(k[a]),E([-1,1],function(e){var q=1===e?"rightNull":"leftNull",d=0,A=c[v[g+e]];if(A)for(u=b;0<=u&&u<n;)y=A.points[u],y||(u===b?k[a][q]=!0:t[u]&&(l=c[a].points[u])&&(d-=l[1]-l[0])),u+=f;k[a][1===e?"rightCliff":"leftCliff"]=
d});else{for(u=b;0<=u&&u<n;){if(y=c[a].points[u]){q=y[1];break}u+=f}q=e.translate(q,0,1,0,1);m.push({isNull:!0,plotX:w.translate(a,0,0,0,1),x:a,plotY:q,yBottom:q})}})}return m},getGraphPath:function(a){var l=m.prototype.getGraphPath,v=this.options,w=v.stacking,e=this.yAxis,c,k,b=[],g=[],n=this.index,t,f=e.stacks[this.stackKey],u=v.threshold,D=e.getThreshold(v.threshold),q,v=v.connectNulls||"percent"===w,A=function(c,q,k){var A=a[c];c=w&&f[A.x].points[n];var d=A[k+"Null"]||0;k=A[k+"Cliff"]||0;var z,
y,A=!0;k||d?(z=(d?c[0]:c[1])+k,y=c[0]+k,A=!!d):!w&&a[q]&&a[q].isNull&&(z=y=u);void 0!==z&&(g.push({plotX:t,plotY:null===z?D:e.getThreshold(z),isNull:A,isCliff:!0}),b.push({plotX:t,plotY:null===y?D:e.getThreshold(y),doCurve:!1}))};a=a||this.points;w&&(a=this.getStackPoints(a));for(c=0;c<a.length;c++)if(k=a[c].isNull,t=r(a[c].rectPlotX,a[c].plotX),q=r(a[c].yBottom,D),!k||v)v||A(c,c-1,"left"),k&&!w&&v||(g.push(a[c]),b.push({x:c,plotX:t,plotY:q})),v||A(c,c+1,"right");c=l.call(this,g,!0,!0);b.reversed=
!0;k=l.call(this,b,!0,!0);k.length&&(k[0]="L");k=c.concat(k);l=l.call(this,g,!1,v);k.xMap=c.xMap;this.areaPath=k;return l},drawGraph:function(){this.areaPath=[];m.prototype.drawGraph.apply(this);var a=this,l=this.areaPath,v=this.options,B=[["area","highcharts-area",this.color,v.fillColor]];E(this.zones,function(e,c){B.push(["zone-area-"+c,"highcharts-area highcharts-zone-area-"+c+" "+e.className,e.color||a.color,e.fillColor||v.fillColor])});E(B,function(e){var c=e[0],k=a[c];k?(k.endX=a.preventGraphAnimation?
null:l.xMap,k.animate({d:l})):(k=a[c]=a.chart.renderer.path(l).addClass(e[1]).attr({fill:r(e[3],C(e[2]).setOpacity(r(v.fillOpacity,.75)).get()),zIndex:0}).add(a.group),k.isArea=!0);k.startX=l.xMap;k.shiftUnit=v.step?2:1})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(L);(function(a){var C=a.pick;a=a.seriesType;a("spline","line",{},{getPointSpline:function(a,F,r){var m=F.plotX,l=F.plotY,w=a[r-1];r=a[r+1];var p,v,B,e;if(w&&!w.isNull&&!1!==w.doCurve&&!F.isCliff&&r&&!r.isNull&&!1!==r.doCurve&&
!F.isCliff){a=w.plotY;B=r.plotX;r=r.plotY;var c=0;p=(1.5*m+w.plotX)/2.5;v=(1.5*l+a)/2.5;B=(1.5*m+B)/2.5;e=(1.5*l+r)/2.5;B!==p&&(c=(e-v)*(B-m)/(B-p)+l-e);v+=c;e+=c;v>a&&v>l?(v=Math.max(a,l),e=2*l-v):v<a&&v<l&&(v=Math.min(a,l),e=2*l-v);e>r&&e>l?(e=Math.max(r,l),v=2*l-e):e<r&&e<l&&(e=Math.min(r,l),v=2*l-e);F.rightContX=B;F.rightContY=e}F=["C",C(w.rightContX,w.plotX),C(w.rightContY,w.plotY),C(p,m),C(v,l),m,l];w.rightContX=w.rightContY=null;return F}})})(L);(function(a){var C=a.seriesTypes.area.prototype,
E=a.seriesType;E("areaspline","spline",a.defaultPlotOptions.area,{getStackPoints:C.getStackPoints,getGraphPath:C.getGraphPath,drawGraph:C.drawGraph,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(L);(function(a){var C=a.animObject,E=a.color,F=a.each,r=a.extend,m=a.isNumber,l=a.merge,w=a.pick,p=a.Series,v=a.seriesType,B=a.svg;v("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1},
select:{color:"#cccccc",borderColor:"#000000"}},dataLabels:{align:null,verticalAlign:null,y:null},softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){p.prototype.init.apply(this,arguments);var a=this,c=a.chart;c.hasRendered&&F(c.series,function(c){c.type===a.type&&(c.isDirty=!0)})},getColumnMetrics:function(){var a=this,c=a.options,k=a.xAxis,
b=a.yAxis,g=k.reversed,n,t={},f=0;!1===c.grouping?f=1:F(a.chart.series,function(c){var e=c.options,g=c.yAxis,q;c.type!==a.type||!c.visible&&a.chart.options.chart.ignoreHiddenSeries||b.len!==g.len||b.pos!==g.pos||(e.stacking?(n=c.stackKey,void 0===t[n]&&(t[n]=f++),q=t[n]):!1!==e.grouping&&(q=f++),c.columnIndex=q)});var l=Math.min(Math.abs(k.transA)*(k.ordinalSlope||c.pointRange||k.closestPointRange||k.tickInterval||1),k.len),m=l*c.groupPadding,q=(l-2*m)/(f||1),c=Math.min(c.maxPointWidth||k.len,w(c.pointWidth,
q*(1-2*c.pointPadding)));a.columnMetrics={width:c,offset:(q-c)/2+(m+((a.columnIndex||0)+(g?1:0))*q-l/2)*(g?-1:1)};return a.columnMetrics},crispCol:function(a,c,k,b){var e=this.chart,n=this.borderWidth,t=-(n%2?.5:0),n=n%2?.5:1;e.inverted&&e.renderer.isVML&&(n+=1);this.options.crisp&&(k=Math.round(a+k)+t,a=Math.round(a)+t,k-=a);b=Math.round(c+b)+n;t=.5>=Math.abs(c)&&.5<b;c=Math.round(c)+n;b-=c;t&&b&&(--c,b+=1);return{x:a,y:c,width:k,height:b}},translate:function(){var a=this,c=a.chart,k=a.options,b=
a.dense=2>a.closestPointRange*a.xAxis.transA,b=a.borderWidth=w(k.borderWidth,b?0:1),g=a.yAxis,n=k.threshold,t=a.translatedThreshold=g.getThreshold(n),f=w(k.minPointLength,5),l=a.getColumnMetrics(),m=l.width,q=a.barW=Math.max(m,1+2*b),A=a.pointXOffset=l.offset;c.inverted&&(t-=.5);k.pointPadding&&(q=Math.ceil(q));p.prototype.translate.apply(a);F(a.points,function(b){var e=w(b.yBottom,t),k=999+Math.abs(e),k=Math.min(Math.max(-k,b.plotY),g.len+k),y=b.plotX+A,d=q,z=Math.min(k,e),l,h=Math.max(k,e)-z;f&&
Math.abs(h)<f&&(h=f,l=!g.reversed&&!b.negative||g.reversed&&b.negative,b.y===n&&a.dataMax<=n&&g.min<n&&(l=!l),z=Math.abs(z-t)>f?e-f:t-(l?f:0));b.barX=y;b.pointWidth=m;b.tooltipPos=c.inverted?[g.len+g.pos-c.plotLeft-k,a.xAxis.len-y-d/2,h]:[y+d/2,k+g.pos-c.plotTop,h];b.shapeType="rect";b.shapeArgs=a.crispCol.apply(a,b.isNull?[y,t,d,0]:[y,z,d,h])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},
pointAttribs:function(a,c){var e=this.options,b,g=this.pointAttrToOptions||{};b=g.stroke||"borderColor";var n=g["stroke-width"]||"borderWidth",t=a&&a.color||this.color,f=a&&a[b]||e[b]||this.color||t,u=a&&a[n]||e[n]||this[n]||0,g=e.dashStyle;a&&this.zones.length&&(t=a.getZone(),t=a.options.color||t&&t.color||this.color);c&&(a=l(e.states[c],a.options.states&&a.options.states[c]||{}),c=a.brightness,t=a.color||void 0!==c&&E(t).brighten(a.brightness).get()||t,f=a[b]||f,u=a[n]||u,g=a.dashStyle||g);b={fill:t,
stroke:f,"stroke-width":u};g&&(b.dashstyle=g);return b},drawPoints:function(){var a=this,c=this.chart,k=a.options,b=c.renderer,g=k.animationLimit||250,n;F(a.points,function(e){var f=e.graphic;if(m(e.plotY)&&null!==e.y){n=e.shapeArgs;if(f)f[c.pointCount<g?"animate":"attr"](l(n));else e.graphic=f=b[e.shapeType](n).add(e.group||a.group);k.borderRadius&&f.attr({r:k.borderRadius});f.attr(a.pointAttribs(e,e.selected&&"select")).shadow(k.shadow,null,k.stacking&&!k.borderRadius);f.addClass(e.getClassName(),
!0)}else f&&(e.graphic=f.destroy())})},animate:function(a){var c=this,e=this.yAxis,b=c.options,g=this.chart.inverted,n={},l=g?"translateX":"translateY",f;B&&(a?(n.scaleY=.001,a=Math.min(e.pos+e.len,Math.max(e.pos,e.toPixels(b.threshold))),g?n.translateX=a-e.len:n.translateY=a,c.group.attr(n)):(f=c.group.attr(l),c.group.animate({scaleY:1},r(C(c.options.animation),{step:function(a,b){n[l]=f+b.pos*(e.pos-f);c.group.attr(n)}})),c.animate=null))},remove:function(){var a=this,c=a.chart;c.hasRendered&&F(c.series,
function(c){c.type===a.type&&(c.isDirty=!0)});p.prototype.remove.apply(a,arguments)}})})(L);(function(a){a=a.seriesType;a("bar","column",null,{inverted:!0})})(L);(function(a){var C=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,findNearestPointBy:"xy",marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},
{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&C.prototype.drawGraph.call(this)}})})(L);(function(a){var C=a.deg2rad,E=a.isNumber,F=a.pick,r=a.relativeLength;a.CenteredSeriesMixin={getCenter:function(){var a=this.options,l=this.chart,w=2*(a.slicedOffset||0),p=l.plotWidth-2*w,l=l.plotHeight-2*w,v=a.center,v=[F(v[0],"50%"),F(v[1],"50%"),a.size||"100%",a.innerSize||0],B=Math.min(p,
l),e,c;for(e=0;4>e;++e)c=v[e],a=2>e||2===e&&/%$/.test(c),v[e]=r(c,[p,l,B,v[2]][e])+(a?w:0);v[3]>v[2]&&(v[3]=v[2]);return v},getStartAndEndRadians:function(a,l){a=E(a)?a:0;l=E(l)&&l>a&&360>l-a?l:a+360;return{start:C*(a+-90),end:C*(l+-90)}}}})(L);(function(a){var C=a.addEvent,E=a.CenteredSeriesMixin,F=a.defined,r=a.each,m=a.extend,l=E.getStartAndEndRadians,w=a.inArray,p=a.noop,v=a.pick,B=a.Point,e=a.Series,c=a.seriesType,k=a.setAnimation;c("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,
enabled:!0,formatter:function(){return this.point.isNull?void 0:this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,states:{hover:{brightness:.1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:a.seriesTypes.column.prototype.pointAttribs,animate:function(a){var b=this,
c=b.points,e=b.startAngleRad;a||(r(c,function(a){var c=a.graphic,f=a.shapeArgs;c&&(c.attr({r:a.startR||b.center[3]/2,start:e,end:e}),c.animate({r:f.r,start:f.start,end:f.end},b.options.animation))}),b.animate=null)},updateTotals:function(){var a,c=0,e=this.points,k=e.length,f,l=this.options.ignoreHiddenPoint;for(a=0;a<k;a++)f=e[a],c+=l&&!f.visible?0:f.isNull?0:f.y;this.total=c;for(a=0;a<k;a++)f=e[a],f.percentage=0<c&&(f.visible||!l)?f.y/c*100:0,f.total=c},generatePoints:function(){e.prototype.generatePoints.call(this);
this.updateTotals()},translate:function(a){this.generatePoints();var b=0,c=this.options,e=c.slicedOffset,f=e+(c.borderWidth||0),k,m,q,A=l(c.startAngle,c.endAngle),y=this.startAngleRad=A.start,A=(this.endAngleRad=A.end)-y,H=this.points,p,r=c.dataLabels.distance,c=c.ignoreHiddenPoint,d,z=H.length,G;a||(this.center=a=this.getCenter());this.getX=function(d,b,c){q=Math.asin(Math.min((d-a[1])/(a[2]/2+c.labelDistance),1));return a[0]+(b?-1:1)*Math.cos(q)*(a[2]/2+c.labelDistance)};for(d=0;d<z;d++){G=H[d];
G.labelDistance=v(G.options.dataLabels&&G.options.dataLabels.distance,r);this.maxLabelDistance=Math.max(this.maxLabelDistance||0,G.labelDistance);k=y+b*A;if(!c||G.visible)b+=G.percentage/100;m=y+b*A;G.shapeType="arc";G.shapeArgs={x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:Math.round(1E3*k)/1E3,end:Math.round(1E3*m)/1E3};q=(m+k)/2;q>1.5*Math.PI?q-=2*Math.PI:q<-Math.PI/2&&(q+=2*Math.PI);G.slicedTranslation={translateX:Math.round(Math.cos(q)*e),translateY:Math.round(Math.sin(q)*e)};m=Math.cos(q)*a[2]/
2;p=Math.sin(q)*a[2]/2;G.tooltipPos=[a[0]+.7*m,a[1]+.7*p];G.half=q<-Math.PI/2||q>Math.PI/2?1:0;G.angle=q;k=Math.min(f,G.labelDistance/5);G.labelPos=[a[0]+m+Math.cos(q)*G.labelDistance,a[1]+p+Math.sin(q)*G.labelDistance,a[0]+m+Math.cos(q)*k,a[1]+p+Math.sin(q)*k,a[0]+m,a[1]+p,0>G.labelDistance?"center":G.half?"right":"left",q]}},drawGraph:null,drawPoints:function(){var a=this,c=a.chart.renderer,e,k,f,l,D=a.options.shadow;D&&!a.shadowGroup&&(a.shadowGroup=c.g("shadow").add(a.group));r(a.points,function(b){k=
b.graphic;if(b.isNull)k&&(b.graphic=k.destroy());else{l=b.shapeArgs;e=b.getTranslate();var g=b.shadowGroup;D&&!g&&(g=b.shadowGroup=c.g("shadow").add(a.shadowGroup));g&&g.attr(e);f=a.pointAttribs(b,b.selected&&"select");k?k.setRadialReference(a.center).attr(f).animate(m(l,e)):(b.graphic=k=c[b.shapeType](l).setRadialReference(a.center).attr(e).add(a.group),b.visible||k.attr({visibility:"hidden"}),k.attr(f).attr({"stroke-linejoin":"round"}).shadow(D,g));k.addClass(b.getClassName())}})},searchPoint:p,
sortByAngle:function(a,c){a.sort(function(a,b){return void 0!==a.angle&&(b.angle-a.angle)*c})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,getCenter:E.getCenter,getSymbol:p},{init:function(){B.prototype.init.apply(this,arguments);var a=this,c;a.name=v(a.name,"Slice");c=function(b){a.slice("select"===b.type)};C(a,"select",c);C(a,"unselect",c);return a},isValid:function(){return a.isNumber(this.y,!0)&&0<=this.y},setVisible:function(a,c){var b=this,e=b.series,f=e.chart,g=e.options.ignoreHiddenPoint;
c=v(c,g);a!==b.visible&&(b.visible=b.options.visible=a=void 0===a?!b.visible:a,e.options.data[w(b,e.data)]=b.options,r(["graphic","dataLabel","connector","shadowGroup"],function(c){if(b[c])b[c][a?"show":"hide"](!0)}),b.legendItem&&f.legend.colorizeItem(b,a),a||"hover"!==b.state||b.setState(""),g&&(e.isDirty=!0),c&&f.redraw())},slice:function(a,c,e){var b=this.series;k(e,b.chart);v(c,!0);this.sliced=this.options.sliced=F(a)?a:!this.sliced;b.options.data[w(this,b.data)]=this.options;this.graphic.animate(this.getTranslate());
this.shadowGroup&&this.shadowGroup.animate(this.getTranslate())},getTranslate:function(){return this.sliced?this.slicedTranslation:{translateX:0,translateY:0}},haloPath:function(a){var b=this.shapeArgs;return this.sliced||!this.visible?[]:this.series.chart.renderer.symbols.arc(b.x,b.y,b.r+a,b.r+a,{innerR:this.shapeArgs.r-1,start:b.start,end:b.end})}})})(L);(function(a){var C=a.addEvent,E=a.arrayMax,F=a.defined,r=a.each,m=a.extend,l=a.format,w=a.map,p=a.merge,v=a.noop,B=a.pick,e=a.relativeLength,c=
a.Series,k=a.seriesTypes,b=a.stableSort;a.distribute=function(a,c){function e(a,b){return a.target-b.target}var f,g=!0,k=a,q=[],n;n=0;for(f=a.length;f--;)n+=a[f].size;if(n>c){b(a,function(a,b){return(b.rank||0)-(a.rank||0)});for(n=f=0;n<=c;)n+=a[f].size,f++;q=a.splice(f-1,a.length)}b(a,e);for(a=w(a,function(a){return{size:a.size,targets:[a.target],align:B(a.align,.5)}});g;){for(f=a.length;f--;)g=a[f],n=(Math.min.apply(0,g.targets)+Math.max.apply(0,g.targets))/2,g.pos=Math.min(Math.max(0,n-g.size*
g.align),c-g.size);f=a.length;for(g=!1;f--;)0<f&&a[f-1].pos+a[f-1].size>a[f].pos&&(a[f-1].size+=a[f].size,a[f-1].targets=a[f-1].targets.concat(a[f].targets),a[f-1].align=.5,a[f-1].pos+a[f-1].size>c&&(a[f-1].pos=c-a[f-1].size),a.splice(f,1),g=!0)}f=0;r(a,function(a){var b=0;r(a.targets,function(){k[f].pos=a.pos+b;b+=k[f].size;f++})});k.push.apply(k,q);b(k,e)};c.prototype.drawDataLabels=function(){function b(a,d){var b=d.filter;return b?(d=b.operator,a=a[b.property],b=b.value,"\x3e"===d&&a>b||"\x3c"===
d&&a<b||"\x3e\x3d"===d&&a>=b||"\x3c\x3d"===d&&a<=b||"\x3d\x3d"===d&&a==b||"\x3d\x3d\x3d"===d&&a===b?!0:!1):!0}var c=this,e=c.chart,f=c.options,k=f.dataLabels,m=c.points,q,A,y=c.hasRendered||0,H,v,w=B(k.defer,!!f.animation),d=e.renderer;if(k.enabled||c._hasPointLabels)c.dlProcessOptions&&c.dlProcessOptions(k),v=c.plotGroup("dataLabelsGroup","data-labels",w&&!y?"hidden":"visible",k.zIndex||6),w&&(v.attr({opacity:+y}),y||C(c,"afterAnimate",function(){c.visible&&v.show(!0);v[f.animation?"animate":"attr"]({opacity:1},
{duration:200})})),A=k,r(m,function(g){var n,h=g.dataLabel,z,y,t=g.connector,m=!h,u;q=g.dlOptions||g.options&&g.options.dataLabels;(n=B(q&&q.enabled,A.enabled)&&!g.isNull)&&(n=!0===b(g,q||k));n&&(k=p(A,q),z=g.getLabelConfig(),u=k[g.formatPrefix+"Format"]||k.format,H=F(u)?l(u,z,e.time):(k[g.formatPrefix+"Formatter"]||k.formatter).call(z,k),u=k.style,z=k.rotation,u.color=B(k.color,u.color,c.color,"#000000"),"contrast"===u.color&&(g.contrastColor=d.getContrast(g.color||c.color),u.color=k.inside||0>B(g.labelDistance,
k.distance)||f.stacking?g.contrastColor:"#000000"),f.cursor&&(u.cursor=f.cursor),y={fill:k.backgroundColor,stroke:k.borderColor,"stroke-width":k.borderWidth,r:k.borderRadius||0,rotation:z,padding:k.padding,zIndex:1},a.objectEach(y,function(a,d){void 0===a&&delete y[d]}));!h||n&&F(H)?n&&F(H)&&(h?y.text=H:(h=g.dataLabel=z?d.text(H,0,-9999).addClass("highcharts-data-label"):d.label(H,0,-9999,k.shape,null,null,k.useHTML,null,"data-label"),h.addClass(" highcharts-data-label-color-"+g.colorIndex+" "+(k.className||
"")+(k.useHTML?"highcharts-tracker":""))),h.attr(y),h.css(u).shadow(k.shadow),h.added||h.add(v),c.alignDataLabel(g,h,k,null,m)):(g.dataLabel=h=h.destroy(),t&&(g.connector=t.destroy()))});a.fireEvent(this,"afterDrawDataLabels")};c.prototype.alignDataLabel=function(a,b,c,f,e){var g=this.chart,q=g.inverted,k=B(a.dlBox&&a.dlBox.centerX,a.plotX,-9999),n=B(a.plotY,-9999),l=b.getBBox(),t,u=c.rotation,d=c.align,z=this.visible&&(a.series.forceDL||g.isInsidePlot(k,Math.round(n),q)||f&&g.isInsidePlot(k,q?f.x+
1:f.y+f.height-1,q)),G="justify"===B(c.overflow,"justify");if(z&&(t=c.style.fontSize,t=g.renderer.fontMetrics(t,b).b,f=m({x:q?this.yAxis.len-n:k,y:Math.round(q?this.xAxis.len-k:n),width:0,height:0},f),m(c,{width:l.width,height:l.height}),u?(G=!1,k=g.renderer.rotCorr(t,u),k={x:f.x+c.x+f.width/2+k.x,y:f.y+c.y+{top:0,middle:.5,bottom:1}[c.verticalAlign]*f.height},b[e?"attr":"animate"](k).attr({align:d}),n=(u+720)%360,n=180<n&&360>n,"left"===d?k.y-=n?l.height:0:"center"===d?(k.x-=l.width/2,k.y-=l.height/
2):"right"===d&&(k.x-=l.width,k.y-=n?0:l.height)):(b.align(c,null,f),k=b.alignAttr),G?a.isLabelJustified=this.justifyDataLabel(b,c,k,l,f,e):B(c.crop,!0)&&(z=g.isInsidePlot(k.x,k.y)&&g.isInsidePlot(k.x+l.width,k.y+l.height)),c.shape&&!u))b[e?"attr":"animate"]({anchorX:q?g.plotWidth-a.plotY:a.plotX,anchorY:q?g.plotHeight-a.plotX:a.plotY});z||(b.attr({y:-9999}),b.placed=!1)};c.prototype.justifyDataLabel=function(a,b,c,f,e,k){var g=this.chart,n=b.align,l=b.verticalAlign,m,t,u=a.box?0:a.padding||0;m=c.x+
u;0>m&&("right"===n?b.align="left":b.x=-m,t=!0);m=c.x+f.width-u;m>g.plotWidth&&("left"===n?b.align="right":b.x=g.plotWidth-m,t=!0);m=c.y+u;0>m&&("bottom"===l?b.verticalAlign="top":b.y=-m,t=!0);m=c.y+f.height-u;m>g.plotHeight&&("top"===l?b.verticalAlign="bottom":b.y=g.plotHeight-m,t=!0);t&&(a.placed=!k,a.align(b,null,e));return t};k.pie&&(k.pie.prototype.drawDataLabels=function(){var b=this,e=b.data,k,f=b.chart,l=b.options.dataLabels,m=B(l.connectorPadding,10),q=B(l.connectorWidth,1),A=f.plotWidth,
y=f.plotHeight,p,v=b.center,w=v[2]/2,d=v[1],z,G,h,x,N=[[],[]],O,C,M,Q,K=[0,0,0,0];b.visible&&(l.enabled||b._hasPointLabels)&&(r(e,function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),c.prototype.drawDataLabels.apply(b),r(e,function(a){a.dataLabel&&a.visible&&(N[a.half].push(a),a.dataLabel._pos=null)}),r(N,function(c,e){var g,q,n=c.length,t=[],u;if(n)for(b.sortByAngle(c,e-.5),0<b.maxLabelDistance&&
(g=Math.max(0,d-w-b.maxLabelDistance),q=Math.min(d+w+b.maxLabelDistance,f.plotHeight),r(c,function(a){0<a.labelDistance&&a.dataLabel&&(a.top=Math.max(0,d-w-a.labelDistance),a.bottom=Math.min(d+w+a.labelDistance,f.plotHeight),u=a.dataLabel.getBBox().height||21,a.positionsIndex=t.push({target:a.labelPos[1]-a.top+u/2,size:u,rank:a.y})-1)}),a.distribute(t,q+u-g)),Q=0;Q<n;Q++)k=c[Q],q=k.positionsIndex,h=k.labelPos,z=k.dataLabel,M=!1===k.visible?"hidden":"inherit",C=g=h[1],t&&F(t[q])&&(void 0===t[q].pos?
M="hidden":(x=t[q].size,C=k.top+t[q].pos)),delete k.positionIndex,O=l.justify?v[0]+(e?-1:1)*(w+k.labelDistance):b.getX(C<k.top+2||C>k.bottom-2?g:C,e,k),z._attr={visibility:M,align:h[6]},z._pos={x:O+l.x+({left:m,right:-m}[h[6]]||0),y:C+l.y-10},h.x=O,h.y=C,B(l.crop,!0)&&(G=z.getBBox().width,g=null,O-G<m?(g=Math.round(G-O+m),K[3]=Math.max(g,K[3])):O+G>A-m&&(g=Math.round(O+G-A+m),K[1]=Math.max(g,K[1])),0>C-x/2?K[0]=Math.max(Math.round(-C+x/2),K[0]):C+x/2>y&&(K[2]=Math.max(Math.round(C+x/2-y),K[2])),z.sideOverflow=
g)}),0===E(K)||this.verifyDataLabelOverflow(K))&&(this.placeDataLabels(),q&&r(this.points,function(a){var d;p=a.connector;if((z=a.dataLabel)&&z._pos&&a.visible&&0<a.labelDistance){M=z._attr.visibility;if(d=!p)a.connector=p=f.renderer.path().addClass("highcharts-data-label-connector  highcharts-color-"+a.colorIndex).add(b.dataLabelsGroup),p.attr({"stroke-width":q,stroke:l.connectorColor||a.color||"#666666"});p[d?"attr":"animate"]({d:b.connectorPath(a.labelPos)});p.attr("visibility",M)}else p&&(a.connector=
p.destroy())}))},k.pie.prototype.connectorPath=function(a){var b=a.x,c=a.y;return B(this.options.dataLabels.softConnector,!0)?["M",b+("left"===a[6]?5:-5),c,"C",b,c,2*a[2]-a[4],2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",b+("left"===a[6]?5:-5),c,"L",a[2],a[3],"L",a[4],a[5]]},k.pie.prototype.placeDataLabels=function(){r(this.points,function(a){var b=a.dataLabel;b&&a.visible&&((a=b._pos)?(b.sideOverflow&&(b._attr.width=b.getBBox().width-b.sideOverflow,b.css({width:b._attr.width+"px",textOverflow:"ellipsis"}),
b.shortened=!0),b.attr(b._attr),b[b.moved?"animate":"attr"](a),b.moved=!0):b&&b.attr({y:-9999}))},this)},k.pie.prototype.alignDataLabel=v,k.pie.prototype.verifyDataLabelOverflow=function(a){var b=this.center,c=this.options,f=c.center,g=c.minSize||80,k,q=null!==c.size;q||(null!==f[0]?k=Math.max(b[2]-Math.max(a[1],a[3]),g):(k=Math.max(b[2]-a[1]-a[3],g),b[0]+=(a[3]-a[1])/2),null!==f[1]?k=Math.max(Math.min(k,b[2]-Math.max(a[0],a[2])),g):(k=Math.max(Math.min(k,b[2]-a[0]-a[2]),g),b[1]+=(a[0]-a[2])/2),k<
b[2]?(b[2]=k,b[3]=Math.min(e(c.innerSize||0,k),k),this.translate(b),this.drawDataLabels&&this.drawDataLabels()):q=!0);return q});k.column&&(k.column.prototype.alignDataLabel=function(a,b,e,f,k){var g=this.chart.inverted,q=a.series,l=a.dlBox||a.shapeArgs,n=B(a.below,a.plotY>B(this.translatedThreshold,q.yAxis.len)),m=B(e.inside,!!this.options.stacking);l&&(f=p(l),0>f.y&&(f.height+=f.y,f.y=0),l=f.y+f.height-q.yAxis.len,0<l&&(f.height-=l),g&&(f={x:q.yAxis.len-f.y-f.height,y:q.xAxis.len-f.x-f.width,width:f.height,
height:f.width}),m||(g?(f.x+=n?0:f.width,f.width=0):(f.y+=n?f.height:0,f.height=0)));e.align=B(e.align,!g||m?"center":n?"right":"left");e.verticalAlign=B(e.verticalAlign,g||m?"middle":n?"top":"bottom");c.prototype.alignDataLabel.call(this,a,b,e,f,k);a.isLabelJustified&&a.contrastColor&&a.dataLabel.css({color:a.contrastColor})})})(L);(function(a){var C=a.Chart,E=a.each,F=a.objectEach,r=a.pick;a=a.addEvent;a(C.prototype,"render",function(){var a=[];E(this.labelCollectors||[],function(l){a=a.concat(l())});
E(this.yAxis||[],function(l){l.options.stackLabels&&!l.options.stackLabels.allowOverlap&&F(l.stacks,function(l){F(l,function(l){a.push(l.label)})})});E(this.series||[],function(l){var m=l.options.dataLabels,p=l.dataLabelCollections||["dataLabel"];(m.enabled||l._hasPointLabels)&&!m.allowOverlap&&l.visible&&E(p,function(m){E(l.points,function(l){l[m]&&(l[m].labelrank=r(l.labelrank,l.shapeArgs&&l.shapeArgs.height),a.push(l[m]))})})});this.hideOverlappingLabels(a)});C.prototype.hideOverlappingLabels=
function(a){var l=a.length,m,p,v,r,e,c,k,b,g,n=function(a,b,c,e,g,k,l,n){return!(g>a+c||g+l<a||k>b+e||k+n<b)};for(p=0;p<l;p++)if(m=a[p])m.oldOpacity=m.opacity,m.newOpacity=1,m.width||(v=m.getBBox(),m.width=v.width,m.height=v.height);a.sort(function(a,b){return(b.labelrank||0)-(a.labelrank||0)});for(p=0;p<l;p++)for(v=a[p],m=p+1;m<l;++m)if(r=a[m],v&&r&&v!==r&&v.placed&&r.placed&&0!==v.newOpacity&&0!==r.newOpacity&&(e=v.alignAttr,c=r.alignAttr,k=v.parentGroup,b=r.parentGroup,g=2*(v.box?0:v.padding||
0),e=n(e.x+k.translateX,e.y+k.translateY,v.width-g,v.height-g,c.x+b.translateX,c.y+b.translateY,r.width-g,r.height-g)))(v.labelrank<r.labelrank?v:r).newOpacity=0;E(a,function(a){var b,c;a&&(c=a.newOpacity,a.oldOpacity!==c&&a.placed&&(c?a.show(!0):b=function(){a.hide()},a.alignAttr.opacity=c,a[a.isOld?"animate":"attr"](a.alignAttr,null,b)),a.isOld=!0)})}})(L);(function(a){var C=a.addEvent,E=a.Chart,F=a.createElement,r=a.css,m=a.defaultOptions,l=a.defaultPlotOptions,w=a.each,p=a.extend,v=a.fireEvent,
B=a.hasTouch,e=a.inArray,c=a.isObject,k=a.Legend,b=a.merge,g=a.pick,n=a.Point,t=a.Series,f=a.seriesTypes,u=a.svg,D;D=a.TrackerMixin={drawTrackerPoint:function(){var a=this,b=a.chart.pointer,c=function(a){var c=b.getPointFromEvent(a);void 0!==c&&(b.isDirectTouch=!0,c.onMouseOver(a))};w(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(w(a.trackerGroups,function(f){if(a[f]){a[f].addClass("highcharts-tracker").on("mouseover",
c).on("mouseout",function(a){b.onTrackerMouseOut(a)});if(B)a[f].on("touchstart",c);a.options.cursor&&a[f].css(r).css({cursor:a.options.cursor})}}),a._hasTracking=!0);v(this,"afterDrawTracker")},drawTrackerGraph:function(){var a=this,b=a.options,c=b.trackByArea,f=[].concat(c?a.areaPath:a.graphPath),e=f.length,g=a.chart,d=g.pointer,k=g.renderer,l=g.options.tooltip.snap,h=a.tracker,n,m=function(){if(g.hoverSeries!==a)a.onMouseOver()},t="rgba(192,192,192,"+(u?.0001:.002)+")";if(e&&!c)for(n=e+1;n--;)"M"===
f[n]&&f.splice(n+1,0,f[n+1]-l,f[n+2],"L"),(n&&"M"===f[n]||n===e)&&f.splice(n,0,"L",f[n-2]+l,f[n-1]);h?h.attr({d:f}):a.graph&&(a.tracker=k.path(f).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:t,fill:c?t:"none","stroke-width":a.graph.strokeWidth()+(c?0:2*l),zIndex:2}).add(a.group),w([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",m).on("mouseout",function(a){d.onTrackerMouseOut(a)});b.cursor&&a.css({cursor:b.cursor});if(B)a.on("touchstart",
m)}));v(this,"afterDrawTracker")}};f.column&&(f.column.prototype.drawTracker=D.drawTrackerPoint);f.pie&&(f.pie.prototype.drawTracker=D.drawTrackerPoint);f.scatter&&(f.scatter.prototype.drawTracker=D.drawTrackerPoint);p(k.prototype,{setItemEvents:function(a,c,f){var e=this,g=e.chart.renderer.boxWrapper,k="highcharts-legend-"+(a instanceof n?"point":"series")+"-active";(f?c:a.legendGroup).on("mouseover",function(){a.setState("hover");g.addClass(k);c.css(e.options.itemHoverStyle)}).on("mouseout",function(){c.css(b(a.visible?
e.itemStyle:e.itemHiddenStyle));g.removeClass(k);a.setState()}).on("click",function(b){var d=function(){a.setVisible&&a.setVisible()};g.removeClass(k);b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,d):v(a,"legendItemClick",b,d)})},createCheckboxForItem:function(a){a.checkbox=F("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);C(a.checkbox,"click",function(b){v(a.series||a,"checkboxClick",{checked:b.target.checked,
item:a},function(){a.select()})})}});m.legend.itemStyle.cursor="pointer";p(E.prototype,{showResetZoom:function(){function a(){b.zoomOut()}var b=this,c=m.lang,f=b.options.chart.resetZoomButton,e=f.theme,g=e.states,d="chart"===f.relativeTo?null:"plotBox";v(this,"beforeShowResetZoom",null,function(){b.resetZoomButton=b.renderer.button(c.resetZoom,null,null,a,e,g&&g.hover).attr({align:f.position.align,title:c.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(f.position,!1,d)})},zoomOut:function(){var a=
this;v(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var b,f=this.pointer,e=!1,k;!a||a.resetSelection?(w(this.axes,function(a){b=a.zoom()}),f.initiated=!1):w(a.xAxis.concat(a.yAxis),function(a){var d=a.axis;f[d.isXAxis?"zoomX":"zoomY"]&&(b=d.zoom(a.min,a.max),d.displayBtn&&(e=!0))});k=this.resetZoomButton;e&&!k?this.showResetZoom():!e&&c(k)&&(this.resetZoomButton=k.destroy());b&&this.redraw(g(this.options.chart.animation,a&&a.animation,100>this.pointCount))},pan:function(a,
b){var c=this,f=c.hoverPoints,e;f&&w(f,function(a){a.setState()});w("xy"===b?[1,0]:[1],function(b){b=c[b?"xAxis":"yAxis"][0];var d=b.horiz,f=a[d?"chartX":"chartY"],d=d?"mouseDownX":"mouseDownY",g=c[d],h=(b.pointRange||0)/2,k=b.getExtremes(),q=b.toValue(g-f,!0)+h,l=b.toValue(g+b.len-f,!0)-h,n=l<q,g=n?l:q,q=n?q:l,l=Math.min(k.dataMin,h?k.min:b.toValue(b.toPixels(k.min)-b.minPixelPadding)),h=Math.max(k.dataMax,h?k.max:b.toValue(b.toPixels(k.max)+b.minPixelPadding)),n=l-g;0<n&&(q+=n,g=l);n=q-h;0<n&&(q=
h,g-=n);b.series.length&&g!==k.min&&q!==k.max&&(b.setExtremes(g,q,!1,!1,{trigger:"pan"}),e=!0);c[d]=f});e&&c.redraw(!1);r(c.container,{cursor:"move"})}});p(n.prototype,{select:function(a,b){var c=this,f=c.series,k=f.chart;a=g(a,!c.selected);c.firePointEvent(a?"select":"unselect",{accumulate:b},function(){c.selected=c.options.selected=a;f.options.data[e(c,f.data)]=c.options;c.setState(a&&"select");b||w(k.getSelectedPoints(),function(a){a.selected&&a!==c&&(a.selected=a.options.selected=!1,f.options.data[e(a,
f.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a){var b=this.series.chart,c=b.pointer;a=a?c.normalize(a):c.getChartCoordinatesFromPoint(this,b.inverted);c.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");w(a.hoverPoints||[],function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var c=this,f=b(c.series.options.point,c.options).events;c.events=f;a.objectEach(f,
function(a,b){C(c,b,a)});this.hasImportedEvents=!0}},setState:function(a,b){var c=Math.floor(this.plotX),f=this.plotY,e=this.series,k=e.options.states[a||"normal"]||{},d=l[e.type].marker&&e.options.marker,q=d&&!1===d.enabled,n=d&&d.states&&d.states[a||"normal"]||{},h=!1===n.enabled,m=e.stateMarkerGraphic,A=this.marker||{},t=e.chart,u=e.halo,D,r=d&&e.markerAttribs;a=a||"";if(!(a===this.state&&!b||this.selected&&"select"!==a||!1===k.enabled||a&&(h||q&&!1===n.enabled)||a&&A.states&&A.states[a]&&!1===
A.states[a].enabled)){r&&(D=e.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.animate(e.pointAttribs(this,a),g(t.options.chart.animation,k.animation)),D&&this.graphic.animate(D,g(t.options.chart.animation,n.animation,d.animation)),m&&m.hide();else{if(a&&n){d=A.symbol||e.symbol;m&&m.currentSymbol!==d&&(m=m.destroy());if(m)m[b?"animate":"attr"]({x:D.x,y:D.y});else d&&(e.stateMarkerGraphic=
m=t.renderer.symbol(d,D.x,D.y,D.width,D.height).add(e.markerGroup),m.currentSymbol=d);m&&m.attr(e.pointAttribs(this,a))}m&&(m[a&&t.isInsidePlot(c,f,t.inverted)?"show":"hide"](),m.element.point=this)}(c=k.halo)&&c.size?(u||(e.halo=u=t.renderer.path().add((this.graphic||m).parentGroup)),u.show()[b?"animate":"attr"]({d:this.haloPath(c.size)}),u.attr({"class":"highcharts-halo highcharts-color-"+g(this.colorIndex,e.colorIndex)}),u.point=this,u.attr(p({fill:this.color||e.color,"fill-opacity":c.opacity,
zIndex:-1},c.attributes))):u&&u.point&&u.point.haloPath&&u.animate({d:u.point.haloPath(0)},null,u.hide);this.state=a;v(this,"afterSetState")}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});p(t.prototype,{onMouseOver:function(){var a=this.chart,b=a.hoverSeries;if(b&&b!==this)b.onMouseOut();this.options.events.mouseOver&&v(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,b=
this.chart,c=b.tooltip,f=b.hoverPoint;b.hoverSeries=null;if(f)f.onMouseOut();this&&a.events.mouseOut&&v(this,"mouseOut");!c||this.stickyTracking||c.shared&&!this.noSharedTooltip||c.hide();this.setState()},setState:function(a){var b=this,c=b.options,f=b.graph,e=c.states,k=c.lineWidth,c=0;a=a||"";if(b.state!==a&&(w([b.group,b.markerGroup,b.dataLabelsGroup],function(d){d&&(b.state&&d.removeClass("highcharts-series-"+b.state),a&&d.addClass("highcharts-series-"+a))}),b.state=a,!e[a]||!1!==e[a].enabled)&&
(a&&(k=e[a].lineWidth||k+(e[a].lineWidthPlus||0)),f&&!f.dashstyle))for(k={"stroke-width":k},f.animate(k,g(e[a||"normal"]&&e[a||"normal"].animation,b.chart.options.chart.animation));b["zone-graph-"+c];)b["zone-graph-"+c].attr(k),c+=1},setVisible:function(a,b){var c=this,f=c.chart,e=c.legendItem,g,d=f.options.chart.ignoreHiddenSeries,k=c.visible;g=(c.visible=a=c.options.visible=c.userOptions.visible=void 0===a?!k:a)?"show":"hide";w(["group","dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(c[a])c[a][g]()});
if(f.hoverSeries===c||(f.hoverPoint&&f.hoverPoint.series)===c)c.onMouseOut();e&&f.legend.colorizeItem(c,a);c.isDirty=!0;c.options.stacking&&w(f.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});w(c.linkedSeries,function(b){b.setVisible(a,!1)});d&&(f.isDirtyBox=!0);!1!==b&&f.redraw();v(c,g)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);v(this,a?"select":
"unselect")},drawTracker:D.drawTrackerGraph})})(L);(function(a){var C=a.Chart,E=a.each,F=a.inArray,r=a.isArray,m=a.isObject,l=a.pick,w=a.splat;C.prototype.setResponsive=function(l){var m=this.options.responsive,p=[],e=this.currentResponsive;m&&m.rules&&E(m.rules,function(c){void 0===c._id&&(c._id=a.uniqueKey());this.matchResponsiveRule(c,p,l)},this);var c=a.merge.apply(0,a.map(p,function(c){return a.find(m.rules,function(a){return a._id===c}).chartOptions})),p=p.toString()||void 0;p!==(e&&e.ruleIds)&&
(e&&this.update(e.undoOptions,l),p?(this.currentResponsive={ruleIds:p,mergedOptions:c,undoOptions:this.currentOptions(c)},this.update(c,l)):this.currentResponsive=void 0)};C.prototype.matchResponsiveRule=function(a,m){var p=a.condition;(p.callback||function(){return this.chartWidth<=l(p.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=l(p.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=l(p.minWidth,0)&&this.chartHeight>=l(p.minHeight,0)}).call(this)&&m.push(a._id)};C.prototype.currentOptions=function(l){function p(e,
c,k,b){var g;a.objectEach(e,function(a,e){if(!b&&-1<F(e,["series","xAxis","yAxis"]))for(a=w(a),k[e]=[],g=0;g<a.length;g++)c[e][g]&&(k[e][g]={},p(a[g],c[e][g],k[e][g],b+1));else m(a)?(k[e]=r(a)?[]:{},p(a,c[e]||{},k[e],b+1)):k[e]=c[e]||null})}var B={};p(l,this.options,B,0);return B}})(L);(function(a){var C=a.addEvent,E=a.Axis,F=a.Chart,r=a.css,m=a.defined,l=a.each,w=a.extend,p=a.noop,v=a.pick,B=a.timeUnits,e=a.wrap;e(a.Series.prototype,"init",function(a){var c;a.apply(this,Array.prototype.slice.call(arguments,
1));(c=this.xAxis)&&c.options.ordinal&&C(this,"updatedData",function(){delete c.ordinalIndex})});e(E.prototype,"getTimeTicks",function(a,e,b,g,l,t,f,u){var c=0,k,n,y={},p,r,v,d=[],z=-Number.MAX_VALUE,G=this.options.tickPixelInterval,h=this.chart.time;if(!this.options.ordinal&&!this.options.breaks||!t||3>t.length||void 0===b)return a.call(this,e,b,g,l);r=t.length;for(k=0;k<r;k++){v=k&&t[k-1]>g;t[k]<b&&(c=k);if(k===r-1||t[k+1]-t[k]>5*f||v){if(t[k]>z){for(n=a.call(this,e,t[c],t[k],l);n.length&&n[0]<=
z;)n.shift();n.length&&(z=n[n.length-1]);d=d.concat(n)}c=k+1}if(v)break}a=n.info;if(u&&a.unitRange<=B.hour){k=d.length-1;for(c=1;c<k;c++)h.dateFormat("%d",d[c])!==h.dateFormat("%d",d[c-1])&&(y[d[c]]="day",p=!0);p&&(y[d[0]]="day");a.higherRanks=y}d.info=a;if(u&&m(G)){u=h=d.length;k=[];var x;for(p=[];u--;)c=this.translate(d[u]),x&&(p[u]=x-c),k[u]=x=c;p.sort();p=p[Math.floor(p.length/2)];p<.6*G&&(p=null);u=d[h-1]>g?h-1:h;for(x=void 0;u--;)c=k[u],g=Math.abs(x-c),x&&g<.8*G&&(null===p||g<.8*p)?(y[d[u]]&&
!y[d[u+1]]?(g=u+1,x=c):g=u,d.splice(g,1)):x=c}return d});w(E.prototype,{beforeSetTickPositions:function(){var a,e=[],b=!1,g,n=this.getExtremes(),t=n.min,f=n.max,u,p=this.isXAxis&&!!this.options.breaks,n=this.options.ordinal,q=Number.MAX_VALUE,A=this.chart.options.chart.ignoreHiddenSeries;g="highcharts-navigator-xaxis"===this.options.className;!this.options.overscroll||this.max!==this.dataMax||this.chart.mouseIsDown&&!g||this.eventArgs&&(!this.eventArgs||"navigator"===this.eventArgs.trigger)||(this.max+=
this.options.overscroll,!g&&m(this.userMin)&&(this.min+=this.options.overscroll));if(n||p){l(this.series,function(b,c){if(!(A&&!1===b.visible||!1===b.takeOrdinalPosition&&!p)&&(e=e.concat(b.processedXData),a=e.length,e.sort(function(a,b){return a-b}),q=Math.min(q,v(b.closestPointRange,q)),a))for(c=a-1;c--;)e[c]===e[c+1]&&e.splice(c,1)});a=e.length;if(2<a){g=e[1]-e[0];for(u=a-1;u--&&!b;)e[u+1]-e[u]!==g&&(b=!0);!this.options.keepOrdinalPadding&&(e[0]-t>g||f-e[e.length-1]>g)&&(b=!0)}else this.options.overscroll&&
(2===a?q=e[1]-e[0]:1===a?(q=this.options.overscroll,e=[e[0],e[0]+q]):q=this.overscrollPointsRange);b?(this.options.overscroll&&(this.overscrollPointsRange=q,e=e.concat(this.getOverscrollPositions())),this.ordinalPositions=e,g=this.ordinal2lin(Math.max(t,e[0]),!0),u=Math.max(this.ordinal2lin(Math.min(f,e[e.length-1]),!0),1),this.ordinalSlope=f=(f-t)/(u-g),this.ordinalOffset=t-g*f):(this.overscrollPointsRange=v(this.closestPointRange,this.overscrollPointsRange),this.ordinalPositions=this.ordinalSlope=
this.ordinalOffset=void 0)}this.isOrdinal=n&&b;this.groupIntervalFactor=null},val2lin:function(a,e){var b=this.ordinalPositions;if(b){var c=b.length,k,l;for(k=c;k--;)if(b[k]===a){l=k;break}for(k=c-1;k--;)if(a>b[k]||0===k){a=(a-b[k])/(b[k+1]-b[k]);l=k+a;break}e=e?l:this.ordinalSlope*(l||0)+this.ordinalOffset}else e=a;return e},lin2val:function(a,e){var b=this.ordinalPositions;if(b){var c=this.ordinalSlope,k=this.ordinalOffset,l=b.length-1,f;if(e)0>a?a=b[0]:a>l?a=b[l]:(l=Math.floor(a),f=a-l);else for(;l--;)if(e=
c*l+k,a>=e){c=c*(l+1)+k;f=(a-e)/(c-e);break}return void 0!==f&&void 0!==b[l]?b[l]+(f?f*(b[l+1]-b[l]):0):a}return a},getExtendedPositions:function(){var a=this,e=a.chart,b=a.series[0].currentDataGrouping,g=a.ordinalIndex,n=b?b.count+b.unitName:"raw",m=a.options.overscroll,f=a.getExtremes(),u,r;g||(g=a.ordinalIndex={});g[n]||(u={series:[],chart:e,getExtremes:function(){return{min:f.dataMin,max:f.dataMax+m}},options:{ordinal:!0},val2lin:E.prototype.val2lin,ordinal2lin:E.prototype.ordinal2lin},l(a.series,
function(c){r={xAxis:u,xData:c.xData.slice(),chart:e,destroyGroupedData:p};r.xData=r.xData.concat(a.getOverscrollPositions());r.options={dataGrouping:b?{enabled:!0,forced:!0,approximation:"open",units:[[b.unitName,[b.count]]]}:{enabled:!1}};c.processData.apply(r);u.series.push(r)}),a.beforeSetTickPositions.apply(u),g[n]=u.ordinalPositions);return g[n]},getOverscrollPositions:function(){var c=this.options.overscroll,e=this.overscrollPointsRange,b=[],g=this.dataMax;if(a.defined(e))for(b.push(g);g<=
this.dataMax+c;)g+=e,b.push(g);return b},getGroupIntervalFactor:function(a,e,b){var c;b=b.processedXData;var k=b.length,l=[];c=this.groupIntervalFactor;if(!c){for(c=0;c<k-1;c++)l[c]=b[c+1]-b[c];l.sort(function(a,b){return a-b});l=l[Math.floor(k/2)];a=Math.max(a,b[0]);e=Math.min(e,b[k-1]);this.groupIntervalFactor=c=k*l/(e-a)}return c},postProcessTickInterval:function(a){var c=this.ordinalSlope;return c?this.options.breaks?this.closestPointRange||a:a/(c/this.closestPointRange):a}});E.prototype.ordinal2lin=
E.prototype.val2lin;e(F.prototype,"pan",function(a,e){var b=this.xAxis[0],c=b.options.overscroll,k=e.chartX,m=!1;if(b.options.ordinal&&b.series.length){var f=this.mouseDownX,u=b.getExtremes(),p=u.dataMax,q=u.min,A=u.max,y=this.hoverPoints,v=b.closestPointRange||b.overscrollPointsRange,f=(f-k)/(b.translationSlope*(b.ordinalSlope||v)),w={ordinalPositions:b.getExtendedPositions()},v=b.lin2val,B=b.val2lin,d;w.ordinalPositions?1<Math.abs(f)&&(y&&l(y,function(a){a.setState()}),0>f?(y=w,d=b.ordinalPositions?
b:w):(y=b.ordinalPositions?b:w,d=w),w=d.ordinalPositions,p>w[w.length-1]&&w.push(p),this.fixedRange=A-q,f=b.toFixedRange(null,null,v.apply(y,[B.apply(y,[q,!0])+f,!0]),v.apply(d,[B.apply(d,[A,!0])+f,!0])),f.min>=Math.min(u.dataMin,q)&&f.max<=Math.max(p,A)+c&&b.setExtremes(f.min,f.max,!0,!1,{trigger:"pan"}),this.mouseDownX=k,r(this.container,{cursor:"move"})):m=!0}else m=!0;m&&(c&&(b.max=b.dataMax+c),a.apply(this,Array.prototype.slice.call(arguments,1)))})})(L);(function(a){function C(){return Array.prototype.slice.call(arguments,
1)}function E(a){a.apply(this);this.drawBreaks(this.xAxis,["x"]);this.drawBreaks(this.yAxis,F(this.pointArrayMap,["y"]))}var F=a.pick,r=a.wrap,m=a.each,l=a.extend,w=a.isArray,p=a.fireEvent,v=a.Axis,B=a.Series;l(v.prototype,{isInBreak:function(a,c){var e=a.repeat||Infinity,b=a.from,g=a.to-a.from;c=c>=b?(c-b)%e:e-(b-c)%e;return a.inclusive?c<=g:c<g&&0!==c},isInAnyBreak:function(a,c){var e=this.options.breaks,b=e&&e.length,g,l,m;if(b){for(;b--;)this.isInBreak(e[b],a)&&(g=!0,l||(l=F(e[b].showPoints,this.isXAxis?
!1:!0)));m=g&&c?g&&!l:g}return m}});r(v.prototype,"setTickPositions",function(a){a.apply(this,Array.prototype.slice.call(arguments,1));if(this.options.breaks){var c=this.tickPositions,e=this.tickPositions.info,b=[],g;for(g=0;g<c.length;g++)this.isInAnyBreak(c[g])||b.push(c[g]);this.tickPositions=b;this.tickPositions.info=e}});r(v.prototype,"init",function(a,c,k){var b=this;k.breaks&&k.breaks.length&&(k.ordinal=!1);a.call(this,c,k);a=this.options.breaks;b.isBroken=w(a)&&!!a.length;b.isBroken&&(b.val2lin=
function(a){var c=a,e,f;for(f=0;f<b.breakArray.length;f++)if(e=b.breakArray[f],e.to<=a)c-=e.len;else if(e.from>=a)break;else if(b.isInBreak(e,a)){c-=a-e.from;break}return c},b.lin2val=function(a){var c,e;for(e=0;e<b.breakArray.length&&!(c=b.breakArray[e],c.from>=a);e++)c.to<a?a+=c.len:b.isInBreak(c,a)&&(a+=c.len);return a},b.setExtremes=function(a,b,c,f,e){for(;this.isInAnyBreak(a);)a-=this.closestPointRange;for(;this.isInAnyBreak(b);)b-=this.closestPointRange;v.prototype.setExtremes.call(this,a,
b,c,f,e)},b.setAxisTranslation=function(a){v.prototype.setAxisTranslation.call(this,a);a=b.options.breaks;var c=[],e=[],f=0,g,k,l=b.userMin||b.min,A=b.userMax||b.max,y=F(b.pointRangePadding,0),r,w;m(a,function(a){k=a.repeat||Infinity;b.isInBreak(a,l)&&(l+=a.to%k-l%k);b.isInBreak(a,A)&&(A-=A%k-a.from%k)});m(a,function(a){r=a.from;for(k=a.repeat||Infinity;r-k>l;)r-=k;for(;r<l;)r+=k;for(w=r;w<A;w+=k)c.push({value:w,move:"in"}),c.push({value:w+(a.to-a.from),move:"out",size:a.breakSize})});c.sort(function(a,
b){return a.value===b.value?("in"===a.move?0:1)-("in"===b.move?0:1):a.value-b.value});g=0;r=l;m(c,function(a){g+="in"===a.move?1:-1;1===g&&"in"===a.move&&(r=a.value);0===g&&(e.push({from:r,to:a.value,len:a.value-r-(a.size||0)}),f+=a.value-r-(a.size||0))});b.breakArray=e;b.unitLength=A-l-f+y;p(b,"afterBreaks");b.options.staticScale?b.transA=b.options.staticScale:b.unitLength&&(b.transA*=(A-b.min+y)/b.unitLength);y&&(b.minPixelPadding=b.transA*b.minPointOffset);b.min=l;b.max=A})});r(B.prototype,"generatePoints",
function(a){a.apply(this,C(arguments));var c=this.xAxis,e=this.yAxis,b=this.points,g,l=b.length,m=this.options.connectNulls,f;if(c&&e&&(c.options.breaks||e.options.breaks))for(;l--;)g=b[l],f=null===g.y&&!1===m,f||!c.isInAnyBreak(g.x,!0)&&!e.isInAnyBreak(g.y,!0)||(b.splice(l,1),this.data[l]&&this.data[l].destroyElements())});a.Series.prototype.drawBreaks=function(a,c){var e=this,b=e.points,g,l,t,f;a&&m(c,function(c){g=a.breakArray||[];l=a.isXAxis?a.min:F(e.options.threshold,a.min);m(b,function(b){f=
F(b["stack"+c.toUpperCase()],b[c]);m(g,function(c){t=!1;if(l<c.from&&f>c.to||l>c.from&&f<c.from)t="pointBreak";else if(l<c.from&&f>c.from&&f<c.to||l>c.from&&f>c.to&&f<c.from)t="pointInBreak";t&&p(a,t,{point:b,brk:c})})})})};a.Series.prototype.gappedPath=function(){var e=this.currentDataGrouping,c=e&&e.totalRange,e=this.options.gapSize,k=this.points.slice(),b=k.length-1,g=this.yAxis;if(e&&0<b)for("value"!==this.options.gapUnit&&(e*=this.closestPointRange),c&&c>e&&(e=c);b--;)k[b+1].x-k[b].x>e&&(c=(k[b].x+
k[b+1].x)/2,k.splice(b+1,0,{isNull:!0,x:c}),this.options.stacking&&(c=g.stacks[this.stackKey][c]=new a.StackItem(g,g.options.stackLabels,!1,c,this.stack),c.total=0));return this.getGraphPath(k)};r(a.seriesTypes.column.prototype,"drawPoints",E);r(a.Series.prototype,"drawPoints",E)})(L);(function(a){var C=a.arrayMax,E=a.arrayMin,F=a.Axis,r=a.defaultPlotOptions,m=a.defined,l=a.each,w=a.extend,p=a.format,v=a.isNumber,B=a.merge,e=a.pick,c=a.Point,k=a.Tooltip,b=a.wrap,g=a.Series.prototype,n=g.processData,
t=g.generatePoints,f={approximation:"average",groupPixelWidth:2,dateTimeLabelFormats:{millisecond:["%A, %b %e, %H:%M:%S.%L","%A, %b %e, %H:%M:%S.%L","-%H:%M:%S.%L"],second:["%A, %b %e, %H:%M:%S","%A, %b %e, %H:%M:%S","-%H:%M:%S"],minute:["%A, %b %e, %H:%M","%A, %b %e, %H:%M","-%H:%M"],hour:["%A, %b %e, %H:%M","%A, %b %e, %H:%M","-%H:%M"],day:["%A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],week:["Week from %A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],month:["%B %Y","%B","-%B %Y"],year:["%Y","%Y","-%Y"]}},
u={line:{},spline:{},area:{},areaspline:{},column:{approximation:"sum",groupPixelWidth:10},arearange:{approximation:"range"},areasplinerange:{approximation:"range"},columnrange:{approximation:"range",groupPixelWidth:10},candlestick:{approximation:"ohlc",groupPixelWidth:10},ohlc:{approximation:"ohlc",groupPixelWidth:5}},D=a.defaultDataGroupingUnits=[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",[1]],["week",
[1]],["month",[1,3,6]],["year",null]],q=a.approximations={sum:function(a){var b=a.length,c;if(!b&&a.hasNulls)c=null;else if(b)for(c=0;b--;)c+=a[b];return c},average:function(a){var b=a.length;a=q.sum(a);v(a)&&b&&(a/=b);return a},averages:function(){var a=[];l(arguments,function(b){a.push(q.average(b))});return void 0===a[0]?void 0:a},open:function(a){return a.length?a[0]:a.hasNulls?null:void 0},high:function(a){return a.length?C(a):a.hasNulls?null:void 0},low:function(a){return a.length?E(a):a.hasNulls?
null:void 0},close:function(a){return a.length?a[a.length-1]:a.hasNulls?null:void 0},ohlc:function(a,b,c,f){a=q.open(a);b=q.high(b);c=q.low(c);f=q.close(f);if(v(a)||v(b)||v(c)||v(f))return[a,b,c,f]},range:function(a,b){a=q.low(a);b=q.high(b);if(v(a)||v(b))return[a,b];if(null===a&&null===b)return null}};g.groupData=function(a,b,c,e){var g=this.data,d=this.options.data,k=[],m=[],h=[],n=a.length,A,y,p=!!b,t=[];e="function"===typeof e?e:q[e]||u[this.type]&&q[u[this.type].approximation]||q[f.approximation];
var r=this.pointArrayMap,D=r&&r.length,w=0;y=0;var B,H;D?l(r,function(){t.push([])}):t.push([]);B=D||1;for(H=0;H<=n&&!(a[H]>=c[0]);H++);for(H;H<=n;H++){for(;void 0!==c[w+1]&&a[H]>=c[w+1]||H===n;){A=c[w];this.dataGroupInfo={start:y,length:t[0].length};y=e.apply(this,t);void 0!==y&&(k.push(A),m.push(y),h.push(this.dataGroupInfo));y=H;for(A=0;A<B;A++)t[A].length=0,t[A].hasNulls=!1;w+=1;if(H===n)break}if(H===n)break;if(r){A=this.cropStart+H;var I=g&&g[A]||this.pointClass.prototype.applyOptions.apply({series:this},
[d[A]]),C;for(A=0;A<D;A++)C=I[r[A]],v(C)?t[A].push(C):null===C&&(t[A].hasNulls=!0)}else A=p?b[H]:null,v(A)?t[0].push(A):null===A&&(t[0].hasNulls=!0)}return[k,m,h]};g.processData=function(){var a=this.chart,b=this.options.dataGrouping,c=!1!==this.allowDG&&b&&e(b.enabled,a.options.isStock),f=this.visible||!a.options.chart.ignoreHiddenSeries,k,d=this.currentDataGrouping,l;this.forceCrop=c;this.groupPixelWidth=null;this.hasProcessed=!0;if(!1!==n.apply(this,arguments)&&c){this.destroyGroupedData();var q=
this.processedXData,h=this.processedYData,u=a.plotSizeX,a=this.xAxis,p=a.options.ordinal,t=this.groupPixelWidth=a.getGroupPixelWidth&&a.getGroupPixelWidth();if(t){this.isDirty=k=!0;this.points=null;c=a.getExtremes();l=c.min;c=c.max;p=p&&a.getGroupIntervalFactor(l,c,this)||1;t=t*(c-l)/u*p;u=a.getTimeTicks(a.normalizeTimeTickInterval(t,b.units||D),Math.min(l,q[0]),Math.max(c,q[q.length-1]),a.options.startOfWeek,q,this.closestPointRange);q=g.groupData.apply(this,[q,h,u,b.approximation]);h=q[0];p=q[1];
if(b.smoothed&&h.length){b=h.length-1;for(h[b]=Math.min(h[b],c);b--&&0<b;)h[b]+=t/2;h[0]=Math.max(h[0],l)}l=u.info;this.closestPointRange=u.info.totalRange;this.groupMap=q[2];m(h[0])&&h[0]<a.dataMin&&f&&(a.min===a.dataMin&&(a.min=h[0]),a.dataMin=h[0]);this.processedXData=h;this.processedYData=p}else this.groupMap=null;this.hasGroupedData=k;this.currentDataGrouping=l;this.preventGraphAnimation=(d&&d.totalRange)!==(l&&l.totalRange)}};g.destroyGroupedData=function(){var a=this.groupedData;l(a||[],function(b,
c){b&&(a[c]=b.destroy?b.destroy():null)});this.groupedData=null};g.generatePoints=function(){t.apply(this);this.destroyGroupedData();this.groupedData=this.hasGroupedData?this.points:null};a.addEvent(c.prototype,"update",function(){if(this.dataGroup)return a.error(24),!1});b(k.prototype,"tooltipFooterHeaderFormatter",function(a,b,c){var f=this.chart.time,e=b.series,d=e.tooltipOptions,g=e.options.dataGrouping,k=d.xDateFormat,h,l=e.xAxis;return l&&"datetime"===l.options.type&&g&&v(b.key)?(a=e.currentDataGrouping,
g=g.dateTimeLabelFormats,a?(l=g[a.unitName],1===a.count?k=l[0]:(k=l[1],h=l[2])):!k&&g&&(k=this.getXDateFormat(b,d,l)),k=f.dateFormat(k,b.key),h&&(k+=f.dateFormat(h,b.key+a.totalRange-1)),p(d[(c?"footer":"header")+"Format"],{point:w(b.point,{key:k}),series:e},f)):a.call(this,b,c)});a.addEvent(g,"destroy",g.destroyGroupedData);b(g,"setOptions",function(a,b){a=a.call(this,b);var c=this.type,e=this.chart.options.plotOptions,g=r[c].dataGrouping;u[c]&&(g||(g=B(f,u[c])),a.dataGrouping=B(g,e.series&&e.series.dataGrouping,
e[c].dataGrouping,b.dataGrouping));this.chart.options.isStock&&(this.requireSorting=!0);return a});a.addEvent(F.prototype,"afterSetScale",function(){l(this.series,function(a){a.hasProcessed=!1})});F.prototype.getGroupPixelWidth=function(){var a=this.series,b=a.length,c,f=0,e=!1,d;for(c=b;c--;)(d=a[c].options.dataGrouping)&&(f=Math.max(f,d.groupPixelWidth));for(c=b;c--;)(d=a[c].options.dataGrouping)&&a[c].hasProcessed&&(b=(a[c].processedXData||a[c].data).length,a[c].groupPixelWidth||b>this.chart.plotSizeX/
f||b&&d.forced)&&(e=!0);return e?f:0};F.prototype.setDataGrouping=function(a,b){var c;b=e(b,!0);a||(a={forced:!1,units:null});if(this instanceof F)for(c=this.series.length;c--;)this.series[c].update({dataGrouping:a},!1);else l(this.chart.options.series,function(b){b.dataGrouping=a},!1);b&&this.chart.redraw()}})(L);(function(a){var C=a.each,E=a.Point,F=a.seriesType,r=a.seriesTypes;F("ohlc","column",{lineWidth:1,tooltip:{pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cb\x3e {series.name}\x3c/b\x3e\x3cbr/\x3eOpen: {point.open}\x3cbr/\x3eHigh: {point.high}\x3cbr/\x3eLow: {point.low}\x3cbr/\x3eClose: {point.close}\x3cbr/\x3e'},
threshold:null,states:{hover:{lineWidth:3}},stickyTracking:!0},{directTouch:!1,pointArrayMap:["open","high","low","close"],toYData:function(a){return[a.open,a.high,a.low,a.close]},pointValKey:"close",pointAttrToOptions:{stroke:"color","stroke-width":"lineWidth"},pointAttribs:function(a,l){l=r.column.prototype.pointAttribs.call(this,a,l);var m=this.options;delete l.fill;!a.options.color&&m.upColor&&a.open<a.close&&(l.stroke=m.upColor);return l},translate:function(){var a=this,l=a.yAxis,w=!!a.modifyValue,
p=["plotOpen","plotHigh","plotLow","plotClose","yBottom"];r.column.prototype.translate.apply(a);C(a.points,function(m){C([m.open,m.high,m.low,m.close,m.low],function(r,e){null!==r&&(w&&(r=a.modifyValue(r)),m[p[e]]=l.toPixels(r,!0))});m.tooltipPos[1]=m.plotHigh+l.pos-a.chart.plotTop})},drawPoints:function(){var a=this,l=a.chart;C(a.points,function(m){var p,r,w,e,c=m.graphic,k,b=!c;void 0!==m.plotY&&(c||(m.graphic=c=l.renderer.path().add(a.group)),c.attr(a.pointAttribs(m,m.selected&&"select")),r=c.strokeWidth()%
2/2,k=Math.round(m.plotX)-r,w=Math.round(m.shapeArgs.width/2),e=["M",k,Math.round(m.yBottom),"L",k,Math.round(m.plotHigh)],null!==m.open&&(p=Math.round(m.plotOpen)+r,e.push("M",k,p,"L",k-w,p)),null!==m.close&&(p=Math.round(m.plotClose)+r,e.push("M",k,p,"L",k+w,p)),c[b?"attr":"animate"]({d:e}).addClass(m.getClassName(),!0))})},animate:null},{getClassName:function(){return E.prototype.getClassName.call(this)+(this.open<this.close?" highcharts-point-up":" highcharts-point-down")}})})(L);(function(a){var C=
a.defaultPlotOptions,E=a.each,F=a.merge,r=a.seriesType,m=a.seriesTypes;r("candlestick","ohlc",F(C.column,{states:{hover:{lineWidth:2}},tooltip:C.ohlc.tooltip,threshold:null,lineColor:"#000000",lineWidth:1,upColor:"#ffffff",stickyTracking:!0}),{pointAttribs:function(a,r){var l=m.column.prototype.pointAttribs.call(this,a,r),v=this.options,w=a.open<a.close,e=v.lineColor||this.color;l["stroke-width"]=v.lineWidth;l.fill=a.options.color||(w?v.upColor||this.color:this.color);l.stroke=a.lineColor||(w?v.upLineColor||
e:e);r&&(a=v.states[r],l.fill=a.color||l.fill,l.stroke=a.lineColor||l.stroke,l["stroke-width"]=a.lineWidth||l["stroke-width"]);return l},drawPoints:function(){var a=this,m=a.chart;E(a.points,function(l){var p=l.graphic,r,e,c,k,b,g,n,t=!p;void 0!==l.plotY&&(p||(l.graphic=p=m.renderer.path().add(a.group)),p.attr(a.pointAttribs(l,l.selected&&"select")).shadow(a.options.shadow),b=p.strokeWidth()%2/2,g=Math.round(l.plotX)-b,r=l.plotOpen,e=l.plotClose,c=Math.min(r,e),r=Math.max(r,e),n=Math.round(l.shapeArgs.width/
2),e=Math.round(c)!==Math.round(l.plotHigh),k=r!==l.yBottom,c=Math.round(c)+b,r=Math.round(r)+b,b=[],b.push("M",g-n,r,"L",g-n,c,"L",g+n,c,"L",g+n,r,"Z","M",g,c,"L",g,e?Math.round(l.plotHigh):c,"M",g,r,"L",g,k?Math.round(l.yBottom):r),p[t?"attr":"animate"]({d:b}).addClass(l.getClassName(),!0))})}})})(L);da=function(a){var C=a.each,E=a.seriesTypes,F=a.stableSort;return{getPlotBox:function(){return a.Series.prototype.getPlotBox.call(this.options.onSeries&&this.chart.get(this.options.onSeries)||this)},
translate:function(){E.column.prototype.translate.apply(this);var a=this.options,m=this.chart,l=this.points,w=l.length-1,p,v,B=a.onSeries;p=B&&m.get(B);var a=a.onKey||"y",B=p&&p.options.step,e=p&&p.points,c=e&&e.length,k=this.xAxis,b=this.yAxis,g=0,n,t,f,u;if(p&&p.visible&&c)for(g=(p.pointXOffset||0)+(p.barW||0)/2,p=p.currentDataGrouping,t=e[c-1].x+(p?p.totalRange:0),F(l,function(a,b){return a.x-b.x}),a="plot"+a[0].toUpperCase()+a.substr(1);c--&&l[w]&&!(n=e[c],p=l[w],p.y=n.y,n.x<=p.x&&void 0!==n[a]&&
(p.x<=t&&(p.plotY=n[a],n.x<p.x&&!B&&(f=e[c+1])&&void 0!==f[a]&&(u=(p.x-n.x)/(f.x-n.x),p.plotY+=u*(f[a]-n[a]),p.y+=u*(f.y-n.y))),w--,c++,0>w)););C(l,function(a,c){var f;a.plotX+=g;void 0===a.plotY&&(0<=a.plotX&&a.plotX<=k.len?a.plotY=m.chartHeight-k.bottom-(k.opposite?k.height:0)+k.offset-b.top:a.shapeArgs={});(v=l[c-1])&&v.plotX===a.plotX&&(void 0===v.stackIndex&&(v.stackIndex=0),f=v.stackIndex+1);a.stackIndex=f})}}}(L);(function(a,C){function E(a){e[a+"pin"]=function(c,b,g,l,m){var f=m&&m.anchorX;
m=m&&m.anchorY;"circle"===a&&l>g&&(c-=Math.round((l-g)/2),g=l);c=e[a](c,b,g,l);f&&m&&(c.push("M","circle"===a?c[1]-c[4]:c[1]+c[4]/2,b>m?b:b+l,"L",f,m),c=c.concat(e.circle(f-1,m-1,2,2)));return c}}var F=a.addEvent,r=a.each,m=a.merge,l=a.noop,w=a.Renderer,p=a.seriesType,v=a.TrackerMixin,B=a.VMLRenderer,e=a.SVGRenderer.prototype.symbols;p("flags","column",{pointRange:0,allowOverlapX:!1,shape:"flag",stackDistance:12,textAlign:"center",tooltip:{pointFormat:"{point.text}\x3cbr/\x3e"},threshold:null,y:-30,
fillColor:"#ffffff",lineWidth:1,states:{hover:{lineColor:"#000000",fillColor:"#ccd6eb"}},style:{fontSize:"11px",fontWeight:"bold"}},{sorted:!1,noSharedTooltip:!0,allowDG:!1,takeOrdinalPosition:!1,trackerGroups:["markerGroup"],forceCrop:!0,init:a.Series.prototype.init,pointAttribs:function(a,e){var b=this.options,c=a&&a.color||this.color,k=b.lineColor,l=a&&a.lineWidth;a=a&&a.fillColor||b.fillColor;e&&(a=b.states[e].fillColor,k=b.states[e].lineColor,l=b.states[e].lineWidth);return{fill:a||c,stroke:k||
c,"stroke-width":l||b.lineWidth||0}},translate:C.translate,getPlotBox:C.getPlotBox,drawPoints:function(){var c=this.points,e=this.chart,b=e.renderer,g,l,t=this.options,f=t.y,u,p,q,A,y,v,w=this.yAxis,B={},d=[];for(p=c.length;p--;)q=c[p],v=q.plotX>this.xAxis.len,g=q.plotX,A=q.stackIndex,u=q.options.shape||t.shape,l=q.plotY,void 0!==l&&(l=q.plotY+f-(void 0!==A&&A*t.stackDistance)),q.anchorX=A?void 0:q.plotX,y=A?void 0:q.plotY,A=q.graphic,void 0!==l&&0<=g&&!v?(A||(A=q.graphic=b.label("",null,null,u,null,
null,t.useHTML).attr(this.pointAttribs(q)).css(m(t.style,q.style)).attr({align:"flag"===u?"left":"center",width:t.width,height:t.height,"text-align":t.textAlign}).addClass("highcharts-point").add(this.markerGroup),q.graphic.div&&(q.graphic.div.point=q),A.shadow(t.shadow),A.isNew=!0),0<g&&(g-=A.strokeWidth()%2),u={y:l,anchorY:y},t.allowOverlapX&&(u.x=g,u.anchorX=q.anchorX),A.attr({text:q.options.title||t.title||"A"})[A.isNew?"attr":"animate"](u),t.allowOverlapX||(B[q.plotX]?B[q.plotX].size=Math.max(B[q.plotX].size,
A.width):B[q.plotX]={align:0,size:A.width,target:g,anchorX:g}),q.tooltipPos=e.inverted?[w.len+w.pos-e.plotLeft-l,this.xAxis.len-g]:[g,l+w.pos-e.plotTop]):A&&(q.graphic=A.destroy());t.allowOverlapX||(a.objectEach(B,function(a){a.plotX=a.anchorX;d.push(a)}),a.distribute(d,this.xAxis.len),r(c,function(a){var b=a.graphic&&B[a.plotX];b&&(a.graphic[a.graphic.isNew?"attr":"animate"]({x:b.pos,anchorX:a.anchorX}),a.graphic.isNew=!1)}));t.useHTML&&a.wrap(this.markerGroup,"on",function(b){return a.SVGElement.prototype.on.apply(b.apply(this,
[].slice.call(arguments,1)),[].slice.call(arguments,1))})},drawTracker:function(){var a=this.points;v.drawTrackerPoint.apply(this);r(a,function(c){var b=c.graphic;b&&F(b.element,"mouseover",function(){0<c.stackIndex&&!c.raised&&(c._y=b.y,b.attr({y:c._y-8}),c.raised=!0);r(a,function(a){a!==c&&a.raised&&a.graphic&&(a.graphic.attr({y:a._y}),a.raised=!1)})})})},animate:l,buildKDTree:l,setClip:l});e.flag=function(a,k,b,g,l){var c=l&&l.anchorX||a;l=l&&l.anchorY||k;return e.circle(c-1,l-1,2,2).concat(["M",
c,l,"L",a,k+g,a,k,a+b,k,a+b,k+g,a,k+g,"Z"])};E("circle");E("square");w===B&&r(["flag","circlepin","squarepin"],function(a){B.prototype.symbols[a]=e[a]})})(L,da);(function(a){function C(a,b,c){this.init(a,b,c)}var E=a.addEvent,F=a.Axis,r=a.correctFloat,m=a.defaultOptions,l=a.defined,w=a.destroyObjectProperties,p=a.each,v=a.fireEvent,B=a.hasTouch,e=a.isTouchDevice,c=a.merge,k=a.pick,b=a.removeEvent,g=a.wrap,n,t={height:e?20:14,barBorderRadius:0,buttonBorderRadius:0,liveRedraw:a.svg&&!e,margin:10,minWidth:6,
step:.2,zIndex:3,barBackgroundColor:"#cccccc",barBorderWidth:1,barBorderColor:"#cccccc",buttonArrowColor:"#333333",buttonBackgroundColor:"#e6e6e6",buttonBorderColor:"#cccccc",buttonBorderWidth:1,rifleColor:"#333333",trackBackgroundColor:"#f2f2f2",trackBorderColor:"#f2f2f2",trackBorderWidth:1};m.scrollbar=c(!0,t,m.scrollbar);a.swapXY=n=function(a,b){var c=a.length,f;if(b)for(b=0;b<c;b+=3)f=a[b+1],a[b+1]=a[b+2],a[b+2]=f;return a};C.prototype={init:function(a,b,e){this.scrollbarButtons=[];this.renderer=
a;this.userOptions=b;this.options=c(t,b);this.chart=e;this.size=k(this.options.size,this.options.height);b.enabled&&(this.render(),this.initEvents(),this.addEvents())},render:function(){var a=this.renderer,b=this.options,c=this.size,e;this.group=e=a.g("scrollbar").attr({zIndex:b.zIndex,translateY:-99999}).add();this.track=a.rect().addClass("highcharts-scrollbar-track").attr({x:0,r:b.trackBorderRadius||0,height:c,width:c}).add(e);this.track.attr({fill:b.trackBackgroundColor,stroke:b.trackBorderColor,
"stroke-width":b.trackBorderWidth});this.trackBorderWidth=this.track.strokeWidth();this.track.attr({y:-this.trackBorderWidth%2/2});this.scrollbarGroup=a.g().add(e);this.scrollbar=a.rect().addClass("highcharts-scrollbar-thumb").attr({height:c,width:c,r:b.barBorderRadius||0}).add(this.scrollbarGroup);this.scrollbarRifles=a.path(n(["M",-3,c/4,"L",-3,2*c/3,"M",0,c/4,"L",0,2*c/3,"M",3,c/4,"L",3,2*c/3],b.vertical)).addClass("highcharts-scrollbar-rifles").add(this.scrollbarGroup);this.scrollbar.attr({fill:b.barBackgroundColor,
stroke:b.barBorderColor,"stroke-width":b.barBorderWidth});this.scrollbarRifles.attr({stroke:b.rifleColor,"stroke-width":1});this.scrollbarStrokeWidth=this.scrollbar.strokeWidth();this.scrollbarGroup.translate(-this.scrollbarStrokeWidth%2/2,-this.scrollbarStrokeWidth%2/2);this.drawScrollbarButton(0);this.drawScrollbarButton(1)},position:function(a,b,c,e){var f=this.options.vertical,g=0,k=this.rendered?"animate":"attr";this.x=a;this.y=b+this.trackBorderWidth;this.width=c;this.xOffset=this.height=e;
this.yOffset=g;f?(this.width=this.yOffset=c=g=this.size,this.xOffset=b=0,this.barWidth=e-2*c,this.x=a+=this.options.margin):(this.height=this.xOffset=e=b=this.size,this.barWidth=c-2*e,this.y+=this.options.margin);this.group[k]({translateX:a,translateY:this.y});this.track[k]({width:c,height:e});this.scrollbarButtons[1][k]({translateX:f?0:c-b,translateY:f?e-g:0})},drawScrollbarButton:function(a){var b=this.renderer,c=this.scrollbarButtons,f=this.options,e=this.size,g;g=b.g().add(this.group);c.push(g);
g=b.rect().addClass("highcharts-scrollbar-button").add(g);g.attr({stroke:f.buttonBorderColor,"stroke-width":f.buttonBorderWidth,fill:f.buttonBackgroundColor});g.attr(g.crisp({x:-.5,y:-.5,width:e+1,height:e+1,r:f.buttonBorderRadius},g.strokeWidth()));g=b.path(n(["M",e/2+(a?-1:1),e/2-3,"L",e/2+(a?-1:1),e/2+3,"L",e/2+(a?2:-2),e/2],f.vertical)).addClass("highcharts-scrollbar-arrow").add(c[a]);g.attr({fill:f.buttonArrowColor})},setRange:function(a,b){var c=this.options,f=c.vertical,e=c.minWidth,g=this.barWidth,
k,m,n=this.rendered&&!this.hasDragged?"animate":"attr";l(g)&&(a=Math.max(a,0),k=Math.ceil(g*a),this.calculatedWidth=m=r(g*Math.min(b,1)-k),m<e&&(k=(g-e+m)*a,m=e),e=Math.floor(k+this.xOffset+this.yOffset),g=m/2-.5,this.from=a,this.to=b,f?(this.scrollbarGroup[n]({translateY:e}),this.scrollbar[n]({height:m}),this.scrollbarRifles[n]({translateY:g}),this.scrollbarTop=e,this.scrollbarLeft=0):(this.scrollbarGroup[n]({translateX:e}),this.scrollbar[n]({width:m}),this.scrollbarRifles[n]({translateX:g}),this.scrollbarLeft=
e,this.scrollbarTop=0),12>=m?this.scrollbarRifles.hide():this.scrollbarRifles.show(!0),!1===c.showFull&&(0>=a&&1<=b?this.group.hide():this.group.show()),this.rendered=!0)},initEvents:function(){var a=this;a.mouseMoveHandler=function(b){var c=a.chart.pointer.normalize(b),f=a.options.vertical?"chartY":"chartX",e=a.initPositions;!a.grabbedCenter||b.touches&&0===b.touches[0][f]||(c=a.cursorToScrollbarPosition(c)[f],f=a[f],f=c-f,a.hasDragged=!0,a.updatePosition(e[0]+f,e[1]+f),a.hasDragged&&v(a,"changed",
{from:a.from,to:a.to,trigger:"scrollbar",DOMType:b.type,DOMEvent:b}))};a.mouseUpHandler=function(b){a.hasDragged&&v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMType:b.type,DOMEvent:b});a.grabbedCenter=a.hasDragged=a.chartX=a.chartY=null};a.mouseDownHandler=function(b){b=a.chart.pointer.normalize(b);b=a.cursorToScrollbarPosition(b);a.chartX=b.chartX;a.chartY=b.chartY;a.initPositions=[a.from,a.to];a.grabbedCenter=!0};a.buttonToMinClick=function(b){var c=r(a.to-a.from)*a.options.step;a.updatePosition(r(a.from-
c),r(a.to-c));v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:b})};a.buttonToMaxClick=function(b){var c=(a.to-a.from)*a.options.step;a.updatePosition(a.from+c,a.to+c);v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:b})};a.trackClick=function(b){var c=a.chart.pointer.normalize(b),f=a.to-a.from,e=a.y+a.scrollbarTop,g=a.x+a.scrollbarLeft;a.options.vertical&&c.chartY>e||!a.options.vertical&&c.chartX>g?a.updatePosition(a.from+f,a.to+f):a.updatePosition(a.from-f,a.to-f);
v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:b})}},cursorToScrollbarPosition:function(a){var b=this.options,b=b.minWidth>this.calculatedWidth?b.minWidth:0;return{chartX:(a.chartX-this.x-this.xOffset)/(this.barWidth-b),chartY:(a.chartY-this.y-this.yOffset)/(this.barWidth-b)}},updatePosition:function(a,b){1<b&&(a=r(1-r(b-a)),b=1);0>a&&(b=r(b-a),a=0);this.from=a;this.to=b},update:function(a){this.destroy();this.init(this.chart.renderer,c(!0,this.options,a),this.chart)},addEvents:function(){var a=
this.options.inverted?[1,0]:[0,1],b=this.scrollbarButtons,c=this.scrollbarGroup.element,e=this.mouseDownHandler,g=this.mouseMoveHandler,k=this.mouseUpHandler,a=[[b[a[0]].element,"click",this.buttonToMinClick],[b[a[1]].element,"click",this.buttonToMaxClick],[this.track.element,"click",this.trackClick],[c,"mousedown",e],[c.ownerDocument,"mousemove",g],[c.ownerDocument,"mouseup",k]];B&&a.push([c,"touchstart",e],[c.ownerDocument,"touchmove",g],[c.ownerDocument,"touchend",k]);p(a,function(a){E.apply(null,
a)});this._events=a},removeEvents:function(){p(this._events,function(a){b.apply(null,a)});this._events.length=0},destroy:function(){var a=this.chart.scroller;this.removeEvents();p(["track","scrollbarRifles","scrollbar","scrollbarGroup","group"],function(a){this[a]&&this[a].destroy&&(this[a]=this[a].destroy())},this);a&&this===a.scrollbar&&(a.scrollbar=null,w(a.scrollbarButtons))}};g(F.prototype,"init",function(a){var b=this;a.apply(b,Array.prototype.slice.call(arguments,1));b.options.scrollbar&&b.options.scrollbar.enabled&&
(b.options.scrollbar.vertical=!b.horiz,b.options.startOnTick=b.options.endOnTick=!1,b.scrollbar=new C(b.chart.renderer,b.options.scrollbar,b.chart),E(b.scrollbar,"changed",function(a){var c=Math.min(k(b.options.min,b.min),b.min,b.dataMin),f=Math.max(k(b.options.max,b.max),b.max,b.dataMax)-c,e;b.horiz&&!b.reversed||!b.horiz&&b.reversed?(e=c+f*this.to,c+=f*this.from):(e=c+f*(1-this.from),c+=f*(1-this.to));b.setExtremes(c,e,!0,!1,a)}))});g(F.prototype,"render",function(a){var b=Math.min(k(this.options.min,
this.min),this.min,k(this.dataMin,this.min)),c=Math.max(k(this.options.max,this.max),this.max,k(this.dataMax,this.max)),f=this.scrollbar,e=this.titleOffset||0;a.apply(this,Array.prototype.slice.call(arguments,1));if(f){this.horiz?(f.position(this.left,this.top+this.height+2+this.chart.scrollbarsOffsets[1]+(this.opposite?0:e+this.axisTitleMargin+this.offset),this.width,this.height),e=1):(f.position(this.left+this.width+2+this.chart.scrollbarsOffsets[0]+(this.opposite?e+this.axisTitleMargin+this.offset:
0),this.top,this.width,this.height),e=0);if(!this.opposite&&!this.horiz||this.opposite&&this.horiz)this.chart.scrollbarsOffsets[e]+=this.scrollbar.size+this.scrollbar.options.margin;isNaN(b)||isNaN(c)||!l(this.min)||!l(this.max)?f.setRange(0,0):(e=(this.min-b)/(c-b),b=(this.max-b)/(c-b),this.horiz&&!this.reversed||!this.horiz&&this.reversed?f.setRange(e,b):f.setRange(1-b,1-e))}});g(F.prototype,"getOffset",function(a){var b=this.horiz?2:1,c=this.scrollbar;a.apply(this,Array.prototype.slice.call(arguments,
1));c&&(this.chart.scrollbarsOffsets=[0,0],this.chart.axisOffset[b]+=c.size+c.options.margin)});g(F.prototype,"destroy",function(a){this.scrollbar&&(this.scrollbar=this.scrollbar.destroy());a.apply(this,Array.prototype.slice.call(arguments,1))});a.Scrollbar=C})(L);(function(a){function C(a){this.init(a)}var E=a.addEvent,F=a.Axis,r=a.Chart,m=a.color,l=a.defaultOptions,w=a.defined,p=a.destroyObjectProperties,v=a.each,B=a.erase,e=a.error,c=a.extend,k=a.grep,b=a.hasTouch,g=a.isArray,n=a.isNumber,t=a.isObject,
f=a.merge,u=a.pick,D=a.removeEvent,q=a.Scrollbar,A=a.Series,y=a.seriesTypes,H=a.wrap,I=[].concat(a.defaultDataGroupingUnits),J=function(a){var b=k(arguments,n);if(b.length)return Math[a].apply(0,b)};I[4]=["day",[1,2,3,4]];I[5]=["week",[1,2,3]];y=void 0===y.areaspline?"line":"areaspline";c(l,{navigator:{height:40,margin:25,maskInside:!0,handles:{width:7,height:15,symbols:["navigator-handle","navigator-handle"],enabled:!0,lineWidth:1,backgroundColor:"#f2f2f2",borderColor:"#999999"},maskFill:m("#6685c2").setOpacity(.3).get(),
outlineColor:"#cccccc",outlineWidth:1,series:{type:y,fillOpacity:.05,lineWidth:1,compare:null,dataGrouping:{approximation:"average",enabled:!0,groupPixelWidth:2,smoothed:!0,units:I},dataLabels:{enabled:!1,zIndex:2},id:"highcharts-navigator-series",className:"highcharts-navigator-series",lineColor:null,marker:{enabled:!1},pointRange:0,threshold:null},xAxis:{overscroll:0,className:"highcharts-navigator-xaxis",tickLength:0,lineWidth:0,gridLineColor:"#e6e6e6",gridLineWidth:1,tickPixelInterval:200,labels:{align:"left",
style:{color:"#999999"},x:3,y:-4},crosshair:!1},yAxis:{className:"highcharts-navigator-yaxis",gridLineWidth:0,startOnTick:!1,endOnTick:!1,minPadding:.1,maxPadding:.1,labels:{enabled:!1},crosshair:!1,title:{text:null},tickLength:0,tickWidth:0}}});a.Renderer.prototype.symbols["navigator-handle"]=function(a,b,c,f,e){a=e.width/2;b=Math.round(a/3)+.5;e=e.height;return["M",-a-1,.5,"L",a,.5,"L",a,e+.5,"L",-a-1,e+.5,"L",-a-1,.5,"M",-b,4,"L",-b,e-3,"M",b-1,4,"L",b-1,e-3]};C.prototype={drawHandle:function(a,
b,c,f){var d=this.navigatorOptions.handles.height;this.handles[b][f](c?{translateX:Math.round(this.left+this.height/2),translateY:Math.round(this.top+parseInt(a,10)+.5-d)}:{translateX:Math.round(this.left+parseInt(a,10)),translateY:Math.round(this.top+this.height/2-d/2-1)})},drawOutline:function(a,b,c,f){var d=this.navigatorOptions.maskInside,e=this.outline.strokeWidth(),h=e/2,e=e%2/2,g=this.outlineHeight,k=this.scrollbarHeight,l=this.size,m=this.left-k,n=this.top;c?(m-=h,c=n+b+e,b=n+a+e,a=["M",m+
g,n-k-e,"L",m+g,c,"L",m,c,"L",m,b,"L",m+g,b,"L",m+g,n+l+k].concat(d?["M",m+g,c-h,"L",m+g,b+h]:[])):(a+=m+k-e,b+=m+k-e,n+=h,a=["M",m,n,"L",a,n,"L",a,n+g,"L",b,n+g,"L",b,n,"L",m+l+2*k,n].concat(d?["M",a-h,n,"L",b+h,n]:[]));this.outline[f]({d:a})},drawMasks:function(a,b,c,f){var d=this.left,e=this.top,h=this.height,g,k,l,m;c?(l=[d,d,d],m=[e,e+a,e+b],k=[h,h,h],g=[a,b-a,this.size-b]):(l=[d,d+a,d+b],m=[e,e,e],k=[a,b-a,this.size-b],g=[h,h,h]);v(this.shades,function(a,b){a[f]({x:l[b],y:m[b],width:k[b],height:g[b]})})},
renderElements:function(){var a=this,b=a.navigatorOptions,c=b.maskInside,e=a.chart,f=e.inverted,g=e.renderer,k;a.navigatorGroup=k=g.g("navigator").attr({zIndex:8,visibility:"hidden"}).add();var l={cursor:f?"ns-resize":"ew-resize"};v([!c,c,!c],function(c,d){a.shades[d]=g.rect().addClass("highcharts-navigator-mask"+(1===d?"-inside":"-outside")).attr({fill:c?b.maskFill:"rgba(0,0,0,0)"}).css(1===d&&l).add(k)});a.outline=g.path().addClass("highcharts-navigator-outline").attr({"stroke-width":b.outlineWidth,
stroke:b.outlineColor}).add(k);b.handles.enabled&&v([0,1],function(c){b.handles.inverted=e.inverted;a.handles[c]=g.symbol(b.handles.symbols[c],-b.handles.width/2-1,0,b.handles.width,b.handles.height,b.handles);a.handles[c].attr({zIndex:7-c}).addClass("highcharts-navigator-handle highcharts-navigator-handle-"+["left","right"][c]).add(k);var d=b.handles;a.handles[c].attr({fill:d.backgroundColor,stroke:d.borderColor,"stroke-width":d.lineWidth}).css(l)})},update:function(a){v(this.series||[],function(a){a.baseSeries&&
delete a.baseSeries.navigatorSeries});this.destroy();f(!0,this.chart.options.navigator,this.options,a);this.init(this.chart)},render:function(b,c,e,f){var d=this.chart,h,g,k=this.scrollbarHeight,l,m=this.xAxis;h=m.fake?d.xAxis[0]:m;var q=this.navigatorEnabled,z,p=this.rendered;g=d.inverted;var t,r=d.xAxis[0].minRange,y=d.xAxis[0].options.maxRange;if(!this.hasDragged||w(e)){if(!n(b)||!n(c))if(p)e=0,f=u(m.width,h.width);else return;this.left=u(m.left,d.plotLeft+k+(g?d.plotWidth:0));this.size=z=l=u(m.len,
(g?d.plotHeight:d.plotWidth)-2*k);d=g?k:l+2*k;e=u(e,m.toPixels(b,!0));f=u(f,m.toPixels(c,!0));n(e)&&Infinity!==Math.abs(e)||(e=0,f=d);b=m.toValue(e,!0);c=m.toValue(f,!0);t=Math.abs(a.correctFloat(c-b));t<r?this.grabbedLeft?e=m.toPixels(c-r,!0):this.grabbedRight&&(f=m.toPixels(b+r,!0)):w(y)&&t>y&&(this.grabbedLeft?e=m.toPixels(c-y,!0):this.grabbedRight&&(f=m.toPixels(b+y,!0)));this.zoomedMax=Math.min(Math.max(e,f,0),z);this.zoomedMin=Math.min(Math.max(this.fixedWidth?this.zoomedMax-this.fixedWidth:
Math.min(e,f),0),z);this.range=this.zoomedMax-this.zoomedMin;z=Math.round(this.zoomedMax);e=Math.round(this.zoomedMin);q&&(this.navigatorGroup.attr({visibility:"visible"}),p=p&&!this.hasDragged?"animate":"attr",this.drawMasks(e,z,g,p),this.drawOutline(e,z,g,p),this.navigatorOptions.handles.enabled&&(this.drawHandle(e,0,g,p),this.drawHandle(z,1,g,p)));this.scrollbar&&(g?(g=this.top-k,h=this.left-k+(q||!h.opposite?0:(h.titleOffset||0)+h.axisTitleMargin),k=l+2*k):(g=this.top+(q?this.height:-k),h=this.left-
k),this.scrollbar.position(h,g,d,k),this.scrollbar.setRange(this.zoomedMin/l,this.zoomedMax/l));this.rendered=!0}},addMouseEvents:function(){var a=this,c=a.chart,e=c.container,f=[],g,k;a.mouseMoveHandler=g=function(b){a.onMouseMove(b)};a.mouseUpHandler=k=function(b){a.onMouseUp(b)};f=a.getPartsEvents("mousedown");f.push(E(e,"mousemove",g),E(e.ownerDocument,"mouseup",k));b&&(f.push(E(e,"touchmove",g),E(e.ownerDocument,"touchend",k)),f.concat(a.getPartsEvents("touchstart")));a.eventsToUnbind=f;a.series&&
a.series[0]&&f.push(E(a.series[0].xAxis,"foundExtremes",function(){c.navigator.modifyNavigatorAxisExtremes()}))},getPartsEvents:function(a){var b=this,c=[];v(["shades","handles"],function(d){v(b[d],function(e,f){c.push(E(e.element,a,function(a){b[d+"Mousedown"](a,f)}))})});return c},shadesMousedown:function(a,b){a=this.chart.pointer.normalize(a);var c=this.chart,d=this.xAxis,e=this.zoomedMin,f=this.left,g=this.size,k=this.range,l=a.chartX,m,n;c.inverted&&(l=a.chartY,f=this.top);1===b?(this.grabbedCenter=
l,this.fixedWidth=k,this.dragOffset=l-e):(a=l-f-k/2,0===b?a=Math.max(0,a):2===b&&a+k>=g&&(a=g-k,d.reversed?(a-=k,n=this.getUnionExtremes().dataMin):m=this.getUnionExtremes().dataMax),a!==e&&(this.fixedWidth=k,b=d.toFixedRange(a,a+k,n,m),w(b.min)&&c.xAxis[0].setExtremes(Math.min(b.min,b.max),Math.max(b.min,b.max),!0,null,{trigger:"navigator"})))},handlesMousedown:function(a,b){this.chart.pointer.normalize(a);a=this.chart;var c=a.xAxis[0],d=a.inverted&&!c.reversed||!a.inverted&&c.reversed;0===b?(this.grabbedLeft=
!0,this.otherHandlePos=this.zoomedMax,this.fixedExtreme=d?c.min:c.max):(this.grabbedRight=!0,this.otherHandlePos=this.zoomedMin,this.fixedExtreme=d?c.max:c.min);a.fixedRange=null},onMouseMove:function(a){var b=this,c=b.chart,d=b.left,e=b.navigatorSize,f=b.range,g=b.dragOffset,k=c.inverted;a.touches&&0===a.touches[0].pageX||(a=c.pointer.normalize(a),c=a.chartX,k&&(d=b.top,c=a.chartY),b.grabbedLeft?(b.hasDragged=!0,b.render(0,0,c-d,b.otherHandlePos)):b.grabbedRight?(b.hasDragged=!0,b.render(0,0,b.otherHandlePos,
c-d)):b.grabbedCenter&&(b.hasDragged=!0,c<g?c=g:c>e+g-f&&(c=e+g-f),b.render(0,0,c-g,c-g+f)),b.hasDragged&&b.scrollbar&&b.scrollbar.options.liveRedraw&&(a.DOMType=a.type,setTimeout(function(){b.onMouseUp(a)},0)))},onMouseUp:function(a){var b=this.chart,c=this.xAxis,d=c&&c.reversed,e=this.scrollbar,f,g,k=a.DOMEvent||a;(!this.hasDragged||e&&e.hasDragged)&&"scrollbar"!==a.trigger||(e=this.getUnionExtremes(),this.zoomedMin===this.otherHandlePos?f=this.fixedExtreme:this.zoomedMax===this.otherHandlePos&&
(g=this.fixedExtreme),this.zoomedMax===this.size&&(g=d?e.dataMin:e.dataMax),0===this.zoomedMin&&(f=d?e.dataMax:e.dataMin),c=c.toFixedRange(this.zoomedMin,this.zoomedMax,f,g),w(c.min)&&b.xAxis[0].setExtremes(Math.min(c.min,c.max),Math.max(c.min,c.max),!0,this.hasDragged?!1:null,{trigger:"navigator",triggerOp:"navigator-drag",DOMEvent:k}));"mousemove"!==a.DOMType&&(this.grabbedLeft=this.grabbedRight=this.grabbedCenter=this.fixedWidth=this.fixedExtreme=this.otherHandlePos=this.hasDragged=this.dragOffset=
null)},removeEvents:function(){this.eventsToUnbind&&(v(this.eventsToUnbind,function(a){a()}),this.eventsToUnbind=void 0);this.removeBaseSeriesEvents()},removeBaseSeriesEvents:function(){var a=this.baseSeries||[];this.navigatorEnabled&&a[0]&&(!1!==this.navigatorOptions.adaptToUpdatedData&&v(a,function(a){D(a,"updatedData",this.updatedDataHandler)},this),a[0].xAxis&&D(a[0].xAxis,"foundExtremes",this.modifyBaseAxisExtremes))},init:function(a){var b=a.options,c=b.navigator,d=c.enabled,e=b.scrollbar,g=
e.enabled,b=d?c.height:0,k=g?e.height:0;this.handles=[];this.shades=[];this.chart=a;this.setBaseSeries();this.height=b;this.scrollbarHeight=k;this.scrollbarEnabled=g;this.navigatorEnabled=d;this.navigatorOptions=c;this.scrollbarOptions=e;this.outlineHeight=b+k;this.opposite=u(c.opposite,!d&&a.inverted);var l=this,e=l.baseSeries,g=a.xAxis.length,m=a.yAxis.length,n=e&&e[0]&&e[0].xAxis||a.xAxis[0];a.extraMargin={type:l.opposite?"plotTop":"marginBottom",value:(d||!a.inverted?l.outlineHeight:0)+c.margin};
a.inverted&&(a.extraMargin.type=l.opposite?"marginRight":"plotLeft");a.isDirtyBox=!0;l.navigatorEnabled?(l.xAxis=new F(a,f({breaks:n.options.breaks,ordinal:n.options.ordinal},c.xAxis,{id:"navigator-x-axis",yAxis:"navigator-y-axis",isX:!0,type:"datetime",index:g,offset:0,keepOrdinalPadding:!0,startOnTick:!1,endOnTick:!1,minPadding:0,maxPadding:0,zoomEnabled:!1},a.inverted?{offsets:[k,0,-k,0],width:b}:{offsets:[0,-k,0,k],height:b})),l.yAxis=new F(a,f(c.yAxis,{id:"navigator-y-axis",alignTicks:!1,offset:0,
index:m,zoomEnabled:!1},a.inverted?{width:b}:{height:b})),e||c.series.data?l.updateNavigatorSeries():0===a.series.length&&H(a,"redraw",function(b,c){0<a.series.length&&!l.series&&(l.setBaseSeries(),a.redraw=b);b.call(a,c)}),l.renderElements(),l.addMouseEvents()):l.xAxis={translate:function(b,c){var d=a.xAxis[0],e=d.getExtremes(),f=d.len-2*k,h=J("min",d.options.min,e.dataMin),d=J("max",d.options.max,e.dataMax)-h;return c?b*d/f+h:f*(b-h)/d},toPixels:function(a){return this.translate(a)},toValue:function(a){return this.translate(a,
!0)},toFixedRange:F.prototype.toFixedRange,fake:!0};a.options.scrollbar.enabled&&(a.scrollbar=l.scrollbar=new q(a.renderer,f(a.options.scrollbar,{margin:l.navigatorEnabled?0:10,vertical:a.inverted}),a),E(l.scrollbar,"changed",function(b){var c=l.size,d=c*this.to,c=c*this.from;l.hasDragged=l.scrollbar.hasDragged;l.render(0,0,c,d);(a.options.scrollbar.liveRedraw||"mousemove"!==b.DOMType&&"touchmove"!==b.DOMType)&&setTimeout(function(){l.onMouseUp(b)})}));l.addBaseSeriesEvents();l.addChartEvents()},
getUnionExtremes:function(a){var b=this.chart.xAxis[0],c=this.xAxis,d=c.options,e=b.options,f;a&&null===b.dataMin||(f={dataMin:u(d&&d.min,J("min",e.min,b.dataMin,c.dataMin,c.min)),dataMax:u(d&&d.max,J("max",e.max,b.dataMax,c.dataMax,c.max))});return f},setBaseSeries:function(a,b){var c=this.chart,d=this.baseSeries=[];a=a||c.options&&c.options.navigator.baseSeries||0;v(c.series||[],function(b,c){b.options.isInternal||!b.options.showInNavigator&&(c!==a&&b.options.id!==a||!1===b.options.showInNavigator)||
d.push(b)});this.xAxis&&!this.xAxis.fake&&this.updateNavigatorSeries(b)},updateNavigatorSeries:function(b){var d=this,e=d.chart,h=d.baseSeries,k,m,n=d.navigatorOptions.series,q,p={enableMouseTracking:!1,index:null,linkedTo:null,group:"nav",padXAxis:!1,xAxis:"navigator-x-axis",yAxis:"navigator-y-axis",showInLegend:!1,stacking:!1,isInternal:!0,visible:!0},t=d.series=a.grep(d.series||[],function(b){var c=b.baseSeries;return 0>a.inArray(c,h)?(c&&(D(c,"updatedData",d.updatedDataHandler),delete c.navigatorSeries),
b.destroy(),!1):!0});h&&h.length&&v(h,function(a){var z=a.navigatorSeries,r=c({color:a.color},g(n)?l.navigator.series:n);z&&!1===d.navigatorOptions.adaptToUpdatedData||(p.name="Navigator "+h.length,k=a.options||{},q=k.navigatorOptions||{},m=f(k,p,r,q),r=q.data||r.data,d.hasNavigatorData=d.hasNavigatorData||!!r,m.data=r||k.data&&k.data.slice(0),z&&z.options?z.update(m,b):(a.navigatorSeries=e.initSeries(m),a.navigatorSeries.baseSeries=a,t.push(a.navigatorSeries)))});if(n.data&&(!h||!h.length)||g(n))d.hasNavigatorData=
!1,n=a.splat(n),v(n,function(a,b){p.name="Navigator "+(t.length+1);m=f(l.navigator.series,{color:e.series[b]&&!e.series[b].options.isInternal&&e.series[b].color||e.options.colors[b]||e.options.colors[0]},p,a);m.data=a.data;m.data&&(d.hasNavigatorData=!0,t.push(e.initSeries(m)))});this.addBaseSeriesEvents()},addBaseSeriesEvents:function(){var a=this,b=a.baseSeries||[];b[0]&&b[0].xAxis&&E(b[0].xAxis,"foundExtremes",this.modifyBaseAxisExtremes);v(b,function(b){E(b,"show",function(){this.navigatorSeries&&
this.navigatorSeries.setVisible(!0,!1)});E(b,"hide",function(){this.navigatorSeries&&this.navigatorSeries.setVisible(!1,!1)});!1!==this.navigatorOptions.adaptToUpdatedData&&b.xAxis&&E(b,"updatedData",this.updatedDataHandler);E(b,"remove",function(){this.navigatorSeries&&(B(a.series,this.navigatorSeries),this.navigatorSeries.remove(!1),delete this.navigatorSeries)})},this)},modifyNavigatorAxisExtremes:function(){var a=this.xAxis,b;a.getExtremes&&(!(b=this.getUnionExtremes(!0))||b.dataMin===a.min&&
b.dataMax===a.max||(a.min=b.dataMin,a.max=b.dataMax))},modifyBaseAxisExtremes:function(){var a=this.chart.navigator,b=this.getExtremes(),c=b.dataMin,e=b.dataMax,b=b.max-b.min,f=a.stickToMin,g=a.stickToMax,k=this.options.overscroll,l,m,q=a.series&&a.series[0],p=!!this.setExtremes;this.eventArgs&&"rangeSelectorButton"===this.eventArgs.trigger||(f&&(m=c,l=m+b),g&&(l=e+k,f||(m=Math.max(l-b,q&&q.xData?q.xData[0]:-Number.MAX_VALUE))),p&&(f||g)&&n(m)&&(this.min=this.userMin=m,this.max=this.userMax=l));a.stickToMin=
a.stickToMax=null},updatedDataHandler:function(){var a=this.chart.navigator,b=this.navigatorSeries;a.stickToMax=a.xAxis.reversed?0===Math.round(a.zoomedMin):Math.round(a.zoomedMax)>=Math.round(a.size);a.stickToMin=n(this.xAxis.min)&&this.xAxis.min<=this.xData[0]&&(!this.chart.fixedRange||!a.stickToMax);b&&!a.hasNavigatorData&&(b.options.pointStart=this.xData[0],b.setData(this.options.data,!1,null,!1))},addChartEvents:function(){E(this.chart,"redraw",function(){var a=this.navigator,b=a&&(a.baseSeries&&
a.baseSeries[0]&&a.baseSeries[0].xAxis||a.scrollbar&&this.xAxis[0]);b&&a.render(b.min,b.max)})},destroy:function(){this.removeEvents();this.xAxis&&(B(this.chart.xAxis,this.xAxis),B(this.chart.axes,this.xAxis));this.yAxis&&(B(this.chart.yAxis,this.yAxis),B(this.chart.axes,this.yAxis));v(this.series||[],function(a){a.destroy&&a.destroy()});v("series xAxis yAxis shades outline scrollbarTrack scrollbarRifles scrollbarGroup scrollbar navigatorGroup rendered".split(" "),function(a){this[a]&&this[a].destroy&&
this[a].destroy();this[a]=null},this);v([this.handles],function(a){p(a)},this)}};a.Navigator=C;H(F.prototype,"zoom",function(a,b,c){var d=this.chart,e=d.options,f=e.chart.zoomType,g=e.navigator,e=e.rangeSelector,k;this.isXAxis&&(g&&g.enabled||e&&e.enabled)&&("x"===f?d.resetZoomButton="blocked":"y"===f?k=!1:"xy"===f&&this.options.range&&(d=this.previousZoom,w(b)?this.previousZoom=[this.min,this.max]:d&&(b=d[0],c=d[1],delete this.previousZoom)));return void 0!==k?k:a.call(this,b,c)});H(r.prototype,
"init",function(a,b,c){E(this,"beforeRender",function(){var a=this.options;if(a.navigator.enabled||a.scrollbar.enabled)this.scroller=this.navigator=new C(this)});a.call(this,b,c)});H(r.prototype,"setChartSize",function(a){var b=this.legend,c=this.navigator,d,e,f,g;a.apply(this,[].slice.call(arguments,1));c&&(e=b&&b.options,f=c.xAxis,g=c.yAxis,d=c.scrollbarHeight,this.inverted?(c.left=c.opposite?this.chartWidth-d-c.height:this.spacing[3]+d,c.top=this.plotTop+d):(c.left=this.plotLeft+d,c.top=c.navigatorOptions.top||
this.chartHeight-c.height-d-this.spacing[2]-(this.rangeSelector&&this.extraBottomMargin?this.rangeSelector.getHeight():0)-(e&&"bottom"===e.verticalAlign&&e.enabled&&!e.floating?b.legendHeight+u(e.margin,10):0)),f&&g&&(this.inverted?f.options.left=g.options.left=c.left:f.options.top=g.options.top=c.top,f.setAxisSize(),g.setAxisSize()))});H(A.prototype,"addPoint",function(a,b,c,f,g){var d=this.options.turboThreshold;d&&this.xData.length>d&&t(b,!0)&&this.chart.navigator&&e(20,!0);a.call(this,b,c,f,g)});
H(r.prototype,"addSeries",function(a,b,c,e){a=a.call(this,b,!1,e);this.navigator&&this.navigator.setBaseSeries(null,!1);u(c,!0)&&this.redraw();return a});H(A.prototype,"update",function(a,b,c){a.call(this,b,!1);this.chart.navigator&&!this.options.isInternal&&this.chart.navigator.setBaseSeries(null,!1);u(c,!0)&&this.chart.redraw()});r.prototype.callbacks.push(function(a){var b=a.navigator;b&&(a=a.xAxis[0].getExtremes(),b.render(a.min,a.max))})})(L);(function(a){function C(a){this.init(a)}var E=a.addEvent,
F=a.Axis,r=a.Chart,m=a.css,l=a.createElement,w=a.defaultOptions,p=a.defined,v=a.destroyObjectProperties,B=a.discardElement,e=a.each,c=a.extend,k=a.fireEvent,b=a.isNumber,g=a.merge,n=a.pick,t=a.pInt,f=a.splat,u=a.wrap;c(w,{rangeSelector:{verticalAlign:"top",buttonTheme:{"stroke-width":0,width:28,height:18,padding:2,zIndex:7},floating:!1,x:0,y:0,height:void 0,inputPosition:{align:"right",x:0,y:0},buttonPosition:{align:"left",x:0,y:0},labelStyle:{color:"#666666"}}});w.lang=g(w.lang,{rangeSelectorZoom:"Zoom",
rangeSelectorFrom:"From",rangeSelectorTo:"To"});C.prototype={clickButton:function(a,c){var g=this,k=g.chart,l=g.buttonOptions[a],m=k.xAxis[0],q=k.scroller&&k.scroller.getUnionExtremes()||m||{},d=q.dataMin,p=q.dataMax,t,h=m&&Math.round(Math.min(m.max,n(p,m.max))),r=l.type,u,q=l._range,v,w,D,B=l.dataGrouping;if(null!==d&&null!==p){k.fixedRange=q;B&&(this.forcedDataGrouping=!0,F.prototype.setDataGrouping.call(m||{chart:this.chart},B,!1));if("month"===r||"year"===r)m?(r={range:l,max:h,chart:k,dataMin:d,
dataMax:p},t=m.minFromRange.call(r),b(r.newMax)&&(h=r.newMax)):q=l;else if(q)t=Math.max(h-q,d),h=Math.min(t+q,p);else if("ytd"===r)if(m)void 0===p&&(d=Number.MAX_VALUE,p=Number.MIN_VALUE,e(k.series,function(a){a=a.xData;d=Math.min(a[0],d);p=Math.max(a[a.length-1],p)}),c=!1),h=g.getYTDExtremes(p,d,k.time.useUTC),t=v=h.min,h=h.max;else{E(k,"beforeRender",function(){g.clickButton(a)});return}else"all"===r&&m&&(t=d,h=p);t+=l._offsetMin;h+=l._offsetMax;g.setSelected(a);m?m.setExtremes(t,h,n(c,1),null,
{trigger:"rangeSelectorButton",rangeSelectorButton:l}):(u=f(k.options.xAxis)[0],D=u.range,u.range=q,w=u.min,u.min=v,E(k,"load",function(){u.range=D;u.min=w}))}},setSelected:function(a){this.selected=this.options.selected=a},defaultButtons:[{type:"month",count:1,text:"1m"},{type:"month",count:3,text:"3m"},{type:"month",count:6,text:"6m"},{type:"ytd",text:"YTD"},{type:"year",count:1,text:"1y"},{type:"all",text:"All"}],init:function(a){var b=this,c=a.options.rangeSelector,f=c.buttons||[].concat(b.defaultButtons),
g=c.selected,l=function(){var a=b.minInput,c=b.maxInput;a&&a.blur&&k(a,"blur");c&&c.blur&&k(c,"blur")};b.chart=a;b.options=c;b.buttons=[];a.extraTopMargin=c.height;b.buttonOptions=f;this.unMouseDown=E(a.container,"mousedown",l);this.unResize=E(a,"resize",l);e(f,b.computeButtonRange);void 0!==g&&f[g]&&this.clickButton(g,!1);E(a,"load",function(){a.xAxis&&a.xAxis[0]&&E(a.xAxis[0],"setExtremes",function(c){this.max-this.min!==a.fixedRange&&"rangeSelectorButton"!==c.trigger&&"updatedData"!==c.trigger&&
b.forcedDataGrouping&&this.setDataGrouping(!1,!1)})})},updateButtonStates:function(){var a=this.chart,c=a.xAxis[0],f=Math.round(c.max-c.min),g=!c.hasVisibleSeries,k=a.scroller&&a.scroller.getUnionExtremes()||c,l=k.dataMin,m=k.dataMax,a=this.getYTDExtremes(m,l,a.time.useUTC),d=a.min,n=a.max,p=this.selected,h=b(p),t=this.options.allButtonsEnabled,r=this.buttons;e(this.buttonOptions,function(a,b){var e=a._range,k=a.type,q=a.count||1,y=r[b],A=0;a=a._offsetMax-a._offsetMin;b=b===p;var u=e>m-l,z=e<c.minRange,
v=!1,x=!1,e=e===f;("month"===k||"year"===k)&&f+36E5>=864E5*{month:28,year:365}[k]*q-a&&f-36E5<=864E5*{month:31,year:366}[k]*q+a?e=!0:"ytd"===k?(e=n-d+a===f,v=!b):"all"===k&&(e=c.max-c.min>=m-l,x=!b&&h&&e);k=!t&&(u||z||x||g);q=b&&e||e&&!h&&!v;k?A=3:q&&(h=!0,A=2);y.state!==A&&y.setState(A)})},computeButtonRange:function(a){var b=a.type,c=a.count||1,e={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5};if(e[b])a._range=e[b]*c;else if("month"===b||"year"===b)a._range=864E5*{month:30,
year:365}[b]*c;a._offsetMin=n(a.offsetMin,0);a._offsetMax=n(a.offsetMax,0);a._range+=a._offsetMax-a._offsetMin},setInputValue:function(a,b){var c=this.chart.options.rangeSelector,e=this.chart.time,f=this[a+"Input"];p(b)&&(f.previousValue=f.HCTime,f.HCTime=b);f.value=e.dateFormat(c.inputEditDateFormat||"%Y-%m-%d",f.HCTime);this[a+"DateBox"].attr({text:e.dateFormat(c.inputDateFormat||"%b %e, %Y",f.HCTime)})},showInput:function(a){var b=this.inputGroup,c=this[a+"DateBox"];m(this[a+"Input"],{left:b.translateX+
c.x+"px",top:b.translateY+"px",width:c.width-2+"px",height:c.height-2+"px",border:"2px solid silver"})},hideInput:function(a){m(this[a+"Input"],{border:0,width:"1px",height:"1px"});this.setInputValue(a)},drawInput:function(a){function e(){var a=v.value,c=(r.inputDateParser||Date.parse)(a),d=k.xAxis[0],e=k.scroller&&k.scroller.xAxis?k.scroller.xAxis:d,g=e.dataMin,e=e.dataMax;c!==v.previousValue&&(v.previousValue=c,b(c)||(c=a.split("-"),c=Date.UTC(t(c[0]),t(c[1])-1,t(c[2]))),b(c)&&(k.time.useUTC||(c+=
6E4*(new Date).getTimezoneOffset()),u?c>f.maxInput.HCTime?c=void 0:c<g&&(c=g):c<f.minInput.HCTime?c=void 0:c>e&&(c=e),void 0!==c&&d.setExtremes(u?c:d.min,u?d.max:c,void 0,void 0,{trigger:"rangeSelectorInput"})))}var f=this,k=f.chart,n=k.renderer.style||{},p=k.renderer,r=k.options.rangeSelector,d=f.div,u="min"===a,v,h,x=this.inputGroup;this[a+"Label"]=h=p.label(w.lang[u?"rangeSelectorFrom":"rangeSelectorTo"],this.inputGroup.offset).addClass("highcharts-range-label").attr({padding:2}).add(x);x.offset+=
h.width+5;this[a+"DateBox"]=p=p.label("",x.offset).addClass("highcharts-range-input").attr({padding:2,width:r.inputBoxWidth||90,height:r.inputBoxHeight||17,stroke:r.inputBoxBorderColor||"#cccccc","stroke-width":1,"text-align":"center"}).on("click",function(){f.showInput(a);f[a+"Input"].focus()}).add(x);x.offset+=p.width+(u?10:0);this[a+"Input"]=v=l("input",{name:a,className:"highcharts-range-selector",type:"text"},{top:k.plotTop+"px"},d);h.css(g(n,r.labelStyle));p.css(g({color:"#333333"},n,r.inputStyle));
m(v,c({position:"absolute",border:0,width:"1px",height:"1px",padding:0,textAlign:"center",fontSize:n.fontSize,fontFamily:n.fontFamily,top:"-9999em"},r.inputStyle));v.onfocus=function(){f.showInput(a)};v.onblur=function(){f.hideInput(a)};v.onchange=e;v.onkeypress=function(a){13===a.keyCode&&e()}},getPosition:function(){var a=this.chart,b=a.options.rangeSelector,a="top"===b.verticalAlign?a.plotTop-a.axisOffset[0]:0;return{buttonTop:a+b.buttonPosition.y,inputTop:a+b.inputPosition.y-10}},getYTDExtremes:function(a,
b,c){var e=this.chart.time,f=new e.Date(a),g=e.get("FullYear",f);c=c?e.Date.UTC(g,0,1):+new e.Date(g,0,1);b=Math.max(b||0,c);f=f.getTime();return{max:Math.min(a||f,f),min:b}},render:function(a,b){var c=this,f=c.chart,g=f.renderer,k=f.container,m=f.options,d=m.exporting&&!1!==m.exporting.enabled&&m.navigation&&m.navigation.buttonOptions,q=w.lang,p=c.div,h=m.rangeSelector,m=h.floating,t=c.buttons,p=c.inputGroup,r=h.buttonTheme,u=h.buttonPosition,v=h.inputPosition,B=h.inputEnabled,D=r&&r.states,C=f.plotLeft,
F,E=c.buttonGroup,L;L=c.rendered;var W=c.options.verticalAlign,Z=f.legend,aa=Z&&Z.options,ba=u.y,X=v.y,ca=L||!1,U=0,R=0,S;if(!1!==h.enabled){L||(c.group=L=g.g("range-selector-group").attr({zIndex:7}).add(),c.buttonGroup=E=g.g("range-selector-buttons").add(L),c.zoomText=g.text(q.rangeSelectorZoom,n(C+u.x,C),15).css(h.labelStyle).add(E),F=n(C+u.x,C)+c.zoomText.getBBox().width+5,e(c.buttonOptions,function(a,b){t[b]=g.button(a.text,F,0,function(){var d=a.events&&a.events.click,e;d&&(e=d.call(a));!1!==
e&&c.clickButton(b);c.isActive=!0},r,D&&D.hover,D&&D.select,D&&D.disabled).attr({"text-align":"center"}).add(E);F+=t[b].width+n(h.buttonSpacing,5)}),!1!==B&&(c.div=p=l("div",null,{position:"relative",height:0,zIndex:1}),k.parentNode.insertBefore(p,k),c.inputGroup=p=g.g("input-group").add(L),p.offset=0,c.drawInput("min"),c.drawInput("max")));C=f.plotLeft-f.spacing[3];c.updateButtonStates();d&&this.titleCollision(f)&&"top"===W&&"right"===u.align&&u.y+E.getBBox().height-12<(d.y||0)+d.height&&(U=-40);
"left"===u.align?S=u.x-f.spacing[3]:"right"===u.align&&(S=u.x+U-f.spacing[1]);E.align({y:u.y,width:E.getBBox().width,align:u.align,x:S},!0,f.spacingBox);c.group.placed=ca;c.buttonGroup.placed=ca;!1!==B&&(U=d&&this.titleCollision(f)&&"top"===W&&"right"===v.align&&v.y-p.getBBox().height-12<(d.y||0)+d.height+f.spacing[0]?-40:0,"left"===v.align?S=C:"right"===v.align&&(S=-Math.max(f.axisOffset[1],-U)),p.align({y:v.y,width:p.getBBox().width,align:v.align,x:v.x+S-2},!0,f.spacingBox),k=p.alignAttr.translateX+
p.alignOptions.x-U+p.getBBox().x+2,d=p.alignOptions.width,q=E.alignAttr.translateX+E.getBBox().x,S=E.getBBox().width+20,(v.align===u.align||q+S>k&&k+d>q&&ba<X+p.getBBox().height)&&p.attr({translateX:p.alignAttr.translateX+(f.axisOffset[1]>=-U?0:-U),translateY:p.alignAttr.translateY+E.getBBox().height+10}),c.setInputValue("min",a),c.setInputValue("max",b),c.inputGroup.placed=ca);c.group.align({verticalAlign:W},!0,f.spacingBox);a=c.group.getBBox().height+20;b=c.group.alignAttr.translateY;"bottom"===
W&&(Z=aa&&"bottom"===aa.verticalAlign&&aa.enabled&&!aa.floating?Z.legendHeight+n(aa.margin,10):0,a=a+Z-20,R=b-a-(m?0:h.y)-10);if("top"===W)m&&(R=0),f.titleOffset&&(R=f.titleOffset+f.options.title.margin),R+=f.margin[0]-f.spacing[0]||0;else if("middle"===W)if(X===ba)R=0>X?b+void 0:b;else if(X||ba)R=0>X||0>ba?R-Math.min(X,ba):b-a+NaN;c.group.translate(h.x,h.y+Math.floor(R));!1!==B&&(c.minInput.style.marginTop=c.group.translateY+"px",c.maxInput.style.marginTop=c.group.translateY+"px");c.rendered=!0}},
getHeight:function(){var a=this.options,b=this.group,c=a.y,e=a.buttonPosition.y,a=a.inputPosition.y,b=b?b.getBBox(!0).height+13+c:0,c=Math.min(a,e);if(0>a&&0>e||0<a&&0<e)b+=Math.abs(c);return b},titleCollision:function(a){return!(a.options.title.text||a.options.subtitle.text)},update:function(a){var b=this.chart;g(!0,b.options.rangeSelector,a);this.destroy();this.init(b);b.rangeSelector.render()},destroy:function(){var b=this,c=b.minInput,e=b.maxInput;b.unMouseDown();b.unResize();v(b.buttons);c&&
(c.onfocus=c.onblur=c.onchange=null);e&&(e.onfocus=e.onblur=e.onchange=null);a.objectEach(b,function(a,c){a&&"chart"!==c&&(a.destroy?a.destroy():a.nodeType&&B(this[c]));a!==C.prototype[c]&&(b[c]=null)},this)}};F.prototype.toFixedRange=function(a,c,e,f){var g=this.chart&&this.chart.fixedRange;a=n(e,this.translate(a,!0,!this.horiz));c=n(f,this.translate(c,!0,!this.horiz));e=g&&(c-a)/g;.7<e&&1.3>e&&(f?a=c-g:c=a+g);b(a)&&b(c)||(a=c=void 0);return{min:a,max:c}};F.prototype.minFromRange=function(){var a=
this.range,c={month:"Month",year:"FullYear"}[a.type],e,f=this.max,g,k,l=function(a,b){var d=new Date(a),e=d["get"+c]();d["set"+c](e+b);e===d["get"+c]()&&d.setDate(0);return d.getTime()-a};b(a)?(e=f-a,k=a):(e=f+l(f,-a.count),this.chart&&(this.chart.fixedRange=f-e));g=n(this.dataMin,Number.MIN_VALUE);b(e)||(e=g);e<=g&&(e=g,void 0===k&&(k=l(e,a.count)),this.newMax=Math.min(e+k,this.dataMax));b(f)||(e=void 0);return e};u(r.prototype,"init",function(a,b,c){E(this,"init",function(){this.options.rangeSelector.enabled&&
(this.rangeSelector=new C(this))});a.call(this,b,c)});u(r.prototype,"render",function(a,b,c){var f=this.axes,g=this.rangeSelector;g&&(e(f,function(a){a.updateNames();a.setScale()}),this.getAxisMargins(),g.render(),f=g.options.verticalAlign,g.options.floating||("bottom"===f?this.extraBottomMargin=!0:"middle"!==f&&(this.extraTopMargin=!0)));a.call(this,b,c)});u(r.prototype,"update",function(b,c,e,f){var g=this.rangeSelector,k;this.extraTopMargin=this.extraBottomMargin=!1;g&&(g.render(),k=c.rangeSelector&&
c.rangeSelector.verticalAlign||g.options&&g.options.verticalAlign,g.options.floating||("bottom"===k?this.extraBottomMargin=!0:"middle"!==k&&(this.extraTopMargin=!0)));b.call(this,a.merge(!0,c,{chart:{marginBottom:n(c.chart&&c.chart.marginBottom,this.margin.bottom),spacingBottom:n(c.chart&&c.chart.spacingBottom,this.spacing.bottom)}}),e,f)});u(r.prototype,"redraw",function(a,b,c){var e=this.rangeSelector;e&&!e.options.floating&&(e.render(),e=e.options.verticalAlign,"bottom"===e?this.extraBottomMargin=
!0:"middle"!==e&&(this.extraTopMargin=!0));a.call(this,b,c)});r.prototype.adjustPlotArea=function(){var a=this.rangeSelector;this.rangeSelector&&(a=a.getHeight(),this.extraTopMargin&&(this.plotTop+=a),this.extraBottomMargin&&(this.marginBottom+=a))};r.prototype.callbacks.push(function(a){function c(){e=a.xAxis[0].getExtremes();b(e.min)&&f.render(e.min,e.max)}var e,f=a.rangeSelector,g,k;f&&(k=E(a.xAxis[0],"afterSetExtremes",function(a){f.render(a.min,a.max)}),g=E(a,"redraw",c),c());E(a,"destroy",function(){f&&
(g(),k())})});a.RangeSelector=C})(L);(function(a){var C=a.arrayMax,E=a.arrayMin,F=a.Axis,r=a.Chart,m=a.defined,l=a.each,w=a.extend,p=a.format,v=a.grep,B=a.inArray,e=a.isNumber,c=a.isString,k=a.map,b=a.merge,g=a.pick,n=a.Point,t=a.Renderer,f=a.Series,u=a.splat,D=a.SVGRenderer,q=a.VMLRenderer,A=a.wrap,y=f.prototype,H=y.init,I=y.processData,J=n.prototype.tooltipFormatter;a.StockChart=a.stockChart=function(d,e,f){var h=c(d)||d.nodeName,l=arguments[h?1:0],m=l.series,n=a.getOptions(),p,q=g(l.navigator&&
l.navigator.enabled,n.navigator.enabled,!0),t=q?{startOnTick:!1,endOnTick:!1}:null,v={marker:{enabled:!1,radius:2}},z={shadow:!1,borderWidth:0};l.xAxis=k(u(l.xAxis||{}),function(a,c){return b({minPadding:0,maxPadding:0,overscroll:0,ordinal:!0,title:{text:null},labels:{overflow:"justify"},showLastLabel:!0},n.xAxis,n.xAxis&&n.xAxis[c],a,{type:"datetime",categories:null},t)});l.yAxis=k(u(l.yAxis||{}),function(a,c){p=g(a.opposite,!0);return b({labels:{y:-2},opposite:p,showLastLabel:!(!a.categories&&"category"!==
a.type),title:{text:null}},n.yAxis,n.yAxis&&n.yAxis[c],a)});l.series=null;l=b({chart:{panning:!0,pinchType:"x"},navigator:{enabled:q},scrollbar:{enabled:g(n.scrollbar.enabled,!0)},rangeSelector:{enabled:g(n.rangeSelector.enabled,!0)},title:{text:null},tooltip:{split:g(n.tooltip.split,!0),crosshairs:!0},legend:{enabled:!1},plotOptions:{line:v,spline:v,area:v,areaspline:v,arearange:v,areasplinerange:v,column:z,columnrange:z,candlestick:z,ohlc:z}},l,{isStock:!0});l.series=m;return h?new r(d,l,f):new r(l,
e)};A(F.prototype,"autoLabelAlign",function(a){var b=this.chart,c=this.options,b=b._labelPanes=b._labelPanes||{},d=this.options.labels;return this.chart.options.isStock&&"yAxis"===this.coll&&(c=c.top+","+c.height,!b[c]&&d.enabled)?(15===d.x&&(d.x=0),void 0===d.align&&(d.align="right"),b[c]=this,"right"):a.apply(this,[].slice.call(arguments,1))});A(F.prototype,"destroy",function(a){var b=this.chart,c=this.options&&this.options.top+","+this.options.height;c&&b._labelPanes&&b._labelPanes[c]===this&&
delete b._labelPanes[c];return a.apply(this,Array.prototype.slice.call(arguments,1))});A(F.prototype,"getPlotLinePath",function(b,f,n,h,p,q){var d=this,t=this.isLinked&&!this.series?this.linkedParent.series:this.series,r=d.chart,u=r.renderer,v=d.left,z=d.top,x,w,y,A,C=[],G=[],D,E;if("xAxis"!==d.coll&&"yAxis"!==d.coll)return b.apply(this,[].slice.call(arguments,1));G=function(a){var b="xAxis"===a?"yAxis":"xAxis";a=d.options[b];return e(a)?[r[b][a]]:c(a)?[r.get(a)]:k(t,function(a){return a[b]})}(d.coll);
l(d.isXAxis?r.yAxis:r.xAxis,function(a){if(m(a.options.id)?-1===a.options.id.indexOf("navigator"):1){var b=a.isXAxis?"yAxis":"xAxis",b=m(a.options[b])?r[b][a.options[b]]:r[b][0];d===b&&G.push(a)}});D=G.length?[]:[d.isXAxis?r.yAxis[0]:r.xAxis[0]];l(G,function(b){-1!==B(b,D)||a.find(D,function(a){return a.pos===b.pos&&a.len&&b.len})||D.push(b)});E=g(q,d.translate(f,null,null,h));e(E)&&(d.horiz?l(D,function(a){var b;w=a.pos;A=w+a.len;x=y=Math.round(E+d.transB);if(x<v||x>v+d.width)p?x=y=Math.min(Math.max(v,
x),v+d.width):b=!0;b||C.push("M",x,w,"L",y,A)}):l(D,function(a){var b;x=a.pos;y=x+a.len;w=A=Math.round(z+d.height-E);if(w<z||w>z+d.height)p?w=A=Math.min(Math.max(z,w),d.top+d.height):b=!0;b||C.push("M",x,w,"L",y,A)}));return 0<C.length?u.crispPolyLine(C,n||1):null});D.prototype.crispPolyLine=function(a,b){var c;for(c=0;c<a.length;c+=6)a[c+1]===a[c+4]&&(a[c+1]=a[c+4]=Math.round(a[c+1])-b%2/2),a[c+2]===a[c+5]&&(a[c+2]=a[c+5]=Math.round(a[c+2])+b%2/2);return a};t===q&&(q.prototype.crispPolyLine=D.prototype.crispPolyLine);
A(F.prototype,"hideCrosshair",function(a,b){a.call(this,b);this.crossLabel&&(this.crossLabel=this.crossLabel.hide())});A(F.prototype,"drawCrosshair",function(a,b,c){var d,e;a.call(this,b,c);if(m(this.crosshair.label)&&this.crosshair.label.enabled&&this.cross){a=this.chart;var f=this.options.crosshair.label,k=this.horiz;d=this.opposite;e=this.left;var l=this.top,n=this.crossLabel,q,r=f.format,t="",u="inside"===this.options.tickPosition,v=!1!==this.crosshair.snap,z=0;b||(b=this.cross&&this.cross.e);
q=k?"center":d?"right"===this.labelAlign?"right":"left":"left"===this.labelAlign?"left":"center";n||(n=this.crossLabel=a.renderer.label(null,null,null,f.shape||"callout").addClass("highcharts-crosshair-label"+(this.series[0]&&" highcharts-color-"+this.series[0].colorIndex)).attr({align:f.align||q,padding:g(f.padding,8),r:g(f.borderRadius,3),zIndex:2}).add(this.labelGroup),n.attr({fill:f.backgroundColor||this.series[0]&&this.series[0].color||"#666666",stroke:f.borderColor||"","stroke-width":f.borderWidth||
0}).css(w({color:"#ffffff",fontWeight:"normal",fontSize:"11px",textAlign:"center"},f.style)));k?(q=v?c.plotX+e:b.chartX,l+=d?0:this.height):(q=d?this.width+e:0,l=v?c.plotY+l:b.chartY);r||f.formatter||(this.isDatetimeAxis&&(t="%b %d, %Y"),r="{value"+(t?":"+t:"")+"}");b=v?c[this.isXAxis?"x":"y"]:this.toValue(k?b.chartX:b.chartY);n.attr({text:r?p(r,{value:b},a.time):f.formatter.call(this,b),x:q,y:l,visibility:b<this.min||b>this.max?"hidden":"visible"});b=n.getBBox();if(k){if(u&&!d||!u&&d)l=n.y-b.height}else l=
n.y-b.height/2;k?(d=e-b.x,e=e+this.width-b.x):(d="left"===this.labelAlign?e:0,e="right"===this.labelAlign?e+this.width:a.chartWidth);n.translateX<d&&(z=d-n.translateX);n.translateX+b.width>=e&&(z=-(n.translateX+b.width-e));n.attr({x:q+z,y:l,anchorX:k?q:this.opposite?0:a.chartWidth,anchorY:k?this.opposite?a.chartHeight:0:l+b.height/2})}});y.init=function(){H.apply(this,arguments);this.setCompare(this.options.compare)};y.setCompare=function(a){this.modifyValue="value"===a||"percent"===a?function(b,
c){var d=this.compareValue;if(void 0!==b&&void 0!==d)return b="value"===a?b-d:b/d*100-(100===this.options.compareBase?0:100),c&&(c.change=b),b}:null;this.userOptions.compare=a;this.chart.hasRendered&&(this.isDirty=!0)};y.processData=function(){var a,b=-1,c,f,g=!0===this.options.compareStart?0:1,k,l;I.apply(this,arguments);if(this.xAxis&&this.processedYData)for(c=this.processedXData,f=this.processedYData,k=f.length,this.pointArrayMap&&(b=B("close",this.pointArrayMap),-1===b&&(b=B(this.pointValKey||
"y",this.pointArrayMap))),a=0;a<k-g;a++)if(l=f[a]&&-1<b?f[a][b]:f[a],e(l)&&c[a+g]>=this.xAxis.min&&0!==l){this.compareValue=l;break}};A(y,"getExtremes",function(a){var b;a.apply(this,[].slice.call(arguments,1));this.modifyValue&&(b=[this.modifyValue(this.dataMin),this.modifyValue(this.dataMax)],this.dataMin=E(b),this.dataMax=C(b))});F.prototype.setCompare=function(a,b){this.isXAxis||(l(this.series,function(b){b.setCompare(a)}),g(b,!0)&&this.chart.redraw())};n.prototype.tooltipFormatter=function(b){b=
b.replace("{point.change}",(0<this.change?"+":"")+a.numberFormat(this.change,g(this.series.tooltipOptions.changeDecimals,2)));return J.apply(this,[b])};A(f.prototype,"render",function(a){this.chart.is3d&&this.chart.is3d()||this.chart.polar||!this.xAxis||this.xAxis.isRadial||(!this.clipBox&&this.animate?(this.clipBox=b(this.chart.clipBox),this.clipBox.width=this.xAxis.len,this.clipBox.height=this.yAxis.len):this.chart[this.sharedClipKey]?this.chart[this.sharedClipKey].attr({width:this.xAxis.len,height:this.yAxis.len}):
this.clipBox&&(this.clipBox.width=this.xAxis.len,this.clipBox.height=this.yAxis.len));a.call(this)});A(r.prototype,"getSelectedPoints",function(a){var b=a.call(this);l(this.series,function(a){a.hasGroupedData&&(b=b.concat(v(a.points||[],function(a){return a.selected})))});return b});A(r.prototype,"update",function(a,c){"scrollbar"in c&&this.navigator&&(b(!0,this.options.scrollbar,c.scrollbar),this.navigator.update({},!1),delete c.scrollbar);return a.apply(this,Array.prototype.slice.call(arguments,
1))})})(L);return L});


/***/ }),

/***/ "./node_modules/is-buffer/index.js":
/***/ (function(module, exports) {

/*!
 * Determine if an object is a Buffer
 *
 * @author   Feross Aboukhadijeh <feross@feross.org> <http://feross.org>
 * @license  MIT
 */

// The _isBuffer check is for Safari 5-7 support, because it's missing
// Object.prototype.constructor. Remove this eventually
module.exports = function (obj) {
  return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer)
}

function isBuffer (obj) {
  return !!obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

// For Node v0.10 support. Remove this eventually.
function isSlowBuffer (obj) {
  return typeof obj.readFloatLE === 'function' && typeof obj.slice === 'function' && isBuffer(obj.slice(0, 0))
}


/***/ }),

/***/ "./node_modules/node-libs-browser/node_modules/timers-browserify/main.js":
/***/ (function(module, exports, __webpack_require__) {

var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__("./node_modules/setimmediate/setImmediate.js");
exports.setImmediate = setImmediate;
exports.clearImmediate = clearImmediate;


/***/ }),

/***/ "./node_modules/process/browser.js":
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/vue-loader/lib/component-normalizer.js":
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-606ea0a4\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/stockchart.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "header" }, [
      _c("div", { staticClass: "coin-toggle" }, [
        _c(
          "div",
          {
            staticClass: "single-toggle active",
            attrs: { "data-toggle": "bitcoin" },
            on: {
              click: function($event) {
                _vm.loadPrice("btc")
              }
            }
          },
          [
            _c("div", { staticClass: "icon icon-btc" }),
            _c("div", { staticClass: "is-hidden-mobile" }, [_vm._v("Bitcoin")])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "single-toggle",
            attrs: { "data-toggle": "ethereum" },
            on: {
              click: function($event) {
                _vm.loadPrice("eth")
              }
            }
          },
          [
            _c("div", { staticClass: "icon icon-eth" }),
            _c("div", { staticClass: "is-hidden-mobile" }, [_vm._v("Ethereum")])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "single-toggle",
            attrs: { "data-toggle": "litecoin" },
            on: {
              click: function($event) {
                _vm.loadPrice("ltc")
              }
            }
          },
          [
            _c("div", { staticClass: "icon icon-ltc" }),
            _c("div", { staticClass: "is-hidden-mobile" }, [_vm._v("Litecoin")])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "single-toggle",
            attrs: { "data-toggle": "ripple" },
            on: {
              click: function($event) {
                _vm.loadPrice("xrp")
              }
            }
          },
          [
            _c("div", { staticClass: "icon icon-rpl" }),
            _c("div", { staticClass: "is-hidden-mobile" }, [_vm._v("Ripple")])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "single-toggle",
            attrs: { "data-toggle": "power" },
            on: {
              click: function($event) {
                _vm.loadPrice("pwr")
              }
            }
          },
          [
            _c("div", { staticClass: "icon icon-pwr" }),
            _c("div", { staticClass: "is-hidden-mobile" }, [
              _vm._v("PowerLedger")
            ])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "graph" }, [
      _c("div", { attrs: { id: "stockChart-container" } }, [
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: this.spinner,
                expression: "this.spinner"
              }
            ]
          },
          [_c("a", { staticClass: "button is-loading" }, [_vm._v("Loading")])]
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-606ea0a4", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-63cb4824\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/piechart.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticStyle: {
        "min-width": "290px",
        height: "290px",
        "max-width": "600px",
        margin: "0 auto"
      },
      attrs: { id: "piechart-container" }
    },
    [
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: this.spinner,
              expression: "this.spinner"
            }
          ]
        },
        [_vm._v("loading....")]
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-63cb4824", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-91ccc3b0\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/radialbar.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "radial-progress-bar",
        {
          attrs: {
            diameter: _vm.diameter,
            "total-steps": _vm.total,
            "completed-steps": _vm.gain,
            "animate-speed": _vm.animateSpeed,
            "stroke-width": _vm.strokeWidth,
            "start-color": _vm.startColor,
            "stop-color": _vm.stopColor,
            "inner-stroke-color": _vm.innerStrokeColor,
            "timing-func": _vm.timingFunc
          }
        },
        [
          _c("p", [_vm._v(_vm._s(_vm.gain) + " %")]),
          _vm._v(" "),
          _c("p", [_vm._v(_vm._s(_vm.updatePhrase))])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-91ccc3b0", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-radial-progress/dist/vue-radial-progress.min.js":
/***/ (function(module, exports, __webpack_require__) {

!function(t,e){ true?module.exports=e():"function"==typeof define&&define.amd?define("RadialProgressBar",[],e):"object"==typeof exports?exports.RadialProgressBar=e():t.RadialProgressBar=e()}(this,function(){return function(t){function e(i){if(r[i])return r[i].exports;var n=r[i]={exports:{},id:i,loaded:!1};return t[i].call(n.exports,n,n.exports,e),n.loaded=!0,n.exports}var r={};return e.m=t,e.c=r,e.p="/dist/",e(0)}([function(t,e,r){"use strict";t.exports=r(5)},function(t,e){"use strict";Object.defineProperty(e,"__esModule",{value:!0}),e.default={props:{diameter:{type:Number,required:!1,default:200},totalSteps:{type:Number,required:!0,default:10},completedSteps:{type:Number,required:!0,default:0},startColor:{type:String,required:!1,default:"#bbff42"},stopColor:{type:String,required:!1,default:"#429321"},strokeWidth:{type:Number,required:!1,default:10},animateSpeed:{type:Number,required:!1,default:1e3},innerStrokeColor:{type:String,required:!1,default:"#323232"},fps:{type:Number,required:!1,default:60},timingFunc:{type:String,required:!1,default:"linear"}},data:function(){return{gradient:{fx:.99,fy:.5,cx:.5,cy:.5,r:.65},gradientAnimation:null,currentAngle:0,strokeDashoffset:0}},computed:{radius:function(){return this.diameter/2},circumference:function(){return Math.PI*this.innerCircleDiameter},stepSize:function(){return 0===this.totalSteps?0:100/this.totalSteps},finishedPercentage:function(){return this.stepSize*this.completedSteps},circleSlice:function(){return 2*Math.PI/this.totalSteps},animateSlice:function(){return this.circleSlice/this.totalPoints},innerCircleDiameter:function(){return this.diameter-2*this.strokeWidth},innerCircleRadius:function(){return this.innerCircleDiameter/2},totalPoints:function(){return this.animateSpeed/this.animationIncrements},animationIncrements:function(){return 1e3/this.fps},hasGradient:function(){return this.startColor!==this.stopColor},containerStyle:function(){return{height:this.diameter+"px",width:this.diameter+"px"}},progressStyle:function(){return{height:this.diameter+"px",width:this.diameter+"px",strokeWidth:this.strokeWidth+"px",strokeDashoffset:this.strokeDashoffset,transition:"stroke-dashoffset "+this.animateSpeed+"ms "+this.timingFunc}},strokeStyle:function(){return{height:this.diameter+"px",width:this.diameter+"px",strokeWidth:this.strokeWidth+"px"}},innerCircleStyle:function(){return{width:this.innerCircleDiameter+"px"}}},methods:{getStopPointsOfCircle:function(t){for(var e=[],r=0;r<t;r++){var i=this.circleSlice*r;e.push(this.getPointOfCircle(i))}return e},getPointOfCircle:function(t){var e=.5,r=e+e*Math.cos(t),i=e+e*Math.sin(t);return{x:r,y:i}},gotoPoint:function(){var t=this.getPointOfCircle(this.currentAngle);this.gradient.fx=t.x,this.gradient.fy=t.y},changeProgress:function(t){var e=this,r=t.isAnimate,i=void 0===r||r;if(this.strokeDashoffset=(100-this.finishedPercentage)/100*this.circumference,this.gradientAnimation&&clearInterval(this.gradientAnimation),!i)return void this.gotoNextStep();var n=(this.completedSteps-1)*this.circleSlice,s=(this.currentAngle-n)/this.animateSlice,o=Math.abs(s-this.totalPoints)/this.totalPoints,a=s<this.totalPoints;this.gradientAnimation=setInterval(function(){return a&&s>=e.totalPoints||!a&&s<e.totalPoints?void clearInterval(e.gradientAnimation):(e.currentAngle=n+e.animateSlice*s,e.gotoPoint(),void(s+=a?o:-o))},this.animationIncrements)},gotoNextStep:function(){this.currentAngle=this.completedSteps*this.circleSlice,this.gotoPoint()}},watch:{totalSteps:function(){this.changeProgress({isAnimate:!0})},completedSteps:function(){this.changeProgress({isAnimate:!0})},diameter:function(){this.changeProgress({isAnimate:!0})},strokeWidth:function(){this.changeProgress({isAnimate:!0})}},created:function(){this.changeProgress({isAnimate:!1})}}},function(t,e,r){e=t.exports=r(3)(),e.push([t.id,".radial-progress-container{position:relative}.radial-progress-inner{top:0;right:0;bottom:0;left:0;position:absolute;border-radius:50%;margin:0 auto;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}",""])},function(t,e){t.exports=function(){var t=[];return t.toString=function(){for(var t=[],e=0;e<this.length;e++){var r=this[e];r[2]?t.push("@media "+r[2]+"{"+r[1]+"}"):t.push(r[1])}return t.join("")},t.i=function(e,r){"string"==typeof e&&(e=[[null,e,""]]);for(var i={},n=0;n<this.length;n++){var s=this[n][0];"number"==typeof s&&(i[s]=!0)}for(n=0;n<e.length;n++){var o=e[n];"number"==typeof o[0]&&i[o[0]]||(r&&!o[2]?o[2]=r:r&&(o[2]="("+o[2]+") and ("+r+")"),t.push(o))}},t}},function(t,e){t.exports=" <div class=radial-progress-container :style=containerStyle> <div class=radial-progress-inner :style=innerCircleStyle> <slot></slot> </div> <svg class=radial-progress-bar :width=diameter :height=diameter version=1.1 xmlns=http://www.w3.org/2000/svg> <defs> <radialGradient :id=\"'radial-gradient' + _uid\" :fx=gradient.fx :fy=gradient.fy :cx=gradient.cx :cy=gradient.cy :r=gradient.r> <stop offset=30% :stop-color=startColor /> <stop offset=100% :stop-color=stopColor /> </radialGradient> </defs> <circle :r=innerCircleRadius :cx=radius :cy=radius fill=transparent :stroke=innerStrokeColor :stroke-dasharray=circumference stroke-dashoffset=0 stroke-linecap=round :style=strokeStyle></circle> <circle :transform=\"'rotate(270, ' + radius + ',' + radius + ')'\" :r=innerCircleRadius :cx=radius :cy=radius fill=transparent :stroke=\"'url(#radial-gradient' + _uid + ')'\" :stroke-dasharray=circumference :stroke-dashoffset=circumference stroke-linecap=round :style=progressStyle></circle> </svg> </div> "},function(t,e,r){var i,n,s={};r(7),i=r(1),n=r(4),t.exports=i||{},t.exports.__esModule&&(t.exports=t.exports.default);var o="function"==typeof t.exports?t.exports.options||(t.exports.options={}):t.exports;n&&(o.template=n),o.computed||(o.computed={}),Object.keys(s).forEach(function(t){var e=s[t];o.computed[t]=function(){return e}})},function(t,e,r){function i(t,e){for(var r=0;r<t.length;r++){var i=t[r],n=d[i.id];if(n){n.refs++;for(var s=0;s<n.parts.length;s++)n.parts[s](i.parts[s]);for(;s<i.parts.length;s++)n.parts.push(c(i.parts[s],e))}else{for(var o=[],s=0;s<i.parts.length;s++)o.push(c(i.parts[s],e));d[i.id]={id:i.id,refs:1,parts:o}}}}function n(t){for(var e=[],r={},i=0;i<t.length;i++){var n=t[i],s=n[0],o=n[1],a=n[2],c=n[3],u={css:o,media:a,sourceMap:c};r[s]?r[s].parts.push(u):e.push(r[s]={id:s,parts:[u]})}return e}function s(t,e){var r=h(),i=v[v.length-1];if("top"===t.insertAt)i?i.nextSibling?r.insertBefore(e,i.nextSibling):r.appendChild(e):r.insertBefore(e,r.firstChild),v.push(e);else{if("bottom"!==t.insertAt)throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");r.appendChild(e)}}function o(t){t.parentNode.removeChild(t);var e=v.indexOf(t);e>=0&&v.splice(e,1)}function a(t){var e=document.createElement("style");return e.type="text/css",s(t,e),e}function c(t,e){var r,i,n;if(e.singleton){var s=g++;r=m||(m=a(e)),i=u.bind(null,r,s,!1),n=u.bind(null,r,s,!0)}else r=a(e),i=l.bind(null,r),n=function(){o(r)};return i(t),function(e){if(e){if(e.css===t.css&&e.media===t.media&&e.sourceMap===t.sourceMap)return;i(t=e)}else n()}}function u(t,e,r,i){var n=r?"":i.css;if(t.styleSheet)t.styleSheet.cssText=x(e,n);else{var s=document.createTextNode(n),o=t.childNodes;o[e]&&t.removeChild(o[e]),o.length?t.insertBefore(s,o[e]):t.appendChild(s)}}function l(t,e){var r=e.css,i=e.media,n=e.sourceMap;if(i&&t.setAttribute("media",i),n&&(r+="\n/*# sourceURL="+n.sources[0]+" */",r+="\n/*# sourceMappingURL=data:application/json;base64,"+btoa(unescape(encodeURIComponent(JSON.stringify(n))))+" */"),t.styleSheet)t.styleSheet.cssText=r;else{for(;t.firstChild;)t.removeChild(t.firstChild);t.appendChild(document.createTextNode(r))}}var d={},f=function(t){var e;return function(){return"undefined"==typeof e&&(e=t.apply(this,arguments)),e}},p=f(function(){return/msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase())}),h=f(function(){return document.head||document.getElementsByTagName("head")[0]}),m=null,g=0,v=[];t.exports=function(t,e){e=e||{},"undefined"==typeof e.singleton&&(e.singleton=p()),"undefined"==typeof e.insertAt&&(e.insertAt="bottom");var r=n(t);return i(r,e),function(t){for(var s=[],o=0;o<r.length;o++){var a=r[o],c=d[a.id];c.refs--,s.push(c)}if(t){var u=n(t);i(u,e)}for(var o=0;o<s.length;o++){var c=s[o];if(0===c.refs){for(var l=0;l<c.parts.length;l++)c.parts[l]();delete d[c.id]}}}};var x=function(){var t=[];return function(e,r){return t[e]=r,t.filter(Boolean).join("\n")}}()},function(t,e,r){var i=r(2);"string"==typeof i&&(i=[[t.id,i,""]]);r(6)(i,{});i.locals&&(t.exports=i.locals)}])});

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/dashboard/components/radialbar.vue":
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__("./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/dashboard/components/radialbar.vue");
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__("./node_modules/vue-style-loader/lib/addStylesClient.js")("1aa319d5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./radialbar.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./radialbar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./node_modules/vue-style-loader/lib/addStylesClient.js":
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__("./node_modules/vue-style-loader/lib/listToStyles.js")

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssridKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ "./node_modules/vue-style-loader/lib/listToStyles.js":
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ "./node_modules/vue/dist/vue.common.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global, setImmediate) {/*!
 * Vue.js v2.5.13
 * (c) 2014-2017 Evan You
 * Released under the MIT License.
 */


/*  */

var emptyObject = Object.freeze({});

// these helpers produces better vm code in JS engines due to their
// explicitness and function inlining
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value e.g. [object Object]
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : typeof val === 'object'
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert a input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if a attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether the object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind, faster than native
 */
function bind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }
  // record original fn length
  boundFn._length = fn.length;
  return boundFn
}

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/)
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/**
 * Return same value
 */
var identity = function (_) { return _; };

/**
 * Generate a static keys string from compiler modules.
 */
function genStaticKeys (modules) {
  return modules.reduce(function (keys, m) {
    return keys.concat(m.staticKeys || [])
  }, []).join(',')
}

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var SSR_ATTR = 'data-server-rendered';

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured'
];

/*  */

var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = /[^\w.$]/;
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */


// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;

var supportsPassive = false;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
        /* istanbul ignore next */
        supportsPassive = true;
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = (function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm || {};
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm) {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */


var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.target) {
    Dep.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// the current target watcher being evaluated.
// this is globally unique because there could be only one
// watcher being evaluated at any time.
Dep.target = null;
var targetStack = [];

function pushTarget (_target) {
  if (Dep.target) { targetStack.push(Dep.target); }
  Dep.target = _target;
}

function popTarget () {
  Dep.target = targetStack.pop();
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode, deep) {
  var componentOptions = vnode.componentOptions;
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    vnode.children,
    vnode.text,
    vnode.elm,
    vnode.context,
    componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.isCloned = true;
  if (deep) {
    if (vnode.children) {
      cloned.children = cloneVNodes(vnode.children, true);
    }
    if (componentOptions && componentOptions.children) {
      componentOptions.children = cloneVNodes(componentOptions.children, true);
    }
  }
  return cloned
}

function cloneVNodes (vnodes, deep) {
  var len = vnodes.length;
  var res = new Array(len);
  for (var i = 0; i < len; i++) {
    res[i] = cloneVNode(vnodes[i], deep);
  }
  return res
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);[
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
].forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * By default, when a reactive property is set, the new value is
 * also converted to become reactive. However when passing down props,
 * we don't want to force conversion because the value may be a nested value
 * under a frozen data structure. Converting it would defeat the optimization.
 */
var observerState = {
  shouldConvert: true
};

/**
 * Observer class that are attached to each observed
 * object. Once attached, the observer converts target
 * object's property keys into getter/setters that
 * collect dependencies and dispatches updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    var augment = hasProto
      ? protoAugment
      : copyAugment;
    augment(value, arrayMethods, arrayKeys);
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through each property and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive(obj, keys[i], obj[keys[i]]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment an target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src, keys) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment an target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    observerState.shouldConvert &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.target) {
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ("development" !== 'production' && customSetter) {
        customSetter();
      }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
    "development" !== 'production' && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
    "development" !== 'production' && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;
  var keys = Object.keys(from);
  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (isPlainObject(toVal) && isPlainObject(fromVal)) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
      "development" !== 'production' && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  return childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
    "development" !== 'production' && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!/^[a-zA-Z][\w-]*$/.test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'can only contain alphanumeric characters and the hyphen, ' +
      'and must start with a letter.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def = dirs[key];
      if (typeof def === 'function') {
        dirs[key] = { bind: def, update: def };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);
  var extendsFrom = child.extends;
  if (extendsFrom) {
    parent = mergeOptions(parent, extendsFrom, vm);
  }
  if (child.mixins) {
    for (var i = 0, l = child.mixins.length; i < l; i++) {
      parent = mergeOptions(parent, child.mixins[i], vm);
    }
  }
  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ("development" !== 'production' && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */

function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // handle boolean props
  if (isType(Boolean, prop.type)) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (!isType(String, prop.type) && (value === '' || value === hyphenate(key))) {
      value = true;
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldConvert = observerState.shouldConvert;
    observerState.shouldConvert = true;
    observe(value);
    observerState.shouldConvert = prevShouldConvert;
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ("development" !== 'production' && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }
  if (!valid) {
    warn(
      "Invalid prop: type check failed for prop \"" + name + "\"." +
      " Expected " + (expectedTypes.map(capitalize).join(', ')) +
      ", got " + (toRawType(value)) + ".",
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isType (type, fn) {
  if (!Array.isArray(fn)) {
    return getType(fn) === getType(type)
  }
  for (var i = 0, len = fn.length; i < len; i++) {
    if (getType(fn[i]) === getType(type)) {
      return true
    }
  }
  /* istanbul ignore next */
  return false
}

/*  */

function handleError (err, vm, info) {
  if (vm) {
    var cur = vm;
    while ((cur = cur.$parent)) {
      var hooks = cur.$options.errorCaptured;
      if (hooks) {
        for (var i = 0; i < hooks.length; i++) {
          try {
            var capture = hooks[i].call(cur, err, vm, info) === false;
            if (capture) { return }
          } catch (e) {
            globalHandleError(e, cur, 'errorCaptured hook');
          }
        }
      }
    }
  }
  globalHandleError(err, vm, info);
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      logError(e, null, 'config.errorHandler');
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */
/* globals MessageChannel */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using both micro and macro tasks.
// In < 2.4 we used micro tasks everywhere, but there are some scenarios where
// micro tasks have too high a priority and fires in between supposedly
// sequential events (e.g. #4521, #6690) or even between bubbling of the same
// event (#6566). However, using macro tasks everywhere also has subtle problems
// when state is changed right before repaint (e.g. #6813, out-in transitions).
// Here we use micro task by default, but expose a way to force macro task when
// needed (e.g. in event handlers attached by v-on).
var microTimerFunc;
var macroTimerFunc;
var useMacroTask = false;

// Determine (macro) Task defer implementation.
// Technically setImmediate should be the ideal choice, but it's only available
// in IE. The only polyfill that consistently queues the callback after all DOM
// events triggered in the same loop is by using MessageChannel.
/* istanbul ignore if */
if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  macroTimerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else if (typeof MessageChannel !== 'undefined' && (
  isNative(MessageChannel) ||
  // PhantomJS
  MessageChannel.toString() === '[object MessageChannelConstructor]'
)) {
  var channel = new MessageChannel();
  var port = channel.port2;
  channel.port1.onmessage = flushCallbacks;
  macroTimerFunc = function () {
    port.postMessage(1);
  };
} else {
  /* istanbul ignore next */
  macroTimerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

// Determine MicroTask defer implementation.
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  microTimerFunc = function () {
    p.then(flushCallbacks);
    // in problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else {
  // fallback to macro
  microTimerFunc = macroTimerFunc;
}

/**
 * Wrap a function so that if any code inside triggers state change,
 * the changes are queued using a Task instead of a MicroTask.
 */
function withMacroTask (fn) {
  return fn._withTask || (fn._withTask = function () {
    useMacroTask = true;
    var res = fn.apply(null, arguments);
    useMacroTask = false;
    return res
  })
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    if (useMacroTask) {
      macroTimerFunc();
    } else {
      microTimerFunc();
    }
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      perf.clearMeasures(name);
    };
  }
}

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' &&
    Proxy.toString().match(/native code/);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) || key.charAt(0) === '_';
      if (!has && !isAllowed) {
        warnNonPresent(target, key);
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        warnNonPresent(target, key);
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val)) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        cloned[i].apply(null, arguments$1);
      }
    } else {
      // return handler return value for single handlers
      return fns.apply(null, arguments)
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  vm
) {
  var name, def, cur, old, event;
  for (name in on) {
    def = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    /* istanbul ignore if */
    if (isUndef(cur)) {
      "development" !== 'production' && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur);
      }
      add(event.name, cur, event.once, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

function mergeVNodeHook (def, hookKey, hook) {
  if (def instanceof VNode) {
    def = def.data.hook || (def.data.hook = {});
  }
  var invoker;
  var oldHook = def[hookKey];

  function wrappedHook () {
    hook.apply(this, arguments);
    // important: remove merged hook to ensure it's called only once
    // and prevent memory leak
    remove(invoker.fns, wrappedHook);
  }

  if (isUndef(oldHook)) {
    // no existing hook
    invoker = createFnInvoker([wrappedHook]);
  } else {
    /* istanbul ignore if */
    if (isDef(oldHook.fns) && isTrue(oldHook.merged)) {
      // already a merged invoker
      invoker = oldHook;
      invoker.fns.push(wrappedHook);
    } else {
      // existing plain hook
      invoker = createFnInvoker([oldHook, wrappedHook]);
    }
  }

  invoker.merged = true;
  def[hookKey] = invoker;
}

/*  */

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    return
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  return res
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor,
  context
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (isDef(factory.contexts)) {
    // already pending
    factory.contexts.push(context);
  } else {
    var contexts = factory.contexts = [context];
    var sync = true;

    var forceRender = function () {
      for (var i = 0, l = contexts.length; i < l; i++) {
        contexts[i].$forceUpdate();
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender();
      }
    });

    var reject = once(function (reason) {
      "development" !== 'production' && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender();
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (typeof res.then === 'function') {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isDef(res.component) && typeof res.component.then === 'function') {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            setTimeout(function () {
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender();
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          setTimeout(function () {
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : null
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn, once) {
  if (once) {
    target.$once(event, fn);
  } else {
    target.$on(event, fn);
  }
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var this$1 = this;

    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        this$1.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var this$1 = this;

    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        this$1.$off(event[i], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    if (fn) {
      // specific handler
      var cb;
      var i$1 = cbs.length;
      while (i$1--) {
        cb = cbs[i$1];
        if (cb === fn || cb.fn === fn) {
          cbs.splice(i$1, 1);
          break
        }
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      for (var i = 0, l = cbs.length; i < l; i++) {
        try {
          cbs[i].apply(vm, args);
        } catch (e) {
          handleError(e, vm, ("event handler for \"" + event + "\""));
        }
      }
    }
    return vm
  };
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  var slots = {};
  if (!children) {
    return slots
  }
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      (slots.default || (slots.default = [])).push(child);
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

function resolveScopedSlots (
  fns, // see flow/vnode
  res
) {
  res = res || {};
  for (var i = 0; i < fns.length; i++) {
    if (Array.isArray(fns[i])) {
      resolveScopedSlots(fns[i], res);
    } else {
      res[fns[i].key] = fns[i].fn;
    }
  }
  return res
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    if (vm._isMounted) {
      callHook(vm, 'beforeUpdate');
    }
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var prevActiveInstance = activeInstance;
    activeInstance = vm;
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(
        vm.$el, vnode, hydrating, false /* removeOnly */,
        vm.$options._parentElm,
        vm.$options._refElm
      );
      // no need for the ref nodes after initial patch
      // this prevents keeping a detached DOM tree in memory (#5851)
      vm.$options._parentElm = vm.$options._refElm = null;
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    activeInstance = prevActiveInstance;
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function mountComponent (
  vm,
  el,
  hydrating
) {
  vm.$el = el;
  if (!vm.$options.render) {
    vm.$options.render = createEmptyVNode;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  callHook(vm, 'beforeMount');

  var updateComponent;
  /* istanbul ignore if */
  if ("development" !== 'production' && config.performance && mark) {
    updateComponent = function () {
      var name = vm._name;
      var id = vm._uid;
      var startTag = "vue-perf-start:" + id;
      var endTag = "vue-perf-end:" + id;

      mark(startTag);
      var vnode = vm._render();
      mark(endTag);
      measure(("vue " + name + " render"), startTag, endTag);

      mark(startTag);
      vm._update(vnode, hydrating);
      mark(endTag);
      measure(("vue " + name + " patch"), startTag, endTag);
    };
  } else {
    updateComponent = function () {
      vm._update(vm._render(), hydrating);
    };
  }

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, null, true /* isRenderWatcher */);
  hydrating = false;

  // manually mounted instance, call mounted on self
  // mounted is called for render-created child components in its inserted hook
  if (vm.$vnode == null) {
    vm._isMounted = true;
    callHook(vm, 'mounted');
  }
  return vm
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren
  var hasChildren = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    parentVnode.data.scopedSlots || // has new scoped slots
    vm.$scopedSlots !== emptyObject // has old scoped slots
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = (parentVnode.data && parentVnode.data.attrs) || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    observerState.shouldConvert = false;
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      props[key] = validateProp(key, vm.$options.props, propsData, vm);
    }
    observerState.shouldConvert = true;
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }

  // update listeners
  if (listeners) {
    var oldListeners = vm.$options._parentListeners;
    vm.$options._parentListeners = listeners;
    updateComponentListeners(vm, listeners, oldListeners);
  }
  // resolve slots + force update if has children
  if (hasChildren) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  var handlers = vm.$options[hook];
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      try {
        handlers[i].call(vm);
      } catch (e) {
        handleError(e, vm, (hook + " hook"));
      }
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
}

/*  */


var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ("development" !== 'production' && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */

var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : '';
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = function () {};
      "development" !== 'production' && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
    var this$1 = this;

  var i = this.deps.length;
  while (i--) {
    var dep = this$1.deps[i];
    if (!this$1.newDepIds.has(dep.id)) {
      dep.removeSub(this$1);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
    var this$1 = this;

  var i = this.deps.length;
  while (i--) {
    this$1.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
    var this$1 = this;

  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this$1.deps[i].removeSub(this$1);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  observerState.shouldConvert = isRoot;
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive(props, key, value, function () {
        if (vm.$parent && !isUpdatingChildComponent) {
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {
      defineReactive(props, key, value);
    }
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  observerState.shouldConvert = true;
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
    "development" !== 'production' && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
      "development" !== 'production' && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ("development" !== 'production' && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : userDef;
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : userDef.get
      : noop;
    sharedPropertyDefinition.set = userDef.set
      ? userDef.set
      : noop;
  }
  if ("development" !== 'production' &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.target) {
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (methods[key] == null) {
        warn(
          "Method \"" + key + "\" has an undefined value in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = methods[key] == null ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  keyOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(keyOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function (newData) {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      cb.call(vm, watcher.value);
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    observerState.shouldConvert = false;
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {
        defineReactive(vm, key, result[key]);
      }
    });
    observerState.shouldConvert = true;
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject).filter(function (key) {
        /* istanbul ignore next */
        return Object.getOwnPropertyDescriptor(inject, key).enumerable
      })
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && provideKey in source._provided) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i);
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i);
    }
  } else if (isObject(val)) {
    keys = Object.keys(val);
    ret = new Array(keys.length);
    for (i = 0, l = keys.length; i < l; i++) {
      key = keys[i];
      ret[i] = render(val[key], key, i);
    }
  }
  if (isDef(ret)) {
    (ret)._isVList = true;
  }
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ("development" !== 'production' && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    nodes = scopedSlotFn(props) || fallback;
  } else {
    var slotNodes = this.$slots[name];
    // warn duplicate slot usage
    if (slotNodes) {
      if ("development" !== 'production' && slotNodes._rendered) {
        warn(
          "Duplicate presence of slot \"" + name + "\" found in the same render tree " +
          "- this will likely cause render errors.",
          this
        );
      }
      slotNodes._rendered = true;
    }
    nodes = slotNodes || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInAlias,
  eventKeyName
) {
  var keyCodes = config.keyCodes[key] || builtInAlias;
  if (keyCodes) {
    if (Array.isArray(keyCodes)) {
      return keyCodes.indexOf(eventKeyCode) === -1
    } else {
      return keyCodes !== eventKeyCode
    }
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
      "development" !== 'production' && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        if (!(key in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree by doing a shallow clone.
  if (tree && !isInFor) {
    return Array.isArray(tree)
      ? cloneVNodes(tree)
      : cloneVNode(tree)
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
      "development" !== 'production' && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var options = Ctor.options;
  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () { return resolveSlots(children, parent); };

  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm = Object.create(parent);
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = data.scopedSlots || emptyObject;
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    vnode.fnContext = contextVm;
    vnode.fnOptions = options;
    if (data.slot) {
      (vnode.data || (vnode.data = {})).slot = data.slot;
    }
  }

  return vnode
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */




// Register the component hook to weex native render engine.
// The hook will be triggered by native, not javascript.


// Updates the state of the component to weex native render engine.

/*  */

// https://github.com/Hanks10100/weex-native-directive/tree/master/component

// listening on native callback

/*  */

/*  */

// hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (
    vnode,
    hydrating,
    parentElm,
    refElm
  ) {
    if (!vnode.componentInstance || vnode.componentInstance._isDestroyed) {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance,
        parentElm,
        refElm
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    } else if (vnode.data.keepAlive) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor, context);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag);

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // merge component management hooks onto the placeholder node
  mergeHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  // Weex specific: invoke recycle-list optimized @render function for
  // extracting cell-slot template.
  // https://github.com/Hanks10100/weex-native-directive/tree/master/component
  /* istanbul ignore if */
  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent, // activeInstance in lifecycle state
  parentElm,
  refElm
) {
  var options = {
    _isComponent: true,
    parent: parent,
    _parentVnode: vnode,
    _parentElm: parentElm || null,
    _refElm: refElm || null
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function mergeHooks (data) {
  if (!data.hook) {
    data.hook = {};
  }
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var fromParent = data.hook[key];
    var ours = componentVNodeHooks[key];
    data.hook[key] = fromParent ? mergeHook$1(ours, fromParent) : ours;
  }
}

function mergeHook$1 (one, two) {
  return function (a, b, c, d) {
    one(a, b, c, d);
    two(a, b, c, d);
  }
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input';(data.props || (data.props = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  if (isDef(on[event])) {
    on[event] = [data.model.callback].concat(on[event]);
  } else {
    on[event] = data.model.callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
    "development" !== 'production' && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ("development" !== 'production' &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if (isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (isDef(vnode)) {
    if (ns) { applyNS(vnode, ns); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (isUndef(child.ns) || isTrue(force))) {
        applyNS(child, ns, force);
      }
    }
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {
    defineReactive(vm, '$attrs', parentData && parentData.attrs || emptyObject, null, true);
    defineReactive(vm, '$listeners', options._parentListeners || emptyObject, null, true);
  }
}

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (vm._isMounted) {
      // if the parent didn't update, the slot nodes will be the ones from
      // last render. They need to be cloned to ensure "freshness" for this render.
      for (var key in vm.$slots) {
        var slot = vm.$slots[key];
        // _rendered is a flag added by renderSlot, but may not be present
        // if the slot is passed from manually written render functions
        if (slot._rendered || (slot[0] && slot[0].elm)) {
          vm.$slots[key] = cloneVNodes(slot, true /* deep */);
        }
      }
    }

    vm.$scopedSlots = (_parentVnode && _parentVnode.data.scopedSlots) || emptyObject;

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if (true) {
        if (vm.$options.renderError) {
          try {
            vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
          } catch (e) {
            handleError(e, vm, "renderError");
            vnode = vm._vnode;
          }
        } else {
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ("development" !== 'production' && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

var uid$1 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$1++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ("development" !== 'production' && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {
      vm._renderProxy = vm;
    }
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    initInjections(vm); // resolve injections before data/props
    initState(vm);
    initProvide(vm); // resolve provide after data/props
    callHook(vm, 'created');

    /* istanbul ignore if */
    if ("development" !== 'production' && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;
  opts._parentElm = options._parentElm;
  opts._refElm = options._refElm;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var extended = Ctor.extendOptions;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = dedupe(latest[key], extended[key], sealed[key]);
    }
  }
  return modified
}

function dedupe (latest, extended, sealed) {
  // compare latest and sealed to ensure lifecycle hooks won't be duplicated
  // between merges
  if (Array.isArray(latest)) {
    var res = [];
    sealed = Array.isArray(sealed) ? sealed : [sealed];
    extended = Array.isArray(extended) ? extended : [extended];
    for (var i = 0; i < latest.length; i++) {
      // push original options and not sealed options to exclude duplicated options
      if (extended.indexOf(latest[i]) >= 0 || sealed.indexOf(latest[i]) < 0) {
        res.push(latest[i]);
      }
    }
    return res
  } else {
    return latest
  }
}

function Vue$3 (options) {
  if ("development" !== 'production' &&
    !(this instanceof Vue$3)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue$3);
stateMixin(Vue$3);
eventsMixin(Vue$3);
lifecycleMixin(Vue$3);
renderMixin(Vue$3);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ("development" !== 'production' && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ("development" !== 'production' && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */

function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    var this$1 = this;

    for (var key in this$1.cache) {
      pruneCacheEntry(this$1.cache, key, this$1.keys);
    }
  },

  watch: {
    include: function include (val) {
      pruneCache(this, function (name) { return matches(val, name); });
    },
    exclude: function exclude (val) {
      pruneCache(this, function (name) { return !matches(val, name); });
    }
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue$3);

Object.defineProperty(Vue$3.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue$3.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

Vue$3.version = '2.5.13';

/*  */

// these are reserved for web because they are directly compiled away
// during template compilation
var isReservedAttr = makeMap('style,class');

// attributes that should be using props for binding
var acceptValue = makeMap('input,textarea,option,select,progress');
var mustUseProp = function (tag, type, attr) {
  return (
    (attr === 'value' && acceptValue(tag)) && type !== 'button' ||
    (attr === 'selected' && tag === 'option') ||
    (attr === 'checked' && tag === 'input') ||
    (attr === 'muted' && tag === 'video')
  )
};

var isEnumeratedAttr = makeMap('contenteditable,draggable,spellcheck');

var isBooleanAttr = makeMap(
  'allowfullscreen,async,autofocus,autoplay,checked,compact,controls,declare,' +
  'default,defaultchecked,defaultmuted,defaultselected,defer,disabled,' +
  'enabled,formnovalidate,hidden,indeterminate,inert,ismap,itemscope,loop,multiple,' +
  'muted,nohref,noresize,noshade,novalidate,nowrap,open,pauseonexit,readonly,' +
  'required,reversed,scoped,seamless,selected,sortable,translate,' +
  'truespeed,typemustmatch,visible'
);

var xlinkNS = 'http://www.w3.org/1999/xlink';

var isXlink = function (name) {
  return name.charAt(5) === ':' && name.slice(0, 5) === 'xlink'
};

var getXlinkProp = function (name) {
  return isXlink(name) ? name.slice(6, name.length) : ''
};

var isFalsyAttrValue = function (val) {
  return val == null || val === false
};

/*  */

function genClassForVnode (vnode) {
  var data = vnode.data;
  var parentNode = vnode;
  var childNode = vnode;
  while (isDef(childNode.componentInstance)) {
    childNode = childNode.componentInstance._vnode;
    if (childNode && childNode.data) {
      data = mergeClassData(childNode.data, data);
    }
  }
  while (isDef(parentNode = parentNode.parent)) {
    if (parentNode && parentNode.data) {
      data = mergeClassData(data, parentNode.data);
    }
  }
  return renderClass(data.staticClass, data.class)
}

function mergeClassData (child, parent) {
  return {
    staticClass: concat(child.staticClass, parent.staticClass),
    class: isDef(child.class)
      ? [child.class, parent.class]
      : parent.class
  }
}

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var namespaceMap = {
  svg: 'http://www.w3.org/2000/svg',
  math: 'http://www.w3.org/1998/Math/MathML'
};

var isHTMLTag = makeMap(
  'html,body,base,head,link,meta,style,title,' +
  'address,article,aside,footer,header,h1,h2,h3,h4,h5,h6,hgroup,nav,section,' +
  'div,dd,dl,dt,figcaption,figure,picture,hr,img,li,main,ol,p,pre,ul,' +
  'a,b,abbr,bdi,bdo,br,cite,code,data,dfn,em,i,kbd,mark,q,rp,rt,rtc,ruby,' +
  's,samp,small,span,strong,sub,sup,time,u,var,wbr,area,audio,map,track,video,' +
  'embed,object,param,source,canvas,script,noscript,del,ins,' +
  'caption,col,colgroup,table,thead,tbody,td,th,tr,' +
  'button,datalist,fieldset,form,input,label,legend,meter,optgroup,option,' +
  'output,progress,select,textarea,' +
  'details,dialog,menu,menuitem,summary,' +
  'content,element,shadow,template,blockquote,iframe,tfoot'
);

// this map is intentionally selective, only covering SVG elements that may
// contain child elements.
var isSVG = makeMap(
  'svg,animate,circle,clippath,cursor,defs,desc,ellipse,filter,font-face,' +
  'foreignObject,g,glyph,image,line,marker,mask,missing-glyph,path,pattern,' +
  'polygon,polyline,rect,switch,symbol,text,textpath,tspan,use,view',
  true
);

var isPreTag = function (tag) { return tag === 'pre'; };

var isReservedTag = function (tag) {
  return isHTMLTag(tag) || isSVG(tag)
};

function getTagNamespace (tag) {
  if (isSVG(tag)) {
    return 'svg'
  }
  // basic support for MathML
  // note it doesn't support other MathML elements being component roots
  if (tag === 'math') {
    return 'math'
  }
}

var unknownElementCache = Object.create(null);
function isUnknownElement (tag) {
  /* istanbul ignore if */
  if (!inBrowser) {
    return true
  }
  if (isReservedTag(tag)) {
    return false
  }
  tag = tag.toLowerCase();
  /* istanbul ignore if */
  if (unknownElementCache[tag] != null) {
    return unknownElementCache[tag]
  }
  var el = document.createElement(tag);
  if (tag.indexOf('-') > -1) {
    // http://stackoverflow.com/a/28210364/1070244
    return (unknownElementCache[tag] = (
      el.constructor === window.HTMLUnknownElement ||
      el.constructor === window.HTMLElement
    ))
  } else {
    return (unknownElementCache[tag] = /HTMLUnknownElement/.test(el.toString()))
  }
}

var isTextInputType = makeMap('text,number,password,search,email,tel,url');

/*  */

/**
 * Query an element selector if it's not an element already.
 */
function query (el) {
  if (typeof el === 'string') {
    var selected = document.querySelector(el);
    if (!selected) {
      "development" !== 'production' && warn(
        'Cannot find element: ' + el
      );
      return document.createElement('div')
    }
    return selected
  } else {
    return el
  }
}

/*  */

function createElement$1 (tagName, vnode) {
  var elm = document.createElement(tagName);
  if (tagName !== 'select') {
    return elm
  }
  // false or null will remove the attribute but undefined will not
  if (vnode.data && vnode.data.attrs && vnode.data.attrs.multiple !== undefined) {
    elm.setAttribute('multiple', 'multiple');
  }
  return elm
}

function createElementNS (namespace, tagName) {
  return document.createElementNS(namespaceMap[namespace], tagName)
}

function createTextNode (text) {
  return document.createTextNode(text)
}

function createComment (text) {
  return document.createComment(text)
}

function insertBefore (parentNode, newNode, referenceNode) {
  parentNode.insertBefore(newNode, referenceNode);
}

function removeChild (node, child) {
  node.removeChild(child);
}

function appendChild (node, child) {
  node.appendChild(child);
}

function parentNode (node) {
  return node.parentNode
}

function nextSibling (node) {
  return node.nextSibling
}

function tagName (node) {
  return node.tagName
}

function setTextContent (node, text) {
  node.textContent = text;
}

function setAttribute (node, key, val) {
  node.setAttribute(key, val);
}


var nodeOps = Object.freeze({
	createElement: createElement$1,
	createElementNS: createElementNS,
	createTextNode: createTextNode,
	createComment: createComment,
	insertBefore: insertBefore,
	removeChild: removeChild,
	appendChild: appendChild,
	parentNode: parentNode,
	nextSibling: nextSibling,
	tagName: tagName,
	setTextContent: setTextContent,
	setAttribute: setAttribute
});

/*  */

var ref = {
  create: function create (_, vnode) {
    registerRef(vnode);
  },
  update: function update (oldVnode, vnode) {
    if (oldVnode.data.ref !== vnode.data.ref) {
      registerRef(oldVnode, true);
      registerRef(vnode);
    }
  },
  destroy: function destroy (vnode) {
    registerRef(vnode, true);
  }
};

function registerRef (vnode, isRemoval) {
  var key = vnode.data.ref;
  if (!key) { return }

  var vm = vnode.context;
  var ref = vnode.componentInstance || vnode.elm;
  var refs = vm.$refs;
  if (isRemoval) {
    if (Array.isArray(refs[key])) {
      remove(refs[key], ref);
    } else if (refs[key] === ref) {
      refs[key] = undefined;
    }
  } else {
    if (vnode.data.refInFor) {
      if (!Array.isArray(refs[key])) {
        refs[key] = [ref];
      } else if (refs[key].indexOf(ref) < 0) {
        // $flow-disable-line
        refs[key].push(ref);
      }
    } else {
      refs[key] = ref;
    }
  }
}

/**
 * Virtual DOM patching algorithm based on Snabbdom by
 * Simon Friis Vindum (@paldepind)
 * Licensed under the MIT License
 * https://github.com/paldepind/snabbdom/blob/master/LICENSE
 *
 * modified by Evan You (@yyx990803)
 *
 * Not type-checking this because this file is perf-critical and the cost
 * of making flow understand it is not worth it.
 */

var emptyNode = new VNode('', {}, []);

var hooks = ['create', 'activate', 'update', 'remove', 'destroy'];

function sameVnode (a, b) {
  return (
    a.key === b.key && (
      (
        a.tag === b.tag &&
        a.isComment === b.isComment &&
        isDef(a.data) === isDef(b.data) &&
        sameInputType(a, b)
      ) || (
        isTrue(a.isAsyncPlaceholder) &&
        a.asyncFactory === b.asyncFactory &&
        isUndef(b.asyncFactory.error)
      )
    )
  )
}

function sameInputType (a, b) {
  if (a.tag !== 'input') { return true }
  var i;
  var typeA = isDef(i = a.data) && isDef(i = i.attrs) && i.type;
  var typeB = isDef(i = b.data) && isDef(i = i.attrs) && i.type;
  return typeA === typeB || isTextInputType(typeA) && isTextInputType(typeB)
}

function createKeyToOldIdx (children, beginIdx, endIdx) {
  var i, key;
  var map = {};
  for (i = beginIdx; i <= endIdx; ++i) {
    key = children[i].key;
    if (isDef(key)) { map[key] = i; }
  }
  return map
}

function createPatchFunction (backend) {
  var i, j;
  var cbs = {};

  var modules = backend.modules;
  var nodeOps = backend.nodeOps;

  for (i = 0; i < hooks.length; ++i) {
    cbs[hooks[i]] = [];
    for (j = 0; j < modules.length; ++j) {
      if (isDef(modules[j][hooks[i]])) {
        cbs[hooks[i]].push(modules[j][hooks[i]]);
      }
    }
  }

  function emptyNodeAt (elm) {
    return new VNode(nodeOps.tagName(elm).toLowerCase(), {}, [], undefined, elm)
  }

  function createRmCb (childElm, listeners) {
    function remove () {
      if (--remove.listeners === 0) {
        removeNode(childElm);
      }
    }
    remove.listeners = listeners;
    return remove
  }

  function removeNode (el) {
    var parent = nodeOps.parentNode(el);
    // element may have already been removed due to v-html / v-text
    if (isDef(parent)) {
      nodeOps.removeChild(parent, el);
    }
  }

  function isUnknownElement$$1 (vnode, inVPre) {
    return (
      !inVPre &&
      !vnode.ns &&
      !(
        config.ignoredElements.length &&
        config.ignoredElements.some(function (ignore) {
          return isRegExp(ignore)
            ? ignore.test(vnode.tag)
            : ignore === vnode.tag
        })
      ) &&
      config.isUnknownElement(vnode.tag)
    )
  }

  var creatingElmInVPre = 0;
  function createElm (vnode, insertedVnodeQueue, parentElm, refElm, nested) {
    vnode.isRootInsert = !nested; // for transition enter check
    if (createComponent(vnode, insertedVnodeQueue, parentElm, refElm)) {
      return
    }

    var data = vnode.data;
    var children = vnode.children;
    var tag = vnode.tag;
    if (isDef(tag)) {
      if (true) {
        if (data && data.pre) {
          creatingElmInVPre++;
        }
        if (isUnknownElement$$1(vnode, creatingElmInVPre)) {
          warn(
            'Unknown custom element: <' + tag + '> - did you ' +
            'register the component correctly? For recursive components, ' +
            'make sure to provide the "name" option.',
            vnode.context
          );
        }
      }
      vnode.elm = vnode.ns
        ? nodeOps.createElementNS(vnode.ns, tag)
        : nodeOps.createElement(tag, vnode);
      setScope(vnode);

      /* istanbul ignore if */
      {
        createChildren(vnode, children, insertedVnodeQueue);
        if (isDef(data)) {
          invokeCreateHooks(vnode, insertedVnodeQueue);
        }
        insert(parentElm, vnode.elm, refElm);
      }

      if ("development" !== 'production' && data && data.pre) {
        creatingElmInVPre--;
      }
    } else if (isTrue(vnode.isComment)) {
      vnode.elm = nodeOps.createComment(vnode.text);
      insert(parentElm, vnode.elm, refElm);
    } else {
      vnode.elm = nodeOps.createTextNode(vnode.text);
      insert(parentElm, vnode.elm, refElm);
    }
  }

  function createComponent (vnode, insertedVnodeQueue, parentElm, refElm) {
    var i = vnode.data;
    if (isDef(i)) {
      var isReactivated = isDef(vnode.componentInstance) && i.keepAlive;
      if (isDef(i = i.hook) && isDef(i = i.init)) {
        i(vnode, false /* hydrating */, parentElm, refElm);
      }
      // after calling the init hook, if the vnode is a child component
      // it should've created a child instance and mounted it. the child
      // component also has set the placeholder vnode's elm.
      // in that case we can just return the element and be done.
      if (isDef(vnode.componentInstance)) {
        initComponent(vnode, insertedVnodeQueue);
        if (isTrue(isReactivated)) {
          reactivateComponent(vnode, insertedVnodeQueue, parentElm, refElm);
        }
        return true
      }
    }
  }

  function initComponent (vnode, insertedVnodeQueue) {
    if (isDef(vnode.data.pendingInsert)) {
      insertedVnodeQueue.push.apply(insertedVnodeQueue, vnode.data.pendingInsert);
      vnode.data.pendingInsert = null;
    }
    vnode.elm = vnode.componentInstance.$el;
    if (isPatchable(vnode)) {
      invokeCreateHooks(vnode, insertedVnodeQueue);
      setScope(vnode);
    } else {
      // empty component root.
      // skip all element-related modules except for ref (#3455)
      registerRef(vnode);
      // make sure to invoke the insert hook
      insertedVnodeQueue.push(vnode);
    }
  }

  function reactivateComponent (vnode, insertedVnodeQueue, parentElm, refElm) {
    var i;
    // hack for #4339: a reactivated component with inner transition
    // does not trigger because the inner node's created hooks are not called
    // again. It's not ideal to involve module-specific logic in here but
    // there doesn't seem to be a better way to do it.
    var innerNode = vnode;
    while (innerNode.componentInstance) {
      innerNode = innerNode.componentInstance._vnode;
      if (isDef(i = innerNode.data) && isDef(i = i.transition)) {
        for (i = 0; i < cbs.activate.length; ++i) {
          cbs.activate[i](emptyNode, innerNode);
        }
        insertedVnodeQueue.push(innerNode);
        break
      }
    }
    // unlike a newly created component,
    // a reactivated keep-alive component doesn't insert itself
    insert(parentElm, vnode.elm, refElm);
  }

  function insert (parent, elm, ref$$1) {
    if (isDef(parent)) {
      if (isDef(ref$$1)) {
        if (ref$$1.parentNode === parent) {
          nodeOps.insertBefore(parent, elm, ref$$1);
        }
      } else {
        nodeOps.appendChild(parent, elm);
      }
    }
  }

  function createChildren (vnode, children, insertedVnodeQueue) {
    if (Array.isArray(children)) {
      if (true) {
        checkDuplicateKeys(children);
      }
      for (var i = 0; i < children.length; ++i) {
        createElm(children[i], insertedVnodeQueue, vnode.elm, null, true);
      }
    } else if (isPrimitive(vnode.text)) {
      nodeOps.appendChild(vnode.elm, nodeOps.createTextNode(String(vnode.text)));
    }
  }

  function isPatchable (vnode) {
    while (vnode.componentInstance) {
      vnode = vnode.componentInstance._vnode;
    }
    return isDef(vnode.tag)
  }

  function invokeCreateHooks (vnode, insertedVnodeQueue) {
    for (var i$1 = 0; i$1 < cbs.create.length; ++i$1) {
      cbs.create[i$1](emptyNode, vnode);
    }
    i = vnode.data.hook; // Reuse variable
    if (isDef(i)) {
      if (isDef(i.create)) { i.create(emptyNode, vnode); }
      if (isDef(i.insert)) { insertedVnodeQueue.push(vnode); }
    }
  }

  // set scope id attribute for scoped CSS.
  // this is implemented as a special case to avoid the overhead
  // of going through the normal attribute patching process.
  function setScope (vnode) {
    var i;
    if (isDef(i = vnode.fnScopeId)) {
      nodeOps.setAttribute(vnode.elm, i, '');
    } else {
      var ancestor = vnode;
      while (ancestor) {
        if (isDef(i = ancestor.context) && isDef(i = i.$options._scopeId)) {
          nodeOps.setAttribute(vnode.elm, i, '');
        }
        ancestor = ancestor.parent;
      }
    }
    // for slot content they should also get the scopeId from the host instance.
    if (isDef(i = activeInstance) &&
      i !== vnode.context &&
      i !== vnode.fnContext &&
      isDef(i = i.$options._scopeId)
    ) {
      nodeOps.setAttribute(vnode.elm, i, '');
    }
  }

  function addVnodes (parentElm, refElm, vnodes, startIdx, endIdx, insertedVnodeQueue) {
    for (; startIdx <= endIdx; ++startIdx) {
      createElm(vnodes[startIdx], insertedVnodeQueue, parentElm, refElm);
    }
  }

  function invokeDestroyHook (vnode) {
    var i, j;
    var data = vnode.data;
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.destroy)) { i(vnode); }
      for (i = 0; i < cbs.destroy.length; ++i) { cbs.destroy[i](vnode); }
    }
    if (isDef(i = vnode.children)) {
      for (j = 0; j < vnode.children.length; ++j) {
        invokeDestroyHook(vnode.children[j]);
      }
    }
  }

  function removeVnodes (parentElm, vnodes, startIdx, endIdx) {
    for (; startIdx <= endIdx; ++startIdx) {
      var ch = vnodes[startIdx];
      if (isDef(ch)) {
        if (isDef(ch.tag)) {
          removeAndInvokeRemoveHook(ch);
          invokeDestroyHook(ch);
        } else { // Text node
          removeNode(ch.elm);
        }
      }
    }
  }

  function removeAndInvokeRemoveHook (vnode, rm) {
    if (isDef(rm) || isDef(vnode.data)) {
      var i;
      var listeners = cbs.remove.length + 1;
      if (isDef(rm)) {
        // we have a recursively passed down rm callback
        // increase the listeners count
        rm.listeners += listeners;
      } else {
        // directly removing
        rm = createRmCb(vnode.elm, listeners);
      }
      // recursively invoke hooks on child component root node
      if (isDef(i = vnode.componentInstance) && isDef(i = i._vnode) && isDef(i.data)) {
        removeAndInvokeRemoveHook(i, rm);
      }
      for (i = 0; i < cbs.remove.length; ++i) {
        cbs.remove[i](vnode, rm);
      }
      if (isDef(i = vnode.data.hook) && isDef(i = i.remove)) {
        i(vnode, rm);
      } else {
        rm();
      }
    } else {
      removeNode(vnode.elm);
    }
  }

  function updateChildren (parentElm, oldCh, newCh, insertedVnodeQueue, removeOnly) {
    var oldStartIdx = 0;
    var newStartIdx = 0;
    var oldEndIdx = oldCh.length - 1;
    var oldStartVnode = oldCh[0];
    var oldEndVnode = oldCh[oldEndIdx];
    var newEndIdx = newCh.length - 1;
    var newStartVnode = newCh[0];
    var newEndVnode = newCh[newEndIdx];
    var oldKeyToIdx, idxInOld, vnodeToMove, refElm;

    // removeOnly is a special flag used only by <transition-group>
    // to ensure removed elements stay in correct relative positions
    // during leaving transitions
    var canMove = !removeOnly;

    if (true) {
      checkDuplicateKeys(newCh);
    }

    while (oldStartIdx <= oldEndIdx && newStartIdx <= newEndIdx) {
      if (isUndef(oldStartVnode)) {
        oldStartVnode = oldCh[++oldStartIdx]; // Vnode has been moved left
      } else if (isUndef(oldEndVnode)) {
        oldEndVnode = oldCh[--oldEndIdx];
      } else if (sameVnode(oldStartVnode, newStartVnode)) {
        patchVnode(oldStartVnode, newStartVnode, insertedVnodeQueue);
        oldStartVnode = oldCh[++oldStartIdx];
        newStartVnode = newCh[++newStartIdx];
      } else if (sameVnode(oldEndVnode, newEndVnode)) {
        patchVnode(oldEndVnode, newEndVnode, insertedVnodeQueue);
        oldEndVnode = oldCh[--oldEndIdx];
        newEndVnode = newCh[--newEndIdx];
      } else if (sameVnode(oldStartVnode, newEndVnode)) { // Vnode moved right
        patchVnode(oldStartVnode, newEndVnode, insertedVnodeQueue);
        canMove && nodeOps.insertBefore(parentElm, oldStartVnode.elm, nodeOps.nextSibling(oldEndVnode.elm));
        oldStartVnode = oldCh[++oldStartIdx];
        newEndVnode = newCh[--newEndIdx];
      } else if (sameVnode(oldEndVnode, newStartVnode)) { // Vnode moved left
        patchVnode(oldEndVnode, newStartVnode, insertedVnodeQueue);
        canMove && nodeOps.insertBefore(parentElm, oldEndVnode.elm, oldStartVnode.elm);
        oldEndVnode = oldCh[--oldEndIdx];
        newStartVnode = newCh[++newStartIdx];
      } else {
        if (isUndef(oldKeyToIdx)) { oldKeyToIdx = createKeyToOldIdx(oldCh, oldStartIdx, oldEndIdx); }
        idxInOld = isDef(newStartVnode.key)
          ? oldKeyToIdx[newStartVnode.key]
          : findIdxInOld(newStartVnode, oldCh, oldStartIdx, oldEndIdx);
        if (isUndef(idxInOld)) { // New element
          createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm);
        } else {
          vnodeToMove = oldCh[idxInOld];
          if (sameVnode(vnodeToMove, newStartVnode)) {
            patchVnode(vnodeToMove, newStartVnode, insertedVnodeQueue);
            oldCh[idxInOld] = undefined;
            canMove && nodeOps.insertBefore(parentElm, vnodeToMove.elm, oldStartVnode.elm);
          } else {
            // same key but different element. treat as new element
            createElm(newStartVnode, insertedVnodeQueue, parentElm, oldStartVnode.elm);
          }
        }
        newStartVnode = newCh[++newStartIdx];
      }
    }
    if (oldStartIdx > oldEndIdx) {
      refElm = isUndef(newCh[newEndIdx + 1]) ? null : newCh[newEndIdx + 1].elm;
      addVnodes(parentElm, refElm, newCh, newStartIdx, newEndIdx, insertedVnodeQueue);
    } else if (newStartIdx > newEndIdx) {
      removeVnodes(parentElm, oldCh, oldStartIdx, oldEndIdx);
    }
  }

  function checkDuplicateKeys (children) {
    var seenKeys = {};
    for (var i = 0; i < children.length; i++) {
      var vnode = children[i];
      var key = vnode.key;
      if (isDef(key)) {
        if (seenKeys[key]) {
          warn(
            ("Duplicate keys detected: '" + key + "'. This may cause an update error."),
            vnode.context
          );
        } else {
          seenKeys[key] = true;
        }
      }
    }
  }

  function findIdxInOld (node, oldCh, start, end) {
    for (var i = start; i < end; i++) {
      var c = oldCh[i];
      if (isDef(c) && sameVnode(node, c)) { return i }
    }
  }

  function patchVnode (oldVnode, vnode, insertedVnodeQueue, removeOnly) {
    if (oldVnode === vnode) {
      return
    }

    var elm = vnode.elm = oldVnode.elm;

    if (isTrue(oldVnode.isAsyncPlaceholder)) {
      if (isDef(vnode.asyncFactory.resolved)) {
        hydrate(oldVnode.elm, vnode, insertedVnodeQueue);
      } else {
        vnode.isAsyncPlaceholder = true;
      }
      return
    }

    // reuse element for static trees.
    // note we only do this if the vnode is cloned -
    // if the new node is not cloned it means the render functions have been
    // reset by the hot-reload-api and we need to do a proper re-render.
    if (isTrue(vnode.isStatic) &&
      isTrue(oldVnode.isStatic) &&
      vnode.key === oldVnode.key &&
      (isTrue(vnode.isCloned) || isTrue(vnode.isOnce))
    ) {
      vnode.componentInstance = oldVnode.componentInstance;
      return
    }

    var i;
    var data = vnode.data;
    if (isDef(data) && isDef(i = data.hook) && isDef(i = i.prepatch)) {
      i(oldVnode, vnode);
    }

    var oldCh = oldVnode.children;
    var ch = vnode.children;
    if (isDef(data) && isPatchable(vnode)) {
      for (i = 0; i < cbs.update.length; ++i) { cbs.update[i](oldVnode, vnode); }
      if (isDef(i = data.hook) && isDef(i = i.update)) { i(oldVnode, vnode); }
    }
    if (isUndef(vnode.text)) {
      if (isDef(oldCh) && isDef(ch)) {
        if (oldCh !== ch) { updateChildren(elm, oldCh, ch, insertedVnodeQueue, removeOnly); }
      } else if (isDef(ch)) {
        if (isDef(oldVnode.text)) { nodeOps.setTextContent(elm, ''); }
        addVnodes(elm, null, ch, 0, ch.length - 1, insertedVnodeQueue);
      } else if (isDef(oldCh)) {
        removeVnodes(elm, oldCh, 0, oldCh.length - 1);
      } else if (isDef(oldVnode.text)) {
        nodeOps.setTextContent(elm, '');
      }
    } else if (oldVnode.text !== vnode.text) {
      nodeOps.setTextContent(elm, vnode.text);
    }
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.postpatch)) { i(oldVnode, vnode); }
    }
  }

  function invokeInsertHook (vnode, queue, initial) {
    // delay insert hooks for component root nodes, invoke them after the
    // element is really inserted
    if (isTrue(initial) && isDef(vnode.parent)) {
      vnode.parent.data.pendingInsert = queue;
    } else {
      for (var i = 0; i < queue.length; ++i) {
        queue[i].data.hook.insert(queue[i]);
      }
    }
  }

  var hydrationBailed = false;
  // list of modules that can skip create hook during hydration because they
  // are already rendered on the client or has no need for initialization
  // Note: style is excluded because it relies on initial clone for future
  // deep updates (#7063).
  var isRenderedModule = makeMap('attrs,class,staticClass,staticStyle,key');

  // Note: this is a browser-only function so we can assume elms are DOM nodes.
  function hydrate (elm, vnode, insertedVnodeQueue, inVPre) {
    var i;
    var tag = vnode.tag;
    var data = vnode.data;
    var children = vnode.children;
    inVPre = inVPre || (data && data.pre);
    vnode.elm = elm;

    if (isTrue(vnode.isComment) && isDef(vnode.asyncFactory)) {
      vnode.isAsyncPlaceholder = true;
      return true
    }
    // assert node match
    if (true) {
      if (!assertNodeMatch(elm, vnode, inVPre)) {
        return false
      }
    }
    if (isDef(data)) {
      if (isDef(i = data.hook) && isDef(i = i.init)) { i(vnode, true /* hydrating */); }
      if (isDef(i = vnode.componentInstance)) {
        // child component. it should have hydrated its own tree.
        initComponent(vnode, insertedVnodeQueue);
        return true
      }
    }
    if (isDef(tag)) {
      if (isDef(children)) {
        // empty element, allow client to pick up and populate children
        if (!elm.hasChildNodes()) {
          createChildren(vnode, children, insertedVnodeQueue);
        } else {
          // v-html and domProps: innerHTML
          if (isDef(i = data) && isDef(i = i.domProps) && isDef(i = i.innerHTML)) {
            if (i !== elm.innerHTML) {
              /* istanbul ignore if */
              if ("development" !== 'production' &&
                typeof console !== 'undefined' &&
                !hydrationBailed
              ) {
                hydrationBailed = true;
                console.warn('Parent: ', elm);
                console.warn('server innerHTML: ', i);
                console.warn('client innerHTML: ', elm.innerHTML);
              }
              return false
            }
          } else {
            // iterate and compare children lists
            var childrenMatch = true;
            var childNode = elm.firstChild;
            for (var i$1 = 0; i$1 < children.length; i$1++) {
              if (!childNode || !hydrate(childNode, children[i$1], insertedVnodeQueue, inVPre)) {
                childrenMatch = false;
                break
              }
              childNode = childNode.nextSibling;
            }
            // if childNode is not null, it means the actual childNodes list is
            // longer than the virtual children list.
            if (!childrenMatch || childNode) {
              /* istanbul ignore if */
              if ("development" !== 'production' &&
                typeof console !== 'undefined' &&
                !hydrationBailed
              ) {
                hydrationBailed = true;
                console.warn('Parent: ', elm);
                console.warn('Mismatching childNodes vs. VNodes: ', elm.childNodes, children);
              }
              return false
            }
          }
        }
      }
      if (isDef(data)) {
        var fullInvoke = false;
        for (var key in data) {
          if (!isRenderedModule(key)) {
            fullInvoke = true;
            invokeCreateHooks(vnode, insertedVnodeQueue);
            break
          }
        }
        if (!fullInvoke && data['class']) {
          // ensure collecting deps for deep class bindings for future updates
          traverse(data['class']);
        }
      }
    } else if (elm.data !== vnode.text) {
      elm.data = vnode.text;
    }
    return true
  }

  function assertNodeMatch (node, vnode, inVPre) {
    if (isDef(vnode.tag)) {
      return vnode.tag.indexOf('vue-component') === 0 || (
        !isUnknownElement$$1(vnode, inVPre) &&
        vnode.tag.toLowerCase() === (node.tagName && node.tagName.toLowerCase())
      )
    } else {
      return node.nodeType === (vnode.isComment ? 8 : 3)
    }
  }

  return function patch (oldVnode, vnode, hydrating, removeOnly, parentElm, refElm) {
    if (isUndef(vnode)) {
      if (isDef(oldVnode)) { invokeDestroyHook(oldVnode); }
      return
    }

    var isInitialPatch = false;
    var insertedVnodeQueue = [];

    if (isUndef(oldVnode)) {
      // empty mount (likely as component), create new root element
      isInitialPatch = true;
      createElm(vnode, insertedVnodeQueue, parentElm, refElm);
    } else {
      var isRealElement = isDef(oldVnode.nodeType);
      if (!isRealElement && sameVnode(oldVnode, vnode)) {
        // patch existing root node
        patchVnode(oldVnode, vnode, insertedVnodeQueue, removeOnly);
      } else {
        if (isRealElement) {
          // mounting to a real element
          // check if this is server-rendered content and if we can perform
          // a successful hydration.
          if (oldVnode.nodeType === 1 && oldVnode.hasAttribute(SSR_ATTR)) {
            oldVnode.removeAttribute(SSR_ATTR);
            hydrating = true;
          }
          if (isTrue(hydrating)) {
            if (hydrate(oldVnode, vnode, insertedVnodeQueue)) {
              invokeInsertHook(vnode, insertedVnodeQueue, true);
              return oldVnode
            } else if (true) {
              warn(
                'The client-side rendered virtual DOM tree is not matching ' +
                'server-rendered content. This is likely caused by incorrect ' +
                'HTML markup, for example nesting block-level elements inside ' +
                '<p>, or missing <tbody>. Bailing hydration and performing ' +
                'full client-side render.'
              );
            }
          }
          // either not server-rendered, or hydration failed.
          // create an empty node and replace it
          oldVnode = emptyNodeAt(oldVnode);
        }

        // replacing existing element
        var oldElm = oldVnode.elm;
        var parentElm$1 = nodeOps.parentNode(oldElm);

        // create new node
        createElm(
          vnode,
          insertedVnodeQueue,
          // extremely rare edge case: do not insert if old element is in a
          // leaving transition. Only happens when combining transition +
          // keep-alive + HOCs. (#4590)
          oldElm._leaveCb ? null : parentElm$1,
          nodeOps.nextSibling(oldElm)
        );

        // update parent placeholder node element, recursively
        if (isDef(vnode.parent)) {
          var ancestor = vnode.parent;
          var patchable = isPatchable(vnode);
          while (ancestor) {
            for (var i = 0; i < cbs.destroy.length; ++i) {
              cbs.destroy[i](ancestor);
            }
            ancestor.elm = vnode.elm;
            if (patchable) {
              for (var i$1 = 0; i$1 < cbs.create.length; ++i$1) {
                cbs.create[i$1](emptyNode, ancestor);
              }
              // #6513
              // invoke insert hooks that may have been merged by create hooks.
              // e.g. for directives that uses the "inserted" hook.
              var insert = ancestor.data.hook.insert;
              if (insert.merged) {
                // start at index 1 to avoid re-invoking component mounted hook
                for (var i$2 = 1; i$2 < insert.fns.length; i$2++) {
                  insert.fns[i$2]();
                }
              }
            } else {
              registerRef(ancestor);
            }
            ancestor = ancestor.parent;
          }
        }

        // destroy old node
        if (isDef(parentElm$1)) {
          removeVnodes(parentElm$1, [oldVnode], 0, 0);
        } else if (isDef(oldVnode.tag)) {
          invokeDestroyHook(oldVnode);
        }
      }
    }

    invokeInsertHook(vnode, insertedVnodeQueue, isInitialPatch);
    return vnode.elm
  }
}

/*  */

var directives = {
  create: updateDirectives,
  update: updateDirectives,
  destroy: function unbindDirectives (vnode) {
    updateDirectives(vnode, emptyNode);
  }
};

function updateDirectives (oldVnode, vnode) {
  if (oldVnode.data.directives || vnode.data.directives) {
    _update(oldVnode, vnode);
  }
}

function _update (oldVnode, vnode) {
  var isCreate = oldVnode === emptyNode;
  var isDestroy = vnode === emptyNode;
  var oldDirs = normalizeDirectives$1(oldVnode.data.directives, oldVnode.context);
  var newDirs = normalizeDirectives$1(vnode.data.directives, vnode.context);

  var dirsWithInsert = [];
  var dirsWithPostpatch = [];

  var key, oldDir, dir;
  for (key in newDirs) {
    oldDir = oldDirs[key];
    dir = newDirs[key];
    if (!oldDir) {
      // new directive, bind
      callHook$1(dir, 'bind', vnode, oldVnode);
      if (dir.def && dir.def.inserted) {
        dirsWithInsert.push(dir);
      }
    } else {
      // existing directive, update
      dir.oldValue = oldDir.value;
      callHook$1(dir, 'update', vnode, oldVnode);
      if (dir.def && dir.def.componentUpdated) {
        dirsWithPostpatch.push(dir);
      }
    }
  }

  if (dirsWithInsert.length) {
    var callInsert = function () {
      for (var i = 0; i < dirsWithInsert.length; i++) {
        callHook$1(dirsWithInsert[i], 'inserted', vnode, oldVnode);
      }
    };
    if (isCreate) {
      mergeVNodeHook(vnode, 'insert', callInsert);
    } else {
      callInsert();
    }
  }

  if (dirsWithPostpatch.length) {
    mergeVNodeHook(vnode, 'postpatch', function () {
      for (var i = 0; i < dirsWithPostpatch.length; i++) {
        callHook$1(dirsWithPostpatch[i], 'componentUpdated', vnode, oldVnode);
      }
    });
  }

  if (!isCreate) {
    for (key in oldDirs) {
      if (!newDirs[key]) {
        // no longer present, unbind
        callHook$1(oldDirs[key], 'unbind', oldVnode, oldVnode, isDestroy);
      }
    }
  }
}

var emptyModifiers = Object.create(null);

function normalizeDirectives$1 (
  dirs,
  vm
) {
  var res = Object.create(null);
  if (!dirs) {
    // $flow-disable-line
    return res
  }
  var i, dir;
  for (i = 0; i < dirs.length; i++) {
    dir = dirs[i];
    if (!dir.modifiers) {
      // $flow-disable-line
      dir.modifiers = emptyModifiers;
    }
    res[getRawDirName(dir)] = dir;
    dir.def = resolveAsset(vm.$options, 'directives', dir.name, true);
  }
  // $flow-disable-line
  return res
}

function getRawDirName (dir) {
  return dir.rawName || ((dir.name) + "." + (Object.keys(dir.modifiers || {}).join('.')))
}

function callHook$1 (dir, hook, vnode, oldVnode, isDestroy) {
  var fn = dir.def && dir.def[hook];
  if (fn) {
    try {
      fn(vnode.elm, dir, vnode, oldVnode, isDestroy);
    } catch (e) {
      handleError(e, vnode.context, ("directive " + (dir.name) + " " + hook + " hook"));
    }
  }
}

var baseModules = [
  ref,
  directives
];

/*  */

function updateAttrs (oldVnode, vnode) {
  var opts = vnode.componentOptions;
  if (isDef(opts) && opts.Ctor.options.inheritAttrs === false) {
    return
  }
  if (isUndef(oldVnode.data.attrs) && isUndef(vnode.data.attrs)) {
    return
  }
  var key, cur, old;
  var elm = vnode.elm;
  var oldAttrs = oldVnode.data.attrs || {};
  var attrs = vnode.data.attrs || {};
  // clone observed objects, as the user probably wants to mutate it
  if (isDef(attrs.__ob__)) {
    attrs = vnode.data.attrs = extend({}, attrs);
  }

  for (key in attrs) {
    cur = attrs[key];
    old = oldAttrs[key];
    if (old !== cur) {
      setAttr(elm, key, cur);
    }
  }
  // #4391: in IE9, setting type can reset value for input[type=radio]
  // #6666: IE/Edge forces progress value down to 1 before setting a max
  /* istanbul ignore if */
  if ((isIE || isEdge) && attrs.value !== oldAttrs.value) {
    setAttr(elm, 'value', attrs.value);
  }
  for (key in oldAttrs) {
    if (isUndef(attrs[key])) {
      if (isXlink(key)) {
        elm.removeAttributeNS(xlinkNS, getXlinkProp(key));
      } else if (!isEnumeratedAttr(key)) {
        elm.removeAttribute(key);
      }
    }
  }
}

function setAttr (el, key, value) {
  if (isBooleanAttr(key)) {
    // set attribute for blank value
    // e.g. <option disabled>Select one</option>
    if (isFalsyAttrValue(value)) {
      el.removeAttribute(key);
    } else {
      // technically allowfullscreen is a boolean attribute for <iframe>,
      // but Flash expects a value of "true" when used on <embed> tag
      value = key === 'allowfullscreen' && el.tagName === 'EMBED'
        ? 'true'
        : key;
      el.setAttribute(key, value);
    }
  } else if (isEnumeratedAttr(key)) {
    el.setAttribute(key, isFalsyAttrValue(value) || value === 'false' ? 'false' : 'true');
  } else if (isXlink(key)) {
    if (isFalsyAttrValue(value)) {
      el.removeAttributeNS(xlinkNS, getXlinkProp(key));
    } else {
      el.setAttributeNS(xlinkNS, key, value);
    }
  } else {
    if (isFalsyAttrValue(value)) {
      el.removeAttribute(key);
    } else {
      // #7138: IE10 & 11 fires input event when setting placeholder on
      // <textarea>... block the first input event and remove the blocker
      // immediately.
      /* istanbul ignore if */
      if (
        isIE && !isIE9 &&
        el.tagName === 'TEXTAREA' &&
        key === 'placeholder' && !el.__ieph
      ) {
        var blocker = function (e) {
          e.stopImmediatePropagation();
          el.removeEventListener('input', blocker);
        };
        el.addEventListener('input', blocker);
        // $flow-disable-line
        el.__ieph = true; /* IE placeholder patched */
      }
      el.setAttribute(key, value);
    }
  }
}

var attrs = {
  create: updateAttrs,
  update: updateAttrs
};

/*  */

function updateClass (oldVnode, vnode) {
  var el = vnode.elm;
  var data = vnode.data;
  var oldData = oldVnode.data;
  if (
    isUndef(data.staticClass) &&
    isUndef(data.class) && (
      isUndef(oldData) || (
        isUndef(oldData.staticClass) &&
        isUndef(oldData.class)
      )
    )
  ) {
    return
  }

  var cls = genClassForVnode(vnode);

  // handle transition classes
  var transitionClass = el._transitionClasses;
  if (isDef(transitionClass)) {
    cls = concat(cls, stringifyClass(transitionClass));
  }

  // set the class
  if (cls !== el._prevClass) {
    el.setAttribute('class', cls);
    el._prevClass = cls;
  }
}

var klass = {
  create: updateClass,
  update: updateClass
};

/*  */

var validDivisionCharRE = /[\w).+\-_$\]]/;

function parseFilters (exp) {
  var inSingle = false;
  var inDouble = false;
  var inTemplateString = false;
  var inRegex = false;
  var curly = 0;
  var square = 0;
  var paren = 0;
  var lastFilterIndex = 0;
  var c, prev, i, expression, filters;

  for (i = 0; i < exp.length; i++) {
    prev = c;
    c = exp.charCodeAt(i);
    if (inSingle) {
      if (c === 0x27 && prev !== 0x5C) { inSingle = false; }
    } else if (inDouble) {
      if (c === 0x22 && prev !== 0x5C) { inDouble = false; }
    } else if (inTemplateString) {
      if (c === 0x60 && prev !== 0x5C) { inTemplateString = false; }
    } else if (inRegex) {
      if (c === 0x2f && prev !== 0x5C) { inRegex = false; }
    } else if (
      c === 0x7C && // pipe
      exp.charCodeAt(i + 1) !== 0x7C &&
      exp.charCodeAt(i - 1) !== 0x7C &&
      !curly && !square && !paren
    ) {
      if (expression === undefined) {
        // first filter, end of expression
        lastFilterIndex = i + 1;
        expression = exp.slice(0, i).trim();
      } else {
        pushFilter();
      }
    } else {
      switch (c) {
        case 0x22: inDouble = true; break         // "
        case 0x27: inSingle = true; break         // '
        case 0x60: inTemplateString = true; break // `
        case 0x28: paren++; break                 // (
        case 0x29: paren--; break                 // )
        case 0x5B: square++; break                // [
        case 0x5D: square--; break                // ]
        case 0x7B: curly++; break                 // {
        case 0x7D: curly--; break                 // }
      }
      if (c === 0x2f) { // /
        var j = i - 1;
        var p = (void 0);
        // find first non-whitespace prev char
        for (; j >= 0; j--) {
          p = exp.charAt(j);
          if (p !== ' ') { break }
        }
        if (!p || !validDivisionCharRE.test(p)) {
          inRegex = true;
        }
      }
    }
  }

  if (expression === undefined) {
    expression = exp.slice(0, i).trim();
  } else if (lastFilterIndex !== 0) {
    pushFilter();
  }

  function pushFilter () {
    (filters || (filters = [])).push(exp.slice(lastFilterIndex, i).trim());
    lastFilterIndex = i + 1;
  }

  if (filters) {
    for (i = 0; i < filters.length; i++) {
      expression = wrapFilter(expression, filters[i]);
    }
  }

  return expression
}

function wrapFilter (exp, filter) {
  var i = filter.indexOf('(');
  if (i < 0) {
    // _f: resolveFilter
    return ("_f(\"" + filter + "\")(" + exp + ")")
  } else {
    var name = filter.slice(0, i);
    var args = filter.slice(i + 1);
    return ("_f(\"" + name + "\")(" + exp + "," + args)
  }
}

/*  */

function baseWarn (msg) {
  console.error(("[Vue compiler]: " + msg));
}

function pluckModuleFunction (
  modules,
  key
) {
  return modules
    ? modules.map(function (m) { return m[key]; }).filter(function (_) { return _; })
    : []
}

function addProp (el, name, value) {
  (el.props || (el.props = [])).push({ name: name, value: value });
  el.plain = false;
}

function addAttr (el, name, value) {
  (el.attrs || (el.attrs = [])).push({ name: name, value: value });
  el.plain = false;
}

// add a raw attr (use this in preTransforms)
function addRawAttr (el, name, value) {
  el.attrsMap[name] = value;
  el.attrsList.push({ name: name, value: value });
}

function addDirective (
  el,
  name,
  rawName,
  value,
  arg,
  modifiers
) {
  (el.directives || (el.directives = [])).push({ name: name, rawName: rawName, value: value, arg: arg, modifiers: modifiers });
  el.plain = false;
}

function addHandler (
  el,
  name,
  value,
  modifiers,
  important,
  warn
) {
  modifiers = modifiers || emptyObject;
  // warn prevent and passive modifier
  /* istanbul ignore if */
  if (
    "development" !== 'production' && warn &&
    modifiers.prevent && modifiers.passive
  ) {
    warn(
      'passive and prevent can\'t be used together. ' +
      'Passive handler can\'t prevent default event.'
    );
  }

  // check capture modifier
  if (modifiers.capture) {
    delete modifiers.capture;
    name = '!' + name; // mark the event as captured
  }
  if (modifiers.once) {
    delete modifiers.once;
    name = '~' + name; // mark the event as once
  }
  /* istanbul ignore if */
  if (modifiers.passive) {
    delete modifiers.passive;
    name = '&' + name; // mark the event as passive
  }

  // normalize click.right and click.middle since they don't actually fire
  // this is technically browser-specific, but at least for now browsers are
  // the only target envs that have right/middle clicks.
  if (name === 'click') {
    if (modifiers.right) {
      name = 'contextmenu';
      delete modifiers.right;
    } else if (modifiers.middle) {
      name = 'mouseup';
    }
  }

  var events;
  if (modifiers.native) {
    delete modifiers.native;
    events = el.nativeEvents || (el.nativeEvents = {});
  } else {
    events = el.events || (el.events = {});
  }

  var newHandler = { value: value };
  if (modifiers !== emptyObject) {
    newHandler.modifiers = modifiers;
  }

  var handlers = events[name];
  /* istanbul ignore if */
  if (Array.isArray(handlers)) {
    important ? handlers.unshift(newHandler) : handlers.push(newHandler);
  } else if (handlers) {
    events[name] = important ? [newHandler, handlers] : [handlers, newHandler];
  } else {
    events[name] = newHandler;
  }

  el.plain = false;
}

function getBindingAttr (
  el,
  name,
  getStatic
) {
  var dynamicValue =
    getAndRemoveAttr(el, ':' + name) ||
    getAndRemoveAttr(el, 'v-bind:' + name);
  if (dynamicValue != null) {
    return parseFilters(dynamicValue)
  } else if (getStatic !== false) {
    var staticValue = getAndRemoveAttr(el, name);
    if (staticValue != null) {
      return JSON.stringify(staticValue)
    }
  }
}

// note: this only removes the attr from the Array (attrsList) so that it
// doesn't get processed by processAttrs.
// By default it does NOT remove it from the map (attrsMap) because the map is
// needed during codegen.
function getAndRemoveAttr (
  el,
  name,
  removeFromMap
) {
  var val;
  if ((val = el.attrsMap[name]) != null) {
    var list = el.attrsList;
    for (var i = 0, l = list.length; i < l; i++) {
      if (list[i].name === name) {
        list.splice(i, 1);
        break
      }
    }
  }
  if (removeFromMap) {
    delete el.attrsMap[name];
  }
  return val
}

/*  */

/**
 * Cross-platform code generation for component v-model
 */
function genComponentModel (
  el,
  value,
  modifiers
) {
  var ref = modifiers || {};
  var number = ref.number;
  var trim = ref.trim;

  var baseValueExpression = '$$v';
  var valueExpression = baseValueExpression;
  if (trim) {
    valueExpression =
      "(typeof " + baseValueExpression + " === 'string'" +
        "? " + baseValueExpression + ".trim()" +
        ": " + baseValueExpression + ")";
  }
  if (number) {
    valueExpression = "_n(" + valueExpression + ")";
  }
  var assignment = genAssignmentCode(value, valueExpression);

  el.model = {
    value: ("(" + value + ")"),
    expression: ("\"" + value + "\""),
    callback: ("function (" + baseValueExpression + ") {" + assignment + "}")
  };
}

/**
 * Cross-platform codegen helper for generating v-model value assignment code.
 */
function genAssignmentCode (
  value,
  assignment
) {
  var res = parseModel(value);
  if (res.key === null) {
    return (value + "=" + assignment)
  } else {
    return ("$set(" + (res.exp) + ", " + (res.key) + ", " + assignment + ")")
  }
}

/**
 * Parse a v-model expression into a base path and a final key segment.
 * Handles both dot-path and possible square brackets.
 *
 * Possible cases:
 *
 * - test
 * - test[key]
 * - test[test1[key]]
 * - test["a"][key]
 * - xxx.test[a[a].test1[key]]
 * - test.xxx.a["asa"][test1[key]]
 *
 */

var len;
var str;
var chr;
var index$1;
var expressionPos;
var expressionEndPos;



function parseModel (val) {
  len = val.length;

  if (val.indexOf('[') < 0 || val.lastIndexOf(']') < len - 1) {
    index$1 = val.lastIndexOf('.');
    if (index$1 > -1) {
      return {
        exp: val.slice(0, index$1),
        key: '"' + val.slice(index$1 + 1) + '"'
      }
    } else {
      return {
        exp: val,
        key: null
      }
    }
  }

  str = val;
  index$1 = expressionPos = expressionEndPos = 0;

  while (!eof()) {
    chr = next();
    /* istanbul ignore if */
    if (isStringStart(chr)) {
      parseString(chr);
    } else if (chr === 0x5B) {
      parseBracket(chr);
    }
  }

  return {
    exp: val.slice(0, expressionPos),
    key: val.slice(expressionPos + 1, expressionEndPos)
  }
}

function next () {
  return str.charCodeAt(++index$1)
}

function eof () {
  return index$1 >= len
}

function isStringStart (chr) {
  return chr === 0x22 || chr === 0x27
}

function parseBracket (chr) {
  var inBracket = 1;
  expressionPos = index$1;
  while (!eof()) {
    chr = next();
    if (isStringStart(chr)) {
      parseString(chr);
      continue
    }
    if (chr === 0x5B) { inBracket++; }
    if (chr === 0x5D) { inBracket--; }
    if (inBracket === 0) {
      expressionEndPos = index$1;
      break
    }
  }
}

function parseString (chr) {
  var stringQuote = chr;
  while (!eof()) {
    chr = next();
    if (chr === stringQuote) {
      break
    }
  }
}

/*  */

var warn$1;

// in some cases, the event used has to be determined at runtime
// so we used some reserved tokens during compile.
var RANGE_TOKEN = '__r';
var CHECKBOX_RADIO_TOKEN = '__c';

function model (
  el,
  dir,
  _warn
) {
  warn$1 = _warn;
  var value = dir.value;
  var modifiers = dir.modifiers;
  var tag = el.tag;
  var type = el.attrsMap.type;

  if (true) {
    // inputs with type="file" are read only and setting the input's
    // value will throw an error.
    if (tag === 'input' && type === 'file') {
      warn$1(
        "<" + (el.tag) + " v-model=\"" + value + "\" type=\"file\">:\n" +
        "File inputs are read only. Use a v-on:change listener instead."
      );
    }
  }

  if (el.component) {
    genComponentModel(el, value, modifiers);
    // component v-model doesn't need extra runtime
    return false
  } else if (tag === 'select') {
    genSelect(el, value, modifiers);
  } else if (tag === 'input' && type === 'checkbox') {
    genCheckboxModel(el, value, modifiers);
  } else if (tag === 'input' && type === 'radio') {
    genRadioModel(el, value, modifiers);
  } else if (tag === 'input' || tag === 'textarea') {
    genDefaultModel(el, value, modifiers);
  } else if (!config.isReservedTag(tag)) {
    genComponentModel(el, value, modifiers);
    // component v-model doesn't need extra runtime
    return false
  } else if (true) {
    warn$1(
      "<" + (el.tag) + " v-model=\"" + value + "\">: " +
      "v-model is not supported on this element type. " +
      'If you are working with contenteditable, it\'s recommended to ' +
      'wrap a library dedicated for that purpose inside a custom component.'
    );
  }

  // ensure runtime directive metadata
  return true
}

function genCheckboxModel (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var valueBinding = getBindingAttr(el, 'value') || 'null';
  var trueValueBinding = getBindingAttr(el, 'true-value') || 'true';
  var falseValueBinding = getBindingAttr(el, 'false-value') || 'false';
  addProp(el, 'checked',
    "Array.isArray(" + value + ")" +
    "?_i(" + value + "," + valueBinding + ")>-1" + (
      trueValueBinding === 'true'
        ? (":(" + value + ")")
        : (":_q(" + value + "," + trueValueBinding + ")")
    )
  );
  addHandler(el, 'change',
    "var $$a=" + value + "," +
        '$$el=$event.target,' +
        "$$c=$$el.checked?(" + trueValueBinding + "):(" + falseValueBinding + ");" +
    'if(Array.isArray($$a)){' +
      "var $$v=" + (number ? '_n(' + valueBinding + ')' : valueBinding) + "," +
          '$$i=_i($$a,$$v);' +
      "if($$el.checked){$$i<0&&(" + value + "=$$a.concat([$$v]))}" +
      "else{$$i>-1&&(" + value + "=$$a.slice(0,$$i).concat($$a.slice($$i+1)))}" +
    "}else{" + (genAssignmentCode(value, '$$c')) + "}",
    null, true
  );
}

function genRadioModel (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var valueBinding = getBindingAttr(el, 'value') || 'null';
  valueBinding = number ? ("_n(" + valueBinding + ")") : valueBinding;
  addProp(el, 'checked', ("_q(" + value + "," + valueBinding + ")"));
  addHandler(el, 'change', genAssignmentCode(value, valueBinding), null, true);
}

function genSelect (
  el,
  value,
  modifiers
) {
  var number = modifiers && modifiers.number;
  var selectedVal = "Array.prototype.filter" +
    ".call($event.target.options,function(o){return o.selected})" +
    ".map(function(o){var val = \"_value\" in o ? o._value : o.value;" +
    "return " + (number ? '_n(val)' : 'val') + "})";

  var assignment = '$event.target.multiple ? $$selectedVal : $$selectedVal[0]';
  var code = "var $$selectedVal = " + selectedVal + ";";
  code = code + " " + (genAssignmentCode(value, assignment));
  addHandler(el, 'change', code, null, true);
}

function genDefaultModel (
  el,
  value,
  modifiers
) {
  var type = el.attrsMap.type;

  // warn if v-bind:value conflicts with v-model
  if (true) {
    var value$1 = el.attrsMap['v-bind:value'] || el.attrsMap[':value'];
    if (value$1) {
      var binding = el.attrsMap['v-bind:value'] ? 'v-bind:value' : ':value';
      warn$1(
        binding + "=\"" + value$1 + "\" conflicts with v-model on the same element " +
        'because the latter already expands to a value binding internally'
      );
    }
  }

  var ref = modifiers || {};
  var lazy = ref.lazy;
  var number = ref.number;
  var trim = ref.trim;
  var needCompositionGuard = !lazy && type !== 'range';
  var event = lazy
    ? 'change'
    : type === 'range'
      ? RANGE_TOKEN
      : 'input';

  var valueExpression = '$event.target.value';
  if (trim) {
    valueExpression = "$event.target.value.trim()";
  }
  if (number) {
    valueExpression = "_n(" + valueExpression + ")";
  }

  var code = genAssignmentCode(value, valueExpression);
  if (needCompositionGuard) {
    code = "if($event.target.composing)return;" + code;
  }

  addProp(el, 'value', ("(" + value + ")"));
  addHandler(el, event, code, null, true);
  if (trim || number) {
    addHandler(el, 'blur', '$forceUpdate()');
  }
}

/*  */

// normalize v-model event tokens that can only be determined at runtime.
// it's important to place the event as the first in the array because
// the whole point is ensuring the v-model callback gets called before
// user-attached handlers.
function normalizeEvents (on) {
  /* istanbul ignore if */
  if (isDef(on[RANGE_TOKEN])) {
    // IE input[type=range] only supports `change` event
    var event = isIE ? 'change' : 'input';
    on[event] = [].concat(on[RANGE_TOKEN], on[event] || []);
    delete on[RANGE_TOKEN];
  }
  // This was originally intended to fix #4521 but no longer necessary
  // after 2.5. Keeping it for backwards compat with generated code from < 2.4
  /* istanbul ignore if */
  if (isDef(on[CHECKBOX_RADIO_TOKEN])) {
    on.change = [].concat(on[CHECKBOX_RADIO_TOKEN], on.change || []);
    delete on[CHECKBOX_RADIO_TOKEN];
  }
}

var target$1;

function createOnceHandler (handler, event, capture) {
  var _target = target$1; // save current target element in closure
  return function onceHandler () {
    var res = handler.apply(null, arguments);
    if (res !== null) {
      remove$2(event, onceHandler, capture, _target);
    }
  }
}

function add$1 (
  event,
  handler,
  once$$1,
  capture,
  passive
) {
  handler = withMacroTask(handler);
  if (once$$1) { handler = createOnceHandler(handler, event, capture); }
  target$1.addEventListener(
    event,
    handler,
    supportsPassive
      ? { capture: capture, passive: passive }
      : capture
  );
}

function remove$2 (
  event,
  handler,
  capture,
  _target
) {
  (_target || target$1).removeEventListener(
    event,
    handler._withTask || handler,
    capture
  );
}

function updateDOMListeners (oldVnode, vnode) {
  if (isUndef(oldVnode.data.on) && isUndef(vnode.data.on)) {
    return
  }
  var on = vnode.data.on || {};
  var oldOn = oldVnode.data.on || {};
  target$1 = vnode.elm;
  normalizeEvents(on);
  updateListeners(on, oldOn, add$1, remove$2, vnode.context);
  target$1 = undefined;
}

var events = {
  create: updateDOMListeners,
  update: updateDOMListeners
};

/*  */

function updateDOMProps (oldVnode, vnode) {
  if (isUndef(oldVnode.data.domProps) && isUndef(vnode.data.domProps)) {
    return
  }
  var key, cur;
  var elm = vnode.elm;
  var oldProps = oldVnode.data.domProps || {};
  var props = vnode.data.domProps || {};
  // clone observed objects, as the user probably wants to mutate it
  if (isDef(props.__ob__)) {
    props = vnode.data.domProps = extend({}, props);
  }

  for (key in oldProps) {
    if (isUndef(props[key])) {
      elm[key] = '';
    }
  }
  for (key in props) {
    cur = props[key];
    // ignore children if the node has textContent or innerHTML,
    // as these will throw away existing DOM nodes and cause removal errors
    // on subsequent patches (#3360)
    if (key === 'textContent' || key === 'innerHTML') {
      if (vnode.children) { vnode.children.length = 0; }
      if (cur === oldProps[key]) { continue }
      // #6601 work around Chrome version <= 55 bug where single textNode
      // replaced by innerHTML/textContent retains its parentNode property
      if (elm.childNodes.length === 1) {
        elm.removeChild(elm.childNodes[0]);
      }
    }

    if (key === 'value') {
      // store value as _value as well since
      // non-string values will be stringified
      elm._value = cur;
      // avoid resetting cursor position when value is the same
      var strCur = isUndef(cur) ? '' : String(cur);
      if (shouldUpdateValue(elm, strCur)) {
        elm.value = strCur;
      }
    } else {
      elm[key] = cur;
    }
  }
}

// check platforms/web/util/attrs.js acceptValue


function shouldUpdateValue (elm, checkVal) {
  return (!elm.composing && (
    elm.tagName === 'OPTION' ||
    isNotInFocusAndDirty(elm, checkVal) ||
    isDirtyWithModifiers(elm, checkVal)
  ))
}

function isNotInFocusAndDirty (elm, checkVal) {
  // return true when textbox (.number and .trim) loses focus and its value is
  // not equal to the updated value
  var notInFocus = true;
  // #6157
  // work around IE bug when accessing document.activeElement in an iframe
  try { notInFocus = document.activeElement !== elm; } catch (e) {}
  return notInFocus && elm.value !== checkVal
}

function isDirtyWithModifiers (elm, newVal) {
  var value = elm.value;
  var modifiers = elm._vModifiers; // injected by v-model runtime
  if (isDef(modifiers)) {
    if (modifiers.lazy) {
      // inputs with lazy should only be updated when not in focus
      return false
    }
    if (modifiers.number) {
      return toNumber(value) !== toNumber(newVal)
    }
    if (modifiers.trim) {
      return value.trim() !== newVal.trim()
    }
  }
  return value !== newVal
}

var domProps = {
  create: updateDOMProps,
  update: updateDOMProps
};

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// merge static and dynamic style data on the same vnode
function normalizeStyleData (data) {
  var style = normalizeStyleBinding(data.style);
  // static style is pre-processed into an object during compilation
  // and is always a fresh object, so it's safe to merge into it
  return data.staticStyle
    ? extend(data.staticStyle, style)
    : style
}

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/**
 * parent component style should be after child's
 * so that parent component's style could override it
 */
function getStyle (vnode, checkChild) {
  var res = {};
  var styleData;

  if (checkChild) {
    var childNode = vnode;
    while (childNode.componentInstance) {
      childNode = childNode.componentInstance._vnode;
      if (
        childNode && childNode.data &&
        (styleData = normalizeStyleData(childNode.data))
      ) {
        extend(res, styleData);
      }
    }
  }

  if ((styleData = normalizeStyleData(vnode.data))) {
    extend(res, styleData);
  }

  var parentNode = vnode;
  while ((parentNode = parentNode.parent)) {
    if (parentNode.data && (styleData = normalizeStyleData(parentNode.data))) {
      extend(res, styleData);
    }
  }
  return res
}

/*  */

var cssVarRE = /^--/;
var importantRE = /\s*!important$/;
var setProp = function (el, name, val) {
  /* istanbul ignore if */
  if (cssVarRE.test(name)) {
    el.style.setProperty(name, val);
  } else if (importantRE.test(val)) {
    el.style.setProperty(name, val.replace(importantRE, ''), 'important');
  } else {
    var normalizedName = normalize(name);
    if (Array.isArray(val)) {
      // Support values array created by autoprefixer, e.g.
      // {display: ["-webkit-box", "-ms-flexbox", "flex"]}
      // Set them one by one, and the browser will only set those it can recognize
      for (var i = 0, len = val.length; i < len; i++) {
        el.style[normalizedName] = val[i];
      }
    } else {
      el.style[normalizedName] = val;
    }
  }
};

var vendorNames = ['Webkit', 'Moz', 'ms'];

var emptyStyle;
var normalize = cached(function (prop) {
  emptyStyle = emptyStyle || document.createElement('div').style;
  prop = camelize(prop);
  if (prop !== 'filter' && (prop in emptyStyle)) {
    return prop
  }
  var capName = prop.charAt(0).toUpperCase() + prop.slice(1);
  for (var i = 0; i < vendorNames.length; i++) {
    var name = vendorNames[i] + capName;
    if (name in emptyStyle) {
      return name
    }
  }
});

function updateStyle (oldVnode, vnode) {
  var data = vnode.data;
  var oldData = oldVnode.data;

  if (isUndef(data.staticStyle) && isUndef(data.style) &&
    isUndef(oldData.staticStyle) && isUndef(oldData.style)
  ) {
    return
  }

  var cur, name;
  var el = vnode.elm;
  var oldStaticStyle = oldData.staticStyle;
  var oldStyleBinding = oldData.normalizedStyle || oldData.style || {};

  // if static style exists, stylebinding already merged into it when doing normalizeStyleData
  var oldStyle = oldStaticStyle || oldStyleBinding;

  var style = normalizeStyleBinding(vnode.data.style) || {};

  // store normalized style under a different key for next diff
  // make sure to clone it if it's reactive, since the user likely wants
  // to mutate it.
  vnode.data.normalizedStyle = isDef(style.__ob__)
    ? extend({}, style)
    : style;

  var newStyle = getStyle(vnode, true);

  for (name in oldStyle) {
    if (isUndef(newStyle[name])) {
      setProp(el, name, '');
    }
  }
  for (name in newStyle) {
    cur = newStyle[name];
    if (cur !== oldStyle[name]) {
      // ie9 setting to null has no effect, must use empty string
      setProp(el, name, cur == null ? '' : cur);
    }
  }
}

var style = {
  create: updateStyle,
  update: updateStyle
};

/*  */

/**
 * Add class with compatibility for SVG since classList is not supported on
 * SVG elements in IE
 */
function addClass (el, cls) {
  /* istanbul ignore if */
  if (!cls || !(cls = cls.trim())) {
    return
  }

  /* istanbul ignore else */
  if (el.classList) {
    if (cls.indexOf(' ') > -1) {
      cls.split(/\s+/).forEach(function (c) { return el.classList.add(c); });
    } else {
      el.classList.add(cls);
    }
  } else {
    var cur = " " + (el.getAttribute('class') || '') + " ";
    if (cur.indexOf(' ' + cls + ' ') < 0) {
      el.setAttribute('class', (cur + cls).trim());
    }
  }
}

/**
 * Remove class with compatibility for SVG since classList is not supported on
 * SVG elements in IE
 */
function removeClass (el, cls) {
  /* istanbul ignore if */
  if (!cls || !(cls = cls.trim())) {
    return
  }

  /* istanbul ignore else */
  if (el.classList) {
    if (cls.indexOf(' ') > -1) {
      cls.split(/\s+/).forEach(function (c) { return el.classList.remove(c); });
    } else {
      el.classList.remove(cls);
    }
    if (!el.classList.length) {
      el.removeAttribute('class');
    }
  } else {
    var cur = " " + (el.getAttribute('class') || '') + " ";
    var tar = ' ' + cls + ' ';
    while (cur.indexOf(tar) >= 0) {
      cur = cur.replace(tar, ' ');
    }
    cur = cur.trim();
    if (cur) {
      el.setAttribute('class', cur);
    } else {
      el.removeAttribute('class');
    }
  }
}

/*  */

function resolveTransition (def) {
  if (!def) {
    return
  }
  /* istanbul ignore else */
  if (typeof def === 'object') {
    var res = {};
    if (def.css !== false) {
      extend(res, autoCssTransition(def.name || 'v'));
    }
    extend(res, def);
    return res
  } else if (typeof def === 'string') {
    return autoCssTransition(def)
  }
}

var autoCssTransition = cached(function (name) {
  return {
    enterClass: (name + "-enter"),
    enterToClass: (name + "-enter-to"),
    enterActiveClass: (name + "-enter-active"),
    leaveClass: (name + "-leave"),
    leaveToClass: (name + "-leave-to"),
    leaveActiveClass: (name + "-leave-active")
  }
});

var hasTransition = inBrowser && !isIE9;
var TRANSITION = 'transition';
var ANIMATION = 'animation';

// Transition property/event sniffing
var transitionProp = 'transition';
var transitionEndEvent = 'transitionend';
var animationProp = 'animation';
var animationEndEvent = 'animationend';
if (hasTransition) {
  /* istanbul ignore if */
  if (window.ontransitionend === undefined &&
    window.onwebkittransitionend !== undefined
  ) {
    transitionProp = 'WebkitTransition';
    transitionEndEvent = 'webkitTransitionEnd';
  }
  if (window.onanimationend === undefined &&
    window.onwebkitanimationend !== undefined
  ) {
    animationProp = 'WebkitAnimation';
    animationEndEvent = 'webkitAnimationEnd';
  }
}

// binding to window is necessary to make hot reload work in IE in strict mode
var raf = inBrowser
  ? window.requestAnimationFrame
    ? window.requestAnimationFrame.bind(window)
    : setTimeout
  : /* istanbul ignore next */ function (fn) { return fn(); };

function nextFrame (fn) {
  raf(function () {
    raf(fn);
  });
}

function addTransitionClass (el, cls) {
  var transitionClasses = el._transitionClasses || (el._transitionClasses = []);
  if (transitionClasses.indexOf(cls) < 0) {
    transitionClasses.push(cls);
    addClass(el, cls);
  }
}

function removeTransitionClass (el, cls) {
  if (el._transitionClasses) {
    remove(el._transitionClasses, cls);
  }
  removeClass(el, cls);
}

function whenTransitionEnds (
  el,
  expectedType,
  cb
) {
  var ref = getTransitionInfo(el, expectedType);
  var type = ref.type;
  var timeout = ref.timeout;
  var propCount = ref.propCount;
  if (!type) { return cb() }
  var event = type === TRANSITION ? transitionEndEvent : animationEndEvent;
  var ended = 0;
  var end = function () {
    el.removeEventListener(event, onEnd);
    cb();
  };
  var onEnd = function (e) {
    if (e.target === el) {
      if (++ended >= propCount) {
        end();
      }
    }
  };
  setTimeout(function () {
    if (ended < propCount) {
      end();
    }
  }, timeout + 1);
  el.addEventListener(event, onEnd);
}

var transformRE = /\b(transform|all)(,|$)/;

function getTransitionInfo (el, expectedType) {
  var styles = window.getComputedStyle(el);
  var transitionDelays = styles[transitionProp + 'Delay'].split(', ');
  var transitionDurations = styles[transitionProp + 'Duration'].split(', ');
  var transitionTimeout = getTimeout(transitionDelays, transitionDurations);
  var animationDelays = styles[animationProp + 'Delay'].split(', ');
  var animationDurations = styles[animationProp + 'Duration'].split(', ');
  var animationTimeout = getTimeout(animationDelays, animationDurations);

  var type;
  var timeout = 0;
  var propCount = 0;
  /* istanbul ignore if */
  if (expectedType === TRANSITION) {
    if (transitionTimeout > 0) {
      type = TRANSITION;
      timeout = transitionTimeout;
      propCount = transitionDurations.length;
    }
  } else if (expectedType === ANIMATION) {
    if (animationTimeout > 0) {
      type = ANIMATION;
      timeout = animationTimeout;
      propCount = animationDurations.length;
    }
  } else {
    timeout = Math.max(transitionTimeout, animationTimeout);
    type = timeout > 0
      ? transitionTimeout > animationTimeout
        ? TRANSITION
        : ANIMATION
      : null;
    propCount = type
      ? type === TRANSITION
        ? transitionDurations.length
        : animationDurations.length
      : 0;
  }
  var hasTransform =
    type === TRANSITION &&
    transformRE.test(styles[transitionProp + 'Property']);
  return {
    type: type,
    timeout: timeout,
    propCount: propCount,
    hasTransform: hasTransform
  }
}

function getTimeout (delays, durations) {
  /* istanbul ignore next */
  while (delays.length < durations.length) {
    delays = delays.concat(delays);
  }

  return Math.max.apply(null, durations.map(function (d, i) {
    return toMs(d) + toMs(delays[i])
  }))
}

function toMs (s) {
  return Number(s.slice(0, -1)) * 1000
}

/*  */

function enter (vnode, toggleDisplay) {
  var el = vnode.elm;

  // call leave callback now
  if (isDef(el._leaveCb)) {
    el._leaveCb.cancelled = true;
    el._leaveCb();
  }

  var data = resolveTransition(vnode.data.transition);
  if (isUndef(data)) {
    return
  }

  /* istanbul ignore if */
  if (isDef(el._enterCb) || el.nodeType !== 1) {
    return
  }

  var css = data.css;
  var type = data.type;
  var enterClass = data.enterClass;
  var enterToClass = data.enterToClass;
  var enterActiveClass = data.enterActiveClass;
  var appearClass = data.appearClass;
  var appearToClass = data.appearToClass;
  var appearActiveClass = data.appearActiveClass;
  var beforeEnter = data.beforeEnter;
  var enter = data.enter;
  var afterEnter = data.afterEnter;
  var enterCancelled = data.enterCancelled;
  var beforeAppear = data.beforeAppear;
  var appear = data.appear;
  var afterAppear = data.afterAppear;
  var appearCancelled = data.appearCancelled;
  var duration = data.duration;

  // activeInstance will always be the <transition> component managing this
  // transition. One edge case to check is when the <transition> is placed
  // as the root node of a child component. In that case we need to check
  // <transition>'s parent for appear check.
  var context = activeInstance;
  var transitionNode = activeInstance.$vnode;
  while (transitionNode && transitionNode.parent) {
    transitionNode = transitionNode.parent;
    context = transitionNode.context;
  }

  var isAppear = !context._isMounted || !vnode.isRootInsert;

  if (isAppear && !appear && appear !== '') {
    return
  }

  var startClass = isAppear && appearClass
    ? appearClass
    : enterClass;
  var activeClass = isAppear && appearActiveClass
    ? appearActiveClass
    : enterActiveClass;
  var toClass = isAppear && appearToClass
    ? appearToClass
    : enterToClass;

  var beforeEnterHook = isAppear
    ? (beforeAppear || beforeEnter)
    : beforeEnter;
  var enterHook = isAppear
    ? (typeof appear === 'function' ? appear : enter)
    : enter;
  var afterEnterHook = isAppear
    ? (afterAppear || afterEnter)
    : afterEnter;
  var enterCancelledHook = isAppear
    ? (appearCancelled || enterCancelled)
    : enterCancelled;

  var explicitEnterDuration = toNumber(
    isObject(duration)
      ? duration.enter
      : duration
  );

  if ("development" !== 'production' && explicitEnterDuration != null) {
    checkDuration(explicitEnterDuration, 'enter', vnode);
  }

  var expectsCSS = css !== false && !isIE9;
  var userWantsControl = getHookArgumentsLength(enterHook);

  var cb = el._enterCb = once(function () {
    if (expectsCSS) {
      removeTransitionClass(el, toClass);
      removeTransitionClass(el, activeClass);
    }
    if (cb.cancelled) {
      if (expectsCSS) {
        removeTransitionClass(el, startClass);
      }
      enterCancelledHook && enterCancelledHook(el);
    } else {
      afterEnterHook && afterEnterHook(el);
    }
    el._enterCb = null;
  });

  if (!vnode.data.show) {
    // remove pending leave element on enter by injecting an insert hook
    mergeVNodeHook(vnode, 'insert', function () {
      var parent = el.parentNode;
      var pendingNode = parent && parent._pending && parent._pending[vnode.key];
      if (pendingNode &&
        pendingNode.tag === vnode.tag &&
        pendingNode.elm._leaveCb
      ) {
        pendingNode.elm._leaveCb();
      }
      enterHook && enterHook(el, cb);
    });
  }

  // start enter transition
  beforeEnterHook && beforeEnterHook(el);
  if (expectsCSS) {
    addTransitionClass(el, startClass);
    addTransitionClass(el, activeClass);
    nextFrame(function () {
      addTransitionClass(el, toClass);
      removeTransitionClass(el, startClass);
      if (!cb.cancelled && !userWantsControl) {
        if (isValidDuration(explicitEnterDuration)) {
          setTimeout(cb, explicitEnterDuration);
        } else {
          whenTransitionEnds(el, type, cb);
        }
      }
    });
  }

  if (vnode.data.show) {
    toggleDisplay && toggleDisplay();
    enterHook && enterHook(el, cb);
  }

  if (!expectsCSS && !userWantsControl) {
    cb();
  }
}

function leave (vnode, rm) {
  var el = vnode.elm;

  // call enter callback now
  if (isDef(el._enterCb)) {
    el._enterCb.cancelled = true;
    el._enterCb();
  }

  var data = resolveTransition(vnode.data.transition);
  if (isUndef(data) || el.nodeType !== 1) {
    return rm()
  }

  /* istanbul ignore if */
  if (isDef(el._leaveCb)) {
    return
  }

  var css = data.css;
  var type = data.type;
  var leaveClass = data.leaveClass;
  var leaveToClass = data.leaveToClass;
  var leaveActiveClass = data.leaveActiveClass;
  var beforeLeave = data.beforeLeave;
  var leave = data.leave;
  var afterLeave = data.afterLeave;
  var leaveCancelled = data.leaveCancelled;
  var delayLeave = data.delayLeave;
  var duration = data.duration;

  var expectsCSS = css !== false && !isIE9;
  var userWantsControl = getHookArgumentsLength(leave);

  var explicitLeaveDuration = toNumber(
    isObject(duration)
      ? duration.leave
      : duration
  );

  if ("development" !== 'production' && isDef(explicitLeaveDuration)) {
    checkDuration(explicitLeaveDuration, 'leave', vnode);
  }

  var cb = el._leaveCb = once(function () {
    if (el.parentNode && el.parentNode._pending) {
      el.parentNode._pending[vnode.key] = null;
    }
    if (expectsCSS) {
      removeTransitionClass(el, leaveToClass);
      removeTransitionClass(el, leaveActiveClass);
    }
    if (cb.cancelled) {
      if (expectsCSS) {
        removeTransitionClass(el, leaveClass);
      }
      leaveCancelled && leaveCancelled(el);
    } else {
      rm();
      afterLeave && afterLeave(el);
    }
    el._leaveCb = null;
  });

  if (delayLeave) {
    delayLeave(performLeave);
  } else {
    performLeave();
  }

  function performLeave () {
    // the delayed leave may have already been cancelled
    if (cb.cancelled) {
      return
    }
    // record leaving element
    if (!vnode.data.show) {
      (el.parentNode._pending || (el.parentNode._pending = {}))[(vnode.key)] = vnode;
    }
    beforeLeave && beforeLeave(el);
    if (expectsCSS) {
      addTransitionClass(el, leaveClass);
      addTransitionClass(el, leaveActiveClass);
      nextFrame(function () {
        addTransitionClass(el, leaveToClass);
        removeTransitionClass(el, leaveClass);
        if (!cb.cancelled && !userWantsControl) {
          if (isValidDuration(explicitLeaveDuration)) {
            setTimeout(cb, explicitLeaveDuration);
          } else {
            whenTransitionEnds(el, type, cb);
          }
        }
      });
    }
    leave && leave(el, cb);
    if (!expectsCSS && !userWantsControl) {
      cb();
    }
  }
}

// only used in dev mode
function checkDuration (val, name, vnode) {
  if (typeof val !== 'number') {
    warn(
      "<transition> explicit " + name + " duration is not a valid number - " +
      "got " + (JSON.stringify(val)) + ".",
      vnode.context
    );
  } else if (isNaN(val)) {
    warn(
      "<transition> explicit " + name + " duration is NaN - " +
      'the duration expression might be incorrect.',
      vnode.context
    );
  }
}

function isValidDuration (val) {
  return typeof val === 'number' && !isNaN(val)
}

/**
 * Normalize a transition hook's argument length. The hook may be:
 * - a merged hook (invoker) with the original in .fns
 * - a wrapped component method (check ._length)
 * - a plain function (.length)
 */
function getHookArgumentsLength (fn) {
  if (isUndef(fn)) {
    return false
  }
  var invokerFns = fn.fns;
  if (isDef(invokerFns)) {
    // invoker
    return getHookArgumentsLength(
      Array.isArray(invokerFns)
        ? invokerFns[0]
        : invokerFns
    )
  } else {
    return (fn._length || fn.length) > 1
  }
}

function _enter (_, vnode) {
  if (vnode.data.show !== true) {
    enter(vnode);
  }
}

var transition = inBrowser ? {
  create: _enter,
  activate: _enter,
  remove: function remove$$1 (vnode, rm) {
    /* istanbul ignore else */
    if (vnode.data.show !== true) {
      leave(vnode, rm);
    } else {
      rm();
    }
  }
} : {};

var platformModules = [
  attrs,
  klass,
  events,
  domProps,
  style,
  transition
];

/*  */

// the directive module should be applied last, after all
// built-in modules have been applied.
var modules = platformModules.concat(baseModules);

var patch = createPatchFunction({ nodeOps: nodeOps, modules: modules });

/**
 * Not type checking this file because flow doesn't like attaching
 * properties to Elements.
 */

/* istanbul ignore if */
if (isIE9) {
  // http://www.matts411.com/post/internet-explorer-9-oninput/
  document.addEventListener('selectionchange', function () {
    var el = document.activeElement;
    if (el && el.vmodel) {
      trigger(el, 'input');
    }
  });
}

var directive = {
  inserted: function inserted (el, binding, vnode, oldVnode) {
    if (vnode.tag === 'select') {
      // #6903
      if (oldVnode.elm && !oldVnode.elm._vOptions) {
        mergeVNodeHook(vnode, 'postpatch', function () {
          directive.componentUpdated(el, binding, vnode);
        });
      } else {
        setSelected(el, binding, vnode.context);
      }
      el._vOptions = [].map.call(el.options, getValue);
    } else if (vnode.tag === 'textarea' || isTextInputType(el.type)) {
      el._vModifiers = binding.modifiers;
      if (!binding.modifiers.lazy) {
        // Safari < 10.2 & UIWebView doesn't fire compositionend when
        // switching focus before confirming composition choice
        // this also fixes the issue where some browsers e.g. iOS Chrome
        // fires "change" instead of "input" on autocomplete.
        el.addEventListener('change', onCompositionEnd);
        if (!isAndroid) {
          el.addEventListener('compositionstart', onCompositionStart);
          el.addEventListener('compositionend', onCompositionEnd);
        }
        /* istanbul ignore if */
        if (isIE9) {
          el.vmodel = true;
        }
      }
    }
  },

  componentUpdated: function componentUpdated (el, binding, vnode) {
    if (vnode.tag === 'select') {
      setSelected(el, binding, vnode.context);
      // in case the options rendered by v-for have changed,
      // it's possible that the value is out-of-sync with the rendered options.
      // detect such cases and filter out values that no longer has a matching
      // option in the DOM.
      var prevOptions = el._vOptions;
      var curOptions = el._vOptions = [].map.call(el.options, getValue);
      if (curOptions.some(function (o, i) { return !looseEqual(o, prevOptions[i]); })) {
        // trigger change event if
        // no matching option found for at least one value
        var needReset = el.multiple
          ? binding.value.some(function (v) { return hasNoMatchingOption(v, curOptions); })
          : binding.value !== binding.oldValue && hasNoMatchingOption(binding.value, curOptions);
        if (needReset) {
          trigger(el, 'change');
        }
      }
    }
  }
};

function setSelected (el, binding, vm) {
  actuallySetSelected(el, binding, vm);
  /* istanbul ignore if */
  if (isIE || isEdge) {
    setTimeout(function () {
      actuallySetSelected(el, binding, vm);
    }, 0);
  }
}

function actuallySetSelected (el, binding, vm) {
  var value = binding.value;
  var isMultiple = el.multiple;
  if (isMultiple && !Array.isArray(value)) {
    "development" !== 'production' && warn(
      "<select multiple v-model=\"" + (binding.expression) + "\"> " +
      "expects an Array value for its binding, but got " + (Object.prototype.toString.call(value).slice(8, -1)),
      vm
    );
    return
  }
  var selected, option;
  for (var i = 0, l = el.options.length; i < l; i++) {
    option = el.options[i];
    if (isMultiple) {
      selected = looseIndexOf(value, getValue(option)) > -1;
      if (option.selected !== selected) {
        option.selected = selected;
      }
    } else {
      if (looseEqual(getValue(option), value)) {
        if (el.selectedIndex !== i) {
          el.selectedIndex = i;
        }
        return
      }
    }
  }
  if (!isMultiple) {
    el.selectedIndex = -1;
  }
}

function hasNoMatchingOption (value, options) {
  return options.every(function (o) { return !looseEqual(o, value); })
}

function getValue (option) {
  return '_value' in option
    ? option._value
    : option.value
}

function onCompositionStart (e) {
  e.target.composing = true;
}

function onCompositionEnd (e) {
  // prevent triggering an input event for no reason
  if (!e.target.composing) { return }
  e.target.composing = false;
  trigger(e.target, 'input');
}

function trigger (el, type) {
  var e = document.createEvent('HTMLEvents');
  e.initEvent(type, true, true);
  el.dispatchEvent(e);
}

/*  */

// recursively search for possible transition defined inside the component root
function locateNode (vnode) {
  return vnode.componentInstance && (!vnode.data || !vnode.data.transition)
    ? locateNode(vnode.componentInstance._vnode)
    : vnode
}

var show = {
  bind: function bind (el, ref, vnode) {
    var value = ref.value;

    vnode = locateNode(vnode);
    var transition$$1 = vnode.data && vnode.data.transition;
    var originalDisplay = el.__vOriginalDisplay =
      el.style.display === 'none' ? '' : el.style.display;
    if (value && transition$$1) {
      vnode.data.show = true;
      enter(vnode, function () {
        el.style.display = originalDisplay;
      });
    } else {
      el.style.display = value ? originalDisplay : 'none';
    }
  },

  update: function update (el, ref, vnode) {
    var value = ref.value;
    var oldValue = ref.oldValue;

    /* istanbul ignore if */
    if (value === oldValue) { return }
    vnode = locateNode(vnode);
    var transition$$1 = vnode.data && vnode.data.transition;
    if (transition$$1) {
      vnode.data.show = true;
      if (value) {
        enter(vnode, function () {
          el.style.display = el.__vOriginalDisplay;
        });
      } else {
        leave(vnode, function () {
          el.style.display = 'none';
        });
      }
    } else {
      el.style.display = value ? el.__vOriginalDisplay : 'none';
    }
  },

  unbind: function unbind (
    el,
    binding,
    vnode,
    oldVnode,
    isDestroy
  ) {
    if (!isDestroy) {
      el.style.display = el.__vOriginalDisplay;
    }
  }
};

var platformDirectives = {
  model: directive,
  show: show
};

/*  */

// Provides transition support for a single element/component.
// supports transition mode (out-in / in-out)

var transitionProps = {
  name: String,
  appear: Boolean,
  css: Boolean,
  mode: String,
  type: String,
  enterClass: String,
  leaveClass: String,
  enterToClass: String,
  leaveToClass: String,
  enterActiveClass: String,
  leaveActiveClass: String,
  appearClass: String,
  appearActiveClass: String,
  appearToClass: String,
  duration: [Number, String, Object]
};

// in case the child is also an abstract component, e.g. <keep-alive>
// we want to recursively retrieve the real component to be rendered
function getRealChild (vnode) {
  var compOptions = vnode && vnode.componentOptions;
  if (compOptions && compOptions.Ctor.options.abstract) {
    return getRealChild(getFirstComponentChild(compOptions.children))
  } else {
    return vnode
  }
}

function extractTransitionData (comp) {
  var data = {};
  var options = comp.$options;
  // props
  for (var key in options.propsData) {
    data[key] = comp[key];
  }
  // events.
  // extract listeners and pass them directly to the transition methods
  var listeners = options._parentListeners;
  for (var key$1 in listeners) {
    data[camelize(key$1)] = listeners[key$1];
  }
  return data
}

function placeholder (h, rawChild) {
  if (/\d-keep-alive$/.test(rawChild.tag)) {
    return h('keep-alive', {
      props: rawChild.componentOptions.propsData
    })
  }
}

function hasParentTransition (vnode) {
  while ((vnode = vnode.parent)) {
    if (vnode.data.transition) {
      return true
    }
  }
}

function isSameChild (child, oldChild) {
  return oldChild.key === child.key && oldChild.tag === child.tag
}

var Transition = {
  name: 'transition',
  props: transitionProps,
  abstract: true,

  render: function render (h) {
    var this$1 = this;

    var children = this.$slots.default;
    if (!children) {
      return
    }

    // filter out text nodes (possible whitespaces)
    children = children.filter(function (c) { return c.tag || isAsyncPlaceholder(c); });
    /* istanbul ignore if */
    if (!children.length) {
      return
    }

    // warn multiple elements
    if ("development" !== 'production' && children.length > 1) {
      warn(
        '<transition> can only be used on a single element. Use ' +
        '<transition-group> for lists.',
        this.$parent
      );
    }

    var mode = this.mode;

    // warn invalid mode
    if ("development" !== 'production' &&
      mode && mode !== 'in-out' && mode !== 'out-in'
    ) {
      warn(
        'invalid <transition> mode: ' + mode,
        this.$parent
      );
    }

    var rawChild = children[0];

    // if this is a component root node and the component's
    // parent container node also has transition, skip.
    if (hasParentTransition(this.$vnode)) {
      return rawChild
    }

    // apply transition data to child
    // use getRealChild() to ignore abstract components e.g. keep-alive
    var child = getRealChild(rawChild);
    /* istanbul ignore if */
    if (!child) {
      return rawChild
    }

    if (this._leaving) {
      return placeholder(h, rawChild)
    }

    // ensure a key that is unique to the vnode type and to this transition
    // component instance. This key will be used to remove pending leaving nodes
    // during entering.
    var id = "__transition-" + (this._uid) + "-";
    child.key = child.key == null
      ? child.isComment
        ? id + 'comment'
        : id + child.tag
      : isPrimitive(child.key)
        ? (String(child.key).indexOf(id) === 0 ? child.key : id + child.key)
        : child.key;

    var data = (child.data || (child.data = {})).transition = extractTransitionData(this);
    var oldRawChild = this._vnode;
    var oldChild = getRealChild(oldRawChild);

    // mark v-show
    // so that the transition module can hand over the control to the directive
    if (child.data.directives && child.data.directives.some(function (d) { return d.name === 'show'; })) {
      child.data.show = true;
    }

    if (
      oldChild &&
      oldChild.data &&
      !isSameChild(child, oldChild) &&
      !isAsyncPlaceholder(oldChild) &&
      // #6687 component root is a comment node
      !(oldChild.componentInstance && oldChild.componentInstance._vnode.isComment)
    ) {
      // replace old child transition data with fresh one
      // important for dynamic transitions!
      var oldData = oldChild.data.transition = extend({}, data);
      // handle transition mode
      if (mode === 'out-in') {
        // return placeholder node and queue update when leave finishes
        this._leaving = true;
        mergeVNodeHook(oldData, 'afterLeave', function () {
          this$1._leaving = false;
          this$1.$forceUpdate();
        });
        return placeholder(h, rawChild)
      } else if (mode === 'in-out') {
        if (isAsyncPlaceholder(child)) {
          return oldRawChild
        }
        var delayedLeave;
        var performLeave = function () { delayedLeave(); };
        mergeVNodeHook(data, 'afterEnter', performLeave);
        mergeVNodeHook(data, 'enterCancelled', performLeave);
        mergeVNodeHook(oldData, 'delayLeave', function (leave) { delayedLeave = leave; });
      }
    }

    return rawChild
  }
};

/*  */

// Provides transition support for list items.
// supports move transitions using the FLIP technique.

// Because the vdom's children update algorithm is "unstable" - i.e.
// it doesn't guarantee the relative positioning of removed elements,
// we force transition-group to update its children into two passes:
// in the first pass, we remove all nodes that need to be removed,
// triggering their leaving transition; in the second pass, we insert/move
// into the final desired state. This way in the second pass removed
// nodes will remain where they should be.

var props = extend({
  tag: String,
  moveClass: String
}, transitionProps);

delete props.mode;

var TransitionGroup = {
  props: props,

  render: function render (h) {
    var tag = this.tag || this.$vnode.data.tag || 'span';
    var map = Object.create(null);
    var prevChildren = this.prevChildren = this.children;
    var rawChildren = this.$slots.default || [];
    var children = this.children = [];
    var transitionData = extractTransitionData(this);

    for (var i = 0; i < rawChildren.length; i++) {
      var c = rawChildren[i];
      if (c.tag) {
        if (c.key != null && String(c.key).indexOf('__vlist') !== 0) {
          children.push(c);
          map[c.key] = c
          ;(c.data || (c.data = {})).transition = transitionData;
        } else if (true) {
          var opts = c.componentOptions;
          var name = opts ? (opts.Ctor.options.name || opts.tag || '') : c.tag;
          warn(("<transition-group> children must be keyed: <" + name + ">"));
        }
      }
    }

    if (prevChildren) {
      var kept = [];
      var removed = [];
      for (var i$1 = 0; i$1 < prevChildren.length; i$1++) {
        var c$1 = prevChildren[i$1];
        c$1.data.transition = transitionData;
        c$1.data.pos = c$1.elm.getBoundingClientRect();
        if (map[c$1.key]) {
          kept.push(c$1);
        } else {
          removed.push(c$1);
        }
      }
      this.kept = h(tag, null, kept);
      this.removed = removed;
    }

    return h(tag, null, children)
  },

  beforeUpdate: function beforeUpdate () {
    // force removing pass
    this.__patch__(
      this._vnode,
      this.kept,
      false, // hydrating
      true // removeOnly (!important avoids unnecessary moves)
    );
    this._vnode = this.kept;
  },

  updated: function updated () {
    var children = this.prevChildren;
    var moveClass = this.moveClass || ((this.name || 'v') + '-move');
    if (!children.length || !this.hasMove(children[0].elm, moveClass)) {
      return
    }

    // we divide the work into three loops to avoid mixing DOM reads and writes
    // in each iteration - which helps prevent layout thrashing.
    children.forEach(callPendingCbs);
    children.forEach(recordPosition);
    children.forEach(applyTranslation);

    // force reflow to put everything in position
    // assign to this to avoid being removed in tree-shaking
    // $flow-disable-line
    this._reflow = document.body.offsetHeight;

    children.forEach(function (c) {
      if (c.data.moved) {
        var el = c.elm;
        var s = el.style;
        addTransitionClass(el, moveClass);
        s.transform = s.WebkitTransform = s.transitionDuration = '';
        el.addEventListener(transitionEndEvent, el._moveCb = function cb (e) {
          if (!e || /transform$/.test(e.propertyName)) {
            el.removeEventListener(transitionEndEvent, cb);
            el._moveCb = null;
            removeTransitionClass(el, moveClass);
          }
        });
      }
    });
  },

  methods: {
    hasMove: function hasMove (el, moveClass) {
      /* istanbul ignore if */
      if (!hasTransition) {
        return false
      }
      /* istanbul ignore if */
      if (this._hasMove) {
        return this._hasMove
      }
      // Detect whether an element with the move class applied has
      // CSS transitions. Since the element may be inside an entering
      // transition at this very moment, we make a clone of it and remove
      // all other transition classes applied to ensure only the move class
      // is applied.
      var clone = el.cloneNode();
      if (el._transitionClasses) {
        el._transitionClasses.forEach(function (cls) { removeClass(clone, cls); });
      }
      addClass(clone, moveClass);
      clone.style.display = 'none';
      this.$el.appendChild(clone);
      var info = getTransitionInfo(clone);
      this.$el.removeChild(clone);
      return (this._hasMove = info.hasTransform)
    }
  }
};

function callPendingCbs (c) {
  /* istanbul ignore if */
  if (c.elm._moveCb) {
    c.elm._moveCb();
  }
  /* istanbul ignore if */
  if (c.elm._enterCb) {
    c.elm._enterCb();
  }
}

function recordPosition (c) {
  c.data.newPos = c.elm.getBoundingClientRect();
}

function applyTranslation (c) {
  var oldPos = c.data.pos;
  var newPos = c.data.newPos;
  var dx = oldPos.left - newPos.left;
  var dy = oldPos.top - newPos.top;
  if (dx || dy) {
    c.data.moved = true;
    var s = c.elm.style;
    s.transform = s.WebkitTransform = "translate(" + dx + "px," + dy + "px)";
    s.transitionDuration = '0s';
  }
}

var platformComponents = {
  Transition: Transition,
  TransitionGroup: TransitionGroup
};

/*  */

// install platform specific utils
Vue$3.config.mustUseProp = mustUseProp;
Vue$3.config.isReservedTag = isReservedTag;
Vue$3.config.isReservedAttr = isReservedAttr;
Vue$3.config.getTagNamespace = getTagNamespace;
Vue$3.config.isUnknownElement = isUnknownElement;

// install platform runtime directives & components
extend(Vue$3.options.directives, platformDirectives);
extend(Vue$3.options.components, platformComponents);

// install platform patch function
Vue$3.prototype.__patch__ = inBrowser ? patch : noop;

// public mount method
Vue$3.prototype.$mount = function (
  el,
  hydrating
) {
  el = el && inBrowser ? query(el) : undefined;
  return mountComponent(this, el, hydrating)
};

// devtools global hook
/* istanbul ignore next */
Vue$3.nextTick(function () {
  if (config.devtools) {
    if (devtools) {
      devtools.emit('init', Vue$3);
    } else if ("development" !== 'production' && isChrome) {
      console[console.info ? 'info' : 'log'](
        'Download the Vue Devtools extension for a better development experience:\n' +
        'https://github.com/vuejs/vue-devtools'
      );
    }
  }
  if ("development" !== 'production' &&
    config.productionTip !== false &&
    inBrowser && typeof console !== 'undefined'
  ) {
    console[console.info ? 'info' : 'log'](
      "You are running Vue in development mode.\n" +
      "Make sure to turn on production mode when deploying for production.\n" +
      "See more tips at https://vuejs.org/guide/deployment.html"
    );
  }
}, 0);

/*  */

var defaultTagRE = /\{\{((?:.|\n)+?)\}\}/g;
var regexEscapeRE = /[-.*+?^${}()|[\]\/\\]/g;

var buildRegex = cached(function (delimiters) {
  var open = delimiters[0].replace(regexEscapeRE, '\\$&');
  var close = delimiters[1].replace(regexEscapeRE, '\\$&');
  return new RegExp(open + '((?:.|\\n)+?)' + close, 'g')
});



function parseText (
  text,
  delimiters
) {
  var tagRE = delimiters ? buildRegex(delimiters) : defaultTagRE;
  if (!tagRE.test(text)) {
    return
  }
  var tokens = [];
  var rawTokens = [];
  var lastIndex = tagRE.lastIndex = 0;
  var match, index, tokenValue;
  while ((match = tagRE.exec(text))) {
    index = match.index;
    // push text token
    if (index > lastIndex) {
      rawTokens.push(tokenValue = text.slice(lastIndex, index));
      tokens.push(JSON.stringify(tokenValue));
    }
    // tag token
    var exp = parseFilters(match[1].trim());
    tokens.push(("_s(" + exp + ")"));
    rawTokens.push({ '@binding': exp });
    lastIndex = index + match[0].length;
  }
  if (lastIndex < text.length) {
    rawTokens.push(tokenValue = text.slice(lastIndex));
    tokens.push(JSON.stringify(tokenValue));
  }
  return {
    expression: tokens.join('+'),
    tokens: rawTokens
  }
}

/*  */

function transformNode (el, options) {
  var warn = options.warn || baseWarn;
  var staticClass = getAndRemoveAttr(el, 'class');
  if ("development" !== 'production' && staticClass) {
    var res = parseText(staticClass, options.delimiters);
    if (res) {
      warn(
        "class=\"" + staticClass + "\": " +
        'Interpolation inside attributes has been removed. ' +
        'Use v-bind or the colon shorthand instead. For example, ' +
        'instead of <div class="{{ val }}">, use <div :class="val">.'
      );
    }
  }
  if (staticClass) {
    el.staticClass = JSON.stringify(staticClass);
  }
  var classBinding = getBindingAttr(el, 'class', false /* getStatic */);
  if (classBinding) {
    el.classBinding = classBinding;
  }
}

function genData (el) {
  var data = '';
  if (el.staticClass) {
    data += "staticClass:" + (el.staticClass) + ",";
  }
  if (el.classBinding) {
    data += "class:" + (el.classBinding) + ",";
  }
  return data
}

var klass$1 = {
  staticKeys: ['staticClass'],
  transformNode: transformNode,
  genData: genData
};

/*  */

function transformNode$1 (el, options) {
  var warn = options.warn || baseWarn;
  var staticStyle = getAndRemoveAttr(el, 'style');
  if (staticStyle) {
    /* istanbul ignore if */
    if (true) {
      var res = parseText(staticStyle, options.delimiters);
      if (res) {
        warn(
          "style=\"" + staticStyle + "\": " +
          'Interpolation inside attributes has been removed. ' +
          'Use v-bind or the colon shorthand instead. For example, ' +
          'instead of <div style="{{ val }}">, use <div :style="val">.'
        );
      }
    }
    el.staticStyle = JSON.stringify(parseStyleText(staticStyle));
  }

  var styleBinding = getBindingAttr(el, 'style', false /* getStatic */);
  if (styleBinding) {
    el.styleBinding = styleBinding;
  }
}

function genData$1 (el) {
  var data = '';
  if (el.staticStyle) {
    data += "staticStyle:" + (el.staticStyle) + ",";
  }
  if (el.styleBinding) {
    data += "style:(" + (el.styleBinding) + "),";
  }
  return data
}

var style$1 = {
  staticKeys: ['staticStyle'],
  transformNode: transformNode$1,
  genData: genData$1
};

/*  */

var decoder;

var he = {
  decode: function decode (html) {
    decoder = decoder || document.createElement('div');
    decoder.innerHTML = html;
    return decoder.textContent
  }
};

/*  */

var isUnaryTag = makeMap(
  'area,base,br,col,embed,frame,hr,img,input,isindex,keygen,' +
  'link,meta,param,source,track,wbr'
);

// Elements that you can, intentionally, leave open
// (and which close themselves)
var canBeLeftOpenTag = makeMap(
  'colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr,source'
);

// HTML5 tags https://html.spec.whatwg.org/multipage/indices.html#elements-3
// Phrasing Content https://html.spec.whatwg.org/multipage/dom.html#phrasing-content
var isNonPhrasingTag = makeMap(
  'address,article,aside,base,blockquote,body,caption,col,colgroup,dd,' +
  'details,dialog,div,dl,dt,fieldset,figcaption,figure,footer,form,' +
  'h1,h2,h3,h4,h5,h6,head,header,hgroup,hr,html,legend,li,menuitem,meta,' +
  'optgroup,option,param,rp,rt,source,style,summary,tbody,td,tfoot,th,thead,' +
  'title,tr,track'
);

/**
 * Not type-checking this file because it's mostly vendor code.
 */

/*!
 * HTML Parser By John Resig (ejohn.org)
 * Modified by Juriy "kangax" Zaytsev
 * Original code by Erik Arvidsson, Mozilla Public License
 * http://erik.eae.net/simplehtmlparser/simplehtmlparser.js
 */

// Regular Expressions for parsing tags and attributes
var attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/;
// could use https://www.w3.org/TR/1999/REC-xml-names-19990114/#NT-QName
// but for Vue templates we can enforce a simple charset
var ncname = '[a-zA-Z_][\\w\\-\\.]*';
var qnameCapture = "((?:" + ncname + "\\:)?" + ncname + ")";
var startTagOpen = new RegExp(("^<" + qnameCapture));
var startTagClose = /^\s*(\/?)>/;
var endTag = new RegExp(("^<\\/" + qnameCapture + "[^>]*>"));
var doctype = /^<!DOCTYPE [^>]+>/i;
var comment = /^<!--/;
var conditionalComment = /^<!\[/;

var IS_REGEX_CAPTURING_BROKEN = false;
'x'.replace(/x(.)?/g, function (m, g) {
  IS_REGEX_CAPTURING_BROKEN = g === '';
});

// Special Elements (can contain anything)
var isPlainTextElement = makeMap('script,style,textarea', true);
var reCache = {};

var decodingMap = {
  '&lt;': '<',
  '&gt;': '>',
  '&quot;': '"',
  '&amp;': '&',
  '&#10;': '\n',
  '&#9;': '\t'
};
var encodedAttr = /&(?:lt|gt|quot|amp);/g;
var encodedAttrWithNewLines = /&(?:lt|gt|quot|amp|#10|#9);/g;

// #5992
var isIgnoreNewlineTag = makeMap('pre,textarea', true);
var shouldIgnoreFirstNewline = function (tag, html) { return tag && isIgnoreNewlineTag(tag) && html[0] === '\n'; };

function decodeAttr (value, shouldDecodeNewlines) {
  var re = shouldDecodeNewlines ? encodedAttrWithNewLines : encodedAttr;
  return value.replace(re, function (match) { return decodingMap[match]; })
}

function parseHTML (html, options) {
  var stack = [];
  var expectHTML = options.expectHTML;
  var isUnaryTag$$1 = options.isUnaryTag || no;
  var canBeLeftOpenTag$$1 = options.canBeLeftOpenTag || no;
  var index = 0;
  var last, lastTag;
  while (html) {
    last = html;
    // Make sure we're not in a plaintext content element like script/style
    if (!lastTag || !isPlainTextElement(lastTag)) {
      var textEnd = html.indexOf('<');
      if (textEnd === 0) {
        // Comment:
        if (comment.test(html)) {
          var commentEnd = html.indexOf('-->');

          if (commentEnd >= 0) {
            if (options.shouldKeepComment) {
              options.comment(html.substring(4, commentEnd));
            }
            advance(commentEnd + 3);
            continue
          }
        }

        // http://en.wikipedia.org/wiki/Conditional_comment#Downlevel-revealed_conditional_comment
        if (conditionalComment.test(html)) {
          var conditionalEnd = html.indexOf(']>');

          if (conditionalEnd >= 0) {
            advance(conditionalEnd + 2);
            continue
          }
        }

        // Doctype:
        var doctypeMatch = html.match(doctype);
        if (doctypeMatch) {
          advance(doctypeMatch[0].length);
          continue
        }

        // End tag:
        var endTagMatch = html.match(endTag);
        if (endTagMatch) {
          var curIndex = index;
          advance(endTagMatch[0].length);
          parseEndTag(endTagMatch[1], curIndex, index);
          continue
        }

        // Start tag:
        var startTagMatch = parseStartTag();
        if (startTagMatch) {
          handleStartTag(startTagMatch);
          if (shouldIgnoreFirstNewline(lastTag, html)) {
            advance(1);
          }
          continue
        }
      }

      var text = (void 0), rest = (void 0), next = (void 0);
      if (textEnd >= 0) {
        rest = html.slice(textEnd);
        while (
          !endTag.test(rest) &&
          !startTagOpen.test(rest) &&
          !comment.test(rest) &&
          !conditionalComment.test(rest)
        ) {
          // < in plain text, be forgiving and treat it as text
          next = rest.indexOf('<', 1);
          if (next < 0) { break }
          textEnd += next;
          rest = html.slice(textEnd);
        }
        text = html.substring(0, textEnd);
        advance(textEnd);
      }

      if (textEnd < 0) {
        text = html;
        html = '';
      }

      if (options.chars && text) {
        options.chars(text);
      }
    } else {
      var endTagLength = 0;
      var stackedTag = lastTag.toLowerCase();
      var reStackedTag = reCache[stackedTag] || (reCache[stackedTag] = new RegExp('([\\s\\S]*?)(</' + stackedTag + '[^>]*>)', 'i'));
      var rest$1 = html.replace(reStackedTag, function (all, text, endTag) {
        endTagLength = endTag.length;
        if (!isPlainTextElement(stackedTag) && stackedTag !== 'noscript') {
          text = text
            .replace(/<!--([\s\S]*?)-->/g, '$1')
            .replace(/<!\[CDATA\[([\s\S]*?)]]>/g, '$1');
        }
        if (shouldIgnoreFirstNewline(stackedTag, text)) {
          text = text.slice(1);
        }
        if (options.chars) {
          options.chars(text);
        }
        return ''
      });
      index += html.length - rest$1.length;
      html = rest$1;
      parseEndTag(stackedTag, index - endTagLength, index);
    }

    if (html === last) {
      options.chars && options.chars(html);
      if ("development" !== 'production' && !stack.length && options.warn) {
        options.warn(("Mal-formatted tag at end of template: \"" + html + "\""));
      }
      break
    }
  }

  // Clean up any remaining tags
  parseEndTag();

  function advance (n) {
    index += n;
    html = html.substring(n);
  }

  function parseStartTag () {
    var start = html.match(startTagOpen);
    if (start) {
      var match = {
        tagName: start[1],
        attrs: [],
        start: index
      };
      advance(start[0].length);
      var end, attr;
      while (!(end = html.match(startTagClose)) && (attr = html.match(attribute))) {
        advance(attr[0].length);
        match.attrs.push(attr);
      }
      if (end) {
        match.unarySlash = end[1];
        advance(end[0].length);
        match.end = index;
        return match
      }
    }
  }

  function handleStartTag (match) {
    var tagName = match.tagName;
    var unarySlash = match.unarySlash;

    if (expectHTML) {
      if (lastTag === 'p' && isNonPhrasingTag(tagName)) {
        parseEndTag(lastTag);
      }
      if (canBeLeftOpenTag$$1(tagName) && lastTag === tagName) {
        parseEndTag(tagName);
      }
    }

    var unary = isUnaryTag$$1(tagName) || !!unarySlash;

    var l = match.attrs.length;
    var attrs = new Array(l);
    for (var i = 0; i < l; i++) {
      var args = match.attrs[i];
      // hackish work around FF bug https://bugzilla.mozilla.org/show_bug.cgi?id=369778
      if (IS_REGEX_CAPTURING_BROKEN && args[0].indexOf('""') === -1) {
        if (args[3] === '') { delete args[3]; }
        if (args[4] === '') { delete args[4]; }
        if (args[5] === '') { delete args[5]; }
      }
      var value = args[3] || args[4] || args[5] || '';
      var shouldDecodeNewlines = tagName === 'a' && args[1] === 'href'
        ? options.shouldDecodeNewlinesForHref
        : options.shouldDecodeNewlines;
      attrs[i] = {
        name: args[1],
        value: decodeAttr(value, shouldDecodeNewlines)
      };
    }

    if (!unary) {
      stack.push({ tag: tagName, lowerCasedTag: tagName.toLowerCase(), attrs: attrs });
      lastTag = tagName;
    }

    if (options.start) {
      options.start(tagName, attrs, unary, match.start, match.end);
    }
  }

  function parseEndTag (tagName, start, end) {
    var pos, lowerCasedTagName;
    if (start == null) { start = index; }
    if (end == null) { end = index; }

    if (tagName) {
      lowerCasedTagName = tagName.toLowerCase();
    }

    // Find the closest opened tag of the same type
    if (tagName) {
      for (pos = stack.length - 1; pos >= 0; pos--) {
        if (stack[pos].lowerCasedTag === lowerCasedTagName) {
          break
        }
      }
    } else {
      // If no tag name is provided, clean shop
      pos = 0;
    }

    if (pos >= 0) {
      // Close all the open elements, up the stack
      for (var i = stack.length - 1; i >= pos; i--) {
        if ("development" !== 'production' &&
          (i > pos || !tagName) &&
          options.warn
        ) {
          options.warn(
            ("tag <" + (stack[i].tag) + "> has no matching end tag.")
          );
        }
        if (options.end) {
          options.end(stack[i].tag, start, end);
        }
      }

      // Remove the open elements from the stack
      stack.length = pos;
      lastTag = pos && stack[pos - 1].tag;
    } else if (lowerCasedTagName === 'br') {
      if (options.start) {
        options.start(tagName, [], true, start, end);
      }
    } else if (lowerCasedTagName === 'p') {
      if (options.start) {
        options.start(tagName, [], false, start, end);
      }
      if (options.end) {
        options.end(tagName, start, end);
      }
    }
  }
}

/*  */

var onRE = /^@|^v-on:/;
var dirRE = /^v-|^@|^:/;
var forAliasRE = /(.*?)\s+(?:in|of)\s+(.*)/;
var forIteratorRE = /,([^,\}\]]*)(?:,([^,\}\]]*))?$/;
var stripParensRE = /^\(|\)$/g;

var argRE = /:(.*)$/;
var bindRE = /^:|^v-bind:/;
var modifierRE = /\.[^.]+/g;

var decodeHTMLCached = cached(he.decode);

// configurable state
var warn$2;
var delimiters;
var transforms;
var preTransforms;
var postTransforms;
var platformIsPreTag;
var platformMustUseProp;
var platformGetTagNamespace;



function createASTElement (
  tag,
  attrs,
  parent
) {
  return {
    type: 1,
    tag: tag,
    attrsList: attrs,
    attrsMap: makeAttrsMap(attrs),
    parent: parent,
    children: []
  }
}

/**
 * Convert HTML string to AST.
 */
function parse (
  template,
  options
) {
  warn$2 = options.warn || baseWarn;

  platformIsPreTag = options.isPreTag || no;
  platformMustUseProp = options.mustUseProp || no;
  platformGetTagNamespace = options.getTagNamespace || no;

  transforms = pluckModuleFunction(options.modules, 'transformNode');
  preTransforms = pluckModuleFunction(options.modules, 'preTransformNode');
  postTransforms = pluckModuleFunction(options.modules, 'postTransformNode');

  delimiters = options.delimiters;

  var stack = [];
  var preserveWhitespace = options.preserveWhitespace !== false;
  var root;
  var currentParent;
  var inVPre = false;
  var inPre = false;
  var warned = false;

  function warnOnce (msg) {
    if (!warned) {
      warned = true;
      warn$2(msg);
    }
  }

  function closeElement (element) {
    // check pre state
    if (element.pre) {
      inVPre = false;
    }
    if (platformIsPreTag(element.tag)) {
      inPre = false;
    }
    // apply post-transforms
    for (var i = 0; i < postTransforms.length; i++) {
      postTransforms[i](element, options);
    }
  }

  parseHTML(template, {
    warn: warn$2,
    expectHTML: options.expectHTML,
    isUnaryTag: options.isUnaryTag,
    canBeLeftOpenTag: options.canBeLeftOpenTag,
    shouldDecodeNewlines: options.shouldDecodeNewlines,
    shouldDecodeNewlinesForHref: options.shouldDecodeNewlinesForHref,
    shouldKeepComment: options.comments,
    start: function start (tag, attrs, unary) {
      // check namespace.
      // inherit parent ns if there is one
      var ns = (currentParent && currentParent.ns) || platformGetTagNamespace(tag);

      // handle IE svg bug
      /* istanbul ignore if */
      if (isIE && ns === 'svg') {
        attrs = guardIESVGBug(attrs);
      }

      var element = createASTElement(tag, attrs, currentParent);
      if (ns) {
        element.ns = ns;
      }

      if (isForbiddenTag(element) && !isServerRendering()) {
        element.forbidden = true;
        "development" !== 'production' && warn$2(
          'Templates should only be responsible for mapping the state to the ' +
          'UI. Avoid placing tags with side-effects in your templates, such as ' +
          "<" + tag + ">" + ', as they will not be parsed.'
        );
      }

      // apply pre-transforms
      for (var i = 0; i < preTransforms.length; i++) {
        element = preTransforms[i](element, options) || element;
      }

      if (!inVPre) {
        processPre(element);
        if (element.pre) {
          inVPre = true;
        }
      }
      if (platformIsPreTag(element.tag)) {
        inPre = true;
      }
      if (inVPre) {
        processRawAttrs(element);
      } else if (!element.processed) {
        // structural directives
        processFor(element);
        processIf(element);
        processOnce(element);
        // element-scope stuff
        processElement(element, options);
      }

      function checkRootConstraints (el) {
        if (true) {
          if (el.tag === 'slot' || el.tag === 'template') {
            warnOnce(
              "Cannot use <" + (el.tag) + "> as component root element because it may " +
              'contain multiple nodes.'
            );
          }
          if (el.attrsMap.hasOwnProperty('v-for')) {
            warnOnce(
              'Cannot use v-for on stateful component root element because ' +
              'it renders multiple elements.'
            );
          }
        }
      }

      // tree management
      if (!root) {
        root = element;
        checkRootConstraints(root);
      } else if (!stack.length) {
        // allow root elements with v-if, v-else-if and v-else
        if (root.if && (element.elseif || element.else)) {
          checkRootConstraints(element);
          addIfCondition(root, {
            exp: element.elseif,
            block: element
          });
        } else if (true) {
          warnOnce(
            "Component template should contain exactly one root element. " +
            "If you are using v-if on multiple elements, " +
            "use v-else-if to chain them instead."
          );
        }
      }
      if (currentParent && !element.forbidden) {
        if (element.elseif || element.else) {
          processIfConditions(element, currentParent);
        } else if (element.slotScope) { // scoped slot
          currentParent.plain = false;
          var name = element.slotTarget || '"default"';(currentParent.scopedSlots || (currentParent.scopedSlots = {}))[name] = element;
        } else {
          currentParent.children.push(element);
          element.parent = currentParent;
        }
      }
      if (!unary) {
        currentParent = element;
        stack.push(element);
      } else {
        closeElement(element);
      }
    },

    end: function end () {
      // remove trailing whitespace
      var element = stack[stack.length - 1];
      var lastNode = element.children[element.children.length - 1];
      if (lastNode && lastNode.type === 3 && lastNode.text === ' ' && !inPre) {
        element.children.pop();
      }
      // pop stack
      stack.length -= 1;
      currentParent = stack[stack.length - 1];
      closeElement(element);
    },

    chars: function chars (text) {
      if (!currentParent) {
        if (true) {
          if (text === template) {
            warnOnce(
              'Component template requires a root element, rather than just text.'
            );
          } else if ((text = text.trim())) {
            warnOnce(
              ("text \"" + text + "\" outside root element will be ignored.")
            );
          }
        }
        return
      }
      // IE textarea placeholder bug
      /* istanbul ignore if */
      if (isIE &&
        currentParent.tag === 'textarea' &&
        currentParent.attrsMap.placeholder === text
      ) {
        return
      }
      var children = currentParent.children;
      text = inPre || text.trim()
        ? isTextTag(currentParent) ? text : decodeHTMLCached(text)
        // only preserve whitespace if its not right after a starting tag
        : preserveWhitespace && children.length ? ' ' : '';
      if (text) {
        var res;
        if (!inVPre && text !== ' ' && (res = parseText(text, delimiters))) {
          children.push({
            type: 2,
            expression: res.expression,
            tokens: res.tokens,
            text: text
          });
        } else if (text !== ' ' || !children.length || children[children.length - 1].text !== ' ') {
          children.push({
            type: 3,
            text: text
          });
        }
      }
    },
    comment: function comment (text) {
      currentParent.children.push({
        type: 3,
        text: text,
        isComment: true
      });
    }
  });
  return root
}

function processPre (el) {
  if (getAndRemoveAttr(el, 'v-pre') != null) {
    el.pre = true;
  }
}

function processRawAttrs (el) {
  var l = el.attrsList.length;
  if (l) {
    var attrs = el.attrs = new Array(l);
    for (var i = 0; i < l; i++) {
      attrs[i] = {
        name: el.attrsList[i].name,
        value: JSON.stringify(el.attrsList[i].value)
      };
    }
  } else if (!el.pre) {
    // non root node in pre blocks with no attributes
    el.plain = true;
  }
}

function processElement (element, options) {
  processKey(element);

  // determine whether this is a plain element after
  // removing structural attributes
  element.plain = !element.key && !element.attrsList.length;

  processRef(element);
  processSlot(element);
  processComponent(element);
  for (var i = 0; i < transforms.length; i++) {
    element = transforms[i](element, options) || element;
  }
  processAttrs(element);
}

function processKey (el) {
  var exp = getBindingAttr(el, 'key');
  if (exp) {
    if ("development" !== 'production' && el.tag === 'template') {
      warn$2("<template> cannot be keyed. Place the key on real elements instead.");
    }
    el.key = exp;
  }
}

function processRef (el) {
  var ref = getBindingAttr(el, 'ref');
  if (ref) {
    el.ref = ref;
    el.refInFor = checkInFor(el);
  }
}

function processFor (el) {
  var exp;
  if ((exp = getAndRemoveAttr(el, 'v-for'))) {
    var res = parseFor(exp);
    if (res) {
      extend(el, res);
    } else if (true) {
      warn$2(
        ("Invalid v-for expression: " + exp)
      );
    }
  }
}

function parseFor (exp) {
  var inMatch = exp.match(forAliasRE);
  if (!inMatch) { return }
  var res = {};
  res.for = inMatch[2].trim();
  var alias = inMatch[1].trim().replace(stripParensRE, '');
  var iteratorMatch = alias.match(forIteratorRE);
  if (iteratorMatch) {
    res.alias = alias.replace(forIteratorRE, '');
    res.iterator1 = iteratorMatch[1].trim();
    if (iteratorMatch[2]) {
      res.iterator2 = iteratorMatch[2].trim();
    }
  } else {
    res.alias = alias;
  }
  return res
}

function processIf (el) {
  var exp = getAndRemoveAttr(el, 'v-if');
  if (exp) {
    el.if = exp;
    addIfCondition(el, {
      exp: exp,
      block: el
    });
  } else {
    if (getAndRemoveAttr(el, 'v-else') != null) {
      el.else = true;
    }
    var elseif = getAndRemoveAttr(el, 'v-else-if');
    if (elseif) {
      el.elseif = elseif;
    }
  }
}

function processIfConditions (el, parent) {
  var prev = findPrevElement(parent.children);
  if (prev && prev.if) {
    addIfCondition(prev, {
      exp: el.elseif,
      block: el
    });
  } else if (true) {
    warn$2(
      "v-" + (el.elseif ? ('else-if="' + el.elseif + '"') : 'else') + " " +
      "used on element <" + (el.tag) + "> without corresponding v-if."
    );
  }
}

function findPrevElement (children) {
  var i = children.length;
  while (i--) {
    if (children[i].type === 1) {
      return children[i]
    } else {
      if ("development" !== 'production' && children[i].text !== ' ') {
        warn$2(
          "text \"" + (children[i].text.trim()) + "\" between v-if and v-else(-if) " +
          "will be ignored."
        );
      }
      children.pop();
    }
  }
}

function addIfCondition (el, condition) {
  if (!el.ifConditions) {
    el.ifConditions = [];
  }
  el.ifConditions.push(condition);
}

function processOnce (el) {
  var once$$1 = getAndRemoveAttr(el, 'v-once');
  if (once$$1 != null) {
    el.once = true;
  }
}

function processSlot (el) {
  if (el.tag === 'slot') {
    el.slotName = getBindingAttr(el, 'name');
    if ("development" !== 'production' && el.key) {
      warn$2(
        "`key` does not work on <slot> because slots are abstract outlets " +
        "and can possibly expand into multiple elements. " +
        "Use the key on a wrapping element instead."
      );
    }
  } else {
    var slotScope;
    if (el.tag === 'template') {
      slotScope = getAndRemoveAttr(el, 'scope');
      /* istanbul ignore if */
      if ("development" !== 'production' && slotScope) {
        warn$2(
          "the \"scope\" attribute for scoped slots have been deprecated and " +
          "replaced by \"slot-scope\" since 2.5. The new \"slot-scope\" attribute " +
          "can also be used on plain elements in addition to <template> to " +
          "denote scoped slots.",
          true
        );
      }
      el.slotScope = slotScope || getAndRemoveAttr(el, 'slot-scope');
    } else if ((slotScope = getAndRemoveAttr(el, 'slot-scope'))) {
      /* istanbul ignore if */
      if ("development" !== 'production' && el.attrsMap['v-for']) {
        warn$2(
          "Ambiguous combined usage of slot-scope and v-for on <" + (el.tag) + "> " +
          "(v-for takes higher priority). Use a wrapper <template> for the " +
          "scoped slot to make it clearer.",
          true
        );
      }
      el.slotScope = slotScope;
    }
    var slotTarget = getBindingAttr(el, 'slot');
    if (slotTarget) {
      el.slotTarget = slotTarget === '""' ? '"default"' : slotTarget;
      // preserve slot as an attribute for native shadow DOM compat
      // only for non-scoped slots.
      if (el.tag !== 'template' && !el.slotScope) {
        addAttr(el, 'slot', slotTarget);
      }
    }
  }
}

function processComponent (el) {
  var binding;
  if ((binding = getBindingAttr(el, 'is'))) {
    el.component = binding;
  }
  if (getAndRemoveAttr(el, 'inline-template') != null) {
    el.inlineTemplate = true;
  }
}

function processAttrs (el) {
  var list = el.attrsList;
  var i, l, name, rawName, value, modifiers, isProp;
  for (i = 0, l = list.length; i < l; i++) {
    name = rawName = list[i].name;
    value = list[i].value;
    if (dirRE.test(name)) {
      // mark element as dynamic
      el.hasBindings = true;
      // modifiers
      modifiers = parseModifiers(name);
      if (modifiers) {
        name = name.replace(modifierRE, '');
      }
      if (bindRE.test(name)) { // v-bind
        name = name.replace(bindRE, '');
        value = parseFilters(value);
        isProp = false;
        if (modifiers) {
          if (modifiers.prop) {
            isProp = true;
            name = camelize(name);
            if (name === 'innerHtml') { name = 'innerHTML'; }
          }
          if (modifiers.camel) {
            name = camelize(name);
          }
          if (modifiers.sync) {
            addHandler(
              el,
              ("update:" + (camelize(name))),
              genAssignmentCode(value, "$event")
            );
          }
        }
        if (isProp || (
          !el.component && platformMustUseProp(el.tag, el.attrsMap.type, name)
        )) {
          addProp(el, name, value);
        } else {
          addAttr(el, name, value);
        }
      } else if (onRE.test(name)) { // v-on
        name = name.replace(onRE, '');
        addHandler(el, name, value, modifiers, false, warn$2);
      } else { // normal directives
        name = name.replace(dirRE, '');
        // parse arg
        var argMatch = name.match(argRE);
        var arg = argMatch && argMatch[1];
        if (arg) {
          name = name.slice(0, -(arg.length + 1));
        }
        addDirective(el, name, rawName, value, arg, modifiers);
        if ("development" !== 'production' && name === 'model') {
          checkForAliasModel(el, value);
        }
      }
    } else {
      // literal attribute
      if (true) {
        var res = parseText(value, delimiters);
        if (res) {
          warn$2(
            name + "=\"" + value + "\": " +
            'Interpolation inside attributes has been removed. ' +
            'Use v-bind or the colon shorthand instead. For example, ' +
            'instead of <div id="{{ val }}">, use <div :id="val">.'
          );
        }
      }
      addAttr(el, name, JSON.stringify(value));
      // #6887 firefox doesn't update muted state if set via attribute
      // even immediately after element creation
      if (!el.component &&
          name === 'muted' &&
          platformMustUseProp(el.tag, el.attrsMap.type, name)) {
        addProp(el, name, 'true');
      }
    }
  }
}

function checkInFor (el) {
  var parent = el;
  while (parent) {
    if (parent.for !== undefined) {
      return true
    }
    parent = parent.parent;
  }
  return false
}

function parseModifiers (name) {
  var match = name.match(modifierRE);
  if (match) {
    var ret = {};
    match.forEach(function (m) { ret[m.slice(1)] = true; });
    return ret
  }
}

function makeAttrsMap (attrs) {
  var map = {};
  for (var i = 0, l = attrs.length; i < l; i++) {
    if (
      "development" !== 'production' &&
      map[attrs[i].name] && !isIE && !isEdge
    ) {
      warn$2('duplicate attribute: ' + attrs[i].name);
    }
    map[attrs[i].name] = attrs[i].value;
  }
  return map
}

// for script (e.g. type="x/template") or style, do not decode content
function isTextTag (el) {
  return el.tag === 'script' || el.tag === 'style'
}

function isForbiddenTag (el) {
  return (
    el.tag === 'style' ||
    (el.tag === 'script' && (
      !el.attrsMap.type ||
      el.attrsMap.type === 'text/javascript'
    ))
  )
}

var ieNSBug = /^xmlns:NS\d+/;
var ieNSPrefix = /^NS\d+:/;

/* istanbul ignore next */
function guardIESVGBug (attrs) {
  var res = [];
  for (var i = 0; i < attrs.length; i++) {
    var attr = attrs[i];
    if (!ieNSBug.test(attr.name)) {
      attr.name = attr.name.replace(ieNSPrefix, '');
      res.push(attr);
    }
  }
  return res
}

function checkForAliasModel (el, value) {
  var _el = el;
  while (_el) {
    if (_el.for && _el.alias === value) {
      warn$2(
        "<" + (el.tag) + " v-model=\"" + value + "\">: " +
        "You are binding v-model directly to a v-for iteration alias. " +
        "This will not be able to modify the v-for source array because " +
        "writing to the alias is like modifying a function local variable. " +
        "Consider using an array of objects and use v-model on an object property instead."
      );
    }
    _el = _el.parent;
  }
}

/*  */

/**
 * Expand input[v-model] with dyanmic type bindings into v-if-else chains
 * Turn this:
 *   <input v-model="data[type]" :type="type">
 * into this:
 *   <input v-if="type === 'checkbox'" type="checkbox" v-model="data[type]">
 *   <input v-else-if="type === 'radio'" type="radio" v-model="data[type]">
 *   <input v-else :type="type" v-model="data[type]">
 */

function preTransformNode (el, options) {
  if (el.tag === 'input') {
    var map = el.attrsMap;
    if (map['v-model'] && (map['v-bind:type'] || map[':type'])) {
      var typeBinding = getBindingAttr(el, 'type');
      var ifCondition = getAndRemoveAttr(el, 'v-if', true);
      var ifConditionExtra = ifCondition ? ("&&(" + ifCondition + ")") : "";
      var hasElse = getAndRemoveAttr(el, 'v-else', true) != null;
      var elseIfCondition = getAndRemoveAttr(el, 'v-else-if', true);
      // 1. checkbox
      var branch0 = cloneASTElement(el);
      // process for on the main node
      processFor(branch0);
      addRawAttr(branch0, 'type', 'checkbox');
      processElement(branch0, options);
      branch0.processed = true; // prevent it from double-processed
      branch0.if = "(" + typeBinding + ")==='checkbox'" + ifConditionExtra;
      addIfCondition(branch0, {
        exp: branch0.if,
        block: branch0
      });
      // 2. add radio else-if condition
      var branch1 = cloneASTElement(el);
      getAndRemoveAttr(branch1, 'v-for', true);
      addRawAttr(branch1, 'type', 'radio');
      processElement(branch1, options);
      addIfCondition(branch0, {
        exp: "(" + typeBinding + ")==='radio'" + ifConditionExtra,
        block: branch1
      });
      // 3. other
      var branch2 = cloneASTElement(el);
      getAndRemoveAttr(branch2, 'v-for', true);
      addRawAttr(branch2, ':type', typeBinding);
      processElement(branch2, options);
      addIfCondition(branch0, {
        exp: ifCondition,
        block: branch2
      });

      if (hasElse) {
        branch0.else = true;
      } else if (elseIfCondition) {
        branch0.elseif = elseIfCondition;
      }

      return branch0
    }
  }
}

function cloneASTElement (el) {
  return createASTElement(el.tag, el.attrsList.slice(), el.parent)
}

var model$2 = {
  preTransformNode: preTransformNode
};

var modules$1 = [
  klass$1,
  style$1,
  model$2
];

/*  */

function text (el, dir) {
  if (dir.value) {
    addProp(el, 'textContent', ("_s(" + (dir.value) + ")"));
  }
}

/*  */

function html (el, dir) {
  if (dir.value) {
    addProp(el, 'innerHTML', ("_s(" + (dir.value) + ")"));
  }
}

var directives$1 = {
  model: model,
  text: text,
  html: html
};

/*  */

var baseOptions = {
  expectHTML: true,
  modules: modules$1,
  directives: directives$1,
  isPreTag: isPreTag,
  isUnaryTag: isUnaryTag,
  mustUseProp: mustUseProp,
  canBeLeftOpenTag: canBeLeftOpenTag,
  isReservedTag: isReservedTag,
  getTagNamespace: getTagNamespace,
  staticKeys: genStaticKeys(modules$1)
};

/*  */

var isStaticKey;
var isPlatformReservedTag;

var genStaticKeysCached = cached(genStaticKeys$1);

/**
 * Goal of the optimizer: walk the generated template AST tree
 * and detect sub-trees that are purely static, i.e. parts of
 * the DOM that never needs to change.
 *
 * Once we detect these sub-trees, we can:
 *
 * 1. Hoist them into constants, so that we no longer need to
 *    create fresh nodes for them on each re-render;
 * 2. Completely skip them in the patching process.
 */
function optimize (root, options) {
  if (!root) { return }
  isStaticKey = genStaticKeysCached(options.staticKeys || '');
  isPlatformReservedTag = options.isReservedTag || no;
  // first pass: mark all non-static nodes.
  markStatic$1(root);
  // second pass: mark static roots.
  markStaticRoots(root, false);
}

function genStaticKeys$1 (keys) {
  return makeMap(
    'type,tag,attrsList,attrsMap,plain,parent,children,attrs' +
    (keys ? ',' + keys : '')
  )
}

function markStatic$1 (node) {
  node.static = isStatic(node);
  if (node.type === 1) {
    // do not make component slot content static. this avoids
    // 1. components not able to mutate slot nodes
    // 2. static slot content fails for hot-reloading
    if (
      !isPlatformReservedTag(node.tag) &&
      node.tag !== 'slot' &&
      node.attrsMap['inline-template'] == null
    ) {
      return
    }
    for (var i = 0, l = node.children.length; i < l; i++) {
      var child = node.children[i];
      markStatic$1(child);
      if (!child.static) {
        node.static = false;
      }
    }
    if (node.ifConditions) {
      for (var i$1 = 1, l$1 = node.ifConditions.length; i$1 < l$1; i$1++) {
        var block = node.ifConditions[i$1].block;
        markStatic$1(block);
        if (!block.static) {
          node.static = false;
        }
      }
    }
  }
}

function markStaticRoots (node, isInFor) {
  if (node.type === 1) {
    if (node.static || node.once) {
      node.staticInFor = isInFor;
    }
    // For a node to qualify as a static root, it should have children that
    // are not just static text. Otherwise the cost of hoisting out will
    // outweigh the benefits and it's better off to just always render it fresh.
    if (node.static && node.children.length && !(
      node.children.length === 1 &&
      node.children[0].type === 3
    )) {
      node.staticRoot = true;
      return
    } else {
      node.staticRoot = false;
    }
    if (node.children) {
      for (var i = 0, l = node.children.length; i < l; i++) {
        markStaticRoots(node.children[i], isInFor || !!node.for);
      }
    }
    if (node.ifConditions) {
      for (var i$1 = 1, l$1 = node.ifConditions.length; i$1 < l$1; i$1++) {
        markStaticRoots(node.ifConditions[i$1].block, isInFor);
      }
    }
  }
}

function isStatic (node) {
  if (node.type === 2) { // expression
    return false
  }
  if (node.type === 3) { // text
    return true
  }
  return !!(node.pre || (
    !node.hasBindings && // no dynamic bindings
    !node.if && !node.for && // not v-if or v-for or v-else
    !isBuiltInTag(node.tag) && // not a built-in
    isPlatformReservedTag(node.tag) && // not a component
    !isDirectChildOfTemplateFor(node) &&
    Object.keys(node).every(isStaticKey)
  ))
}

function isDirectChildOfTemplateFor (node) {
  while (node.parent) {
    node = node.parent;
    if (node.tag !== 'template') {
      return false
    }
    if (node.for) {
      return true
    }
  }
  return false
}

/*  */

var fnExpRE = /^\s*([\w$_]+|\([^)]*?\))\s*=>|^function\s*\(/;
var simplePathRE = /^\s*[A-Za-z_$][\w$]*(?:\.[A-Za-z_$][\w$]*|\['.*?']|\[".*?"]|\[\d+]|\[[A-Za-z_$][\w$]*])*\s*$/;

// keyCode aliases
var keyCodes = {
  esc: 27,
  tab: 9,
  enter: 13,
  space: 32,
  up: 38,
  left: 37,
  right: 39,
  down: 40,
  'delete': [8, 46]
};

// #4868: modifiers that prevent the execution of the listener
// need to explicitly return null so that we can determine whether to remove
// the listener for .once
var genGuard = function (condition) { return ("if(" + condition + ")return null;"); };

var modifierCode = {
  stop: '$event.stopPropagation();',
  prevent: '$event.preventDefault();',
  self: genGuard("$event.target !== $event.currentTarget"),
  ctrl: genGuard("!$event.ctrlKey"),
  shift: genGuard("!$event.shiftKey"),
  alt: genGuard("!$event.altKey"),
  meta: genGuard("!$event.metaKey"),
  left: genGuard("'button' in $event && $event.button !== 0"),
  middle: genGuard("'button' in $event && $event.button !== 1"),
  right: genGuard("'button' in $event && $event.button !== 2")
};

function genHandlers (
  events,
  isNative,
  warn
) {
  var res = isNative ? 'nativeOn:{' : 'on:{';
  for (var name in events) {
    res += "\"" + name + "\":" + (genHandler(name, events[name])) + ",";
  }
  return res.slice(0, -1) + '}'
}

function genHandler (
  name,
  handler
) {
  if (!handler) {
    return 'function(){}'
  }

  if (Array.isArray(handler)) {
    return ("[" + (handler.map(function (handler) { return genHandler(name, handler); }).join(',')) + "]")
  }

  var isMethodPath = simplePathRE.test(handler.value);
  var isFunctionExpression = fnExpRE.test(handler.value);

  if (!handler.modifiers) {
    if (isMethodPath || isFunctionExpression) {
      return handler.value
    }
    /* istanbul ignore if */
    return ("function($event){" + (handler.value) + "}") // inline statement
  } else {
    var code = '';
    var genModifierCode = '';
    var keys = [];
    for (var key in handler.modifiers) {
      if (modifierCode[key]) {
        genModifierCode += modifierCode[key];
        // left/right
        if (keyCodes[key]) {
          keys.push(key);
        }
      } else if (key === 'exact') {
        var modifiers = (handler.modifiers);
        genModifierCode += genGuard(
          ['ctrl', 'shift', 'alt', 'meta']
            .filter(function (keyModifier) { return !modifiers[keyModifier]; })
            .map(function (keyModifier) { return ("$event." + keyModifier + "Key"); })
            .join('||')
        );
      } else {
        keys.push(key);
      }
    }
    if (keys.length) {
      code += genKeyFilter(keys);
    }
    // Make sure modifiers like prevent and stop get executed after key filtering
    if (genModifierCode) {
      code += genModifierCode;
    }
    var handlerCode = isMethodPath
      ? handler.value + '($event)'
      : isFunctionExpression
        ? ("(" + (handler.value) + ")($event)")
        : handler.value;
    /* istanbul ignore if */
    return ("function($event){" + code + handlerCode + "}")
  }
}

function genKeyFilter (keys) {
  return ("if(!('button' in $event)&&" + (keys.map(genFilterCode).join('&&')) + ")return null;")
}

function genFilterCode (key) {
  var keyVal = parseInt(key, 10);
  if (keyVal) {
    return ("$event.keyCode!==" + keyVal)
  }
  var code = keyCodes[key];
  return (
    "_k($event.keyCode," +
    (JSON.stringify(key)) + "," +
    (JSON.stringify(code)) + "," +
    "$event.key)"
  )
}

/*  */

function on (el, dir) {
  if ("development" !== 'production' && dir.modifiers) {
    warn("v-on without argument does not support modifiers.");
  }
  el.wrapListeners = function (code) { return ("_g(" + code + "," + (dir.value) + ")"); };
}

/*  */

function bind$1 (el, dir) {
  el.wrapData = function (code) {
    return ("_b(" + code + ",'" + (el.tag) + "'," + (dir.value) + "," + (dir.modifiers && dir.modifiers.prop ? 'true' : 'false') + (dir.modifiers && dir.modifiers.sync ? ',true' : '') + ")")
  };
}

/*  */

var baseDirectives = {
  on: on,
  bind: bind$1,
  cloak: noop
};

/*  */

var CodegenState = function CodegenState (options) {
  this.options = options;
  this.warn = options.warn || baseWarn;
  this.transforms = pluckModuleFunction(options.modules, 'transformCode');
  this.dataGenFns = pluckModuleFunction(options.modules, 'genData');
  this.directives = extend(extend({}, baseDirectives), options.directives);
  var isReservedTag = options.isReservedTag || no;
  this.maybeComponent = function (el) { return !isReservedTag(el.tag); };
  this.onceId = 0;
  this.staticRenderFns = [];
};



function generate (
  ast,
  options
) {
  var state = new CodegenState(options);
  var code = ast ? genElement(ast, state) : '_c("div")';
  return {
    render: ("with(this){return " + code + "}"),
    staticRenderFns: state.staticRenderFns
  }
}

function genElement (el, state) {
  if (el.staticRoot && !el.staticProcessed) {
    return genStatic(el, state)
  } else if (el.once && !el.onceProcessed) {
    return genOnce(el, state)
  } else if (el.for && !el.forProcessed) {
    return genFor(el, state)
  } else if (el.if && !el.ifProcessed) {
    return genIf(el, state)
  } else if (el.tag === 'template' && !el.slotTarget) {
    return genChildren(el, state) || 'void 0'
  } else if (el.tag === 'slot') {
    return genSlot(el, state)
  } else {
    // component or element
    var code;
    if (el.component) {
      code = genComponent(el.component, el, state);
    } else {
      var data = el.plain ? undefined : genData$2(el, state);

      var children = el.inlineTemplate ? null : genChildren(el, state, true);
      code = "_c('" + (el.tag) + "'" + (data ? ("," + data) : '') + (children ? ("," + children) : '') + ")";
    }
    // module transforms
    for (var i = 0; i < state.transforms.length; i++) {
      code = state.transforms[i](el, code);
    }
    return code
  }
}

// hoist static sub-trees out
function genStatic (el, state) {
  el.staticProcessed = true;
  state.staticRenderFns.push(("with(this){return " + (genElement(el, state)) + "}"));
  return ("_m(" + (state.staticRenderFns.length - 1) + (el.staticInFor ? ',true' : '') + ")")
}

// v-once
function genOnce (el, state) {
  el.onceProcessed = true;
  if (el.if && !el.ifProcessed) {
    return genIf(el, state)
  } else if (el.staticInFor) {
    var key = '';
    var parent = el.parent;
    while (parent) {
      if (parent.for) {
        key = parent.key;
        break
      }
      parent = parent.parent;
    }
    if (!key) {
      "development" !== 'production' && state.warn(
        "v-once can only be used inside v-for that is keyed. "
      );
      return genElement(el, state)
    }
    return ("_o(" + (genElement(el, state)) + "," + (state.onceId++) + "," + key + ")")
  } else {
    return genStatic(el, state)
  }
}

function genIf (
  el,
  state,
  altGen,
  altEmpty
) {
  el.ifProcessed = true; // avoid recursion
  return genIfConditions(el.ifConditions.slice(), state, altGen, altEmpty)
}

function genIfConditions (
  conditions,
  state,
  altGen,
  altEmpty
) {
  if (!conditions.length) {
    return altEmpty || '_e()'
  }

  var condition = conditions.shift();
  if (condition.exp) {
    return ("(" + (condition.exp) + ")?" + (genTernaryExp(condition.block)) + ":" + (genIfConditions(conditions, state, altGen, altEmpty)))
  } else {
    return ("" + (genTernaryExp(condition.block)))
  }

  // v-if with v-once should generate code like (a)?_m(0):_m(1)
  function genTernaryExp (el) {
    return altGen
      ? altGen(el, state)
      : el.once
        ? genOnce(el, state)
        : genElement(el, state)
  }
}

function genFor (
  el,
  state,
  altGen,
  altHelper
) {
  var exp = el.for;
  var alias = el.alias;
  var iterator1 = el.iterator1 ? ("," + (el.iterator1)) : '';
  var iterator2 = el.iterator2 ? ("," + (el.iterator2)) : '';

  if ("development" !== 'production' &&
    state.maybeComponent(el) &&
    el.tag !== 'slot' &&
    el.tag !== 'template' &&
    !el.key
  ) {
    state.warn(
      "<" + (el.tag) + " v-for=\"" + alias + " in " + exp + "\">: component lists rendered with " +
      "v-for should have explicit keys. " +
      "See https://vuejs.org/guide/list.html#key for more info.",
      true /* tip */
    );
  }

  el.forProcessed = true; // avoid recursion
  return (altHelper || '_l') + "((" + exp + ")," +
    "function(" + alias + iterator1 + iterator2 + "){" +
      "return " + ((altGen || genElement)(el, state)) +
    '})'
}

function genData$2 (el, state) {
  var data = '{';

  // directives first.
  // directives may mutate the el's other properties before they are generated.
  var dirs = genDirectives(el, state);
  if (dirs) { data += dirs + ','; }

  // key
  if (el.key) {
    data += "key:" + (el.key) + ",";
  }
  // ref
  if (el.ref) {
    data += "ref:" + (el.ref) + ",";
  }
  if (el.refInFor) {
    data += "refInFor:true,";
  }
  // pre
  if (el.pre) {
    data += "pre:true,";
  }
  // record original tag name for components using "is" attribute
  if (el.component) {
    data += "tag:\"" + (el.tag) + "\",";
  }
  // module data generation functions
  for (var i = 0; i < state.dataGenFns.length; i++) {
    data += state.dataGenFns[i](el);
  }
  // attributes
  if (el.attrs) {
    data += "attrs:{" + (genProps(el.attrs)) + "},";
  }
  // DOM props
  if (el.props) {
    data += "domProps:{" + (genProps(el.props)) + "},";
  }
  // event handlers
  if (el.events) {
    data += (genHandlers(el.events, false, state.warn)) + ",";
  }
  if (el.nativeEvents) {
    data += (genHandlers(el.nativeEvents, true, state.warn)) + ",";
  }
  // slot target
  // only for non-scoped slots
  if (el.slotTarget && !el.slotScope) {
    data += "slot:" + (el.slotTarget) + ",";
  }
  // scoped slots
  if (el.scopedSlots) {
    data += (genScopedSlots(el.scopedSlots, state)) + ",";
  }
  // component v-model
  if (el.model) {
    data += "model:{value:" + (el.model.value) + ",callback:" + (el.model.callback) + ",expression:" + (el.model.expression) + "},";
  }
  // inline-template
  if (el.inlineTemplate) {
    var inlineTemplate = genInlineTemplate(el, state);
    if (inlineTemplate) {
      data += inlineTemplate + ",";
    }
  }
  data = data.replace(/,$/, '') + '}';
  // v-bind data wrap
  if (el.wrapData) {
    data = el.wrapData(data);
  }
  // v-on data wrap
  if (el.wrapListeners) {
    data = el.wrapListeners(data);
  }
  return data
}

function genDirectives (el, state) {
  var dirs = el.directives;
  if (!dirs) { return }
  var res = 'directives:[';
  var hasRuntime = false;
  var i, l, dir, needRuntime;
  for (i = 0, l = dirs.length; i < l; i++) {
    dir = dirs[i];
    needRuntime = true;
    var gen = state.directives[dir.name];
    if (gen) {
      // compile-time directive that manipulates AST.
      // returns true if it also needs a runtime counterpart.
      needRuntime = !!gen(el, dir, state.warn);
    }
    if (needRuntime) {
      hasRuntime = true;
      res += "{name:\"" + (dir.name) + "\",rawName:\"" + (dir.rawName) + "\"" + (dir.value ? (",value:(" + (dir.value) + "),expression:" + (JSON.stringify(dir.value))) : '') + (dir.arg ? (",arg:\"" + (dir.arg) + "\"") : '') + (dir.modifiers ? (",modifiers:" + (JSON.stringify(dir.modifiers))) : '') + "},";
    }
  }
  if (hasRuntime) {
    return res.slice(0, -1) + ']'
  }
}

function genInlineTemplate (el, state) {
  var ast = el.children[0];
  if ("development" !== 'production' && (
    el.children.length !== 1 || ast.type !== 1
  )) {
    state.warn('Inline-template components must have exactly one child element.');
  }
  if (ast.type === 1) {
    var inlineRenderFns = generate(ast, state.options);
    return ("inlineTemplate:{render:function(){" + (inlineRenderFns.render) + "},staticRenderFns:[" + (inlineRenderFns.staticRenderFns.map(function (code) { return ("function(){" + code + "}"); }).join(',')) + "]}")
  }
}

function genScopedSlots (
  slots,
  state
) {
  return ("scopedSlots:_u([" + (Object.keys(slots).map(function (key) {
      return genScopedSlot(key, slots[key], state)
    }).join(',')) + "])")
}

function genScopedSlot (
  key,
  el,
  state
) {
  if (el.for && !el.forProcessed) {
    return genForScopedSlot(key, el, state)
  }
  var fn = "function(" + (String(el.slotScope)) + "){" +
    "return " + (el.tag === 'template'
      ? el.if
        ? ((el.if) + "?" + (genChildren(el, state) || 'undefined') + ":undefined")
        : genChildren(el, state) || 'undefined'
      : genElement(el, state)) + "}";
  return ("{key:" + key + ",fn:" + fn + "}")
}

function genForScopedSlot (
  key,
  el,
  state
) {
  var exp = el.for;
  var alias = el.alias;
  var iterator1 = el.iterator1 ? ("," + (el.iterator1)) : '';
  var iterator2 = el.iterator2 ? ("," + (el.iterator2)) : '';
  el.forProcessed = true; // avoid recursion
  return "_l((" + exp + ")," +
    "function(" + alias + iterator1 + iterator2 + "){" +
      "return " + (genScopedSlot(key, el, state)) +
    '})'
}

function genChildren (
  el,
  state,
  checkSkip,
  altGenElement,
  altGenNode
) {
  var children = el.children;
  if (children.length) {
    var el$1 = children[0];
    // optimize single v-for
    if (children.length === 1 &&
      el$1.for &&
      el$1.tag !== 'template' &&
      el$1.tag !== 'slot'
    ) {
      return (altGenElement || genElement)(el$1, state)
    }
    var normalizationType = checkSkip
      ? getNormalizationType(children, state.maybeComponent)
      : 0;
    var gen = altGenNode || genNode;
    return ("[" + (children.map(function (c) { return gen(c, state); }).join(',')) + "]" + (normalizationType ? ("," + normalizationType) : ''))
  }
}

// determine the normalization needed for the children array.
// 0: no normalization needed
// 1: simple normalization needed (possible 1-level deep nested array)
// 2: full normalization needed
function getNormalizationType (
  children,
  maybeComponent
) {
  var res = 0;
  for (var i = 0; i < children.length; i++) {
    var el = children[i];
    if (el.type !== 1) {
      continue
    }
    if (needsNormalization(el) ||
        (el.ifConditions && el.ifConditions.some(function (c) { return needsNormalization(c.block); }))) {
      res = 2;
      break
    }
    if (maybeComponent(el) ||
        (el.ifConditions && el.ifConditions.some(function (c) { return maybeComponent(c.block); }))) {
      res = 1;
    }
  }
  return res
}

function needsNormalization (el) {
  return el.for !== undefined || el.tag === 'template' || el.tag === 'slot'
}

function genNode (node, state) {
  if (node.type === 1) {
    return genElement(node, state)
  } if (node.type === 3 && node.isComment) {
    return genComment(node)
  } else {
    return genText(node)
  }
}

function genText (text) {
  return ("_v(" + (text.type === 2
    ? text.expression // no need for () because already wrapped in _s()
    : transformSpecialNewlines(JSON.stringify(text.text))) + ")")
}

function genComment (comment) {
  return ("_e(" + (JSON.stringify(comment.text)) + ")")
}

function genSlot (el, state) {
  var slotName = el.slotName || '"default"';
  var children = genChildren(el, state);
  var res = "_t(" + slotName + (children ? ("," + children) : '');
  var attrs = el.attrs && ("{" + (el.attrs.map(function (a) { return ((camelize(a.name)) + ":" + (a.value)); }).join(',')) + "}");
  var bind$$1 = el.attrsMap['v-bind'];
  if ((attrs || bind$$1) && !children) {
    res += ",null";
  }
  if (attrs) {
    res += "," + attrs;
  }
  if (bind$$1) {
    res += (attrs ? '' : ',null') + "," + bind$$1;
  }
  return res + ')'
}

// componentName is el.component, take it as argument to shun flow's pessimistic refinement
function genComponent (
  componentName,
  el,
  state
) {
  var children = el.inlineTemplate ? null : genChildren(el, state, true);
  return ("_c(" + componentName + "," + (genData$2(el, state)) + (children ? ("," + children) : '') + ")")
}

function genProps (props) {
  var res = '';
  for (var i = 0; i < props.length; i++) {
    var prop = props[i];
    /* istanbul ignore if */
    {
      res += "\"" + (prop.name) + "\":" + (transformSpecialNewlines(prop.value)) + ",";
    }
  }
  return res.slice(0, -1)
}

// #3895, #4268
function transformSpecialNewlines (text) {
  return text
    .replace(/\u2028/g, '\\u2028')
    .replace(/\u2029/g, '\\u2029')
}

/*  */

// these keywords should not appear inside expressions, but operators like
// typeof, instanceof and in are allowed
var prohibitedKeywordRE = new RegExp('\\b' + (
  'do,if,for,let,new,try,var,case,else,with,await,break,catch,class,const,' +
  'super,throw,while,yield,delete,export,import,return,switch,default,' +
  'extends,finally,continue,debugger,function,arguments'
).split(',').join('\\b|\\b') + '\\b');

// these unary operators should not be used as property/method names
var unaryOperatorsRE = new RegExp('\\b' + (
  'delete,typeof,void'
).split(',').join('\\s*\\([^\\)]*\\)|\\b') + '\\s*\\([^\\)]*\\)');

// strip strings in expressions
var stripStringRE = /'(?:[^'\\]|\\.)*'|"(?:[^"\\]|\\.)*"|`(?:[^`\\]|\\.)*\$\{|\}(?:[^`\\]|\\.)*`|`(?:[^`\\]|\\.)*`/g;

// detect problematic expressions in a template
function detectErrors (ast) {
  var errors = [];
  if (ast) {
    checkNode(ast, errors);
  }
  return errors
}

function checkNode (node, errors) {
  if (node.type === 1) {
    for (var name in node.attrsMap) {
      if (dirRE.test(name)) {
        var value = node.attrsMap[name];
        if (value) {
          if (name === 'v-for') {
            checkFor(node, ("v-for=\"" + value + "\""), errors);
          } else if (onRE.test(name)) {
            checkEvent(value, (name + "=\"" + value + "\""), errors);
          } else {
            checkExpression(value, (name + "=\"" + value + "\""), errors);
          }
        }
      }
    }
    if (node.children) {
      for (var i = 0; i < node.children.length; i++) {
        checkNode(node.children[i], errors);
      }
    }
  } else if (node.type === 2) {
    checkExpression(node.expression, node.text, errors);
  }
}

function checkEvent (exp, text, errors) {
  var stipped = exp.replace(stripStringRE, '');
  var keywordMatch = stipped.match(unaryOperatorsRE);
  if (keywordMatch && stipped.charAt(keywordMatch.index - 1) !== '$') {
    errors.push(
      "avoid using JavaScript unary operator as property name: " +
      "\"" + (keywordMatch[0]) + "\" in expression " + (text.trim())
    );
  }
  checkExpression(exp, text, errors);
}

function checkFor (node, text, errors) {
  checkExpression(node.for || '', text, errors);
  checkIdentifier(node.alias, 'v-for alias', text, errors);
  checkIdentifier(node.iterator1, 'v-for iterator', text, errors);
  checkIdentifier(node.iterator2, 'v-for iterator', text, errors);
}

function checkIdentifier (
  ident,
  type,
  text,
  errors
) {
  if (typeof ident === 'string') {
    try {
      new Function(("var " + ident + "=_"));
    } catch (e) {
      errors.push(("invalid " + type + " \"" + ident + "\" in expression: " + (text.trim())));
    }
  }
}

function checkExpression (exp, text, errors) {
  try {
    new Function(("return " + exp));
  } catch (e) {
    var keywordMatch = exp.replace(stripStringRE, '').match(prohibitedKeywordRE);
    if (keywordMatch) {
      errors.push(
        "avoid using JavaScript keyword as property name: " +
        "\"" + (keywordMatch[0]) + "\"\n  Raw expression: " + (text.trim())
      );
    } else {
      errors.push(
        "invalid expression: " + (e.message) + " in\n\n" +
        "    " + exp + "\n\n" +
        "  Raw expression: " + (text.trim()) + "\n"
      );
    }
  }
}

/*  */

function createFunction (code, errors) {
  try {
    return new Function(code)
  } catch (err) {
    errors.push({ err: err, code: code });
    return noop
  }
}

function createCompileToFunctionFn (compile) {
  var cache = Object.create(null);

  return function compileToFunctions (
    template,
    options,
    vm
  ) {
    options = extend({}, options);
    var warn$$1 = options.warn || warn;
    delete options.warn;

    /* istanbul ignore if */
    if (true) {
      // detect possible CSP restriction
      try {
        new Function('return 1');
      } catch (e) {
        if (e.toString().match(/unsafe-eval|CSP/)) {
          warn$$1(
            'It seems you are using the standalone build of Vue.js in an ' +
            'environment with Content Security Policy that prohibits unsafe-eval. ' +
            'The template compiler cannot work in this environment. Consider ' +
            'relaxing the policy to allow unsafe-eval or pre-compiling your ' +
            'templates into render functions.'
          );
        }
      }
    }

    // check cache
    var key = options.delimiters
      ? String(options.delimiters) + template
      : template;
    if (cache[key]) {
      return cache[key]
    }

    // compile
    var compiled = compile(template, options);

    // check compilation errors/tips
    if (true) {
      if (compiled.errors && compiled.errors.length) {
        warn$$1(
          "Error compiling template:\n\n" + template + "\n\n" +
          compiled.errors.map(function (e) { return ("- " + e); }).join('\n') + '\n',
          vm
        );
      }
      if (compiled.tips && compiled.tips.length) {
        compiled.tips.forEach(function (msg) { return tip(msg, vm); });
      }
    }

    // turn code into functions
    var res = {};
    var fnGenErrors = [];
    res.render = createFunction(compiled.render, fnGenErrors);
    res.staticRenderFns = compiled.staticRenderFns.map(function (code) {
      return createFunction(code, fnGenErrors)
    });

    // check function generation errors.
    // this should only happen if there is a bug in the compiler itself.
    // mostly for codegen development use
    /* istanbul ignore if */
    if (true) {
      if ((!compiled.errors || !compiled.errors.length) && fnGenErrors.length) {
        warn$$1(
          "Failed to generate render function:\n\n" +
          fnGenErrors.map(function (ref) {
            var err = ref.err;
            var code = ref.code;

            return ((err.toString()) + " in\n\n" + code + "\n");
        }).join('\n'),
          vm
        );
      }
    }

    return (cache[key] = res)
  }
}

/*  */

function createCompilerCreator (baseCompile) {
  return function createCompiler (baseOptions) {
    function compile (
      template,
      options
    ) {
      var finalOptions = Object.create(baseOptions);
      var errors = [];
      var tips = [];
      finalOptions.warn = function (msg, tip) {
        (tip ? tips : errors).push(msg);
      };

      if (options) {
        // merge custom modules
        if (options.modules) {
          finalOptions.modules =
            (baseOptions.modules || []).concat(options.modules);
        }
        // merge custom directives
        if (options.directives) {
          finalOptions.directives = extend(
            Object.create(baseOptions.directives || null),
            options.directives
          );
        }
        // copy other options
        for (var key in options) {
          if (key !== 'modules' && key !== 'directives') {
            finalOptions[key] = options[key];
          }
        }
      }

      var compiled = baseCompile(template, finalOptions);
      if (true) {
        errors.push.apply(errors, detectErrors(compiled.ast));
      }
      compiled.errors = errors;
      compiled.tips = tips;
      return compiled
    }

    return {
      compile: compile,
      compileToFunctions: createCompileToFunctionFn(compile)
    }
  }
}

/*  */

// `createCompilerCreator` allows creating compilers that use alternative
// parser/optimizer/codegen, e.g the SSR optimizing compiler.
// Here we just export a default compiler using the default parts.
var createCompiler = createCompilerCreator(function baseCompile (
  template,
  options
) {
  var ast = parse(template.trim(), options);
  if (options.optimize !== false) {
    optimize(ast, options);
  }
  var code = generate(ast, options);
  return {
    ast: ast,
    render: code.render,
    staticRenderFns: code.staticRenderFns
  }
});

/*  */

var ref$1 = createCompiler(baseOptions);
var compileToFunctions = ref$1.compileToFunctions;

/*  */

// check whether current browser encodes a char inside attribute values
var div;
function getShouldDecode (href) {
  div = div || document.createElement('div');
  div.innerHTML = href ? "<a href=\"\n\"/>" : "<div a=\"\n\"/>";
  return div.innerHTML.indexOf('&#10;') > 0
}

// #3663: IE encodes newlines inside attribute values while other browsers don't
var shouldDecodeNewlines = inBrowser ? getShouldDecode(false) : false;
// #6828: chrome encodes content in a[href]
var shouldDecodeNewlinesForHref = inBrowser ? getShouldDecode(true) : false;

/*  */

var idToTemplate = cached(function (id) {
  var el = query(id);
  return el && el.innerHTML
});

var mount = Vue$3.prototype.$mount;
Vue$3.prototype.$mount = function (
  el,
  hydrating
) {
  el = el && query(el);

  /* istanbul ignore if */
  if (el === document.body || el === document.documentElement) {
    "development" !== 'production' && warn(
      "Do not mount Vue to <html> or <body> - mount to normal elements instead."
    );
    return this
  }

  var options = this.$options;
  // resolve template/el and convert to render function
  if (!options.render) {
    var template = options.template;
    if (template) {
      if (typeof template === 'string') {
        if (template.charAt(0) === '#') {
          template = idToTemplate(template);
          /* istanbul ignore if */
          if ("development" !== 'production' && !template) {
            warn(
              ("Template element not found or is empty: " + (options.template)),
              this
            );
          }
        }
      } else if (template.nodeType) {
        template = template.innerHTML;
      } else {
        if (true) {
          warn('invalid template option:' + template, this);
        }
        return this
      }
    } else if (el) {
      template = getOuterHTML(el);
    }
    if (template) {
      /* istanbul ignore if */
      if ("development" !== 'production' && config.performance && mark) {
        mark('compile');
      }

      var ref = compileToFunctions(template, {
        shouldDecodeNewlines: shouldDecodeNewlines,
        shouldDecodeNewlinesForHref: shouldDecodeNewlinesForHref,
        delimiters: options.delimiters,
        comments: options.comments
      }, this);
      var render = ref.render;
      var staticRenderFns = ref.staticRenderFns;
      options.render = render;
      options.staticRenderFns = staticRenderFns;

      /* istanbul ignore if */
      if ("development" !== 'production' && config.performance && mark) {
        mark('compile end');
        measure(("vue " + (this._name) + " compile"), 'compile', 'compile end');
      }
    }
  }
  return mount.call(this, el, hydrating)
};

/**
 * Get outerHTML of elements, taking care
 * of SVG elements in IE as well.
 */
function getOuterHTML (el) {
  if (el.outerHTML) {
    return el.outerHTML
  } else {
    var container = document.createElement('div');
    container.appendChild(el.cloneNode(true));
    return container.innerHTML
  }
}

Vue$3.compile = compileToFunctions;

module.exports = Vue$3;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/node-libs-browser/node_modules/timers-browserify/main.js").setImmediate))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./resources/assets/js/dashboard/components/piechart.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/piechart.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-63cb4824\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/piechart.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/dashboard/components/piechart.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-63cb4824", Component.options)
  } else {
    hotAPI.reload("data-v-63cb4824", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/js/dashboard/components/radialbar.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__("./node_modules/vue-style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91ccc3b0\",\"scoped\":false,\"hasInlineConfig\":true}!./node_modules/vue-loader/lib/selector.js?type=styles&index=0!./resources/assets/js/dashboard/components/radialbar.vue")
}
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/radialbar.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-91ccc3b0\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/radialbar.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/dashboard/components/radialbar.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-91ccc3b0", Component.options)
  } else {
    hotAPI.reload("data-v-91ccc3b0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/js/dashboard/components/stockchart.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/assets/js/dashboard/components/stockchart.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-606ea0a4\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/assets/js/dashboard/components/stockchart.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/dashboard/components/stockchart.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-606ea0a4", Component.options)
  } else {
    hotAPI.reload("data-v-606ea0a4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/assets/js/dashboard/main.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__("./node_modules/vue/dist/vue.common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_piechart__ = __webpack_require__("./resources/assets/js/dashboard/components/piechart.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_piechart___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_piechart__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_radialbar__ = __webpack_require__("./resources/assets/js/dashboard/components/radialbar.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_radialbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_radialbar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_stockchart__ = __webpack_require__("./resources/assets/js/dashboard/components/stockchart.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_stockchart___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_stockchart__);





new __WEBPACK_IMPORTED_MODULE_0_vue___default.a({
    el: '#chart',
    components: {
        piechart: __WEBPACK_IMPORTED_MODULE_1__components_piechart___default.a,
        radialbar: __WEBPACK_IMPORTED_MODULE_2__components_radialbar___default.a,
        stockchart: __WEBPACK_IMPORTED_MODULE_3__components_stockchart___default.a
    }
});

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/dashboard/main.js");


/***/ })

/******/ });