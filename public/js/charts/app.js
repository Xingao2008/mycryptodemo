$(document).ready(function() {

    window.onload = function () {
        var dataPointsbtc = [];
        $.getJSON("/api/prices/btc", function(data) {
            $.each(data, function(key, value){
                dataPointsbtc.push({x: value[0], y: value[1]});
            });
            var chartbtc = new CanvasJS.Chart("chartContainer-btc",{
                backgroundColor: "#f9fbfc",
                zoomEnabled: true,
                responsive: true,
                creditText: '',
                toolTip: {
                    enabled: true,
                    animationEnabled: true,
                    backgroundColor: "#4084D3",
                    fontColor: "white",
                    fontFamily: "greycliff-demibold",
                    cornerRadius: 3,
                    borderThickness: 5,
                    borderColor: "#4084D3"
                },
                axisX:{
                    gridThickness: 0,
                    labelFontColor: "#ACB6C8",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8",
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    labelFontColor: "#ACB6C8",
                    labelFontFamily: "greycliff-demibold",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                data: [{
                    type: "spline",
                    xValueType: "dateTime",
                    dataPoints: dataPointsbtc,
                    lineColor: "#ACB6C8",
                }]
            });
            chartbtc.render();
        });


        var dataPointsltc = [];
        $.getJSON("/api/prices/ltc", function(data) {
            $.each(data, function(key, value){
                dataPointsltc.push({x: value[0], y: value[1]});
            });
            var chartltc = new CanvasJS.Chart("chartContainer-ltc",{
                backgroundColor: "#f9fbfc",
                zoomEnabled: true,
                responsive: true,
                toolTip: {
                    enabled: true,
                    animationEnabled: true,
                    backgroundColor: "#4084D3",
                    fontColor: "white",
                    fontFamily: "greycliff-demibold",
                    cornerRadius: 3,
                    borderThickness: 5,
                    borderColor: "#4084D3"
                },
                axisX:{
                    gridThickness: 0,
                    labelFontColor: "#ACB6C8",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    labelFontColor: "#ACB6C8",
                    labelFontFamily: "greycliff-demibold",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                data: [{
                    type: "spline",
                    xValueType: "dateTime",
                    dataPoints: dataPointsltc,
                    lineColor: "#ACB6C8"
                }]
            });
            chartltc.render();
        });


        var dataPointseth = [];
        $.getJSON("/api/prices/eth", function(data) {
            $.each(data, function(key, value){
                dataPointseth.push({x: value[0], y: value[1]});
            });
            var charteth = new CanvasJS.Chart("chartContainer-eth",{
                backgroundColor: "#f9fbfc",
                zoomEnabled: true,
                responsive: true,
                toolTip: {
                    enabled: true,
                    animationEnabled: true,
                    backgroundColor: "#4084D3",
                    fontColor: "white",
                    fontFamily: "greycliff-demibold",
                    cornerRadius: 3,
                    borderThickness: 5,
                    borderColor: "#4084D3"
                },
                axisX:{
                    gridThickness: 0,
                    labelFontColor: "#ACB6C8",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    labelFontColor: "#ACB6C8",
                    labelFontFamily: "greycliff-demibold",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                data: [{
                    type: "spline",
                    xValueType: "dateTime",
                    dataPoints: dataPointseth,
                    lineColor: "#ACB6C8"
                }]
            });
            charteth.render();
        });


        var dataPointsxrp = [];
        $.getJSON("/api/prices/xrp", function(data) {
            $.each(data, function(key, value){
                dataPointsxrp.push({x: value[0], y: value[1]});
            });
            var chartxrp = new CanvasJS.Chart("chartContainer-xrp",{
                backgroundColor: "#f9fbfc",
                zoomEnabled: true,
                responsive: true,
                toolTip: {
                    enabled: true,
                    animationEnabled: true,
                    backgroundColor: "#4084D3",
                    fontColor: "white",
                    fontFamily: "greycliff-demibold",
                    cornerRadius: 3,
                    borderThickness: 5,
                    borderColor: "#4084D3"
                },
                axisX:{
                    gridThickness: 0,
                    labelFontColor: "#ACB6C8",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    labelFontColor: "#ACB6C8",
                    labelFontFamily: "greycliff-demibold",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                data: [{
                    type: "spline",
                    xValueType: "dateTime",
                    dataPoints: dataPointsxrp,
                    lineColor: "#ACB6C8"
                }]
            });
            chartxrp.render();
        });

        var dataPointspwr = [];
        $.getJSON("/api/prices/pwr", function(data) {
            $.each(data, function(key, value){
                dataPointspwr.push({x: value[0], y: value[1]});
            });
            var chartpwr = new CanvasJS.Chart("chartContainer-pwr",{
                backgroundColor: "#f9fbfc",
                zoomEnabled: true,
                responsive: true,
                toolTip: {
                    enabled: true,
                    animationEnabled: true,
                    backgroundColor: "#4084D3",
                    fontColor: "white",
                    fontFamily: "greycliff-demibold",
                    cornerRadius: 3,
                    borderThickness: 5,
                    borderColor: "#4084D3"
                },
                axisX:{
                    gridThickness: 0,
                    labelFontColor: "#ACB6C8",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                axisY: {
                    gridThickness: 0,
                    includeZero: false,
                    labelFontColor: "#ACB6C8",
                    labelFontFamily: "greycliff-demibold",
                    lineColor: "#ACB6C8",
                    tickColor: "#ACB6C8"
                },
                data: [{
                    type: "spline",
                    xValueType: "dateTime",
                    dataPoints: dataPointspwr,
                    lineColor: "#ACB6C8"
                }]
            });
            chartpwr.render();
        });


    }
});
