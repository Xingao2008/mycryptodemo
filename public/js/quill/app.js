$(document).ready(function() {
    var quill = new Quill('#editor', {
        modules: {
            toolbar: [
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],['bold', 'italic'],
                ['link', 'blockquote', 'code-block'],
                [{ list: 'ordered' }, { list: 'bullet' }]
            ]
        },
        placeholder: 'Begin writing...',
        theme: 'snow'
    });
});


$(window).on('load', function(){
    $('body').on('click', function(){
        var data = $('.ql-editor').html();
        $('#content').html(data);
    });
});