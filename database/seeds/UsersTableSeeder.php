<?php

use Illuminate\Database\Seeder;
use PragmaRX\Google2FA\Google2FA;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @TODO make user of factories
     *
     * @return void
     */
    public function run()
    {
        $google2fa = new Google2FA();
        $secretKey = $google2fa->generateSecretKey();
        //$secretKey = str_pad($secretKey, pow(2,ceil(log(strlen($secretKey),2))), 'X');

        $user = new \App\Models\User();
        $user->fill([
            'name' => "Main Admin",
            'first_name' => 'Jaryd',
            'middle_name' => 'na',
            'last_name' => 'Koennigsman',
            'verify_count' => 0,
            'email' => "info@mycryptowallet.com.au",
            'password' => bcrypt('rgUjWxmXRahEzX4n'),
            'status' => 'active',
            'admin' => TRUE,
            'tfa' => $secretKey,
        ]);
        $user->save();


        DB::table('verifications')->insert([
            'user_id' => 1,
            'account' => 'approved',
            'id_passport' => 'approved',
            'id_drivers' => 'approved',
            'id_bank' => 'approved',
            'ubc' => 'approved',
            'sms' => 'approved',
            'bank' => 'approved',
            'tfa' => 'approved',
            'balance1' => 'approved',
            'balance2' => 'approved',
            'abn' => 'approved',
        ]);
        DB::table('balances')->insert(['user_id' => 1, 'currency' => 'btc', 'balance' => 0.000000000000000000, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 1, 'currency' => 'eth', 'balance' => 0.000000000000000000, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 1, 'currency' => 'ltc', 'balance' => 0.000000000000000000, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 1, 'currency' => 'xrp', 'balance' => 0.000000000000000000, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 1, 'currency' => 'aud', 'balance' => 0.000000000000000000, 'locked' => FALSE,]);

        foreach (['btc', 'eth', 'ltc', 'xrp', 'aud'] as $currency) {
            factory(\App\Models\Wallet::class)->create([
                'user_id' => 1,
                'type' => $currency
            ]);
        }


        $user = new \App\Models\User();
        $user->fill([
            'name' => "admin",
            'first_name' => 'Mathew',
            'middle_name' => 'Philip',
            'last_name' => 'Withoos',
            'verify_count' => 0,
            'email' => "mattwithoos@gmail.com",
            'password' => bcrypt('G4pubFtTmxaUA9zY'),
            'status' => 'active',
            'admin' => TRUE,
            'tfa' => $google2fa->generateSecretKey(),
        ]);
        $user->save();

        DB::table('verifications')->insert([
            'user_id' => 2,
            'account' => 'approved',
            'id_passport' => 'approved',
            'id_drivers' => 'approved',
            'id_bank' => 'approved',
            'ubc' => 'approved',
            'sms' => 'approved',
            'bank' => 'approved',
            'tfa' => 'approved',
            'balance1' => 'approved',
            'balance2' => 'approved',
            'abn' => 'approved',
        ]);
        DB::table('balances')->insert(['user_id' => 2, 'currency' => 'btc', 'balance' => 0, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 2, 'currency' => 'eth', 'balance' => 0, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 2, 'currency' => 'ltc', 'balance' => 0, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 2, 'currency' => 'xrp', 'balance' => 0, 'locked' => FALSE,]);
        DB::table('balances')->insert(['user_id' => 2, 'currency' => 'aud', 'balance' => 0, 'locked' => FALSE,]);

    }
}
