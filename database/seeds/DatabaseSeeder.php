<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

        \DB::table('users')->truncate();
        \DB::table('balances')->truncate();
        \DB::table('wallets')->truncate();
        \DB::table('verifications')->truncate();

        $this->call(UsersTableSeeder::class);
        $this->command->info('Seeded the users!');
        $this->call('CountriesSeeder');
        $this->command->info('Seeded the countries!');
        $this->call(FakerSeeder::class);
        $this->command->info('Seeded the orders');

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();
    }
}
