<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsdCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = \App\Models\User::get();
        foreach($users as $user) {
            $has_record = \App\Models\Balance::where('user_id','=',$user->id)->where('currency','=','usd')->first();

            if(!$has_record) {
                \App\Models\Balance::create([
                    'user_id'       => $user->id,
                    'currency'        => 'usd',
                    'balance'          => 0.000000000000000000,
                    'locked'     => FALSE,
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
