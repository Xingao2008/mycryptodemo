<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurchargeBalanaceTbale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surcharge_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency');
            $table->decimal('amount', 36, 18)->nullable()->default(0);
            $table->timestamps();
        });
        foreach(config('currencies.allAvailableCurrencies') as $c)
        {
            DB::table('surcharge_balances')->insert([
                'currency' => $c,
                'amount' => 0
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surcharge_balances');
    }
}
