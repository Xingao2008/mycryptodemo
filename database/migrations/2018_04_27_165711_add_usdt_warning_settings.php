<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsdtWarningSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('settings')->insert([
            'id' => 'usdt_warning_amount',
            'value' => '4000',
            'name' => 'USDT Warning Amount',
            'is_boolean' => 0,
            'type' => 0
        ]);

        DB::table('settings')->insert([
            'id' => 'usdt_warning_user',
            'value' => 'info@fairdigital.com.au',
            'name' => 'USDT Warning User',
            'is_boolean' => 0,
            'type' => 0
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
