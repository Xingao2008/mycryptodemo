<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBtcmarketsId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crypto_buys', function (Blueprint $table) {
            $table->integer('btcmarkets_id')->nullable();
        });
        Schema::table('crypto_sells', function (Blueprint $table) {
            $table->integer('btcmarkets_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crypto_buys', function($table) {
            $table->dropColumn('btcmarkets_id');
        });
        Schema::table('crypto_sells', function($table) {
            $table->dropColumn('btcmarkets_id');
        });
    }
}
