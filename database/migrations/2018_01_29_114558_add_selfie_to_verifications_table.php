<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSelfieToVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verifications', function (Blueprint $table) {
            $table->string('selfie')->default('incomplete')->after('abn');
            $table->string('selfie_notes')->nullable()->after('selfie');
        });

        DB::table('verifications')->update(['selfie' => 'legacy']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verifications', function (Blueprint $table) {
            $table->dropColumn('selfie');
            $table->dropColumn('selfie_notes');;
        });
    }
}
