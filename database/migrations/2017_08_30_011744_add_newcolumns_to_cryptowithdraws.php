<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToCryptoWithdraws extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crypto_withdraws', function($table) {
            $table->decimal('fees', 36, 18)->nullable();
            $table->integer('confirmations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crypto_withdraws', function($table) {
            $table->dropColumn('fees');
            $table->dropColumn('confirmations');
        });
    }
}
