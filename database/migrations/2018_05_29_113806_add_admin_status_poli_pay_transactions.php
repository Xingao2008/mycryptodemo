<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminStatusPoliPayTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('poli_pay_transactions', function($table) {
            $table->boolean('admin_status')->default(0);
        });

        \DB::statment('UPDATE poli_pay_transactions SET admin_status = 1 WHERE created_at < "2018-05-30 18:00:00"');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('poli_pay_transactions', function($table) {
            $table->dropColumn('admin_status');
        });
    }
}
