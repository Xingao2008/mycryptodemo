<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('value')->nullable();
            $table->timestamps();
        });

        DB::table('settings')->insert([
            'id' => 'hidden_fee',
            'value' => 4,
        ]);

        DB::table('settings')->insert([
            'id' => 'public_fee',
            'value' => 2.4,
        ]);

        DB::table('settings')->insert([
            'id' => 'maintenance_active',
            'value' => FALSE,
        ]);

        DB::table('settings')->insert([
            'id' => 'buys_active',
            'value' => TRUE,
        ]);

        DB::table('settings')->insert([
            'id' => 'sells_active',
            'value' => TRUE,
        ]);

        DB::table('settings')->insert([
            'id' => 'withdraws_active',
            'value' => TRUE,
        ]);

        DB::table('settings')->insert([
            'id' => 'deposits_active',
            'value' => TRUE,
        ]);



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
