<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudflareFirewallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('firewall');

        Schema::create('cloudflare_firewall', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address');
            $table->boolean('whitelist');
            $table->string('reason')->nullable();
            $table->string('cloudflare_id')->nullable();
            $table->dateTime('expires')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloudflare_firewall');
    }
}
