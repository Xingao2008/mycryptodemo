<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCryptoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crypto_buys', function (Blueprint $table) {
            $table->dropColumn('started');
            $table->string('external_id');
        });
        Schema::table('crypto_sells', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->string('external_id');
            $table->string('type');
            $table->decimal('amount', 36, 18);
            $table->decimal('price', 10, 2);
            $table->decimal('total', 10, 2);
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crypto_buys', function($table) {
            $table->timestamp('started')->nullable();
            $table->dropColumn('external_id');
        });
        Schema::table('crypto_sells', function($table) {
            $table->dropColumn('user_id')->nullable();
            $table->dropColumn('external_id');
            $table->dropColumn('type');
            $table->dropColumn('amount');
            $table->dropColumn('price');
            $table->dropColumn('total');
            $table->dropColumn('status');
        });
    }
}
