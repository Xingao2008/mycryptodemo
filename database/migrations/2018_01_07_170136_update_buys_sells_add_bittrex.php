<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBuysSellsAddBittrex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crypto_buys', function (Blueprint $table) {
            $table->string('bittrex_withdraw_id')->nullable();
            $table->string('bittrex_order_id')->nullable();
            $table->string('bittrex_txid')->nullable();
            $table->string('bittrex_rate')->nullable();
        });
        Schema::table('crypto_sells', function (Blueprint $table) {
            $table->string('bittrex_deposit_id')->nullable();
            $table->string('bittrex_order_id')->nullable();
            $table->string('bittrex_txid')->nullable();
            $table->string('bittrex_rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crypto_buys', function($table) {
            $table->dropColumn('bittrex_withdraw_id');
            $table->dropColumn('bittrex_order_id');
            $table->dropColumn('bittrex_txid');
            $table->dropColumn('bittrex_rate');
        });
        Schema::table('crypto_sells', function($table) {
            $table->dropColumn('bittrex_deposit_id');
            $table->dropColumn('bittrex_order_id');
            $table->dropColumn('bittrex_txid');
            $table->dropColumn('bittrex_rate');
        });
    }
}
