<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReasonToFirewallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('firewall', function(Blueprint $table){
            $table->string('reason')->nullable()->after('whitelisted');

            $table->dateTime('expires')->nullable()->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firewall', function(Blueprint $table){
            $table->dropColumn('reason');

            $table->dropColumn('expires');
        });
    }
}
