<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reference_id')->nullable();
            $table->unsignedInteger('user_to');
            $table->unsignedInteger('user_from')->nullable();
            $table->string('address_to')->nullable();
            $table->string('address_from')->nullable();
            $table->string('transaction_type');
            $table->string('currency');
            $table->decimal('amount', 36, 18);
            $table->string('transaction_id')->nullable();
            $table->decimal('fee', 36, 18)->nullable();
            $table->string('fee_type')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledgers');
    }
}
