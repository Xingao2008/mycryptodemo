<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFiatwithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fiat_withdraws', function (Blueprint $table) {
            $table->dropColumn('actioned');
            $table->unsignedInteger('user_id')->change();
            $table->string('notes')->nullable()->change();
        });
        Schema::table('text_verifications', function (Blueprint $table) {
            $table->string('reference_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fiat_withdraws', function($table) {
            $table->date('actioned')->nullable();
            $table->unsignedInteger('user_id')->nullable()->change();
            $table->string('notes')->change();
        });
        Schema::table('text_verifications', function (Blueprint $table) {
            $table->dropColumn('reference_id');
        });
    }
}
