<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAvailableCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('countries')
        ->where('id', 840)
        ->update(['available' => 1]);
        DB::table('countries')
        ->where('id', 826)
        ->update(['available' => 1]);
        DB::table('countries')
        ->where('id', 124)
        ->update(['available' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('countries')
        ->where('id', 840)
        ->update(['available' => 0]);
        DB::table('countries')
        ->where('id', 826)
        ->update(['available' => 0]);
        DB::table('countries')
        ->where('id', 124)
        ->update(['available' => 0]);
    }
}
