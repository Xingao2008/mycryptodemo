<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSurchargeCashoutPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('permissions')->insert([
            'name' => 'admin.exchange.fees.convert.index',
            'guard_name' => 'web',
            'description' => 'Admin access to cashout all the surcharge',
            'title' => 'Admin Surcharge cashout'
        ]); 

        DB::table('role_has_permissions')->insert([
            'permission_id' => 25,
            'role_id' => 1
        ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
