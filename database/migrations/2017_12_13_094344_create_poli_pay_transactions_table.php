<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliPayTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poli_pay_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique()->index();
            $table->integer('user_id');
            $table->string('user_ip')->nullable();
            $table->string('status')->nullable();
            $table->string('amount')->default(0);
            $table->integer('transaction_time')->default(0);
            $table->string('transaction_id')->nullable();
            $table->string('transaction_ref_no')->nullable();
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poli_pay_transactions');
    }
}
