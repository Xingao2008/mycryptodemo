<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency');
            $table->decimal('bid', 36, 18)->default(0.0000000000000000);
            $table->decimal('ask', 36, 18)->default(0.0000000000000000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_prices');
    }
}
