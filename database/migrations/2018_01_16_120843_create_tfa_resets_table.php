<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTfaResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tfa_resets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('active')->default(0);
            $table->integer('complete')->default(0);
            $table->string('auth_code')->nullable();
            $table->text('reason')->nullable();
            $table->integer('staff_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tfa_resets');
    }
}
