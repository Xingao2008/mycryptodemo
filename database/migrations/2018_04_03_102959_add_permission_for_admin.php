<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissionForAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('permissions')->insert([
            'name' => 'admin.exchange.index',
            'guard_name' => 'web',
            'description' => 'Admin access to the exchange section',
            'title' => 'Admin Exchange'
        ]); 

        // DB::table('role_has_permissions')->insert([
        //     'permission_id' => 23,
        //     'role_id' => 1
        // ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
