<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetRegistrationFlowFinish extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $vers = \App\Models\Verification::whereDate('created_at', '<', '2018-07-20')->get();
        foreach ($vers as $ver) {
            $ver->register_flow = 'finish';
            $ver->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
