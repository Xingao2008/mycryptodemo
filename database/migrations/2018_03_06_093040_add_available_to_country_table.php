<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvailableToCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->integer('display')->after('flag')->default(1);
            $table->integer('available')->after('display')->default(0);
        });

        DB::table('countries')
            ->where('name', 'Australia')
            ->update(['available' => 1]);

        DB::table('countries')
        ->where('name', 'New Zealand')
        ->update(['available' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn(['display', 'available']);
        });
    }
}
