<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('dob')->nullable();
            $table->string('last_login')->nullable();
            $table->boolean('verified')->default(FALSE);
            $table->string('tfa')->default(FALSE);
            $table->boolean('tfa_status')->default(FALSE);
            $table->string('ubc')->default(ubc());
            $table->string('status')->default('pending');
            $table->string('status_code')->nullable();
            $table->boolean('admin')->default(FALSE);
            $table->string('abn')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
