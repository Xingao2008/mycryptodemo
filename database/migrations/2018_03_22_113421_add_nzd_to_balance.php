<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNzdToBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = \App\Models\User::get();
        foreach($users as $user) {
            $has_record = \App\Models\Balance::where('user_id','=',$user->id)->where('currency','=','nzd')->first();

            if(!$has_record) {
                \App\Models\Balance::create([
                    'user_id'       => $user->id,
                    'currency'        => 'nzd',
                    'balance'          => 0.000000000000000000,
                    'locked'     => FALSE,
                ]);
            }
        }
        \App\Models\Setting::create([
            'id'       => 'nzd_daily_withdraw_limit',
            'value'        => '0',
            'is_boolean' => 0,
            'type' => 2,
        ]);
        \App\Models\Setting::create([
            'id'       => 'nzd_fee',
            'value'        => '0',
            'is_boolean' => 0,
            'type' => 0,
        ]);
        \App\Models\Setting::create([
            'id'       => 'nzd_withdraw_limit',
            'value'        => '0',
            'is_boolean' => 0,
            'type' => 2,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
