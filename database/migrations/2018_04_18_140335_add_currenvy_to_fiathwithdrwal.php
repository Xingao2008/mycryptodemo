<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrenvyToFiathwithdrwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fiat_withdraws', function(Blueprint $table){
            $table->string('currency')->nullable()->default('aud')->after('amount');
        });
        Schema::table('fiat_deposits', function(Blueprint $table){
            $table->string('currency')->nullable()->default('aud')->after('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fiat_withdraws', function (Blueprint $table) {
            $table->dropColumn(['currency']);
        }); 
        Schema::table('fiat_deposits', function (Blueprint $table) {
            $table->dropColumn(['currency']);
        }); 
    }
}
