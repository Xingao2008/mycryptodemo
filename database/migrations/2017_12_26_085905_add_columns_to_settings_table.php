<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function(Blueprint $table){
            $table->string('name')->nullable()->after('value');
            $table->boolean('is_boolean')->default(1)->after('name');
            $table->integer('type')->default(0)->after('is_boolean')->comment('0: setting, 1: pricing, 2: limit');
        });

        $settings = [
            ['id' => 'btc_fee', 'name' => 'BTC Fee', 'is_boolean' => 0, 'type' => 1],
            ['id' => 'eth_fee', 'name' => 'ETH Fee', 'is_boolean' => 0, 'type' => 1],
            ['id' => 'ltc_fee', 'name' => 'LTC Fee', 'is_boolean' => 0, 'type' => 1],
            ['id' => 'xrp_fee', 'name' => 'XRP Fee', 'is_boolean' => 0, 'type' => 1],
            ['id' => 'buys_active', 'name' => 'Enable Buys', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'deposits_active', 'name' => 'Enable Deposits', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'enable_polipay', 'name' => 'Enable PoliPay', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'maintenance_active', 'name' => 'Enable Maintenance Mode', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'sells_active', 'name' => 'Enable Sells', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'withdraws_active', 'name' => 'Enable Withdraws', 'is_boolean' => 1, 'type' => 0],
            ['id' => 'hidden_fee', 'name' => 'Hidden Fee', 'is_boolean' => 0, 'type' => 0],
            ['id' => 'public_fee', 'name' => 'Public Fee', 'is_boolean' => 0, 'type' => 0],

            ['id' => 'buy_transaction_limit', 'name' => 'buy transaction limit', 'is_boolean' => 0, 'type' => 2, 'value' => '3000'],
            ['id' => 'buy_daily_limit', 'name' => 'buy daily limit', 'is_boolean' => 0, 'type' => 2, 'value' => '6000'],
            ['id' => 'sell_transaction_limit', 'name' => 'sell transaction limit', 'is_boolean' => 0, 'type' => 2, 'value' => '3000'],
            ['id' => 'sell_daily_limit', 'name' => 'sell daily limit', 'is_boolean' => 0, 'type' => 2, 'value' => '6000'],
            ['id' => 'poli_transaction_limit', 'name' => 'poli transaction limit', 'is_boolean' => 0, 'type' => 2, 'value' => '1000'],
            ['id' => 'poli_daily_limit', 'name' => 'poli daily limit', 'is_boolean' => 0, 'type' => 2, 'value' => '2500'],

        ];

        foreach($settings as $setting) {
            $tmp_setting = \App\Models\Setting::firstOrNew(['id' => $setting['id']]);
            $tmp_setting->update($setting);
            $tmp_setting->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function(Blueprint $table){
            $table->dropColumn('name');
            $table->dropColumn('is_boolean');
            $table->dropColumn('type');
        });
    }
}
