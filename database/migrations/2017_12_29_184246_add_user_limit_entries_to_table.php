<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserLimitEntriesToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $limits = [
            [
                'id' => 'buy_transaction_limit',
                'value' => '3000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'buy_daily_limit',
                'value' => '6000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'sell_transaction_limit',
                'value' => '3000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'sell_daily_limit',
                'value' => '6000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'poli_transaction_limit',
                'value' => '1000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'poli_daily_limit',
                'value' => '3000',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'btc_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'btc_daily_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'ltc_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'ltc_daily_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'eth_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'eth_daily_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'xrp_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
            [
                'id' => 'xrp_daily_withdraw_limit',
                'value' => '1',
                'is_boolean' => 0,
                'type' => 2
            ],
        ];

        foreach($limits as $limit) {
           $s = \App\Models\Setting::firstOrNew(['id' => $limit['id']]);
           $s->fill($limit);
           $s->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
