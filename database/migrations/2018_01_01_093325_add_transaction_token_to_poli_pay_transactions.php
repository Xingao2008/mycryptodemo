<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionTokenToPoliPayTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('poli_pay_transactions', function (Blueprint $table) {
            $table->string('transaction_token')->after('user_id')->nullable();
            $table->dropColumn('reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('poli_pay_transactions', function (Blueprint $table) {
            $table->dropColumn('transaction_token');
            $table->string('reference')->unique()->index();
        });
    }
}
