<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExchangePauseToSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('settings')->insert([
            'id' => 'exchange_pause',
            'value' => '0',
            'name' => 'Pause exchange',
            'is_boolean' => 1,
            'type' => 0
        ]); 

        DB::table('permissions')->insert([
            'name' => 'admin.exchange.pause',
            'guard_name' => 'web',
            'description' => 'Admin can pause the exchange',
            'title' => 'Admin Exchange Pause'
        ]); 

        DB::table('role_has_permissions')->insert([
            'permission_id' => 24,
            'role_id' => 1
        ]); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
