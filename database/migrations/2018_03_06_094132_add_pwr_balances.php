<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPwrBalances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = \App\Models\User::get();
        foreach($users as $user) {
            $has_record = \App\Models\Balance::where('user_id','=',$user->id)->where('currency','=','pwr')->first();

            if(!$has_record) {
                \App\Models\Balance::create([
                    'user_id'       => $user->id,
                    'currency'        => 'pwr',
                    'balance'          => 0.000000000000000000,
                    'locked'     => FALSE,
                ]);
            }
        }
        \App\Models\Setting::create([
            'id'       => 'pwr_daily_withdraw_limit',
            'value'        => '500',
            'is_boolean' => 0,
            'type' => 2,
        ]);
        \App\Models\Setting::create([
            'id'       => 'pwr_fee',
            'value'        => '4',
            'is_boolean' => 1,
            'type' => 0,
        ]);
        \App\Models\Setting::create([
            'id'       => 'pwr_withdraw_limit',
            'value'        => '500',
            'is_boolean' => 0,
            'type' => 2,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $pwr_users = \App\Models\User::get();
        foreach($pwr_users as $pwr_user) {
            if($ind_user = \App\Models\Balance::where('user_id','=',$pwr_user->id)->where('currency','=','pwr')->where('balance','=',0.000000000000000000)->first()) {
                $ind_user->delete();
            }
        }

        if($pwr_daily = \App\Models\Setting::where('id','=','pwr_daily_withdraw_limit')->first()) {
            $pwr_daily->delete();
        }
        if($pwr_fee = \App\Models\Setting::where('id','=','pwr_fee')->first()) {
            $pwr_fee->delete();
        }
        if($pwr_limit = \App\Models\Setting::where('id','=','pwr_withdraw_limit')->first()) {
            $pwr_limit->delete();
        }
    }
}
