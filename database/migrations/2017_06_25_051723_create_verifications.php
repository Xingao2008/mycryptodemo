<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('account')->default('incomplete');
            $table->string('account_notes')->nullable();

            $table->string('id_passport')->default('incomplete');
            $table->string('id_passport_notes')->nullable();
            $table->string('id_drivers')->default('incomplete');
            $table->string('id_drivers_notes')->nullable();
            $table->string('id_bank')->default('incomplete');
            $table->string('id_bank_notes')->nullable();

            $table->string('ubc')->default('incomplete');
            $table->string('ubc_notes')->nullable();

            $table->string('sms')->default('incomplete');

            $table->string('bank')->default('incomplete');
            $table->string('bank_notes')->nullable();

            $table->string('tfa')->default('incomplete');
            $table->string('balance1')->default('incomplete');
            $table->string('balance2')->default('incomplete');

            $table->string('abn')->default('incomplete');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifications');
    }
}
