<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->bigInteger('min')->default(0);
            $table->bigInteger('max')->default(0);
            $table->decimal('fee', 36, 18)->nullable()->default(0);
            $table->integer('exchange_tier')->default(1);
            $table->timestamps();
        });
        foreach(config('fee.exchange') as $fee)
        {
            DB::table('fees')->insert([
                'type' => 'exchange',
                'min' => $fee['min'],
                'max' => $fee['max'],
                'fee' => $fee['fee'],
                'exchange_tier' => $fee['exchange_tier']
            ]);
        }

        Schema::table('users', function(Blueprint $table){
            $table->integer('exchange_tier')->nullable()->default(1)->after('remember_token');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['exchange_tier']);
        }); 
    }
}
