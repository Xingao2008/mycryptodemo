<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->after('id');
            $table->enum('order_type', ['limit', 'market'])->after('user_id');
            $table->string('currency', 5)->after('order_type')->nullable();
            $table->string('currency_pair', 5)->after('currency')->nullable();
            $table->enum('status',['active','complete','cancel','suspend'])->after('currency_pair');
            $table->decimal('volume', 36, 18)->after('status')->nullable();
            $table->enum('market_type',['buy','sell'])->after('volume');
            $table->decimal('price', 36, 18)->after('market_type')->nullable();
            $table->decimal('remaining', 36, 18)->after('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['user_id', 'order_type', 'currency', 'status', 'volume', 'market_type', 'price', 'remaining']);
        });
    }
}
