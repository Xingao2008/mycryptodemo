<?php

namespace App\Console\Commands;

use App\Models\PoliPayTransaction;
use Illuminate\Console\Command;

class UpdatePoliPayTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'polipay:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = PoliPayTransaction::whereNotIn('status', ['completed', 'cancelled', 'timedout'])->get();
        foreach($transactions as $transaction) {
            if(!$transaction->transaction_token) continue;

            $this->info('Processing: ' . $transaction->id);

            $p = new \App\Helpers\PoliPayApi();

            $rsp = $p->getTransaction($transaction->transaction_token);

            \App\Models\PoliPayTransaction::updateTransaction($rsp);
        }
    }
}
