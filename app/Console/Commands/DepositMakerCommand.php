<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Exception;

class DepositMakerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:DepositMakerCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes deposits into users accounts after confirmation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(setting('deposits_active')) {

            $failed_pwr = \App\Models\CryptoDeposit::where('status','=','insufficient funds for gas * price + value')->where('type','=','pwr')->get();

            foreach($failed_pwr as $key=>$value) {
                $failed_pwr_ind = \App\Models\CryptoDeposit::where('id','=',$value->id)->first();
                $failed_pwr_ind->status = "completing";
                $failed_pwr_ind->save();
            }

            \DB::beginTransaction();
            $pwr_deposits = \App\Models\CryptoDeposit::where('status', '=', "completing")->get();

            foreach($pwr_deposits as $key=>$pwr_deposit) {
                    $pwr_holding_wallet = config('crypto.holding.eth');
                    $transferkey = config('crypto.transfer_key.eth');
                    $pwr_holding_balance = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=pwr&address='.$pwr_holding_wallet));

                    $pwr_user_deposit = \App\Models\CryptoDeposit::where('id', '=', $pwr_deposit->id)->first();
                    $wallet = \App\Models\Wallet::where('address', '=', $pwr_user_deposit->to)->first();
                    if(!$wallet) {continue;}

                    $pwr_user_balance = \App\Models\Balance::where('user_id', '=', $wallet->user_id)->where('currency', '=', 'pwr')->first();
                    $pwr_oldbalance = $pwr_user_balance->balance;

                    $pwr_user_deposit->status = "complete";
                    $pwr_user_deposit->save();
                try {
                    $result = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer_token.php?coin=pwr&to='.$pwr_holding_wallet.'&from='.$pwr_deposit->to.'&key='.$transferkey.'&amount='.$pwr_deposit->amount));
                    if( is_string($result) ) {
                        if( substr($result, 0, 2) == "0x" ) {
                            $ledger = new \App\Models\Ledger;
                            $ledger->reference_id = $pwr_deposit->id;
                            $ledger->user_to = $wallet->user_id;
                            $ledger->user_from = 0;
                            $ledger->address_to = $pwr_holding_wallet;
                            $ledger->transaction_type = "holding_transfer";
                            $ledger->currency = "pwr";
                            $ledger->amount = $pwr_deposit->amount;
                            $ledger->oldbalance = $pwr_holding_balance;
                            $ledger->address_from = $pwr_deposit->from;
                            $ledger->transaction_id = $result;
                            $ledger->save();

                            $new_balance = bcadd($pwr_user_balance->balance, $pwr_deposit->amount, 18);
                            $pwr_user_balance->balance = $new_balance;
                            $pwr_user_balance->save();

                            $user = \App\Models\User::find($wallet->user_id);
                            $data = [
                                'email' => $user->email,
                                'amount' => $pwr_deposit->amount,
                                'type' => 'pwr',
                            ];
                            \Mail::send('email.crypto-deposit-complete', $data, function($message) use ($data) {
                                $message->to($data['email']);
                                $message->subject('MyCryptoWallet: POWR Crypto Deposit Complete!');
                            });
                            \App\Models\Emails::create([
                                'user_id'    => $user->id,
                                'to'      => $user->email,
                                'subject' => 'POWR Crypto Deposit Complete',
                                'data' => json_encode($data),
                            ]);

                        }
                    } else {
                        throw new \Exception('Error: '.$result->error);
                    }
                } catch (\Exception $e) {
                    \DB::rollBack();
                    $user_deposit = \App\Models\CryptoDeposit::where('id', '=', $pwr_user_deposit->id)->first();
                    if(null !== $e->getMessage()) {
                        $user_deposit->status = "failed, on final deposit function, ".$e->getMessage();
                    } else {
                        $user_deposit->status = "failed, on final deposit function, no message";
                    }
                    $user_deposit->save();
                }
                \DB::commit();
            }


            $deposits = \App\Models\CryptoDeposit::where('status', '=', "confirmed")->get();

            \DB::beginTransaction();
            foreach($deposits as $key=>$deposit) {
                // Skip holding wallet "deposits"
                // if( $deposit->to == config('crypto.holding.btc') ||
                //     $deposit->to == config('crypto.holding.ltc') ||
                //     $deposit->to == config('crypto.holding.eth') ||
                //     $deposit->to == config('crypto.holding.xrp'))
                // {continue;}

                // Skip deposits that don't match a wallet
                $wallet = \App\Models\Wallet::where('address', '=', $deposit->to)->first();
                if(!$wallet) {continue;}

                $user_balance = \App\Models\Balance::where('user_id', '=', $wallet->user_id)->where('currency', '=', $deposit->type)->first();
                $oldbalance = $user_balance->balance;

                $user_deposit = \App\Models\CryptoDeposit::where('id', '=', $deposit->id)->first();

                try {
                    if($deposit->type == "pwr") {
                        $user_deposit->status = "completing";
                    } else {
                        $user_deposit->status = "complete";
                    }

                    $user_deposit->save();

                    $ledger = new \App\Models\Ledger;
                    $ledger->reference_id = $deposit->id;
                    $ledger->user_to = $wallet->user_id;
                    $ledger->user_from = 0;
                    $ledger->address_to = $deposit->to;
                    $ledger->transaction_type = "deposit";
                    $ledger->currency = $deposit->type;
                    $ledger->amount = $deposit->amount;
                    $ledger->fee = $deposit->fees;
                    $ledger->oldbalance = $oldbalance;
                    if($deposit->type == "eth" || $deposit->type == "xrp" || $deposit->type == "pwr" ) {
                        $ledger->fee_type = "internal_transfer";
                    }
                    $ledger->address_from = $deposit->from;
                    $ledger->transaction_id = $deposit->transaction_id;
                    $ledger->save();

                    if($deposit->type == "btc" || $deposit->type == "ltc") {
                        $new_balance = bcadd($user_balance->balance, $deposit->amount, 18);
                        $user_balance->balance = $new_balance;
                        $user_balance->save();

                        // $user = \App\Models\User::find($wallet->user_id);
                        // $data = [
                        //     'email' => $user->email,
                        //     'amount' => $deposit->amount,
                        //     'type' => $deposit->type,
                        // ];
                        // \Mail::send('email.crypto-deposit-complete', $data, function($message) use ($data) {
                        //     $message->to($data['email']);
                        //     $message->subject('myCryptoWallet: '.strtoupper($data['type']).' Crypto Deposit Complete!');
                        // });
                        // \App\Models\Emails::create([
                        //     'user_id'    => $user->id,
                        //     'to'      => $user->email,
                        //     'subject' => strtoupper($data['type']).' Crypto Deposit Complete',
                        //     'data' => json_encode($data),
                        // ]);

                    }
                } catch (\Exception $e)
                {
                    \DB::rollBack();
                    $user_deposit = \App\Models\CryptoDeposit::where('id', '=', $deposit->id)->first();
                    $user_deposit->status = "failed";
                    $user_deposit->save();
                }

                try {
                    if($deposit->type == "eth") {
                        $holding_wallet = config('crypto.holding.eth');
                        $transferkey = config('crypto.transfer_key.eth');
                        $holding_balance = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=eth&address='.$holding_wallet));

                        $result = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer.php?coin=eth&from='.$deposit->to.'&key='.$transferkey.'&to='.$holding_wallet.'&amount='.$deposit->amount));

                        if( is_string($result) ) {
                            if( substr($result, 0, 2) == "0x" ) {
                                $ledger = new \App\Models\Ledger;
                                $ledger->reference_id = $deposit->id;
                                $ledger->user_to = $wallet->user_id;
                                $ledger->user_from = 0;
                                $ledger->address_to = $holding_wallet;
                                $ledger->transaction_type = "holding_transfer";
                                $ledger->currency = "eth";
                                $ledger->amount = $deposit->amount;
                                $ledger->oldbalance = $holding_balance;
                                $ledger->address_from = $deposit->from;
                                $ledger->transaction_id = $result;
                                $ledger->save();

                                $new_balance = bcadd($user_balance->balance, $deposit->amount, 18);
                                $user_balance->balance = $new_balance;
                                $user_balance->save();

                                $user = \App\Models\User::find($wallet->user_id);
                                $data = [
                                    'email' => $user->email,
                                    'amount' => $deposit->amount,
                                    'type' => 'eth',
                                ];
                                \Mail::send('email.crypto-deposit-complete', $data, function($message) use ($data) {
                                    $message->to($data['email']);
                                    $message->subject('myCryptoWallet: ETH Crypto Deposit Complete!');
                                });
                                \App\Models\Emails::create([
                                    'user_id'    => $user->id,
                                    'to'      => $user->email,
                                    'subject' => 'ETH Crypto Deposit Complete',
                                    'data' => json_encode($data),
                                ]);

                            }
                        } else {
                            $user_deposit_eth = \App\Models\CryptoDeposit::where('id', '=', $deposit->id)->where('type','=','eth')->first();
                            $user_deposit_eth->status = "failed";
                            $user_deposit_eth->save();
                            throw new \Exception('Error: '.json_encode($result));
                        }
                    }
                    if($deposit->type == "xrp") {
                        $holding_wallet = config('crypto.holding.xrp');

                        $secret = \App\Models\Ripple::where('account_id','=',$deposit->to)->first()->master_seed;
                        $holding_balance = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=xrp&address='.$holding_wallet));
                        $balance = json_decode(file_get_contents("https://cd.fairdigital.com.au/api/getbalance.php?coin=xrp&address=".$deposit->to));
                        $fee = getTxFees('xrp');
                        $min_bal = 20;
                        $check_amount = bcadd($deposit->amount,$fee,6);
                        $required_minimum = bcadd($check_amount, $min_bal, 6);


                        if(bccomp($balance,$required_minimum,6) >= 0) {
                            $final_amount_no_fee = $deposit->amount;
                            $final_amount = bcsub($deposit->amount,$fee,6);
                        } else {
                            $final_amount_no_fee = bcsub($deposit->amount,$min_bal,6);
                            $final_amount = bcsub($final_amount_no_fee,$fee,6);
                        }


                        $result = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer.php?coin=xrp&from='.$deposit->to.'&to='.$holding_wallet.'&secret='.$secret.'&amount='.$final_amount));

                        if( (isset($result->engine_result) && $result->engine_result == "tesSUCCESS") || (isset($result->engine_result) && $result->engine_result == "SUCCESS") ) {
                            $txid = $result->tx_json->hash;
                            $ledger = new \App\Models\Ledger;
                            $ledger->reference_id = $deposit->id;
                            $ledger->user_to = $wallet->user_id;
                            $ledger->user_from = 0;
                            $ledger->address_to = $holding_wallet;
                            $ledger->transaction_type = "holding_transfer";
                            $ledger->currency = "xrp";
                            $ledger->amount = $final_amount_no_fee;
                            $ledger->oldbalance = $holding_balance;
                            $ledger->address_from = $deposit->from;
                            $ledger->transaction_id = $txid;
                            $ledger->fee = 0.000001;
                            $ledger->fee_type = "internal_transfer";
                            $ledger->save();

                            $new_balance = bcadd($user_balance->balance, $final_amount, 18);
                            $user_balance->balance = $new_balance;
                            $user_balance->save();

                            $user = \App\Models\User::find($wallet->user_id);
                            $data = [
                                'email' => $user->email,
                                'amount' => $final_amount_no_fee,
                                'type' => 'xrp',
                            ];
                            \Mail::send('email.crypto-deposit-complete', $data, function($message) use ($data) {
                                $message->to($data['email']);
                                $message->subject('myCryptoWallet: XRP Crypto Deposit Complete!');
                            });
                            \App\Models\Emails::create([
                                'user_id'    => $user->id,
                                'to'      => $user->email,
                                'subject' => 'XRP Crypto Deposit Complete',
                                'data' => json_encode($data),
                            ]);

                        } else {
                            $user_deposit_xrp = \App\Models\CryptoDeposit::where('id', '=', $deposit->id)->where('type','=','xrp')->first();
                            if(isset($result->engine_result)) {
                                $user_deposit_xrp->status = "failed:".$result->engine_result;
                                $user_deposit_xrp->save();
                                throw new \Exception('Error: '.json_encode($result->engine_result));
                            } else {
                                $user_deposit_xrp->status = "failed on deposit, no message";
                                $user_deposit_xrp->save();
                                throw new \Exception('Error: failed on deposit, no message');
                            }
                        }
                    }
                    if($deposit->type == "pwr") {
                        $actual_eth_balance = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=eth&address='.$deposit->to));
                        if($actual_eth_balance < 0.0061) {
                            $holding_wallet = config('crypto.holding.eth');
                            $transferkey = config('crypto.holding.ethkey');
                            $holding_balance = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=eth&address='.$holding_wallet));

                            $result = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer.php?coin=eth&to='.$deposit->to.'&key='.$transferkey.'&from='.$holding_wallet.'&amount=0.008'));

                            if( is_string($result) ) {
                                if( substr($result, 0, 2) == "0x" ) {
                                    $ledger = new \App\Models\Ledger;
                                    $ledger->reference_id = $deposit->id;
                                    $ledger->user_to = $wallet->user_id;
                                    $ledger->user_from = 0;
                                    $ledger->address_to = $holding_wallet;
                                    $ledger->transaction_type = "pwr_transaction_fee_paid";
                                    $ledger->currency = "eth";
                                    $ledger->amount = 0.008;
                                    $ledger->oldbalance = $holding_balance;
                                    $ledger->address_from = $deposit->from;
                                    $ledger->transaction_id = $result;
                                    $ledger->save();

                                    // store this as ignored so we don't credit the user this fee!
                                    \App\Models\IgnoredTransactions::create([
                                        'txid'       => $result,
                                        'currency'   => 'eth',
                                        'reason'     => 'pwr fee',

                                    ]);
                                }
                            }
                        }
                    }
                } catch (\Exception $e)
                {
                    \DB::rollBack();
                    $user_deposit = \App\Models\CryptoDeposit::where('id', '=', $deposit->id)->first();
                    if(null !== $e->getMessage()) {
                        $user_deposit->status = "failed, on final deposit function, ".$e->getMessage();
                    } else {
                        $user_deposit->status = "failed, on transaction funding, no message";
                    }
                    $user_deposit->save();
                }

                \DB::commit();

            }



        }

    }
}
