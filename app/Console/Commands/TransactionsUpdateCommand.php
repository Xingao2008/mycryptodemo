<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\CryptoDeposit;

class TransactionsUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TransactionsUpdateCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the latest transactions from CryptoDaemon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $holdingwallet = config('crypto.holding.eth');
        $xrp_holdingwallet = config('crypto.holding.xrp');

        $btc_data = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=btc'));
        $ltc_data = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=ltc'));
        $eth_data = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=eth&address=' . $holdingwallet));
        $xrp_data = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=xrp&address=' . $xrp_holdingwallet));
        $pwr_data = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=pwr&address=' . $holdingwallet));

        // **************************
        // Bitcoin Transaction Update
        // **************************
        foreach ($btc_data as $key => $value) {
            if ($value->category == "receive") {
                $txid = $value->txid;
                $address = $value->address;
                $confirmations = $value->confirmations;
                $amount = btcFormat($value->amount);
                if ($confirmations < 6) {
                    $status = "unconfirmed";
                } else {
                    $status = "confirmed";
                }

                $deposit = \App\Models\CryptoDeposit::where('transaction_id', '=', $txid)->where('to', '=', $address)->where('type', '=', 'btc')->first();

                $user_wallet = \App\Models\Wallet::where('address', '=', $address)->where('type', '=', 'btc')->first();

                if ($user_wallet == null) {
                    $userid = 0;
                } else {
                    $userid = $user_wallet->user_id;
                }

                if ($deposit == null) {
                    $new_deposit = new \App\Models\CryptoDeposit;
                    $new_deposit->user_id = $userid;
                    $new_deposit->to = $address;
                    $new_deposit->from = "unknown";
                    $new_deposit->type = 'btc';
                    $new_deposit->transaction_id = $txid;
                    $new_deposit->amount = $amount;
                    $new_deposit->confirmations = $confirmations;
                    $new_deposit->status = $status;
                    $new_deposit->save();

                } else {
                    $deposit->user_id = $userid;
                    $deposit->confirmations = $confirmations;
                    if ($deposit->status == "unconfirmed") {
                        $deposit->status = $status;
                    }
                    $deposit->save();
                }
            }


            if ($value->category == "send") {
                $txid = $value->txid;
                $to = $value->address;
                $confirmations = $value->confirmations;
                $amount = btcFormat($value->amount);
                $fee = btcFormat($value->fee);
                if ($confirmations < 6) {
                    $status = "unconfirmed";
                } else {
                    $status = "confirmed";
                }

                $withdraw = \App\Models\CryptoWithdraw::where('transaction_id', '=', $txid)->where('to', '=', $to)->where('type', '=', 'btc')->first();

                if ($withdraw == null) {

                    // $new_withdraw = new \App\Models\CryptoWithdraw;
                    // $new_withdraw->user_id = 0;
                    // $new_withdraw->to = $to;
                    // $new_withdraw->from = "unknown";
                    // $new_withdraw->type = 'btc';
                    // $new_withdraw->transaction_id = $txid;
                    // $new_withdraw->amount = $amount;
                    // $new_withdraw->confirmations = $confirmations;
                    // $new_withdraw->status = "unreconciled";
                    // $new_withdraw->save();

                } else {
                    $withdraw->confirmations = $confirmations;
                    if ($withdraw->status == "unconfirmed" || $withdraw->status == "sent") {
                        $withdraw->status = $status;
                    }
                    if (!($withdraw->txfees ?? 1)) {
                        $withdraw->txfees = $fee;
                    }
                    $withdraw->save();

                    $ledger_update = \App\Models\Ledger::where('reference_id', '=', 'w' . $withdraw->id)->first();
                    if (!($ledger_update->transaction_id ?? 1)) {
                        $ledger_update->transaction_id = $txid;
                    }
                    $ledger_update->save();
                }
            }
        }


        // **************************
        // Litecoin Transaction Update
        // **************************
        foreach ($ltc_data as $key => $value) {
            if ($value->category == "receive") {
                $txid = $value->txid;
                $address = $value->address;
                $confirmations = $value->confirmations;
                $amount = ltcFormat($value->amount);
                if ($confirmations < 12) {
                    $status = "unconfirmed";
                } else {
                    $status = "confirmed";
                }

                $deposit = \App\Models\CryptoDeposit::where('transaction_id', '=', $txid)->where('to', '=', $address)->where('type', '=', 'ltc')->first();

                $user_wallet = \App\Models\Wallet::where('address', '=', $address)->where('type', '=', 'ltc')->first();

                if ($user_wallet == null) {
                    $userid = 0;
                } else {
                    $userid = $user_wallet->user_id;
                }

                if ($deposit == null) {

                    $new_deposit = new \App\Models\CryptoDeposit;
                    $new_deposit->user_id = $userid;
                    $new_deposit->to = $address;
                    $new_deposit->from = "unknown";
                    $new_deposit->type = 'ltc';
                    $new_deposit->transaction_id = $txid;
                    $new_deposit->amount = $amount;
                    $new_deposit->confirmations = $confirmations;
                    $new_deposit->status = $status;
                    $new_deposit->save();

                } else {
                    $deposit->user_id = $userid;
                    $deposit->confirmations = $confirmations;
                    if ($deposit->status == "unconfirmed") {
                        $deposit->status = $status;
                    }
                    $deposit->save();
                }
            }
            if ($value->category == "send") {
                $txid = $value->txid;
                $to = $value->address;
                $confirmations = $value->confirmations;
                $amount = ltcFormat($value->amount);
                $fee = ltcFormat($value->fee);
                if ($confirmations < 6) {
                    $status = "unconfirmed";
                } else {
                    $status = "confirmed";
                }

                $withdraw = \App\Models\CryptoWithdraw::where('transaction_id', '=', $txid)->where('to', '=', $to)->where('type', '=', 'ltc')->first();

                if ($withdraw == null) {

                    // $new_withdraw = new \App\Models\CryptoWithdraw;
                    // $new_withdraw->user_id = 0;
                    // $new_withdraw->to = $to;
                    // $new_withdraw->from = "unknown";
                    // $new_withdraw->type = 'ltc';
                    // $new_withdraw->transaction_id = $txid;
                    // $new_withdraw->amount = $amount;
                    // $new_withdraw->confirmations = $confirmations;
                    // $new_withdraw->status = "unreconciled";
                    // $new_withdraw->save();

                } else {
                    $withdraw->confirmations = $confirmations;
                    if ($withdraw->status == "unconfirmed" || $withdraw->status == "sent") {
                        $withdraw->status = $status;
                    }
                    if ($withdraw->txfees == NULL) {
                        $withdraw->txfees = $fee;
                    }
                    $withdraw->save();

                    $ledger_update = \App\Models\Ledger::where('reference_id', '=', 'w' . $withdraw->id)->first();
                    if ($ledger_update->transaction_id == NULL) {
                        $ledger_update->transaction_id = $txid;
                    }
                    $ledger_update->save();
                }
            }
        }

        // **************************
        // Ethereum Transaction Update - withdrawals only
        // **************************

        //Semi-absent due to alternative implementation by Eth devs. Handled manually.
        if ($eth_data) {
            foreach ($eth_data as $key => $value) {
                if ($value->from == $holdingwallet) {
                    $record = \App\Models\CryptoWithdraw::where('type', '=', 'eth')->where('status', '=', 'sent')->where('transaction_id', '=', $value->hash)->first();
                    if ($record) {
                        $record->amount = $value->amount;
                        $record->txfees = $value->fee;
                        $record->confirmations = $value->confirmations;
                        if ($record->confirmations > 20) {
                            $record->status = "complete";
                        }
                        $record->save();
                    }
                }
            }
        }

        // **************************
        // POWR Transaction Update - withdrawals only
        // **************************

        //Semi-absent due to alternative implementation by Eth devs. Handled manually.
        if ($pwr_data) {
            foreach ($pwr_data as $key => $value) {
                if ($value->from == $holdingwallet) {
                    $record = \App\Models\CryptoWithdraw::where('type', '=', 'pwr')->where('status', '=', 'sent')->where('transaction_id', '=', $value->hash)->first();
                    if ($record) {
                        $record->amount = $value->amount;
                        $record->txfees = $value->fee;
                        $record->confirmations = $value->confirmations;
                        if ($record->confirmations > 20) {
                            $record->status = "complete";
                        }
                        $record->save();
                    }
                }
            }
        }


        // **************************
        // Ripple Transaction Update
        // **************************
        if ($xrp_data) {
            foreach ($xrp_data as $key => $value) {
                if ($value->from == $xrp_holdingwallet) {
                    $record = \App\Models\CryptoWithdraw::where('type', '=', 'xrp')->where('status', '=', 'sent')->where('transaction_id', '=', $value->hash)->first();
                    if ($record) {
                        $record->amount = $value->amount;
                        $record->txfees = $value->fee / 1000000;
                        $record->confirmations = $value->confirmations;
                        if ($record->confirmations) {
                            $record->status = "complete";
                        }
                        $record->save();
                        $ledger = \App\Models\Ledger::where('currency', '=', 'xrp')->where('transaction_id', '=', 'null')->where('reference_id', '=', 'w' . $record->id)->first();
                        if ($ledger) {
                            $ledger->transaction_id = $record->hash;
                            $ledger->save();
                        }
                    }
                }
            }
        }

        $pwr_unconfirmed = \App\Models\CryptoDeposit::where('type','=','pwr')->where('status','=','unconfirmed')->get();

        if($pwr_unconfirmed) {
            foreach($pwr_unconfirmed as $key=>$value) {
                $api_url = 'https://cd.fairdigital.com.au/api/transactions.php?coin=pwr&address=' . $value->to;
                $eth_data = json_decode(file_get_contents($api_url));

                foreach ($eth_data as $key => $value) {
                    if ($value->type == "deposit") {
                        $deposit = \App\Models\CryptoDeposit::where('type','=','pwr')->where('transaction_id','=',$value->hash)->first();
                        if($deposit) {

                            $confirmations = $value->confirmations;

                            if ($confirmations < 30) {
                                $status = "unconfirmed";
                            } else {
                                $status = "confirmed";
                                $deposit->confirmations = $confirmations;
                                $deposit->status = $status;
                                $deposit->save();
                            }
                        }
                    }
                }
            }
        }



        $xrp_wallets = \App\Models\Wallet::where('type', '=', 'xrp')->get();

        if ($xrp_wallets) {
            foreach ($xrp_wallets as $key => $ind_wallet) {

                $xrp_datas = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transactions.php?coin=xrp&address=' . $ind_wallet->address));

                if ($xrp_datas) {
                    foreach ($xrp_datas as $xkey => $value) {
                        if ($ind_wallet->address == $value->to) {
                            $txid = $value->hash;
                            $address = $value->to;
                            if ($value->confirmations) {
                                $confirmations = 1;
                            } else {
                                $confirmations = 0;
                            }
                            $amount = $value->amount;
                            if (!$confirmations) {
                                $status = "unconfirmed";
                            } else {
                                $status = "confirmed";
                            }

                            $deposit = \App\Models\CryptoDeposit::where('transaction_id', '=', $txid)->where('to', '=', $address)->where('type', '=', 'xrp')->first();

                            $user_wallet = \App\Models\Wallet::where('address', '=', $address)->where('type', '=', 'xrp')->first();

                            if ($user_wallet == null) {
                                $userid = 0;
                            } else {
                                $userid = $user_wallet->user_id;
                            }

                            if ($deposit == null) {

                                $new_deposit = new \App\Models\CryptoDeposit;
                                $new_deposit->user_id = $userid;
                                $new_deposit->to = $address;
                                $new_deposit->from = $value->from;
                                $new_deposit->type = 'xrp';
                                $new_deposit->transaction_id = $txid;
                                $new_deposit->amount = $amount;
                                $new_deposit->confirmations = $confirmations;
                                $new_deposit->status = $status;
                                $new_deposit->save();

                            } else {
                                $deposit->user_id = $userid;
                                $deposit->confirmations = $confirmations;
                                if ($deposit->status == "unconfirmed") {
                                    $deposit->status = $status;
                                }
                                $deposit->save();
                            }

                        }
                    }
                }
            }
        }


    }
}
