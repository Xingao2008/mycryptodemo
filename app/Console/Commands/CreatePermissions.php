<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use App\Models\Permission;

class CreatePermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $roles = ['admin', 'user'];

        $permissions = collect([
            ['name' => 'admin.dashboard', 'title' => 'Admin Access', 'description' => 'Allows access to the admin section'],
            ['name' => 'admin.dashboard.stats', 'title' => 'Admin Dashboard Stats', 'description' => 'Allows access to the stats on the admin dashboard'],
            ['name' => 'admin.balances.index', 'title' => 'Admin Balances Page', 'description' => 'Allows access to the list of balances'],
            ['name' => 'admin.impersonate', 'title' => 'Impersonate', 'description' => 'Allows access to impersonate users'],
            ['name' => 'admin.balances.edit', 'title' => 'Admin Balances Edit', 'description' => 'Allows access to edit users balances'],
            ['name' => 'admin.deposits.index', 'title' => 'Admin Deposits', 'description' => 'Allows access to the deposits section'],
            ['name' => 'admin.withdrawals.index', 'title' => 'Admin Withdrawals', 'description' => 'Allows access to the withdrawals section'],
            ['name' => 'admin.ledgers.index', 'title' => 'Admin Ledgers', 'description' => 'Allows access to the ledgers section'],
            ['name' => 'admin.emails.index', 'title' => 'Admin Emails', 'description' => 'Allows access to the emails section'],
            ['name' => 'admin.firewall.index', 'title' => 'Admin Firewall', 'description' => 'Allows access to the firewall section'],
            ['name' => 'admin.orders.index', 'title' => 'Admin Orders', 'description' => 'Allows access to the orders section'],
            ['name' => 'admin.news.index', 'title' => 'Admin News', 'description' => 'Allows access to the news section'],
            ['name' => 'admin.polipay.index', 'title' => 'Admin POLi Pay', 'description' => 'Allows access to the polipay list'],
            ['name' => 'admin.settings.index', 'title' => 'Admin Settings', 'description' => 'Allows access to the settings editor'],
            ['name' => 'admin.users.index', 'title' => 'Admin Users', 'description' => 'Allows access to the users list/editor'],
            ['name' => 'admin.userlimits.index', 'title' => 'Admin User Limits', 'description' => 'Allows access to the user limits editor'],
            ['name' => 'admin.roles.index', 'title' => 'Admin Roles', 'description' => 'Allows access to the roles editor'],
            ['name' => 'admin.verifications.index', 'title' => 'Admin Verifications', 'description' => 'Allows access to the verifications editor'],
            ['name' => 'admin.failedtransactions.index', 'title' => 'Admin Failed Transactions', 'description' => 'Allows access to the failed transactions editor'],
            ['name' => 'admin.permissions.index', 'title' => 'Admin Permissions', 'description' => 'Allows access to the add/remove permissions'],
            ['name' => 'admin.resettfa', 'title' => 'Admin Reset 2FA', 'description' => 'Allows access to reset a users 2FA status'],
            ['name' => 'admin.selfie.index', 'title' => 'Admin Verify Selfies', 'description' => 'Allows admin to verify a users selfie'],
            ['name' => 'admin.users.blocked', 'title' => 'Admin Blocked Users', 'description' => 'Allows admin to unblock users'],
        ]);

        foreach ($permissions as $permission) {
            $permission['guard_name'] = 'web';
            $tmp_perm = Permission::firstOrCreate(['name' => $permission['name']]);
            $tmp_perm->fill($permission);
            $tmp_perm->save();
        }

        foreach ($roles as $role) {

            $r = Role::where('name', $role)->first();
            if (!$r) {
                $r = Role::create(['name' => $role]);
            }

            if ($role == 'admin') {
                $r->syncPermissions($permissions->pluck('name'));
            }
        }

        User::all()->chunk(50, function ($users) {
            foreach ($users as $user) {
                $user->syncRoles('user');
            }
        });

        $user = User::find(1);
        $user->syncRoles('admin');
    }
}
