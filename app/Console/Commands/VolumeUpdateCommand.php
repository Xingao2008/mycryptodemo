<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class VolumeUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:VolumeUpdateCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transaction volume update command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transaction_volume_items = \App\Models\TransactionVolume::pluck('reference_id')->all();
        $users = \App\Models\User::where('status','=','active')->get();
        foreach($users as $user) {
            $ledgers = \App\Models\Ledger::where('user_to','=',$user->id)->whereNotIn('id', $transaction_volume_items)->get();
            if (empty($ledgers)) {
                continue;
            }
            foreach ($ledgers as $ledger) {
                if($ledger->currency == "aud") {
                    if($ledger->transaction_type !== "deposit") {
                        $new_tv = new \App\Models\TransactionVolume;
                        $new_tv->reference_id = $ledger->id;
                        $new_tv->user_id = $ledger->user_to;
                        $new_tv->action = $ledger->transaction_type;
                        $new_tv->value = $ledger->amount;
                        $new_tv->currency = "aud";
                        $new_tv->price_used = "1.000000000000000000";
                        $new_tv->created_at = $ledger->created_at;
                        $new_tv->updated_at = $ledger->updated_at;
                        $new_tv->save();
                    }
                } else {
                    if($ledger->transaction_type != "buy_received") {
                        $price = latest_price($ledger->currency);

                        $aud_amount = bcmul($ledger->amount, $price, 18);

                        $new_tv = new \App\Models\TransactionVolume;
                        $new_tv->reference_id = $ledger->id;
                        $new_tv->user_id = $ledger->user_to;
                        $new_tv->action = $ledger->transaction_type;
                        $new_tv->value = $aud_amount;
                        $new_tv->currency = $ledger->currency;
                        $new_tv->price_used = $price;
                        $new_tv->created_at = $ledger->created_at;
                        $new_tv->updated_at = $ledger->updated_at;
                        $new_tv->save();
                    }
                }
            }
        }

    }
}
