<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Bittrex\Bittrex;

class BuySellCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:BuySellCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Triggers any buy or sell actions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //pending = order placed on mcw
        //underway = processing started, prevents multiple attempts
        //bought/sold = order placed on btcmarkets
        //receiving/sending = funds received or sent to btcmarkets
        //complete = confirmations requirements satisfied and funds deposited
        //no_wallet = user didnt crete a wallet and thus needs to before continuing
        //balance_error = user has a negative balance

        /**
         * Withdraws crypto from BTCMarkets from buy orders
         */
        $buys_bought = \App\Models\CryptoBuy::where('status', '=', 'bought')->get();
        if ($buys_bought) {
            foreach ($buys_bought as $key => $buy_bought) {


                $user_buy_bought = \App\Models\CryptoBuy::where('id','=',$buy_bought->id)->first();

                $currency = strtoupper($user_buy_bought->type);

                if ($user_buy_bought->type == "btc") {
                    $holdingwallet = config('crypto.holding.btc');
                    $fee = 0.001;
                }
                if ($user_buy_bought->type == "ltc") {
                    $holdingwallet = config('crypto.holding.ltc');
                    $fee = 0.01;
                }
                if ($user_buy_bought->type == "eth") {
                    $holdingwallet = config('crypto.holding.eth');
                    $fee = 0.006;
                }
                if ($user_buy_bought->type == "xrp") {
                    $holdingwallet = config('crypto.holding.xrp');
                    $fee = 1;
                }
                if ($user_buy_bought->type == "pwr") {
                    $holdingwallet = config('crypto.holding.eth');
                    $fee = 1;
                }

                $amount = bcadd($user_buy_bought->amount, $fee, 16);

                $bittrex = new Bittrex;

                try {
                    $result = $bittrex->withdraw($currency, $amount, $holdingwallet);
                    $user_buy_bought->status = "receiving";
                    $user_buy_bought->bittrex_withdraw_id = $result->uuid;
                    $user_buy_bought->save();

                    //warn Jaryd if USDT Balance lower than certain amount
                    sendUsdtWarning();
                }
                catch (\Exception $e) {
                    if(null !== $e->getMessage()) {
                        $user_buy_bought->status = "withdraw market error:".$e->getMessage();
                    } else {
                        $user_buy_bought->status = "failed, no message";
                    }
                    $user_buy_bought->save();
                }



            }
        }

        /**
         * Places orders on Bittrex
         */
        $buys = \App\Models\CryptoBuy::where('status', '=', 'pending')->get();
        if ($buys) {
            foreach ($buys as $key => $buy) {
                $user = \App\Models\User::where('id', '=', $buy->user_id)->first();
                $wallet = \App\Models\Wallet::where('user_id', '=', $buy->user_id)->where('type', '=', $buy->type)->first();
                if (!$wallet) {
                    //no wallet, order will fall out of processing now
                    $buy_failure = \App\Models\CryptoBuy::where('id', '=', $buy->id)->first();
                    $buy_failure->status = "no_wallet";
                    $buy_failure->save();
                    continue;
                }

                $user_balance = \App\Models\Balance::where('user_id', '=', $buy->user_id)->where('currency', '=', $buy->type)->first();
                $balance = $user_balance->balance;
                if ($balance < 0) {
                    //balance has a negative number so freeze the user account
                    $user->status = "inactive: negative balance";
                    $buy_failure = \App\Models\CryptoBuy::where('id', '=', $buy->id)->first();
                    $buy_failure->status = "balance_error";
                    $buy_failure->save();
                    continue;
                }

                $user_buy = \App\Models\CryptoBuy::where('id', '=', $buy->id)->first();

                $coin_type = $user_buy->type;

                if($coin_type == "btc") {
                    $fee = 0.001;
                }
                if($coin_type == "eth") {
                    $fee = 0.006;
                }
                if($coin_type == "ltc") {
                    $fee = 0.01;
                }
                if($coin_type == "xrp") {
                    $fee = 1;
                }
                if($coin_type == "pwr") {
                    $fee = 1;
                }

                if($coin_type == "pwr") {
                    $bittrex = new Bittrex;

                    $market = "USDT-BTC";
                    $quantity = bcmul(bcadd($user_buy->amount, $fee, 16), bittrex_rate_btc('powr', 'buy'), 16);
                    $rate = bittrex_rate('btc', 'buy');
                    try {
                        $result = $bittrex->buyLimit($market, $quantity, $rate);
                        $user_buy->status = "bought-btc";
                        $user_buy->bittrex_order_id = $result->uuid;
                        $user_buy->bittrex_rate = 0;
                        $user_buy->save();
                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_buy->status = "buy market error:".$e->getMessage();
                        } else {
                            $user_buy->status = "failed, no message";
                        }
                        $user_buy->save();
                    }

                    sleep(1);

                    $bittrex2 = new Bittrex;

                    $market = "BTC-POWR";
                    $quantity = bcadd($user_buy->amount, $fee, 16);
                    $rate = bittrex_rate_btc('powr', 'buy');
                    try {
                        $result = $bittrex2->buyLimit($market, $quantity, $rate);

                        $user_buy->status = "complete";
                        $user_buy->bittrex_order_id = $result->uuid;
                        $user_buy->bittrex_rate = usd_to_currency(bcmul(bittrex_rate_btc('powr', 'buy'), bittrex_rate('btc', 'buy'),16), 'AUD');
                        $user_buy->bittrex_txid = "stored on bittrex";
                        $user_buy->save();

                        $oldbalance = $user_balance->balance;

                        $ledger = new \App\Models\Ledger;
                        $ledger->reference_id = "bw".$user_buy->id;
                        $ledger->user_to = $user_buy->user_id;
                        $ledger->user_from = 0;
                        $ledger->address_to = config('crypto.holding.eth');
                        $ledger->transaction_type = "buy_received";
                        $ledger->currency = $coin_type;
                        $ledger->amount = $user_buy->amount;
                        $ledger->fee = $fee;
                        $ledger->oldbalance = $oldbalance;
                        $ledger->fee_type = "bittrex_withdraw";
                        $ledger->address_from = "Bittrex";
                        $ledger->transaction_id = "stored on bittrex";
                        $ledger->save();

                        $final_amount = $user_buy->amount;
                        $new_balance = bcadd($user_balance->balance, $final_amount, 18);
                        $user_balance->balance = $new_balance;
                        $user_balance->save();

                        $ledger_for_email = \App\Models\Ledger::where('reference_id','=','b'.$user_buy->id)->first();
                        $user_facing_buy_fee = (setting('public_fee') / 100) * $user_buy->total;
                        $user = \App\Models\User::find($user_buy->user_id);
                        $data = [
                            'email' => $user->email,
                            'amount' => $user_buy->amount,
                            'received' => $user_buy->total,
                            'fee' => $user_facing_buy_fee,
                            'type' => $user_buy->type,
                        ];
                        \Mail::send('email.buy-receipt', $data, function($message) use ($data) {
                            $message->to($data['email']);
                            $message->subject('myCryptoWallet: '.strtoupper($data['type']).' Buy Receipt');
                        });
                        \App\Models\Emails::create([
                            'user_id'    => $user->id,
                            'to'      => $user->email,
                            'subject' => strtoupper($data['type']).' Buy Receipt',
                            'data' => json_encode($data),
                        ]);


                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_buy->status = "buy pwr market error:".$e->getMessage();
                        } else {
                            $user_buy->status = "failed, no message, pwr";
                        }
                        $user_buy->save();
                    }
                } else {
                    // ***************
                    // all other coins
                    // ***************
                    $bittrex = new Bittrex;

                    $market = "USDT-".strtoupper($user_buy->type);
                    $quantity = bcadd($user_buy->amount, $fee, 16);
                    $rate = bittrex_rate($user_buy->type, 'buy');
                    try {
                        $result = $bittrex->buyLimit($market, $quantity, $rate);
                        $user_buy->status = "bought";
                        $user_buy->bittrex_order_id = $result->uuid;
                        $user_buy->bittrex_rate = usd_to_currency($rate, 'AUD');
                        $user_buy->save();


                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_buy->status = "buy market error:".$e->getMessage();
                        } else {
                            $user_buy->status = "failed, no message";
                        }
                        $user_buy->save();
                    }
                }


            }
        }

        /**
         * Credit the user once funds have landed
         */
        $bittrex_withdraws_obj = new Bittrex;

        $withdraw_history = $bittrex_withdraws_obj->getWithdrawalHistory();
        $pending_buys = \App\Models\CryptoBuy::where('status','=','receiving')->get();
        foreach($pending_buys as $key=>$value){
            foreach($withdraw_history as $wh_key=>$wh_value) {
                if($wh_value->PaymentUuid == $value->bittrex_withdraw_id) {

                    $coin_type = $value->type;

                    $buy_update = \App\Models\CryptoBuy::where('id','=',$value->id)->first();
                    $amount = $buy_update->amount;

                    if($coin_type == "btc") {
                        $holdingwallet = config('crypto.holding.btc');
                        $fee = 0.001;
                    }
                    if($coin_type == "eth") {
                        $holdingwallet = config('crypto.holding.eth');
                        $fee = 0.006;
                    }
                    if($coin_type == "ltc") {
                        $holdingwallet = config('crypto.holding.ltc');
                        $fee = 0.01;
                    }
                    if($coin_type == "xrp") {
                        $holdingwallet = config('crypto.holding.xrp');
                        $fee = 1;
                    }

                    $wallet = \App\Models\Wallet::where('user_id', '=', $buy_update->user_id)->where('type','=',$coin_type)->first();
                    if(!$wallet) {continue;}

                    $user_balance = \App\Models\Balance::where('user_id', '=', $buy_update->user_id)->where('currency', '=', $coin_type)->first();
                    $oldbalance = $user_balance->balance;

                    $ledger = new \App\Models\Ledger;
                    $ledger->reference_id = "bw".$buy_update->id;
                    $ledger->user_to = $buy_update->user_id;
                    $ledger->user_from = 0;
                    $ledger->address_to = $holdingwallet;
                    $ledger->transaction_type = "buy_received";
                    $ledger->currency = $coin_type;
                    $ledger->amount = $amount;
                    $ledger->fee = $fee;
                    $ledger->oldbalance = $oldbalance;
                    $ledger->fee_type = "bittrex_withdraw";
                    $ledger->address_from = "Bittrex";
                    $ledger->transaction_id = $wh_value->TxId;
                    $ledger->save();

                    $final_amount = $amount;
                    $new_balance = bcadd($user_balance->balance, $final_amount, 18);
                    $user_balance->balance = $new_balance;
                    $user_balance->save();

                    $buy_update->status = "complete";
                    $buy_update->bittrex_txid = $wh_value->TxId;
                    $buy_update->save();

                    $ledger_for_email = \App\Models\Ledger::where('reference_id','=','b'.$buy_update->id)->first();
                    $user_facing_buy_fee = (setting('public_fee') / 100) * $buy_update->total;
                    $user = \App\Models\User::find($buy_update->user_id);
                    $data = [
                        'email' => $user->email,
                        'amount' => $buy_update->amount,
                        'received' => $buy_update->total,
                        'fee' => $user_facing_buy_fee,
                        'type' => $buy_update->type,
                    ];
                    \Mail::send('email.buy-receipt', $data, function($message) use ($data) {
                        $message->to($data['email']);
                        $message->subject('myCryptoWallet: '.strtoupper($data['type']).' Buy Receipt');
                    });
                    \App\Models\Emails::create([
                        'user_id'    => $user->id,
                        'to'      => $user->email,
                        'subject' => strtoupper($data['type']).' Buy Receipt',
                        'data' => json_encode($data),
                    ]);

                }
            }
        }


        /**
         * Sends deposit to Bittrex
         */
        $sells = \App\Models\CryptoSell::where('status', '=', 'pending')->get();
        if ($sells) {
            foreach ($sells as $key => $sell) {
                $user = \App\Models\User::where('id', '=', $sell->user_id)->first();
                $wallet = \App\Models\Wallet::where('user_id', '=', $sell->user_id)->where('type', '=', $sell->type)->first();
                if (!$wallet) {
                    //no wallet, order will fall out of processing now
                    $sell_failure = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                    $sell_failure->status = "no_wallet";
                    $sell_failure->save();
                    continue;
                }

                $user_balance = \App\Models\Balance::where('user_id', '=', $sell->user_id)->where('currency', '=', $sell->type)->first();
                $balance = $user_balance->balance;
                if ($balance < 0) {
                    //balance has a negative number so freeze the user account
                    $user->status = "inactive: negative balance";
                    $sell_failure = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                    $sell_failure->status = "balance_error";
                    $sell_failure->save();
                    continue;
                }

                $user_sell = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                $user_sell->status = "processing";
                $user_sell->save();

                $type = $user_sell->type;
                $amount = $user_sell->amount;

                if ($type == "btc") {
                    $key = 'key=' . config('crypto.transfer_key.btc');
                    $to = config('crypto.bittrex.btc');
                    $from = config('crypto.holding.btc');
                    $fee = 0.0011;
                    $dt = "";
                }
                if ($type == "ltc") {
                    $key = 'key=' . config('crypto.transfer_key.ltc');
                    $to = config('crypto.bittrex.ltc');
                    $from = config('crypto.holding.ltc');
                    $fee = 0.011;
                    $dt = "";
                }
                if ($type == "eth") {
                    $key = 'key=' . config('crypto.holding.ethkey');
                    $to = config('crypto.bittrex.eth');
                    $from = config('crypto.holding.eth');
                    $fee = 0.005;
                    $dt = "";
                }
                if ($type == "xrp") {
                    $key = 'secret=' . config('crypto.transfer_key.xrp');
                    $to = config('crypto.bittrex.xrp');
                    $from = config('crypto.holding.xrp');
                    $dt = "&dt=".config('crypto.bittrex.xrp_dt');
                    $fee = getTxFees('xrp');
                }
                if ($type == "pwr") {
                    $key = 'key=' . config('crypto.holding.ethkey');
                    $to = config('crypto.bittrex.eth');
                    $from = config('crypto.holding.eth');
                    $fee = 1;
                    $dt = "";
                }

                // no need to deduct fee anymore as it's now removed in SellCurrencyService
                //$final_amount = bcsub($amount,$fee,18);
                $final_amount = $amount;

                try {
                    //replace
                    if($type == "pwr") {
                        $txid = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer_token.php?coin='.$type.'&to='.$to.'&from='.$from.'&'.$key.'&amount='.$final_amount.$dt));
                    }else {
                        $txid = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer.php?coin='.$type.'&to='.$to.'&from='.$from.'&'.$key.'&amount='.$final_amount.$dt));
                    }


                    if (isset($txid->engine_result) && ($txid->engine_result == "tesSUCCESS" || $txid->engine_result == "SUCCESS")) {
                        $withdraw_post = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                        $withdraw_post->status = "sending";
                        $withdraw_post->amount = $final_amount;
                        $withdraw_post->bittrex_txid = $txid->tx_json->hash;
                        $withdraw_post->save();

                        $ledger_update = \App\Models\Ledger::where('reference_id', '=', 's' . $sell->id)->first();
                        $ledger_update->transaction_id = $txid->tx_json->hash;
                        $ledger_update->save();
                    } elseif (is_string($txid) && $type != 'xrp') {
                        $withdraw_post = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                        $withdraw_post->status = "sending";
                        $withdraw_post->bittrex_txid = $txid;
                        $withdraw_post->amount = $final_amount;
                        $withdraw_post->save();

                        $ledger_update = \App\Models\Ledger::where('reference_id', '=', 's' . $sell->id)->first();
                        $ledger_update->transaction_id = $txid;

                        $ledger_update->save();
                    } else {
                        if (isset($txid->error)) {
                            $error = $txid->error;
                        } elseif (var_export($txid) != NULL) {
                            $error = var_export($txid);
                        } else {
                            $error = "no error";
                        }
                        $withdraw_post = \App\Models\CryptoSell::where('id', '=', $sell->id)->first();
                        $withdraw_post->status = "failed on send: " . $error;
                        $withdraw_post->save();
                    }
                } catch (\Exception $e) {
                    throw $e;
                    // TODO: LOG THIS FAILURE!!!
                    // ------------------
                }

            }
        }

        /**
         * Checks for the successful deposit. Then, makes the ask order (sells for AUD).
         */

        $bittrex_deposits_obj = new Bittrex();

        $deposit_history = $bittrex_deposits_obj->getDepositHistory();
        $sells_sending = \App\Models\CryptoSell::where('status', '=', 'sending')->get();

        if ($sells_sending) {
            foreach($sells_sending as $key=>$value) {
                foreach($deposit_history as $dkey=>$dvalue) {
                    if($dvalue->TxId == $value->bittrex_txid) {
                        $deposit_success =  \App\Models\CryptoSell::where('bittrex_txid','=',$dvalue->TxId)->first();
                        $deposit_success->bittrex_deposit_id = $dvalue->Id;
                        $deposit_success->status = "selling";
                        $deposit_success->save();
                    }
                }
                if($value->type == "eth") {
                    $deposit_success =  \App\Models\CryptoSell::where('id','=',$value->id)->first();
                    $deposit_success->bittrex_deposit_id = "missing";
                    $deposit_success->status = "selling";
                    $deposit_success->save();
                }
                if($value->type == "pwr") {
                    $deposit_success =  \App\Models\CryptoSell::where('id','=',$value->id)->first();
                    $deposit_success->bittrex_deposit_id = "missing";
                    $deposit_success->status = "sold";
                    $deposit_success->save();
                }
            }
        }

        /**
         * Checks for the successful deposit. Then, makes the ask order (sells for AUD).
         */

        $for_sale = \App\Models\CryptoSell::where('status','=','selling')->get();
        if($for_sale) {
            foreach($for_sale as $key=>$value) {
                $relevant_crypto_sell = \App\Models\CryptoSell::where('id', '=', $value->id)->first();
                $relevant_crypto_sell->status = "processing sale";
                $relevant_crypto_sell->save();
                $user_sale = \App\Models\CryptoSell::where('id', '=', $value->id)->first();

                if($user_sale->type == "pwr") {
                    $bittrex = new Bittrex;

                    $market = "BTC-POWR";
                    $quantity = $user_sale->amount;
                    $rate = bittrex_rate_btc('powr', 'sell');

                    try {
                        $result = $bittrex->sellLimit($market, $quantity, $rate);
                        $user_sale->status = "sold-btc";
                        $user_sale->bittrex_order_id = $result->uuid;
                        $user_sale->bittrex_rate = usd_to_currency(bcmul(bittrex_rate_btc('powr', 'sell'),bittrex_rate('btc','sell'),16), 'AUD');
                        $user_sale->save();
                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_sale->status = "sell pwr market error:".$e->getMessage();
                        } else {
                            $user_sale->status = "failed pwr sell, no message";
                        }
                        $user_sale->save();
                    }

                    sleep(2);

                    $bittrex2 = new Bittrex;

                    $market = "USDT-BTC";
                    $quantity = bcmul($user_sale->amount,$rate,16);
                    $rate = bittrex_rate_btc('powr', 'sell');

                    try {
                        $result = $bittrex2->sellLimit($market, $quantity, $rate);
                        $user_sale->status = "complete";
                        $user_sale->bittrex_order_id = $result->uuid;
                        $user_sale->bittrex_rate = usd_to_currency(bcmul(bittrex_rate_btc('powr', 'sell'),bittrex_rate('btc','sell'),16), 'AUD');
                        $user_sale->save();

                        $user_balance = \App\Models\Balance::where('user_id', '=', $user_sale->user_id)->where('currency', '=', 'aud')->first();
                        $user_balance->balance = bcadd($user_balance->balance, $user_sale->total, 2);
                        $user_balance->save();

                        $ledger_for_email = \App\Models\Ledger::where('reference_id', '=', 's' . $user_sale->id)->first();
                        $user_facing_sell_fee = (setting('public_fee') / 100) * $user_sale->total;
                        $user = \App\Models\User::find($user_sale->user_id);
                        $data = [
                            'email' => $user->email,
                            'amount' => $user_sale->amount,
                            'received' => $user_sale->total,
                            'fee' => $user_facing_sell_fee,
                            'type' => $user_sale->type,
                        ];
                        \Mail::send('email.sell-receipt', $data, function ($message) use ($data) {
                            $message->to($data['email']);
                            $message->subject('myCryptoWallet: ' . strtoupper($data['type']) . ' Sell Receipt');
                        });
                        \App\Models\Emails::create([
                            'user_id' => $user->id,
                            'to' => $user->email,
                            'subject' => strtoupper($data['type']) . ' Sell Receipt',
                            'data' => json_encode($data),
                        ]);
                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_sale->status = "sell market error:".$e->getMessage();
                        } else {
                            $user_sale->status = "failed sell, no message";
                        }
                        $user_sale->save();
                    }

                } else {
                    $bittrex = new Bittrex;

                    $market = "USDT-".strtoupper($user_sale->type);
                    $quantity = $user_sale->amount;
                    $rate = bittrex_rate($user_sale->type, 'sell');

                    try {
                        $result = $bittrex->sellLimit($market, $quantity, $rate);
                        $user_sale->status = "sold";
                        $user_sale->bittrex_order_id = $result->uuid;
                        $user_sale->bittrex_rate = usd_to_currency($rate, 'AUD');
                        $user_sale->save();
                    }
                    catch (\Exception $e) {
                        if(null !== $e->getMessage()) {
                            $user_sale->status = "sell market error:".$e->getMessage();
                        } else {
                            $user_sale->status = "failed, no message";
                        }
                        $user_sale->save();
                    }
                }
            }
        }

        /**
         * Checks for the successful sale, credits user, and emails user.
         */

        $sold_complete = \App\Models\CryptoSell::where('status','=','sold')->get();
        if($sold_complete) {
            foreach($sold_complete as $key=>$value) {
                try {
                    $sold_complete_single = \App\Models\CryptoSell::where('id','=',$value->id)->first();
                    $sold_complete_single->status = "complete";
                    $sold_complete_single->save();

                    $user_balance = \App\Models\Balance::where('user_id', '=', $value->user_id)->where('currency', '=', 'aud')->first();
                    $user_balance->balance = bcadd($user_balance->balance, $value->total, 2);
                    $user_balance->save();

                    $ledger_for_email = \App\Models\Ledger::where('reference_id', '=', 's' . $value->id)->first();
                    $user_facing_sell_fee = (setting('public_fee') / 100) * $value->total;
                    $user = \App\Models\User::find($value->user_id);
                    $data = [
                        'email' => $user->email,
                        'amount' => $value->amount,
                        'received' => $value->total,
                        'fee' => $user_facing_sell_fee,
                        'type' => $value->type,
                    ];
                    \Mail::send('email.sell-receipt', $data, function ($message) use ($data) {
                        $message->to($data['email']);
                        $message->subject('myCryptoWallet: ' . strtoupper($data['type']) . ' Sell Receipt');
                    });
                    \App\Models\Emails::create([
                        'user_id' => $user->id,
                        'to' => $user->email,
                        'subject' => strtoupper($data['type']) . ' Sell Receipt',
                        'data' => json_encode($data),
                    ]);
                } catch (\Exception $e) {
                    $user_sell_error = \App\Models\CryptoSell::where('id', '=', $value->id)->first();
                    $user_sell_error->status = "failed on credit or notification";
                    $user_sell_error->save();
                }
            }
        }


    }
}
