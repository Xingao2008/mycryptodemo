<?php

namespace App\Console\Commands;

use App\Models\Firewall;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FirewallExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firewall:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purges blacklist entries that have since expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Purging expired blacklisted entries');
        $firewall = Firewall::where('expires', '>=', Carbon::now())->get();

        foreach($firewall as $fw) {
            $fw->delete();
        }
    }
}
