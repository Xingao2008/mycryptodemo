<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\UsdExchange;

class SyncUsdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:SyncUsdCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the latest fiat currencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $params = [
            //'access_key' => 'd0b5758811e9d87d4c9d63932838d3c6', //this is Aaron key
            'access_key' => '5a7bad509abee2afe1a18829e885891d', //Gex's key
            'format' => 1,
            'source' => 'USD',
            'currencies' => 'AUD,NZD,CAD,GBP,EUR'
        ];
        $uri = 'http://www.apilayer.net/api/live?' . http_build_query($params);

        $exchange = json_decode(file_get_contents($uri));

        if ($exchange->success != true) {
            return;
        }

        $source = $exchange->source;

        foreach ($exchange->quotes as $currency => $value) {
            $currency = str_replace($source, '', $currency);
            // $this->info("{$source} -> {$currency}: $value");
            UsdExchange::create([
                'source' => $source,
                'currency' => $currency,
                'value' => $value,
            ]);
        }
    }
}
