<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Exception;
use App\Bittrex\Bittrex;

class WithdrawMakerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:WithdrawMakerCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes withdrawals into users external accounts after confirmation.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (setting('withdraws_active')) {
            $withdrawals = \App\Models\CryptoWithdraw::where('status', '=', "ready")->get();

            foreach ($withdrawals as $key => $withdraw) {
                // let's update this record and save it before continuing so we don't double-withdraw
                $withdraw_pre = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                $withdraw_pre->status = "processing";
                $withdraw_pre->save();

                $to = $withdraw->to;
                $from = $withdraw->from;
                $type = $withdraw->type;
                $amount = $withdraw->amount;

                // changed due to double-up of fee taking
                $fee = 0;


                if ($type == "btc") {
                    $key = 'key=' . config('crypto.transfer_key.btc');
                    $holdingwallet = config('crypto.holding.btc');
                }
                if ($type == "ltc") {
                    $key = 'key=' . config('crypto.transfer_key.ltc');
                    $holdingwallet = config('crypto.holding.ltc');
                }
                if ($type == "eth") {
                    $key = 'key=' . config('crypto.holding.ethkey');
                    $holdingwallet = config('crypto.holding.eth');
                }
                if ($type == "xrp") {
                    $key = 'secret=' . config('crypto.transfer_key.xrp');
                    $holdingwallet = config('crypto.holding.xrp');
                }


                $final_amount = bcsub($amount, $fee, 18);

                try {
                    if($type == "pwr") {

                        $bittrex = new Bittrex;

                        try {
                            $bittrex_fee_inc = bcadd($amount,5,16);
                            $result = $bittrex->withdraw('powr', $bittrex_fee_inc, $to);

                            //warn Jaryd if USDT Balance lower than certain amount
                            sendUsdtWarning();

                            $withdraw_post = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                            $withdraw_post->status = "complete";
                            $withdraw_post->transaction_id = "internal_txid".$result->uuid;
                            $withdraw_post->save();

                            $ledger_update = \App\Models\Ledger::where('reference_id', '=', 'w' . $withdraw->id)->first();
                            $ledger_update->address_from = "bittrex";
                            $ledger_update->transaction_id = "internal_txid:".$result->uuid;
                            $ledger_update->save();

                            $user = \App\Models\User::find($ledger_update->user_id);
                            if ($user) {
                                $data = [
                                    'email' => $user->email,
                                    'amount' => $withdraw_post->amount,
                                    'type' => $ledger_update->currency,
                                ];
                                \Mail::send('email.crypto-withdrawal-complete', $data, function ($message) use ($data) {
                                    $message->to($data['email']);
                                    $message->subject('myCryptoWallet: ' . strtoupper($data['type']) . ' Withdrawal Complete!');
                                });
                                \App\Models\Emails::create([
                                    'user_id' => $user->id,
                                    'to' => $user->email,
                                    'subject' => strtoupper($data['type']) . ' Withdrawal Complete',
                                    'data' => json_encode($data),
                                ]);
                            }

                        }
                        catch (\Exception $e) {
                            $withdraw_post1 = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                            if(null !== $e->getMessage()) {
                                $withdraw_post1->status = "withdraw error:".$e->getMessage();
                            } else {
                                $withdraw_post1->status = "failed, no message";
                            }
                            $withdraw_post1->save();
                        }
                    } else {
                        $txid = json_decode(file_get_contents('https://cd.fairdigital.com.au/api/transfer.php?coin=' . $type . '&to=' . $to . '&from=' . $holdingwallet . '&' . $key . '&amount=' . $final_amount));

                        if (isset($txid->engine_result) && ($txid->engine_result == "tesSUCCESS" || $txid->engine_result == "SUCCESS")) {
                            $withdraw_post = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                            $withdraw_post->transaction_id = $txid->tx_json->hash;
                            $withdraw_post->status = "sent";
                            $withdraw_post->save();

                            $ledger_update = \App\Models\Ledger::where('reference_id', '=', 'w' . $withdraw->id)->first();
                            $ledger_update->address_from = $from;
                            $ledger_update->transaction_id = $txid->tx_json->hash;
                            $ledger_update->save();

                            $user = \App\Models\User::find($ledger_update->user_id);
                            if ($user) {
                                $data = [
                                    'email' => $user->email,
                                    'amount' => $withdraw_post->amount,
                                    'type' => $ledger_update->currency,
                                ];
                                \Mail::send('email.crypto-withdrawal-complete', $data, function ($message) use ($data) {
                                    $message->to($data['email']);
                                    $message->subject('myCryptoWallet: ' . strtoupper($data['type']) . ' Withdrawal Complete!');
                                });
                                \App\Models\Emails::create([
                                    'user_id' => $user->id,
                                    'to' => $user->email,
                                    'subject' => strtoupper($data['type']) . ' Withdrawal Complete',
                                    'data' => json_encode($data),
                                ]);
                            }

                        } elseif (is_string($txid) && $type != 'xrp') {
                            $withdraw_post = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                            $withdraw_post->transaction_id = $txid;
                            $withdraw_post->status = "sent";
                            $withdraw_post->save();

                            $ledger_update = \App\Models\Ledger::where('reference_id', '=', 'w' . $withdraw->id)->first();
                            $ledger_update->address_from = $from;
                            $ledger_update->transaction_id = $txid;
                            $ledger_update->save();

                            $user = \App\Models\User::find($ledger_update->user_to);
                            if ($user) {
                                $data = [
                                    'email' => $user->email,
                                    'amount' => $withdraw_post->amount,
                                    'type' => $withdraw_post->type,
                                ];
                                \Mail::send('email.crypto-withdrawal-complete', $data, function ($message) use ($data) {
                                    $message->to($data['email']);
                                    $message->subject('myCryptoWallet: ' . strtoupper($data['type']) . ' Withdrawal Complete!');
                                });
                                \App\Models\Emails::create([
                                    'user_id' => $user->id,
                                    'to' => $user->email,
                                    'subject' => strtoupper($data['type']) . ' Withdrawal Complete',
                                    'data' => json_encode($data),
                                ]);
                            }

                        } else {
                            if (isset($txid->error)) {
                                $error = $txid->error;
                            } elseif (var_export($txid) != NULL) {
                                $error = var_export($txid);
                            } elseif ($txid->engine_result == "tecNO_DST_INSUF_XRP") {
                                $error = "Recipient address not created, thus too little XRP was sent to it to create it. XRP tx fee was charged.";
                            } else {
                                $error = "no error";
                            }
                            $withdraw_post = \App\Models\CryptoWithdraw::where('id', '=', $withdraw->id)->first();
                            $withdraw_post->transaction_id = $error;
                            $withdraw_post->status = "failed";
                            $withdraw_post->save();
                        }
                    }
                } catch (\Exception $e) {
                    throw $e;
                    // TODO: LOG THIS FAILURE!!!
                    // ------------------
                }

            }
        }

    }
}
