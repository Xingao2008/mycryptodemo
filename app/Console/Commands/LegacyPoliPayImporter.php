<?php

namespace App\Console\Commands;

use App\Helpers\PoliPayApi;
use App\Models\PoliPayTransaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LegacyPoliPayImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'polipay:legacy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pp = new PoliPayApi();

        $transactions = collect();

        for($i=0; $i<20; $i++) {
            foreach($pp->getDailyTransactions( Carbon::now()->subDay($i)->toDateString() ) as $t) {
                $transactions->push($t);
            }
        }

        foreach($transactions as $transaction) {
            $poli = PoliPayTransaction::firstOrNew(['transaction_ref_no' => $transaction->TransactionRefNo]);
            $poli->user_id = $poli->user_id ?? 0;
            $poli->save();
            $transaction->MerchantReference = $poli->id;

            PoliPayTransaction::updateTransaction($transaction);
        }
    }
}
