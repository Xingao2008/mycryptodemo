<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\ExternalPrices;
use App\Bittrex\Bittrex;

class PriceUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:PriceUpdateCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the latest prices via BitFinex';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = 10000;

        $bittrex = new Bittrex;

        $orderbook['btc'] = $bittrex->getOrderBook('USDT-BTC', 'both');
        $orderbook['ltc'] = $bittrex->getOrderBook('USDT-LTC', 'both');
        $orderbook['eth'] = $bittrex->getOrderBook('USDT-ETH', 'both');
        $orderbook['xrp'] = $bittrex->getOrderBook('USDT-XRP', 'both');
        //$orderbook['pwr'] = $bittrex->getOrderBook('BTC-POWR', 'both');

        $btc['ask'] = usd_to_currency(bittrex_volume_capacity($orderbook['btc'], 'sell', $amount), 'AUD');
        $btc['bid'] = usd_to_currency(bittrex_volume_capacity($orderbook['btc'], 'buy', $amount), 'AUD');

        $ltc['ask'] = usd_to_currency(bittrex_volume_capacity($orderbook['ltc'], 'sell', $amount), 'AUD');
        $ltc['bid'] = usd_to_currency(bittrex_volume_capacity($orderbook['ltc'], 'buy', $amount), 'AUD');

        $eth['ask'] = usd_to_currency(bittrex_volume_capacity($orderbook['eth'], 'sell', $amount), 'AUD');
        $eth['bid'] = usd_to_currency(bittrex_volume_capacity($orderbook['eth'], 'buy', $amount), 'AUD');

        $xrp['ask'] = usd_to_currency(bittrex_volume_capacity($orderbook['xrp'], 'sell', $amount), 'AUD');
        $xrp['bid'] = usd_to_currency(bittrex_volume_capacity($orderbook['xrp'], 'buy', $amount), 'AUD');

        $pwr['bid'] = usd_to_currency(bcmul(bittrex_rate_btc_pwr('powr', 'sell'),bittrex_rate_pwr('btc','sell'),16), 'AUD');
        $pwr['ask'] = usd_to_currency(bcmul(bittrex_rate_btc_pwr('powr', 'buy'),bittrex_rate_pwr('btc','buy'),16), 'AUD');



        ExternalPrices::create([
            'currency' => 'btc',
            'bid' => $btc['bid'],
            'ask' => $btc['ask'],
            'source' => 'bittrex',
        ]);

        ExternalPrices::create([
            'currency' => 'ltc',
            'bid' => $ltc['bid'],
            'ask' => $ltc['ask'],
            'source' => 'bittrex',
        ]);

        ExternalPrices::create([
            'currency' => 'eth',
            'bid' => $eth['bid'],
            'ask' => $eth['ask'],
            'source' => 'bittrex',
        ]);

        ExternalPrices::create([
            'currency' => 'xrp',
            'bid' => $xrp['bid'],
            'ask' => $xrp['ask'],
            'source' => 'bittrex',
        ]);

        ExternalPrices::create([
            'currency' => 'pwr',
            'bid' => $pwr['bid'],
            'ask' => $pwr['ask'],
            'source' => 'bittrex',
        ]);
    }
}
