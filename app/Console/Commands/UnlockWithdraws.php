<?php

namespace App\Console\Commands;

use App\Models\TfaResets;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UnlockWithdraws extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraws:unlock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::whereLockWithdraw(1)->where('lock_withdraw_time', '<', Carbon::now()->toDateTimeString())->update(['lock_withdraw' => 0, 'lock_withdraw_time' => null]);

        TfaResets::where('created_at', '>', Carbon::now()->subHours(1))->update(['auth_code' => '', 'active' => 0]);
    }
}
