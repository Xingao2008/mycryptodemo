<?php

namespace App\Console\Commands;

use App\Models\Firewall;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CloudFlareSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloudflare:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tmp_fw = new Firewall();
        $table_name = $tmp_fw->getTable();
        unset($tmp_fw);

        $key = new \Cloudflare\API\Auth\APIKey(config('cloudflare.email'), config('cloudflare.api_key'));
        $adapter = new \Cloudflare\API\Adapter\Guzzle($key);
        $rules = new \Cloudflare\API\Endpoints\AccessRules($adapter);

        //$list = collect();

        $pages = 2;
        for ($x = 1; $x <= $pages; $x++) {
            $entries = $rules->listRules(config('cloudflare.zone_id'), '', '', '', '', $x);
            $pages = $entries->result_info->total_pages;
            foreach ($entries->result as $entry) {

                $fw = \DB::table($table_name)->where('cloudflare_id', $entry->id)->first();

                $data = [
                    'ip_address' => $entry->configuration->value,
                    'whitelist' => $entry->mode == 'whitelist' ? true : false,
                    'cloudflare_id' => $entry->id,
                    'expires' => $fw->expires ?? null,
                    'reason' => $entry->notes,
                    'created_at' => Carbon::parse($entry->created_on)->toDateTimeString(),
                    'updated_at' => Carbon::now(),
                ];

                if(!$fw) {
                    \DB::table($table_name)->insert($data);
                }
            }
        }

        //\DB::table($table_name)->truncate();
        //foreach($list as $d) {
        //    \DB::table($table_name)->insert($d);
        //}
    }
}
