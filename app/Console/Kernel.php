<?php

namespace App\Console;

use App\Console\Commands\BuySellCommand;
use App\Console\Commands\UsdtRechargeWarning;
use App\Console\Commands\CloudFlareSync;
use App\Console\Commands\CreatePermissions;
use App\Console\Commands\DepositMakerCommand;
use App\Console\Commands\FirewallExpiry;
use App\Console\Commands\LegacyPoliPayImporter;
use App\Console\Commands\PriceUpdateCommand;
use App\Console\Commands\TransactionsUpdateCommand;
use App\Console\Commands\UnlockWithdraws;
use App\Console\Commands\UpdatePoliPayTransactions;
use App\Console\Commands\VolumeUpdateCommand;
use App\Console\Commands\WithdrawMakerCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\SyncUsdCommand;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\PriceUpdateCommand',
        'App\Console\Commands\TransactionsUpdateCommand',
        'App\Console\Commands\DepositMakerCommand',
        'App\Console\Commands\WithdrawMakerCommand',
//        'App\Console\Commands\BuySellCommand',
        //'App\Console\Commands\VolumeUpdateCommand',
        'App\Console\Commands\SyncUsdCommand',
        FirewallExpiry::class,
        UpdatePoliPayTransactions::class,
        LegacyPoliPayImporter::class,
        CreatePermissions::class,
        CloudFlareSync::class,
        UnlockWithdraws::class,
        UsdtRechargeWarning::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        if (\App::environment('production')) {
            $schedule->command(UpdatePoliPayTransactions::class)->hourly();
            $schedule->command(SyncUsdCommand::class)->hourly();
            $schedule->command(UnlockWithdraws::class)->everyFifteenMinutes();
            $schedule->command(UsdtRechargeWarning::class)->everyFifteenMinutes();
            $schedule->command(PriceUpdateCommand::class)->everyMinute();
            $schedule->command(TransactionsUpdateCommand::class)->everyMinute();
            $schedule->command(DepositMakerCommand::class)->everyMinute();
            $schedule->command(WithdrawMakerCommand::class)->everyMinute();
    //        $schedule->command(BuySellCommand::class)->everyMinute();
    //        $schedule->command(VolumeUpdateCommand::class)->hourly();
            $schedule->command(FirewallExpiry::class)->everyFifteenMinutes(); // Purge old blacklist entries every 15mins
            $schedule->command(CloudFlareSync::class)->hourly(); // Update DB from CloudFlare
        } else {
            $schedule->command(UnlockWithdraws::class)->daily();
            $schedule->command(UsdtRechargeWarning::class)->daily();
            $schedule->command(TransactionsUpdateCommand::class)->daily();
            $schedule->command(DepositMakerCommand::class)->daily();
            $schedule->command(WithdrawMakerCommand::class)->daily();
            $schedule->command(FirewallExpiry::class)->hourly(); 
            $schedule->command(CloudFlareSync::class)->daily();
        }
        
        
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
