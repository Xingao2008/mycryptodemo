<?php

namespace App\Providers;


use App\Models\Balance;
use App\Models\ExternalPrices;
use App\Models\Firewall;
use App\Models\Ledger;
use App\Models\Setting;
use App\Models\User;
use App\Observers\BalanceObserver;
use App\Observers\FirewallObserver;
use App\Observers\LedgerObserver;
use App\Observers\SettingsObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        view()->composer('partials.mywallet', function (\Illuminate\View\View $view) {
            $view->with('price', ExternalPrices::price());
            $view->with('price_24', ExternalPrices::price24());
            $view->with('bccomp_result', ExternalPrices::bccomp_results());
            $view->with('price_change', ExternalPrices::price_changes());
            $view->with('funds', Balance::funds());
        });

        view()->composer('layouts.app', function (\Illuminate\View\View $view) {
            $view->with('price', ExternalPrices::price());
        });

        view()->composer('layouts.private', function (\Illuminate\View\View $view) {
            $view->with('price', ExternalPrices::price());
        });

        Setting::observe(SettingsObserver::class);
        Balance::observe(BalanceObserver::class);
        User::observe(UserObserver::class);
        Firewall::observe(FirewallObserver::class);
        Ledger::observe(LedgerObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*
        if ($this->app->environment() !== 'production') {
            //\DB::listen(function($query) {
            //    \Log::info($query->sql, $query->bindings);
            //});
        }
        */
    }
}
