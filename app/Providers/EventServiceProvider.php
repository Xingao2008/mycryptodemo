<?php

namespace App\Providers;

use App\Events\LoginBlacklisted;
use App\Listeners\LoginBlacklistedEmailAdmin;
use App\Listeners\PasswordResetLockWithdraw;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
//        'Illuminate\Auth\Events\Registered' => [
//            'App\Listeners\UserRegistered',
//        ],
        LoginBlacklisted::class => [
            LoginBlacklistedEmailAdmin::class
        ],
        PasswordReset::class => [
            PasswordResetLockWithdraw::class
        ],
        'App\Events\UserRegistered' => [
            'App\Listeners\UserRegistered',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('mailer.sending', function($message)
        {
            dd($message);
        });
    }
}
