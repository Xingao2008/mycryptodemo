<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;
use Spatie\Menu\Laravel\Url;
use Illuminate\Contracts\Auth\Access\Gate;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Menu::macro('adminlteSubmenu', function ($submenuName) {
            return Menu::new()->prepend('<a href="#"><span> ' . $submenuName . '</span> <i class="fa fa-angle-left pull-right"></i></a>')
                ->addParentClass('treeview')->addClass('treeview-menu');
        });
        Menu::macro('adminlteMenu', function () {
            return Menu::new()
                ->addClass('sidebar-menu');
        });
        Menu::macro('adminlteSeparator', function ($title) {
            return Html::raw($title)->addParentClass('header');
        });
        Menu::macro('adminlteDefaultMenu', function ($content) {
            return Html::raw('<i class="fa fa-link"></i><span>' . $content . '</span>')->html();
        });

        \Menu::macro('main', function () {
            return Menu::new()
                ->addClass('sidebar-menu')
                ->add(Html::raw('MENU')->addParentClass('header'))
                ->url('admin/dashboard', '<i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard')
                ->url('dashboard', '<i class="fa fa-sitemap" aria-hidden="true"></i> Public Site')
                ->urlIfCan('admin.balances.index', '/admin/balances', '<i class="fa fa-money" aria-hidden="true"></i> Balances')
                ->urlIfCan('admin.deposits.index', '/admin/funds-approval', '<i class="fa fa-sign-in" aria-hidden="true"></i> Deposits Approval')
                ->urlIfCan('admin.withdrawals.index', '/admin/withdrawals-approval', '<i class="fa fa-external-link" aria-hidden="true"></i> Withdrawals Approval')
                ->urlIfCan('admin.ledgers.index', '/admin/ledgers', '<i class="fa fa-book" aria-hidden="true"></i> Ledgers')
                ->urlIfCan('admin.emails.index', '/admin/emails', '<i class="fa fa-envelope-o" aria-hidden="true"></i> Emails')
                ->urlIfCan('admin.failedtransactions.index', '/admin/failed-transactions', '<i class="fa fa-ban" aria-hidden="true"></i> Failed Transactions')
                ->urlIfCan('admin.firewall.index', '/admin/firewall', '<i class="fa fa-fire" aria-hidden="true"></i> Firewall')
                ->addIfCan('admin.orders.index',
                    Menu::adminlteSubmenu('<i class="fa fa-exchange" aria-hidden="true"></i> Orders')
                        ->url('admin/orders/buys', 'Buys')
                        ->url('admin/orders/sells', 'Sells')
                )
                ->urlIfCan('admin.news.index', '/admin/news', '<i class="fa fa-newspaper-o" aria-hidden="true"></i> News')
                ->addIfCan('admin.polipay.index',
                    Menu::adminlteSubmenu('<i class="fa fa-usd" aria-hidden="true"></i> POLi Pay')
                        ->url('admin/polipay', 'POLi Pay Transactions')
                        ->url('admin/polipay-deposit', 'POLi Pay Deposit')
                )
                ->urlIfCan('admin.settings.index', '/admin/settings', '<i class="fa fa-cog" aria-hidden="true"></i> Settings')
                ->addIfCan('admin.roles.index', Link::to('/admin/roles', '<i class="fa fa-users" aria-hidden="true"></i> Roles'))
                ->addIfCan('admin.permissions.index', Link::to('/admin/permissions', '<i class="fa fa-lock" aria-hidden="true"></i> Permissions'))
                // ->urlIfCan('admin.selfie.index', '/admin/selfies', '<i class="fa fa-id-badge" aria-hidden="true"></i> Selfies')
                ->urlIfCan('admin.users.index', '/admin/tickets/create', '<i class="fa fa-ticket" aria-hidden="true"></i> Create Ticket')
                ->urlIfCan('admin.users.index', '/admin/users', '<i class="fa fa-user" aria-hidden="true"></i> Users')
                ->urlIfCan('admin.users.blocked', '/admin/users_blocked', '<i class="fa fa-ban" aria-hidden="true"></i> Block Users')
                ->urlIfCan('admin.userlimits.index', '/admin/user_limits', '<i class="fa fa-user-times" aria-hidden="true"></i> User Limits')
                ->urlIfCan('admin.verifications.index', '/admin/verifications', '<i class="fa fa-check" aria-hidden="true"></i> Verifications')
                ->addIfCan('admin.exchange.index',
                    Menu::adminlteSubmenu('<i class="fa fa-exchange" aria-hidden="true"></i> Exchanges')
                        ->url('admin/exchange/fees', 'Fees')
                        ->url('admin/exchange/orders', 'Orders')
                        ->url('admin/exchange/fees/convert', 'Cashout')
                        ->url('admin/exchange/fees/convert/report', 'Cashout Report')
                        ->url('admin/exchange/pause', 'Pause')
                )
                /*
                ->addIf(
                    \Auth::user()->hasPermissionTo('admin..index'),
                    Link::to()
                )
                */

                ->setActiveFromUrl(url()->to(request()->segment(1) . '/' . request()->segment(2)))
                ->setActiveClass('active');

        });

    }
}
