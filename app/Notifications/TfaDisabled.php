<?php

namespace App\Notifications;

use App\Models\TfaResets;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class TfaDisabled extends Notification
{
    use Queueable;
    /**
     * @var TfaResets
     */
    private $tfaResets;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TfaResets $tfaResets)
    {
        //
        $this->tfaResets = $tfaResets;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', TwilioChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Hi there,')
            ->line('We are now processing your request to disable 2FA. You will need to visit link contained in the SMS to confirm disabling your 2FA.')
            ->line('If this was in error or you didn\'t authorise this, contact support immediately.')
            ->line('Kind regards,')
            ->line('myCryptoWallet Support');
    }

    public function toTwilio($notifiable)
    {
        return (new TwilioSmsMessage())
            ->content("We have received your request to disable 2FA on your myCryptoWallet account. Please visit the link below to finalise this. If you haven't requested this, do not tap the link and contact support immediately. " . $this->tfaResets->url());
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
