<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;

class CheckForMaintenanceMode
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance() && !$this->isBackendRequest($request)) {
            $data = json_decode(file_get_contents($this->app->storagePath() . '/framework/down'), true);

            throw new MaintenanceModeException($data['time'], $data['retry'], $data['message']);
        }

        return $next($request);
    }

    private function isBackendRequest($request)
    {
        return (
            $request->is('admin/*') or
            $request->is('/') or
            $request->is('about/marketplace') or
            $request->is('about/buysell') or
            $request->is('faq') or
            $request->is('about') or
            $request->is('news') or
            $request->is('contact') or
            $request->is('logout') or
            $request->is('login')
        );
    }
}
