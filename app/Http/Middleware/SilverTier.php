<?php

namespace App\Http\Middleware;

use App\Models\Verification;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;


class SilverTier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
        $v = Verification::where('user_id', '=', $user->id)->first();

        if($v->account == 'approved' && $v->id_passport == 'approved' && $v->id_drivers == 'approved' &&
            $v->id_bank == 'approved') {
            $tier = 'silver';
        } else {
            Flash::message('Unfortunately you are not approved to access this section of the website. Please ensure you have complete the Minimum verification, wait for admin approval and check any admin notes on the <a href="/verify">verification</a> page.');
            return redirect ('/dashboard');
        }


        // if ($user && $user->status == 'blocked') {
        //     Auth::logout();

        //     Flash::message('Your account is currently unavailable. Please contact us for more information.');

        //     return redirect('/login');
        // }
        return $next($request);
    }
}
