<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        // User is active
        if ($user && $user->status == 'active') {
            return $next($request);
        }

        // User is pending
        if ($user && $user->status == 'pending') {
            $email = $user->email;

            if ($request->wantsJson() || $request->isXmlHttpRequest()) {
                throw new \Exception('Please verify your email.');
            }

            Auth::logout();

            Flash::message('Thanks for signing up! Please check your email for a verification code. <a href="/register/resend/'.$email.'">Resend?</a>');

            return redirect('/login');
        }

        // User is blocked
        if ($user && $user->status == 'blocked') {
            $errorMessage = 'Your account is currently unavailable. Please contact us for more information.';

            if ($request->wantsJson() || $request->isXmlHttpRequest()) {
                throw new \Exception($errorMessage);
            }

            Auth::logout();

            Flash::message($errorMessage);

            return redirect('/login');
        }
    }
}
