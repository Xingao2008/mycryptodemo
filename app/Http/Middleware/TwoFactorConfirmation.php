<?php

namespace App\Http\Middleware;

use Closure;

class TwoFactorConfirmation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('2fa.confirmation')) {
            return $next($request);
        }

        session()->put('2fa.intended', $request->fullUrl());

        if(!\Auth::user()->tfa_status) {
            flash('Please enable TFA before proceeding')->error();
            return redirect()->to('admin');
        }

        return redirect()->to('admin/tfa/auth');
    }
}
