<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BalanceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'currency' => $this->currency,
            'balance' => $this->balance,
        ];
    }
}
