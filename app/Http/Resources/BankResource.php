<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BankResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'           => $this->bank_name,
            'branch'         => $this->branch_name,
            'bsb'            => $this->bank_bsb,
            'account_number' => $this->bank_number,
        ];
    }
}
