<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'street'    => $this->street,
            'city'      => $this->city,
            'state'     => $this->state,
            'post_code' => $this->post_code,
            'country'   => $this->country->name,
        ];
    }
}
