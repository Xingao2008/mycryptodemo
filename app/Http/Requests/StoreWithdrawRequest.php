<?php

namespace App\Http\Requests;

use App\Rules\Btc;
use App\Rules\Eth;
use App\Rules\Ltc;
use App\Rules\Xrp;
use Illuminate\Foundation\Http\FormRequest;
use config;

class StoreWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if(in_array(request()->segment(3), config('currencies.fiatCurrencies'))) {
            return [
                'external' => 'sometimes',
                'amount' => 'required|numeric',
            ];
        } else {

            $rule = '';

            switch(request()->segment(3)) {
                case 'btc':
                    $rule = new Btc();
                    break;
                case 'ltc':
                    $rule = new Ltc;
                    break;
                case 'eth':
                    $rule = new Eth;
                    break;
                case 'xrp':
                    $rule = new Xrp;
                    break;
            }

            return [
                'external' => [
                    'required',
                    'string',
                    $rule
                ],
                'amount' => 'required|numeric',
            ];
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'external.min' => 'That doesn\'t appear to be a valid address - insufficient characters.',
            'external.required'  => 'An external wallet address is required.',
            'external.alpha_num' => 'Need a valid address.',
        ];
    }
}
