<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cookie;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class ReferController extends Controller
{
    public function index()
    {
      $user = \Auth::user();
      $referrals = \App\Models\User::where('refer_uid', '=', $user->id)->get();

      if ($referrals) {
        foreach($referrals as $referral) {

          //foreach($referrals as $referee) {
          $buysell = ['transaction_type' => 'buy','transaction_type' =>'sell'];
          $transactions = \App\Models\Ledger::where($buysell)->where('user_to','=',$referral->id)->orderBy('created_at','desc')->get();

          $fees[$referral->id]['all_fees'] = 0;
          if($transactions) {
            foreach($transactions as $transaction) {
              $fees[$referral->id]['all_fees'] += $transaction->fee;
            }
          }

          $past_date = Carbon::now()->startOfMonth()->subDay()->endOfDay();
          $fees[$referral->id]['past_month_fees'] = 0;
          if($transactions) {
            foreach($transactions as $transaction) {
              if(($transaction->created_at->month == $past_date->month) && ($transaction->created_at->year == $past_date->year)) {
                $fees[$referral->id]['past_month_fees'] += $transaction->fee;
              }
            }
          }

          $fees[$referral->id]['all_fees'] = $fees[$referral->id]['all_fees'] * 0.3;
          $fees[$referral->id]['past_month_fees'] = $fees[$referral->id]['past_month_fees'] * 0.3;

          if($transactions) {
            foreach($transactions as $transaction) {
              $month = $transaction->created_at->month;
              $year = $transaction->created_at->year;
              if(Carbon::now()->month == $month && Carbon::now()->year == $year){
                // do we need to do anything? probably not
              } else {
              if(!isset($monthly_fees[$year][$month])) {
                $monthly_fees[$year][$month]['amount'] = 0;
              }
              $monthly_fees[$year][$month]['amount'] += ($transaction->fee * 0.3);
              $monthly_fees[$year][$month]['status'] = \App\Models\Referrals::where('user_id','=',$user->id)->where('period_month','=',$month)->where('period_year','=',$year)->first();
              }
            }
          }
        }
      }

      return view('private.refer',compact('fees','monthly_fees'));
    }

    public function claim($year, $month) {
      if((isset($year) && isset($month)) && (is_numeric($year) && is_numeric($month))) {
        if(Carbon::now()->month == $month && Carbon::now()->year == $year) {
          Flash::message('You can\'t claim the current month. This action has been logged and will be reviewed by support.');
          return redirect('/refer');
        }
        $amount = 0;
        $user = \Auth::user();
        $referral_payment = \App\Models\Referrals::where('user_id','=',$user->id)->where('period_month','=',$month)->where('period_year','=',$year)->first();

          if(!$referral_payment) {
            $referrals = \App\Models\User::where('refer_uid', '=', $user->id)->get();
            if ($referrals) {
              foreach($referrals as $referral) {
                $buysell = ['transaction_type' => 'buy','transaction_type' =>'sell'];
                $transactions = \App\Models\Ledger::where($buysell)->where('user_to','=',$referral->id)->orderBy('created_at','desc')->get();

                if($transactions) {

                  // $monthly_fees = array();
                  foreach($transactions as $transaction) {
                    $tyear = $transaction->created_at->year;
                    $tmonth = $transaction->created_at->month;

                    if($tyear == $year && $tmonth == $month) {
                      $amount += ($transaction->fee * 0.3);
                    }
                  }
                } else {
                  Flash::message('There are no referrals. This action has been logged and will be reviewed by support.');
                  return redirect('/refer');
                }
              }
            } else {
              Flash::message('No one referred yet. This action has been logged and will be reviewed by support.');
              return redirect('/refer');
            }
          }

          if($amount == 0) {
              Flash::message('There is no income. This action has been logged and will be reviewed by support.');
              return redirect('/refer');
          } else {

            $balance = \App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'aud')->first();
            $oldbalance = $balance->balance;

            $referral_payout = \App\Models\Referrals::create([
                'user_id' => $user->id,
                'amount' => $amount,
                'period_year' => $year,
                'period_month' => $month,
                'status' => 'paid',
            ]);

            $ledger = new \App\Models\Ledger;
            $ledger->user_to = $balance->user_id;
            $ledger->reference_id = "rfp".$referral_payout->id;
            $ledger->transaction_type = "referral_payout";
            $ledger->currency = 'aud';
            $ledger->amount = $amount;
            $ledger->notes = "";
            $ledger->oldbalance = $oldbalance;
            $ledger->save();

            $balance->balance = bcadd($balance->balance, $amount, 2);
            $balance->save();

          }



          Flash::message('Success! The funds are in your account now and immediately available.');
          return redirect('/refer');
        } else {
          Flash::message('There\'s already a payment made for this year and month.');
          return redirect('/refer');
        }

      Flash::message('There was an issue processing your request, please contact support.');
      return redirect('/refer');
    }

    public function referee($referid)
    {
      if($referid){
        Cookie::queue('referid', $referid, 86400);
      }
      return redirect('/');
    }
}
