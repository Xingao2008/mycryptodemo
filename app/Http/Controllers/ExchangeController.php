<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 23/02/2018
 * Time: 1:16 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Setting;
use Cookie;
use Illuminate\Support\Facades\Auth;
use Laracasts\Flash\Flash;

class ExchangeController extends Controller
{
    public function index(Request $request) {
        //double check is user already got the token
        //incase some logged in user suddenly move to exchange
         //our cookie is generated in JS so cannot use Laravel internal function
        if (!isset($_COOKIE['api_token']) || empty($_COOKIE['api_token'])) {
            Auth::logout();
            $request->session()->flush();
            //strange, cookie can only destroy like this
            Cookie::queue('uid', null, -1);
            Cookie::queue('api_token', null, -1);
            //laravel internal will recreate a new session to replace the old one
            return redirect()->route('login')->withCookie(cookie('laravel_session', '', -1));
        }
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            Flash::message('Exchange is temporarily disabled.');
            return redirect('/dashboard');
        }
        
        return view('private.exchange');
    }

    public function manageOrder() {

        return view('private.manage-order');
    }
}