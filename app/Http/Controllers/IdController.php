<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadRequest;
use App\Http\Requests\VerifySMSRequest;
use App\Models\Bank;
use App\Models\TextVerification;
use App\Models\Uploads;
use App\Models\User;
use App\Models\Verification;
use App\Notifications\IdVerificationCode;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Models\Version;

class IdController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware(['auth', 'status']);
    }

    public function index()
    {
        $user_id = \Auth::user()->id;
        $passport = Uploads::where('user_id', '=', $user_id)->where('type', '=', 'passport')->first();
        $drivers = Uploads::where('user_id', '=', $user_id)->where('type', '=', 'drivers')->first();
        $bank = Uploads::where('user_id', '=', $user_id)->where('type', '=', 'bank')->first();

        if ($passport !== null) {
            $passport_location = $passport->location;
        }

        if ($drivers !== null) {
            $drivers_location = $drivers->location;
        }

        if ($bank !== null) {
            $bank_location = $bank->location;
        }

        return view('private.upload-id', compact('passport_location', 'drivers_location', 'bank_location'));
    }

    public function ubc_index()
    {

        $user_id = \Auth::user()->id;
        $ubc = Uploads::where('user_id', '=', $user_id)->where('type', '=', 'ubc')->first();

        if ($ubc !== null) {
            $ubc_location = $ubc->location;
        }

        return view('private.upload-ubc', compact('ubc_location'));
    }

    public function upload(UploadRequest $request)
    {
        $user_id = \Auth::user()->id;
        $verification = Verification::where('user_id', '=', $user_id)->first();

        if (isset($request->passport)) {
            $passport = $request->passport->store('identification');
            Uploads::create([
                'user_id' => $user_id,
                'type' => 'passport',
                'location' => $passport,
            ]);

            $verification->id_passport = 'pending';
        }

        if (isset($request->drivers)) {
            $drivers = $request->drivers->store('identification');
            Uploads::create([
                'user_id' => $user_id,
                'type' => 'drivers',
                'location' => $drivers,
            ]);

            $verification->id_drivers = 'pending';
        }

        if (isset($request->bank)) {
            $bank = $request->bank->store('identification');
            Uploads::create([
                'user_id' => $user_id,
                'type' => 'bank',
                'location' => $bank,
            ]);

            $verification->id_bank = 'pending';
        }

        if (!isset($request->passport) && !isset($request->drivers) && !isset($request->bank)) {
            Flash::message('Nothing was uploaded. Please ensure you attach an image.');

            return redirect('account/upload_id');
        }


        $verification->save();
        Flash::message('Successfully uploaded!');

        return redirect('account/upload_id');
    }

    public function ubc_upload(UploadRequest $request)
    {
        $user_id = \Auth::user()->id;
        $verification = Verification::where('user_id', '=', $user_id)->first();

        if (isset($request->ubc)) {
            $ubc = $request->ubc->store('identification');
            Uploads::create([
                'user_id' => $user_id,
                'type' => 'ubc',
                'location' => $ubc,
            ]);

            $verification->ubc = 'pending';
        }

        if (!isset($request->ubc)) {
            Flash::message('Nothing was uploaded. Please ensure you attach an image.');

            return redirect('account/upload_ubc');
        }

        $verification->save();
        Flash::message('Successfully uploaded!');

        return redirect('account/upload_ubc');
    }

    public function sms_index()
    {
        $user = \Auth::user();

        $verification = Verification::where('user_id', '=', $user->id)->first();

        $textverification = TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();

        if ($textverification !== NULL) {
            $show_form = TRUE;
        }

        return view('private.verify-sms', compact(['verification', 'show_form']));
    }

    public function sms_verify(VerifySMSRequest $request)
    {

        if ($request->sms == 'send') {
            //TODO: Add text/sms limits per account

            $user = \Auth::user();

            $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

            $user->notify(new IdVerificationCode($code));

            TextVerification::create([
                'user_id' => $user->id,
                'to' => $user->phone,
                'type' => 'verify',
                'code' => $code,
            ]);


            Flash::message('Success! Please wait up to 30 seconds for your verification code.');

            return redirect('account/verify_sms');
        }

        if ($request->sms == 'verify') {
            $user = \Auth::user();
            $textverification = TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();
            $verification = Verification::where('user_id', '=', $user->id)->first();
            $user_entered = $request->code;

            if ($textverification->attempts == 5) {
                Flash::message('Sorry, you\'ve exceeded the maximum number of allowed attempts. Please resend the SMS.');

                return redirect('account/verify_sms');
            }

            if ($user_entered == $textverification->code) {
                $textverification->verified = TRUE;
                $textverification->save();

                $verification->sms = 'approved';
                $verification->save();
                Flash::message('Congratulations! You\'ve successfully verified your mobile.');

                return redirect('verify');
            } else {
                $textverification->attempts++;
                $textverification->save();
                Flash::message('Incorrect code, please try again. Note that a maximum of 5 attempts per SMS are allowed. (attempt ' . $textverification->attempts . ' of 5)');

                return redirect('account/verify_sms');
            }
        }
    }

    public function bank()
    {
        $user = \Auth::user();

        $bank = Bank::where('user_id', '=', $user->id)->first();

        return view('private.bank', compact(['bank']));
    }

    public function bank_save(Request $request)
    {

        $user = \Auth::user();

        $verification = Verification::where('user_id', '=', $user->id)->first();

//        $bank = Bank::where('user_id', '=', $user->id)->first();
        if (isset($user->address->country_id) && $user->address->country_id != 36) {
            $this->validate($request, [
                'bank_name' => 'required',
                'bank_branch' => 'sometimes',
                'bank_number' => 'required|numeric',
            ]);
        } else {
            $this->validate($request, [
                'bank_name' => 'required',
                'bank_branch' => 'sometimes',
                'bank_bsb' => 'required|digits:6',
                'bank_number' => 'required|numeric',
            ]);
        }
        

        $textverification = TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();

        if ($textverification !== NULL) {
            $show_form = TRUE;
        }

        if (!is_null($request->bank_name)
        && !is_null($request->bank_number)) {

            //if haven't create, create a new bank acc
            $bank = Bank::where('user_id', '=', $user->id)->first();
            if (is_null($bank)) {
                $bank = new Bank;
                $bank->user_id = auth()->id();
                $bank->bank_name = $request->bank_name;
                $bank->bank_branch = $request->bank_branch;
                $bank->bank_bsb = $request->bank_bsb ?? '';
                $bank->bank_number = $request->bank_number;
                $bank->save();
            }

            $ubc_user = User::where('id', '=', auth()->id())->first();
            $ubc_user->ubc = strtoupper(bin2hex(openssl_random_pseudo_bytes(3)) . '-' . bin2hex(openssl_random_pseudo_bytes(3)));
            $ubc_user->save();

            $verification->bank = "approved";
            $verification->save();

            $current_ver = Version::where('model', '=', 'banks')
                ->where('user_id', '=', auth()->id())
                ->where('status', '=', 'verified')
                ->orderBy('created_at', 'desc')
                ->first();

            if (is_null($current_ver)) {
                $valid_version = 1;
            } else {
                $valid_version = $current_ver->version + 1;
            }

            $save_data = array(
                'bank_name' => $request->bank_name,
                'bank_branch' => $request->bank_branch,
                'bank_bsb' => $request->bank_bsb ??'',
                'bank_number' => $request->bank_number
            );

            $new_version = new Version;
            $new_version->model = 'banks';
            $new_version->user_id = auth()->id();
            $new_version->version = $valid_version;
            $new_version->model_id = $user->bank->id;
            $new_version->data = json_encode($save_data);
            $new_version->status = 'pending';
            $new_version->save();

            if ($valid_version === 1) {

                $new_version->status = 'verified';
                $new_version->save();

                return redirect()->route('account')->with('status', 'Account updated.');

            } else {

                $dob = null;

                $version['id'] = $new_version->id;
                $version['status'] = $new_version->status;

                return view('private.account.smsverify', compact(['version', 'show_form', 'dob']));
            }

        } else {
            flash('Please fulfill all the bank detail')->error();

            return redirect('/account/bank');
        }



//        if (!$bank) {
//            $bank = Bank::create([
//                'user_id' => auth()->id(),
//                'bank_name' => $request->bank_name,
//                'bank_branch' => $request->bank_branch,
//                'bank_bsb' => $request->bank_bsb,
//                'bank_number' => $request->bank_number,
//            ]);
//
//
//            $ubc_user = User::where('id', '=', auth()->id())->first();
//            $ubc_user->ubc = strtoupper(bin2hex(openssl_random_pseudo_bytes(3)) . '-' . bin2hex(openssl_random_pseudo_bytes(3)));
//            $ubc_user->save();
//
//            $verification->bank = "approved";
//            $verification->save();
//            Flash::message('<b>Success!</b> You\'ve added your bank account. Support will review shortly.');
//        } else {
//            $bank->bank_name = $request->bank_name;
//            $bank->bank_branch = $request->bank_branch;
//            $bank->bank_bsb = $request->bank_bsb;
//            $bank->bank_number = $request->bank_number;
//            $bank->save();
//
//            $verification->bank = "pending";
//            $verification->save();
//            Flash::message('<b>Success!</b> You\'ve updated your bank account. Support will review shortly.');
//        }

//        return redirect('account/bank');
    }
}
