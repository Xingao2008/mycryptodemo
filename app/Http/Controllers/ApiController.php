<?php

namespace App\Http\Controllers;

use App\Mail\OverSixtySecond;
use App\Models\ExternalPrices;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use App\Helpers\VixVerifyHelper;
use App\Classes\GBGSoapClient;
use \Freshdesk\Api;

class ApiController extends Controller
{
    /**
     * How long to cache the pricing results
     * @var int
     */
    private $currencyCacheTime = 5;

    /**
     * Get BTC pricing history
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function btc() {
		return response()->json(
		    $this->getCryptoPrice('btc')
        );
    }

    /**
     * Get ETH pricing history
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function eth() {
        return response()->json(
            $this->getCryptoPrice('eth')
        );
    }

    /**
     * Get LTC pricing history
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ltc() {
        return response()->json(
            $this->getCryptoPrice('ltc')
        );
    }

    /**
     * Get XRP pricing history
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function xrp() {
        return response()->json(
            $this->getCryptoPrice('xrp')
        );
    }

    public function pwr() {
        return response()->json(
            $this->getCryptoPrice('pwr')
        );
    }

    public function getBalanceInAud() {
        $prices = prices('origin');

        $balance = \App\Models\Balance::funds();
        foreach(config('currencies.crypto') as $cur) {
            $myBalance[$cur] = $prices[$cur]->ask * $balance[$cur][0]->getOriginal('balance');
        }

        $summaryInAud =  array_sum($myBalance); // total asset balance

        foreach(config('currencies.crypto') as $cur) {
            if ($summaryInAud == 0) {
                $percentInAud[$cur] = 0; // prevent divide by 0
            } else {
                $percentInAud[$cur] = bcdiv($myBalance[$cur], $summaryInAud, 8) * 100;
            }
        }

        return $percentInAud; // return in percentage
    }

    public function getBalanceUpdateInAud() {
        $prices24 = prices('24');
        $prices = prices('origin');

        $balance = \App\Models\Balance::funds();
        foreach(config('currencies.crypto') as $cur) {
            $myBalance[$cur] = $prices24[$cur]->ask * $balance[$cur][0]->getOriginal('balance');
        }

        $summaryInAud_24 =  array_sum($myBalance); // total asset balance before 24

        foreach(config('currencies.crypto') as $cur) {
            $myBalance[$cur] = $prices[$cur]->ask * $balance[$cur][0]->getOriginal('balance');
        }

        $summaryInAud = array_sum($myBalance); // total asset balance

        if ($summaryInAud == 0) {
            $assetChange = 0;
        } else {
            $assetChange = bcdiv(bcsub($summaryInAud, $summaryInAud_24, 8), $summaryInAud, 8) * 100; // return in percent
        }

        $assetChange = round($assetChange, 2);

        return $assetChange;
    }

    private function getCryptoPrice($currency){
        // use redis to cache
        $currency = \Cache::remember("api_{{$currency}}_prices", $this->currencyCacheTime, function() use ($currency) {

            $prices = ExternalPrices::select(\DB::raw('UNIX_TIMESTAMP(created_at) as myTime'), 'ask')->where('currency','=',$currency)->limit(10000)->orderBy('created_at', 'desc')->get();
            // dd($prices);
            $i = 0; $return = array();
            foreach ($prices as $price) {
                $return[$i][0] = (int)($price['myTime'].'000');
                $return[$i][1] = (float)$price['ask'];
                $i++;
            }
            return $return;

        });

        return response()->json($currency);

//       return cache()->remember("api_{{$currency}}_prices", $this->currencyCacheTime, function() use ($currency) {
//
//            $prices = \App\Models\ExternalPrices::where('currency','=',$currency)->limit(360)->orderBy('created_at', 'desc')->get();
//            $i = 0;
//            foreach ($prices as $key=>$value) {
//                $value->ask = hidden_applier($value->ask, 'ask');
//
//                // $result[$key][0] = $value->created_at->timestamp;
//                $result[$key][0] = (time() - (60*$i)) * 1000;
//                $result[$key][1] = (float)$value->ask;
//                $i++;
//            }
//
//            $historical_prices = \App\Models\PriceControl::orderBy('created_at', 'asc')->get();
//
//           $price_history = [];
//
//            foreach($historical_prices as $historical_price) {
//                $price_history[$historical_price->created_at->toDateTimeString()][$historical_price->currency] = [
//                    'id' => $historical_price->id,
//                    'value' => $historical_price->value,
//                ];
//            }
//
//            foreach($price_history as $key=>$value) {
//                if(isset($nextkey)){
//                    $oldkey = $key;
//                    $oldkey = \Carbon\Carbon::parse($oldkey);
//                    $nextkey = \Carbon\Carbon::parse($nextkey);
//                    $timediff = $oldkey->diffInMinutes($nextkey);
//                    $price_history[$key]['timediff'] = $timediff;
//                } else {
//                    $timevalue = 0;
//                }
//                $nextkey = $key;
//                $values['btc'] = $value['btc'];
//                $values['ltc'] = $value['ltc'];
//                $values['eth'] = $value['eth'];
//                $values['xrp'] = $value['xrp'];
//                $values['pwr'] = $value['pwr'];
//            }
//
//            $now = \Carbon\Carbon::now();
//            $stringnow = $now->toDateTimeString();
//            $price_history[$stringnow]['timediff'] = $now->diffInMinutes(\Carbon\Carbon::parse($nextkey));
//            $price_history[$stringnow]['btc'] = $values['btc'];
//            $price_history[$stringnow]['ltc'] = $values['ltc'];
//            $price_history[$stringnow]['eth'] = $values['eth'];
//            $price_history[$stringnow]['xrp'] = $values['xrp'];
//            $price_history[$stringnow]['pwr'] = $values['pwr'];
//
//            $sumtime = 0;
//            $totalsumtime = 0;
//            foreach($price_history as $key=>$value) {
//                if(isset($value['timediff'])) {
//                    if($value['timediff'] !== 0) {
//                        $totalsumtime = $totalsumtime + $value['timediff'];
//                    }
//                }
//            }
//
//            foreach($price_history as $key=>$value) {
//                if(isset($value['timediff'])) {
//                    if($value['timediff'] !== 0) {
//                        $sumtime = $value['timediff'];
//                        $counter = 0;
//
//                        while($sumtime !== 0) {
//                            if(isset($result[$totalsumtime])) {
//                                $result[$totalsumtime][1] = $result[$totalsumtime][1] * (1+($value[$currency]['value'] / 100));
//
//
//                            }
//                                $counter++;
//                                $sumtime--;
//                                $totalsumtime--;
//                        }
//                    }
//                }
//            }
//            $result[0][1] = ($result[0][1] * ($price_history[$stringnow][$currency]['value'])/100) + $result[0][1];
//
//            return $result;
//       });
    }


    public function vixVerify(Request $request)
    { 
        if ($request->get('user')) {
            
                $vixVerify = new VixVerifyHelper($request->all());
                $resp = $vixVerify->verify();
    
                $message = '';
                $account_status = '';
                if (!$resp['success']) {
                    $success = false;
                    $account_status = strtolower($resp['vix_status']);
    
                    if ($resp['vix_status'] == 'ERROR') {
                        $message = (isset($resp['vix_error_message'])) ? $resp['vix_error_message'] : "ERROR";
                    } else {
                        $message = 'There was a problem verifying your identification. Please email support@mycryptowallet.com if you cannot proceed.';
                    }
    
                } else {
                    $success = true;
                    $account_status = 'approved';
                }
    
                if ($success) {
                    $verification = \App\Models\Verification::where('user_id','=',$request->get('user'));
                    if ($verification) {
                        $verification->update([
                            'id_passport' => $account_status,
                            'id_drivers' => $account_status,
                            'id_drivers_notes' => 'vix verified',
                            'id_bank' => $account_status
                        ]);
                    }
    
                    $user = \App\Models\User::find($request->get('user'));
    
                    $dob = Carbon::createFromFormat('d/m/Y', $user->dob);
                    if($dob->age >= 60) {
                        \Mail::send(new OverSixtySecond($user));
                    } else {
                        $user->update(['verified' => 1]);
                    }
    
                    \App\Models\Ip::create([
                        'user_id'    => $user->id,
                        'ip'      => request()->ip(),
                        'event' => 'vix verified',
                    ]);
                }
    
                $data = compact('success', 'message');
    
                return response()->json($data);
            
        } else {
            //check is the user fail 3 times before
            
            if ($request->has('country_id') && in_array($request->country_id,[840, 826, 124, 392])) {
                //send to GBG
                $dob = explode('/', $request->dob);
                $country = \App\Models\Countries::find($request->country_id);
                $gbg = new GBGSoapClient();
                $gbg->setProfile('US');
                $user = \App\Models\User::find($request->user_id);
                $attempts = \App\Models\ServiceLog::where('user_id', $user->id)->where('service_type', 'gbg_validation')->count();
                if ($attempts > 2) {
                    return respondWithJson(false, 'Too many attempts', '', 200);
                }
                $data = [
                    'user' => $user,
                    'personalDetails' => [
                        'Forename' => $request->givenNames,
                        'Surname' => $request->surname,
                        'DOBDay' => $dob[0],
                        'DOBMonth' => $dob[1],
                        'DOBYear' => $dob[2][0].$dob[2][1]
                    ],
                    'currentAddress' => [
                        'Country' => $country->name,
                        'Street' => $request->street,
                        'City' => $request->city,
                        'ZipPostcode' => $request->post_code,
                        'StateDistrict' => $request->state
                    ],
                    'contactDetails' => [
                        'MobileTelephone' => $request->phone
                    ],
                    'GBGIdentityID'=> ($request->has('GBGIdentityID')?$request->GBGIdentityID : '')
                ];
                $return = $gbg->sendAuthenticateSP($data);
                // echo '<pre>';
                // print_r($return);
                // echo '</pre>';
                // exit();
                if ($return['success']) {
                    //success
                    if ($return['data']['Score'] < 1001) {
                        $log = new \App\Models\ServiceLog();
                        $log->user_id = $user->id;
                        $log->service_type = 'gbg_validation';
                        $log->request = json_encode($data);
                        $log->response = json_encode($return);
                        $log->save();
                        if ($attempts == 2) {
                            $this->fireSupportTicket(['user_id' => $user->id, 'request'=> json_encode($data), 'response' => json_encode($return)]);
                        }
                        return respondWithJson(false, 'Your score did not match our minimum requirement', 
                            $return['data']['BandText']
                        , 200);
                    } else {
                        //success and enough score
                        $verification = \App\Models\Verification::where('user_id','=',$request->get('user'));
                        if ($verification) {
                            $verification->update([
                                'id_passport' => $account_status,
                                'id_drivers' => $account_status,
                                'id_drivers_notes' => 'vix verified',
                                'id_bank' => $account_status
                            ]);
                        }
                        return respondWithJson(true, 'Validation successful', 
                            $return['data']['BandText']
                        , 200);
                    }
                    
                } else {
                    //fail
                    $log = new \App\Models\ServiceLog();
                    $log->user_id = $user->id;
                    $log->service_type = 'gbg_validation';
                    $log->request = json_encode($data);
                    $log->response = json_encode($return);
                    $log->save();
                    if ($attempts == 2) {
                        $this->fireSupportTicket(['user_id' => $user->id, 'request'=> json_encode($data), 'response' => json_encode($return)]);
                    }
                    return respondWithJson(false, $return['error']['message'], '', 200);
                }
            }
            return response()->json(['success' => false, 'message' => 'There was a problem verifying your identification. Please email support@mycryptowallet.com if you cannot proceed.']);
        }
    }

    public function vixVerifyAttempts(Request $request)
    {
        $success = true;
        $message = '';

        $user_id = $request->get('user_id');

        $user = \App\Models\User::find($user_id);
        if ($user) {
            $user->verify_count = $user->verify_count + 1;
            $success = $user->update();
        }

        $data = compact('success', 'message');

        return response()->json($data);
    }

    protected function fireSupportTicket($data)
    {
        $email_config_id = config('freshdesk.email_config_id', 0);

        if(!$email_config_id) {
            $emails = cache()->remember('freshdesk_email_configs', 1440, function () {
                $api = new Api(config('freshdesk.api_key'), config('freshdesk.domain'));
                return $api->emailConfigs()->all();
            });

            $email_config_id = $emails[0]['id'];
        }

        $user = User::whereId($data['user_id'])->firstOrFail();
        $body = $data['request'].'<br /><br />'.$data['response'];
        $data = [
            'description' => $body,
            //'description_text' => strip_tags(request('body')),
            'email' => $user->email,
            'name' => $user->name,
            'priority' => 3,
            'subject' => 'User validation fail more than 3 times from GBG Validation',
            //'status' => 2,
            'email_config_id' => (int)$email_config_id,
            // 'cc_emails' => [\Auth::user()->email]
        ];

        try {
            $api = new Api(\Auth::user()->get_setting('freshdesk_api_key', false), config('freshdesk.domain'));
            $r = $api->tickets->createOutboundEmail($data);
        } catch (\Exception $e) {
            flash($e->getRequestException()->getMessage())->error();
            return redirect()->route('admin.ticket.create', request()->get('user_id'))->withInput();
        }
    }

    
}
