<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Balance;
use App\Models\ExternalPrices;
use App\Models\Wallet;
use config;

class WalletsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = \Auth::user();

        $price = ExternalPrices::price();

        $wallets = Balance::where('user_id', '=', $user->id)->get();
        $btc = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'btc')->first();
        $eth = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'eth')->first();
        $ltc = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'ltc')->first();
        $xrp = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'xrp')->first();
        $pwr = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'eth')->first();

        $wallet['total'] = 0;
        $wallet['nzd'] = 0;
        $wallet['aud'] = 0;
        $wallet['usd'] = 0;

        foreach($wallets as $key=>$value) {
            $wallet[$value->currency] = $value;
            // If the currency is AUD, just add it, else ask * balance
            if ($value->currency == 'aud') {
                $wallet['aud'] = $value->balance;
            }elseif ($value->currency == 'nzd') {
                $wallet['nzd'] = $value->balance;
            }elseif ($value->currency == 'usd') {
                $wallet['usd'] = $value->balance;
            }else{
                $wallet['total'] +=  $price[$value->currency]->bid * $value->balance;
            }
            if ( !in_array($value->currency, config('currencies.fiatCurrencies')) ) {
                $wallet[$value->currency]['get_crypto_balance'] = get_crypto_balance($value->currency);
                $wallet[$value->currency]['get_user_crypto_value'] = get_user_crypto_value($value->currency);
                $wallet[$value->currency]['get_user_nzd_crypto_value'] = convertFiatCurrency('aud','nzd',get_user_crypto_value($value->currency));
                $wallet[$value->currency]['get_user_usd_crypto_value'] = convertFiatCurrency('aud','usd',get_user_crypto_value($value->currency));
            }
            
        }
        $wallet['nzd_total'] = convertFiatCurrency('aud','nzd',$wallet['aud']) +  convertFiatCurrency('aud','usd',$wallet['usd']) + $wallet['nzd'] + $wallet['total'];
        $wallet['usd_total'] = convertFiatCurrency('aud','usd',$wallet['aud']) + convertFiatCurrency('aud','nzd',$wallet['nzd']) + $wallet['usd'] + $wallet['total'];
        $wallet['total'] = convertFiatCurrency('nzd','aud',$wallet['nzd']) +  convertFiatCurrency('aud','usd',$wallet['usd']) + $wallet['aud'] + $wallet['total'];
        return view('private.wallets', compact('wallet', 'btc', 'eth', 'ltc', 'xrp', 'pwr', 'price'));
    }
}
