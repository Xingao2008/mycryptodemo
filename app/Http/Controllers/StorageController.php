<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use App\Models\Verification;
use Response;

class StorageController extends Controller
{
    public function postImage($filename)
    {
        $path = storage_path('app/news/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        //dd($uploads->user_id);
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    public function getImage($filename)
    {

        $path = storage_path('app/identification/' . $filename);

        if (!File::exists($path)) {
            abort(404);
        }

        // Check user is allowed.
        // TODO: Allow admin to access anyway
        $user = \Auth::user();
        $uploads = \App\Models\Uploads::where('location', '=', 'identification/' . $filename)->first();
        if ($user->id !== $uploads->user_id) {
            if (!$user->isAdmin()) {
                return redirect('/login');
            }
        }
        //dd($uploads->user_id);
        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}