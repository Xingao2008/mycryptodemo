<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function home()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        \Assets::add('charts');
        return view('public.home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function marketplace()
    {
        return view('public.marketplace');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function buy()
    {
        return view('public.about-buy');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('public/about');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('public/contact');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function support()
    {
        return view('public/support');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacyPolicy()
    {
        return view('public/privacy-policy');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function termsConditionsService()
    {
        return view('public/terms-conditions-service');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function termsConditionsUse()
    {
        return view('public/terms-conditions-use');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function disclaimer()
    {
        return view('public/disclaimer');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function copyright()
    {
        return view('public/copyright');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function fees()
    {
        return view('public/fees');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function news()
    {
        $posts = \App\Models\News::all();

        return view('public.news')->with('posts', $posts);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function newsPost($slug)
    {
        $post = \App\Models\News::where('slug', '=', $slug)->firstOrFail();
        return view('public.news-single')->with('post', $post);
    }

}
