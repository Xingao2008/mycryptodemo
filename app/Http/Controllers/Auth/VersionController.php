<?php

namespace App\Http\Controllers\Auth;

use App\Models\Address;
use App\Models\Bank;
use App\Models\Verification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\VerifySMSRequest;
use App\Notifications\IdVerificationCode;
use App\Models\TextVerification;
use App\Models\Version;
use Carbon\Carbon;
use App\Mail\OverSixtyFirst;

class VersionController extends Controller
{
    public function sms_verify(VerifySMSRequest $req)
    {
        if ($req->sms == 'send') {

            $user = \Auth::user();

            $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

            $user->notify(new IdVerificationCode($code));

            TextVerification::create([
                'user_id' => $user->id,
                'to' => $user->phone,
                'type' => 'verify',
                'code' => $code,
            ]);

            $version['id'] = $req->version_id;
            $version['status'] = $req->version_status;
            $dob = $req->dob;
            $show_form = True;


            flash('Success! Please wait up to 30 seconds for your verification code.')->success();

            return view('private.account.smsverify', compact(['version', 'show_form', 'dob']));
        }
    }

    public function verify(Request $req)
    {
        $user = \Auth::user();
        $user_id = $user->id;
        $version_id = $req->version_id;
        $version_status = $req->version_status;
        $code = $req->code;
        $dob = $req->dob;

        if ($req->sms == 'verify') {
            $txtVerify = TextVerification::where('user_id', '=', $user_id)
                ->orderBy('created_at', 'desc')
                ->orderBy('id', 'desc')
                ->where('verified', '=', '0')->first();

            if ($txtVerify->attempts == 5) {

                $version['id'] = $version_id;
                $version['status'] = $version_status;

                if ($txtVerify !== NULL) {
                    $show_form = TRUE;
                }

                flash('Sorry, you\'ve exceeded the maximum number of allowed attempts. Please resend the SMS.')->error();

                return view('private.account.smsverify', compact(['version', 'show_form', 'dob']));
            }


            if ($code == $txtVerify->code) {

                $txtVerify->verified = TRUE;
                $txtVerify->save();

                $verification = Verification::firstOrNew(['user_id' => $user->id]);
                if ($verification->sms != 'approved') {
                    $verification->sms = 'approved';
                    $verification->save();
                }

                $version = Version::where('id', '=', $version_id)->firstOrFail();
                $version->status = 'verified';
                $version->save();

                $ver_arr = json_decode($version->data);

                if ($version->model === 'addresses') {
                    $address = Address::where('id', '=', $version->model_id)->firstOrFail();
                    $address->street = $ver_arr->street;
                    $address->city = $ver_arr->city;
                    $address->state = $ver_arr->state;
                    $address->post_code = $ver_arr->post_code;
                    $address->country_id = $ver_arr->country_id;
                    $address->save();
                } else {
                    $bank = Bank::where('id', '=', $version->model_id)->firstOrFail();
                    $bank->bank_name = $ver_arr->bank_name;
                    $bank->bank_branch = $ver_arr->bank_branch;
                    $bank->bank_bsb = $ver_arr->bank_bsb;
                    $bank->bank_number = $ver_arr->bank_number;
                    $bank->save();
                }

                //send email to over 60
                if (!is_null($dob)) {
                    $dob = Carbon::createFromFormat('d/m/Y', $dob);

                    if ($dob->age >= 60) {
                        \Mail::send(new OverSixtyFirst($user));
                    }
                }

                flash('Congratulations! You\'ve successfully verified your mobile. Your update will be processed shortly.')->success();

                return redirect()->route('account');
            } else {

                $txtVerify->attempts++;
                $txtVerify->save();

                $version['id'] = $version_id;
                $version['status'] = $version_status;

                if ($txtVerify !== NULL) {
                    $show_form = TRUE;
                }

                flash('Incorrect code, please try again. Note that a maximum of 5 attempts per SMS are allowed. (attempt ' . $txtVerify->attempts . ' of 5)')->error();

                return view('private.account.smsverify', compact(['version', 'show_form', 'dob']));
            }
        }
    }
}
