<?php

namespace App\Http\Controllers\Auth;

use App\Events\LoginBlacklisted;
use App\Http\Controllers\Controller;
use App\Models\Firewall;
use App\Traits\AttemptTracker;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Mail\ResetPassword;
use Illuminate\Support\Facades\Session;

class ForgotPasswordController extends Controller
{
    use AttemptTracker;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    private $maxAttempts = 20;
    private $decayMinutes = 1440;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $email = $request['email'];
        $token = $request['_token'];

        $this->validateEmail($request);

        if ($this->hasTooManyAttempts($request)) {
            // Keep incrementing anyway
            $this->incrementAttempts($request);

            $this->fireAttemptLockoutEvent($request);

            return response()->json(array('state' => 500, 'msg' => 'You have tried this too many times, please contact support if you need help.'));
        }

        $this->incrementAttempts($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $this->sendResetLinkResponse($response);
    }

    protected function sendResetLinkResponse($response)
    {

        return response()->json(array('state' => 200, 'msg' => 'If that email exists on our system, you will receive a password reset link. Please also check spam/junk folder'));
//        flash('If that email exists on our system, you will receive a password reset link!')->success();
//        return redirect('/password/reset');
    }

    /**
     * Fire an event when a lockout occurs. - Also Blacklist the IP
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function fireAttemptLockoutEvent(Request $request)
    {
        $blacklist = Firewall::blacklist(
            $request->ip(),
            cache($this->attemptCacheKey($request)) . ' failed login attempts',
            Carbon::now()->addDays(2)
        );

        if ($blacklist) {
            event(new LoginBlacklisted($blacklist));
        }
    }
}
