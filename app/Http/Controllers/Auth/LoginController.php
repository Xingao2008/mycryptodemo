<?php

namespace App\Http\Controllers\Auth;

use App\Events\LoginBlacklisted;
use App\Http\Controllers\Controller;
use App\Models\Firewall;
use App\Models\LoginAttempts;
use App\Models\TfaResets;
use App\Models\User;
use App\Models\Verification;
use App\Traits\AttemptTracker;
use Carbon\Carbon;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use PragmaRX\Google2FA\Google2FA;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Crypt;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AttemptTracker, AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * How long to store login attempts for
     *
     * @var int
     */
    public $decayMinutes = 60;


    /**
     * How long to blacklist a failed user for
     *
     * @var int
     */
    public $blacklistHours = 48;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    // roy's new function
    public function doLogin(Request $request)
    {
        $validator = \Validator::make(request()->all(), [
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        // Make sure they actually include an email and password
        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => 'Email and password are required'));
        }

        if ($this->hasTooManyAttempts($request)) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            $this->fireAttemptLockoutEvent($request);
            return response()->json(array('state' => 500, 'msg' => 'Too many attempts, login locked'));
        }

        if (!\Auth::validate(request()->only(['email', 'password']))) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            $this->incrementAttempts($request);
            return response()->json(array('state' => 500, 'msg' => 'Invalid email or password'));
        }

        $user = User::whereEmail(request()->get('email'))->first();

        //check status of flow is finish or not
        if ($user->verification->register_flow !== 'finish') {
            return response()->json(array('state' => 500, 'msg' => 'The registration is not completed yet, please go to registration page to finish the rest'));
        }

        if ($user->status != 'active') {
            return response()->json(array('state' => 500, 'msg' => 'Please verify your email'));
        }

        \App\Models\Ip::create([
            'user_id' => $user->id,
            'ip' => request()->ip(),
            'event' => 'login',
        ]);

        // check tfa
        if (!$user->tfa_status) {
            \App\Models\Ip::create([
                'user_id' => $user->id,
                'ip' => request()->ip(),
                'event' => 'login',
            ]);

            return $this->attemptLogin($this->credentials($request), $user);
        }

        if (!request()->get('tfa', false)) {
            return response()->json(array('state' => 403, 'msg' => 'TFA required'));
        }

        $tfa_check = $user->check_tfa(request()->get('tfa', 0));

        if (!$tfa_check) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            return response()->json(array('state' => 400, 'msg' => 'TFA failed'));
        }
        // end of tfa

        return $this->attemptLogin($this->credentials($request), $user);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = \Validator::make(request()->all(), [
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        // Make sure they actually include an email and password
        if ($validator->fails()) {
            return response()->json(['error' => 'invalid_input', 'errors' => $validator->messages()->first()], 400);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyAttempts($request)) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            $this->fireAttemptLockoutEvent($request);
            return response()->json(['error' => 'login_lockout'], 400);
        }

        // Check if they have valid login details
        if (!\Auth::validate(request()->only(['email', 'password']))) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            $this->incrementAttempts($request);
            return response()->json(['error' => 'invalid_login'], 400);
        }

        $user = User::whereEmail(request()->get('email'))->first();

        if (!$user->tfa_status) {
            \App\Models\Ip::create([
                'user_id' => $user->id,
                'ip' => request()->ip(),
                'event' => 'login',
            ]);

            return $this->attemptLogin($this->credentials($request), $user);
        }

        if (!request()->get('tfa', false)) {
            return response()->json(['message' => 'tfa_required']);
        }

        $tfa_check = $user->check_tfa(request()->get('tfa', 0));

        if (!$tfa_check) {
            LoginAttempts::create(['ip_address' => $request->ip(), 'email' => request('email'), 'failed' => true]);
            return response()->json(['error' => 'tfa_failed'], 400);
        }

        \App\Models\Ip::create([
            'user_id' => $user->id,
            'ip' => request()->ip(),
            'event' => 'login',
        ]);

        return $this->attemptLogin($this->credentials($request), $user);
    }

    // roy's new function
    private function attemptLogin($credentials, User $user): JsonResponse
    {
        $login = \Auth::attempt(request()->only(['email', 'password']), request('remember'));

        if ($login) {
            LoginAttempts::create(['ip_address' => request()->ip(), 'email' => request('email'), 'failed' => false]);
            $token = JWTAuth::fromUser($user);
            $encryptedUserId = base64_encode($user->id);

            $steps = Verification::where('user_id', '=', $user->id)->first();
//            return response()->json(array('state' => 200, 'step' => $steps->register_flow, 'msg' => 'Login was successful'));

            return response()->json(
                [
                    'state' => 200,
                    'step' => $steps->register_flow,
                    'msg' => 'Login was successful',
                    'tfa_status' => $user->tfa_status,
                    'token' => $token,
                    'uid'=> $encryptedUserId
                ]);
        } else {
            LoginAttempts::create(['ip_address' => request()->ip(), 'email' => request('email'), 'failed' => true]);
//            return response()->json(['error' => 'invalid_login'], 400);
            return response()->json(array('state' => 500, 'msg' => 'Invalid email or password'));
        }

    }

    public function tfaReset($code)
    {
        $tfa_reset = TfaResets::whereAuthCode($code)->first();

        if (!$tfa_reset) {
            flash('URL invalid or expired, please contact support')->error();
            return redirect()->to('login');
        }

        $user = User::findOrFail($tfa_reset->user_id);

        $user->tfa_status = 0;
        $user->save();

        $tfa_reset->active = 0;
        $tfa_reset->complete = 1;
        $tfa_reset->auth_code = '';
        $tfa_reset->save();

        flash('TFA Reset, you may now login')->success();

        return redirect()->to('login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {   
        
        Auth::logout();
        $request->session()->flush();
        //strange, cookie can only destroy like this
        Cookie::queue('uid', null, -1);
        Cookie::queue('api_token', null, -1);
        //laravel internal will recreate a new session to replace the old one
        return redirect()->route('home')->withCookie(cookie('laravel_session', '', -1));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Fire an event when a lockout occurs. - Also Blacklist the IP
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function fireAttemptLockoutEvent(Request $request)
    {
        event(new Lockout($request));

        $blacklist = Firewall::blacklist(
            $request->ip(),
            cache($this->attemptCacheKey($request)) . ' failed login attempts',
            Carbon::now()->addDays(2)
        );

        if ($blacklist) {
            event(new LoginBlacklisted($blacklist));
        }
    }
}
