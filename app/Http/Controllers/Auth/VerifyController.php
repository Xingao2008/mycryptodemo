<?php

namespace App\Http\Controllers\Auth;

use App\Mail\OverSixtyFirst;
use App\Models\User;
use App\Models\Address;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use PragmaRX\Google2FA\Google2FA;
use Laracasts\Flash\Flash;
use App\Models\Verification;
use DateTime;
use App\Notifications\UserResetPassword;
use App\Classes\GBGSoapClient;

// use Webpatser\Countries\CountriesFacade\Countries;
// use Lecturize\Addresses\AddressesServiceProvider as AddressesServiceProvider;
// use Webpatser\Countries\CountriesServiceProvider;
// // use Lecturize\Addresses\Traits\HasAddresses as LecTraits;

class VerifyController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'status']);
    }

    public function verifyGBGAuthenticateSP(Request $request)
    {
        $user = Auth::user();
        $gbgClient = new GBGSoapClient();
        $dob = explode('/',$user->dob);
        if (isset($dob[1]) && $dob[1][0] == '0') {
            $dob[1] = $dob[1][1];
        }
        $data = [
            'personalDetails' => [
                // 'Title' => 'Mr',
                'Forename' => $user->first_name,
                'Surname' => $user->last_name,
                'Surname' => $user->last_name,
                // 'Gender' => 'Male',
                'DOBDay' => $dob[0]??'',
                'DOBMonth' => $dob[1]??'',
                'DOBYear' => (isset($dob[2]) ? $dob[2][2].$dob[2][3]:'')
            ],
            'currentAddress' => [
                'Country' => $user->address->country->name,
                'Street' => $user->address->street,
                'City' => $user->address->city,
                'ZipPostcode' => $user->address->post_code,
                'StateDistrict' => $user->address->state
            ],
            'contactDetails' => [
                'MobileTelephone' => $user->phone
            ]
        ];
        $gbgClient->sendAuthenticateSP($data);
    }
}
