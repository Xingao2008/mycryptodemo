<?php

namespace App\Http\Controllers\Auth;

use App\Mail\OverSixtyFirst;
use App\Models\User;
use App\Models\Address;
use App\Models\Version;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use PragmaRX\Google2FA\Google2FA;
use Laracasts\Flash\Flash;
use App\Models\Verification;
use DateTime;
use App\Notifications\UserResetPassword;
use App\Classes\GBGSoapClient;
use App\Models\TextVerification;

// use Webpatser\Countries\CountriesFacade\Countries;
// use Lecturize\Addresses\AddressesServiceProvider as AddressesServiceProvider;
// use Webpatser\Countries\CountriesServiceProvider;
// // use Lecturize\Addresses\Traits\HasAddresses as LecTraits;

class AccountController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Account Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the update to accounts
    |
    */


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'status']);
    }

    public function index()
    {
        $verification = Verification::firstOrNew(['user_id' => \Auth::user()->id]);

        if($verification->account == 'rejected') {
            flash('Sorry you are locked from editing your account, please contact support.')->error();
            return redirect()->route('verify');
        }

        
        $collection = \App\Models\Countries::all();
        $countries = $collection->filter(function ($value, $key) {
            if (in_array($value->id, config('countries.support_countries'))) {
                return $value;
            }
        });
        return view('private.account', compact('countries'))->with('status', null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        if ($request->has('country_calling_code') && $request->country_calling_code == '1') {
            $rules = [
                'first_name' => 'required|min:2',
                'middle_name' => 'sometimes',
                'last_name' => 'required|min:2',
                'phone' => [
                    'required',
                    Rule::phone()->country(['US']),
                    Rule::unique('users', 'phone')->ignore(\Auth::user()->id)
                ],
                'dob' => 'required|date_format:d/m/Y',
                'street' => 'sometimes|nullable',
                'city' => 'sometimes|nullable',
                'state' => 'sometimes|nullable',
                'country_id' => 'sometimes|nullable|min:1|max:3',
                'abn' => 'sometimes',
                'country_calling_code' => 'required',
                'GBGIdentityID' => 'sometimes|numeric|nullable'
            ];
        } else {
            $rules = [
                'first_name' => 'required|min:2',
                'middle_name' => 'sometimes',
                'last_name' => 'required|min:2',
                'phone' => [
                    'required',
                    Rule::phone()->country(['NZ', 'AU', 'GB', 'CA', 'JP'])->type('mobile'),
                    Rule::unique('users', 'phone')->ignore(\Auth::user()->id)
                ],
                'dob' => 'required|date_format:d/m/Y',
                'street' => 'sometimes|nullable',
                'city' => 'sometimes|nullable',
                'state' => 'sometimes|nullable',
                'country_id' => 'sometimes|nullable|min:1|max:3',
                'abn' => 'sometimes',
                'country_calling_code' => 'required',
                'GBGIdentityID' => 'sometimes|numeric|nullable'
            ];
        }
        $messages = [
            'phone.required' => 'Phone number must be a valid mobile number and does not contain invalid characters',
        ];

        if ($request->has('country_id') && $request->country_id == 36) {
            array_push($rules, ['post_code' => 'sometimes|digits:4|numeric|nullable']);
        } else {
            array_push($rules, ['post_code' => 'sometimes|numeric|nullable']);
        }
        
        $this->validate($request, $rules, $messages);
        
        // if (!starts_with(request('phone'), [0, 4, 2])) {
        //     flash('Must be an valid AU/NZ mobile starting with 0, 4, 2.')->error();
        //     return redirect('/account');
        // }
        //Not sure how can anyone pass through the validation above so Check date format again
        $checkDate = explode('/',$request->dob);
        if (strlen($checkDate[2]) != 4) {
            flash('Your year format in dob must be YYYY. Example: 1980')->error();
            return redirect()->route('account');
        }
        $date = date("Y-m-d",strtotime(str_replace('/', '-',$request->dob)));
        $from = new DateTime($date);
        $to   = new DateTime('today');
        $yrDiff = $from->diff($to)->y;
        if($yrDiff < 18) {
           
            flash('Sorry you must be 18 to use this site')->error();
            return redirect()->route('account');
        }

        $user = User::whereId(auth()->id())->firstOrFail();
        $verification = Verification::firstOrNew(['user_id' => $user->id]);


        if ($user->phone && request('phone') !== $user->phone) {
            if ($verification->sms == "approved") {
                Flash::message('For security reasons you cannot change your mobile number. Please contact support.');

                return redirect('/account');
            }
        }

        $textverification = TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();

        if ($textverification !== NULL) {
            $show_form = TRUE;
        }

        if (!is_null($request->street) && !is_null($request->city) && !is_null($request->post_code)) {

            $user->fill(request()->all());

            if (request('abn')) {
                $verification->abn = "pending";
            }

            if ($request->has('GBGIdentityID')) {
                $verification->GBGIdentityID = $request->input('GBGIdentityID');
            }

            $user->save();

            $verification->account = "approved";

            if (request('abn')) {
                $verification->abn = "pending";
            }

            $verification->save();

            \App\Models\Ip::create([
                'user_id' => $user->id,
                'ip' => request()->ip(),
                'event' => 'account details changed',
            ]);

            //get valid verion of address
            $current_ver = Version::where('model', '=', 'addresses')
                ->where('user_id', '=', $user->id)
                ->where('status', '=', 'verified')
                ->orderBy('created_at', 'desc')
                ->first();

            if (is_null($current_ver)) {
                $valid_version = 1;
            } else {
                $valid_version = $current_ver->version + 1;
            }

            $save_data = array(
                'street' => $request->street,
                'city' => $request->city,
                'state' => $request->state,
                'post_code' => $request->post_code,
                'country_id' => $request->country_id
            );

            $new_version = new Version;
            $new_version->model = 'addresses';
            $new_version->user_id = $user->id;
            $new_version->version = $valid_version;
            $new_version->model_id = $user->address->id ?? 0;
            $new_version->status = 'pending';
            $new_version->data = json_encode($save_data);
            $new_version->save();

            $dob =$request->dob;

            if ($valid_version === 1) {
                // new users, don't need to sms verification
                $address = Address::where('user_id', '=', $user->id)->first();
                if (!$address) {
                    $address = new Address;
                }
                $address->user_id = $user->id;
                $address->fill(request()->all());
                $address->save();

                $new_version->status = 'verified'; //1st enter set status verified
                $new_version->save();

                //send email if age over 60
                $dob = Carbon::createFromFormat('d/m/Y', $dob);
                if($dob->age >= 60) {
                    \Mail::send(new OverSixtyFirst($user));
                }

                return redirect()->route('account')->with('status', 'Account updated.');

            } else {

                $version['id'] = $new_version->id;
                $version['status'] = $new_version->status;

                return view('private.account.smsverify', compact(['version', 'show_form', 'dob']));
            }

        } else {
            flash('Please fulfill all the address detail')->error();

            return redirect('/account');
        }
    }

    public function tfa()
    {
        $user = User::findOrFail(auth()->id());
        $google2fa = new Google2FA();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl(
            'myCryptoWallet',
            $user->email,
            $user->tfa
        );
        $tfasecret = $user->tfa;

        return view('private.tfa', compact('google2fa_url', 'tfasecret'))->with('status', null);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function tfa_update(Request $request)
    {
        $user = User::findOrFail(auth()->id());
        $secret = $request->input('tfa');
        $google2fa = new Google2FA();

        if (!$secret) {
            return Redirect::route('tfa')->with('status', '<b>Failure</b> - Please enter the secret code generated on your Google Authenticator.');
        }

        $valid = $google2fa->verifyKey($user->tfa, $secret);

        if ($valid) {
            $verification = Verification::where('user_id', '=', $user->id)->first();

            $verification->tfa = "approved";
            $verification->save();

            $user->tfa_status = TRUE;
            $user->save();

            return Redirect::route('tfa')->with('status', '<b>Success!</b> Code was accepted.');
        } else {
            return Redirect::route('tfa')->with('status', '<b>Failure</b> - Code was incorrect.');
        }
    }

    /**
     * Display Identity verification form
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function identity_index(Request $request)
    {
        $collection = \App\Models\Countries::all();
        $countries = $collection->filter(function ($value, $key) {
            if (in_array($value->id, config('countries.support_countries'))) {
                return $value;
            }
        });
        $user = User::find(auth()->id());
        //check is the user fail 3 times before
        //disable for now
        // $attempts = \App\Models\ServiceLog::where('user_id', $user->id)->where('service_type', 'gbg_validation')->count();
        // if ($attempts > 2) {
        //     flash('You\'ve exceeded the number of attempts to get verified. Please email your ID documents (passport, driver\'s license and proof of address) to support@mycryptowallet.com.au')->error();
        //     return redirect()->back();
        // }
        return view('private.identity', compact('countries', 'user'))->with('status', null);
    }

    public function password()
    {
        return view('private.account.password');
    }

    public function passwordgo(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:8',
            'confirm_password' => 'required|min:8',
        ]);

        if ($request->password !== $request->confirm_password) {
            return Redirect::route('new_password')->with('status', 'Passwords do not match!');
        }

        $user = auth()->user();
        $user->password = \Hash::make($request->password);
        $user->save();

        $user->notify(new UserResetPassword());

        return Redirect::route('account')->with('status', 'Password was updated!');
    }

    public function settings()
    {
        return view('private.account.settings');
    }

    public function settingsUpdate()
    {
        $user = \Auth::user();

        $arr = $user->settings;

        $arr['withdraw_tfa'] = ($user->tfa_status) ? request()->get('withdraw_tfa', 0) : 0;
        $arr['default_currency'] = request()->get('default_currency', 'btc');
        $arr['default_fiat_currency'] = request()->get('default_fiat_currency', 'aud');

        if(\Auth::user()->can('admin.users.index')) {
            $arr['freshdesk_api_key'] = request()->get('freshdesk_api_key', null);
        }

        $user->settings = $arr;
        $user->save();

        flash('Settings updated');

        return redirect()->route('settings');
    }

    public function verifyGBGAuthenticateSP()
    {
        $gbgClient = new GBGSoapClient();
        $data = [
            'personalDetails' => [
                'Title' => 'Mr',
                'Forename' => 'barbara',
                'Surname' => 'miller',
                'Gender' => 'Male',
                'DOBDay' => '24',
                'DOBMonth' => '2',
                'DOBYear' => '1983'
            ],
            'currentAddress' => [
                'Country' => 'United States',
                'Street' => 'cimarron rd e',
                'City' => 'LOMBARD',
                'ZipPostcode' => '60148',
                'StateDistrict' => 'IL'
            ]
        ];
        $gbgClient->sendAuthenticateSP($data);
    }
}
