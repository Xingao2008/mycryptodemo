<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Models\Address;
use App\Models\User;
use App\Models\Balance;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Laracasts\Flash\Flash;
use App\Models\Emails;
use App\Mail\ConfirmEmail;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Http\Request;
use App\Models\Country;
use Cookie;
use App\Models\TextVerification;
use App\Models\Verification;
use App\Models\Ip;
use App\Notifications\IdVerificationCode;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

//    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'sometimes',
            'first_name' => 'required|string|max:255',
            'middle_name' => 'sometimes|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'status_code' => ''
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $confirmation_code = str_random(30);
        $google2fa = new Google2FA();

        $google2fa->generateSecretKey();
        $secretKey = $google2fa->generateSecretKey();
        /**init the default fiat currency */
        $arr = [];
        if (isset($data['country_id']) && $data['country_id'] == 554)
        {
            $arr['default_fiat_currency'] = 'nzd';
        } elseif (isset($data['country_id']) && ($data['country_id'] == 840 || $data['country_id'] == 826 || $data['country_id'] == 124)){
            $arr['default_fiat_currency'] = 'usd';
        } else {
            $arr['default_fiat_currency'] = 'aud';
        }

        $referral = \Cookie::get('referid');

        if($referral) {
            if(is_numeric($referral)) {
                $referral = $referral - 100000;
            } else { $referral = NULL; }
        } else { $referral = NULL; }

        $user = User::create([
            'name' => $data['first_name'] . " " . $data['middle_name'] . " " . $data['last_name'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status_code' => $confirmation_code,
            'tfa' => $secretKey,
            'settings' => $arr,
            'ubc' => 0,
            'refer_uid' => $referral,
        ]);

        $user->assignRole('user');

        \App\Models\Ip::create([
            'user_id'    => $user->id,
            'ip'      => request()->ip(),
            'event' => 'account created',
        ]);

        \App\Models\Address::updateOrCreate(
            [
                'user_id' => $user->id, 'country_id' => $data['country_id'] ?? 36
            ]
        );

        return $user;
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            Flash::message('No confirmation code found.');
            return redirect('/login');
        }

        $user = User::whereStatusCode($confirmation_code)->first();
        if ( ! $user)
        {
            Flash::message('Invalid or expired confirmation code.');
            return redirect('/login');
        }

        $user->status = 'active';
//        $user->status_code = null;
        $user->save();

        return redirect('/login')->with('loginEmail', $user->email);
    }

    public function resend($email) {
        $user = User::where('email', '=', $email)->first();

        if ( ! $user || $user->status_code == NULL || empty($user) || !isset($user) ) {
            return redirect('/login');
        }

        $email = $user->email;
        $status_code = $user->status_code;

        \Mail::to($email)->send(new ConfirmEmail($status_code));

        Emails::create([
            'user_id'    => $user->id,
            'to'      => $user->email,
            'subject' => 'Verify your email address',
        ]);

        Flash::message('Verification email has been resent.');
        return redirect('/login');

    }

    /**
     * Override the default registration controller
     */
    public function showRegistrationForm(Request $request) 
    {
        $tryUserCountry = geoip($request->ip());
        if (isset($tryUserCountry->country)) {
            $userCountry = $tryUserCountry->country;
        } else {
            //set a default
            $userCountry = 'Australia';
        }
        $countries = Country::select('id','name','display','available','currency_code')
                    ->where('display',1)
                    ->orderBy('available','DESC')
                    ->orderBy('name','ASC')
                    ->get();
        return view('auth.register', compact('countries','userCountry'));
    }

    // roy's new function
    public function getStep(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|email|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => $validator->errors()));
        }

        $user = User::where('email', '=', $request->email)->first();
        if (is_null($user)) {
            return response()->json(array('state' => 200, 'step' => 1));
        } else {
            $steps = Verification::where('user_id', '=', $user->id)->first();
            return response()->json(array('state' => 200, 'step' => $steps->register_flow));
        }
    }

    // roy's new function
    public function register(Request $request)
    {
//        dd($request->all());
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => 'The email has been taken'));
        }

        // check phone duplication
        $phones = User::where('id' ,'>' ,0)->pluck('phone')->toArray();
        foreach($phones as $phone) {
            if ($phone == $request->phone) {
                return response()->json(array('state' => 500, 'msg' => 'The phone number has been taken'));

            }
        }

        // google 2FA
        $google2fa = new Google2FA();
        $google2fa->generateSecretKey();
        $secretKey = $google2fa->generateSecretKey();

        // referal
        $referral = \Cookie::get('referid');
        if($referral) {
            if(is_numeric($referral)) {
                $referral = $referral - 100000;
            } else { $referral = NULL; }
        } else { $referral = NULL; }

        //set up default settings
        $arr = [];
        if (isset($request->country_id) && $request->country_id == 64)
        {
            $arr['default_fiat_currency'] = 'nzd';
        } elseif (isset($request->country_id) && $request->country_id == 1){
            $arr['default_fiat_currency'] = 'usd';
        } else {
            $arr['default_fiat_currency'] = 'aud';
        }

        // create new user
        $user = new User;
        $user->name = $request->first_name.' '.$request->last_name;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->country_calling_code = $request->phone_prefix;
        $user->phone = $request->phone;
        $user->password = str_random(10); // have to have this, table mark password as not null
        $user->status_code = str_random(30);
        $user->ubc = 0;
        $user->tfa = $secretKey;
        $user->refer_uid = $referral;
        $user->settings = $arr;
        $user->save();

        $user->assignRole('user');

        Ip::create([
            'user_id'    => $user->id,
            'ip'      => request()->ip(),
            'event' => 'account created',
        ]);

        $ver = new Verification;
        $ver->user_id = $user->id;
        $ver->register_flow = 'mobile';
        $ver->save();

        //match dialing code with area code
        $areaCode = 36; //Australia by default
        $prefixs = config('countries.phone_prefix');
        foreach ($prefixs as $key => $prefix) {
            if ($key == $request->phone_prefix) {
                $areaCode = $prefixs[$key];
            }
        }

        Address::create([
            'user_id' => $user->id,
            'country_id' => $areaCode
        ]);

        $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

        $user->notify(new IdVerificationCode($code));

        TextVerification::create([
            'user_id' => $user->id,
            'to' => $user->phone,
            'type' => 'verify',
            'code' => $code,
        ]);

        return response()->json(array('state' => 200, 'msg' => 'Success! Please wait up to 30 seconds for your verification code.'));
    }

    // roy's new function
    public function sendSMS(Request $request) {
        $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
        $user = User::where('email', '=', $request->email)->first();
        $user->notify(new IdVerificationCode($code));

        TextVerification::create([
            'user_id' => $user->id,
            'to' => $user->phone,
            'type' => 'verify',
            'code' => $code,
        ]);

        return response()->json(array('state' => 200, 'msg' => 'SMS has been sent to '.$user->phone));
    }

    // roy's new function
    public function verifyRegisterMobile(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'code' => 'required|string|max:6|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => $validator->errors()));
        }

        $user = User::where('email', '=', $request->email)->first();

        $textverification = TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();
        $verification = Verification::where('user_id', '=', $user->id)->first();
        $user_entered = $request->code;

        if ($textverification->attempts == 5) {
            return response()->json(array('state' => 500, 'msg' => 'Sorry, you\'ve exceeded the maximum number of allowed attempts. Please resend the SMS.'));
        }

        if ($user_entered == $textverification->code) {
            $textverification->verified = TRUE;
            $textverification->save();

            $verification->sms = 'approved';
            $verification->register_flow = 'details';
            $verification->save();
            return response()->json(array('state' => 200, 'msg' => 'The SMS verification code is correct!'));
        } else {
            $textverification->attempts++;
            $textverification->save();
            $error = 'Incorrect code, please try again. Note that a maximum of 5 attempts per SMS are allowed. (attempt ' . $textverification->attempts . ' of 5)';
            return response()->json(array('state' => 500, 'msg' => $error));
        }
    }

    // roy's new function
    public function addProfile(Request $request)
    {
        \Validator::extend('olderThan', function($attribute, $value, $parameters)
        {
            $minAge = ( ! empty($parameters)) ? (int) $parameters[0] : 18;
            return (new \DateTime)->diff(new \DateTime($value))->y >= $minAge;
        });

        $validator = \Validator::make($request->all(), [
            'birthday' => 'required|olderThan: 18'
        ]);

        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => 'The birthday field is required and you have to be over 18 years old'));
        }

        $user = User::where('email', '=', $request->email)->orderBy('created_at', 'desc')->first();
        $user->dob = is_null($request->birthday) ? '' : $request->birthday;
        $userSave = $user->save();

        $addr = Address::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->first();
        $addr->street = is_null($request->street) ? '' : $request->street;
        $addr->city = is_null($request->city) ? '' : $request->city;
        $addr->state = is_null($request->state) ? '' : $request->state;
        $addr->post_code = is_null($request->postCode) ? '' : $request->postCode;
        $addr->country_id = is_null($request->countryId) ? '36' : $request->countryId;
        $addrSave = $addr->save();

        $verification = Verification::where('user_id', '=', $user->id)->first();
        $verification->register_flow = 'finish';
        $verification->save();

        if (!$addrSave && !$userSave) {
            return response()->json(array('state' => 500, 'msg' => 'There was a problem, please try again or skip'));
        } else {
            event(new UserRegistered($user));

            return response()->json(array('state' => 200, 'msg' => 'Verification email has been sent, please also check your spam/junk folder'));
        }
    }

    // roy's new function
    public function setPassword(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'password' => 'required|min:8|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,32}$/',
            'confirm_password' => 'required|min:8|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,32}$/',
        ]);

        if ($validator->fails()) {
            return response()->json(array('state' => 500, 'msg' => 'Password update fails'));
        }

        if ($request->password !== $request->confirm_password) {
            return response()->json(array('state' => 500, 'msg' => 'Password doesn\'t match'));
        }

        $user = User::where('email', '=', $request->email)->first();
        $user->password = $request->password;  // its hashed somewhere else, dont hash again.
        $user->save();

        $verification = Verification::where('user_id', '=', $user->id)->first();
        $verification->register_flow = 'verify';
        $verification->save();

        return response()->json(array('state' => 200, 'msg' => 'Password set successfully!'));
    }

    /**
     * New authentication screen with flow
     */
    public function showRegistrationFlow(Request $request) 
    {
        return view('auth.register_process');
    }
}