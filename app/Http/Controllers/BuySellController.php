<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Services\BuyCurrencyService;
use App\Services\SellCurrencyService;
use App\Models\ExternalPrices;
use App\Models\Balance;
use App\Models\UsdExchange;
use App\Models\Setting;
use App\Models\Ledger;


class BuySellController extends Controller
{
    protected $buyCurrencyService;
    protected $sellCurrencyService;

    /**
     * Create a new controller instance.
     *
     * @param BuyCurrencyService $buyCurrencyService
     * @param SellCurrencyService $sellCurrencyService
     *
     * @return void
     */
    public function __construct(BuyCurrencyService $buyCurrencyService, SellCurrencyService $sellCurrencyService)
    {
        $this->buyCurrencyService = $buyCurrencyService;
        $this->sellCurrencyService = $sellCurrencyService;

        //$this->middleware('auth');
        $this->middleware(['auth', 'status']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function buy_index($coin = null)
    {
        if (setting('buys_active')) {
            return view('private.buy')
                ->with('coin_balances', Balance::coinBalances(\Auth::user()))
                ->with('price', prices('total'))
                ->with('price_change', ExternalPrices::price_changes())
                ->with('funds', Balance::funds(\Auth::user()))
                ->with('coin', $coin);
        } else {
            \Flash::error('Buy functionalities are currently disabled.');
            return redirect('/dashboard');
        }
    }

    public function buy_process(Request $request)
    {
        try {
            $this->buyCurrencyService->handle($request);
        } catch (\Exception $e) {
            \Flash::error($e->getMessage());
            return back()->withInput();
        }

        return redirect('/buy/confirm')->with(compact('type', 'baseline_price', 'amount', 'quantity'));
    }

    public function buy_confirm()
    {
        $type = \Session::get('type');
        $baseline_price = \Session::get('baseline_price');
        $amount = \Session::get('amount');
        $quantity = \Session::get('quantity');
        $currency = \Session::get('currency');
        $user = Auth::user();

        \App\Models\Ip::create([
            'user_id' => $user->id,
            'ip' => request()->ip(),
            'event' => 'crypto buy',
        ]);

        return view('private.buy-confirm', compact('type', 'baseline_price', 'amount', 'quantity', 'currency'))
            ->with('funds', Balance::funds())
            ->with('bccomp_result', ExternalPrices::bccomp_results());
    }

    public function sell_index($coin = null)
    {
        if (setting('sells_active')) {
            return view('private.sell')
                ->with('coin_balances', Balance::coinBalances(\Auth::user()))
                ->with('price', prices('total'))
                ->with('price_change', ExternalPrices::price_changes())
                ->with('funds', Balance::funds(\Auth::user()))
                ->with('coin', $coin);
        } else {
            \Flash::error('Sell functionalities are currently disabled.');
            return redirect('/dashboard');
        }
    }

    public function sell_process(Request $request)
    {
        try {
            $this->sellCurrencyService->handle($request);
        } catch (\Exception $e) {
            \Flash::error($e->getMessage());
            return back()->withInput();
        }

        return redirect('/sell/confirm')->with(compact('type', 'baseline_price', 'amount', 'quantity'));
    }

    public function sell_confirm()
    {
        $type = \Session::get('type');
        $baseline_price = \Session::get('baseline_price');
        $amount = \Session::get('amount');
        $quantity = \Session::get('quantity');
        $currency = \Session::get('currency');


        \App\Models\Ip::create([
            'user_id' => \Auth::user()->id,
            'ip' => request()->ip(),
            'event' => 'crypto sell',
        ]);

        return view('private.sell-confirm', compact('type', 'baseline_price', 'amount', 'quantity', 'currency'))
            ->with('funds', Balance::funds())
            ->with('bccomp_result', ExternalPrices::bccomp_results());
    }

    /**
     * Dashboard for user to convert their currency
     */
    public function getCurrencyConvert()
    {
        $user = Auth::user();
        $currencyRate = [];
        foreach (config('currencies.fiatCurrencies') as $c) {
            $rate =  UsdExchange::where('currency', $c)->orderBy('created_at', 'DESC')->first();
            $currencyRate[$c] = $rate->value ?? 1;
        }
       
        return view('private.currency')
                ->with('coin_balances', Balance::coinBalances(\Auth::user()))
                ->with('currency_rate', $currencyRate)
                ->with('funds', Balance::funds(\Auth::user()))
               ;
    }

    public function postCurrencyConvert(Request $request)
    { 
        $validator = \Validator::make(request()->all(), [
            'inputCurrencyFrom' => 'required',
            'inputCurrencyFromValue' => 'required|numeric',
            'inputCurrencyTo' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->to('/currency')->withErrors($validator)->withInput();
        }
        $coinBalance = Balance::coinBalances(\Auth::user());
        $limit_balance = $coinBalance[$request->inputCurrencyFrom];
        if ($limit_balance < $request->inputCurrencyFromValue) {
            return redirect()->back()->withErrors('Amount must be more than wallets')->withInput();
        }
        if ($request->inputCurrencyFromValue < config('fee.fiat_convert.min_convert_fee')) {
            return redirect()->back()->withErrors('Amount must be more than the minimum transfer amount')->withInput();
        }
        $getConvertedAmount = convertFiatCurrency($request->inputCurrencyFrom, $request->inputCurrencyTo, $request->inputCurrencyFromValue);
       
        \DB::beginTransaction();
        try {
            $fromBalance = Balance::where('user_id', Auth::user()->id)
                            ->where('currency', $request->inputCurrencyFrom)
                            ->first();
            $fromBalance->balance = $fromBalance->balance - $request->inputCurrencyFromValue;
            $fromBalance->save();

            $toBalance = Balance::where('user_id', Auth::user()->id)
                                    ->where('currency', $request->inputCurrencyTo)
                                    ->first();

            $old_balance = $toBalance->balance;

            
            $getConvertedAmount = $getConvertedAmount - bcmul($getConvertedAmount, self::calculateServiceFee(), 18) ;
            $toBalance->balance = $toBalance->balance + $getConvertedAmount;
            $toBalance->save();
        } catch (\Exception $e)
        {
            \DB::rollBack();
            return 'There was an issue converting your currency, please try again or contact support. ERR: DB_FAIL';

        }
        \DB::commit();

        $ledger = new Ledger;
        $ledger->user_to = Auth::user()->id;
        $ledger->user_from = 0;
        $ledger->address_from = $request->inputCurrencyFrom;
        $ledger->address_to = $request->inputCurrencyTo;
        $ledger->transaction_type = "fiat_convert";
        $ledger->currency = $request->inputCurrencyTo;
        $ledger->amount = $getConvertedAmount ;
        $ledger->fee = bcmul($getConvertedAmount, self::calculateServiceFee(), 18);
        $ledger->oldbalance = $old_balance;
        $ledger->fee_type = "fiat_convert_fee";
        $ledger->save();

        flash('Success! Currency has been converted')->success();
        return redirect()->to('currency');
        
    }

    /**
     * Return the calculated service fee base on the admin setting
     */
    public static function calculateServiceFee()
    {
        $fee = Setting::where('id','fiat_convert_fee')->first();
        // $feeRange = config('fee.exchange');
        return $fee->value / 100;
    }
}



