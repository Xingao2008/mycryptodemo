<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Models\ExternalPrices;
use App\Models\Balance;


class DepositController extends Controller
{

    public function index($path)
    {
        $allCurrencies = config('currencies.allAvailableCurrencies');

        if (in_array($path, config('currencies.fiatCurrencies'))) {
            if (\App\Models\Verification::where('user_id', '=', Auth::user()->id)->first()->bank !== "approved") {
                Flash::message('Please add your bank details before accessing this page - go to Account and click Bank Details.');

                return redirect('/wallets');
            }
        }

        if (in_array($path, $allCurrencies)) {
            return view('private.deposit')->with('path', $path);
        }

        return redirect('wallets/deposit/aud');
    }

    public function ethCheckForDeposit()
    {
        if (setting('deposits_active')) {
            $user = \Auth::user();
            $user_wallet = \App\Models\Wallet::where('user_id', '=', $user->id)->where('type', '=', 'eth')->first();
            $message = FALSE;
            if (!$user_wallet) {
                Flash::message('Error: no wallet generated or found');

                return redirect('/wallets');
            }

            $api_url = 'https://cd.fairdigital.com.au/api/transactions.php?coin=eth&address=' . $user_wallet->address;
            $eth_data = json_decode(file_get_contents($api_url));

            if (!$eth_data) {
                Flash::message('No Ethereum deposits found, try again later or check your deposit TXID.');

                return redirect('/wallets');
            }



            $ignored_txids = \App\Models\IgnoredTransactions::where('currency','=','eth')->pluck('txid')->toArray();

            foreach ($eth_data as $key => $value) {
                if ($value->type == "deposit" && !in_array($value->hash, $ignored_txids)) {

                    $txid = $value->hash;
                    $to = $value->to;
                    $confirmations = $value->confirmations;
                    $amount = $value->amount;
                    $from = $value->from;
                    $fee = $value->fee;

                    if ($confirmations < 30) {
                        $status = "unconfirmed";
                    } else {
                        $status = "confirmed";
                    }

                    $deposit = \App\Models\CryptoDeposit::where('transaction_id', '=', $txid)->where('to', '=', $to)->where('type', '=', 'eth')->first();

                    if ($deposit == null) {
                        $new_deposit = new \App\Models\CryptoDeposit;
                        $new_deposit->user_id = $user->id;
                        $new_deposit->to = $to;
                        $new_deposit->from = $from;
                        $new_deposit->type = 'eth';
                        $new_deposit->transaction_id = $txid;
                        $new_deposit->amount = $amount;
                        $new_deposit->fees = $fee;
                        $new_deposit->confirmations = $confirmations;
                        $new_deposit->status = $status;
                        $new_deposit->save();

                        Flash::message('New deposit discovered (TXID ' . $txid . '). Please check again shortly for the number of confirmations.');
                        $message = TRUE;

                    } else {
                        $deposit_status = $deposit->status;
                        $deposit->confirmations = $confirmations;
                        if ($deposit->status == "unconfirmed") {
                            $deposit->status = $status;
                            if ($status == "confirmed") {
                                Flash::message('Deposit with TXID of ' . $txid . ') is pending final transfer, please check again in ~5 minutes ');
                                $message = TRUE;
                            };
                        }
                        $deposit->save();

                        if ($confirmations < 30) {
                            Flash::message('Deposit with TXID of ' . $txid . ') is pending ' . $confirmations . ' confirmations of a required 60.');
                            $message = TRUE;
                        }
                    }

                }

            }
            if ($message == FALSE) {
                Flash::message('Transactions found. If you are expecting a deposit, please wait around ~5 minutes for it to appear.');
            }
            return redirect('/wallets');
        } else {
            Flash::message('Deposits functionality is currently disabled.');
            return redirect('/wallets');
        }
    }

    public function pwrCheckForDeposit()
    {
        if (setting('deposits_active')) {
            $user = \Auth::user();
            $user_wallet = \App\Models\Wallet::where('user_id', '=', $user->id)->where('type', '=', 'eth')->first();
            $message = FALSE;
            if (!$user_wallet) {
                Flash::message('Error: no wallet generated or found');

                return redirect('/wallets');
            }

            $api_url = 'https://cd.fairdigital.com.au/api/transactions.php?coin=pwr&address=' . $user_wallet->address;
            $eth_data = json_decode(file_get_contents($api_url));

            if (!$eth_data) {
                Flash::message('No PowerLedger deposits found, try again later or check your deposit TXID.');

                return redirect('/wallets');
            }

            $ignored_txids = array(
                '0xf7061f2e8b529a91e004787a34ea1ad4be833e1f3b879263d49debb33f28d6b2',
                '0xa94e937143b6332dc32d228a1ebb207f1486130357323fc7a13d713493c6aa2a',
                '0xfd43a8d95bc36e86b3967b1dd8a963e1f6c97bb9a40b7a02295dd362de2cf284',
            );

            foreach ($eth_data as $key => $value) {
                if ($value->type == "deposit" && !in_array($value->hash, $ignored_txids)) {

                    $txid = $value->hash;
                    $to = $value->to;
                    $confirmations = $value->confirmations;
                    $amount = $value->amount;
                    $from = $value->from;
                    $fee = $value->fee;

                    if ($confirmations < 30) {
                        $status = "unconfirmed";
                    } else {
                        $status = "confirmed";
                    }

                    $deposit = \App\Models\CryptoDeposit::where('transaction_id', '=', $txid)->where('to', '=', $to)->where('type', '=', 'pwr')->first();

                    if ($deposit == null) {
                        $new_deposit = new \App\Models\CryptoDeposit;
                        $new_deposit->user_id = $user->id;
                        $new_deposit->to = $to;
                        $new_deposit->from = $from;
                        $new_deposit->type = 'pwr';
                        $new_deposit->transaction_id = $txid;
                        $new_deposit->amount = $amount;
                        $new_deposit->fees = $fee;
                        $new_deposit->confirmations = $confirmations;
                        $new_deposit->status = $status;
                        $new_deposit->save();

                        Flash::message('New deposit discovered (TXID ' . $txid . '). Please check again shortly for the number of confirmations.');
                        $message = TRUE;

                    } else {
                        $deposit_status = $deposit->status;
                        $deposit->confirmations = $confirmations;
                        if ($deposit->status == "unconfirmed") {
                            $deposit->status = $status;
                            if ($status == "confirmed") {
                                Flash::message('Deposit with TXID of ' . $txid . ') is pending final transfer, please check again in ~5 minutes ');
                                $message = TRUE;
                            };
                        }
                        $deposit->save();

                        if ($confirmations < 30) {
                            Flash::message('Deposit with TXID of ' . $txid . ') is pending ' . $confirmations . ' confirmations of a required 60.');
                            $message = TRUE;
                        }
                    }

                }

            }
            if ($message == FALSE) {
                Flash::message('Transactions found. If you are expecting a deposit, please wait around ~5 minutes for it to appear.');
            }
            return redirect('/wallets');
        } else {
            Flash::message('Deposits functionality is currently disabled.');
            return redirect('/wallets');
        }
    }
}
