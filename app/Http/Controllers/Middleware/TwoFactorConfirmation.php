<?php

namespace App\Http\Controllers\Middleware;


use App\Http\Controllers\Controller;

class TwoFactorConfirmation extends Controller
{

    public function auth(){
        return view('admin.tfa.auth');
    }

    public function postAuth() {
        $this->validate(request(), ['tfa' => 'required']);

        $user = \Auth::user();

        if($user->check_tfa(request('tfa'))) {
            session()->flash('2fa.confirmation');
            return redirect()->to( session('2fa.intended') );
        }

        flash('Invalid TFA')->error();
        return redirect()->back();
    }
}