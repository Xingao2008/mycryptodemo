<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Classes\OrderContract;
use Illuminate\Validation\Rule;
use Validator;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Balance;
use App\Models\Setting;
use DB;
use App\Events\TradeHistoryEvent;
use App\Events\ExchangePublicUpdateEvent;
use App\Jobs\ProcessExchangeOrder;
use App\Classes\MyWallet;

class ExchangeController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    
    /**
     * Entry point for the exchage page.
     */
    public function index()
    {
        // return view
    }

    public function postOrder(Request $request)
    {
        $validator = Validator::make($request->all(),[
            // 'order_type' => [
            //     'required'
            //     // ,Rule::in(['limit', 'market']),
            // ],
            'order_type' => 'required',
            'currency' => 'required|max:3',
            'currency_pair' => 'required|max:3',
            'volume' => 'required|numeric',
            'market_type' => 'required',
            // 'market_type' => [
            //     'required'
            //     // ,Rule::in(['buy', 'sell']),
            // ],
            'price' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return respondWithJson(false, 'Price missing or Validation error', $validator->messages(), 400);
        }
        
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            return respondWithJson(false, 'Exchange is temporarily closed', '', 400);
        }

        $newOrder = new OrderContract();
        $orderData = [
            'user_id' => $this->guard()->user()->id,
            'order_type' => $request->order_type,
            'currency' => $request->currency,
            'currency_pair' => $request->currency_pair,
            'volume' => $request->volume,
            'market_type' => $request->market_type,
            'price' => $request->price,
            'remaining' => $request->volume,
            'status' => 'active'
        ];
        $newOrder->createContract($orderData);
        if (!$newOrder->checkAccountHasEnoughFund()) {
            return respondWithJson(false, 'Insufficient fund', '', 400);
        }
        //for MVP, make sure we got enough order to fullfill the market order. if not, reject it
        if ($request->order_type == 'market' && !$newOrder->hasEnoughOrders()) {
            return respondWithJson(false, 'Not enough orders to fill the market order', '', 400);
        }
        //enough money and enough orders to full fill so create orders
        $newOrder->generateContract();

        ProcessExchangeOrder::dispatch($newOrder, Auth::user(), false)->onQueue('ProcessExchangeOrder');
        // if ($newOrder->startBuySell()) {
        //     //broadcasting new update
        //$this->generateEventContent($request);
        return respondWithJson(true, 'Order process finished', '', 200);
        // }
        
    }


    /**
     * Return trade history data
     * API for the buy sell order book
     * 
     * Can call this API via /?currency=btc&start_date=
     * Available option:
     * currency: our currently available coin type ( btc, xrp,ltc...)
     * start_date: date format (2018-02-27)
     * end_date: date format (2018-02-27)
     * order_type: (limit or market)
     * market_type: (buy or sell)
     * volume: Only sorting 'Desc' or 'Asc
     * price: Only sorting 'Desc' or 'Asc
     * order_status: 'active','complete','cancel','suspend'
     */
    public function getTradeHistory(Request $request)
    {

        $results = $this->returnAllTradeHistory($request);
        return respondWithJson(true, '', $results, 200);
    }

    protected function returnAllTradeHistory(Request $request)
    {
        /**
         * setup some default parameter
         */
        $limit = 50;
        $defaultSort = 'ASC';
        //check is there any custom sorting sending through
        if (!$request->has('price')) {
            if ($request->market_type == 'buy') {
                $defaultSort = 'DESC';
            }
        }
        
        $query = Order::query();
        $query->when(request('currency', false), function ($q, $currency) { 
            return $q->where('currency', $currency);
        });
        $query->when(request('currency_pair', false), function ($q, $currency) {
            return $q->where('currency_pair', $currency);
        });
        $query->when(request('start_date', false), function ($q, $start_date) { 
            return $q->where('created_at', '>=', $start_date);
        });
        $query->when(request('end_date', false), function ($q, $end_date) { 
            return $q->where('created_at', '<=', $end_date);
        });
        $query->when(request('order_type', false), function ($q, $order_type) { 
            return $q->where('order_type', $order_type);
        });
        $query->when(request('market_type', false), function ($q, $market_type) { 
            return $q->where('market_type', $market_type);
        });
        $query->when(request('order_status', false), function ($q, $order_status) { 
            return $q->where('status', $order_status);
        });
        //this should also just asc or desc
        $query->when(request('price', false), function ($q, $defaultSort) {
            return $q->orderBy('price', $defaultSort);
        });
        //this should be just asc or desc
        $query->when(request('volume', false), function ($q, $volume) { 
            return $q->orderBy('volume', $volume);
        });

        $query->limit($limit);
        return $query->get();
    }

    /**
     * Generate broadcasting data for socket
     */
    protected function generateEventContent(Request $request)
    {
        $data = [];
        //$publicTradeHistory = $this->returnAllTradeHistory($request);
        //getPriceHistory
        //$allOrders = Order::getMarketPrice(request('currency','btc'), request('currency_pair','aud'));
        //getCurrentPrice
        //$yesterdayClosingPrice = Order::getYesterdayClosePrice(request('currency','btc'), request('currency_pair','aud'));
        //most 5 recent orders
        $myRecentOrders = $this->returnMyRecentOrders();
        $myTradeHistory = $this->returnMyTradeHistory($request);
        $data = [
            //'priceHistory' => $allOrders,
            //'yesterdayClosingPrice' => $yesterdayClosingPrice,
            'myRecentOrders' => $myRecentOrders,
            'myTradeHistory' => $myTradeHistory,
            //'publicTradeHistory' => $publicTradeHistory
        ];
        // event(new TradeHistoryEvent(Auth::user()));
        // event(new ExchangePublicUpdateEvent(true));
    }

    /** 
    * Available option:
    * Can call this API via /?currency=btc&currency_pair=aud
    * 
    */
    public function getPriceHistory(Request $request)
    {
        $allOrders = Order::getMarketPrice(request('currency','btc'), request('currency_pair','aud'));
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $allOrders
        ], 200,
            [],
            JSON_NUMERIC_CHECK);
    }

    /**
     * get current market price
     *
     * @ void
     * @ return json
     */
    public function getCurrentPrice(Request $request)
    {
        $yesterdayClosingSellPrice = Order::getYesterdayCloseSellPrice(request('currency','btc'), request('currency_pair','aud'));
        $yesterdayClosingBuyPrice = Order::getYesterdayCloseBuyPrice(request('currency','btc'), request('currency_pair','aud'));
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => [
                'close_sell_price' => $yesterdayClosingSellPrice[0] ?? '', 
                'close_buy_price' => $yesterdayClosingBuyPrice[0] ?? ''
                ]
        ], 200,
        [],
        JSON_NUMERIC_CHECK);
    }

    /**
     * Return all the recent trade history for the login user
     * Can call this API via /?currency=btc&start_date=
     * Available option:
     * currency: our currently available coin type ( btc, xrp,ltc...)
     * currency_pair: our currently available coin type ( btc, xrp,ltc...)
     * start_date: date format (2018-02-27)
     * end_date: date format (2018-02-27)
     * order_type: (limit or market)
     * market_type: (buy or sell)
     * status
     * volume: Only sorting 'Desc' or 'Asc
     * price: Only sorting 'Desc' or 'Asc
     */
    public function getMyTradeHistory(Request $request)
    {
        $results = $this->returnMyTradeHistory($request);
        return response()->json([
            'success' => true,
            'message' => '',
            'data' => $results
        ], 200,
        [],
        JSON_NUMERIC_CHECK);
    }

    protected function returnMyTradeHistory(Request $request)
    {
        $limit = 50;
        $query = Transaction::query();
        $query->when(request('currency', false), function ($q, $currency) { 
            return $q->where('orders.currency', $currency);
        });
        $query->when(request('currency_pair', false), function ($q, $currency) { 
            return $q->where('orders.currency', $currency);
        });
        $query->when(request('start_date', false), function ($q, $start_date) { 
            return $q->where('transactions.updated_at', '>=', $start_date);
        });
        $query->when(request('end_date', false), function ($q, $end_date) { 
            return $q->where('transactions.updated_at', '<=', $end_date);
        });
        $query->when(request('order_type', false), function ($q, $order_type) { 
            return $q->where('orders.order_type', $order_type);
        });
        $query->when(request('market_type', false), function ($q, $market_type) { 
            return $q->where('orders.market_type', $market_type);
        });
        //this should be just asc or desc
        $query->when(request('volume', false), function ($q, $volume) { 
            return $q->orderBy('transactions.volume', $volume);
        });
        //this should also just asc or desc
        $query->when(request('price', false), function ($q, $price) { 
            return $q->orderBy('orders.price', $price);
        });
        $query->join('orders', 'orders.id', '=', 'transactions.order_id');
        $query->select('transactions.id as tid', 'transactions.volume', 'orders.market_type', 'transactions.currency','orders.price', 'transactions.created_at', 'transactions.status as status');
//        $query->where('orders.user_id', Auth::user()->id);
        $query->limit($limit);
        return $query->get();
    }

    /**
     * Return the most recent 5 orders
     */
    public function getMyRecentOrders(Request $request)
    {
        $orders = $this->returnMyRecentOrders($request->limit);
        return response()->json([
                'success' => true,
                'message' => '',
                'data' => $orders
            ], 200,
            [],
            JSON_NUMERIC_CHECK);  
    }

    protected function returnMyRecentOrders($limit)
    {
        return Order::where('user_id', Auth::user()->id)
                ->select('id','order_type','currency','status','currency_pair','volume','market_type','price','updated_at')
                ->orderBy('updated_at','desc')
                ->limit($limit)
                ->get();
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }

    /**
     * Return all the data for the new wallet widgets
     */
    public function getExchangeWalletData()
    {
        $currencies = config('crypto.available_currency');
        $data = [];
        $aud = Balance::where('user_id', '=', Auth::user()->id)->where('currency','aud')->select('currency', 'balance')->first();
        //$data['aud']['price'] = get_crypto_balance($cur);
        $data = [
            'price' => self::price(),
            'price24' => self::price24(),
            'bccomp_results' => self::bccomp_results(),
            'price_changes' => self::price_changes()
        ];
        $data['aud']['user_crypto_balance'] = $aud->balance + 0;
        foreach ($currencies as $cur) {
            $data[$cur]['user_crypto_balance'] = get_crypto_balance($cur) + 0;
            $data[$cur]['user_crpyto_value'] = get_user_crypto_value($cur);

        }
        
        return respondWithJson(true, '', $data, 200);
    }

    /**
     * The following are the helper functions to calculate the wallet data
     * May be move it to somewhere?
     */
    protected static function price()
    {
        $currencies = config('crypto.available_currency');
        // for aud price
        foreach ($currencies as $cur) {
            $price[$cur]['bid'] = Order::where('currency', '=', $cur)
                                ->where('currency_pair', '=', 'aud')
                                ->where('status','complete')
                                ->where('market_type','buy')
                                ->latest()
                                ->first();
            $price[$cur]['ask'] = Order::where('currency', '=', $cur)
                                ->where('currency_pair', '=', 'aud')
                                ->where('status','complete')
                                ->where('market_type','sell')
                                ->latest()
                                ->first();
        }

        foreach ($price as $key => $value) {
            if (!isset($price[$key]['bid']['volume'])) {
                continue;
            }
            $bidPrice = $price[$key]['bid']['volume'] * $price[$key]['bid']['price']; 
            $price[$key]['bid'] = bcdiv($bidPrice - OrderContract::calculateServiceFee($price[$key]['bid']['volume'], Auth::user()->id), $price[$key]['bid']['volume'], 8) + 0;

            if (!isset($price[$key]['ask']['volume'])) {
                continue;
            }
            $askPrice = $price[$key]['ask']['price'];
            $price[$key]['ask'] = $askPrice;
        }

        $rtn['aud'] = $price;

        // for btc price
        foreach ($currencies as $cur) {
            if ($cur === 'btc') continue;

            $priceBTC[$cur]['bid'] = Order::where('currency', '=', $cur)
                ->where('currency_pair', '=', 'btc')
                ->where('status','complete')
                ->where('market_type','buy')
                ->latest()
                ->first();
            $priceBTC[$cur]['ask'] = Order::where('currency', '=', $cur)
                ->where('currency_pair', '=', 'btc')
                ->where('status','complete')
                ->where('market_type','sell')
                ->latest()
                ->first();
        }

        $priceBTC['aud']['bid'] = Order::where('currency', '=', 'aud')
            ->where('currency_pair', '=', 'btc')
            ->where('status','complete')
            ->where('market_type','buy')
            ->latest()
            ->first();
        $priceBTC['aud']['ask'] = Order::where('currency', '=', 'aud')
            ->where('currency_pair', '=', 'btc')
            ->where('status','complete')
            ->where('market_type','sell')
            ->latest()
            ->first();

        foreach ($priceBTC as $key => $value) {
            if (!isset($priceBTC[$key]['bid']['volume'])) {
                continue;
            }
            $bidPrice = $priceBTC[$key]['bid']['volume'] * $priceBTC[$key]['bid']['price'];
            $priceBTC[$key]['bid'] = bcdiv($bidPrice - OrderContract::calculateServiceFee($priceBTC[$key]['bid']['volume'], Auth::user()->id), $priceBTC[$key]['bid']['volume'], 8) + 0;

            if (!isset($priceBTC[$key]['ask']['volume'])) {
                continue;
            }
            $askPrice = $priceBTC[$key]['ask']['price'];
            $priceBTC[$key]['ask'] = $askPrice;
        }

        $rtn['btc'] = $priceBTC;

        return $rtn;
    }

    protected static function price24()
    {
        $currencies = config('crypto.available_currency');
        //for aud price
        foreach ($currencies as $cur) {
            $price_24[$cur]['bid'] = Order::where('currency', '=', $cur)
                                ->where('currency_pair', '=', 'aud')
                                ->where('status','complete')
                                ->where('market_type','buy')
                                ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24[$cur]['ask'] = Order::where('currency', '=', $cur)
                                ->where('currency_pair', '=', 'aud')
                                ->where('status','complete')
                                ->where('market_type','sell')
                                ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        }
        foreach ($price_24 as $key => $value) {
            if (!isset($price_24[$key]['bid']['volume'])) {
                continue;
            }
            $bidPrice = $price_24[$key]['bid']['volume'] * $price_24[$key]['bid']['price']; 
            $price_24[$key]['bid'] = bcdiv($bidPrice - OrderContract::calculateServiceFee($price_24[$key]['bid']['volume'], Auth::user()->id), $price_24[$key]['bid']['volume'], 8) + 0;

            if (!isset($price_24[$key]['ask']['volume'])) {
                continue;
            }
            $askPrice = $price_24[$key]['ask']['price'];
            $price_24[$key]['ask'] = $askPrice;
        }

        $rtn['aud'] = $price_24;

        //for btc price
        foreach ($currencies as $cur) {
            $price_24BTC[$cur]['bid'] = Order::where('currency', '=', $cur)
                ->where('currency_pair', '=', 'btc')
                ->where('status','complete')
                ->where('market_type','buy')
                ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24BTC[$cur]['ask'] = Order::where('currency', '=', $cur)
                ->where('currency_pair', '=', 'btc')
                ->where('status','complete')
                ->where('market_type','sell')
                ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        }
        $price_24BTC['aud']['bid'] = Order::where('currency', '=', 'aud')
            ->where('currency_pair', '=', 'btc')
            ->where('status','complete')
            ->where('market_type','buy')
            ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $price_24BTC['aud']['ask'] = Order::where('currency', '=', 'aud')
            ->where('currency_pair', '=', 'btc')
            ->where('status','complete')
            ->where('market_type','sell')
            ->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();

        foreach ($price_24BTC as $key => $value) {
            if (!isset($price_24BTC[$key]['bid']['volume'])) {
                continue;
            }
            $bidPrice = $price_24BTC[$key]['bid']['volume'] * $price_24BTC[$key]['bid']['price'];
            $price_24BTC[$key]['bid'] = bcdiv($bidPrice - OrderContract::calculateServiceFee($price_24BTC[$key]['bid']['volume'], Auth::user()->id), $price_24BTC[$key]['bid']['volume'], 8) + 0;

            if (!isset($price_24BTC[$key]['ask']['volume'])) {
                continue;
            }
            $askPrice = $price_24BTC[$key]['ask']['price'];
            $price_24BTC[$key]['ask'] = $askPrice;
        }

        $rtn['btc'] = $price_24BTC;

        return $rtn;
    }

    protected static function bccomp_results()
    {
        
        $price = self::price();
        $price_24 = self::price24();

        $bccomp_result['aud']['btc']['bid'] = bccomp($price['aud']['btc']['bid'], $price_24['aud']['btc']['bid'], 2);
        $bccomp_result['aud']['btc']['ask'] = bccomp($price['aud']['btc']['ask'], $price_24['aud']['btc']['ask'], 2);
        $bccomp_result['aud']['eth']['bid'] = bccomp($price['aud']['eth']['bid'], $price_24['aud']['eth']['bid'], 2);
        $bccomp_result['aud']['eth']['ask'] = bccomp($price['aud']['eth']['ask'], $price_24['aud']['eth']['ask'], 2);
        $bccomp_result['aud']['ltc']['bid'] = bccomp($price['aud']['ltc']['bid'], $price_24['aud']['ltc']['bid'], 2);
        $bccomp_result['aud']['ltc']['ask'] = bccomp($price['aud']['ltc']['ask'], $price_24['aud']['ltc']['ask'], 2);
        $bccomp_result['aud']['xrp']['bid'] = bccomp($price['aud']['xrp']['bid'], $price_24['aud']['xrp']['bid'], 2);
        $bccomp_result['aud']['xrp']['ask'] = bccomp($price['aud']['xrp']['ask'], $price_24['aud']['xrp']['ask'], 2);
        $bccomp_result['aud']['pwr']['bid'] = bccomp($price['aud']['pwr']['bid'], $price_24['aud']['pwr']['bid'], 2);
        $bccomp_result['aud']['pwr']['ask'] = bccomp($price['aud']['pwr']['ask'], $price_24['aud']['pwr']['ask'], 2);

        $bccomp_result['btc']['aud']['bid'] = bccomp($price['btc']['aud']['bid'], $price_24['btc']['aud']['bid'], 2);
        $bccomp_result['btc']['aud']['ask'] = bccomp($price['btc']['aud']['ask'], $price_24['btc']['aud']['ask'], 2);
        $bccomp_result['btc']['eth']['bid'] = bccomp($price['btc']['eth']['bid'], $price_24['btc']['eth']['bid'], 2);
        $bccomp_result['btc']['eth']['ask'] = bccomp($price['btc']['eth']['ask'], $price_24['btc']['eth']['ask'], 2);
        $bccomp_result['btc']['ltc']['bid'] = bccomp($price['btc']['ltc']['bid'], $price_24['btc']['ltc']['bid'], 2);
        $bccomp_result['btc']['ltc']['ask'] = bccomp($price['btc']['ltc']['ask'], $price_24['btc']['ltc']['ask'], 2);
        $bccomp_result['btc']['xrp']['bid'] = bccomp($price['btc']['xrp']['bid'], $price_24['btc']['xrp']['bid'], 2);
        $bccomp_result['btc']['xrp']['ask'] = bccomp($price['btc']['xrp']['ask'], $price_24['btc']['xrp']['ask'], 2);
        $bccomp_result['btc']['pwr']['bid'] = bccomp($price['btc']['pwr']['bid'], $price_24['btc']['pwr']['bid'], 2);
        $bccomp_result['btc']['pwr']['ask'] = bccomp($price['btc']['pwr']['ask'], $price_24['btc']['pwr']['ask'], 2);

        return $bccomp_result;
    }

    protected static function price_changes()
    {
        return cache()->remember('price_changes', 1, function () {
            $price = self::price();
            $price_24 = self::price24();
            $price_change = [];
            foreach ($price['aud'] as $key => $value) {
                if (isset($price_24['aud'][$key]['bid'])) {
                    $sub_result = bcsub($value['bid'], $price_24['aud'][$key]['bid'], 2);
                    $div_result = @bcdiv($sub_result, $value['bid'], 2);
                    $price_change['aud'][$key] = $div_result * 100;
                } else {
                    $price_change['aud'][$key] = 0;
                }
                
            }

            foreach ($price['btc'] as $key => $value) {
                if (isset($price_24['btc'][$key]['bid'])) {
                    $sub_result1 = bcsub($value['bid'], $price_24['btc'][$key]['bid'], 2);
                    $div_result1 = @bcdiv($sub_result1, $value['bid'], 2);
                    $price_change['btc'][$key] = $div_result1 * 100;
                } else {
                    $price_change['btc'][$key] = 0;
                }
            }

            return $price_change;
        });
    }
    

    public function patchCancelOrder(Request $request, $id = 0)
    {
        if ($id == 0) {
            return respondWithJson(false, 'Invalid order id', '', 400);
        }
        $order = new OrderContract();
        $order->getContract($id);
        if (!$order->isOrderOwner(Auth::user()->id)) {
            return respondWithJson(false, 'You dont have permission to edit this order', '', 400);
        }
        if ($order->status !== 'active') {
            return respondWithJson(false, 'Not active order', '', 400);
        }
        ProcessExchangeOrder::dispatch($order, Auth::user(), true)->onQueue('ProcessExchangeOrder');
        // if ($order->refundOrder() === false) {
        //     return respondWithJson(false, 'Invalid order', '', 400);
        // } else {
        //     return respondWithJson(true, 'Your order has been cancelled.', '', 200);
        // }

    }
}