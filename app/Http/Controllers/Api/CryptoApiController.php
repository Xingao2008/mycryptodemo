<?php
namespace App\Http\Controllers\Api;

use GuzzleHttp\Client as Client;
use App\Http\Controllers\Controller;

class CryptoApiController extends Controller
{
    private $header;
    private $client;

    public function __construct()
    {
        $this->header = 'https://localhost/api';
        $this->client = new Client();
    }

    public function login()
    {
        $api_url = $this->header.'/login';
        $res = $this->client->request('POST', $api_url, [
            'form_params' => [
                'email' => 'mattwithoos@gmail.com',
                'password' => 'G4pubFtTmxaUA9zY'
            ]
        ]);

        if (200 === $res->getStatusCode()) {
            header('Content-type: application/json');
            echo $res->getBody();
        } else {
            return false;
        }
    }

    /**
     * @param $exchangeId
     *
     * @return list of $symbols
     */
    public function getSymbol($exchange_id)
    {
        $api_url = $this->header.
            'v1/symbols/?'.
            'filter_symbol_id='.$exchange_id.
            '&apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    /**
     * @param $symbol
     * @param $from
     * @param $to
     * @param $limit
     *
     * design for chart
     *
     * @return $res
     * trade information with time, price, volumn, and buy/sell
     */
    public function getTradeHistory($symbol,$from, $to, $limit = 100)
    {
        $api_url = $this->header.
            'v1/trades/'.$symbol.'/history?'.
            'time_start='.$from.
            '&time_end='.$to.
            '&limit='.$limit.
            '&apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    /**
     * @param $symbol
     * @param $limit
     *
     * design for buy/sell book
     *
     * @return $res
     * trade information with time, price, volumn, and buy/sell
     */
    public function getTrade($symbol, $limit = 100)
    {
        $api_url = $this->header.
            'v1/trades/'.$symbol.'/latest?'.
            'limit='.$limit.
            '&apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    /**
     * @param $asset_base
     * @param $asset_quote
     *
     * @return $res
     * the price of coin/dollar
     */
    public function getPrice($asset_base, $asset_quote)
    {
        $api_url = $this->header.
            'v1/exchangerate/'.$asset_base.
            '/'.$asset_quote.
            '/?apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    /**
     * @param void
     *
     * @return $res
     * the list of all periods
     */
    public function getPeriod()
    {
        $api_url = $this->header.
            'v1/ohlcv/periods/?apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    /**
     * @param $symbol
     * @param $period_id
     * @param $time_start
     * @param $time_end
     * @param $limit
     *
     * @return $res
     * the trading history of candal stick.
     */
    public function getOHLCV($symbol, $period_id, $time_start, $time_end, $limit = 100)
    {
        $api_url = $this->header.
            'v1/ohlcv/'.$symbol.
            '/history?period_id='.$period_id.
            '&time_start='.$time_start.
            '&time_end='.$time_end.
            '&limit='.$limit.
            '&apikey='.$this->key;

        $res = $this->client->get($api_url);
        $this->runApi($res);
    }

    public function runApi($res)
    {
        if (200 === $res->getStatusCode()) {
            header('Content-type: application/json');
            echo $res->getBody();
        } else {
            return false;
        }
    }
}