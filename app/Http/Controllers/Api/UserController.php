<?php

namespace App\Http\Controllers\Api;

use App\Models\Address;
use App\Models\Bank;
use App\Http\Resources\BalanceResource;
use App\Http\Resources\UserResource;
use App\Services\BuyCurrencyService;
use App\Services\SellCurrencyService;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    protected $buyCurrencyService;
    protected $sellCurrencyService;

    public function __construct(BuyCurrencyService $buyCurrencyService, SellCurrencyService $sellCurrencyService)
    {
        $this->buyCurrencyService = $buyCurrencyService;
        $this->sellCurrencyService = $sellCurrencyService;
    }

    public function show(Request $request)
    {
        $user = User::with('balances')->find($request->user()->id);
        return new UserResource($user);
    }

    public function store(Request $request)
    {
        $user = $request->user();

        $request->validate([
            'email' => [Rule::unique('users')->ignore($user->id)],
            'country_id' => 'exists:countries,id',
            'bank_bsb' => 'numeric',
            'bank_number' => 'numeric',
        ]);

        $address = Address::firstOrCreate(['user_id' => $user->id]);
        $address->update($request->all());

        $bank = Bank::firstOrCreate(['user_id' => $user->id]);
        $bank->update($request->all());

        $user->update($request->all());

        return new UserResource($user);
    }

    public function wallets(Request $request)
    {
        return BalanceResource::collection($request->user()->balances);
    }

    // @TODO may be moved to separate controller
    public function buy(Request $request)
    {
        try {
            $this->buyCurrencyService->handle($request);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'code' => 'buy_process_failed',
                'message' => $e->getMessage(),
            ], 500);
        }

        return response()->json([
            'success' => true,
        ]);
    }

    // @TODO may be moved to separate controller
    public function sell(Request $request)
    {
        try {
            $this->sellCurrencyService->handle($request);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'code' => 'sell_process_failed',
                'message' => $e->getMessage(),
            ], 500);
        }

        return response()->json([
            'success' => true,
        ]);
    }
}
