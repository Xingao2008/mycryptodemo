<?php
/**
 * Created by PhpStorm.
 * User: xingao
 * Date: 3/4/18
 * Time: 12:36 PM
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\EmailList;
use Illuminate\Http\Request;

class EmailsController extends Controller
{
    public function import(Request $request)
    {
        $emailsValidate = array();
        $rightEmails = preg_replace('/\s+/', '', strtolower($request->emails)); // remove whitespace and lowcase all character
        $emails = explode(';', $rightEmails);
        $countOfEmails = count($emails);

        foreach($emails as $key => $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailsValidate[$key]['email'] = $email;
                $emailsValidate[$key]['list_name'] = $request->list_name;
            }
        }
        $countOfValidateEmails = count($emailsValidate);

        EmailList::insert($emailsValidate); //use it for insert large chunk of data

        return response()->json([
            'success' => true,
            'message' => 'Recipent list saved',
            'data' => [
                'countOfEmails' => $countOfEmails,
                'countOfValidateEmails' => $countOfValidateEmails

            ],
        ], 200,
            [],
            JSON_NUMERIC_CHECK);
    }

    public function getList()
    {
        return EmailList::getList();
    }

    public function sendBulkEmail(Request $request) {
        $list_name = $request->title;
        $subject = $request->subject;
        $content = $request->body;
        $senders = EmailList::getRecipient($list_name);
        $data = [];

        foreach ($senders as $sender) {
            $data['sender'] = $sender['email'];
            $data['subject'] = $subject;

            \Mail::send('email.bulk-empty', $data, function ($message) use ($data) {
                $message->from('noreply@fairdigital.com.au');
                $message->to($data['sender']);
                $message->subject('myCryptoWallet: ' . strtoupper($data['subject']));
            });
        }

//        return response()->json(['message' => 'Request completed']);
    }
}