<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Verification;

class VerifyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        $verification = \DB::table('verifications')->where('user_id', '=', $user->id)->first();

        foreach ($verification as $key => $value) {

            if ($value == 'incomplete') {
                $status[$key] = '<i class="fa fa-minus" aria-hidden="true"></i> incomplete';
            } elseif ($value == 'pending') {
                $status[$key] = '<i class="fa fa-clock-o" aria-hidden="true"></i> pending';
            } elseif ($value == 'approved') {
                $status[$key] = '<i class="fa fa-check" aria-hidden="true"></i> complete/approved';
            } elseif ($value == 'rejected') {
                $status[$key] = '<i class="fa fa-times" aria-hidden="true"></i> rejected';
            } else {
                $status[$key] = '<i class="fa fa-times" aria-hidden="true"></i> error - contact support';
            }
        }
        if (($verification->id_bank == "approved") && ($verification->id_passport == "approved") && ($verification->id_drivers == "approved")) {
            $status['id'] = '<i class="fa fa-check" aria-hidden="true"></i> complete/approved';
        } else {
            $status['id'] = '<i class="fa fa-minus" aria-hidden="true"></i> incomplete';
        }

        if ($verification->account !== "approved") {
            $status['account_completed'] = FALSE;
        } else {
            $status['account_completed'] = TRUE;
        }

        $status['overall'] = tier();

        return view('private.verify', compact(['verification', 'status'], 'coin_balances'));
    }

    public function selfie()
    {
        $user = \Auth::user();
        $verification = Verification::where('user_id', '=', $user->id)->first();

        if ($verification->selfie != 'incomplete') {
            flash('You have already added your selfie')->error();
            return redirect()->to('verify');
        }

        if ($verification->bank != 'approved') {
            flash('Please add your bank account first')->error();
            return redirect()->to('verify');
        }

        if ($verification->id_drivers != 'approved') {
            flash('Please verify your id first')->error();
            return redirect()->to('verify');
        }

        \Assets::add('fileupload');

        return view('private.selfie');
    }

    public function postSelfie(Request $request)
    {
        $validation = validator(request()->all(), ['file' => 'image|max:3000']);

        if ($validation->fails()) {
            return response()->make($validation->getMessageBag()->first(), 400);
        }

        $file = request()->file('file');

        $user = \Auth::user();
        $verification = Verification::where('user_id', '=', $user->id)->first();

        $user->addMedia($file)->toMediaCollection('selfie');

        $verification->selfie = 'pending';
        $verification->save();

        return response()->json([]);
    }

    public function accountProgress()
    {
        $user = \Auth::user();
        $verification = \DB::table('verifications')->where('user_id', '=', $user->id)->first();
        if ($verification->account !== "approved") {
            $status['account_completed'] = FALSE;
        } else {
            $status['account_completed'] = TRUE;
        }
        $status['overall'] = tier();
        return view('private.account-progress', compact(['verification', 'status'], 'coin_balances'));
    }
}
