<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MarketplaceController extends Controller
{
    public function index($path)
    {
        if($path == "btc" || $path == "ltc" || $path == "eth" || $path == "xrp") {
          return view('private.marketplace', ['path' => $path]);
        }
        return redirect('marketplace/btc');
    }
}
