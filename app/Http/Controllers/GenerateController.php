<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use App\Models\Ripple;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use DB;
use App\Classes\MyWallet;

class GenerateController extends Controller
{
    public function index(Request $request, $path)
    {
        $request->session()->get('btc-wallet-request-sent');

        $user = \Auth::user();
        $myWallet = new MyWallet();
        $myWallet->userId = $user->id;
        $myWallet->path = $path;

        if($path == "btc" || $path == "ltc") {
          if($path == "btc") {$fullword = "Bitcoin";}
          if($path == "ltc") {$fullword = "Litecoin";}

          $wallet = Wallet::where('user_id', '=', $user->id)->where('type', '=', $path)->first();

          if (!$request->session()->get('btc-wallet-request-sent') && empty($wallet)){
            $obj = $myWallet->generateWallet();
            Flash::message('Success! Your '.$fullword.' address is: '.$obj);
            $request->session()->put('btc-wallet-request-sent', false);
            return redirect('wallets');
          } else {
            Flash::message('You already have a '.$fullword.' address!');
            $request->session()->put('btc-wallet-request-sent', false);
            return redirect('wallets');
          }
        }

        if($path == "eth") {

          $wallet = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'eth')->first();

          if ($wallet == null) {
            $myWallet->ubc = $user->ubc;
            $obj = $myWallet->generateWallet();
            Flash::message('Success! Your Ethereum address is: '.$obj);
            return redirect('wallets');
          } else {
            Flash::message('You already have a Ethereum address!');

            return redirect('wallets');
          }
        }

        if($path == "xrp") {
          $wallet = Wallet::where('user_id', '=', $user->id)->where('type', '=', 'xrp')->first();
          if ($wallet == null) {
            $msg = $myWallet->createRippleWallet();
            Flash::message($msg);
            return redirect('wallets');
          } else {
            Flash::message('You already have a Ripple address!');
            return redirect('wallets');
          }
        }
        return redirect('wallets');
    }
}
