<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Verification;
use App\Notifications\VerificationUpdate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\GBGSoapClient;

class VerificationsController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');
        return view('admin.verifications.index');
    }

    public function edit(Request $request, $id)
    {
        $verification = Verification::firstOrCreate(['user_id' => $id]);
        return view('admin.verifications.edit')->with('verification', $verification);
    }

    public function postEdit($id)
    {
        $verification = Verification::firstOrCreate(['user_id' => $id]);

        if ($success = $verification->update(request()->all())) {
            $user = User::where('id', '=', $verification->user_id)->first();
            $user->notify(new VerificationUpdate());
        }

        flash('User Verification Updated')->success();
        return redirect()->route('admin.verifications');
    }

    public function approve(Request $request,$id)
    {
        $verification = Verification::firstOrCreate(['user_id' => $id]);
        if ($request->has('verify')) {
            $msg = 'admin manually approved from GBG response at ' . Carbon::now()->toDateTimeString();
        } else {
            $msg = 'admin manually approved at ' . Carbon::now()->toDateTimeString();
        }
        $data = [
            'account' => 'approved',
            'id_bank' => 'approved',
            'id_drivers' => 'approved',
            'id_passport' => 'approved',
            'id_drivers_notes' => $msg
        ];

        if ($verification->update($data)) {
            $user = User::findOrFail($verification->user_id);

            $user->verified = 1;
            $user->save();
            $user->notify(new VerificationUpdate());

        }

        flash('User Verified')->success();
        return redirect()->route('admin.verifications');
    }

    public function reject(Request $request, $id) {
        $verification = Verification::firstOrCreate(['user_id' => $id]);
        if ($request->has('verify')) {
            $msg = 'admin manually rejected from GBG response at ' . Carbon::now()->toDateTimeString();
        } else {
            $msg = 'admin manually rejected at ' . Carbon::now()->toDateTimeString();
        }
        $data = [
            'account' => 'incomplete',
            'id_bank' => 'incomplete',
            'id_drivers' => 'incomplete',
            'id_passport' => 'incomplete',
            'id_drivers_notes' => $msg,
        ];

        if ($verification->update($data)) {
            $user = User::findOrFail($verification->user_id);

            $user->verified = 0;
            $user->save();
            //$user->notify(new VerificationUpdate());
        }

        flash('User Verification Rejected')->success();
        return redirect()->route('admin.verifications');
    }

    public function manualVerify($user_id) {
        $user = User::find($user_id);
        return view('admin.verifications.manual', compact('user'));
    }


    public function postManualVerify(Request $request, $user_id) {
        
        $dob = explode('/', $request->dob);
        
        $gbg = new GBGSoapClient();
        $gbg->setProfile($request->profile);
        
        $data = [
            'personalDetails' => [
                'Forename' => $request->first_name,
                'MiddleName' => $request->middle_name,
                'Surname' => $request->last_name,
                'DOBDay' => $dob[0],
                'DOBMonth' => $dob[1],
                'DOBYear' => $dob[2][0].$dob[2][1]
            ],
            'currentAddress' => [
                'Country' => $request->country,
                'Street' => $request->street,
                'City' => $request->city,
                'ZipPostcode' => $request->ZipPostcode,
                'StateDistrict' => $request->ZipPostcode
            ],
            'contactDetails' => [
                'MobileTelephone' => $request->phone
            ],
            'GBGIdentityID'=> ($request->has('idCard')?$request->idCard : ''),
        ];
        if ($request->has('passport_number') && !empty($request->passport_number) &&$request->has('passport_day')) {
            array_push($data, [
                'passport' => [
                    'Number'=>$request->passport_Number,
                    'ExpiryDay'=>$request->passport_ExpiryDay,
                    'ExpiryMonth'=>$request->passport_ExpiryMonth,
                    'ExpiryYear'=>$request->passport_ExpiryYear,
                    'CountryOfOrigin'=>$request->passport_CountryOfOrigin,
                    'IssueDay'=>$request->passport_IssueDay,
                    'IssueMonth'=>$request->passport_IssueMonth,
                    'IssueYear'=>$request->passport_IssueYear,
                    'ShortPassportNumber'=>$request->passport_ShortPassportNumber,

                ]
            ]);
        }
        if ($request->has('eid_line1') && !empty($request->eid_line1) &&$request->has('eid_day')) {
            array_push($data, [
                'eid' => [
                    'Line1'=>$request->eid_line1,
                    'Line2'=>$request->eid_line2,
                    'Line3'=>$request->eid_line3,
                    'ExpiryDay'=>$request->eid_day,
                    'ExpiryMonth'=>$request->eid_month,
                    'ExpiryYear'=>$request->eid_year,
                    'CountryOfNationality'=>$request->eid_CountryOfNationality,
                    'CountryOfIssue'=>$request->eid_CountryOfIssue

                ]
            ]);
        }
        $return = $gbg->sendAuthenticateSP($data);
        $log = new \App\Models\ServiceLog();
        $log->user_id = $user_id;
        $log->service_type = 'gbg_validation';
        $log->request = json_encode($data);
        $log->response = json_encode($return);
        $log->save();
        return $return;
    }
    
}
