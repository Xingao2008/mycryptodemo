<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PoliPayApi;
use App\Http\Controllers\Controller;
use App\Models\PoliPayTransaction;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\FraudLog;

class PoliPayController extends AdminBaseController
{

    private $polipay;

    public function __construct()
    {
        $this->polipay = new PoliPayApi();
    }

    public function index()
    {
        $transactions = PoliPayTransaction::orderBy('created_at', 'DESC')->get();

        \Assets::group('admin')->add('datatables');

        return view()->make('admin.polipay.index')->with('transactions', $transactions)->with('statuses', $this->polipay->getStatuses());
    }

    public function edit($id)
    {
        $transaction = PoliPayTransaction::findOrFail($id);

        \Assets::group('admin')->add('select2');

        return view()->make('admin.polipay.edit')
            ->with('transaction', $transaction)
            ->with('users', User::all())
            ->with('statuses', $this->polipay->getStatuses());
    }

    public function postEdit($id)
    {
        $transaction = PoliPayTransaction::findOrFail($id);

        $transaction->fill(request()->except('_token'));
        $transaction->save();

        flash('Transaction Updated')->success();

        return redirect()->to('admin/polipay');
    }

    public function dataTables()
    {
        $pn = PoliPayTransaction::getTableName();
        $un = User::getTableName();

        $query = PoliPayTransaction::
                join($un, "$un.id", '=', "$pn.user_id", 'left')
                ->selectRaw("$un.name as name, $pn.*");


        return DataTables::of($query)
            ->editColumn('name', function ($transaction) {
                if (!$transaction->name) {
                    return 'No User Associated';
                }
                return '<a href="' . url()->route('admin.users.edit', $transaction->user_id) . '">' . $transaction->name . '</a>';
            })
            ->editColumn('status', function ($transaction) {
                return $this->polipay->getStatuses()[$transaction->status];
            })
            ->rawColumns(['name'])
            ->make(true);
    }

    public function deposit()
    {
        $frauds = FraudLog::all();

        $reps = DB::table('users as u')
            ->join('poli_pay_transactions as p', 'u.id', '=', 'p.user_id')
            ->where('p.admin_status', false)
            ->where('p.status', '=', 'completed')
            ->get();

        $arr = array();
        foreach ($reps as $rep) {
            $arr[$rep->user_id][] = $rep;
        }

        return view('admin.polipay.deposit')->with('reps', $arr)->with('frauds', $frauds);
    }

    public function remove(Request $request)
    {
        $transaction = PoliPayTransaction::where('id', '=', $request->id)->first();
        $transaction->status = 'fraud';  // reset status only, admin_status still keep as false.

        $user = DB::table('users as u')
            ->select('u.id', 'u.name', 'u.email', 'u.dob', 'u.phone', 'u.ubc', 'addr.street', 'addr.city', 'addr.state', 'addr.post_code', 'addr.country_id')
            ->leftjoin('addresses as addr', 'u.id', '=', 'addr.user_id')
            ->where('u.id', '=', $transaction->user_id)
            ->first();

        $fraud = new FraudLog;
        $fraud->username = $user->name;
        $fraud->email = $user->email;
        $fraud->dob = $user->dob;
        $fraud->phone = $user->phone;
        $fraud->ubc = $user->ubc;
        $fraud->street = is_null($user->street) ? '' : $user->street;
        $fraud->city = is_null($user->city) ? '' : $user->city;
        $fraud->state = is_null($user->state) ? '' : $user->state;
        $fraud->post_code = is_null($user->post_code) ? '' : $user->post_code;
        $fraud->country_id = is_null($user->country_id) ? 0 : $user->country_id;
        $fraud->poli_transaction_id = $transaction->id;

        $saveFraud = $fraud->save();
        $savePoliFraud = $transaction->save();

        if ($saveFraud && $savePoliFraud) {
            flash('Polipayment mark as fraud')->error();
        } else {
            flash('Polipayment failed')->error();
        }

        return redirect()->to('admin/polipay-deposit');
    }

    public function approve(Request $request)
    {
        $transaction = PoliPayTransaction::where('id', '=', $request->id)->first();
        $transaction->admin_status = true;

        if ($transaction->save()) {
            flash('Polipayment approved')->success();
        } else {
            flash('Polipayment failed')->error();
        }

        return redirect()->to('admin/polipay-deposit');
    }
}