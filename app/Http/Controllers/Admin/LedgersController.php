<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ledger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LedgersController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');

        return view('admin.ledgers.index', compact('ledgers'));
    }

    public function dataTables() {
        return \DataTables::of(Ledger::query())->make();
    }

    public function csv() {
        $csvExporter = new \Laracsv\Export();

        $ledger = Ledger::all();

        $csvExporter->build($ledger, [
            'reference_id',
            'sender.name',
            'receiver.name',
            'address_to',
            'address_from',
            'transaction_type',
            'currency',
            'amount',
            'transaction_id',
            'fee',
            'fee_type',
            'notes',
            'created_at',
            'oldbalance'
        ])->download();
    }
}
