<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends AdminBaseController
{
    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index', ['roles' => $roles]);
    }

    public function edit($id)
    {
        if ($id == 2) {
            flash('Role "user" cannot be edited at this time.')->error();
            return redirect()->to('admin/roles');
        }
        $role = Role::findOrFail($id);

        \Assets::group('admin')->add('icheck');

        return view('admin.roles.edit', ['role' => $role]);
    }

    public function postEdit($id)
    {
        if ($id == 2) {
            flash('Role "user" cannot be edited at this time.')->error();
            return redirect()->to('admin/roles');
        }
        $role = Role::where('id', $id)->first();

        $role->name = str_slug(request()->get('name'));
        $role->save();

        $tmp_permissions = request()->except(['name', '_token']);
        $permissions = [];
        foreach ($tmp_permissions as $key => $permission) {
            $permissions[] = str_replace('_', '.', $key);
        }

        $role->syncPermissions($permissions);

        flash('Role updated')->success();
        return redirect()->to('admin/roles');

    }


    public function create()
    {
        \Assets::group('admin')->add('icheck');

        return view('admin.roles.create');
    }

    public function postCreate()
    {
        if (!request()->get('name', false)) {
            flash('Please enter a name')->error();
            return redirect()->back()->withInput();
        }

        $role = new Role();

        $role->name = str_slug(request()->get('name'));
        $role->save();

        $tmp_permissions = request()->except(['name', '_token']);
        $permissions = [];
        foreach ($tmp_permissions as $key => $permission) {
            $permissions[] = str_replace('_', '.', $key);
        }

        $role->syncPermissions($permissions);

        flash('Role created')->success();
        return redirect()->to('admin/roles');
    }
}