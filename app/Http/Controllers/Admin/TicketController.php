<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\User;
use \Freshdesk\Api;


class TicketController extends Controller
{
    public function create($id = null)
    {
        $apiKey = \Auth::user()->get_setting('freshdesk_api_key', false);
        if(!$apiKey) {
            flash('Please add your Freshdesk API key')->error();

            return redirect()->route('settings');
        }

        $user = null;
        if ($id) {
            $user = User::find($id);
        }

        return view()->make('admin.tickets.create')->with('user', $user);
    }

    public function postCreate()
    {
        $this->validate(request(), [
            'user_id' => 'required|exists:users,id',
            'subject' => 'required',
            'body' => 'required',
            'priority' => 'required'
        ]);

        $email_config_id = config('freshdesk.email_config_id', 0);

        if(!$email_config_id) {
            $emails = cache()->remember('freshdesk_email_configs', 1440, function () {
                $api = new Api(config('freshdesk.api_key'), config('freshdesk.domain'));
                return $api->emailConfigs()->all();
            });

            $email_config_id = $emails[0]['id'];
        }

        $user = User::whereId(request('user_id'))->firstOrFail();

        $data = [
            'description' => request('body'),
            //'description_text' => strip_tags(request('body')),
            'email' => $user->email,
            'name' => $user->name,
            'priority' => (int)request('priority'),
            'subject' => request('subject'),
            //'status' => 2,
            'email_config_id' => (int)$email_config_id,
            //'cc_emails' => [\Auth::user()->email]
        ];

        try {
            $api = new Api(\Auth::user()->get_setting('freshdesk_api_key', false), config('freshdesk.domain'));
            $r = $api->tickets->createOutboundEmail($data);
        } catch (\Exception $e) {
            flash($e->getRequestException()->getMessage())->error();
            return redirect()->route('admin.ticket.create', request()->get('user_id'))->withInput();
        }

        flash('Ticket Created')->success();
        return redirect()->route('admin.users.edit', request()->get('user_id'));
    }
}