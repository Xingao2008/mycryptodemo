<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;

class FailedTransactionsController extends AdminBaseController
{
    public function index()
    {
        $failed_deposits = \App\Models\CryptoDeposit::where('status', '=', 'failed')->orWhere('status', '=', 'processing')->orWhere('status', '=', 'started')->get();
        $failed_withdrawals = \App\Models\CryptoWithdraw::where('status', '=', 'failed')->orWhere('status', '=', 'processing')->orWhere('status', '=', 'started')->get();

        return view('admin.failed-transactions.index', compact('failed_deposits', 'failed_withdrawals'));
    }

    public function removeWithdrawal($id)
    {
      $withdrawal_record = \App\Models\CryptoWithdraw::where('id', '=', $id)->first();

      if($withdrawal_record->status == "failed" || $withdrawal_record->status == "processing" || $withdrawal_record->status == "started") {
        $amount = $withdrawal_record->amount;
        $user_id = $withdrawal_record->user_id;
        $currency = $withdrawal_record->type;

        if($withdrawal_record->transaction_id) {
          $txid = $withdrawal_record->transaction_id;
        } else {
          $txid = "none";
        }
        $ledger_record = \App\Models\Ledger::where('reference_id', '=', 'w'.$id)->first();
        $user_balance = \App\Models\Balance::where('user_id', '=', $user_id)->where('currency','=',$currency)->first();
        \DB::beginTransaction();
        try {
          if($ledger_record) {
            $fee = bcsub($ledger_record->amount,$withdrawal_record->amount, 16);
          } else {
            $fee = 0;
          }

          $ledger = new \App\Models\Ledger;
          $ledger->reference_id = 'del'.$withdrawal_record->id;
          $ledger->user_to = $user_id;
          $ledger->user_from = 0;
          $ledger->address_to = $withdrawal_record->to;
          $ledger->transaction_type = "deleted_failed_withdrawal";
          $ledger->currency = $currency;
          $ledger->amount = $amount;
          $ledger->oldbalance = $user_balance->balance;
          $ledger->address_from = $withdrawal_record->from;
          $ledger->transaction_id = $txid;
          $ledger->fee = $fee;
          $ledger->fee_type = "failed_attempt_fee";
          $ledger->save();

          if($ledger_record) {
            \App\Models\Ledger::destroy($ledger_record->id);
          }

          $user_balance->balance = bcadd($user_balance->balance, $amount, 16);
          $user_balance->save();

          \App\Models\CryptoWithdraw::destroy($withdrawal_record->id);

        } catch (\Exception $e)
        {
            \DB::rollBack();
            throw $e;
        }
        \DB::commit();

        return redirect()->route('admin.failed-transactions')->with('changes-saved', true);
      } else {

        return redirect()->route('admin.failed-transactions')->with('failed-change', true);
      }
    }
}
