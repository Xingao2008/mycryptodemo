<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\WithdrawRemoval;
use App\Models\FiatWithdraw;
use App\Models\User;
use App\Models\Address;
use Illuminate\Http\Request;

class WithdrawalsApprovalController extends AdminBaseController
{
    public function index()
    {
        $withdraws = FiatWithdraw::with(['user', 'user_bank'])->where('status', 'ready')->get();
        $withdraws_unconfirmed = FiatWithdraw::with(['user', 'user_bank'])->where('status', 'started')->get();

        foreach ($withdraws as $key => $item) {
            $address = Address::where('user_id', $item->user_id)->first();
            $city = $address->city;
            $street = $address->street;
            $postCode = $address->post_code;
            $state = $address->state;

            $withdraws[$key]['address'] = $street. ' - ' .$city. ' - ' .$state. ' - '.$postCode;
        }

        foreach ($withdraws_unconfirmed as $key => $item) {
            $address = Address::where('user_id', $item->user_id)->first();
            $city = $address->city;
            $street = $address->street;
            $postCode = $address->post_code;
            $state = $address->state;

            $withdraws_unconfirmed[$key]['address'] = $street. ' - ' .$city. ' - ' .$state. ' - '.$postCode;
        }
        

        return view('admin.withdrawals-approval.index', compact('withdraws', 'withdraws_unconfirmed'));
    }

    /**
     * Form for recording an AUD deposit.
     */
    public function add(Request $request, $id)
    {
        $withdraw = \App\Models\FiatWithdraw::find($id);

        $user = User::find($withdraw->user_id);

        $fiat_withdrawals = \App\Models\FiatWithdraw::where('user_id', '=', $user->id)->get();
        if ($request->isMethod('post')) {
            $withdraw->status = "complete";

            $withdraw->save();

            $user = \App\Models\User::find($withdraw->user_id);
            $data = [
                'email' => $user->email,
                'amount' => $withdraw->amount,
                'type' => 'AUD',
            ];

            \Mail::send('email.bank-withdrawal-complete', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('myCryptoWallet: ' . $data['type'] . ' Withdrawal Complete!');
            });

            \App\Models\Emails::create([
                'user_id' => $user->id,
                'to' => $user->email,
                'subject' => strtoupper($data['type']) . ' Withdrawal Complete',
                'data' => json_encode($data),
            ]);

            return redirect()->route('admin.withdrawalsApproval')->with('changes-saved', true);
        }

        return view('admin.withdrawals-approval.new', compact('user', 'withdraw', 'fiat_withdrawals'));
    }

    /*
     * bulk approve withdraw
     */
    public function addAll(Request $request)
    {
        $ids = $request->ids;

        foreach($ids as $id) {

            $withdraw = \App\Models\FiatWithdraw::find($id);
            $withdraw->status = "complete";
            $withdraw->save();
        }

        return 'success';
    }

    public function remove(Request $request, $id)
    {
        $withdraw = \App\Models\FiatWithdraw::findOrFail($id);

        \Assets::group('admin')->add('toggle');

        $user = User::findOrFail($withdraw->user_id);

        return view('admin.withdrawals-approval.remove', compact('user', 'withdraw'));
    }

    public function doRemove(Request $request, $id)
    {

        $withdraw = \App\Models\FiatWithdraw::findOrFail($id);

        \Assets::group('admin')->add('toggle');

        $user = User::findOrFail($withdraw->user_id);

        $balance = \App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'aud')->first();
        $oldbalance = $balance->balance;
        $balance->balance = bcadd($balance->balance, $withdraw->amount, 2);
        $balance->save();

        $ledger = new \App\Models\Ledger;
        $ledger->user_to = $balance->user_id;
        $ledger->transaction_type = "admin_withdraw_reversal";
        $ledger->currency = 'aud';
        $ledger->amount = $withdraw->amount;
        $ledger->notes = "";
        $ledger->oldbalance = $oldbalance;
        $ledger->save();

        \App\Models\FiatWithdraw::destroy($id);

        \App\Models\Ledger::destroy(
            \App\Models\Ledger::where('reference_id', '=', 'x' . $id)->where('currency', '=', 'aud')->first()->id
        );

        if (request()->get('send_email', 0)) {
            \Mail::to($user)->send(new WithdrawRemoval($user, request()->get('reason')));
        }

        return redirect()->route('admin.withdrawalsApproval')->with('changes-saved', true);
    }
}
