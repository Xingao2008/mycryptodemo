<?php

namespace App\Http\Controllers\Admin;

use App\Models\CryptoBuy;
use App\Models\CryptoSell;
use App\Classes\BuySellClasses\Buy\Buy;
use App\Classes\BuySellClasses\Sell\Sell;
use App\Models\Ledger;

class OrdersController extends AdminBaseController
{
    public function buys()
    {
        \Assets::group('admin')->add('datatables');

        return view('admin.orders.buys', compact('buys'));
    }

    public function buys_pending($id)
    {
        $change_id = CryptoBuy::where('id', '=', $id)->first();
        if (strpos($change_id->status, 'INSUFFICIENT_FUNDS') !== false) {
            $change_id->status = 'pending';
            $change_id->save();

            flash('Success! to update Order id '.$id.' status to pending!')->success();
        } else {
            flash('Fail to update Order id '.$id.' status to pending!')->error();
        }

        return redirect()->to('admin/orders/buys');
    }

    public function buys_approve($id)
    {
        $buy = CryptoBuy::where('id', '=', $id)->first();

        if( strpos($buy->status, 'complete') === false ) {
            $relevant_crypto_buy = new Buy( $buy );

            try {
                $relevant_crypto_buy->purchaseOthers();

                //doing the real treat, write the error if fail
                if ($relevant_crypto_buy->getCrypto()->type === 'pwr') {
                    $relevant_crypto_buy->ContactBittrexWithPowr('manually complete');
                } else {
                    $relevant_crypto_buy->ContactBittrex('manually complete');
                }
                //finish buying

            } catch(\Exception $e) {
                if(!is_null($e->getMessage())) {
                    $relevant_crypto_buy->getCrypto()->status = "buy market error:".$e->getMessage();
                } else {
                    $relevant_crypto_buy->getCrypto()->status = "failed, no message";
                }
                $relevant_crypto_buy->getCrypto()->save();
            }

            //remind to recharge
            $relevant_crypto_buy->remindToRecharge();

            flash('Success! Order id '.$id.' has been processed and credited to the user!')->success();
            return redirect()->to('admin/orders/buys');

        } else {
            flash('Order is not in receiving status so we can\'t proceed.')->error();
            return redirect()->to('admin/orders/buys');
        }
    }

    public function sells_approve($id)
    {
        $sell = CryptoSell::where('id', '=', $id)->first();

        if ( strpos($sell->status, 'complete') === false ) {
            $relevant_crypto_sell = new Sell( $sell );

            try {
                $relevant_crypto_sell->SellOthers();

                //doing the real treat, write the error if fail
                if ($relevant_crypto_sell->getCrypto()->type === 'pwr') {
                    $relevant_crypto_sell->ContactBittrexWithPowr('manually complete');
                } else {
                    $relevant_crypto_sell->ContactBittrex('manually complete');
                }
                //finish selling
            } catch (\Exception $e) {
                if(!is_null($e->getMessage())) {
                    $relevant_crypto_sell->getCrypto()->status = "sell market error:".$e->getMessage();
                } else {
                    $relevant_crypto_sell->getCrypto()->status = "failed, no message";
                }
                $relevant_crypto_sell->getCrypto()->save();
            }

            //remind to recharge
            $relevant_crypto_sell->remindToRecharge();

            flash('Success! Order id '.$id.' has been processed and credited to the user!')->success();
            return redirect()->to('admin/orders/sells');

        } else {
            flash('Order is not in receiving status so we can\'t proceed.')->error();
            return redirect()->to('admin/orders/sells');
        }
    }

    public function sells()
    {
        \Assets::group('admin')->add('datatables');

        return view('admin.orders.sells', compact('sells'));
    }

    public function buysDataTables()
    {
        $model = CryptoBuy::query();

        return \DataTables::eloquent($model)
            ->order(function ($query) {
                $query->orderBy('status', 'asc');
                $query->orderBy('updated_at', 'desc');
            })
            ->toJson();
    }

    public function sellsDataTables()
    {
        $model = CryptoSell::query();

        return \DataTables::eloquent($model)
            ->order(function ($query) {
                $query->orderBy('status', 'desc');
                $query->orderBy('updated_at', 'desc');
            })
            ->toJson();
    }
}
