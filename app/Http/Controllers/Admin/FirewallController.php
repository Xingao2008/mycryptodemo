<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Auth\LoginController;
use App\Models\Firewall;

class FirewallController extends AdminBaseController
{
    public function index()
    {
        $whitelist = \App\Models\Firewall::whereWhitelist(1)->get();
        $blacklist = \App\Models\Firewall::whereWhitelist(0)->get();

        \Assets::group('admin')->add('datatables');

        return view('admin.firewall.index', compact('whitelist', 'blacklist'));
    }

    public function delete($id)
    {
        $entry = \App\Models\Firewall::findOrFail($id);

        cache()->forget('attempts:forgot_password:' . $entry->ip_address);
        cache()->forget('attempts:login:' . $entry->ip_address);
        cache()->forget('attempts:attempt_tracker:' . $entry->ip_address);

        $entry->delete();

        flash('Firewall entry deleted successfully');

        return redirect()->back();
    }

    public function create()
    {
        \Assets::group('admin')->add('icheck');

        return view()->make('admin.firewall.create');
    }

    public function postCreate()
    {

        $validator = \Validator::make(request()->all(), [
            'ip_address' => 'required|string',
            'reason' => 'required|string',
            'expires' => 'required|date|after:today',
            'whitelist' => 'required|required',
        ]);

        if ($validator->fails()) {
            return redirect()->to('admin/firewall/new')->withErrors($validator)->withInput();
        }

        try {
            if (request()->get('whitelist', 'blacklist') == 'whitelist') {
                Firewall::whitelist(request()->get('ip_address'), request()->get('reason'), request()->get('expires'));
            } else {
                Firewall::blacklist(request()->get('ip_address'), request()->get('reason'), request()->get('expires'));
            }
        } catch (\Exception $e) {
            flash($e->getMessage())->error();
            return redirect()->to('admin/firewall');
        }


        flash()->success(request()->get('ip_address') . ' has been ' . ucfirst(request()->get('whitelist')) . 'ed');
        return redirect()->to('admin/firewall');
    }
}