<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends AdminBaseController
{
    public function index()
    {
        $posts = News::get();

        return view('admin.news.index', compact('posts'));
    }


    public function create()
    {
        \Assets::group('admin')->add('wysihtml5');

        return view('admin.news.create');
    }


    public function edit($id)
    {
        $post = News::where('id', '=', $id)->first();

        \Assets::group('admin')->add('wysihtml5');

        return view('admin.news.edit', compact('post'));
    }


    public function delete($id)
    {

        $post = News::where('id', '=', $id)->first();

        $post->delete();

        \Flash::message('Your post has been deleted.');

        return redirect('admin/news');
    }


    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:1|max:255',
            'author' => 'required|min:1|max:255',
            'content' => 'required',
            'id' => 'sometimes',
            'featured_image' => 'sometimes',
        ]);

        $post = request()->has('id') ? News::find(request('id')) : new News();

        $slug = str_slug(request()->get('title'));

        if (request()->has('featured_image')) {
            $featured_image = request()->file('featured_image')->store('news');
            $post->featured_image = $featured_image;
        }

        $post->title = request('title');
        $post->author = request('author');
        $post->content = request('content');
        $post->published = request()->filled('published');
        $post->slug = $slug;

        $post->save();

        \Flash::message('Success! Your new post has been published.');


        return redirect('admin/news');
    }


}
