<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Balance;
use App\Models\Ledger;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BalancesController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');

        return view('admin.balances.index', compact('users'));
    }

    public function edit(Request $request, $id)
    {
        $balance = Balance::find($id);
        $user = User::find($balance->user_id);
        if ($request->isMethod('post')) {

            // Record on `balances`
            $req = $request->all();
            $oldbal = \App\Models\Balance::where('user_id','=',$balance->user_id)->where('currency','=',$req['currency'])->first();
            if(!$oldbal) {
                $oldbalance = 0;
            } else {
                $oldbalance = $oldbal->balance;
            }


            $params = [
                'id' => $id,
                'user_id' => $balance->user_id,
                'currency' => $req['currency'],
                'balance' => $req['balance'],
                'locked' => $req['locked'],
                'created_at' => $balance->created_at,
                'updated_at' => date('Y-m-d H:i:s')
            ];

            (new Balance)->saveOperation($params);


            // Record on `ledger`
            $ledger = new Ledger;
            $ledger->user_to = $balance->user_id;
            $ledger->transaction_type = "admin_balance_change";
            $ledger->currency = $req['currency'];
            $ledger->amount = bcsub($req['balance'],$oldbalance,18);
            $ledger->notes = "";
            $ledger->oldbalance = $oldbalance;
            $ledger->save();

            return redirect()->route('admin.balances')->with('changes-saved', true);
        }

        return view('admin.balances.edit', compact('balance', 'user'));
    }

    public function list_balances(Request $request, $id)
    {
        $user = User::find($id);
        $balances = Balance::where('user_id', $id)->get();

        return view('admin.balances.list', compact('balances', 'user'));
    }
}
