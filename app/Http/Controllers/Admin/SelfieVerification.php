<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\SelfieApproved;
use App\Notifications\SelfieDenied;

class SelfieVerification extends Controller
{

    public function index() {
        $users = User::select(['users.*', 'verifications.selfie'])->join('verifications', 'verifications.user_id', '=', 'users.id')->where('verifications.selfie', '=', 'pending')->get();
        return view('admin.selfie.index')->with('users', $users);
    }

    public function edit($id) {
        $user = User::findOrFail($id);

        return view('admin.selfie.edit')->with('user', $user);
    }

    public function postEdit($id) {
        $user = User::whereId($id)->firstOrFail();

        $verification = $user->verification;
        $verification->selfie = request()->get('approve', 0) ? 'approved' : 'incomplete';
        $verification->selfie_notes = request()->get('notes', '');

        if($verification->selfie != 'approved' && !request()->get('notes', false)) {
            flash('Please include a note when denying a selfie')->error();
            return redirect()->back();
        }

        $verification->save();

        if($verification->selfie == 'approved') {
            $user->notify(new SelfieApproved());
        } else {
            $user->notify(new SelfieDenied($verification));
        }

        return redirect()->to('admin/selfies');
    }

}