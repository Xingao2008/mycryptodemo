<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserLimits;

class UserLimitsController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');
        return view('admin.user_limits.index', compact('users'));
    }

    public function edit($id) {
        $user = User::findOrFail($id);

        $limits_tmp = UserLimits::whereUserId($id)->get();
        $limits = collect();

        foreach($limits_tmp as $limit) {
            $limits[$limit->key] = $limit->value;
        }

        return view('admin.user_limits.edit', ['user' => $user, 'limits' => $limits]);
    }

    public function update($id) {
        $user = User::findOrFail($id);

        foreach(request()->except('_token') as $limit => $value)
        {
            $value = (float)$value;

            if($value == 0) {
                UserLimits::whereUserId($user->id)->where('key', $limit)->delete();
            } else {
                $user_limit = UserLimits::firstOrNew(['key' => $limit, 'user_id' => $user->id]);
                $user_limit->value = $value;
                $user_limit->save();
            }

            UserLimits::cacheForget($limit, $user->id);
        }


        flash('User limits successfully updated')->success();

        return redirect()->to('admin/user_limits');
    }
}