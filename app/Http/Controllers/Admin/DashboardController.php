<?php

namespace App\Http\Controllers\Admin;


use App\Models\Ledger;
use App\Models\User;
use Carbon\Carbon;
use DB;

class DashboardController extends AdminBaseController
{
    public function index()
    {
        $volume['all']['aud'] = 0;
        $volume['all']['btc'] = 0;
        $volume['all']['ltc'] = 0;
        $volume['all']['xrp'] = 0;
        $volume['all']['eth'] = 0;
        $volume['30']['aud'] = 0;
        $volume['30']['btc'] = 0;
        $volume['30']['ltc'] = 0;
        $volume['30']['xrp'] = 0;
        $volume['30']['eth'] = 0;
        $profits['all'] = 0;
        $profits['30'] = 0;
        $profits_graph = [];
        $balance['aud'] = 0;
        $balance['btc'] = 0;
        $balance['ltc'] = 0;
        $balance['eth'] = 0;
        $balance['xrp'] = 0;

        $ledgers = \App\Models\Ledger::select(['currency', \DB::raw('SUM(amount) as sum')])->groupBy('currency')->get();
        foreach ($ledgers as $row) {
            $volume['all'][$row->currency] = $row->sum;
        }

        $ledgers = \App\Models\Ledger::select(['currency', \DB::raw('SUM(amount) as sum')])->where('created_at', '>=', Carbon::now()->subMonth())->groupBy('currency')->get();
        foreach ($ledgers as $row) {
            $volume['30'][$row->currency] = $row->sum;
        }

        $profits['all'] = Ledger::where('fee_type', 'buy_sell_total_fee')
            ->orWhere('fee_type', 'buyselltotalfee')
            ->sum('fee');
        $profits['30'] = Ledger::where('fee_type', 'buy_sell_total_fee')
            ->orWhere('fee_type', 'buyselltotalfee')
            ->where('created_at', '>=', Carbon::now()->subMonth())
            ->sum('fee');

        $profits_graph = cache()->remember('admin:profits_graph', 60, function () {
            return Ledger::select([DB::raw('DATE(created_at) as x'), DB::raw('sum(fee) as y')])
                ->groupBy('x')
                ->where('created_at', '>', Carbon::now()->subDay(30))
                ->where('fee_type', 'buy_sell_total_fee')
                ->orWhere('fee_type', 'buyselltotalfee')
                ->get()
                ->toArray();
        });


        $holdingbalances['btc'] = cache()->remember('admin:holding_balances_btc', 5, function () {
            if (app()->environment() == 'production') {
                return json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=btc'));
            } else {
                return '1.' . rand(123445, 9999999);
            }
        });

        $holdingbalances['ltc'] = cache()->remember('admin:holding_balances_ltc', 5, function () {
            if (app()->environment() == 'production') {
                return json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=ltc'));
            } else {
                return '4.' . rand(123445, 9999999);
            }
        });

        $holdingbalances['eth'] = cache()->remember('admin:holding_balances_eth', 5, function () {
            if (app()->environment() == 'production') {
                return json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=eth&address=' . config('crypto.holding.eth')));
            } else {
                return '3.' . rand(123445, 9999999);
            }
        });

        $holdingbalances['xrp'] = cache()->remember('admin:holding_balances_xrp', 5, function () {
            if (app()->environment() == 'production') {
                return json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getbalance.php?coin=xrp&address=' . config('crypto.holding.xrp')));
            } else {
                return '2.' . rand(123445, 9999999);
            }
        });

        $b = \App\Models\Balance::select([DB::raw('SUM(balance) as sum'), 'currency'])->groupBy('currency')->get();
        foreach($b as $e) {
            $balance[$e->currency] = $e->sum;
        }

        $user_signups = cache()->remember('user_signups_graph', 60, function () {
            return User::select([DB::raw('DATE(created_at) as x'), DB::raw('count(*) as y')])
                ->groupBy('x')
                ->where('created_at', '>', Carbon::now()->subDay(30))
                ->get()
                ->toArray();
        });

        $transactions = cache()->remember('transactions_graph', 60, function () {
            return Ledger::select([DB::raw('DATE(created_at) as x'), DB::raw('count(*) as y')])
                ->groupBy('x')
                ->where('created_at', '>', Carbon::now()->subDay(30))
                ->get()
                ->toArray();
        });

        $usdt_balance = getBittrexUSDT();

        return view('admin.dashboard.index', compact('volume', 'profits', 'holdingbalances', 'balance', 'user_signups', 'transactions', 'profits_graph', 'usdt_balance'));
    }
}
