<?php

namespace App\Http\Controllers\Admin;

use App\Models\Emails;
use Illuminate\Http\Request;

class EmailsController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');

        return view('admin.emails.index');
    }

    public function dataTables() {
        $query = Emails::join('users', 'emails.user_id', '=', 'users.id', 'left')
            ->select(['emails.id','emails.user_id','users.name','emails.to','emails.subject','emails.data','emails.created_at']);

        return \DataTables::of($query)->make();
    }

    public function importEmail() {
        return view('admin.emails.importEmail');
    }

    public function emailList() {
        return view('admin.emails.emailList');
    }

    public function emailForm(Request $request) {
        return view('admin.emails.emailForm')->with('title', $request->title);
    }
}