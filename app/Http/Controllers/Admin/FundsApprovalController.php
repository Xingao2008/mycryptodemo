<?php

namespace App\Http\Controllers\Admin;

use App\Models\FiatDeposit;
use App\Models\User;
use App\Notifications\AudDepositComplete;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FundsApprovalController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');

        $deposits = FiatDeposit::orderBy('created_at', 'DESC')->limit(5)->get();

        return view('admin.funds-approval.index')->with('deposits', $deposits);
    }

    /**
     * Form for recording an AUD deposit.
     */
    public function add(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $fiat_deposits = \App\Models\FiatDeposit::where('user_id','=',$id)->get();

        return view('admin.funds-approval.new', compact('user','fiat_deposits'));
    }

    public function postAdd(Request $request, $id) {
        $this->validate($request, ['transaction_date' => 'nullable|sometimes|date|after:01/01/2017', 'amount' => 'required|numeric', 'currency' => 'required']);

        $data = request()->all();
        $data['transaction_date'] = $data['transaction_date'] ?? Carbon::now()->toDateTimeString();

        $user = User::find($id);

        $user->addBankDeposit($data);

        $user->notify(new AudDepositComplete(request('amount')));

        flash('Deposit approved')->success();

        if(request()->ajax()) {
            return ['message' => 'success'];
        }

        return redirect()->route('admin.fundsApproval');
    }

    public function dataTable() {
        return \DataTables::of(FiatDeposit::with('user'))->make(true);
    }
}
