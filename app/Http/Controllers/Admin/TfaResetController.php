<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\TfaResets;
use App\Models\User;
use App\Notifications\TfaDisabled;
use Auth;
use Carbon\Carbon;
use PragmaRX\Google2FA\Google2FA;

class TfaResetController extends Controller
{
    public function index() {
        $tfa_resets = TfaResets::all();

        \Assets::group('admin')->add('datatables');

        return view('admin.tfa_reset.index')->with('tfa_resets', $tfa_resets);
    }

    public function show($id) {
        $tfa_reset = TfaResets::findOrFail($id);

        return view('admin.tfa_reset.show')->with('tfa_reset', $tfa_reset);
    }

    public function getReset($id)
    {
        $user = User::findOrFail($id);
        $resets = TfaResets::whereUserId($id)->whereActive(1)->count();

        if ($resets) {
            flash("There is already a pending TFA reset for " . $user->name)->error();
            return redirect()->to('admin/users');
        }

        \Assets::group('admin')->add(['wysihtml5', 'fileupload']);

        $tfa_reset = TfaResets::firstOrCreate(['user_id' => $id, 'staff_id' => Auth::user()->id, 'active' => 0, 'complete' => 0]);

        return view('admin.tfa_reset.new')->with('user', $user)->with('tfa_reset', $tfa_reset);
    }

    public function postReset($id)
    {
        $tfa_reset = TfaResets::whereId($id)->firstOrFail();

        $user = User::whereId($tfa_reset->user_id)->firstOrFail();

        $tfa_reset->active = 1;
        $tfa_reset->auth_code = uniqid();
        $tfa_reset->reason = request()->get('reason');
        $tfa_reset->save();

        $google2fa = new Google2FA();

        $user->lock_withdraw = 1;
        $user->lock_withdraw_time = Carbon::now()->addHours(48);
        $user->tfa = $google2fa->generateSecretKey();
        $user->save();

        \App\Models\Emails::create([
            'user_id'    => $user->id,
            'to'      => $user->email,
            'subject' => 'Two-Factor Auth Reset Email'
        ]);

        $user->notify(new TfaDisabled($tfa_reset));


        flash('TFA Reset process started')->success();

        return redirect()->to('admin/users');
    }

    public function upload($id)
    {
        $tfa_reset = TfaResets::whereId($id)->firstOrFail();

        if (request()->hasFile('evidence')) {
            $tfa_reset->addMedia(request()->file('evidence'))->toMediaCollection();
        }

        return [];
    }
}
