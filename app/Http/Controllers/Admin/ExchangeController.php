<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Fee;
use App\Models\SurchargeBalance;
use App\Models\Ledger;
use DB;
use Config;
use App\Models\Setting;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    /** 
     * Function for admin to list all the surcharge
     * Currently all the user are exchange_tier 1
     * 
     */
    public function getFeeList()
    {
        $fees = Fee::where('type','exchange')->get();

        return view()->make('admin.exchange.fee.index')->with('fees', $fees);
    }

    public function getEditFee($id)
    {
        $fee = Fee::where('id',$id)->first();

        return view()->make('admin.exchange.fee.edit')->with('fee', $fee);
    }
    
    /**
     * Function to update the surcharge
     */
    public function patchFee(Request $request)
    {
        $validator = \Validator::make(request()->all(), [
            'id' => 'required|numeric',
            'min' => 'required|numeric',
            'max' => 'required|numeric',
            'exchange_tier' => 'required|numeric',
            'fee' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->to('admin/exchange/fees')->withErrors($validator)->withInput();
        }
        $fee = Fee::where('id',$request->id)->first();
        $fee->min = $request->min;
        $fee->max = $request->max;
        $fee->exchange_tier = $request->exchange_tier;
        $fee->fee = $request->fee;
        $fee->save();

        flash()->success('Fee setting has been updated');
        return redirect('/admin/exchange/fees');
    }

    public function getConvert(Request $request)
    {
        $records = DB::table('transactions as t')
                ->join('orders as o', 'o.id', '=', 't.order_id')
                ->leftJoin('orders as s', 's.id', '=', 't.source_id')
                ->select('t.currency as tcurrency', 't.surcharge as tsurcharge',
                't.volume as tvolume',
                'o.price as oprice',
                's.price as sprice',
                't.created_at as tdate')
                ->where(function ($query) use ($request) {
                    if ($request->currency != '') {
                        $query->where('t.currency', '=', $request->currency);
                    }
    
                    if ($request->search_datetime != '') {
                        $date = str_replace('/', '-', $request->search_datetime);
                        $date = date('Y-m-d', strtotime($date));
                        $query->whereDate('t.created_at', '=', $date);
                    }
                    $query->where('t.isCredit', 1);
                    $query->where('t.status', '!=', 'cancel');
                })
                ->paginate(15);
        $balance = SurchargeBalance::all();
       
        return view()->make('admin.exchange.cashout.index')
                ->with('records', $records)
                ->with('request', $request)
                ->with('balance', $balance);
    }

    public function postCashout(Request $request)
    {
        if (empty($request->amount)) {
            flash('No amount has been entered')->error();
            return redirect()->back();
        }
        if (empty($request->currency)) {
            flash('No currency has been selected')->error();
            return redirect()->back();
        }
        if ($request->amount == 0) {
            flash('Amount cannot be zero')->error();
            return redirect()->back();
        }
        $balance = SurchargeBalance::where('currency',$request->currency)->first();
        if (bccomp($balance->amount, $request->amount, 18) > -1) {
            $old_balance = $balance->amount;
            $balance->amount = bcsub($balance->amount, $request->amount, 18);
            $balance->save();
            $ledger = new Ledger;

            $ledger->user_to = 0;
            $ledger->user_from = 0;
            $ledger->address_from = $request->currency;
            $ledger->transaction_type = "surcharge_cashout";
            $ledger->currency = $request->currency;
            $ledger->amount = $request->amount;
            $ledger->oldbalance = $old_balance;
            $ledger->fee_type = "surcharge_cashout";
            $ledger->save();
            return redirect()->to('admin/exchange/fees/convert');
        } else {
            flash('Amount cannot be greater than your balance')->error();
            return redirect()->back();
        }
    }

    public function getCashoutReport(Request $request)
    {
        $records = DB::table('ledgers as t')
                ->select('t.currency as currency', 't.amount as amount',
                't.oldbalance as old_balance',
                't.created_at as created_at')
                ->where(function ($query) use ($request) {
                    if ($request->currency != '') {
                        $query->where('t.currency', '=', $request->currency);
                    }
    
                    if ($request->search_datetime != '') {
                        $date = str_replace('/', '-', $request->search_datetime);
                        $date = date('Y-m-d', strtotime($date));
                        $query->whereDate('t.created_at', '=', $date);
                    }
                    $query->where('t.fee_type', 'surcharge_cashout');
                    $query->where('t.user_from', 0);
                    $query->where('t.user_to', 0);
                })
                ->paginate(30);
        $balance = SurchargeBalance::all();
        return view()->make('admin.exchange.cashout.report')
                ->with('records', $records)
                ->with('request', $request)
                ->with('balance', $balance);
    }

    public function getOrderCondition(Request $request)
    {
        $orders = DB::table('orders as o')
            ->join('users as u', 'o.user_id', '=', 'u.id')
            ->select('u.name as name', 'o.order_type as order_type', 'o.currency as currency', 'o.currency_pair as currency_pair', 'o.status as status', 'o.volume as volume',
                'o.market_type as market_type', 'o.price as price', 'o.remaining as remaining', 'o.updated_at as o_datetime', 'o.id as orderId', 'o.updated_at as datetime')
            ->where(function ($query) use ($request) {
                if ($request->user_name != '') {
                    $query->where('u.name', 'like', '%'.$request->user_name.'%');
                }

                if ($request->currency != '') {
                    $query->where('o.currency', '=', $request->currency);
                }

                if ($request->search_datetime != '') {
                    $date = str_replace('/', '-', $request->search_datetime);
                    $date = strtotime($date);
                    $date = date('Y-m-d', $date);
                    $query->whereDate('o.updated_at', '=', $date);
                }
            })
            ->paginate(15);

//        dd($orders);
        foreach($orders as $key => $order) {
            $transactions = DB::table('transactions as t')
                ->select('t.status as t_status', 't.volume as t_volume', 't.updated_at as t_datetime')
                ->where('t.order_id', '=', $order->orderId)
                ->get();
            $orders[$key]->transactions = $transactions;
        }


        return view()->make('admin.exchange.orders')
            ->with('orders', $orders)
            ->with('req', $request);
    }
    /**
     * Function to pause the exchange
     * Dummy page with a button to reconfirm the admin action
     */
    public function getPause()
    {
        $isPause = Setting::getValue('exchange_pause');
        
        return view()->make('admin.exchange.pause')->with('isPause',$isPause);
    }

    public function postPause(Request $request)
    {
        $validator = \Validator::make(request()->all(), [
            'pauseStatus' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->to('admin/exchange/pause')->withErrors($validator)->withInput();
        }
        Setting::updateValue('exchange_pause', $request->pauseStatus);
        return redirect('/admin/exchange/pause');
    }

}