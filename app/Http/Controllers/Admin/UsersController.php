<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Address;
use App\Models\Bank;
use App\Models\Countries;
use App\Models\Balance;
use App\Models\FiatDeposit;
use App\Models\FiatWithdraw;
use App\Models\Firewall;
use App\Models\Ip;
use App\Models\Ledger;
use App\Models\PoliPayTransaction;
use App\Models\Version;
use App\Models\TfaResets;
use App\Models\User;
use App\Models\UserNotes;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UsersController extends AdminBaseController
{
    public function index()
    {
        \Assets::group('admin')->add('datatables');
        return view('admin.users.index', compact('users'));
    }

    public function edit(Request $request, $id)
    {
        $user = User::find($id);
        $ips = Ip::where('user_id', $id)->orderBy('created_at', 'desc')->get();
        $verifications = \App\Models\Verification::where('user_id', '=', $id)->first();
        $wallets = \App\Models\Wallet::where('user_id', '=', $id)->get();
        $bank = \App\Models\Bank::where('user_id', '=', $id)->first();
        $balances = \App\Models\Balance::where('user_id', '=', $id)->get();
        $buys = \App\Models\CryptoBuy::where('user_id', '=', $id)->get();
        $sells = \App\Models\CryptoSell::where('user_id', '=', $id)->get();
        $deposits = \App\Models\CryptoDeposit::where('user_id', '=', $id)->get();
        $withdrawals = \App\Models\CryptoWithdraw::where('user_id', '=', $id)->get();
        $roles = Role::all();
        $polipay = PoliPayTransaction::whereUserId($id)->get();
        $countrie_calling_codes = Countries::where('calling_code', '!=', 1)
            ->orderByRaw('LENGTH(calling_code)', 'ASC')
            ->orderByRaw('calling_code', 'ASC')
            ->pluck('calling_code', 'calling_code')->all();
        $countries = Countries::all()->filter(function ($value, $key) {
            if (in_array($value->id, config('countries.support_countries'))) {
                return $value;
            }
        });
        $addrs = Version::where('user_id', '=', $user->id)
            ->select('data', 'version', 'status')
            ->where('model', '=', 'addresses')
            ->wherenotNull('data')
            ->get();
        $banks = Version::where('user_id', '=', $user->id)
            ->select('data', 'version', 'status')
            ->where('model', '=', 'banks')
            ->whereNotNull('data')
            ->get();

        $addrs_arr = array();
        $banks_arr = array();

        $i = 0;
        foreach($addrs as $addr) {
            $addrs_arr[$i]['data'] = json_decode($addr->data);
            $addrs_arr[$i]['version'] = $addr->version;
            $addrs_arr[$i]['status'] = $addr->status;
            $i++;
        }

        $i = 0;
        foreach($banks as $banking) {
            $banks_arr[$i]['data'] = json_decode($banking->data);
            $banks_arr[$i]['version'] = $banking->version;
            $banks_arr[$i]['status'] = $banking->status;
            $i++;
        }

        \Assets::group('admin')->add(['select2', 'datatables']);

        return view('admin.users.edit', compact('user', 'ips', 'verifications', 'wallets', 'bank', 'balances', 'sells', 'buys', 'withdrawals', 'deposits', 'fiat_withdrawals', 'fiat_deposits', 'roles', 'polipay', 'countrie_calling_codes', 'countries', 'addrs_arr', 'banks_arr'));
    }

    public function postEdit(Request $request, $id)
    {
        $user = User::find($id);

        if (request('userfieldtype') == "user") {

            $validator = validator(request()->all(), [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'email' => 'required|email',
                'dob' => 'required|string'
            ]);

            if ($validator->fails()) {
                flash('Validation error')->error();
                return redirect()->to('admin/users/'. $id .'/edit#tab2')->withErrors($validator)->withInput();
            }

            $user->fill(request()->all());
            $user->save();


            if (count(request()->get('roles'))) {
                $user->syncRoles(request()->get('roles'));
            } else {
                $user->syncRoles('user');
            }

            if ($request->isXmlHttpRequest()) {
                return response()->json(['success' => true]);
            }
            return redirect('/admin/users/' . $id . '/edit')->with('changes-saved', true);
        }

        if (request('userfieldtype') == "bank") {
            $bank = Bank::where('user_id', '=', $id)->first();

            if ($bank) {
                $bank->user_id = $id;
                $bank->bank_name = $request->bank_name;
                $bank->bank_branch = $request->bank_branch;
                $bank->bank_bsb = $request->bank_bsb;
                $bank->bank_number = $request->bank_number;
                $bank->save();
            } else {
                $bank = new Bank;
                $bank->user_id = $id;
                $bank->bank_name = $request->bank_name;
                $bank->bank_branch = $request->bank_branch;
                $bank->bank_bsb = $request->bank_bsb;
                $bank->bank_number = $request->bank_number;
                $bank->save();
            }

            $save_data = array(
                'bank_name' => $request->bank_name,
                'bank_branch' => $request->bank_branch,
                'bank_bsb' => $request->bank_bsb,
                'bank_number' => $request->bank_number
            );

            //get valid verion of address
            $current_ver = Version::where('model', '=', 'banks')
                ->where('user_id', '=', $bank->user_id)
                ->where('status', '=', 'verified')
                ->orderBy('created_at', 'desc')
                ->first();

            if (is_null($current_ver)) {
                $valid_version = 1;
            } else {
                $valid_version = $current_ver->version + 1;
            }

            $new_version = new Version;
            $new_version->model = 'banks';
            $new_version->user_id = $bank->user_id;
            $new_version->version = $valid_version;
            $new_version->model_id = $bank->id;
            $new_version->data = json_encode($save_data);
            $new_version->status = 'verified';
            $new_version->save();

            if ($request->isXmlHttpRequest()) {
                return response()->json(['success' => true]);
            }
            return redirect('/admin/users/' . $id . '/edit')->with('changes-saved', true);
        }
    }

    public function postAddress($id)
    {
        $user = User::findOrFail($id);

        $validator = validator(request()->all(), [
            'street' => 'required|string',
            'city' => 'required|string',
            'post_code' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            flash('Validation error')->error();
            return redirect()->to('admin/users/'. $id .'/edit#tab6')->withErrors($validator)->withInput();
        }

        $address = Address::where('user_id', '=', $id)->first();

        if ($address) {
            $address->user_id = $user->id;
            $address->fill(request()->all());
            $address->save();
        } else {
            $address = new Address;
            $address->user_id = $user->id;
            $address->fill(request()->all());
            $address->save();
        }

        //get valid verion of address
        $current_ver = Version::where('model', '=', 'addresses')
            ->where('user_id', '=', $address->user_id)
            ->where('status', '=', 'verified')
            ->orderBy('created_at', 'desc')
            ->first();

        $save_data = array(
            'street' => request()->get('street'),
            'city' => request()->get('city'),
            'state' => request()->get('state'),
            'post_code' => request()->get('post_code'),
            'country_id' => request()->get('country_id')
        );

        if (is_null($current_ver)) {
            $valid_version = 1;
        } else {
            $valid_version = $current_ver->version + 1;
        }

        $new_version = new Version;
        $new_version->model = 'addresses';
        $new_version->user_id = $address->user_id;
        $new_version->version = $valid_version;
        $new_version->model_id = $address->id;
        $new_version->data = json_encode($save_data);
        $new_version->status = 'verified';
        $new_version->save();

        return redirect('/admin/users/' . $id . '/edit')->with('changes-saved', true);
    }

    public function fiat($user_id)
    {
        $fiat_deposits = \App\Models\FiatDeposit::where('user_id', '=', $user_id)->get();
        $fiat_withdrawals = \App\Models\FiatWithdraw::where('user_id', '=', $user_id)->get();

        $transfers = collect();

        foreach ($fiat_deposits as $deposit) {
            $tmp = $deposit->toArray();
            $tmp['type'] = 'deposit';

            $transfers->push($tmp);
        }

        foreach ($fiat_withdrawals as $withdrawal) {
            $tmp = $withdrawal->toArray();
            $tmp['type'] = 'withdrawal';

            $transfers->push($tmp);
        }

        return $transfers;
    }

    public function fiatNotes()
    {
        $type = request()->get('type', 'deposit');
        $notes = request()->get('notes', '');
        $id = request()->get('id', '');

        $transfer = ($type == 'deposit') ? FiatDeposit::find($id) : FiatWithdraw::find($id);

        $transfer->notes = $notes;

        $transfer->save();
    }

    public function reverseFiat()
    {
        $type = request()->get('type', 'deposit');
        $id = request()->get('id', '');

        \DB::beginTransaction();
        try {

            $transfer = ($type == 'deposit') ? FiatDeposit::find($id) : FiatWithdraw::find($id);

            $balance = Balance::whereUserId($transfer->user_id)->first();

            $ledger = new \App\Models\Ledger;
            $ledger->user_to = $transfer->user_id;
            $ledger->user_from = 0;
            $ledger->transaction_type = "reverse_bank_" . $type;
            $ledger->currency = 'aud';
            $ledger->amount = $transfer->amount;
            $ledger->oldbalance = $balance->balance;

            if ($type == 'deposit') {
                $balance->balance = bcsub($balance->balance, $transfer->amount);
                Ledger::whereReferenceId('bd' . $transfer->id)->delete();
                $ledger->reference_id = 'delbd' . $transfer->id;
            } else {
                $balance->balance = bcadd($balance->balance, $transfer->amount);
                Ledger::whereReferenceId('x' . $transfer->id)->delete();
                $ledger->reference_id = 'delx' . $transfer->id;
            }

            $ledger->save();

            $transfer->delete();
        } catch (\Exception $e) {
            \DB::rollBack();
            throw $e;
        }

        \DB::commit();

        return [];
    }


    public function impersonate($id)
    {
        $manager = app('impersonate');
        $to = $manager->findUserById($id);

        if (!$manager->isImpersonating()) {
            $manager->take(auth()->user(), $to);
        }

        return redirect()->home();
    }

    public function leaveImpersonate($id)
    {
        $manager = app('impersonate');

        if (!$manager->isImpersonating()) {
            abort(403);
        }

        $manager->leave();

        return redirect()->to('admin/users/' . $id . '/edit');
    }

    public function select2()
    {
        $users = User::where('name', 'LIKE', '%' . request('term') . '%')->limit(25)->get();

        $results['results'] = [];

        foreach ($users as $user) {
            $results['results'][] = ['id' => $user->id, 'text' => $user->name];
        }
        return $results;
    }

    public function dataTables()
    {
        return DataTables::of(User::query())->addColumn('role', function ($user) {
            return title_case($user->getRoleNames()->implode(', '));
        })->make(true);
    }

    public function postNote($id) {
        $this->validate(request(), ['note' => 'required']);

        $note = new UserNotes();
        $note->user_id = $id;
        $note->admin_id = \Auth::user()->id;
        $note->note = request()->get('note');
        $note->save();

        flash('Note added');

        return redirect()->route('admin.users.edit', $id);
    }

    public function getBlockedUsers()
    {
        $blocked_users = \DB::table('users')
            ->select('users.email as email', 'users.name as name', 'users.ubc as ubc', 'users.id as id')
            ->join('login_attempts', 'users.email', '=', 'login_attempts.email')
            ->join('cloudflare_firewall', 'login_attempts.ip_address', '=', 'cloudflare_firewall.ip_address')
            ->groupBy('users.email')
            ->paginate(15);

        return view('admin.users.blocked', ['blocked_users' => $blocked_users]);
    }

    public function searchBlockedUsers(Request $req)
    {
        $user_name = $req->user_name;
        $blocked_users = User::where('name', 'like', '%' .$user_name. '%')
            ->select('users.email as email', 'users.name as name', 'users.ubc as ubc', 'users.id as id')
            ->join('login_attempts', 'users.email', '=', 'login_attempts.email')
            ->join('cloudflare_firewall', 'login_attempts.ip_address', '=', 'cloudflare_firewall.ip_address')
            ->groupBy('users.email')
            ->paginate(15);

        return view('admin.users.blocked', ['blocked_users' => $blocked_users]);
    }

    public function unBlockUsers(Request $req)
    {
        $id = $req->id; //user table id

        //get Firewall id;
        $id = \DB::table('cloudflare_firewall')
            ->select('cloudflare_firewall.id')
            ->join('login_attempts', 'login_attempts.ip_address', '=', 'cloudflare_firewall.ip_address')
            ->join('users', 'users.email', '=', 'login_attempts.email')
            ->where('users.id', '=', $id)
            ->first();

        $entry = Firewall::findOrFail($id);

        //clear the cache
        cache()->forget('attempts:forgot_password:' . $entry->ip_address);
        cache()->forget('attempts:login:' . $entry->ip_address);
        cache()->forget('attempts:attempt_tracker:' . $entry->ip_address);

        //remove from database
        $entry->delete();

        flash('User has been unblocked')->success();


        return redirect()->back();
    }
}
