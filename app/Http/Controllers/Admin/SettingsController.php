<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\PriceControl;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * setting table column type mean: 0: setting, 1: pricing, 2: limit
 * to use in different section
 */
class SettingsController extends AdminBaseController
{
    public function index()
    {
        $settings = \App\Models\Setting::where('type', 0)->orderBy('is_boolean', 'ASC')->orderBy('name', 'ASC')->get();

        $settings_tmp = \App\Models\Setting::get();
        $limits = collect();
        foreach($settings_tmp as $s) {
            $limits[$s->id] = $s->value;
        }

        $settings_tmp = \App\Models\Setting::get();

        $prices['btc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'btc')->first();
        $prices['24btc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'btc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $prices['eth'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'eth')->first();
        $prices['24eth'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'eth')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $prices['ltc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'ltc')->first();
        $prices['24ltc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'ltc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $prices['xrp'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'xrp')->first();
        $prices['24xrp'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'xrp')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $prices['pwr'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'pwr')->first();
        $prices['24pwr'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'pwr')->where('created_at', '<=', \Carbon\Carbon::now())->latest()->first();

        // add this after 1 day of launch to replace above line (due to not enough data)
        // $prices['24pwr'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'pwr')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();

        foreach ($prices as $key => $price) {
            $hidden = setting('hidden_fee') / 100;
            $prices_original[$key] = new \stdClass();
            $prices_original[$key]->bid = number_format(($price->bid * $hidden) + $price->bid, 2, '.', '');
            $prices_original[$key]->ask = number_format(($price->ask * $hidden) + $price->ask, 2, '.', '');
        }

        foreach ($prices as $key => $price) {
            $hidden = setting('hidden_fee') / 100;
            $prices[$key]->bid = number_format(($price->bid * $hidden) + $price->bid, 2, '.', '');
            $prices[$key]->ask = number_format(($price->ask * $hidden) + $price->ask, 2, '.', '');
        }

        foreach ($settings_tmp as $setting) {
            $pricing[$setting->id] = $setting->value;
        }

        $historical_prices = PriceControl::orderBy('created_at', 'desc')->get();

        foreach($historical_prices as $historical_price) {
            $price_history[$historical_price->created_at->toDateTimeString()][$historical_price->currency] = [
                'id' => $historical_price->id,
                'value' => $historical_price->value,
            ];
        }


        if(!isset($price_history)) {
            $price_history = null;
        }

        return view('admin.settings.index')
            ->with('settings', $settings)
            ->with('limits', $limits)
            ->with('pricing', $pricing)
            ->with('prices', $prices)
            ->with('price_history', $price_history)
            ->with('prices_original', $prices_original);
    }

    public function limits() {


        return view('admin.settings.limits')->with('settings', $settings);
    }

    public function pricing()
    {


        return view('admin.settings.pricing', compact('settings', 'prices', 'prices_original'));
    }

    /**
     * @todo store settings
     */
    public function store(Request $request)
    {
        if ($request->hiddentype == "settings") {

            return $this->storeSettings();

        } elseif ($request->hiddentype == "pricing") {
            return $this->storePricing();

        } elseif ($request->hiddentype == "limits") {
            return $this->storeLimits();
        }

        return redirect()->back();
    }

    private function storeSettings()
    {
        $settings = \App\Models\Setting::where('type', 0)->get();

        $rules = [];

        foreach ($settings as $setting) {
            $rules[$setting->name] = 'required' . ($setting->is_boolean) ? '|boolean' : '';
        }

        $validator = \Validator::make(request()->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->getMessageBag())->withInput();
        }

        foreach ($settings as $setting) {
            $setting->value = request()->get($setting->id, 0);
            $setting->save();
        }

        if ($settings->find('maintenance_active')->value) {
            \Artisan::call('down');
        } else {
            \Artisan::call('up');
        }

        \Flash::message('Success! The settings have been saved.');

        return redirect('/admin/settings');
    }

    private function storePricing()
    {

        $this->validate(request(), [
            'btc_id'      => 'required|numeric',
            'ltc_id'      => 'required|numeric',
            'eth_id'      => 'required|numeric',
            'xrp_id'      => 'required|numeric',
            'pwr_id'      => 'required|numeric',
            'btc_fee' => 'required|numeric',
            'eth_fee' => 'required|numeric',
            'ltc_fee' => 'required|numeric',
            'xrp_fee' => 'required|numeric',
            'pwr_fee' => 'required|numeric',
        ]);

        $btc_fee = PriceControl::firstOrNew(['id'=>request()->get('btc_id')]);
        $btc_fee->currency = 'btc';
        $btc_fee->value = request()->get('btc_fee');
        $btc_fee->save();


        $eth_fee = PriceControl::firstOrNew(['id'=>request()->get('eth_id')]);
        $eth_fee->currency = 'eth';
        $eth_fee->value = request()->get('eth_fee');
        $eth_fee->save();


        $ltc_fee = PriceControl::firstOrNew(['id'=>request()->get('ltc_id')]);
        $ltc_fee->currency = 'ltc';
        $ltc_fee->value = request()->get('ltc_fee');
        $ltc_fee->save();


        $xrp_fee = PriceControl::firstOrNew(['id'=>request()->get('xrp_id')]);
        $xrp_fee->currency = 'xrp';
        $xrp_fee->value = request()->get('xrp_fee');
        $xrp_fee->save();


        $pwr_fee = PriceControl::firstOrNew(['id'=>request()->get('pwr_id')]);
        $pwr_fee->currency = 'pwr';
        $pwr_fee->value = request()->get('pwr_fee');
        $pwr_fee->save();

        if(request()->get('subpricing') !== 'subpricing') {
            $sbtc_fee = Setting::firstOrNew(['id' => 'btc_fee']);
            $sbtc_fee->id = 'btc_fee';
            $sbtc_fee->type = 1;
            $sbtc_fee->value = request()->get('btc_fee');
            $sbtc_fee->save();

            $seth_fee = Setting::firstOrNew(['id' => 'eth_fee']);
            $seth_fee->id = 'eth_fee';
            $seth_fee->value = request()->get('eth_fee');
            $seth_fee->type = 1;
            $seth_fee->save();

            $sltc_fee = Setting::firstOrNew(['id' => 'ltc_fee']);
            $sltc_fee->id = 'ltc_fee';
            $sltc_fee->type = 1;
            $sltc_fee->value = request()->get('ltc_fee');
            $sltc_fee->save();

            $sxrp_fee = Setting::firstOrNew(['id' => 'xrp_fee']);
            $sxrp_fee->id = 'xrp_fee';
            $sxrp_fee->type = 1;
            $sxrp_fee->value = request()->get('xrp_fee');
            $sxrp_fee->save();

            $spwr_fee = Setting::firstOrNew(['id' => 'pwr_fee']);
            $spwr_fee->id = 'pwr_fee';
            $spwr_fee->type = 1;
            $spwr_fee->value = request()->get('pwr_fee');
            $spwr_fee->save();
        }


        \Flash::message('Success! The settings have been saved.');

        return redirect('/admin/settings');
    }

    public function storeLimits() {
        foreach(request()->except(['_token', 'hiddentype']) as $key => $value) {
            $setting = Setting::firstOrNew(['id' => $key]);
            $setting->value = number_format($value, 6, '.', '');
            $setting->name = ($setting->name) ?: str_replace('_', ' ', title_case($key));
            $setting->type = 2;
            $setting->save();
        }

        flash('Success! The settings have been saved.');

        return redirect('/admin/settings');
    }
}
