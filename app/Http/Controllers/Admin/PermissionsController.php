<?php

namespace App\Http\Controllers\Admin;


use App\Models\Permission;
use Illuminate\Validation\Validator;

class PermissionsController extends AdminBaseController
{
    public function index()
    {
        $permissions = Permission::all();

        return view('admin.permissions.index', ['permissions' => $permissions]);
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        return view('admin.permissions.edit', ['permission' => $permission]);
    }

    public function postEdit($id)
    {
        $permission = Permission::findOrFail($id);

        $validator = $this->validator();

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $data = request()->all();
        $data['name'] = str_slug($data['name'], '.');

        $permission->fill($data);
        $permission->save();

        flash('Permission updated')->success();
        return redirect()->to('admin/permissions');
    }


    public function create()
    {
        return view('admin.permissions.create');
    }

    public function postCreate()
    {
        $validator = $this->validator();

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $permission = new Permission();

        $data = request()->all();
        $data['name'] = str_slug($data['name'], '.');

        $permission->fill($data);
        $permission->save();

        flash('Permission created')->success();
        return redirect()->to('admin/permissions');
    }

    /**
     * @return Validator
     */
    public function validator(): Validator
    {
        return \Validator::make(request()->all(), [
            'name' => 'required|string',
            'title' => 'required|string',
            'description' => 'required|string',
        ]);
    }
}