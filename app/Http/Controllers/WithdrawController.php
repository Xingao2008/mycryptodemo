<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWithdrawRequest;
use App\Http\Requests\VerifySMSRequest;
use App\Models\Balance;
use App\Models\CryptoWithdraw;
use App\Models\Ledger;
use App\Models\TextVerification;
use App\Models\TransactionVolume;
use App\Models\UserLimits;
use App\Models\Wallet;
use App\Models\ExternalPrices;
use App\Notifications\IdVerificationCode;
use Auth;
use Laracasts\Flash\Flash;

class WithdrawController extends Controller
{
    public function index($path, $coin = null)
    {
        $user = \Auth::user();
//        if ($user->verification->selfie != 'approved' && $user->verification->selfie != 'legacy') {
//            flash('Please upload your selfie before trying to withdraw, <a href="'.route('selfie').'">here</a>')->error();
//            return redirect()->route('verify');
//        }

        if (setting('withdraws_active') && !$user->lock_withdraw) {

            if (in_array($path, config('currencies.allAvailableCurrencies'))) {
                $withdrawLimit = setting($path.'_withdraw_limit');
                $checkUserLimit = UserLimits::where('key', $path.'_withdraw_limit')->where('user_id', Auth::user()->id)->first();
                if ($checkUserLimit && isset($checkUserLimit->value)) {
                    $withdrawLimit = $checkUserLimit->value;
                }
                return view('private.withdraw')
                    ->with('path', $path)
                    ->with('price', ExternalPrices::price())
                    ->with('funds', Balance::funds(\Auth::user()))
                    ->with('withdrawLimit',$withdrawLimit)
                    ->with('coin', $coin);
            }

            return redirect('wallets/withdraw/aud')->with('path', $path);
        } else {
            Flash::message('Withdrawal functionality is currently disabled.');
            return redirect('/wallets');
        }
    }

    private function get200Equal($amount, $price, $user, $currency)
    {
        $transactions = TransactionVolume::whereUserId($user->id)->whereCurrency($currency)->whereAction('withdraw')->where('updated_at', '>=', \Carbon\Carbon::now()->subHours(24))->get();
        $transaction_amount = 0;
        foreach ($transactions as $transaction) {
            $transaction_amount += bcdiv($transaction->value, $transaction->price_used, 8);
        }
        if ($price == null) $price = 1; // aud conversion
        $aud = ($amount + $transaction_amount) * $price;
        if ($aud <= 200) {
            return true;
        } else {
            return false;
        }
    }

    private function hasWithdrawLock($amount, $currency)
    {
        $user = \Auth::user();
        $cryptos = config('crypto.available_currency');


        $pending = \DB::table('users as u')
            ->select('amount')
            ->join('poli_pay_transactions as p', 'u.id', '=', 'p.user_id')
            ->where('p.admin_status', false)
            ->where('p.status', '=', 'completed')
            ->where('u.id', '=', $user->id)
            ->get();


        if ($pending->isEmpty()) { //if there no poli record, skip the rest, return unlock value.
            return false;
        }

//        $poliPendingSum = array_sum(array_column($pending->toArray(), 'amount')); //add up dollar, may need to use bc math for accuracy
        $poliPendingSum = 0;
        foreach ($pending->toArray() as $item) {
            $poliPendingSum = bcadd($poliPendingSum, floatval($item->amount), 10);
        }


        $balanceAll = Balance::where('user_id', '=', $user->id)
            ->get();
        $balanceAll = $balanceAll->toArray();
        foreach ($balanceAll as $key => $item) {
            $crypto = strtolower($item['currency']);

            if (in_array($crypto, $cryptos)) {
                $balanceAll[$key]['aud'] = bcmul($item['balance'], prices('origin')[$crypto]->ask, 10);  // for crypto
            } else {
                $balanceAll[$key]['aud'] = convertFiatCurrency(strtoupper($crypto), 'AUD', $item['balance']);  //for dollar
            }
        }

//        $assetAudSum = array_sum(array_column($balanceAll, 'aud')); //total asset value in AUD
        $assetAudSum = 0;
        foreach ($balanceAll as $item) {
            $assetAudSum = bcadd($assetAudSum, $item['aud'], 10);
        }


        $whatLeft = bcsub($assetAudSum, $poliPendingSum, 10);


        if (in_array($currency, $cryptos)) {
            $amountAud = bcmul($amount, prices('origin')[$currency]->ask, 10);  // for crypto
        } else {
            $amountAud = convertFiatCurrency($currency, 'AUD', $amount); //for dollar
        }


        $diff = bcsub($whatLeft, $amountAud, 10);

        if($diff >= 0) {
            return false;
        } else {
            return true;
        }
    }

    public function go(StoreWithdrawRequest $request, $path)
    {
        if($this->hasWithdrawLock($request->amount, $path)) {
            Flash::message('Sorry, there is a pending PoliPayment on your account, and you aren\'t able to withdraw an amount that exceeds the pending amount until the funds are finalised (usually 3 business days from purchase). Please contact support if you have any issues.');
            return redirect('/wallets');

        } else {
            $user = \Auth::user();
            $pass200 = $this->get200Equal($request->amount, $request->convertion_rate, $user, $path);

            /**
             * Disabled on 19/6/18. From Jaryd command
             */
            // if ($user->verification->selfie != 'approved' && $user->verification->selfie != 'legacy' && !$pass200) {
            //     flash('User can only withdraw less than $200 equivalent value per day without upload your selfie. Please upload your selfie in <a href="'.route('selfie').'">here</a> if the amount is more than that.')->error();
            //     return redirect()->route('verify');
            // }

            if (setting('withdraws_active') && !$user->lock_withdraw) {

                if (in_array($path, config('currencies.crypto'))) {
                    \DB::beginTransaction();
                    try {

                        $currency = $path;
                        $amount = $request->amount;
                        $external = $request->external;
                        $from = Wallet::where('type', '=', $currency)->where('user_id', '=', $user->id)->latest()->lockForUpdate()->first();
                        $balance_raw = Balance::where('currency', '=', $currency)->where('user_id', '=', $user->id)->lockForUpdate()->first();
                        $balance = $balance_raw->balance;
                        if(is_null($from->address) || is_null($from)) {
                            flash('Whoops, looks like there isn\'t address or user information in our database, please contact the support')->error();
                            return redirect()->back()->withInput();
                        }

                        if ($external == $from->address) {
                            flash("Whoops, looks like you tried to withdraw to your myCryptoWallet address. Please check the address and try again.")->error();
                            return redirect()->back()->withInput();
                        }

                        $withdraw_fee['btc'] = 0.00011;
                        $withdraw_fee['ltc'] = 0.011;
                        $withdraw_fee['eth'] = 0.005;
                        $withdraw_fee['xrp'] = getTxFees('xrp');
                        $withdraw_fee['pwr'] = 15;

                        $minimum_withdraw['btc'] = 0.01;
                        $minimum_withdraw['ltc'] = 0.1;
                        $minimum_withdraw['eth'] = 0.05;
                        $minimum_withdraw['xrp'] = 10;
                        $minimum_withdraw['pwr'] = 50;

                        $maximum_withdraw['btc'] = UserLimits::get_limit('btc_withdraw_limit');
                        $maximum_withdraw['ltc'] = UserLimits::get_limit('ltc_withdraw_limit');
                        $maximum_withdraw['eth'] = UserLimits::get_limit('eth_withdraw_limit');
                        $maximum_withdraw['xrp'] = UserLimits::get_limit('xrp_withdraw_limit');
                        $maximum_withdraw['pwr'] = UserLimits::get_limit('pwr_withdraw_limit');

                        $maximum_daily_withdraw['btc'] = UserLimits::get_limit('btc_daily_withdraw_limit');
                        $maximum_daily_withdraw['ltc'] = UserLimits::get_limit('ltc_daily_withdraw_limit');
                        $maximum_daily_withdraw['eth'] = UserLimits::get_limit('eth_daily_withdraw_limit');
                        $maximum_daily_withdraw['xrp'] = UserLimits::get_limit('xrp_daily_withdraw_limit');
                        $maximum_daily_withdraw['pwr'] = UserLimits::get_limit('pwr_daily_withdraw_limit');

                        $final_amount = bcsub($amount, $withdraw_fee[$currency], 18);

                        // ****************
                        // bccomp
                        // 0 = equal
                        // 1 = left is larger than right
                        // -1 = right is larger than left

                        if (bccomp($amount, $balance, 18) == 1) {
                            \Flash::error('Insufficient funds.');
                            return redirect()->to('wallets/withdraw/' . $path)
                                ->withInput($request->input());
                        }

                        if (bccomp($final_amount, $balance, 18) == 1) {
                            \Flash::error('Insufficient funds after taking fees into account.');
                            return redirect()->to('wallets/withdraw/' . $path)
                                ->withInput($request->input());
                        }

                        if ($amount < $minimum_withdraw[$currency]) {
                            \Flash::error('The minimum withdrawal for ' . strtoupper($currency) . ' is ' . $minimum_withdraw[$currency]);
                            return redirect()->to('wallets/withdraw/' . $path)
                                ->withInput($request->input());
                        }

                        if ($amount > $maximum_withdraw[$currency]) {
                            flash()->error('The maximum withdrawal for ' . strtoupper($currency) . ' is ' . $maximum_withdraw[$currency]);
                            return redirect()->to('wallets/withdraw/' . $path)
                                ->withInput($request->input());
                        }

                        if (\Auth::user()->get_setting('withdraw_tfa', 0)) {
                            if (!\Auth::user()->check_tfa(request()->get('tfa', 0))) {
                                flash()->error('Invalid TFA code');
                                return redirect()->to('wallets/withdraw/' . $path)->withInput($request->input());
                            }
                        }


                        $transactions = TransactionVolume::whereUserId($user->id)->whereCurrency($currency)->whereAction('withdraw')->where('created_at', '>=', \Carbon\Carbon::now()->subHours(24))->get();
                        $transaction_amount = 0;
                        foreach ($transactions as $transaction) {
                            $transaction_amount += bcdiv($transaction->value, $transaction->price_used, 8);
                        }

                        if ($transaction_amount > $maximum_daily_withdraw[$currency]) {
                            flash()->error("You've surpassed your maximum daily withdraw limit of {$maximum_daily_withdraw[$currency]}" . strtoupper($currency));
                            return redirect()->to('wallets/withdraw/' . $path)->withInput($request->input());
                        }


                        $new_withdraw = new CryptoWithdraw;
                        $new_withdraw->user_id = $user->id;
                        $new_withdraw->to = $external;
                        $new_withdraw->from = $from->address;
                        $new_withdraw->type = $currency;
                        $new_withdraw->transaction_id = "soonTM";
                        // show the ACTUAL amount sent
                        $new_withdraw->amount = $final_amount;
                        $new_withdraw->fees = $withdraw_fee[$currency];
                        $new_withdraw->confirmations = 0;
                        $new_withdraw->status = "started";
                        $new_withdraw->save();

                        $new_ledger = new Ledger;
                        $new_ledger->reference_id = "w" . $new_withdraw->id;
                        $new_ledger->user_from = 0;
                        $new_ledger->address_to = $external;
                        $new_ledger->address_from = $from->address;
                        $new_ledger->user_to = $user->id;
                        $new_ledger->transaction_type = "withdraw";
                        $new_ledger->currency = $currency;
                        // show the REQUESTED amount
                        $new_ledger->amount = $amount;
                        $new_ledger->fee = $withdraw_fee[$currency];
                        $new_ledger->fee_type = "mcw";
                        $new_ledger->oldbalance = $balance_raw->balance;
                        $new_ledger->save();

                        $price = latest_price($currency);

                        $new_tv = new \App\Models\TransactionVolume;
                        $new_tv->reference_id = $new_ledger->id;
                        $new_tv->user_id = $new_ledger->user_to;
                        $new_tv->action = $new_ledger->transaction_type;
                        $new_tv->value = bcmul($new_ledger->amount, $price, 18);;
                        $new_tv->currency = $currency;
                        $new_tv->price_used = $price;

                        $new_tv->save();

                        $new_balance = bcsub($balance, $amount, 18);
                        $balance_raw->balance = $new_balance;
                        $balance_raw->save();
                    } catch (\Exception $e) {
                        \DB::rollBack();
                        throw $e;
                    }

                    \DB::commit();
                    \Flash::message('Your request was successful, however you will need to verify the withdrawal via SMS now.');

                    \App\Models\Ip::create([
                        'user_id' => $user->id,
                        'ip' => request()->ip(),
                        'event' => 'crypto withdrawal',
                    ]);

                    return redirect('wallets/verify/withdraw/' . $new_withdraw->id);

                } elseif (in_array($path, config('currencies.fiatCurrencies'))) {
                    $currency = $path;
                    $amount = $request->amount;
                    $balance_raw = Balance::where('currency', '=', $currency)->where('user_id', '=', $user->id)->first();
                    $bank = \App\Models\Bank::where('user_id', '=', $user->id)->first();
                    $balance = $balance_raw->balance;


                    $minimum_withdraw['aud'] = 10.00;
                    $minimum_withdraw['nzd'] = 10.00;
                    $minimum_withdraw['usd'] = 10.00;


                    // ****************
                    // bccomp
                    // 0 = equal
                    // 1 = left is larger than right
                    // -1 = right is larger than left

                    if (bccomp($amount, $balance, 18) == 1) {
                        \Flash::error('Insufficient funds.');
                        return redirect()->to('wallets/withdraw/' . $path)
                            ->withInput($request->input());
                    }

                    if ($amount < $minimum_withdraw[$currency]) {
                        \Flash::error('The minimum withdrawal for ' . strtoupper($currency) . ' is $' . $minimum_withdraw[$currency]);
                        return redirect()->to('wallets/withdraw/' . $path)
                            ->withInput($request->input());
                    }

                    if (!$bank) {
                        \Flash::error('You haven\'t added your bank details. Please go to Account and add your bank details there.');
                        return redirect()->to('wallets/withdraw/' . $path)
                            ->withInput($request->input());
                    }

                    if (\Auth::user()->get_setting('withdraw_tfa', 0)) {
                        if (!\Auth::user()->check_tfa(request()->get('tfa', 0))) {
                            flash()->error('Invalid TFA code');
                            return redirect()->to('wallets/withdraw/' . $path)->withInput($request->input());
                        }
                    }

                    \DB::beginTransaction();
                    try {
                        $new_withdraw = new \App\Models\FiatWithdraw;
                        $new_withdraw->user_id = $user->id;
                        $new_withdraw->bank = $bank->id;
                        $new_withdraw->amount = $amount;
                        $new_withdraw->currency = $currency;
                        $new_withdraw->status = "started";
                        $new_withdraw->save();

                        $new_ledger = new Ledger;
                        $new_ledger->reference_id = "x" . $new_withdraw->id;
                        $new_ledger->user_from = 0;
                        $new_ledger->address_to = 'bank' . $bank->id;
                        $new_ledger->user_to = $user->id;
                        $new_ledger->transaction_type = "withdraw";
                        $new_ledger->currency = $currency;
                        // show the REQUESTED amount
                        $new_ledger->amount = $amount;
                        $new_ledger->oldbalance = $balance_raw->balance;
                        $new_ledger->save();

                        $new_balance = bcsub($balance, $amount, 18);
                        $balance_raw->balance = $new_balance;
                        $balance_raw->save();
                    } catch (\Exception $e) {
                        \DB::rollBack();
                        throw $e;
                    }

                    \DB::commit();

                    return redirect('wallets/verify/withdraw/f' . $new_withdraw->id);

                }

                return redirect('wallets');
            } else {
                Flash::message('Withdrawal functionality is currently disabled.');
                return redirect('/wallets');
            }
        }
    }

    public function verify($path)
    {
        $user = \Auth::user();
        if (setting('withdraws_active') && !$user->lock_withdraw) {

            $fiattest = substr($path, 0, 1);
            if ($fiattest == "f") {
                $temp_path = substr($path, 1);
                $withdraw = \App\Models\FiatWithdraw::where('id', '=', $temp_path)->where('user_id', '=', $user->id)->first();
            } else {
                $withdraw = CryptoWithdraw::where('id', '=', $path)->where('user_id', '=', $user->id)->first();
            }

            if (!$withdraw) {
                return redirect('wallets');
            }

            $textverification = \App\Models\TextVerification::where('user_id', '=', $user->id)->orderBy('created_at', 'desc')->where('verified', '=', '0')->where('reference_id', '=', $path)->first();

            if ($textverification !== NULL) {
                $show_form = TRUE;
            }

            $status = $withdraw->status;

            $user = Auth::user();
           
            $coin_balances['aud'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'AUD')->first()->balance);
            $coin_balances['nzd'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'nzd')->first()->balance);
            $coin_balances['usd'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'usd')->first()->balance);
            $coin_balances['btc'] = btcFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'btc')->first()->balance);
            $coin_balances['ltc'] = ltcFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'ltc')->first()->balance);
            $coin_balances['eth'] = ethFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'eth')->first()->balance);
            $coin_balances['xrp'] = xrpFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'xrp')->first()->balance);
            $coin_balances['pwr'] = xrpFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'pwr')->first()->balance);

            return view('private.withdraw-sms', compact(['show_form', 'path', 'status', 'coin_balances']));
        } else {
            Flash::message('Withdrawal functionality is currently disabled.');
            return redirect('/wallets');
        }

    }

    public function sms_verify(VerifySMSRequest $request)
    {
        $user = \Auth::user();
        if (setting('withdraws_active') && !$user->lock_withdraw) {
            $withdraw_id = $request->withdraw_id;
            if ($request->sms == 'send') {


                $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

                $user->notify(new IdVerificationCode($code));

                TextVerification::create([
                    'user_id' => $user->id,
                    'to' => $user->phone,
                    'type' => 'verify',
                    'code' => $code,
                    'reference_id' => $withdraw_id,
                ]);

                Flash::message('Success! Please wait up to 30 seconds for your verification code.');

                return redirect('wallets/verify/withdraw/' . $withdraw_id);
            }

            if ($request->sms == 'verify') {
                $user = \Auth::user();
                $textverification = TextVerification::where('user_id', '=', $user->id)->where('verified', '=', '0')->orderBy('created_at', 'desc')->where('reference_id', '=', $withdraw_id)->first();

                $fiattest = substr($withdraw_id, 0, 1);
                if ($fiattest == "f") {
                    $withdraw_id = substr($withdraw_id, 1);
                    $withdraw_request = \App\Models\FiatWithdraw::where('id', '=', $withdraw_id)->where('user_id', '=', $user->id)->first();
                } else {
                    $withdraw_request = CryptoWithdraw::where('id', '=', $withdraw_id)->where('user_id', '=', $user->id)->first();
                }

                $user_entered = $request->code;

                if ($textverification->attempts == 5) {
                    Flash::message('Sorry, you\'ve exceeded the maximum number of allowed attempts. Please resend the SMS.');

                    return redirect('wallets/verify/withdraw/' . $withdraw_id);
                }

                if ($user_entered == $textverification->code) {
                    $textverification->verified = TRUE;
                    $textverification->save();

                    $withdraw_request->status = 'ready';
                    $withdraw_request->save();
                    Flash::message('Congratulations! You\'ve successfully verified your mobile. Your withdrawal request will be processed shortly.');

                    return redirect('wallets');
                } else {
                    $textverification->attempts++;
                    $textverification->save();
                    Flash::message('Incorrect code, please try again. Note that a maximum of 5 attempts per SMS are allowed. (attempt ' . $textverification->attempts . ' of 5)');

                    return redirect('wallets/verify/withdraw/' . $withdraw_id);
                }
            }
        } else {
            Flash::message('Withdrawal functionality is currently disabled.');
            return redirect('/wallets');
        }
    }

}
