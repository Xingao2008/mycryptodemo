<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Models\Wallet;
use App\Models\CryptoDeposit;
use App\Models\CryptoWithdraw;
use DB;

class OrdersController extends Controller
{
    public function index()
    {
        $user = \Auth::user();
        $user_deposits = DB::table('wallets')
            ->join('crypto_deposits', 'wallets.address', '=', 'crypto_deposits.to')
            //->join('crypto_withdraws', 'wallets.address', '=', 'crypto_withdraws.to')
            ->where('wallets.user_id','=',$user->id)
            ->orderBy('crypto_deposits.created_at','desc')
            ->select('*')
            ->get();

        $user_withdraws = DB::table('users')
            ->join('ledgers', 'users.id', '=', 'ledgers.user_to')
            //->join('crypto_withdraws', 'wallets.address', '=', 'crypto_withdraws.to')
            ->where('transaction_type','=','withdraw')
            ->where('ledgers.user_to','=',$user->id)
            ->orderBy('ledgers.created_at','desc')
            ->distinct()
            ->select('ledgers.reference_id','address_to', 'currency','amount','status','transaction_id','fee','ledgers.created_at')
            ->get();

        foreach($user_withdraws as $wkey=>$withdraw) {
            if(substr($withdraw->address_to, 0,4) == "bank") {
                $user_withdraws[$wkey]->address_to = "User's Bank";
            }
        }

        foreach($user_withdraws as $key=>$value) {



            if($value->currency != "aud") {
                $id = $value->reference_id;
                $new_id = ltrim($id, 'w');
                $crypto_withdraw = CryptoWithdraw::where('id', '=', $new_id)->first();
                $user_withdraws[$key]->status = $crypto_withdraw['status'];
                $user_withdraws[$key]->confirmations = $crypto_withdraw['confirmations'];
                $user_withdraws[$key]->actual_fee = $user_withdraws[$key]->amount - $crypto_withdraw['amount'];

                if($value->status == "started") {
                    $user_withdraws[$key]->verifyid = $crypto_withdraw->id;
                }
            } else {
                $id = $value->reference_id;
                $new_id = ltrim($id, 'x');
                $crypto_withdraw = \App\Models\FiatWithdraw::where('id', '=', $new_id)->first();
                $user_withdraws[$key]->status = $crypto_withdraw['status'];
                $user_withdraws[$key]->confirmations = "N/A";
                $user_withdraws[$key]->actual_fee = '$0';
                $user_withdraws[$key]->amount = "$".number_format($user_withdraws[$key]->amount,2);
                if($value->status == "started") {
                    $user_withdraws[$key]->verifyid = 'f'.$crypto_withdraw->id;
                }
            }


        }
        $user_buys = \App\Models\CryptoBuy::where('user_id','=',$user->id)->orderBy('created_at','desc')->get();
        foreach($user_buys as $key=>$value) {
            $ledger = \App\Models\Ledger::where('reference_id','=','b'.$value->id)->first();
            if(isset($ledger->fee)) {
                $user_buys[$key]->actual_fee = number_format($ledger->fee,2);
            }
        }

        $user_sells = \App\Models\CryptoSell::where('user_id','=',$user->id)->orderBy('created_at','desc')->get();
        foreach($user_sells as $key=>$value) {
            $ledger = \App\Models\Ledger::where('reference_id','=','s'.$value->id)->first();
            $user_sells[$key]->actual_fee = number_format($ledger->fee,2);
        }

        return view('private.ordershistory', compact(['user_deposits','user_withdraws','user_buys','user_sells']));
    }
}
