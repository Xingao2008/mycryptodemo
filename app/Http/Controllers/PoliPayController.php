<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Ledger;
use App\Models\PoliPayTransaction;
use App\Models\UserLimits;
use Carbon\Carbon;
use Omnipay\Omnipay;
use Omnipay\Poli\Message\CompletePurchaseResponse;

class PoliPayController extends Controller
{

    /**
     * @var \Omnipay\Poli\Gateway
     */
    private $gateway;

     public function __construct()
     {
         // Create the gateway instance and login
         $this->gateway = Omnipay::create('Poli');
         $this->gateway->setMerchantCode(config('polipay.merchant_code'));
         $this->gateway->setAuthenticationCode(config('polipay.auth_code'));
     }

     public function index()
     {
         if(!setting('enable_polipay') || !\Auth::user()->canPoliPay())
         {
             return redirect()->back();
         }
         
         $validate = \Validator::make(
             request()->all(),
             [
                 'amount' => 'required|numeric|between:50.00,' . UserLimits::get_limit('poli_transaction_limit')
             ]
         );

         if ($validate->fails()) {
             flash($validate->messages()->first())->error();
             return redirect()->back()->withInput();
         }

         $transactions = PoliPayTransaction::whereUserId(\Auth::user()->id)->whereStatus('completed')->where('created_at', '>', Carbon::now()->subHours(24))->sum('amount');

         if($transactions >= UserLimits::get_limit('poli_daily_limit'))
         {
             flash('You have exceeded the transaction volume limit of $'.UserLimits::get_limit('poli_daily_limit').' for the past 24 hours. Your current transaction volume is: $'.number_format($transactions) )->error();
             return redirect()->back()->withInput();
         }

         $poli_record = new PoliPayTransaction();
         $poli_record->amount = number_format(request()->get('amount'), 2, '.', '');
         $poli_record->user_id = \Auth::user()->id;
         $poli_record->status = 'incomplete';
         $poli_record->admin_status = false;
         $poli_record->save();

         try {
             $response = $this->gateway->purchase(array(
                 'amount' => $poli_record->amount,
                 'currency' => 'AUD',
                 'transactionID' => $poli_record->id,
                 'returnUrl' => url()->route('polipay.complete'),
                 'notifyUrl' => url()->route('polipay.notify'),
                 'cancelUrl' => url()->route('polipay.cancelled'),
             ))->send();

             if(preg_match('/\?Token=(.*)$/', $response->getData()['NavigateURL'], $match)) {
                 $poli_record->transaction_token = $match[1];
                 $poli_record->save();
             }
         } catch (\Exception $e) {
             flash("Error Occured: " . $e->getMessage())->error();
             return redirect()->to('wallets/deposit/aud');
         }


         /**
          * Process response
          * @var $response \Omnipay\Poli\Message\PurchaseResponse
          */
         if ($response->isRedirect()) {
             // Redirect to offsite payment gateway
             $response->redirect();

         } else {
             // Payment failed
             echo $response->getMessage();
         }
     }

     public function complete()
     {
         if(request()->has(['Token', 'token']))
         {
             /**
              * Parse the request to get the transaction response
              * @var $response CompletePurchaseResponse
              */
             $response = $this->gateway->completePurchase()->send();
             $data = $response->getData();

             $status = strtolower($data->TransactionStatus);

             $record = $this->record_payment($response);

             switch($status)
             {
                 case 'failed':
                 case 'receiptunverified':
                     flash($data->ErrorMessage)->error();
                     break;
                 case 'completed':
                     //flash("$" . number_format($response->getData()->AmountPaid, 2) . "AUD has been deposited into your wallet")->success();
                     return view()->make('private.poli-receipt')->with('polidata', $data)->with('record', $record);
                     break;
             }
         } else {
             flash()->success('We are now processing your transaction');
         }

         return redirect()->to('wallets/deposit/aud');
     }

     public function cancelled()
     {
         flash("Transaction cancelled")->error();
         return redirect()->to('wallets/deposit/aud');
     }

     public function notify()
     {

          // Parse the request to get the transaction response
          /* @var $response CompletePurchaseResponse */

         try {
             $response = $this->gateway->completePurchase()->send();
         } catch (\Exception $e) {
             return response()->json(['error' => $e->getMessage()]);
         }

         $this->record_payment($response);

         return response()->json(['status' => 'ok']);
     }


    /**
     * @param CompletePurchaseResponse $response
     * @return PoliPayTransaction
     */
     private function record_payment(CompletePurchaseResponse $response): PoliPayTransaction
     {
         return PoliPayTransaction::updateTransaction($response->getData());
     }
}
