<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use GuzzleHttp\Client;
use App\Models\User;
use App\Models\ExternalPrices;
use App\Models\Balance;
use App\Bittrex\Bittrex;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','status']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $tier = tier($user);
        $tfa_status = $user->tfa_status;

        \Assets::add('charts');

        return view('private.dashboard')->with('tier', $tier)->with('tfa_status', $tfa_status);
    }

    public function indexNew(Request $request)
    {
        return view('private.account.dashboard-new');
    }
}
