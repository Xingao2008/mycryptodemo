<?php
namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TradeHistoryEvent implements ShouldBroadcast
{
    use SerializesModels;

    public $success;
    public $user;

    /**
     * Create a new event instance.
     *
     * @param  json $data
     * @param  string $channel
     * @return void
     */
    public function __construct($user, $success)
    {
        $this->user = $user;
        $this->success = $success;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        $uid = base64_encode($this->user->id);
        return new PrivateChannel('exchange_user_'.$uid);
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['order_create' => $this->success];
    }

    /**
     * Get the name the event should be broadcast on.
     *
     * @return string
     */
    // public function broadcastAs()
    // {
    //     return 'exchange.trade_history';
    // }
}