<?php

namespace App\Events;

use App\Models\Firewall;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LoginBlacklisted implements ShouldQueue
{

    use Dispatchable, SerializesModels;
    /**
     * @var Firewall
     */
    public $blacklist;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Firewall $blacklist)
    {
        $this->blacklist = $blacklist;

    }
}
