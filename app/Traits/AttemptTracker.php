<?php


namespace App\Traits;


use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Http\Request;

trait AttemptTracker
{
    /**
     * @param $request
     * @return bool
     */
    public function hasTooManyAttempts(Request $request)
    {
        return (cache($this->attemptCacheKey($request), 0) >= $this->getMaxAttempts());
    }

    /**
     * Fire an event when a lockout occurs. - Also Blacklist the IP
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function fireAttemptLockoutEvent(Request $request)
    {
        //event(new Lockout($request));

        //Firewall::blacklist($request->ip(), cache($this->throttleKey($request)) . ' failed login attempts', Carbon::now()->addDays(2));
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function attemptCacheKey(Request $request)
    {

        switch(get_class()) {
            case LoginController::class:
                $key = 'login';
                break;
            case ForgotPasswordController::class:
                $key = 'forgot_password';
                break;
            default:
                $key = 'attempt_tracker';
        }

        return 'attempts:' . $key . ':' . $request->ip();
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function incrementAttempts(Request $request)
    {
        if (!cache()->has($this->attemptCacheKey($request))) {
            cache()->put($this->attemptCacheKey($request), 0, $this->getDecayMinutes());
        }

        cache()->increment($this->attemptCacheKey($request));
    }

    /**
     * Get the maximum number of attempts to allow.
     *
     * @return int
     */
    public function getMaxAttempts()
    {
        return property_exists($this, 'maxAttempts') ? $this->maxAttempts : 5;
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */
    public function getDecayMinutes()
    {
        return property_exists($this, 'decayMinutes') ? $this->decayMinutes : 1;
    }
}