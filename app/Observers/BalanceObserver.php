<?php

namespace App\Observers;


use App\Models\Balance;

class BalanceObserver
{
    public function saved(Balance $balance)
    {
        cache()->forget( Balance::getFundsCacheKey($balance->user_id) );
        cache()->forget( Balance::getCoinBalancesCacheKey($balance->user_id) );
    }
}