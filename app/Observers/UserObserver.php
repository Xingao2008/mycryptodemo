<?php


namespace App\Observers;

use App\Models\User;

class UserObserver
{
    public function saving(User $user)
    {
        if (!$user->settings) {
            $user->settings = [];
        }

        $user->name = title_case(preg_replace('!\s+!', ' ', implode(' ', [$user->first_name, $user->middle_name, $user->last_name])));
    }

}