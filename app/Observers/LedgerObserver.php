<?php

namespace App\Observers;


use App\Models\Ledger;
use App\Models\TransactionVolume;

class LedgerObserver
{
    public function created(Ledger $ledger)
    {
        if ($ledger->currency == "aud") {
            if($ledger->transaction_type !== "deposit") {
                $transaction_volume = TransactionVolume::firstOrNew(['reference_id' => $ledger->id]);
                $transaction_volume->reference_id = $ledger->id;
                $transaction_volume->user_id = $ledger->user_to;
                $transaction_volume->action = $ledger->transaction_type;
                $transaction_volume->value = $ledger->amount;
                $transaction_volume->currency = "aud";
                $transaction_volume->price_used = "1.000000000000000000";
                $transaction_volume->created_at = $ledger->created_at;
                $transaction_volume->updated_at = $ledger->updated_at;
                $transaction_volume->save();
            }
        } elseif ($ledger->transaction_type != "buy_received") {
            $price = latest_price($ledger->currency);

            $aud_amount = bcmul($ledger->amount, $price, 18);

            $transaction_volume = TransactionVolume::firstOrNew(['reference_id' => $ledger->id]);
            $transaction_volume->reference_id = $ledger->id;
            $transaction_volume->user_id = $ledger->user_to;
            $transaction_volume->action = $ledger->transaction_type;
            $transaction_volume->value = $aud_amount;
            $transaction_volume->currency = $ledger->currency;
            $transaction_volume->price_used = $price;
            $transaction_volume->created_at = $ledger->created_at;
            $transaction_volume->updated_at = $ledger->updated_at;
            $transaction_volume->save();
        }
    }
}