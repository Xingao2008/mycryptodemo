<?php

namespace App\Observers;


use App\Models\Firewall;
use Carbon\Carbon;
use Cloudflare\API\Configurations\AccessRules;

class FirewallObserver
{
    private $rules;

    public function __construct()
    {
        $key = new \Cloudflare\API\Auth\APIKey(config('cloudflare.email'), config('cloudflare.api_key'));
        $adapter = new \Cloudflare\API\Adapter\Guzzle($key);
        $this->rules = new \App\Helpers\AccessRules($adapter);
    }

    public function creating(Firewall $firewall)
    {
        $date = Carbon::parse($firewall->expires);
        if($date->hour == 00 && $date->minute == 00)
        {
            $curDate = Carbon::now();
            $date->hour = $curDate->hour;
            $date->minute = $curDate->minute;
            $date->second = $curDate->second;
        }

        if (config('cloudflare.enable')) {

            $config = new AccessRules();
            $config->setIP($firewall->ip_address);

            $rsp = $this->rules->newRule(
                config('cloudflare.zone_id'),
                $firewall->whitelist ? 'whitelist' : 'block',
                $config,
                $firewall->reason
            );

            if($rsp) {
                $firewall->cloudflare_id = $rsp;
            }
        }
    }

    public function deleting(Firewall $firewall)
    {
        if (config('cloudflare.enable')) {
            if (!$firewall->cloudflare_id) {
                return false;
            }

            try {
                $this->rules->deleteRule(
                    config('cloudflare.zone_id'),
                    $firewall->cloudflare_id
                );
            } catch (\Exception $e) {

            }
        }
    }
}