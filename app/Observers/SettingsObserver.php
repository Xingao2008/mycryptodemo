<?php


namespace App\Observers;


use App\Models\Setting;

class SettingsObserver
{
    public function saved(Setting $setting)
    {
        Setting::cacheForget($setting->id);
    }
}