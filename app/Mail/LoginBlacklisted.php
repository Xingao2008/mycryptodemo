<?php

namespace App\Mail;

use App\Models\Firewall;
use App\Models\LoginAttempts;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class LoginBlacklisted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var Firewall
     */
    public $blacklist;
    /**
     * @var LoginAttempts
     */
    public $attempts;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Firewall $blacklist, Collection $attempts)
    {
        $this->blacklist = $blacklist;
        $this->attempts = $attempts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.login-blacklisted')->subject('Login Attempts Blacklisted');
    }
}
