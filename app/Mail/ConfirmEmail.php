<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $status_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($status_code)
    {
        $this->status_code = $status_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = 'Verify your email address';
        $status_code = $this->status_code;

        return $this
            ->view('email.verify')
            ->subject($subject)
            ->with(['confirmation' => $status_code]);
    }
}
