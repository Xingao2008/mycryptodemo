<?php

namespace App\Helpers;


use App\Exceptions\PoliPayException;
use Carbon\Carbon;
use stdClass;

class PoliPayApi
{
    /**
     * @var string
     */
    private $auth;
    /**
     * @var array
     */
    private $headers;
    /**
     * @var string
     */
    private $baseUrl = 'https://poliapi.apac.paywithpoli.com/api/v2/';

    /**
     * PoliPayApi constructor.
     */
    public function __construct()
    {
        $this->auth = base64_encode(config('polipay.merchant_code') . ':' . config('polipay.auth_code'));
        $this->headers = [
            'Authorization' => 'Basic ' . $this->auth,
            'Accept' => 'application/json'
        ];
    }

    public function getStatuses() {
        $tmp_statuses = [
            'Initiated',
            'FinancialInstitutionSelected',
            'EULAAccepted',
            'InProcess',
            'Unknown',
            'ReceiptUnverified',
            'Completed',
            'Incompatible',
            'Rejected',
            'Failed',
            'Cancelled',
            'TimedOut',
            'UnknownUser',
            'Incomplete',
            'Fraud'
        ];

        foreach ($tmp_statuses as $tmp) {
            $statuses[strtolower($tmp)] = $tmp;
        }

        return $statuses;
    }

    /**
     * Gets a single transaction
     *
     * @param string $token
     * @return stdClass
     */
    public function getTransaction(string $token): stdClass
    {
        $url = $this->baseUrl . 'Transaction/GetTransaction?Token=' . $token;

        return $this->_get($url);
    }

    public function getDailyTransactions(string $date): array
    {
        $date = Carbon::parse($date)->format('Y-m-d');
        $url = $this->baseUrl . 'Transaction/GetDailyTransactions?date=' . $date;

        return $this->_get($url);
    }

    /**
     * Sends a get request
     *
     * @param $url
     * @return stdClass
     */
    private function _get($url)
    {
        $rsp = \Unirest\Request::get($url, $this->headers);

        if ($rsp->code != 200) {
            throw new PoliPayException($rsp->body->ErrorMessage, $rsp->body->ErrorCode);
        }

        return $rsp->body;
    }
}