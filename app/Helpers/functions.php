<?php

use App\Models\Balance;
use App\Models\ExternalPrices;
use App\Bittrex\Bittrex;
use App\Models\User;
use App\Notifications\UsdtWarning;
// use Response;

require_once __DIR__ . '/validation.php';

function getRandomHex($num_bytes = 3)
{
    return bin2hex(openssl_random_pseudo_bytes($num_bytes));
}

function ubc()
{
    $ubc_prep = getRandomHex(3) . '-' . getRandomHex(3);
    $ubc = strtoupper($ubc_prep);
    return $ubc;
}

function first_name()
{
    $name = Auth::user()->name;
    $arr = explode(' ', trim($name));
    return $arr[0];
}

function tier(App\Models\User $user = null)
{
    $user = ($user) ?? \Auth::user();
    $v = \App\Models\Verification::where('user_id', '=', $user->id)->first();

    $tier = 'unverified';

    // checks first that all requirements met for silver
    if ($v->account == 'approved' &&
        $v->id_passport == 'approved' &&
        $v->id_drivers == 'approved' &&
        $v->id_bank == 'approved' &&
        $v->sms == 'approved') {

        $tier = 'silver';
    }

    // checks first that all requirements met for gold
    if ($tier == 'silver' &&
        $v->bank == 'approved' &&
        $v->tfa == 'approved') {
        // && $v->balance1 == 'approved'

        $tier = 'gold';
    }

    //TODO: Platinum
    if ($tier == 'gold' &&
        $v->balance2 == 'approved' &&
        $v->abn == 'approved') {

        $tier = 'platinum';
    }


    return $tier;
}

function btcFormat($value)
{
    return number_format((float)$value, '8');
}

function ethFormat($value)
{
    return $value;
}

function ltcFormat($value)
{
    return number_format((float)$value, '8');
}

function xrpFormat($value)
{
    return number_format((float)$value, '6', '.', '');
}

function audFormat($value)
{
    return number_format((float)$value, '2');
}

function formatCrypto($value)
{
    return number_format((float)$value, '6', '.', '');
}

function bchexdec($hex)
{
    if (strlen($hex) == 1) {
        return hexdec($hex);
    } else {
        $remain = substr($hex, 0, -1);
        $last = substr($hex, -1);
        return bcadd(bcmul(16, bchexdec($remain)), hexdec($last));
    }
}

function bcdechex($dec)
{
    $last = bcmod($dec, 16);
    $remain = bcdiv(bcsub($dec, $last), 16);

    if ($remain == 0) {
        return dechex($last);
    } else {
        return bcdechex($remain) . dechex($last);
    }
}

function wei2eth($wei)
{
    return bcdiv($wei, '1000000000000000000', 18);
}

function eth2wei($eth)
{
    return number_format((float)bcmul($eth, '1000000000000000000', 18), 0, ".", "");;
}

function hasMinusSign($value)
{
    return (substr(strval($value), 0, 1) == "-");
}

function getTxFees($currency)
{

    if (app()->environment() != 'production') {
        return '.000001';
    }

    if ($currency == 'xrp') {
        $data['type'] = 'xrp';
        $data['decimals'] = 6;
    }

    return bcmul(json_decode(file_get_contents('https://cd.fairdigital.com.au/api/getfees.php?coin=' . $data['type'])), 2.5, $data['decimals']);
}

function post_btcmarkets($uri, $query = null)
{
    // Instantiate a new GuzzleHTTP Client
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.btcmarkets.net']);

    // Get timestamp in milliseconds
    $timestamp = json_decode(number_format(round(microtime(1) * 1000), 0, '', ''));

    // Prepare the data
    if ($query) {
        $json_query = json_encode($query);
    }

    // Prepare the signature
    $raw_signature = $uri . "\n" . $timestamp . "\n" . $json_query;

    // Sign using HMAC SHA512, encode with base64
    $signature = base64_encode(hash_hmac("sha512", $raw_signature, base64_decode(config('crypto.btcmarket.secret_key')), true));

    // Set headers and body body
    $response = $client->request('POST', $uri, [
        'headers' => [
            'accept' => "application/json",
            'Content-Type' => "application/json",
            'User-Agent' => 'btc markets php client',
            'accept-charset' => "UTF-8",
            'apikey' => config('crypto.btcmarket.private_key'),
            'signature' => $signature,
            'timestamp' => $timestamp,
        ],

        'body' => $json_query,
    ]);

    // Retrieve the results
    return json_decode($response->getBody());
}

function get_btcmarkets($uri, $query = null)
{
    // Instantiate a new GuzzleHTTP Client
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.btcmarkets.net']);

    // Get timestamp in milliseconds
    $timestamp = json_decode(number_format(round(microtime(1) * 1000), 0, '', ''));

    // Prepare the data
    if ($query) {
        $json_query = json_encode($query);
        $query = ['query' => $query];
    } else {
        $json_query = null;
    }

    // Prepare the signature
    $raw_signature = $uri . "\n" . $timestamp . "\n" . $json_query;
    //$raw_signature = '/account/balance'."\n".$timestamp."\n".$json_query;

    // Sign using HMAC SHA512, encode with base64
    $signature = base64_encode(hash_hmac("sha512", $raw_signature, base64_decode(config('crypto.btcmarket.secret_key')), true));

    // Set headers and body body
    $response = $client->request('GET', $uri, [
        'headers' => [
            'accept' => "application/json",
            'Content-Type' => "application/json",
            'User-Agent' => 'btc markets php client',
            'accept-charset' => "UTF-8",
            'apikey' => config('crypto.btcmarket.private_key'),
            'signature' => $signature,
            'timestamp' => $timestamp,
        ],
        $query,
        //'body' => $json_query,
    ]);

    // Retrieve the results
    return json_decode($response->getBody());
}

function volume_capacity($data, $asks_or_bids, $amount)
{
    $price = $data->$asks_or_bids[0][0];
    $volume = $data->$asks_or_bids[0][1];
    $sum = 0;
    foreach ($data->$asks_or_bids as $key => $value) {
        $price = $value[0];
        $volume = $volume + $value[1];
        $sum = $sum + ($value[0] * $value[1]);
        if ($sum >= $amount) {
            break;
        }
    }
    return $price;
}

function prices($type)
{
    //type = raw, baseline, total
    $prices['btc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'btc')->first();
    $prices['eth'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'eth')->first();
    $prices['ltc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'ltc')->first();
    $prices['xrp'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'xrp')->first();
    $prices['pwr'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'pwr')->first();

    $setting_hidden_fee = 1 + (setting('hidden_fee') / 100);
    $setting_total_fee = 1 + ((setting('public_fee') / 100) + (setting('hidden_fee') / 100));
    //ask = use for users buying
    //bid = use for users selling
    //
    //4% hidden, 2.8% public, 6.8% total

    if ($type == '24') {
        $prices['btc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'btc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->first();
        $prices['eth'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'eth')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->first();
        $prices['ltc'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'ltc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->first();
        $prices['xrp'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'xrp')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->first();
        $prices['pwr'] = \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', 'pwr')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->first();

        return $prices;
    }

    if ($type == 'origin') {
        return $prices;
    }

    if ($type == 'raw') {
        foreach ($prices as $key => $value) {
            $prices[$key]->ask = audFormat($prices[$key]->ask);
            $prices[$key]->bid = audFormat($prices[$key]->bid);
        }
    }

    if ($type == 'baseline') {
        foreach ($prices as $key => $value) {
            $prices[$key]->ask = bcmul($prices[$key]->ask, $setting_hidden_fee, 2);
            $prices[$key]->bid = bcdiv($prices[$key]->bid, $setting_hidden_fee, 2);

        }
    }

    if ($type == 'total') {
        foreach ($prices as $key => $value) {
            $prices[$key]->ask = bcmul($prices[$key]->ask, $setting_total_fee, 2);
            $prices[$key]->bid = bcdiv($prices[$key]->bid, $setting_total_fee, 2);
        }
        foreach ($prices as $key => $value) {
            $prices[$key]->ask = manipulator_applier($prices[$key]->ask, $key, 'ask');
            $prices[$key]->bid = manipulator_applier($prices[$key]->bid, $key, 'bid');
        }
    }

    return $prices;
}

function latest_price($currency)
{
    if (!in_array($currency, config('currencies.fiatCurrencies'))) {
        return cache()->remember('latest_price_' . $currency, 1, function() use($currency) {
            return \App\Models\ExternalPrices::orderBy('created_at', 'desc')->where('currency', '=', $currency)->first()->bid;
        });

    } else {
        //TODO: double check with Matt and see should we still using 1 for AUD
        if ($currency === 'nzd') {
            return \App\Models\UsdExchange::orderBy('created_at', 'desc')->where('currency', '=', strtoupper($currency))->first()->value;
        } 
        return 1;
    }
}

function setting($type)
{
    /*
    if ($setting = \App\Models\Setting::where('id', '=', $type)->first()) {
        return $setting->value;
    } else {
        return NULL;
    }
    */
    return \App\Models\Setting::cacheGet($type);
}

function userVolume($user_id, $format = FALSE)
{
    $volumes = \App\Models\TransactionVolume::where('user_id', '=', $user_id)->get();
    $total = 0.00;
    foreach ($volumes as $volume) {
        $total = $total + $volume->value;
    }

    if (!$format) {
        return $total;
    } else {
        return number_format($total, 2);
    }
}

function userVolume30($user_id, $format = FALSE)
{
    $volumes = \App\Models\TransactionVolume::where('user_id', '=', $user_id)->where('created_at', '>=', \Carbon\Carbon::now()->subMonth())->get();
    $total = 0.00;
    foreach ($volumes as $volume) {
        $total = $total + $volume->value;
    }

    if (!$format) {
        return $total;
    } else {
        return number_format($total, 2);
    }

}

function trans_vol_24($user_id)
{
    $transaction_volume = \App\Models\TransactionVolume::where('user_id', '=', $user_id)->where('created_at', '>=', \Carbon\Carbon::now()->subHours(24))->get();
    $transactions = 0;
    if ($transaction_volume) {
        foreach ($transaction_volume as $transaction) {
            $transactions = $transactions + $transaction->value;
        }
    }

    return number_format($transactions, 2);
}

function trans_vol_24_buy($user_id)
{
    $transactions = \App\Models\TransactionVolume::where('user_id', '=', $user_id)
        ->where('action', '=', 'buy')
        ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(24))
        ->sum('value');


    return number_format($transactions, 2);
}

function trans_vol_24_sell($user_id)
{
    $transactions = \App\Models\TransactionVolume::where('user_id', '=', $user_id)
        ->where('action', '=', 'sell')
        ->where('created_at', '>=', \Carbon\Carbon::now()->subHours(24))
        ->sum('value');

    return number_format($transactions, 2);
}

function hidden_applier($price, $bidask)
{
    $hidden = setting('hidden_fee') / 100;

    if ($bidask == 'ask') {
        $price = ($price * $hidden) + $price;
    } elseif ($bidask == 'bid') {
        $price = $price - ($price * $hidden);
    } else {
        return NULL;
    }

    return $price;
}

function manipulator_applier($price, $type, $bidask)
{
    $fee = setting($type . '_fee') / 100;

    if ($bidask == 'ask') { //buy
        $price = ($price * $fee) + $price;
    } elseif ($bidask == 'bid') { //sell
        $price = $price - ($price * $fee);
    } else {
        return NULL;
    }

    return $price;
}

function formatted_user_balance($currency='aud')
{ 
    // if(!isset($currency) || empty($currency)) {
    //     $currency = strtolower(Auth::user()->address->country->currency_code) ?? 'aud' ;
    // }
    $country = \App\Models\Country::where('currency_code',strtoupper($currency))->where('available',1)->first();
    $balance = \App\Models\Balance::funds()[$currency][0]->balance ?? 0;
    if ($country) {
        return $country->currency_symbol . number_format($balance, 2).' '.$country->currency_code;
    }
    return number_format($balance, 8, '.', '');
}

function user_transaction_limits($key)
{
    return \App\Models\UserLimits::get_limit($key);
}

function get_app_css()
{
    return mix('/css/app.css');
}

function get_states_array()
{
    return [
        'VIC' => 'VIC',
        'ACT' => 'ACT',
        'QLD' => 'QLD',
        'NSW' => 'NSW',
        'TAS' => 'TAS',
        'WA' => 'WA',
        'SA' => 'SA',
        'NT' => 'NT'
    ];
}

function get_states_for_country($countryCode = 36)
{
    $data = [
        36 => [
            'VIC' => 'Victoria',
            'ACT' => 'Australian Capital Territory',
            'QLD' => 'Queensland',
            'NSW' => 'New South Wales',
            'TAS' => 'Tasmania',
            'WA' => 'Western Australia',
            'SA' => 'South Australia',
            'NT' => 'Northern Territory'
        ],
        554 => [
            'Auckland' => 'Auckland',
            'Canterbury' => 'Canterbury',
            'Gisborne' => 'Gisborne',
            'Hawke Bay' => 'Hawke Bay',
            'Marlborough' => 'Marlborough',
            'Manawatu-Wanganui' => 'Manawatu-Wanganui',
            'Nelson' => 'Nelson',
            'Northland' => 'Northland',
            'Otago' => 'Otago',
            'Southland' => 'Southland',
            'Tasman' => 'Tasman',
            'Taranaki' => 'Taranaki',
            'Waikato' => 'Waikato',
            'Wellington' => 'Wellington',
            'West Coast' => 'West Coast',
            'Chatham Islands Territory' => 'Chatham Islands Territory',
            'Bay of Plenty' => 'Bay of Plenty',

        ]
    ];

    return $data[$countryCode];
}

function get_countries_array()
{
    return Countries::orderBy('name', 'ASC')->get()->pluck('name', 'id');
}


function post_bittrex($uri, $query = null)
{
    // Instantiate a new GuzzleHTTP Client
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.btcmarkets.net']);

    // Get timestamp in milliseconds
    $timestamp = json_decode(number_format(round(microtime(1) * 1000), 0, '', ''));

    // Prepare the data
    if ($query) {
        $json_query = json_encode($query);
    }

    // Prepare the signature
    $raw_signature = $uri . "\n" . $timestamp . "\n" . $json_query;

    // Sign using HMAC SHA512, encode with base64
    $signature = base64_encode(hash_hmac("sha512", $raw_signature, base64_decode(env('SK_BTCMARKETS')), true));

    // Set headers and body body
    $response = $client->request('POST', $uri, [
        'headers' => [
            'accept' => "application/json",
            'Content-Type' => "application/json",
            'User-Agent' => 'btc markets php client',
            'accept-charset' => "UTF-8",
            'apikey' => env('PK_BTCMARKETS'),
            'signature' => $signature,
            'timestamp' => $timestamp,
        ],

        'body' => $json_query,
    ]);

    // Retrieve the results
    return json_decode($response->getBody());
}

function get_bittrex($uri, $query = null)
{
    // Instantiate a new GuzzleHTTP Client
    $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.btcmarkets.net']);

    // Get timestamp in milliseconds
    $timestamp = json_decode(number_format(round(microtime(1) * 1000), 0, '', ''));

    // Prepare the data
    if ($query) {
        $json_query = json_encode($query);
        $query = ['query' => $query];
    } else {
        $json_query = null;
    }

    // Prepare the signature
    $raw_signature = $uri . "\n" . $timestamp . "\n" . $json_query;
    //$raw_signature = '/account/balance'."\n".$timestamp."\n".$json_query;

    // Sign using HMAC SHA512, encode with base64
    $signature = base64_encode(hash_hmac("sha512", $raw_signature, base64_decode(env('SK_BTCMARKETS')), true));

    // Set headers and body body
    $response = $client->request('GET', $uri, [
        'headers' => [
            'accept' => "application/json",
            'Content-Type' => "application/json",
            'User-Agent' => 'btc markets php client',
            'accept-charset' => "UTF-8",
            'apikey' => env('PK_BTCMARKETS'),
            'signature' => $signature,
            'timestamp' => $timestamp,
        ],
        $query,
        //'body' => $json_query,
    ]);

    // Retrieve the results
    return json_decode($response->getBody());
}

function markedup_prices($time)
{

    $fee['btc'] = setting('btc_fee') / 100;
    $fee['ltc'] = setting('ltc_fee') / 100;
    $fee['eth'] = setting('eth_fee') / 100;
    $fee['xrp'] = setting('xrp_fee') / 100;
    $hidden = setting('hidden_fee') / 100;

    if ($time == "now") {
        $price['btc'] = ExternalPrices::where('currency', '=', 'btc')->latest()->first();
        $price['eth'] = ExternalPrices::where('currency', '=', 'eth')->latest()->first();
        $price['ltc'] = ExternalPrices::where('currency', '=', 'ltc')->latest()->first();
        $price['xrp'] = ExternalPrices::where('currency', '=', 'xrp')->latest()->first();

        foreach ($price as $key => $value) {
            //hidden
            $price[$key]->ask = ($price[$key]->ask * $hidden) + $price[$key]->ask;
            $price[$key]->bid = $price[$key]->bid - ($price[$key]->bid * $hidden);
            // manipulator
            $price[$key]->ask = ($price[$key]->ask * $fee[$key]) + $price[$key]->ask;
            $price[$key]->bid = $price[$key]->bid - ($price[$key]->bid * $fee[$key]);
        }

        return $price;
    } elseif ($time == "24") {
        $price_24['btc'] = ExternalPrices::where('currency', '=', 'btc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $price_24['eth'] = ExternalPrices::where('currency', '=', 'eth')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $price_24['ltc'] = ExternalPrices::where('currency', '=', 'ltc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
        $price_24['xrp'] = ExternalPrices::where('currency', '=', 'xrp')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();

        foreach ($price as $key => $value) {
            //hidden
            $price_24[$key]->ask = ($price_24[$key]->ask * $hidden) + $price_24[$key]->ask;
            $price_24[$key]->bid = $price_24[$key]->bid - ($price_24[$key]->bid * $hidden);
            // manipulator
            $price_24[$key]->ask = ($price_24[$key]->ask * $fee[$key]) + $price_24[$key]->ask;
            $price_24[$key]->bid = $price_24[$key]->bid - ($price_24[$key]->bid * $fee[$key]);
        }

        return $price_24;
    }
}

function usd_to_currency($value, $currency)
{
    $rate = App\Models\UsdExchange::where('currency', '=', $currency)->latest()->first()->value;

    $price = bcmul($value, $rate, 16);


    // IMPORTANT: Protects fiat transfer costs, at 1.25%
    $fiat_protection = bcmul($price, 1.0025, 16);

    return $fiat_protection;
}

function bittrex_volume_capacity($data, $asks_or_bids, $amount)
{
    $price = $data->$asks_or_bids[0]->Rate;
    $volume = $data->$asks_or_bids[0]->Quantity;
    $sum = 0;
    foreach ($data->$asks_or_bids as $key => $value) {
        $price = $value->Rate;
        $volume = $volume + $value->Quantity;
        $sum = $sum + ($value->Rate * $value->Quantity);
        if ($sum >= $amount) {
            break;
        }
    }
    return $price;
}

// NOTE: we flip the buy sell meaning, because on Bittrex, Buy = what people are BUYing it for (use for sells), vice versa
function bittrex_rate($coin, $buy_or_sell)
{
    if ($buy_or_sell == "buy") {
        $buy_or_sell = "sell";
    } elseif ($buy_or_sell == "sell") {
        $buy_or_sell = "buy";
    }
    $bittrex = new \App\Bittrex\Bittrex;

    $orderbook = $bittrex->getOrderBook('USDT-' . strtoupper($coin), 'both');

    $price = bittrex_volume_capacity($orderbook, $buy_or_sell, 10000);

    return $price;
}

function bittrex_rate_btc($coin, $buy_or_sell)
{
    if ($buy_or_sell == "buy") {
        $buy_or_sell = "sell";
    } elseif ($buy_or_sell == "sell") {
        $buy_or_sell = "buy";
    }
    $bittrex = new \App\Bittrex\Bittrex;

    $orderbook = $bittrex->getOrderBook('BTC-' . strtoupper($coin), 'both');

    $price = bittrex_volume_capacity($orderbook, $buy_or_sell, 1);

    return number_format($price, 16);
}

function bittrex_rate_pwr($coin, $buy_or_sell)
{
    if ($buy_or_sell == "buy") {
        $buy_or_sell = "sell";
    } elseif ($buy_or_sell == "sell") {
        $buy_or_sell = "buy";
    }
    $bittrex = new \App\Bittrex\Bittrex;

    $orderbook = $bittrex->getOrderBook('USDT-' . strtoupper($coin), 'both');

    $price = bittrex_volume_capacity($orderbook, $buy_or_sell, 3000);

    return $price;
}

function bittrex_rate_btc_pwr($coin, $buy_or_sell)
{
    if ($buy_or_sell == "buy") {
        $buy_or_sell = "sell";
    } elseif ($buy_or_sell == "sell") {
        $buy_or_sell = "buy";
    }
    $bittrex = new \App\Bittrex\Bittrex;

    $orderbook = $bittrex->getOrderBook('BTC-' . strtoupper($coin), 'both');

    $price = bittrex_volume_capacity($orderbook, $buy_or_sell, 0.4);

    return number_format($price, 16);
}

/**
 * Splits a BSB number into 000-000
 *
 * @param int $bsb
 * @return string
 */
function format_bsb($bsb): string
{
    return preg_replace('/(\d{3})(\d+)/', '$1-$2', $bsb);
}

function get_crypto_balance($currency) {
    return formatCrypto(\App\Models\Balance::funds()[$currency]['0']->balance);
}

function get_user_crypto_value($currency) {
    return audFormat(\App\Models\ExternalPrices::price()[$currency]->bid * \App\Models\Balance::funds()[$currency]['0']->balance);
}

function get_pending_crypto_buy($currency) {
    return formatCrypto(\App\Models\Balance::check_incomplete('CryptoBuy', $currency));
}

function get_pending_aud_sell($currency) {
    return audFormat(\App\Models\Balance::check_incomplete('CryptoSell', $currency));
}

function get_pending_aud_withdraw() {
    return audFormat(\App\Models\Balance::check_incomplete('FiatWithdraw'));
}

/**
 * Get the return json structure.
 *
 * @param  boolean $success
 * @param  string $message
 * @param  array $data
 * @param  integer $code
 *
 * @return \Illuminate\Http\JsonResponse
 */
function respondWithJson($success, $msg, $data, $code)
{
    return response()->json([
        'success' => $success,
        'message' => $msg,
        'data' => $data
    ], $code);
}

function remove_site_fee($amount)
{
    $fee = (setting('public_fee') / 100) * $amount;
    return $amount - $fee;
}

/**
 * Return the calculated currency base on our usd_exchange table rate
 */
function convertFiatCurrency($fromC='AUD', $toC='NZD', $amount=0)
{
    $fromC = strtoupper($fromC); //incase someone passed the lower case
    $toC = strtoupper($toC);
    $returnValue = 0;
    $amount = str_replace( ',', '', $amount ); // incase some weird characters passed

    if ($fromC == 'USD') {
        $exchangeRateFromCurrency = 1;
    } else {
        $exchangeRateFromCurrency = App\Models\UsdExchange::where('currency', $fromC)->orderBy('created_at', 'DESC')->first();
    }
    if ($toC == 'USD') {
        $exchangeRateToCurrency = 1;
    } else {
        $exchangeRateToCurrency = App\Models\UsdExchange::where('currency', $toC)->orderBy('created_at', 'DESC')->first();
    }


    if ($fromC == 'USD') {
        $returnValue = bcdiv($amount, $exchangeRateFromCurrency, 10);
    } else {
        $returnValue = bcdiv($amount, $exchangeRateFromCurrency->value,10);
    }
    if ($toC == 'USD') {
        $returnValue = bcmul($returnValue, $exchangeRateToCurrency, 10);
    } else {
        $returnValue = bcmul($returnValue, $exchangeRateToCurrency->value, 10);
    }


    return $returnValue;
}

/**
 * get bittrex balance
 */
function getBittrexUSDT()
{
    $myBittrex = new Bittrex();

    $myBalance = $myBittrex->getBalance('USDT')->Balance;

    return $myBalance;
}

function sendUsdtWarning()
{
    $user_email = setting('usdt_warning_user');
    $warning_amount = setting('usdt_warning_amount');
    $user = User::where('email', '=', $user_email)->first();

    $myBalance = getBittrexUSDT();

    if ($myBalance < $warning_amount && $user != null)
        $user->notify(new UsdtWarning());
    //TODO:: Error handling
}