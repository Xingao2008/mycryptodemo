<?php

namespace App\Helpers;

use Cloudflare\API\Adapter\Adapter;
use Cloudflare\API\Configurations\Configurations;

class AccessRules extends \Cloudflare\API\Endpoints\AccessRules
{
    public $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        parent::__construct($adapter);
    }

    public function newRule(string $zoneID, string $mode, Configurations $configuration, string $notes = null): string
    {
        $options = [
            'mode' => $mode,
            'configuration' => (object)$configuration->getArray()
        ];

        if ($notes !== null) {
            $options['notes'] = $notes;
        }


        $query = $this->adapter->post('zones/' . $zoneID . '/firewall/access_rules/rules', [], $options);


        $body = json_decode($query->getBody());

        if (isset($body->result->id)) {
            return $body->result->id;
        }

        return null;
    }

}