<?php

namespace App\Helpers;

use App\Models\User;

use GuzzleHttp\Client;
use Carbon\Carbon;

class VixVerifyHelper
{
    private $id;
    private $user;
    private $token;

    public function __construct($opt)
    {
        $this->id = (isset($opt['id'])) ? $opt['id'] : null;
        $this->user = (isset($opt['user'])) ? $opt['user'] : null;
        $this->token = (isset($opt['token'])) ? $opt['token'] : null;
    }

    public function verify()
    {
        $status = '';

        if ( !$user = User::find($this->user) ) {
            return false;
        }

        try {

            $client = new Client();
            $response = $client->request('GET', config('vix.url'), [
                'query' => [
                    'accountId' => config('vix.account_id'),
                    'webServicePassword' => config('vix.password'),
                    'verificationToken' => $this->token,
                ]
            ]);

            $vix_error_message = false;
            $success = false;
            $body = json_decode($response->getBody());
            if (!$body->error) {
                $success = [
                    'VERIFIED',
                    'VERIFIED_ADMIN',
                    'VERIFIED_WITH_CHANGES'
                ];

                $status = $body->verificationResult;
                if (in_array($body->verificationResult, $success))
                    $success = true;
                else
                    $success = false;
                    $data = [
                        'email' => $user->email,
                        'name' => $user->name,
                        'id' => $user->id,
                    ];

                    \App\Models\Emails::create([
                        'user_id'    => $user->id,
                        'to'      => $user->email,
                        'subject' => 'myCryptoWallet: A User Failed Their Verification',
                        'data' => json_encode($data),
                    ]);
            } else {
                if ($body->error) {
                    $status = 'ERROR';
                    $vix_error_message = $body->errorMessage;
                }
                $data = [
                    'email' => $user->email,
                    'name' => $user->name,
                    'id' => $user->id,
                ];

                \App\Models\Emails::create([
                    'user_id'    => $user->id,
                    'to'      => $user->email,
                    'subject' => 'myCryptoWallet: A User Failed Their Verification',
                    'data' => json_encode($data),
                ]);
            }

            return ['success' => $success, 'vix_status' => $status, 'vix_error_message' => $vix_error_message];

        } catch (Exception $e) {
            return json_encode($e->getMessage());
        }
    }


}
