<?php

/**
 * Checks a BTC address against a regex
 *
 * @param $address
 * @return bool
 */
function validate_btc(string $address): bool
{
    return (bool)preg_match('/^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/', $address);
}

/**
 * Checks a LTC address against a regex
 *
 * @param string $address
 * @return bool
 */
function validate_ltc(string $address): bool
{
    return (bool)preg_match('/^[LM][a-km-zA-HJ-NP-Z1-9]{26,34}$/', $address);
}

/**
 * Checks XRP address against regex
 *
 * @param string $address
 * @return bool
 */
function validate_xrp(string $address): bool
{
    return (bool)preg_match('/^r[rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz]{27,35}$/', $address);
}

/**
 * Checks a ETH address
 *
 * @param string $address
 * @return bool
 */
function validate_eth(string $address): bool
{
    if (!preg_match('/^(0x)?[0-9a-f]{40}$/i', $address)) {
        // check if it has the basic requirements of an address
        return false;
    } elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/', $address) || preg_match('/^(0x)?[0-9A-F]{40}$/', $address)) {
        // If it's all small caps or all all caps, return true
        return true;
    } else {
        // // Check each case
        // $address = str_replace('0x', '', $address);
        // $addressHash = hash('sha3', strtolower($address));
        // $addressArray = str_split($address);
        // $addressHashArray = str_split($addressHash);

        // for ($i = 0; $i < 40; $i++) {
        //     // the nth letter should be uppercase if the nth digit of casemap is 1
        //     if ((intval($addressHashArray[$i], 16) > 7 && strtoupper($addressArray[$i]) !== $addressArray[$i]) || (intval($addressHashArray[$i], 16) <= 7 && strtolower($addressArray[$i]) !== $addressArray[$i])) {
        //         return false;
        //     }
        // }
        return true;
    }
}
