<?php

namespace App\Services;

use App\Models\CryptoSell;
use App\Models\UserLimits;
use Illuminate\Http\Request;
use App\Classes\BuySellClasses\Sell\Sell;


class SellCurrencyService
{
    public function handle(Request $request)
    {
        $request->validate([
            'amount' => "required|numeric|between:0.000001,100000.00000000",
            'currency' => 'required|in:btc,ltc,eth,xrp,pwr',
        ]);

        $sell = new Sell(new CryptoSell);
        $user = $request->user();

//        record each of client requests
        try {
            if (!$sell->saveRequest($request, 'sell')) {
                throw new \Exception('User request fail');
            }
        } catch(\Exception $e) {
            throw $e;
        }

        \DB::beginTransaction();
        try {
            $balance_raw = $sell->getBalanceLocked($user->id, $request->currency);
            $balance = $balance_raw->balance;

            if (bccomp($request->get('amount_' . $request->currency), $balance, 18) == 1) {
                throw new \Exception('Insufficient funds.');
            }

            if ($request->amount < 25.00) {
                throw new \Exception('You must sell at least $25 worth of coins (you tried selling $' . $request->amount . '). Remember to factor in the ' . $sell->getPublicFee() . '% maker/taker fee and miner fee.');
            }

            if ($request->amount > UserLimits::get_limit('sell_transaction_limit')) {
                throw new \Exception('You cannot sell more than $' . UserLimits::get_limit('sell_transaction_limit') . ' worth of coins (you tried selling $' . $request->amount . '). Remember to factor in the ' . $sell->getPublicFee() . '% maker/taker fee and miner fee.');
            }

            if (trans_vol_24_sell($user->id) > UserLimits::get_limit('sell_daily_limit')) {
                throw new \Exception('You have exceeded the transaction volume limit of $' . UserLimits::get_limit('sell_daily_limit') . ' for the past 24 hours. Your current transaction volume is: $' . trans_vol_24($user->id));
            }
        } catch(\Exception $e) {
            throw $e;
        }
//        end of frontend exception

//        stage I, bankend, save the exception
        try {
            if (!$sell->initialSell($request)) {
                throw new \Exception('Fail to initialize the sell');
            }

            if ($sell->markIfNoWallet()) {
                if (is_null($sell->generateWallet($user->id, $request->currency))) {
                    throw new \Exception('Unable to generate wallet');
                }
            }

            if (!$sell->initialLedger()) {
                throw new \Exception('save copy of crypto-sell fail');
            }
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $sell->getCrypto()->status = "sell market error:".$e->getMessage();
            } else {
                $sell->getCrypto()->status = "failed, no message";
            }
            $sell->getCrypto()->save();
        }
//        first stage finish

//        save the session
        $sell->saveSession('sell');


//        stage II
        try {
            if (!$sell->markIfNegWallet()) {
                $sell->SellOthers();
            } else {
                throw new \Exception('Insufficient fund, please top up your account.');
            }
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $sell->getCrypto()->status = "sell market error:".$e->getMessage();
            } else {
                $sell->getCrypto()->status = "failed, no message";
            }
            $sell->getCrypto()->save();
        }
//        end of stage II

//        final stage, add the lock, operate balance
        try {
            if (!$sell->deductCryptoBalance()) {
                throw new \Exception('deduct crypto balance fail');
            }

            if (!$sell->topupCurrencyBalance()) {
                throw new \Exception('Unable credit user the currency balance');
            }

            //doing the real treat, write the error if fail
            if ($sell->getCrypto()->type === 'pwr') {
                $sell->ContactBittrexWithPowr();
            } else {
                $sell->ContactBittrex();
            }
            //finish selling

        } catch (\Exception $e) {
            \DB::rollBack();
            if(!is_null($e->getMessage())) {
                $sell->getCrypto()->status = "sell market error:".$e->getMessage();
            } else {
                $sell->getCrypto()->status = "failed, no message";
            }
            $sell->getCrypto()->save();
        }
        \DB::commit();

        // send email
        $data = [
            'email' => $user->email,
            'amount' => $sell->getCrypto()->amount,
            'received' => $sell->getCrypto()->total,
            'fee' => $sell->getPublicFee() * $sell->getCrypto()->total,
            'type' => $sell->getCrypto()->type,
            'currency' => $sell->getCrypto()->currency
        ];
        $sell->sendEmail('sell-receipt', 'sell', $data);

        //remind to recharge
        $sell->remindToRecharge();
    }
}
