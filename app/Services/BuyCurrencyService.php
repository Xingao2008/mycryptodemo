<?php

namespace App\Services;

use App\Models\CryptoBuy;
use App\Models\UserLimits;
use Illuminate\Http\Request;
use App\Classes\BuySellClasses\Buy\Buy;

class BuyCurrencyService
{
    public function handle(Request $request)
    {
        $request->validate([
            'amount' => "required|numeric|regex:/^\d*(\.\d{1,2})?$/|between:50.00," . UserLimits::get_limit('buy_transaction_limit'),
            'currency' => 'required|in:btc,ltc,eth,xrp,pwr',
        ]);

        $buy = new Buy(new CryptoBuy);
        $user = $request->user();

//        record each of client requests
        try {
            if (!$buy->saveRequest($request, 'buy')) {
                throw new \Exception('User request fail');
            }
        } catch(\Exception $e) {
            throw $e;
        }

        \DB::beginTransaction();
        try {
            $balance_raw = $buy->getBalanceLocked($user->id, $request->fiat_currency);
            $balance = $balance_raw->balance;

            if (bccomp($request->amount, $balance, 18) == 1) {  //check dollar balance enough or not
                throw new \Exception('Insufficient funds.');
            }

            if (trans_vol_24_buy($user->id) > UserLimits::get_limit('buy_daily_limit')) {  //check reach the daily limits
                throw new \Exception('You have exceeded the transaction volume limit of $' . UserLimits::get_limit('buy_daily_limit') . ' for the past 24 hours. Your current transaction volume is: $' . trans_vol_24($user->id));
            }
        } catch (\Exception $e) {
            throw $e;
        }
//        end of throw out frontend exception

//        bankend operation, save the exception, stage I
        try {
            if(!$buy->initialBuy($request)) { //initialize the crypto buy
                throw new \Exception('Fail to initialize the buy');
            }

            if($buy->markIfNoWallet()) {
                if(is_null($buy->generateWallet($user->id, $request->currency))) {
                    throw new \Exception('Unable to generate wallet');
                }
            }

            if(!$buy->initialLedger()) {
                throw new \Exception('save copy of crypto-buy fail');
            }
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $buy->getCrypto()->status = "buy market error:".$e->getMessage();
            } else {
                $buy->getCrypto()->status = "failed, no message";
            }
            $buy->getCrypto()->save();
        }
//        first stage finish

//        save the session
        $buy->saveSession('buy');


//        stage II
        try {
            if (!$buy->markIfNegWallet()) {
                $buy->purchaseOthers();
            } else {
                throw new \Exception('Insufficient fund, please top up your account.');
            }
        } catch (\Exception $e) { // save into table instead of throw out
            if(!is_null($e->getMessage())) {
                $buy->getCrypto()->status = "buy market error:".$e->getMessage();
            } else {
                $buy->getCrypto()->status = "failed, no message";
            }
            $buy->getCrypto()->save();
        }
//        end of stage II

//        final stage, add the lock, operate balance
        try {
            if(!$buy->deductCurrencyBalance()) { // add lock
                throw new \Exception('deduct currency balance fail');
            }

            if (!$buy->addCryptoBalance()) { // add lock
                throw new \Exception('Unable credit user the crypto balance');
            }

            //doing the real treat, write the error if fail
            if ($buy->getCrypto()->type === 'pwr') {
                $buy->ContactBittrexWithPowr();
            } else {
                $buy->ContactBittrex();
            }
            //finish buying
        } catch (\Exception $e) {
            \DB::rollBack();
            if(!is_null($e->getMessage())) {
                $buy->getCrypto()->status = "buy market error:".$e->getMessage();
            } else {
                $buy->getCrypto()->status = "failed, no message";
            }
            $buy->getCrypto()->save();
        }
        \DB::commit();

        // send email
        $data = [
            'email' => $user->email,
            'amount' => $buy->getCrypto()->amount,
            'received' => $buy->getCrypto()->total,
            'fee' => $buy->getPublicFee() * $buy->getCrypto()->total,
            'type' => $buy->getCrypto()->type,
            'currency' => $buy->getCrypto()->currency
        ];
        $buy->sendEmail('buy-receipt', 'buy', $data);

        //remind to recharge
        $buy->remindToRecharge();
    }
}