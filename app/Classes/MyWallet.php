<?php

namespace App\Classes;

use App\Models\Wallet;
use App\Models\Ripple;
use App\Models\ExternalPrices;
use App\Models\Balance;
use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use DB;

class MyWallet
{
    public $returnArray;
    public $path;
    public $userId;
    public $obj;
    public $ubc;
    //lock it to 3 times for now
    protected $numRetry;

    public function __construct()
    {
        $this->returnArray = [];
        $this->numRetry = 3;
    }

    public function generateWallet()
    {
        $obj = '';
        if ($this->path == 'btc' || $this->path == 'ltc') {
            $obj = $this->createBitcoinWallet();
        }
        elseif ($this->path == 'eth' || $this->path == 'pwr') {
            $obj = $this->createEthWallet();
        }
        // elseif ($this->path == 'xrp') {
        //     $obj = $this->createRippleWallet();
        // }
        return $obj;
    }

    /**
     * Function to create the bitcoinc wallet
     * litecoin using the same mechanism so we can also put it here
     */
    public function createBitcoinWallet()
    {
        $json = $this->connectDaemonServer('https://cd.fairdigital.com.au/api/generatewallet.php?coin='.$this->path);
        if (isset($json) && !empty($json)) {
            $obj = json_decode($json);
            if ($this->saveWallet($obj)) {
                return $obj;
            }
        }
        throw new \Exception('There was an issue generating your wallet, please try again or contact support');


    }

    /**
     * Ethereum wallet need to treat a little bit different
     * Power will also goes to here and only create one eth address
     */
    public function createEthWallet()
    {
        $json = $this->connectDaemonServer('https://cd.fairdigital.com.au/api/generatewallet.php?coin=eth&password='.$this->ubc);

        if (isset($json) && !empty($json)) {
            $obj = json_decode($json);
            if ($this->saveWallet($obj)) {
                return $obj;
            }
        }
        throw new \Exception('There was an issue generating your wallet, please try again or contact support');
    }

    /**
     * Save the wallet record into DB
     */
    public function saveWallet($obj)
    {
        $save_wallet = new Wallet;
        $save_wallet->user_id = $this->userId;
        $save_wallet->type = $this->path;
        $save_wallet->address = $obj;
        return $save_wallet->save();
    }

    /**
     * Create Ripple wallet
     * Special treatment
     */
    public function createRippleWallet()
    {
        $json = $this->connectDaemonServer('https://cd.fairdigital.com.au/api/generatewallet.php?coin=xrp');
        $obj = json_decode($json);
        if(isset($obj->account_id) && substr($obj->account_id, 0, 1) === 'r') {
            \DB::beginTransaction();
            try {
                $this->saveWallet($obj->account_id);

                $xrp_wallet_data = new Ripple;
                $xrp_wallet_data->user_id = $this->userId;
                $xrp_wallet_data->account_id = $obj->account_id;
                $xrp_wallet_data->key_type = $obj->key_type;
                $xrp_wallet_data->master_key = $obj->master_key;
                $xrp_wallet_data->master_seed = $obj->master_seed;
                $xrp_wallet_data->master_seed_hex = $obj->master_seed_hex;
                $xrp_wallet_data->public_key = $obj->public_key;
                $xrp_wallet_data->public_key_hex = $obj->public_key_hex;
                $xrp_wallet_data->status = $obj->status;
                $xrp_wallet_data->save();
            } catch (\Exception $e)
            {
              \DB::rollBack();
              return 'There was an issue generating your wallet, please try again or contact support. ERR: DB_FAIL';

            }
            \DB::commit();
            return'Success! Your Ripple address is: '.$obj->account_id;
          } else {
            return 'There was an issue generating your wallet, please try again or contact support. ERR: AP_INVAL';
          }
    }

    /**
     * Try to recall our daemon server if fail
     */
    private function connectDaemonServer($url = '')
    {
        $json = false;
        $i = 0;
        while(!$json && $i < $this->numRetry)
        {
            $json = @file_get_contents($url);
            $i++;
            if (!$json) {
                usleep(10);
            }
        }
        return $json;
    }

    /**
     * Return all the balance from the wallet in a group
     */
    public static function getAllBalancesForWallet()
    {

        $user = \Auth::user();

        $price = ExternalPrices::price();
       
        $wallets = Balance::where('user_id', '=', $user->id)->select('currency', 'balance')->get(); 
        if (!isset($wallets) || empty($wallets)) {
            return [];
        }
        $wallet['total'] = 0;
        foreach($wallets as $key=>$value) {
            $wallet[$value->currency] = $value;
            // If the currency is AUD, just add it, else ask * balance
            $wallet['total'] += ($value->currency == 'aud') ? $value->balance : $price[$value->currency]->bid * $value->balance;
            if ($value->currency != 'aud') {
                $wallet[$value->currency]['get_crypto_balance'] = get_crypto_balance($value->currency);
                $wallet[$value->currency]['get_user_crypto_value'] = get_user_crypto_value($value->currency);
            }
        }
        return $wallets;
    }
    
}
