<?php
namespace App\Classes;
use SoapClient;
use Auth;
use App\Models\User;
use App\Classes\WsseAuthHeader;
use App\Classes\GBGClasses\CurrentAddress;
use App\Classes\GBGClasses\DrivingLicense;
use App\Classes\GBGClasses\EuropeanIdentityCard;
use App\Classes\GBGClasses\InternationalPassport;
use App\Classes\GBGClasses\PersonalDetails;
use App\Classes\GBGClasses\ProfileIDVersion;
use App\Classes\GBGClasses\IdentityCard;
use App\Classes\GBGClasses\SocialSecurity;
use App\Classes\GBGClasses\ContactDetails;

/**
 * According to GBG document, some of the validation only available in certain country
 * ns:EuropeanIdentityCard -> UK
 * ns:InternationalPassport -> USA
 * US component only available to USA
 * 
 * Further document: http://www.id3globalsupport.com/sample-code/xml-country-specific
 */
class GBGSoapClient 
{
    private $username;
    private $password;
    private $wsse_header;
    private $options;
    private $wsdl;
    private $wss_ns;
    public $soapClient;
    private $profile;
    private $profileId;

    public function __construct() 
    {
        $this->username = env('ID3GLOBAL_USERNAME');
        $this->password = env('ID3GLOBAL_PASSWORD');
        $this->wsdl = env('ID3GLOBAL_URL');
        $this->wsse_header = new WsseAuthHeader($this->username, $this->password);
        $this->options = [
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'wdsl_local_copy' => true
        ];
        //$this->wsse_header = new WsseAuthHeader($this->username, $this->password);
        // $this->soapClient = new SoapClient(env('ID3GLOBAL_URL'), array('soap_version'   => SOAP_1_2));
        $this->soapClient = new SoapClient($this->wsdl, $this->options);
        $this->soapClient->__setSoapHeaders(array(
            $this->wsse_header
        ));    
    }

    public function setProfile($code = 'US')
    {
        switch ($code) {
            case 'US':
                $this->profileId = env('ID3GLOBAL_USPROFILEID');
                break;
            case 'UK':
                $this->profileId = env('ID3GLOBAL_UKPROFILEID');
                break;
            case 'CA':
                $this->profileId = env('ID3GLOBAL_CAPROFILEID');
                break;
            default:
                $this->profileId = 'd8866758-7f66-458e-8c0e-4f7e3c196758';
        }
    }

    /**
     * Testing API to test the username password is correct and the soap is working fine
     */
    public function test()
    {
        $objParam = new \stdClass();
        $objParam->AccountName = 'admin@myholdingenterprises.com';
        $objParam->Password = 'Qh/9P}xr5A|q3#jM';
        if (is_soap_fault($this->soapClient))
        {
            throw new Exception(" {$this->soapClient->faultcode}: {$this->soapClient->faultstring} ");
        }

        $objRet = null;
        try
        {
            $objRet = $this->soapClient->CheckCredentials($objParam);
            echo '<pre>';
            print "Decision Band :" . ($objRet->CheckCredentialsResult->OrgName) . "<br />";
            echo '</pre>';
        }

        catch(Exception $e)
        {
            echo "<pre>";
            print_r($e);
            echo "</pre>";
        }

        if (is_soap_fault($objRet))
        {
            throw new Expception(" {$objRet->faultcode}: {$objRet->faultstring} ");
        }
    }

    public function sendAuthenticateSP($data)
    {
        //$user = $data['user']; 
        $profileIDVersion = new ProfileIDVersion();
        $profileIDVersion->ID = $this->profileId;
        $personalDetails = new PersonalDetails();
        collect($data['personalDetails'])->map(function ($v, $k) use ($personalDetails) {
            $personalDetails->$k = $v;
        });
        // $personalDetails->Title = 'Mr';
        // $personalDetails->Forename = 'barbara';
        // $personalDetails->Surname = 'miller';
        // $personalDetails->Gender = 'Male';
        // $personalDetails->DOBDay = '24';
        // $personalDetails->DOBMonth = '2';
        // $personalDetails->DOBYear = '1983';

        $currentAddress = new CurrentAddress();
        collect($data['currentAddress'])->map(function ($v, $k) use ($currentAddress) {
            $currentAddress->$k = $v;
        });

        // $currentAddress->Country = 'United States';
        // $currentAddress->Street = 'cimarron rd e';
        // $currentAddress->City = 'LOMBARD';
        // $currentAddress->ZipPostcode = '60148';
        // $currentAddress->StateDistrict = 'IL';
        // $internationalPassport = new InternationalPassport();
        // $internationalPassport->Number = '';
        // $internationalPassport->ExpiryDay = 10;
        // $internationalPassport->ExpiryMonth = 4;
        // $internationalPassport->ExpiryYear = 20;
        // $internationalPassport->CountryOfOrigin = 'AUSTRALIA';
        // $internationalPassport->IssueDay = 10;
        // $internationalPassport->IssueMonth = 4;
        // $internationalPassport->IssueYear = 10;

        $objParam = new \stdClass();
        $objParam->ProfileIDVersion = $profileIDVersion;
        $objParam->InputData = new \stdClass();
        $objParam->InputData->Personal = new \stdClass();
        $objParam->InputData->Personal->PersonalDetails = $personalDetails;
        $objParam->InputData->Addresses = new \stdClass();
        $objParam->InputData->Addresses->CurrentAddress = $currentAddress;

        $objParam->InputData->ContactDetails = new \stdClass();
        $objParam->InputData->ContactDetails->MobileTelephone = $data['contactDetails']['MobileTelephone'];
        // $objParam->IdentityDocuments = new \stdClass();
        // $objParam->IdentityDocuments->InternationalPassport = $internationalPassport;
        /**
         * GBG require some special format for the ID card for individual country. Thus, hardcode as follow
         */
        
        if ($this->profileId == 'US') {
            //USA
            $objParam->InputData->IdentityDocuments = new \stdClass();
            if (isset($data['passport'])) {
                $passport = new InternationalPassport();
                collect($data['passport'])->map(function ($v, $k) use ($passport) {
                    $passport->$k = $v;
                });
                $objParam->InputData->IdentityDocuments->InternationalPassport = $passport;
        
            }
            $objParam->InputData->IdentityDocuments->US = new \stdClass();
            $objParam->InputData->IdentityDocuments->US->SocialSecurity = new \stdClass();
            $objParam->InputData->IdentityDocuments->US->SocialSecurity->Number = $data['GBGIdentityID'];


        } elseif ($this->profileId == 'NZ'){
            //NZ optional
            // $objParam->InputData->NewZealand = new \stdClass();
            // $objParam->InputData->NewZealand->Number= Auth::user()->verifications->GBGIdentityID;
            // $objParam->InputData->NewZealand->Version= Auth::user()->verifications->GBGIdentityID;
        } elseif ($this->profileId == 'UK') {
            //UK - will have another option to second verify
            if (isset($data['eid'])) {
                $objParam->InputData->IdentityDocuments = new \stdClass();
                $eid = new EuropeanIdentityCard();
                collect($data['eid'])->map(function ($v, $k) use ($eid) {
                    $eid->$k = $v;
                });
                $objParam->InputData->IdentityDocuments->EuropeanIdentityCard = $eid;
        
            }
            

        } elseif ($this->profileId == 'JP') {
            //Jap
        } elseif ($this->profileId == 'CA') {
            //Canada
            $objParam->InputData->IdentityDocuments = new \stdClass();
            $objParam->InputData->IdentityDocuments->Canada = new \stdClass();
            $objParam->InputData->IdentityDocuments->Canada->SocialInsuranceNumber= $data['GBGIdentityID'];
        } else {
            //Aus
        }

        if (is_soap_fault($this->soapClient))
        {
            // throw new Exception(" {$this->soapClient->faultcode}: {$this->soapClient->faultstring} ");
            return [
                'success' => false,
                'error' => [
                    'code' => $this->soapClient->faultcode,
                    'message' => $this->soapClient->faultstring
                ]
            ];
            // return respondWithJson(false, $this->soapClient->faultstring, '', 200);
        }

        $response = null;
        try
        {
            $response = $this->soapClient->AuthenticateSP($objParam);
            // return respondWithJson(true, 'Soap call success', 
            //     (array)$response->AuthenticateSPResult
            // , 200);
            return [
                'success' => true,
                'data' => (array)$response->AuthenticateSPResult
            ];
            
            // print_r($response->AuthenticateSPResult->Score);
            // print_r($response->AuthenticateSPResult->BandText);
            // echo '</pre>';
        }catch(Exception $e)
        {
            // return respondWithJson(false, '', $e, 200);
            return [
                'success' => false,
                'error' => $e
            ];
        }

        if (is_soap_fault($response))
        {
            return [
                'success' => false,
                'error' => [
                    'code' => $this->soapClient->faultcode,
                    'message' => $this->soapClient->faultstring
                ]
            ];
        }
    }
}


?>