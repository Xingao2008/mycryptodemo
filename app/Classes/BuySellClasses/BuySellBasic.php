<?php

namespace App\Classes\BuySellClasses;

use App\Models\User;
use App\Models\Wallet;
use App\Models\Balance;
use App\Classes\MyWallet;
use App\Models\Log;

trait BuySellBasic
{
    function saveRequest($req, $type) {
        $data = array(
            'trade_type' => $type,
            'crypto' => $req->currency,
            'crypto_amount' => $req->get('amount_'.$req->currency),
            'currency' => $req->fiat_currency,
            'currency_amount' => $req->amount,
        );
        $user_request = new Log;
        $user_request->user_id = $req->user()->id;
        $user_request->severity = 'buy_sell_requests';
        $user_request->detail = json_encode($data);
        return $user_request->save();
    }

    function getUser($user_id)
    {
        $user = User::where('id', '=', $user_id)->first();

        return $user;
    }

    function getWallet($user_id, $type)
    {
        $wallet = Wallet::where('user_id', '=', $user_id)->where('type', '=', $type)->first();

        return $wallet;
    }

    function getBalanceLocked($user_id, $currency)
    {
        $balance = Balance::where('user_id', '=', $user_id)->where('currency', '=', $currency)->lockForUpdate()->first();

        return $balance;
    }

    function getHiddenFee()
    {
        return setting('hidden_fee') / 100;
    }

    function getPublicFee()
    {
        return setting('public_fee') / 100;
    }

    function getTotalFee()
    {
        return ($this->getHiddenFee() + $this->getPublicFee());
    }

    function getCryptoFee()
    {
        return setting($this->cryptoTrade->type.'_fee') / 100;
    }

    function getFees($amount)
    {
        return bcmul($amount, ($this->getTotalFee() + $this->getCryptoFee()), 16);
    }

    function getCryptoAddress($user_id, $type)
    {
        if ($type == 'pwr') {
            $user_address = Wallet::where('user_id', '=', $user_id)->where('type', '=', 'eth')->first();
        } else {
            $user_address = Wallet::where('user_id', '=', $user_id)->where('type', '=', $type)->first();
        }

        if(!$user_address) {
            $this->generateWallet($user_id, $type);

            if ($type == 'pwr') {
                $user_address = Wallet::where('user_id', '=', $user_id)->where('type', '=', 'eth')->first();
            } else {
                $user_address = Wallet::where('user_id', '=', $user_id)->where('type', '=', $type)->first();
            }
        }

        return $user_address;
    }

    function convertPrice($currency, $price)
    {
        return convertFiatCurrency('aud', $currency, $price);  //price in other than AUD
    }

    function convertUsdOther($rate, $currency)
    {
        return usd_to_currency($rate, $currency);
    }

    function getBittrexRate($coinType, $type)
    {
        return bittrex_rate($coinType, $type);
    }

    function getBTCBittrexRate($coinType, $type)
    {
        return bittrex_rate_btc($coinType, $type);
    }

    function remindToRecharge()
    {
        return sendUsdtWarning();
    }

    function generateWallet($user_id, $type)
    {
        $myWallet = new MyWallet();
        $myWallet->userId = $user_id;
        $myWallet->path = $type;
        return $myWallet->generateWallet();
    }
}