<?php

namespace App\Classes\BuySellClasses\Buy;

use App\Classes\BuySellClasses\Trade;
use App\Models\CryptoBuy;
use App\Bittrex\Bittrex;
use App\Models\UserLimits;
use App\Models\Ledger;


class Buy extends Trade
{
    private $cryptoBuy;

    public function __construct(CryptoBuy $cryptoBuy)
    {
        $this->cryptoBuy = $cryptoBuy;
        parent::__contruct($this->cryptoBuy);
    }

    public function ContactBittrexWithPowr($msg = null)
    {
        $market = "USDT-BTC";
        $quantity = bcmul($this->cryptoBuy->amount, $this->getBTCBittrexRate('powr', 'buy'), 16); //convert pwr to btc
        $type = 'btc';
        $bittrex = new Bittrex;
        $result = false;
        $rate = $this->getBittrexRate($type, 'buy');

        try {
            $result = $bittrex->buyLimit($market, $quantity, $rate);
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoBuy->status = "buy market error:".$e->getMessage();
            } else {
                $this->cryptoBuy->status = "failed, no message";
            }
            $this->cryptoBuy->save();
        }

        sleep(1);

        $market = "BTC-POWR";
        $quantity = $this->cryptoBuy->amount; //fee applied already
        $type = 'powr'; //pwr, naming different way
        $rate = $this->getBTCBittrexRate($type, 'buy');

        try {
            $result = $bittrex->buyLimit($market, $quantity, $rate);
            $this->cryptoBuy->bittrex_rate = $this->convertUsdOther(
                bcmul(
                    $this->getBTCBittrexRate('powr', 'buy'),
                    $this->getBittrexRate('btc', 'buy'),
                    16)
            , $this->cryptoBuy->currency);
            $this->cryptoBuy->bittrex_order_id = $result->uuid;
            if ($msg === null) {
                $this->cryptoBuy->status = 'complete';
            } else {
                $this->cryptoBuy->status = $msg;
            }
            $this->cryptoBuy->save();

        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoBuy->status = "buy market error:".$e->getMessage();
            } else {
                $this->cryptoBuy->status = "failed, no message";
            }
            $this->cryptoBuy->save();
        }

        return $result;
    }

    public function ContactBittrex($msg = null)
    {
        $market = "USDT-".strtoupper($this->cryptoBuy->type);
        $quantity = $this->cryptoBuy->amount; //fee has been applied
        $type = $this->cryptoBuy->type;
        $bittrex = new Bittrex;
        $result = false;
        $rate = $this->getBittrexRate($type, 'buy');

        try {
            $result = $bittrex->buyLimit($market, $quantity, $rate);
            $this->cryptoBuy->bittrex_rate = $this->convertUsdOther($this->getBittrexRate($this->cryptoBuy->type, 'buy'), $this->cryptoBuy->currency);
            $this->cryptoBuy->bittrex_order_id = $result->uuid;
            if ($msg === null) {
                $this->cryptoBuy->status = 'complete';
            } else {
                $this->cryptoBuy->status = $msg;
            }
            $this->cryptoBuy->save();

        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoBuy->status = "buy market error:".$e->getMessage();
            } else {
                $this->cryptoBuy->status = "failed, no message";
            }
            $this->cryptoBuy->save();
        }

        return $result;
    }

    public function initialBuy($request)
    {
        $prices = prices('total'); //price within public + hidden fee in AUD
        $price = $prices[$request->currency]->ask; //AUD price
        $price = $this->convertPrice($request->fiat_currency, $price); //convert to trading currency

        $this->cryptoBuy->user_id = $request->user()->id;
        $this->cryptoBuy->type = $request->currency; //crypto
        $this->cryptoBuy->currency = $request->fiat_currency; //trading dollar
        $this->cryptoBuy->price = $price; //converted trading dollar
        $this->cryptoBuy->amount = bcdiv($request->amount, $price, 6); //quantity of crypto with converted price
        $this->cryptoBuy->total = $request->amount; //dollar
        $this->cryptoBuy->status = "pending";
        $this->cryptoBuy->external_id = "soonTM";
        return $this->cryptoBuy->save();
    }

    public function initialLedger()
    {
        $ledger = new ledger;
        $ledger->reference_id = "b".$this->cryptoBuy->id;
        $ledger->user_to = 0;
        $ledger->user_from = $this->cryptoBuy->user_id; //Bittrex
        $ledger->address_to = 'Bittrex';
        $ledger->address_from = $this->getCryptoAddress($this->cryptoBuy->user_id, $this->cryptoBuy->type)->address;
        $ledger->transaction_type = "buy";
        $ledger->currency = $this->cryptoBuy->currency; //dollar
        $ledger->amount = $this->cryptoBuy->total; //dollar spend on buy crypto
        $ledger->fee = $this->getFees($this->cryptoBuy->total); //fee in dollar
        $ledger->oldbalance = $this->balance_dollar->balance; //balance in dollar
        $ledger->fee_type = "buy_sell_total_fee";
        return $ledger->save();
    }

    public function deductCurrencyBalance()
    {
        $balance_locked = $this->getBalanceLocked($this->cryptoBuy->user_id, $this->cryptoBuy->currency);
        $oldBalance = $balance_locked->balance; //dollar
        $ableToSub = true;

        if (bccomp($this->cryptoBuy->total, $oldBalance, 18) == 1) {  //check dollar balance enough or not
            $ableToSub = false;
        }

        if (trans_vol_24_buy($this->cryptoBuy->user_id) > UserLimits::get_limit('buy_daily_limit')) {  //check reach the daily limits
            $ableToSub = false;
        }

        if ($ableToSub) {
            $balance_locked->balance = bcsub($oldBalance, $this->cryptoBuy->total, 18);
            return $balance_locked->save();
        } else {
            return false;
        }
    }

    private function saveBuy($status)
    {
        $this->cryptoBuy->status = $status;
        return $this->cryptoBuy->save();
    }

    private function saveLedger()
    {
        $ledger = new Ledger;
        $ledger->reference_id = 'br'.$this->cryptoBuy->id;
        $ledger->user_to = $this->cryptoBuy->user_id;
        $ledger->user_from = 0;
        $ledger->address_to = $this->getCryptoAddress($this->cryptoBuy->user_id, $this->cryptoBuy->type)->address;
        $ledger->address_from = "Bittrex";
        $ledger->transaction_type = "buy_received";
        $ledger->currency = $this->cryptoBuy->type; //crypto
        $ledger->amount = $this->cryptoBuy->amount; //crypto
        $ledger->fee = $this->getFees($this->cryptoBuy->amount); //crypto
        $ledger->oldbalance = $this->balance_crypto->balance;
        $ledger->fee_type = "buy_sell_total_fee";
        return $ledger->save();
    }

    public function purchaseOthers()
    {
        try {
            if(!$this->saveBuy('bought')) {
                throw new \Exception('Unable to finish buy');
            }

            if(!$this->saveLedger()) {
                throw new \Exception('Unable to update ledger');
            }
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoBuy->status = "buy market error:".$e->getMessage();
            } else {
                $this->cryptoBuy->status = "failed, no message";
            }
            $this->cryptoBuy->save();
        }
    }

    public function addCryptoBalance()
    {
        $balance_locked = $this->getBalanceLocked($this->cryptoBuy->user_id, $this->cryptoBuy->type);
        $balance_locked->balance = bcadd($balance_locked->balance, $this->cryptoBuy->amount, 18);
        return $balance_locked->save();
    }
}