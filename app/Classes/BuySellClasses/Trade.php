<?php

namespace App\Classes\BuySellClasses;

use App\Models\Ledger;
use App\Models\Balance;

class Trade
{
    use BuySellBasic;
    private $cryptoTrade;
    protected $wallet;
    protected $balance_dollar;
    protected $balance_crypto;
    protected $user;

    public function __contruct($cryptoTrade)
    {
        $this->cryptoTrade = $cryptoTrade;
    }

    public function markIfNoWallet()
    {
        $this->balance_crypto = $this->getBalanceLocked($this->cryptoTrade->user_id, $this->cryptoTrade->type);
        $this->balance_dollar = $this->getBalanceLocked($this->cryptoTrade->user_id, $this->cryptoTrade->currency);
        $this->user = $this->getUser($this->cryptoTrade->user_id);
        $this->wallet = $this->getWallet($this->cryptoTrade->user_id, $this->cryptoTrade->type);
        if (!$this->wallet) {
            $this->cryptoTrade->status = "no_wallet";
            $this->cryptoTrade->save();

            return true;
        } else {
            return false;
        }
    }

    public function markIfNegWallet()
    {
        if ($this->balance_dollar->balance < 0) {
            //balance has a negative number so freeze the user account
            $this->user->status = "inactive: negative balance";
            $this->user->save();

            $this->cryptoTrade->status = "balance_error";
            $this->cryptoTrade->save();

            return true;
        } else {
            return false;
        }
    }

    public function getCrypto()
    {
        return $this->cryptoTrade;
    }

    public function saveSession($trade_type)
    {
        $baseline_prices = prices('baseline');
        $type = $this->cryptoTrade->type;

        if ('buy' === $trade_type) {
            $baseline_price = $baseline_prices[$type]->ask;
            $baseline_price = manipulator_applier($baseline_price, $type, 'ask');
            $amount = $this->cryptoTrade->total;
            $quantity = $this->cryptoTrade->amount;
            $currency = $this->cryptoTrade->currency;
        }
        if ('sell' === $trade_type) {
            $baseline_price = $baseline_prices[$type]->bid;
            $baseline_price = manipulator_applier($baseline_price, $type, 'bid');
            $amount = $this->cryptoTrade->amount;
            $quantity = $this->cryptoTrade->total;
            $currency = $this->cryptoTrade->currency;
        }


        \Session::put('type', $type);
        \Session::put('baseline_price', $baseline_price);
        \Session::put('amount', $amount);
        \Session::put('quantity', $quantity);
        \Session::put('currency', $currency);
    }

    public function sendEmail($template, $type, $data)
    {
        \Mail::send('email.'.$template, $data, function($message) use ($data, $type) {
            $message->to($data['email']);
            $message->subject('myCryptoWallet: '.strtoupper($data['type']).' '.strtoupper($type).' Receipt');
        });

        \App\Models\Emails::create([
            'user_id'    => $this->user->id,
            'to'      => $this->user->email,
            'subject' => strtoupper($data['type']).' '.strtoupper($type).' Receipt',
            'data' => json_encode($data),
        ]);
    }
}