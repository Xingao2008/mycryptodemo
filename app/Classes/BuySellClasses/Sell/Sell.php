<?php

namespace App\Classes\BuySellClasses\Sell;

use App\Classes\BuySellClasses\Trade;
use App\Models\CryptoSell;
use App\Bittrex\Bittrex;
use App\Models\UserLimits;
use App\Models\Ledger;


class Sell extends Trade
{
    private $cryptoSell;

    public function __construct(CryptoSell $cryptoSell)
    {
        $this->cryptoSell = $cryptoSell;
        parent::__contruct($this->cryptoSell);
    }

    public function ContactBittrexWithPowr($msg = null)
    {
        $market = "BTC-POWR";
        $quantity = $this->cryptoSell->amount; //fee applied already
        $type = 'powr'; //pwr, naming different way
        $rate = $this->getBTCBittrexRate($type, 'sell');
        $bittrex = new Bittrex;
        $result = false;

        try {
            $result = $bittrex->sellLimit($market, $quantity, $rate);
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoSell->status = "sell market error:".$e->getMessage();
            } else {
                $this->cryptoSell->status = "failed, no message";
            }
            $this->cryptoSell->save();
        }

        sleep(1);

        $market = "USDT-BTC";
        $quantity = bcmul($this->cryptoSell->amount, $this->getBTCBittrexRate('powr', 'sell'), 16); //convert pwr to btc
        $type = 'btc';
        $rate = $this->getBittrexRate($type, 'sell');

        try {
            $result = $bittrex->sellLimit($market, $quantity, $rate);
            $this->cryptoSell->bittrex_rate = $this->convertUsdOther(
                bcmul(
                    $this->getBTCBittrexRate('powr', 'sell'),
                    $this->getBittrexRate('btc', 'sell'),
                    16)
                , $this->cryptoSell->currency);
            $this->cryptoSell->bittrex_order_id = $result->uuid;
            if ($msg === null) {
                $this->cryptoSell->status = 'complete';
            } else {
                $this->cryptoSell->status = $msg;
            }
            $this->cryptoSell->save();

        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoSell->status = "sell market error:".$e->getMessage();
            } else {
                $this->cryptoSell->status = "failed, no message";
            }
            $this->cryptoSell->save();
        }

        return $result;
    }

    public function ContactBittrex($msg = null)
    {
        $market = "USDT-".strtoupper($this->cryptoSell->type);
        $quantity = $this->cryptoSell->amount; //fee has been applied before
        $type = $this->cryptoSell->type;
        $bittrex = new Bittrex;
        $result = false;
        $rate = $this->getBittrexRate($type, 'sell');

        try {
            $result = $bittrex->sellLimit($market, $quantity, $rate);
            $this->cryptoSell->bittrex_rate = $this->convertUsdOther($this->getBittrexRate($this->cryptoSell->type, 'sell'), $this->cryptoSell->currency);
            $this->cryptoSell->bittrex_order_id = $result->uuid;
            if ($msg === null) {
                $this->cryptoSell->status = 'complete';
            } else {
                $this->cryptoSell->status = $msg;
            }
            $this->cryptoSell->save();

        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoSell->status = "sell market error:".$e->getMessage();
            } else {
                $this->cryptoSell->status = "failed, no message";
            }
            $this->cryptoSell->save();
        }

        return $result;
    }

    public function initialSell($request)
    {
        $prices = prices('total');
        $price = $prices[$request->currency]->bid; //AUD price
        $price = $this->convertPrice($request->fiat_currency, $price); //convert to trading currency

        $this->cryptoSell->user_id = $request->user()->id;
        $this->cryptoSell->type = $request->currency; //crypto
        $this->cryptoSell->currency = $request->fiat_currency; //currency
        $this->cryptoSell->amount = bcdiv($request->amount, $price, 6);//quantity in crypto.
        $this->cryptoSell->price = $price;
        $this->cryptoSell->total = $request->amount; //in dollar
        $this->cryptoSell->status = "pending";
        $this->cryptoSell->external_id = "soonTM";
        return $this->cryptoSell->save();
    }

    public function initialLedger()
    {
        $ledger = new Ledger;
        $ledger->reference_id = "s".$this->cryptoSell->id;
        $ledger->user_to = 0;
        $ledger->user_from = $this->cryptoSell->user_id;
        $ledger->address_to = 'Bittrex';
        $ledger->address_from = $this->getCryptoAddress($this->cryptoSell->user_id, $this->cryptoSell->type)->address;
        $ledger->transaction_type = "sell";
        $ledger->currency = $this->cryptoSell->type; //crypto
        $ledger->amount = $this->cryptoSell->amount; //crypto to sell
        $ledger->fee = $this->getFees($this->cryptoSell->amount); //fee in currency
        $ledger->oldbalance = $this->balance_crypto->balance; //balance in currency
        $ledger->fee_type = "buy_sell_total_fee";
        return $ledger->save();
    }

    private function saveSell($status)
    {
        $this->cryptoSell->status = $status;
        return $this->cryptoSell->save();
    }

    private function saveLedger()
    {
        $ledger = new Ledger;
        $ledger->reference_id = 'sr'.$this->cryptoSell->id;
        $ledger->user_to = $this->cryptoSell->user_id;
        $ledger->user_from = 0;
        $ledger->address_to = $this->getCryptoAddress($this->cryptoSell->user_id, $this->cryptoSell->type)->address;
        $ledger->address_from = "Bittrex";
        $ledger->transaction_type = "sold_received";
        $ledger->currency = $this->cryptoSell->currency; //dollar
        $ledger->amount = $this->cryptoSell->total; //dollar
        $ledger->fee = $this->getFees($this->cryptoSell->total);
        $ledger->oldbalance = $this->balance_dollar->balance;
        $ledger->fee_type = "buy_sell_total_fee";
        return $ledger->save();
    }

    public function deductCryptoBalance()
    {
        $balance_lock = $this->getBalanceLocked($this->cryptoSell->user_id, $this->cryptoSell->type);
        $old_balance = $balance_lock->balance;
        $ableToSub = true;

        if (bccomp($this->cryptoSell->amount, $old_balance, 18) == 1) {  //check dollar balance enough or not
            $ableToSub = false;
        }

        if (trans_vol_24_buy($this->cryptoSell->user_id) > UserLimits::get_limit('sell_daily_limit')) {  //check reach the daily limits
            $ableToSub = false;
        }

        if ($this->cryptoSell->total < 25.00) {
            $ableToSub = false;
        }

        if ($this->cryptoSell->total > UserLimits::get_limit('sell_transaction_limit')) {
            $ableToSub = false;
        }

        if ($ableToSub) {
            $balance_lock->balance = bcsub($old_balance, $this->cryptoSell->amount,18);
            return $balance_lock->save();
        } else {
            return false;
        }
    }

    public function SellOthers()
    {
        try {
            if(!$this->saveSell('sold')) {
                throw new \Exception('Unable to finish sell');
            }

            if(!$this->saveLedger()) {
                throw new \Exception('Unable to update ledger');
            }
        } catch (\Exception $e) {
            if(!is_null($e->getMessage())) {
                $this->cryptoSell->status = "sell market error:".$e->getMessage();
            } else {
                $this->cryptoSell->status = "failed, no message";
            }
            $this->cryptoSell->save();
        }
    }

    public function topupCurrencyBalance()
    {
        $balance_locked = $this->getBalanceLocked($this->cryptoSell->user_id, $this->cryptoSell->currency);
        $balance_locked->balance = bcadd($balance_locked->balance, $this->cryptoSell->total, 18);
        return $balance_locked->save();
    }
}