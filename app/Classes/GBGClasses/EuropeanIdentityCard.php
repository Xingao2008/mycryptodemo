<?php
namespace App\Classes\GBGClasses;

class EuropeanIdentityCard 
{
    public $Line1;
    public $Line2;
    public $Line3;
    public $ExpiryDay;
    public $ExpiryMonth;
    public $ExpiryYear;
    public $CountryOfNationality;
    public $CountryOfIssue;
}