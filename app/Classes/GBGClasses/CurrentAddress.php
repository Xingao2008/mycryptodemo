<?php
namespace App\Classes\GBGClasses;

class CurrentAddress 
{
    public $Country;
    public $SubStreet;
    public $Street;
    public $SubCity;
    public $City;
    public $StateDistrict;
    public $Region;
    public $ZipPostcode;
    public $Building;
    public $SubBuilding;
    public $Premise;
}