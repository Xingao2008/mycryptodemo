<?php
namespace App\Classes\GBGClasses;

class PersonalDetails 
{
    public $Title;
    public $Forename;
    public $MiddleName;
    public $Surname;
    public $DOBDay;
    public $DOBMonth;
    public $DOBYear;
}