<?php
namespace App\Classes\GBGClasses;

class InternationalPassport 
{
    public $Number;
    public $ExpiryDay;
    public $ExpiryMonth;
    public $ExpiryYear;
    public $CountryOfOrigin;
    public $IssueDay;
    public $IssueMonth;
    public $IssueYear;
    public $ShortPassportNumber;
}