<?php

namespace App\Classes;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Balance;
use App\Models\User;
use App\Models\Fee;
use App\Models\SurchargeBalance;
use App\Models\Setting;
use DB;
use Response;
use Exception;
use Auth;

class OrderContract
{
    private $order_id;
    private $user_id;
    private $order_type;
    private $currency;
    private $currency_pair;
    private $volume;
    private $market_type;
    private $price;
    private $remaining;
    public $status;

    public function __construct()
    {
        $this->volume = 0;
        $this->price = 0;
    }

    public function createContract($order)
    {
        $this->user_id = $order['user_id'];
        $this->order_type = $order['order_type'];
        $this->currency = $order['currency'];
        $this->currency_pair = $order['currency_pair'];
        $this->volume = $order['volume'];
        $this->market_type = $order['market_type'];
        $this->price = $order['price'];
        $this->remaining = $order['volume'];
        $this->status = 'active';
    }

    public function getContract($order_id = 0)
    {
        $order = Order::find($order_id);
        if (!$order) {
            return respondWithJson(false, 'Invalid order', '', 400);
        }
        $this->order_id = $order->id;
        $this->user_id = $order->user_id;
        $this->order_type = $order->order_type;
        $this->currency = $order->currency;
        $this->currency_pair = $order->currency_pair;
        $this->volume = $order->volume;
        $this->market_type = $order->market_type;
        $this->price = $order->price;
        $this->remaining = $order->remaining;
        $this->status = $order->status;

    }

    public function saveContract($order_id = 0)
    {
        $order = Order::find($order_id);
        $order->user_id = $this->user_id;
        $order->order_type = $this->order_type;
        $order->currency = $this->currency;
        $order->currency_pair = $this->currency_pair;
        $order->volume = $this->volume;
        $order->market_type = $this->market_type;
        $order->price = $this->price;
        $order->remaining = $this->remaining;
        $order->status = $this->status;
        $order->save();
    }

    public function generateContract()
    {
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            return false;
        }
        DB::beginTransaction();
        try {
            
            $order = new Order;
            $order->user_id = $this->user_id;
            $order->order_type = $this->order_type;
            $order->currency = $this->currency;
            $order->currency_pair = $this->currency_pair;
            $order->volume = $this->volume;
            $order->market_type = $this->market_type;
            $order->price = $this->price;
            $order->remaining = $this->remaining;
            $order->status = $this->status;
            $order->save();
            $this->order_id = $order->id;
            //deduct the money first if market_type = buy
            if ($this->market_type == 'buy') {
                $this->creditOrDeductBalance($this->user_id, $this->currency, true, $this->volume, $this->price, $order->id);
            } else {
                $this->creditOrDeductBalance($this->user_id, $this->currency_pair, true, $this->volume, $this->price, $order->id);
            }
        }
        catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
    }

    public function checkAccountHasEnoughFund($amount = 0)
    {
        if ($this->market_type == 'buy') {
            $amount = bcmul($this->volume, $this->price, 18);
            $balance_raw = Balance::where('currency', '=', $this->currency)
            ->where('user_id', '=', $this->user_id)
            ->lockForUpdate()
            ->first();
        } else {
            $amount = $this->volume;
            $balance_raw = Balance::where('currency', '=', $this->currency_pair)
            ->where('user_id', '=', $this->user_id)
            ->lockForUpdate()
            ->first();
        }

        if (!isset($balance_raw->balance)) {
            return false;
        }
        $balance = $balance_raw->balance;
        if (bccomp($amount, $balance, 18) == 1) {
           return false;
        }
        return true;
    }

    /**
     * Return the calculated service fee base on the volume
     */
    public static function calculateServiceFee( $volume, $user_id)
    {
        $user = User::find($user_id);
        $fees = Fee::where('type','exchange')->where('exchange_tier',$user->exchange_tier)->get();
        // $feeRange = config('fee.exchange');
        $payFee = 0;
        foreach($fees as $fee){
            if ($volume > $fee['min'] && $volume < $fee['max']){
                $divFee = bcdiv($fee['fee'], 100, 18);
                $payFee = bcmul($volume, $divFee, 18) ;
                break;
            }
        }
        return $payFee;
    }

    /**
     * update the order status either it is finished or admin hold it.
     */
    public function closeContract($order_id = 0, $status = 'complete')
    {
        $order = Order::find($order_id);
        if (!$order) {
            return respondWithJson(false, 'Invalid order', '', 400);
        }
        $order->remaining = 0;
        $order->status = $status;
        return $order->save();
    }

    public function startBuySell()
    {
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            return false;
        }
        DB::transaction(function () {
            $marketTypeToCall = ($this->market_type == 'buy' ? 'sell' : 'buy');
            $orders = Order::where('market_type', $marketTypeToCall)
                    ->where('remaining', '>', 0)
                    ->where('currency',  $this->currency )
                    ->where('currency_pair', $this->currency_pair)
                    ->where('status', 'active')
                    ->where('price', ($this->market_type == 'buy' ? '<=':'>='), $this->price)
                    ->where('user_id', '<>', $this->user_id)
                    ->orderBy('price',($this->market_type == 'buy' ? 'asc':'desc'))
                    ->orderBy('market_type','desc')
                    ->orderBy('created_at','asc')
                    ->get();
            //error_log(print_r(DB::getQueryLog()));        
            if (!$orders || count($orders) < 1) {
                return respondWithJson(true, 'No matching orders', '', 200);
            }
            foreach ($orders as $order) {
                $transactionsvolume = 0;
                //double check the price 
                if ($this->market_type == 'buy') {
                    if ($this->price < $order->price) {
                        continue;
                    }
                } else {
                    if ($this->price > $order->price) {
                        continue;
                    }
                }
                
                if(!isset($order->id)) {
                    continue;
                }
                $transactionsvolume = 0;
                
                //if price ok deduct the active contract
                if ($this->remaining - $order->remaining > 0) {
                    $this->remaining = bcsub($this->remaining, $order->remaining, 18);
                    $transactionsvolume =  $order->remaining;
                    //transactions for the login user
                    // $this->createTransactions($this->order_id, $this->user_id, $transactionsvolume, $this->price);
                    //transactions for matche user
                    // $this->createTransactions($order->id, $order->user_id, $transactionsvolume, $this->price);
                    $order->remaining = 0;
                } else {
                    $order->remaining = bcsub($order->remaining, $this->remaining, 18);
                    $transactionsvolume =  $this->remaining;
                    // $this->createTransactions($this->order_id, $this->user_id, $transactionsvolume, $this->price);
                    // $this->createTransactions($order->id, $order->user_id, $transactionsvolume, $this->price);
                    $this->remaining = 0;
                }
                //If this is a buying order, we dont have to credit back the logged in user for now.
                //Money already deducted when contract create
                //Credit the matched user
                if ($this->market_type == 'buy') {
                    $this->creditOrDeductBalance($this->user_id, $this->currency_pair, false, $transactionsvolume, $this->price, $this->order_id);
                    $this->creditOrDeductBalance($order->user_id, $this->currency, false, $transactionsvolume, $this->price, $this->order_id, $order->id);
                    //$this->creditOrDeductBalance($order->user_id, $this->currency_pair, true, $transactionsvolume, $this->price, $this->order_id, $order->id);
                } else {
                    //owner credit back the coin he trying to sell
                    $this->creditOrDeductBalance($this->user_id, $this->currency, false, $transactionsvolume, $this->price, $this->order_id);
                    //$this->creditOrDeductBalance($this->user_id, $this->currency_pair, false, $transactionsvolume, $this->price, $this->order_id);
                    //matched user deduct the money that he need to spend
                    //and credit back the coin that he buy
                    //$this->creditOrDeductBalance($order->user_id, $this->currency, true, $transactionsvolume, $this->price, $this->order_id, $order->id);
                    $this->creditOrDeductBalance($order->user_id, $this->currency_pair, false, $transactionsvolume, $this->price, $this->order_id, $order->id);
                }
                
                
                //create transactions
                //$this->createTransactions($this->order_id, $this->user_id, $transactionsvolume);
                //$this->createTransactions($order->id, $order->user_id, $transactionsvolume);
                //close the contract for either party if the volume reach zero
                if ($this->remaining === 0) {
                    $this->closeContract($this->order_id);
                    // $order->save();
                    //sometimes both of the contract just turn into zero in the same time
                    if ($order->remaining == 0) {
                        $this->closeContract($order->id);
                        //$this->saveContract($this->order_id);
                    } else {
                        $order->save();
                    }
                    break;
                } else {
                    $this->saveContract($this->order_id);
                }
                //So the current contract is still active, let check is the matching order already reacht to zero
                if ($order->remaining == 0) {
                    $this->closeContract($order->id);
                    //$this->saveContract($this->order_id);
                }
                
            }
        });
        return true;
    }

    public function createTransactions($order_id, $source_id, $user_id, $volume, $price, $currency, $surcharge,$isCredit=false, $status='complete') 
    {
        $transactions = new Transaction;
        $transactions->order_id = $order_id;
        if (isset($source_id) && !empty($source_id)) {
            $transactions->source_id = $source_id;
        }
        $transactions->user_id = $user_id;
        $transactions->volume = $volume;
        $transactions->currency = $currency;
        if ($surcharge > 0) {
            $transactions->surcharge = $surcharge;
        }
        $transactions->status = $status;
        $transactions->isCredit = $isCredit;
        if ($transactions->save()) {
            //save success, top up the surcharge table
            if ($transactions->isCredit == 1 && $transactions->status != 'cancel' && $surcharge > 0) {
                $this->topUpSurchargeBalance($currency, $surcharge);
            }
            return true;
        } else {
            //something went wrong
            return false;
        }
        
    }

    public function topUpSurchargeBalance($currency, $amount)
    {
        $balance = SurchargeBalance::where('currency',$currency)->first();
        $balance->amount = bcadd($balance->amount, $amount, 18);
        $balance->save();
    }

    public function creditOrDeductBalance($user_id, $currency, $deduct = true, $volume, $price, $order_id, $source_id='')
    {
        $balance = Balance::where('user_id', $user_id)
                            ->where('currency',$currency)
                            ->first();
         //error_log('####################');                    
         //error_log('userId: '.$user_id);
        // error_log('Currency: '.$currency);
        // error_log('Deduct: '.$deduct);
        // error_log('Balance: '.$balance->balance);
        // error_log('Volume: '.$volume);

        /**
         * Really bad login under but it was done when i creating my graphic to debug the whole flow
         * TODO: visit the logic and optimze it
         */
        $amount = 0;
        if ($deduct) {
            if ($this->market_type == 'buy') {
                if ($user_id == $this->user_id) {
                    $amount = bcmul($volume, $price, 18);
                    $balance->balance = bcsub($balance->balance, $amount, 18);
                }else{
                    $amount = $volume;
                    $balance->balance = bcsub($balance->balance, $volume, 18);
                }
            } else {
                if ($user_id == $this->user_id) {
                    $amount = $volume;
                    $balance->balance = bcsub($balance->balance, $volume, 18);
                }else{
                    $amount = bcmul($volume, $price, 18);
                    $balance->balance = bcsub($balance->balance, $amount, 18);
                }
            }

            $balance->save();
            $this->createTransactions($order_id, $source_id, $user_id, $amount, $price, $currency, -1, false);
        } else {
            if ($this->market_type == 'buy') {
                if ($user_id == $this->user_id) {
                    $amount = $volume;
                    $surcharge = self::calculateServiceFee($amount, $this->user_id);
                    $amount = bcsub($amount, $surcharge, 18);
                    $balance->balance = bcadd($balance->balance, $amount, 18);
                }else{
                    $amount = bcmul($volume, $price, 18);
                    $surcharge = self::calculateServiceFee($amount, $this->user_id);
                    $amount = bcsub($amount, $surcharge, 18);
                    $balance->balance = bcadd($balance->balance, $amount, 18);
                }
            } else {
                if ($user_id == $this->user_id) {
                    $amount = bcmul($volume, $price, 18);
                    $surcharge = self::calculateServiceFee($amount, $this->user_id);
                    $amount = bcsub($amount, $surcharge, 18);
                    $balance->balance = bcadd($balance->balance, $amount, 18);
                }else{
                    $amount = $volume;
                    $surcharge = self::calculateServiceFee($amount, $this->user_id);
                    $amount = bcsub($amount, $surcharge, 18);
                    $balance->balance = bcadd($balance->balance, $amount, 18);
                }
            }
            // error_log('$balance->balance '.$balance->balance);
            $balance->save();
            $this->createTransactions($order_id, $source_id, $user_id, $amount, $price, $currency, $surcharge,true);
        }

    }

    /**
     * MVP only
     * Check do we have enough orders to fill up the market order
     * Reject it if not enough
     */
    public function hasEnoughOrders() {
        $marketTypeToCall = ($this->market_type == 'buy' ? 'sell' : 'buy');

        $orders = DB::table('orders')
                ->select(DB::raw('SUM(remaining) as sum_remaining'))
                ->where('market_type', $marketTypeToCall)
                ->where('remaining', '>', 0)
                ->where('currency',  $this->currency )
                ->where('currency_pair', $this->currency_pair)
                ->where('price', ($this->market_type == 'buy' ? '<=':'>='), $this->price)
                ->where('user_id', '<>', $this->user_id)
                ->having('sum_remaining', '>=', $this->remaining)
                ->get();
        if (!$orders || count($orders) < 1) {
            return false;
        }
        // $orderRemaining = $this->remaining;
        // $enoughOrders = false;
        // foreach ($orders as $order) {
        //     if ($this->market_type == 'buy') {
        //         if ($this->price < $order->price) {
        //             continue;
        //         }
        //     } else {
        //         if ($this->price > $order->price) {
        //             continue;
        //         }
        //     }
        //     $orderRemaining = $orderRemaining - $order->remaining;
        //     if ($orderRemaining <= 0) {
        //         return true;
        //     }
        // }
        return true;
    }
    
    /**
     * Throw exception straight away to pause the whole system
     * whenever negative amount has been detected
     */
    public function checkBalanceIsPositive($balance)
    {
        try {
            if ($balance < 0) {
                return false;
            }
            if($this->hasMinusSign($balance)) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    protected function hasMinusSign($value) {
        return (substr(strval($value), 0, 1) == "-");
    }

    /**
     * Cancel order by user. Check how much balance left and refund it to user
     */
    public function refundOrder()
    {
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            return false;
        }
        if ($this->remaining < 0) {
            return false;
        } else {
            //just to be save. use transaction and double check
            DB::transaction(function () {
                $order = Order::find($this->order_id);
                if (!isset($order->remaining) || $order->remaining <= 0) {
                    return false;
                }
                
                if ($order->market_type == 'buy') {
                    //refund the pair
                    $selectedCurrency = $order->currency;
                } else {
                    //refund the currency
                    $selectedCurrency = $order->currency_pair;
                }
                $balance = Balance::where('user_id', $order->user_id)
                            ->where('currency',$selectedCurrency)
                            ->first();
                if($order->user_id !== $balance->user_id || $order->order_type !== 'limit') {
                    //may be should report a dangerous attempt
                    return false;
                }
                if ($order->market_type == 'buy') {
                    $addedBalance = bcmul($order->remaining, $order->price, 18);
                } else {
                    $addedBalance = $order->remaining;
                }
                $balance->balance = bcadd($balance->balance, $addedBalance, 18);
                $balance->save();  

                //create a transaction for reference
                $this->createTransactions($order->id,$order->id, $order->user_id, $addedBalance, 0, $selectedCurrency, -1, true, 'cancel');

                $order->status = 'cancel';
                $order->remaining = 0;
                $order->save();
                
            }, 3);
            return true;
        }
    }

    public function isOrderOwner($user_id = 0)
    {
        if (isset($this->order_id)) {
            if ($this->user_id === $user_id) {
                return true;
            }
        }
        return false;
    }
}