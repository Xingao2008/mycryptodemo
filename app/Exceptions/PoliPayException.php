<?php


namespace App\Exceptions;


class PoliPayException extends \Exception
{
    public function errorMessage() {
        return $this->getMessage();
    }
}