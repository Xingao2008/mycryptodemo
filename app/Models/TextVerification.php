<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TextVerification extends Model implements AuditableContract
{

    use Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'to', 'type', 'code', 'verified', 'attempts', 'reference_id',
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
