<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginAttempts extends Model
{
    protected $table = 'login_attempts';

    protected $fillable = ['ip_address', 'email', 'failed'];

    protected $casts = [
        'ip_address' => 'string', 'email' => 'string', 'failed' => 'boolean'
    ];
}
