<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Countries extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [];

    //protected $guarded = [];

    // public function getAll()
    // {
    //   return $this->belongsTo(User::class, 'id');
    // }

    public function address()
    {
      return $this->hasOne(Address::class, 'country_id');
    }
}
