<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmailList extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'list_number',
        'email'
    ];

    protected $table = 'email_list';

    public static function getList()
    {
        $emailList = EmailList::select(DB::raw('count(*) as user_count, list_name'))
                        ->groupBy('list_name')
                        ->get();

        return response()->json([
        'success' => true,
        'message' => 'Load email list successful',
        'data' => $emailList,
    ], 200,
        [],
        JSON_NUMERIC_CHECK);
    }

    public static function getRecipient($title)
    {
        $contacts = EmailList::select('email')
                        ->where('list_name', $title)
                        ->get();

        return $contacts;
    }
}