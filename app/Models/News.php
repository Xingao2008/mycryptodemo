<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Lecturize\Addresses\Traits;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class News extends Model implements AuditableContract
{
    use Auditable;

    protected $guarded = ['slug'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
