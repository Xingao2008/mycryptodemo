<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLimits extends Model
{
    protected $fillable = ['user_id', 'key', 'value'];

    private static $cache_key = 'user_limits:';

    /**
     * @param string $key
     * @param int $user_id
     */
    public static function cacheForget(string $key, int $user_id): void
    {
        try {
            cache()->forget(self::getCacheKey($key, $user_id));
        } catch (\Exception $e) {
        }
    }

    /**
     * @param string $key
     * @param int $user_id
     * @return int
     */
    public static function cacheGet(string $key, int $user_id): int
    {
        try {
            return cache()->rememberForever(self::getCacheKey($key, $user_id), function () use ($key, $user_id) {
                $limit = \App\Models\UserLimits::where('key', $key)->where('user_id', $user_id)->first();
                return $limit->value ?? 0;
            });
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * returns the cache key used for a limit
     *
     * @param string $key
     * @param int $user_id
     * @return string
     */
    private static function getCacheKey(string $key, int $user_id): string
    {
        return 'settings:' . $user_id . ':' . $key;
    }

    /**
     * returns limit for a user
     *
     * @param string $key
     * @param int $user_id
     * @return int
     */
    public static function get_limit(string $key, int $user_id = 0)
    {
        if (!$user_id) {
            $user_id = \Auth::user()->id;
        }

        $user_limit = self::cacheGet($key, $user_id) ?: setting($key);

        return number_format($user_limit, 6, '.', '');
    }
}
