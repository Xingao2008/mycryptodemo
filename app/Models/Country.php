<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Country extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [];

    public function users()
    {
        return $this->hasMany(User::class);
    }
    
}
