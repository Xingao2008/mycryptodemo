<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class Order extends Model implements AuditableContract
{
    use Auditable;

    protected $casts = [
        'volume' => 'float',
        'price' => 'float',
        'remaining' => 'float',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public static function getMarketPrice($currency = 'btc', $currency_pair = 'aud')
    {
        return DB::select( 
            DB::raw("SELECT o.currency, o.currency_pair, o.volume, o.price, UNIX_TIMESTAMP(o.updated_at) as date FROM orders o
            JOIN (SELECT DATE(updated_at) date_date, MIN(updated_at) max_date
                  FROM orders
                  where currency = :currency and currency_pair = :currency_pair and market_type = 'sell' and status = 'complete'
                  GROUP BY DATE(updated_at)
                  ) o2
              ON o.updated_at = o2.max_date 
              WHERE o.market_type ='sell'"), array(
            'currency' => $currency,
            'currency_pair' => $currency_pair,
        ));
    }

    public static function getYesterdayCloseSellPrice($currency = 'btc', $currency_pair = 'aud')
    {
        return DB::select( 
            DB::raw("SELECT o.currency, o.currency_pair, o.volume, o.price, UNIX_TIMESTAMP(o.updated_at) as date FROM orders o
            JOIN (SELECT updated_at, MAX(updated_at) max_date
                  FROM orders
                  where currency = :currency and currency_pair = :currency_pair and market_type = 'sell' and status = 'complete'
                  and DATE(updated_at) = DATE(NOW() - INTERVAL 1 DAY)
                  GROUP BY updated_at
                  ) o2
              ON o.updated_at = o2.max_date 
              WHERE o.market_type ='sell' LIMIT 1"), array(
            'currency' => $currency,
            'currency_pair' => $currency_pair,
        ));
    }

    public static function getYesterdayCloseBuyPrice($currency = 'btc', $currency_pair = 'aud')
    {
        return DB::select( 
            DB::raw("SELECT o.currency, o.currency_pair, o.volume, o.price, UNIX_TIMESTAMP(o.updated_at) as date FROM orders o
            JOIN (SELECT updated_at, MAX(updated_at) max_date
                  FROM orders
                  where currency = :currency and currency_pair = :currency_pair and market_type = 'buy' and status = 'complete'
                  and DATE(updated_at) = DATE(NOW() - INTERVAL 1 DAY)
                  GROUP BY updated_at
                  ) o2
              ON o.updated_at = o2.max_date 
              WHERE o.market_type ='buy' LIMIT 1"), array(
            'currency' => $currency,
            'currency_pair' => $currency_pair,
        ));
    }
}
