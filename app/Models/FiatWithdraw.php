<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class FiatWithdraw extends Model implements AuditableContract
{
    use Auditable;


    public function user() {
        return $this->belongsTo(User::class);
    }

    public function user_bank() {
        return $this->belongsTo(Bank::class, 'bank');
    }
}
