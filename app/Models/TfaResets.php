<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class TfaResets extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['user_id', 'staff_id', 'active'];

    public function url() {
        return url()->route('login.tfareset', $this->auth_code);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function staff() {
        return $this->belongsTo(User::class, 'staff_id');
    }
}
