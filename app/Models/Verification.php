<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Verification extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'account', 'id_passport', 'id_passport_notes', 'id_drivers', 'id_drivers_notes', 'id_bank', 'ubc', 'sms', 'bank', '2fa', 'balance1', 'balance2', 'abn', 'selfie', 'selfie_notes'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include pending verification.
     * That is at least a field is pending.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query
            ->orWhere('account', 'pending')
            ->orWhere('id_passport', 'pending')
            ->orWhere('id_drivers', 'pending')
            ->orWhere('id_bank', 'pending')
            ->orWhere('ubc', 'pending')
            ->orWhere('sms', 'pending')
            ->orWhere('bank', 'pending')
            ->orWhere('tfa', 'pending')
            ->orWhere('balance1', 'pending')
            ->orWhere('balance2', 'pending')
            ->orWhere('abn', 'pending');
    }
}
