<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class ExternalPrices extends Model implements AuditableContract
{
    use Auditable;
    protected $guarded = ['id'];

    protected $primaryKey = 'currency';

    public $incrementing = false;


    public static function price()
    {
        return cache()->remember('external_prices', 1, function () {
            foreach(config('currencies.crypto') as $cur)
            {
                $price[$cur] = ExternalPrices::where('currency', '=', $cur)->latest()->first();
            }

            foreach ($price as $key => $value) {
                $bidPrice = manipulator_applier(hidden_applier($price[$key]->bid, 'bid'), $key, 'bid');
                $price[$key]->bid = $bidPrice - ($bidPrice * (setting('public_fee')/100));

                $askPrice = manipulator_applier(hidden_applier($price[$key]->ask, 'ask'), $key, 'ask');
                $price[$key]->ask = $askPrice * (1+(setting('public_fee')/100));
            }

            return $price;
        });
    }

    public static function price24()
    {
        return cache()->remember('external_prices_24', 1, function () {
            $price_24['btc'] = ExternalPrices::where('currency', '=', 'btc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24['eth'] = ExternalPrices::where('currency', '=', 'eth')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24['ltc'] = ExternalPrices::where('currency', '=', 'ltc')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24['xrp'] = ExternalPrices::where('currency', '=', 'xrp')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();
            $price_24['pwr'] = ExternalPrices::where('currency', '=', 'pwr')->where('created_at', '<=', \Carbon\Carbon::now())->latest()->first();

            // replace below with above after 1 day
            // $price_24['pwr'] = ExternalPrices::where('currency', '=', 'pwr')->where('created_at', '<=', \Carbon\Carbon::now()->subDay())->latest()->first();

            foreach ($price_24 as $key => $value) {
                $bidPrice24 = manipulator_applier(hidden_applier($price_24[$key]->bid, 'bid'), $key, 'bid');
                $price_24[$key]->bid = $bidPrice24 - ($bidPrice24 * (setting('public_fee')/100));

                $askPrice24 = manipulator_applier(hidden_applier($price_24[$key]->ask, 'ask'), $key, 'ask');
                $price_24[$key]->ask = $askPrice24 * (1+(setting('public_fee')/100));
            }

            return $price_24;
        });
    }

    public static function bccomp_results()
    {
        return cache()->remember('bccomp_results', 1, function () {
            $price = ExternalPrices::price();
            $price_24 = ExternalPrices::price24();
            foreach(config('currencies.crypto') as $cur) 
            {
                $bccomp_result[$cur]['bid'] = bccomp($price[$cur]->bid, $price_24[$cur]->bid, 2);
                $bccomp_result[$cur]['ask'] = bccomp($price[$cur]->ask, $price_24[$cur]->ask, 2);
            }
            return $bccomp_result;
        });
    }

    public static function price_changes()
    {
        //remove the cache also and see can fix the problem or not
        //error reported: https://sentry.ecdev.com.au/fairdigital/mycryptowallet/issues/1954/
        $price = ExternalPrices::price();
        $price_24 = ExternalPrices::price24();

        $price_change = [];
        foreach ($price as $key => $value) {
            $sub_result = bcsub($value->bid, $price_24[$key]->bid, 2);
            $div_result = bcdiv($sub_result, $value->bid, 2);
            $price_change[$key] = $div_result * 100;
        }
        //create a stupid foreach loop just to filter out any array results
        foreach($price_change as $k => $v) {
            if (is_array($price_change[$k])) {
                $price_change[$k] = '';
            }
        }
        return $price_change;
        
    }


}
