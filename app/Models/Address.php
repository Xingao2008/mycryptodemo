<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Address extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'user_id', 'street', 'city', 'post_code', 'state', 'country_id', 'addressable_id',
    ];

    //protected $guarded = [];

    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function country()
    {
      return $this->belongsTo(Country::class);
    }
}
