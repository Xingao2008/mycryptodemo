<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $incrementing = false;

    protected $fillable = ['id', 'value', 'name', 'is_boolean', 'type'];
    protected $casts = [
        'id' => 'string',
        'value' => 'string',
        'name' => 'string',
        'is_boolean' => 'boolean',
        'type' => 'integer'
    ];

    private static $cache_key = 'settings:';

    public static function cacheForget($id)
    {
        cache()->forget(self::getCacheKey($id));
    }

    public static function cacheGet($id)
    {
        return cache()->rememberForever(self::getCacheKey($id), function () use ($id) {
            $setting = \App\Models\Setting::where('id', $id)->first();
            return $setting->value ?? null;
        });
    }

    public static function getValue($id)
    {
        $setting = \App\Models\Setting::where('id', $id)->first();
        return $setting->value ?? null;
    }

    public static function updateValue($id,$value)
    {
        $setting = \App\Models\Setting::where('id', $id)->first();
        $setting->value = $value;
        return $setting->save();
    }

    private static function getCacheKey($id)
    {
        return 'settings:' . $id;
    }
}
