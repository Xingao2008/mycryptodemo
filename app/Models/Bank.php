<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lecturize\Addresses\Traits;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Bank extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'user_id', 'bank_name', 'bank_branch', 'bank_bsb', 'bank_number',
    ];

    //protected $guarded = [];

    public function user()
    {
      return $this->belongsTo(User::class, 'id');
    }

}
