<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SurchargeBalance extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'amount'
    ];
    protected $table = 'surcharge_balances';
}