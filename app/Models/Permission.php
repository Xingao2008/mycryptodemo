<?php

namespace App\Models;


class Permission extends \Spatie\Permission\Models\Permission
{
    protected $fillable = ['name', 'title', 'description', 'guard_name'];
}