<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsdExchange extends Model
{
    protected $fillable = [
        'source', 'currency', 'value',
    ];
}
