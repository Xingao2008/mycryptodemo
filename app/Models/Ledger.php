<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Ledger extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = ['reference_id'];

    public function sender() {
        return $this->belongsTo(User::class, 'user_from');
    }

    public function receiver() {
        return $this->belongsTo(User::class, 'user_to');
    }
}
