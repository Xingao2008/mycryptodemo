<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Lecturize\Addresses\Traits;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Balance extends Model implements AuditableContract
{
    use Auditable;

    protected $guarded = ['balance'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include from such currency.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $currency
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCurrency($query, $currency)
    {
        return $query->where('currency', $currency);
    }

    public function setBalanceAttribute($val) {
        $decimals = ($this->currency == 'aud') ? 2 : 8;
        $this->attributes['balance'] = number_format( str_replace(',', '', $val), $decimals, '.', '');
    }

    /**
     * Handles save operation
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function saveOperation($params)
    {
        DB::beginTransaction();
        try {

            // @todo: do we initialize all balances upon registration?
            if (isset($params['id'])) {
                $model = Balance::find($params['id']);
                $model->id = $params['id'];
                $model->updated_at = date('Y-m-d H:i:s');
            } else {
                $model = new Balance;
                $model->created_at = date('Y-m-d H:i:s');
                $model->updated_at = date('Y-m-d H:i:s');
            }

            $model->user_id = $params['user_id'];
            $model->currency = $params['currency'];
            $model->balance = $params['balance'];
            $model->locked = $params['locked'];
            $model->save();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return DB::commit();
    }

    public static function funds(User $user = null)
    {
        if (!$user) {
            $user = \Auth::user();
        }

        return cache()->remember(Balance::getFundsCacheKey($user->id), 1, function () use ($user) {
            $funds = Balance::where('user_id', '=', $user->id)->get()->groupBy('currency');

            $funds['aud']['0']->balance = audFormat($funds['aud']['0']->balance);
            $funds['nzd']['0']->balance = audFormat($funds['nzd']['0']->balance);
            $funds['usd']['0']->balance = audFormat($funds['usd']['0']->balance);
            $funds['btc']['0']->balance = btcFormat($funds['btc']['0']->balance);
            $funds['eth']['0']->balance = ethFormat($funds['eth']['0']->balance);
            $funds['ltc']['0']->balance = ltcFormat($funds['ltc']['0']->balance);
            $funds['xrp']['0']->balance = xrpFormat($funds['xrp']['0']->balance);
            $funds['pwr']['0']->balance = xrpFormat($funds['pwr']['0']->balance);

            return $funds;
        });

    }

    public static function coinBalances(User $user = null)
    {
        if (!$user) {
            $user = \Auth::user();
        }

        return cache()->remember(Balance::getCoinBalancesCacheKey($user->id), 1, function () use ($user) {
            $coin_balances['aud'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'AUD')->first()->balance);
            $coin_balances['nzd'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'NZD')->first()->balance);
            $coin_balances['usd'] = audFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'USD')->first()->balance);
            $coin_balances['btc'] = btcFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'btc')->first()->balance);
            $coin_balances['ltc'] = ltcFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'ltc')->first()->balance);
            $coin_balances['eth'] = ethFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'eth')->first()->balance);
            $coin_balances['xrp'] = xrpFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'xrp')->first()->balance);
            $coin_balances['pwr'] = xrpFormat(\App\Models\Balance::where('user_id', '=', $user->id)->where('currency', '=', 'pwr')->first()->balance);

            return $coin_balances;
        });
    }

    public static function getCoinBalancesCacheKey(int $user_id){
        return 'balances:user_coin_' . $user_id;
    }

    public static function getFundsCacheKey(int $user_id){
        return 'balances:user_funds_' . $user_id;
    }

     /**
     * Search crypto_buy, crypto_sell, withdraw table to find status of records exclude substing of complete
     * means the transaction is pending, then push it to blade
     * @param model name, currency = null
     *
     * @return amount which can be multiple or false if not find record
     */
    public static function check_incomplete($model, $currency = null)
    {
        $search = 'complete';
        $user = \Auth::user();
        $userId = $user['id'];
        $modelName = 'App\\Models\\'.$model;
        $incompleteOrder = $modelName::select(DB::raw('SUM(amount) as amount'))
            ->where('user_id', $userId)
            ->where('status', 'NOT LIKE', '%'.$search.'%')
            ->where(function ($incompleteOrder) use ($currency) {
                if ($currency !== NULL) {
                    $incompleteOrder->where('type', $currency)->groupBy('user_id', 'type');
                } else {
                    $incompleteOrder->groupBy('user_id');
                }
            })
            ->first();
        if ($incompleteOrder === NULL) {
            return false;
        } else {
            return $incompleteOrder->amount;
        }
    }
}
