<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use DB;

class Transaction extends Model implements AuditableContract
{
    use Auditable;

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

}
