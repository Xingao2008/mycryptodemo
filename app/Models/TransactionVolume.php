<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TransactionVolume extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = ['reference_id'];

    public function ledger()
    {
      return $this->hasOne(Ledger::class, 'id', 'reference_id');
    }

}
