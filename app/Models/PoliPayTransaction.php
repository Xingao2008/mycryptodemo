<?php

namespace App\Models;

use Alsofronie\Uuid\Uuid32ModelTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use stdClass;

class PoliPayTransaction extends Model
{
    protected $fillable = ['id', 'reference', 'transaction_token', 'transaction_ref_no', 'user_id', 'status', 'transaction_id'];

    public static function updateTransaction(stdClass $data)
    {
        $poli_record = PoliPayTransaction::whereId($data->MerchantReference)->firstOrFail();

        if($poli_record->created_at < Carbon::createFromDate(2017, 01, 9))
        {
            return null;
        }

        $old_status = $poli_record->status;

        $poli_record->user_ip = $data->UserIPAddress ?? '127.0.0.1';
        $poli_record->status = strtolower($data->TransactionStatus);
        $poli_record->amount = number_format($data->AmountPaid, 2, '.', '');
        $poli_record->transaction_time = strtotime($data->EndDateTime);
        $poli_record->transaction_id = $data->TransactionID ?? 0;
        $poli_record->transaction_ref_no = $data->TransactionRefNo;
        $poli_record->data = json_encode($data);
        $poli_record->save();

        $user = \App\Models\User::find($poli_record->user_id);
        if(!$user) {
            $poli_record->status = 'unknownuser';
            $poli_record->save();
            return null;
        }


        // If the status is completed, and it's marked as Dirty (changed), then we add it to the ledger and send the email out
        if ($poli_record->status == 'completed' && $old_status != 'completed') {
            $balance = Balance::whereUserId($poli_record->user_id)->whereCurrency('aud')->first();
            $ledger = Ledger::firstOrNew(['reference_id' => $poli_record->id]);
            $ledger->reference_id = "pp".$poli_record->id;
            $ledger->user_to = $poli_record->user_id;
            $ledger->user_from = 0;
            $ledger->transaction_type = 'deposit';
            $ledger->currency = 'aud';
            $ledger->amount = $poli_record->amount;
            $ledger->oldbalance = $balance->balance;
            $ledger->created_at = Carbon::createFromTimestampUTC($poli_record->transaction_time);
            $ledger->save();

            $balance->balance = bcadd($balance->balance, $poli_record->amount, 18);
            $balance->save();



            cache()->forget('user_funds_' . $user->id);

            $data = [
                'email' => $user->email,
                'amount' => $poli_record->amount,
                'type' => 'AUD',
            ];
            \Mail::send('email.poli-pay-complete', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject('myCryptoWallet: PoliPay Deposit Complete!');
            });
            \App\Models\Emails::create([
                'user_id' => $user->id,
                'to' => $user->email,
                'subject' => 'PoliPay Deposit Complete',
                'data' => json_encode($data),
            ]);
        }

        return $poli_record;

    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}
