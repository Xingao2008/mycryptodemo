<?php

namespace App\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Lab404\Impersonate\Services\ImpersonateManager;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use PragmaRX\Google2FA\Google2FA;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements AuditableContract, UserResolver, HasMedia, JWTSubject
{
    use Notifiable, Auditable, HasApiTokens, HasRoles, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'middle_name', 'last_name', 'verified', 'verify_count',
        'email', 'password', 'status_code', 'tfa', 'tfa_status', 'abn', 'status', 'phone', 'dob', 'settings', 'lock_withdraw', 'lock_withdraw_time', 'refer_uid', 'country_calling_code'
    ];

    protected $casts = [
        'settings' => 'json',
        'lock_withdraw' => 'boolean',
        'lock_withdraw_time' => 'datetime',
    ];

     /**
     * Add a mutator to ensure hashed passwords
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function address()
    {
        return $this->hasOne(Address::class, 'user_id');
    }

    public function verification()
    {
        return $this->hasOne(Verification::class, 'user_id');
    }

    public function bank()
    {
        return $this->hasOne(Bank::class, 'user_id');
    }

    public function mobilesetting()
    {
        return $this->hasOne(MobileSetting::class, 'user_id');
    }

    public function uploads()
    {
        return $this->hasMany(Uploads::class);
    }

    public function sms()
    {
        return $this->hasMany(SMS::class);
    }

    public function balances()
    {
        return $this->hasMany(Balance::class);
    }

    public function wallets()
    {
        return $this->hasMany(Wallet::class);
    }

    public function notes()
    {
        return $this->hasMany(UserNotes::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->can('admin.dashboard');
    }

    public function addBankDeposit($data = [])
    {
        DB::beginTransaction();
        try {
            // @todo: do we initialize all balances upon registration?
            //
            $deposits_new = new \App\Models\FiatDeposit;
            $deposits_new->user_id = $this->id;
            $deposits_new->source = "bank";
            $deposits_new->amount = $data['amount'];
            $deposits_new->currency = $data['currency'];;
            $deposits_new->date_received = Carbon::parse($data['transaction_date']);
            $deposits_new->status = "complete";
            $deposits_new->notes = "none";
            $deposits_new->save();

            $balance = $this->balances()->ofCurrency('aud')->first();
            $ledger = new Ledger();
            $ledger->reference_id = 'bd' . $deposits_new->id;
            $ledger->user_to = $this->id;
            $ledger->user_from = 0;
            $ledger->transaction_type = hasMinusSign($data['amount']) ? 'admin_edit' : 'deposit';
            // $ledger->currency = 'aud';
            $ledger->currency = $data['currency'];
            $ledger->amount = $data['amount'];
            $ledger->oldbalance = $balance->balance;
            $ledger->created_at = Carbon::parse($data['transaction_date']);
            $ledger->save();


            $balance->balance = bcadd($balance->balance, $data['amount'], 18);
            $balance->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        return DB::commit();
    }

    /**
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }


    public function canImpersonate()
    {
        return $this->can('admin.impersonate');
    }

    public function canBeImpersonated()
    {
        return \Auth::user()->can('admin.impersonate');
    }

    /**
     * Check if the current user is impersonated.
     *
     * @param   void
     * @return  bool
     */
    public function isImpersonated()
    {
        return app(ImpersonateManager::class)->isImpersonating();
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get_setting(string $key, $default = null)
    {
        $settings = collect($this->settings);

        return $settings->get($key, $default);
    }

    /**
     * @return float
     */
    public function transaction_volume(): float
    {
        return (float)number_format(TransactionVolume::whereUserId($this->id)->sum('value'), 2, '.', '');
    }

    /**
     * @param string $tfa_code
     * @return bool
     */
    public function check_tfa(string $tfa_code): bool
    {
        if (app()->environment() !== 'production') {
            return true;
        }

        if ($this->tfa_status) {
            $google2fa = new Google2FA();

            if (!is_string($tfa_code)) {
                return false;
            }
            $valid = $google2fa->verifyKey($this->tfa, $tfa_code);

            return $valid;
        } else {
            return true;
        }
    }

    /**
     * Does the user meet the POLi Pay requirements?
     * Poli is forbidden (21/5/2018)
     *
     * @return bool
     */
    public function canPoliPay(): bool
    {
        // $transaction_volume = $this->transaction_volume() >= 1000; //disabled on 8/6/18
        $tier = tier($this) != 'unverified';
        $age = $this->created_at->diffInDays() >= 5;

        return $tier && $age;
    }

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function routeNotificationForTwilio()
    {
        //check if the user has input a + in front. if so, just use the phone field
        $phone = $this->phone;
        if ($phone[0] == '+') {
            return $this->phone;
        }
        if ($phone[0] == '6' && $phone[1] == '1') {
            return $this->phone;
        }
        if ($phone[0] == '6' && $phone[1] == '4') {
            return $this->phone;
        }

        $phone_prefix = $this->country_calling_code;

        //Get the notify user
        $user_email = setting('usdt_warning_user');

        //Check where is this user come from
        if (Auth::user() == null) {
            $country = DB::table('countries')
                ->join('addresses', 'countries.id', '=', 'addresses.country_id')
                ->join('users', 'users.id', '=', 'addresses.user_id')
                ->where('users.email', $user_email)
                ->select('countries.*')
                ->first();
        } else {
            $country = DB::table('countries')
                ->join('addresses', 'countries.id', '=', 'addresses.country_id')
                ->join('users', 'users.id', '=', 'addresses.user_id')
                ->where('users.id', Auth::user()->id)
                ->select('countries.*')
                ->first();
        }


        if (substr($phone, 1) == '0') {
            $phone = ltrim($phone, '0');
        }

        if(strlen($phone) > 10) //filter out users who enter country code + number as phone number
            return $phone;

        if($phone_prefix != null && $phone_prefix != '') {
            return '+'.$phone_prefix.$phone;
        } else if (isset($country->id) && $country->id == 554) {
            return '+'.$country->calling_code.$phone;
        } else {
            return '+61'.$phone;
        }
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
