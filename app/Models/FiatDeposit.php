<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class FiatDeposit extends Model implements AuditableContract
{
    use Auditable;


    public function user() {
        return $this->belongsTo(User::class);
    }
}
