<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Emails extends Model implements AuditableContract
{
    use Auditable;

    protected $fillable = [
        'user_id',
        'to',
        'subject',
        'data'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
