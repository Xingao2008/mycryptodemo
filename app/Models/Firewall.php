<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Firewall extends Model
{

    protected $table = 'cloudflare_firewall';
    protected $dates = ['expires'];

    protected $guarded = [];
    protected $fillable = ['ip_address'];

    protected $casts = [
        'ip_address' => 'string',
        'whitelist' => 'boolean',
        'reason' => 'string',
        'cloudflare_id' => 'string'
    ];

    /**
     * @param $ip
     * @param string $reason
     * @param null $expires
     * @return Firewall
     */
    public static function blacklist($ip, $reason = '', $expires = null)
    {
        $blacklist = \App\Models\Firewall::firstOrNew(['ip_address' => $ip]);
        $blacklist->reason = $reason;
        $blacklist->whitelist = false;
        $blacklist->expires = ($expires) ? Carbon::parse($expires) : Carbon::now()->addYears(100);
        $blacklist->save();

        return $blacklist;
    }

    public static function whitelist($ip, $reason = '', $expires = null)
    {
        $whitelist = Firewall::firstOrNew(['ip_address' => $ip]);
        $whitelist->reason = $reason;
        $whitelist->whitelist = true;
        $whitelist->expires = ($expires) ? Carbon::parse($expires) : Carbon::now()->addYears(100);
        $whitelist->save();

        return $whitelist;
    }
}