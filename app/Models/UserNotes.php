<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotes extends Model
{
    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function admin() {
        return $this->belongsTo(User::class, 'admin_id');
    }
}
