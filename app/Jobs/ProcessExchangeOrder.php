<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Classes\OrderContract;
use Redis;
use App\Events\TradeHistoryEvent;
use App\Events\ExchangePublicUpdateEvent;
use Auth;
use App\Models\Setting;

class ProcessExchangeOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orderContract;
    protected $user;
    protected $isCancel = false;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderContract $orderContract, $user, $isCancel)
    {
        $this->orderContract = $orderContract;
        $this->user = $user;
        $this->isCancel = $isCancel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $isPause = Setting::getValue('exchange_pause');
        if ($isPause) {
            return false;
        }
        Redis::funnel('key')->limit(1)->then(function () {
            if ($this->isCancel) {
                if ($this->orderContract->refundOrder() === false) {
                    event(new TradeHistoryEvent($this->user, false));
                } else {
                    event(new TradeHistoryEvent($this->user, true));
                }
            } else {
                if ($this->orderContract->startBuySell()) {
                    event(new TradeHistoryEvent($this->user, true));
                    event(new ExchangePublicUpdateEvent(true));
                }else{
                    event(new TradeHistoryEvent($this->user,false));
                }
            }
            
        }, function () {
            // Could not obtain lock...
            //alert us if fail
            return $this->release(3);
        });
        
    }

   
}
