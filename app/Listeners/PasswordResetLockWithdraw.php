<?php

namespace App\Listeners;

use App\Models\User;
use App\Notifications\UserResetPassword;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetLockWithdraw implements ShouldQueue
{

    /**
     * Create the event listener.
     *
     * @param PasswordReset $reset
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param PasswordReset $event
     * @return void
     */
    public function handle(PasswordReset $event)
    {
        $user = User::whereId($event->user->id)->first();

        if($user) {

            \App\Models\Emails::create([
                'user_id'    => $user->id,
                'to'      => $user->email,
                'subject' => 'Password Reset Email'
            ]);

            $user->lock_withdraw = true;
            $user->lock_withdraw_time = Carbon::now()->addHours(48);
            $user->save();

            $user->notify(new UserResetPassword());
        }
    }
}
