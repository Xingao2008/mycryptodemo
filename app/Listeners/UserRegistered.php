<?php

namespace App\Listeners;


use App\Mail\ConfirmEmail;
use App\Models\Balance;
use App\Models\Emails;
use App\Models\Verification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistered implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(\App\Events\UserRegistered $event)
    {
        $user = $event->user;
        

        \Mail::to($user->email)->send(new ConfirmEmail($user->status_code));

        Emails::create([
            'to'      => $user->email,
            'subject' => 'Verify your email address',
        ]);

        foreach (config('currencies.allAvailableCurrencies') as $c) {
            Balance::create([
                'user_id'       => $user->id,
                'currency'        => $c,
                'balance'          => 0.000000000000000000,
                'locked'     => FALSE,
            ]);
        }
    }
}
