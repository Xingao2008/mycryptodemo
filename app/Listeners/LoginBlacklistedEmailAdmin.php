<?php

namespace App\Listeners;

use App\Events\LoginBlacklisted;
use App\Models\Firewall;
use App\Models\LoginAttempts;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LoginBlacklistedEmailAdmin implements ShouldQueue
{
    /**
     * LoginBlacklistedEmailAdmin constructor.
     * @param LoginBlacklisted $blacklisted
     */
    public function __construct(LoginBlacklisted $blacklisted)
    {
    }

    /**
     * Handle the event.
     *
     * @param  LoginBlacklisted $event
     * @return void
     */
    public function handle($event)
    {
        /*
                $attempts = LoginAttempts::whereIpAddress($event->blacklist->ip_address)->orderBy('created_at', 'asc')->get();

                \Mail::to('support@mycryptowallet.com.au')->send(
                    new \App\Mail\LoginBlacklisted(
                        $event->blacklist,
                        $attempts
                    )
                );
        */
    }
}
