<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
date_default_timezone_set('Australia/Melbourne');

Route::get('/mychart', function() {
    return view('welcome');
});

Route::get('/', 'PublicController@home')->name('home');
Route::get('/register', function() {
    return view('public.register');
})->name('register')->middleware([]);
Route::get('/login', function() {
    return view('public.login');
})->name('login')->middleware([]);

//Auth::routes();
// Authentication Routes...
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login')->middleware([]);
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('tfa_reset/{code}', 'Auth\LoginController@tfaReset')->name('login.tfareset');

Route::post('login/do-login', 'Auth\LoginController@doLogin');
Route::get('login/logout', 'Auth\LoginController@doLogout');

// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware([]);
Route::post('register', 'Auth\RegisterController@register')->middleware([]);
Route::post('register/resend-msg', 'Auth\RegisterController@sendSMS')->middleware([]);
Route::post('register/get-step', 'Auth\RegisterController@getStep')->middleware([]);
Route::post('register/mobile/verify', 'Auth\RegisterController@verifyRegisterMobile');
Route::post('register/addProfile', 'Auth\RegisterController@addProfile');
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm'
]);
Route::get('register/resend/{email}', 'Auth\RegisterController@resend');
Route::post('register/new_password', 'Auth\RegisterController@setPassword');

Route::get('signup', 'Auth\RegisterController@showRegistrationFlow')->name('signup')->middleware([]);

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email')->middleware([]);
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset')->middleware([]);
Route::post('password/new_reset', 'Auth\ResetPasswordController@newReset')->name('password.request')->middleware([]);
//Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request')->middleware([]);

// Public pages
Route::get('/about/marketplace', 'PublicController@marketplace')->name('about-marketplace');
Route::get('/about/withdrawls-deposits', 'PublicController@buy')->name('about-buy');
Route::get('/support', 'PublicController@support')->name('support');
Route::get('/about', 'PublicController@about')->name('about');
Route::get('/contact', 'PublicController@contact')->name('contact');
Route::get('/fees', 'PublicController@fees')->name('fees');

Route::view('/faq', 'public.faq')->name('faq');
Route::get('/news', 'PublicController@news')->name('news');

Route::get('/news/{slug}', 'PublicController@newsPost');

// PUBLIC LEGAL
Route::view('/legal', 'public.legal')->name('legal');
Route::get('/privacy-policy', 'PublicController@privacyPolicy')->name('privacy-policy');
Route::get('/terms-conditions-use', 'PublicController@termsConditionsUse')->name('terms-conditions-use');
Route::get('/terms-conditions-service', 'PublicController@termsConditionsService')->name('terms-conditions-service');
Route::get('/disclaimer', 'PublicController@disclaimer')->name('disclaimer');
Route::get('/copyright', 'PublicController@copyright')->name('copyright');
Route::get('/refer/id/{code}', 'ReferController@referee');

Route::get('/api/prices/btc', 'ApiController@btc');
Route::get('/api/prices/eth', 'ApiController@eth');
Route::get('/api/prices/ltc', 'ApiController@ltc');
Route::get('/api/prices/xrp', 'ApiController@xrp');
Route::get('/api/prices/pwr', 'ApiController@pwr');
Route::get('/api/balance', 'ApiController@getBalanceInAud');
Route::get('/api/assetUpdate', 'ApiController@getBalanceUpdateInAud');
// Route::any('/api/verification', 'ApiController@vixVerify');

Route::get('storage/news/{filename}', 'StorageController@postImage');
Route::get('test', 'ApiController@test')->name('test.test');
// Private pages
Route::middleware(['auth', 'status',])->group(function () {

    Route::get('/account-progress', 'VerifyController@accountProgress')->name('account-progress');
    
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/verify', 'VerifyController@index')->name('verify');
    Route::get('/verify/selfie', 'VerifyController@selfie')->name('selfie');
    Route::post('/verify/selfie', 'VerifyController@postSelfie')->name('selfie.upload');
    Route::get('/account', 'Auth\AccountController@index')->name('account');
    Route::post('/account', 'Auth\AccountController@update');
    Route::get('/account/tfa', 'Auth\AccountController@tfa')->name('tfa');
    Route::post('/account/tfa', 'Auth\AccountController@tfa_update');
    Route::get('/account/settings', 'Auth\AccountController@settings')->name('settings');
    Route::post('/account/settings', 'Auth\AccountController@settingsUpdate');
    Route::get('/account/upload_id', 'IdController@index')->name('id');
    Route::post('/account/upload_id', 'IdController@upload');
    Route::get('/account/upload_ubc', 'IdController@ubc_index')->name('ubc');
    Route::post('/account/upload_ubc', 'IdController@ubc_upload');
    Route::get('/account/verify_sms', 'IdController@sms_index')->name('sms');
    Route::post('/account/verify_sms', 'IdController@sms_verify');
    Route::get('/account/bank', 'IdController@bank')->name('bank');
    Route::post('/account/bank', 'IdController@bank_save');
    Route::get('/account/identity', 'Auth\AccountController@identity_index')->name('identity');
    Route::get('/account/password', 'Auth\AccountController@password')->name('password');
    Route::post('/account/password/go', 'Auth\AccountController@passwordgo');
    Route::get('/orderhistory', 'OrdersController@index')->name('orderhistory');
    Route::get('/refer', 'ReferController@index')->name('refer');
    Route::get('/refer/claim/{year}/{month}', 'ReferController@claim');
    // Route::get('/transactions', 'TransactionsController@index')->name('transactions');
    Route::get('storage/identification/{filename}', 'StorageController@getImage');
    Route::get('/manage-order', 'ExchangeController@manageOrder');

    Route::post('account/sms_bank_addr', 'Auth\VersionController@sms_verify');
    Route::post('account/sms_bank_addr/verify', 'Auth\VersionController@verify');

    Route::get('account/dashboard', 'DashboardController@indexNew')->name('dashboardNew');
    Route::middleware(['silvertier',])->group(function () {
        //Silver tier or higher:
        Route::get('/wallets/deposit/{path?}', array('uses' => 'DepositController@index'))->where('path', '.+')->name('deposit');
        Route::get('/wallets/withdraw/{path?}', array('uses' => 'WithdrawController@index'))->where('path', '.+')->name('withdraw');
        Route::post('/wallets/withdraw/{path?}/go', array('uses' => 'WithdrawController@go'))->where('path', '.+')->name('go');
        Route::get('/wallets/verify/withdraw/{path?}', array('uses' => 'WithdrawController@verify'))->where('path', '.+')->name('withdraw_verify');
        Route::post('/wallets/verify/withdraw/{path?}', array('uses' => 'WithdrawController@sms_verify'))->where('path', '.+')->name('verify_sms');
        Route::get('/wallets/ethCheckForDeposit', 'DepositController@ethCheckForDeposit')->name('ethCheckForDeposit');
        Route::get('/wallets/pwrCheckForDeposit', 'DepositController@pwrCheckForDeposit')->name('pwrCheckForDeposit');
        Route::get('/wallets/generate/{path?}', array('uses' => 'GenerateController@index'))->where('path', '.+')->name('generate');
        Route::get('/marketplace/{path?}', array('uses' => 'MarketplaceController@index'))->where('path', '.+')->name('marketplace');

        Route::post('/buy/process', 'BuySellController@buy_process');
        Route::get('/buy/confirm', 'BuySellController@buy_confirm');
        Route::get('/buy/{coin?}', 'BuySellController@buy_index')->name('buy');

        Route::post('/sell/process', 'BuySellController@sell_process');
        Route::get('/sell/confirm', 'BuySellController@sell_confirm');
        Route::get('/sell/{coin?}', 'BuySellController@sell_index')->name('sell');
        Route::get('/wallets', 'WalletsController@index')->name('wallets');

        Route::get('/currency', 'BuySellController@getCurrencyConvert')->name('currency');
        Route::post('/currency/convert', 'BuySellController@postCurrencyConvert');
    });

    Route::get('exchange', 'ExchangeController@index')->name('exchange');

    /**
     * New route to fire soap request to GBG
     */
    Route::post('verify/gbg', 'Auth\AccountController@verifyGBGAuthenticateSP');
});

Route::group(['prefix' => 'admin/tfa'], function(){
    Route::get('auth', '\App\Http\Controllers\Middleware\TwoFactorConfirmation@auth');
    Route::post('auth', '\App\Http\Controllers\Middleware\TwoFactorConfirmation@postAuth');
});

Route::namespace('Admin')->prefix('admin')->middleware(['auth', 'admin', 'permission:admin.dashboard'])->group(function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('/', 'DashboardController@index');

    Route::get('users', 'UsersController@index')->name('admin.users')->middleware('permission:admin.users.index');
    Route::get('users/select2', 'UsersController@select2')->name('admin.users.select2')->middleware('permission:admin.users.index');
    Route::get('users/datatables', 'UsersController@dataTables')->name('admin.users.datatables')->middleware('permission:admin.users.index');
    Route::post('users/{id}/address', 'UsersController@postAddress')->middleware('permission:admin.users.index');
    Route::get('users/{id}/edit', 'UsersController@edit')->name('admin.users.edit')->middleware('permission:admin.users.index');
    Route::post('users/{id}/edit', 'UsersController@postEdit')->name('admin.users.postedit')->middleware('permission:admin.users.index');

    Route::get('users_blocked', 'UsersController@getBlockedUsers')->name('admin.users.blocked')->middleware('permission:admin.users.blocked');
    Route::post('users_blocked/search', 'UsersController@searchBlockedUsers')->name('admin.blocked.search')->middleware('permission:admin.users.blocked');
    Route::get('users_unblock/{id}', 'UsersController@unBlockUsers')->name('admin.users.unblock')->middleware('permission:admin.users.blocked');

    Route::get('tfa_reset', 'TfaResetController@index')->name('admin.tfa_reset.index')->middleware('permission:admin.resettfa');
    Route::get('tfa_reset/user/{id}', 'TfaResetController@getReset')->name('admin.tfa_reset.user')->middleware('permission:admin.resettfa');
    Route::post('tfa_reset/{id}/upload', 'TfaResetController@upload')->name('admin.tfa_reset.upload')->middleware('permission:admin.resettfa');
    Route::get('tfa_reset/{id}', 'TfaResetController@show')->name('admin.tfa_reset.show')->middleware('permission:admin.resettfa');
    Route::post('tfa_reset/{id}', 'TfaResetController@postReset')->name('admin.tfa_reset.send')->middleware('permission:admin.resettfa');

    Route::post('users/fiat/notes', 'UsersController@fiatNotes')->middleware('permission:admin.users.index');
    Route::post('users/fiat/reverse', 'UsersController@reverseFiat')->middleware('permission:admin.users.index');
    Route::any('users/{user_id}/fiat', 'UsersController@fiat')->middleware('permission:admin.users.index');
    Route::post('users/{user_id}/notes', 'UsersController@postNote')->middleware('permission:admin.users.index')->name('admin.users.notes');

    Route::get('verifications', 'VerificationsController@index')->name('admin.verifications')->middleware('permission:admin.verifications.index');
    Route::get('verifications/{id}/edit', 'VerificationsController@edit')->middleware('permission:admin.verifications.index');
    Route::post('verifications/{id}/edit', 'VerificationsController@postEdit')->middleware('permission:admin.verifications.index');
    Route::get('verifications/{id}/approve', 'VerificationsController@approve')->middleware('permission:admin.verifications.index');
    Route::get('verifications/{id}/reject', 'VerificationsController@reject')->middleware('permission:admin.verifications.index');
    Route::get('verifications/{user_id}/manual', 'VerificationsController@manualVerify')->name('admin.verifications.manual')->middleware('permission:admin.verifications.index');
    Route::post('verifications/{user_id}/manual', 'VerificationsController@postManualVerify')->name('admin.verifications.manual')->middleware('permission:admin.verifications.index');
    


    Route::get('selfies', 'SelfieVerification@index')->middleware('permission:admin.selfie.index');
    Route::get('selfies/{id}', 'SelfieVerification@edit')->middleware('permission:admin.selfie.index')->name('admin.selfies.edit');
    Route::post('selfies/{id}', 'SelfieVerification@postEdit')->middleware('permission:admin.selfie.index');

    Route::get('funds-approval', 'FundsApprovalController@index')->name('admin.fundsApproval')->middleware('permission:admin.deposits.index', 'u2f');
    Route::get('funds-approval/datatables', 'FundsApprovalController@dataTable')->name('admin.fundsApproval.dataTable')->middleware('permission:admin.deposits.index');
    Route::get('funds-approval/{id}/add', 'FundsApprovalController@add')->name('admin.funds-approval.add')->middleware('permission:admin.deposits.index', 'u2f');
    Route::post('funds-approval/{id}/add', 'FundsApprovalController@postAdd')->name('admin.funds-approval.postAdd')->middleware('permission:admin.deposits.index', 'u2f');

    Route::post('withdrawals-approval/addAll', 'WithdrawalsApprovalController@addAll')->middleware('permission:admin.withdrawals.index');
    Route::get('withdrawals-approval', 'WithdrawalsApprovalController@index')->name('admin.withdrawalsApproval')->middleware('permission:admin.withdrawals.index');
    Route::any('withdrawals-approval/{id}/add', 'WithdrawalsApprovalController@add')->middleware('permission:admin.withdrawals.index');
    Route::get('withdrawals-approval/{id}/remove', 'WithdrawalsApprovalController@remove')->middleware('permission:admin.withdrawals.index');
    Route::get('withdrawals-approval/{id}/remove/go', 'WithdrawalsApprovalController@doRemove')->name('admin.withdrawalsApproval.doRemove')->middleware('permission:admin.withdrawals.index', '2fa.confirm');

    Route::get('balances', 'BalancesController@index')->name('admin.balances')->middleware('permission:admin.balances.index', 'u2f');
    Route::get('balances/{id}/list', 'BalancesController@list_balances')->middleware('permission:admin.balances.index', 'u2f');
    Route::any('balances/{id}/edit', 'BalancesController@edit')->middleware('permission:admin.balances.index', 'u2f');

    Route::get('ledgers', 'LedgersController@index')->middleware('permission:admin.ledgers.index');
    Route::get('ledgers/datatables', 'LedgersController@dataTables')->name('admin.ledgers.datatables')->middleware('permission:admin.ledgers.index');
    Route::get('ledgers/csv', 'LedgersController@csv')->name('admin.ledgers.csv')->middleware('permission:admin.ledgers.index');

    Route::get('orders/buys', 'OrdersController@buys')->name('admin.orders.buy')->middleware('permission:admin.orders.index');
    Route::get('orders/buys/datatables', 'OrdersController@buysDataTables')->name('admin.orders.buy.datatables')->middleware('permission:admin.orders.index');
    Route::get('orders/buys/{id}/approve', 'OrdersController@buys_approve')->middleware('permission:admin.orders.index');
    Route::get('orders/buys/{id}/pending', 'OrdersController@buys_pending')->middleware('permission:admin.orders.index');

    Route::get('orders/sells', 'OrdersController@sells')->middleware('permission:admin.orders.index');
    Route::get('orders/sells/datatables', 'OrdersController@sellsDataTables')->name('admin.orders.sell.datatables')->middleware('permission:admin.orders.index');
    Route::get('orders/sells/{id}/approve', 'OrdersController@sells_approve')->middleware('permission:admin.orders.index');


    Route::get('failed-transactions', 'FailedTransactionsController@index')->name('admin.failed-transactions')->middleware('permission:admin.failedtransactions.index');
    Route::any('failed-transactions/withdraw-removal/{id}', 'FailedTransactionsController@removeWithdrawal')->middleware('permission:admin.failedtransactions.index');

    Route::get('emails', 'EmailsController@index')->middleware('permission:admin.emails.index');
    Route::get('emails/datatables', 'EmailsController@dataTables')->name('admin.emails.datatables')->middleware('permission:admin.emails.index');
    Route::get('emails/import-email', 'EmailsController@importEmail')->name('admin.emails.importemail')->middleware('permission:admin.emails.index');
    Route::get('emails/email-list', 'EmailsController@emailList')->name('admin.emails.emaillist')->middleware('permission:admin.emails.index');
    Route::get('emails/email-form/{title?}', 'EmailsController@emailForm')->name('admin.emails.emailform')->middleware('permission:admin.emails.index');
    Route::post('emails/send-bulk-email', 'EmailsController@sendBulkEmail')->name('admin.emails.sendBulkEmail')->middleware('permission:admin.emails.index');


    Route::get('news', 'NewsController@index')->middleware('permission:admin.news.index');
    Route::any('news/new', 'NewsController@create')->middleware('permission:admin.news.index');
    Route::any('news/{id}/edit', 'NewsController@edit')->middleware('permission:admin.news.index');
    Route::any('news/save', 'NewsController@save')->middleware('permission:admin.news.index');
    Route::any('news/{id}/delete', 'NewsController@delete')->middleware('permission:admin.news.index');

    Route::get('settings', 'SettingsController@index')->name('admin.settings')->middleware('permission:admin.settings.index', 'u2f');
    Route::get('settings/pricing', 'SettingsController@pricing')->middleware('permission:admin.settings.index', 'u2f');
    Route::get('settings/limits', 'SettingsController@limits')->middleware('permission:admin.settings.index', 'u2f');
    Route::post('settings/store', 'SettingsController@store')->middleware('permission:admin.settings.index', 'u2f');

    Route::get('firewall', 'FirewallController@index')->middleware('permission:admin.firewall.index');
    Route::get('firewall/new', 'FirewallController@create')->middleware('permission:admin.firewall.index');
    Route::post('firewall/new', 'FirewallController@postCreate')->middleware('permission:admin.firewall.index');
    Route::get('firewall/{id}/delete', 'FirewallController@delete')->middleware('permission:admin.firewall.index');

    Route::get('polipay', 'PoliPayController@index')->middleware('permission:admin.polipay.index');
    Route::get('polipay/datatables', 'PoliPayController@dataTables')->name('admin.polipay.datatables')->middleware('permission:admin.polipay.index');
    Route::get('polipay/{id}/edit', 'PoliPayController@edit')->name('admin.polipay.edit')->middleware('permission:admin.polipay.index');
    Route::post('polipay/{id}/edit', 'PoliPayController@postEdit')->middleware('permission:admin.polipay.index');

    Route::get('polipay-deposit', 'PoliPayController@deposit')->middleware('permission:admin.polipay.index');
    Route::get('polipay-deposit/{id}/approve', 'PoliPayController@approve')->middleware('permission:admin.polipay.index');
    Route::get('polipay-deposit/{id}/remove', 'PoliPayController@remove')->middleware('permission:admin.polipay.index');

    Route::get('user_limits', 'UserLimitsController@index')->middleware('permission:admin.userlimits.index');
    Route::get('user_limits/{id}/edit', 'UserLimitsController@edit')->middleware('permission:admin.userlimits.index')->name('admin.userlimits.edit');
    Route::post('user_limits/{id}', 'UserLimitsController@update')->middleware('permission:admin.userlimits.index');

    Route::get('roles', 'RolesController@index')->middleware('permission:admin.roles.index');
    Route::get('roles/create', 'RolesController@create')->middleware('permission:admin.roles.index');
    Route::post('roles/create', 'RolesController@postCreate')->middleware('permission:admin.roles.index');
    Route::get('roles/{id}', 'RolesController@edit')->middleware('permission:admin.roles.index');
    Route::post('roles/{id}', 'RolesController@postEdit')->middleware('permission:admin.roles.index');

    Route::get('permissions', 'PermissionsController@index')->middleware('permission:admin.permissions.index');
    Route::get('permissions/create', 'PermissionsController@create')->middleware('permission:admin.permissions.index');
    Route::post('permissions/create', 'PermissionsController@postCreate')->middleware('permission:admin.permissions.index');
    Route::get('permissions/{id}', 'PermissionsController@edit')->middleware('permission:admin.permissions.index');
    Route::post('permissions/{id}', 'PermissionsController@postEdit')->middleware('permission:admin.permissions.index');
    Route::get('users/loginas/{id}', 'UsersController@impersonate')->name('admin.impersonate.take');

    Route::get('tickets/create/{id?}', 'TicketController@create')->name('admin.ticket.create')->middleware('permission:admin.users.index');
    Route::post('tickets/create', 'TicketController@postCreate')->name('admin.ticket.postCreate')->middleware('permission:admin.users.index');
    //Exchange features
    Route::get('exchange/fees', 'ExchangeController@getFeeList')->name('admin.exchange.fees.index')->middleware('permission:admin.exchange.index');
    Route::get('exchange/fees/{id}/edit', 'ExchangeController@getEditFee')->name('admin.exchange.fees.edit')->middleware('permission:admin.exchange.index');
    Route::patch('exchange/fees', 'ExchangeController@patchFee')->name('admin.exchange.fees.patch')->middleware('permission:admin.exchange.index');
    Route::get('exchange/fees/convert', 'ExchangeController@getConvert')->name('admin.exchange.fees.convert.index')->middleware('permission:admin.exchange.cashout');
    Route::get('exchange/fees/convert/search', 'ExchangeController@getConvert')->name('admin.exchange.fees.convert.search')->middleware('permission:admin.exchange.cashout');
    Route::post('exchange/fees/convert/cashout', 'ExchangeController@postCashout')->name('admin.exchange.fees.cashout')->middleware('permission:admin.exchange.cashout');
    Route::get('exchange/fees/convert/report', 'ExchangeController@getCashoutReport')->name('admin.exchange.fees.convert.report')->middleware('permission:admin.exchange.cashout');

    Route::get('exchange/orders', 'ExchangeController@getOrderCondition')->name('admin.exchange.orders.index')->middleware('permission:admin.exchange.index');
    Route::get('exchange/orders/search', 'ExchangeController@getOrderCondition')->name('admin.exchange.orders.search')->middleware('permission:admin.exchange.index');

    Route::get('exchange/pause', 'ExchangeController@getPause')->name('admin.exchange.pause')->middleware('permission:admin.exchange.pause');
    Route::post('exchange/pause', 'ExchangeController@postPause')->name('admin.exchange.pause')->middleware('permission:admin.exchange.pause');
   
});

Route::get('admin/users/leave/{id}', 'Admin\UsersController@leaveImpersonate')->name('admin.impersonate.leave');

Route::group(['prefix' => 'admin'], function () {
    Route::impersonate();
});

Route::group(['prefix' => 'polipay'], function () {
    Route::post('/', 'PoliPayController@index')->name('polipay.index')->middleware(['auth',]);
    Route::get('/success', 'PoliPayController@complete')->name('polipay.complete');
    Route::get('/cancelled', 'PoliPayController@cancelled')->name('polipay.cancelled');
    Route::any('/notify', 'PoliPayController@notify')->name('polipay.notify');
});
