<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('exchange_user_{id}', function ($user, $id) {
    $id = base64_decode($id);
    return (int) $user->id === (int) $id;
});
Broadcast::channel('exchange-public', function ($user) {
    return (int) $user->id === Auth::user()->id;
});