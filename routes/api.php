<?php

use Illuminate\Http\Request;
Event::listen('illuminate.query',function($query){
    var_dump($query);
});
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('verification', 'ApiController@vixVerify');
Route::post('/user/verify/attempts', 'ApiController@vixVerifyAttempts');
Route::get('/ttest', 'ApiController@test');

Route::namespace('Api')->middleware(['auth:api', 'status'])->group(function () {
    Route::get('me', 'UserController@show');
    Route::post('user', 'UserController@store');
    Route::get('wallets', 'UserController@wallets');
    Route::post('buy', 'UserController@buy');
    Route::post('sell', 'UserController@sell');
});

Route::namespace('Api')->middleware(['guest'])->group(function () {
    //testing API for the bot to login
    Route::post('login', 'AuthenticationController@login');
});

Route::group([

    'middleware' => 'api',
    'namespace' => 'Api'

], function ($router) {

    Route::post('refresh-token', 'AuthenticationController@refresh');
    Route::post('me', 'AuthenticationController@me');

    Route::get('ping','ExchangeController@pong');
    Route::post('create-orders','ExchangeController@postOrder');
    Route::get('trade-history','ExchangeController@getTradeHistory');
    Route::get('my-trade-history','ExchangeController@getMyTradeHistory');
    Route::get('market-price','ExchangeController@getPriceHistory');
    Route::get('my-recent-orders/{limit?}','ExchangeController@getMyRecentOrders');
    Route::get('current-price', 'ExchangeController@getCurrentPrice');
    Route::get('my-wallet', 'ExchangeController@getExchangeWalletData');
    Route::patch('cancel-exchange-order/{id}', 'ExchangeController@patchCancelOrder');

    Route::post('importEmail', 'EmailsController@import');
    Route::get('getEmailList', 'EmailsController@getList');
    Route::post('sendBulkEmail', 'EmailsController@sendBulkEmail');
});