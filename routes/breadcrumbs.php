<?php

\Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', url('dashboard'));
});

\Breadcrumbs::register('verify', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Verify', route('verify'));
});

\Breadcrumbs::register('account', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Account', route('account'));
});

\Breadcrumbs::register('account.password', function($breadcrumbs){
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Password', route('password'));
});

\Breadcrumbs::register('account.settings', function($breadcrumbs){
    $breadcrumbs->parent('account');
    $breadcrumbs->push('Settings', route('settings'));
});

\Breadcrumbs::register('account.tfa', function($breadcrumbs){
    $breadcrumbs->parent('account');
    $breadcrumbs->push('TFA', route('tfa'));
});

\Breadcrumbs::register('dashboard.marketplace', function($breadcrumbs){
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Marketplace', route('exchange.index'));
});