@component('mail::message')
@component('mail::panel')
Hi {{ $user->first_name }},

Your recent withdrawal request was cancelled. Your funds have been added back to your balance.

The message from the support team when they cancelled the request was:

*{{ $message }}*

Kind regards,

myCryptoWallet Support
@endcomponent
@endcomponent
