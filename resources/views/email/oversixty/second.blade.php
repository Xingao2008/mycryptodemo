@component('mail::message')
Hi there,

A user over 60 years old who we previously emailed about has now completed their Vix Verification. We have set the "verified" status to 0, so you will now need to log into the panel and verify them manually (go to Users and click Verify or similar).

Please ensure you are confident this is not a case of elderly fraud (e.g. by asking for a selfie) before approving.

*User ID*: {{ $user->id }}
*Full name*: {{ $user->name }}
*Email*: {{ $user->email }}
*DOB*: {{ $user->dob }}

Kind regards,
**myCryptoWallet Bot**
@endcomponent
