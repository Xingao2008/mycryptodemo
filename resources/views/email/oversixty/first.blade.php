@component('mail::message')
Hi there,

This is a system-generated message alerting you that a person over the age of 60 has just entered their date of birth. To prevent elderly fraud we recommend monitoring this account.

When they complete their Vix Verification, their account will not be approved and you will need to finalise the approval, perhaps by asking for a selfie.

*User ID*: {{ $user->id }}
*Full name*: {{ $user->name }}
*Email*: {{ $user->email }}
*DOB*: {{ $user->dob }}

Kind regards,
**myCryptoWallet Bot**
@endcomponent
