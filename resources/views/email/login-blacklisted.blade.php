@component('mail::message')

# IP has been Blacklisted for too many login attempts

**IP**: {{ $blacklist->ip_address }}

**Expires**: {{ $blacklist->expires }}

@component('mail::table')
| Email | Time |
|-------|------|
@foreach($attempts as $attempt)
| {{ $attempt->email }} | {{ $attempt->created_at }} |
@endforeach
@endcomponent



@component('mail::button', ['url' => url()->to('admin/firewall'), 'color' => 'red'])
    View Blacklist
@endcomponent


Thanks,
{{ config('app.name') }}
@endcomponent