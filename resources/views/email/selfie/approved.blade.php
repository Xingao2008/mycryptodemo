@component('mail::message')
# Your Selfie

This is just a quick message to let you know that your selfie was approved, you may now make withdrawals!

Thanks,<br>
myCryptoWallet
@endcomponent
