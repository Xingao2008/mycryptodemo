@component('mail::message')
# Your Selfie

Unfortunately your selfie was denied by staff.

Please see the staff notes and try again.

@component('mail::button', ['url' => route('verify')])
Verify
@endcomponent

@if($verification->selfie_notes)
### Staff Notes

{{ $verification->selfie_notes }}
@endif

Thanks,<br>
myCryptoWallet
@endcomponent