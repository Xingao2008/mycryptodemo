<div class="blockfolio">
    <div class="columns header level">
        <div class="level-left">
            <h2>Blockfolio</h2>
        </div>
    </div>
    <div class="columns body-content">
        <div class="column is-3">
        <!-- pie chart goes in here -->
            <piechart></piechart>
        </div>
        <div class="column is-6 asset-container">
            <div class="columns">
                <div class="asset-change">
                    <p class="value">$<span>5648121</span> / <span class="today">+1271.51 today</span></p>
                </div>
            </div>
        <!-- coin price. and coins -->
            <!-- row1 -->
            <div class="columns">
                <div class="column coins">
                    <div class="columns each-coin is-mobile">
                        <div class="column is-3 coin-logo">
                            <span class="coin-logo icon-btc"></span>
                        </div>
                        <div class="column is-4 coin-holding">
                            <span class="coin-name">BTC</span>
                            <br>
                            <span class="holding">{{@ get_crypto_balance('btc') }}</span>
                        </div>
                        <div class="column is-5 coin-value">
                            <b>${{@get_user_crypto_value('btc')}}</b><br>
                            <span class="change">+$88888888</span>
                        </div>
                    </div>
                </div>
                <div class="column coins">
                    <div class="columns each-coin is-mobile">
                        <div class="column is-3 coin-logo">
                            <span class="coin-logo icon-eth"></span>
                        </div>
                        <div class="column is-4 coin-holding">
                            <span class="coin-name">ETH</span>
                            <br>
                            <span class="holding">{{@ get_crypto_balance('eth') }}</span>
                        </div>
                        <div class="column is-5 coin-value">
                            <b>${{@get_user_crypto_value('eth')}}</b><br>
                            <span class="change">+$88888888</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row2 -->
            <div class="columns">
                <div class="column coins">
                    <div class="columns each-coin is-mobile">
                        <div class="column is-3 coin-logo">
                            <span class="coin-logo icon-ltc"></span>
                        </div>
                        <div class="column is-4 coin-holding">
                            <span class="coin-name">LTC</span>
                            <br>
                            <span class="holding">{{@ get_crypto_balance('ltc') }}</span>
                        </div>
                        <div class="column is-5 coin-value">
                            <b>${{@get_user_crypto_value('ltc')}}</b><br>
                            <span class="change">+$88888888</span>
                        </div>
                    </div>
                </div>
                <div class="column coins">
                    <div class="columns each-coin is-mobile">
                        <div class="column is-3 coin-logo">
                            <span class="coin-logo icon-rpl"></span>
                        </div>
                        <div class="column is-4 coin-holding">
                            <span class="coin-name">XRP</span>
                            <br>
                            <span class="holding">{{@ get_crypto_balance('xrp') }}</span>
                        </div>
                        <div class="column is-5 coin-value">
                            <b>${{@get_user_crypto_value('xrp')}}</b><br>
                            <span class="change">+$88888888</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row3 -->
            <div class="columns">
                <div class="column coins">
                    <div class="columns each-coin is-mobile">
                        <div class="column is-3 coin-logo">
                            <span class="coin-logo icon-pwr"></span>
                        </div>
                        <div class="column is-4 coin-holding">
                            <span class="coin-name">PWR</span>
                            <br>
                            <span class="holding">{{@ get_crypto_balance('pwr') }}</span>
                        </div>
                        <div class="column is-5 coin-value">
                            <b>${{@get_user_crypto_value('pwr')}}</b><br>
                            <span class="change">+$88888888</span>
                        </div>
                    </div>
                </div>
                <div class="column is-hidden-mobile">

                </div>
            </div>
        </div>
        <div class="column is-3">
        <!-- asset value -->
            <div style="margin-left: 15px;">
                <radialbar></radialbar>
            </div>
        </div>
    </div>
</div>