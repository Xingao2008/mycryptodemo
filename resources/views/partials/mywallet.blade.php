<div class="my-wallet">
    <div class="switch-field">
            <input type="radio" id="switch_aud" name="switch_3" value="yes" {{( 'aud' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'checked':'')}}/>
            <label for="switch_aud">AUD</label>
            <input type="radio" id="switch_nzd" name="switch_3" value="maybe" {{( 'nzd' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'checked':'')}}/>
            <label for="switch_nzd">NZD</label>
            <input type="radio" id="switch_usd" name="switch_3" value="maybe" {{( 'usd' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'checked':'')}}/>
            <label for="switch_usd">USD</label>
        </div>
    <div class="top">
        <div class="header">
            <h4>myWallet</h4>
        </div>
        <div class="wallet-body">
            <div class="is-mobile columns aud_deposit" {{( 'aud' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}} >
                <div class="column">{{ formatted_user_balance('aud') }} </div>
                <div class="column"><a href="{{ url()->to('/wallets/deposit/aud') }}" class="button btn-trans-blue">Deposit</a></div>
            </div>
            <div class="is-mobile columns nzd_deposit" {{( 'nzd' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>
                <div class="column">{{ formatted_user_balance('nzd') }} </div>
                <div class="column"><a href="{{ url()->to('/wallets/deposit/nzd') }}" class="button btn-trans-blue">Deposit</a></div>
            </div>
            <div class="is-mobile columns usd_deposit" {{( 'usd' == \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>
                <div class="column">{{ formatted_user_balance('usd') }} </div>
                <div class="column"><a href="{{ url()->to('/wallets/deposit/usd') }}" class="button btn-trans-blue">Deposit</a></div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="coin-feed">
            <div class="single-coin">
                <div class="wrap">
                    <div class="header">
                        <span class="icon-btc"></span>
                        <div class="the-coin">
                            <div class="price">{{@ get_crypto_balance('btc') }} BTC</div>
                            <span class="aud_deposit balance">${{@audFormat(convertFiatCurrency('aud','aud',get_user_crypto_value('btc')))}}</span>
                            <span class="nzd_deposit balance">${{@audFormat(convertFiatCurrency('aud','nzd',get_user_crypto_value('btc')))}}</span>
                            <span class="usd_deposit balance">${{@audFormat(convertFiatCurrency('aud','usd',get_user_crypto_value('btc')))}}</span>
                            @if (strpos(strtolower(request()->path()), 'sell') !== false && get_pending_aud_sell('btc') != 0)
                                <div class="pendingAmount">
                                    pending
                                    <span class="aud_deposit">AUD ${{@audFormat(get_pending_aud_sell('btc'))}}</span>
                                    <span class="nzd_deposit">NZD ${{@audFormat(convertFiatCurrency('aud','nzd',get_pending_aud_sell('btc')))}}</span>
                                    <span class="usd_deposit">USD ${{@audFormat(convertFiatCurrency('aud','usd',get_pending_aud_sell('btc')))}}</span>
                                </div>
                            @endif

                            @if (strpos(strtolower(request()->path()), 'buy') !== false && get_pending_crypto_buy('btc') != 0)
                                <div class="pendingAmount">pending {{@ get_pending_crypto_buy('btc')}}</div>
                            @endif
                        </div>
                        <div class="toggle">
                            <div class="icon-chevron-down"></div>
                        </div>
                    </div>
                </div>
                <div class="button-dropdown">
                    <div class="wrapper">
                            <div class="columns wallet-title">
                                <div class="column part-header">24hr Market Price</div>
                            </div>
                            <div class="columns is-mobile buy-sell">
                                <div class="column price">
                                    <span class="aud_deposit">${{@audFormat($price['btc']->bid)}}</span>
                                    <span class="nzd_deposit">${{@audFormat(convertFiatCurrency('aud','nzd',$price['btc']->bid))}}</span>
                                    <span class="usd_deposit">${{@audFormat(convertFiatCurrency('aud','usd',$price['btc']->bid))}}</span>
                                    <h4>(Sell)</h4>
                                    <!-- <div>
                                        @if ($bccomp_result['btc']['bid'] == 1)
                                            <div class="icon-long-arrow-down rotate"></div><span class="positive percentage">{{@ $price_change['btc'] }}
                                                %</span>
                                        @elseif ($bccomp_result['btc']['bid'] == -1)
                                            <div class="icon-long-arrow-down"></div><span class="negative percentage">{{@ $price_change['btc'] }}
                                                %</span>
                                        @else
                                            <span class="no-change">~</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="column">
                                    <span class="aud_deposit">${{ audFormat(remove_site_fee($price['btc']->ask)) }}</span>
                                    <span class="nzd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','nzd',$price['btc']->ask))) }}</span>
                                    <span class="usd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','usd',$price['btc']->ask))) }}</span>
                                    <h4>(Buy)</h4>
                                </div>
                            </div>
                            <div class="columns is-mobile">
                                <!-- Price Change for sell price -->
                                @if ($bccomp_result['btc']['bid'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['btc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['btc']['bid'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['btc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                                <!-- Price Change for buy price -->
                                @if ($bccomp_result['btc']['ask'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['btc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['btc']['ask'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['btc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="columns is-mobile coin-options">
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/withdraw/btc') }}">Withdraw</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/deposit/btc') }}">Deposit</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/buy', ['coin' => 'btc']) }}">Buy</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/sell', ['coin' => 'btc']) }}">Sell</a></div>
                    </div>
                </div>
            </div>
            <div class="single-coin">
                <div class="wrap">
                    <div class="header">
                        <span class="icon-eth"></span>
                        <div class="the-coin">
                            <div class="price">{{@ get_crypto_balance('eth') }} ETH</div>
                            <span class="aud_deposit balance">${{@audFormat(convertFiatCurrency('aud','aud',get_user_crypto_value('eth')))}}</span>
                            <span class="nzd_deposit balance">${{@audFormat(convertFiatCurrency('aud','nzd',get_user_crypto_value('eth')))}}</span>
                            <span class="usd_deposit balance">${{@audFormat(convertFiatCurrency('aud','usd',get_user_crypto_value('eth')))}}</span>
                            @if (strpos(strtolower(request()->path()), 'sell') !== false && get_pending_aud_sell('eth') != 0)
                                <div class="pendingAmount">
                                    pending
                                    <span class="aud_deposit">AUD ${{@audFormat(get_pending_aud_sell('eth'))}}</span>
                                    <span class="nzd_deposit">NZD ${{@audFormat(convertFiatCurrency('aud','nzd',get_pending_aud_sell('eth')))}}</span>
                                    <span class="usd_deposit">USD ${{@audFormat(convertFiatCurrency('aud','usd',get_pending_aud_sell('eth')))}}</span>
                                </div>
                            @endif

                            @if (strpos(strtolower(request()->path()), 'buy') !== false && get_pending_crypto_buy('eth') != 0)
                                <div class="pendingAmount">pending {{@ get_pending_crypto_buy('eth')}}</div>
                            @endif
                        </div>
                        <div class="toggle">
                            <div class="icon-chevron-down"></div>
                        </div>
                    </div>
                </div>
                <div class="button-dropdown">
                    <div class="wrapper">
                            <div class="columns wallet-title">
                                <div class="column part-header">24hr Market Price</div>
                            </div>
                            <div class="columns is-mobile buy-sell">
                                <div class="column price">
                                    <span class="aud_deposit">${{@audFormat($price['eth']->bid)}}</span>
                                    <span class="nzd_deposit">${{@audFormat(convertFiatCurrency('aud','nzd',$price['eth']->bid))}}</span>
                                    <span class="usd_deposit">${{@audFormat(convertFiatCurrency('aud','usd',$price['eth']->bid))}}</span>
                                    <h4>(Sell)</h4>
                                    <!-- <div>
                                        @if ($bccomp_result['eth']['bid'] == 1)
                                            <div class="icon-long-arrow-down rotate"></div><span class="positive percentage">{{@ $price_change['eth'] }}
                                                %</span>
                                        @elseif ($bccomp_result['eth']['bid'] == -1)
                                            <div class="icon-long-arrow-down"></div><span class="negative percentage">{{@ $price_change['eth'] }}
                                                %</span>
                                        @else
                                            <span class="no-change">~</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="column">
                                    <span class="aud_deposit">${{ audFormat(remove_site_fee($price['eth']->ask)) }}</span>
                                    <span class="nzd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','nzd',$price['eth']->ask))) }}</span>
                                    <span class="usd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','usd',$price['eth']->ask))) }}</span>
                                    <h4>(Buy)</h4>
                                </div>
                            </div>
                            <div class="columns is-mobile">
                                <!-- Price Change for sell price -->
                                @if ($bccomp_result['eth']['bid'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['eth'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['eth']['bid'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['eth'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                                <!-- Price Change for buy price -->
                                @if ($bccomp_result['eth']['ask'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['eth'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['eth']['ask'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['eth'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="columns is-mobile coin-options">
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/withdraw/eth') }}">Withdraw</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/deposit/eth') }}">Deposit</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/buy', ['coin' => 'eth']) }}">Buy</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/sell', ['coin' => 'eth']) }}">Sell</a></div>
                    </div>
                </div>
            </div>
            <div class="single-coin">
                <div class="wrap">
                    <div class="header">
                        <span class="icon-ltc"></span>
                        <div class="the-coin">
                            <div class="price">{{@ get_crypto_balance('ltc') }} LTC</div>
                            <span class="aud_deposit balance">${{@audFormat(convertFiatCurrency('aud','aud',get_user_crypto_value('ltc')))}}</span>
                            <span class="nzd_deposit balance">${{@audFormat(convertFiatCurrency('aud','nzd',get_user_crypto_value('ltc')))}}</span>
                            <span class="usd_deposit balance">${{@audFormat(convertFiatCurrency('aud','usd',get_user_crypto_value('ltc')))}}</span>
                            @if (strpos(strtolower(request()->path()), 'sell') !== false && get_pending_aud_sell('ltc') != 0)
                                <div class="pendingAmount">
                                    pending
                                    <span class="aud_deposit">AUD ${{@audFormat(get_pending_aud_sell('ltc'))}}</span>
                                    <span class="nzd_deposit">NZD ${{@audFormat(convertFiatCurrency('aud','nzd',get_pending_aud_sell('ltc')))}}</span>
                                    <span class="usd_deposit">USD ${{@audFormat(convertFiatCurrency('aud','usd',get_pending_aud_sell('ltc')))}}</span>
                                </div>
                            @endif

                            @if (strpos(strtolower(request()->path()), 'buy') !== false && get_pending_crypto_buy('ltc') != 0)
                                <div class="pendingAmount">pending {{@ get_pending_crypto_buy('ltc')}}</div>
                            @endif
                        </div>
                        <div class="toggle">
                            <div class="icon-chevron-down"></div>
                        </div>
                    </div>
                </div>
                <div class="button-dropdown">
                    <div class="wrapper">
                            <div class="columns wallet-title">
                                <div class="column part-header">24hr Market Price</div>
                            </div>
                            <div class="columns is-mobile buy-sell">
                                <div class="column price">
                                    <span class="aud_deposit">${{@audFormat($price['ltc']->bid)}}</span>
                                    <span class="nzd_deposit">${{@audFormat(convertFiatCurrency('aud','nzd',$price['ltc']->bid))}}</span>
                                    <span class="usd_deposit">${{@audFormat(convertFiatCurrency('aud','usd',$price['ltc']->bid))}}</span>
                                    <h4>(Sell)</h4>
                                    <!-- <div>
                                        @if ($bccomp_result['ltc']['bid'] == 1)
                                            <div class="icon-long-arrow-down rotate"></div><span class="positive percentage">{{@ $price_change['ltc'] }}
                                                %</span>
                                        @elseif ($bccomp_result['ltc']['bid'] == -1)
                                            <div class="icon-long-arrow-down"></div><span class="negative percentage">{{@ $price_change['ltc'] }}
                                                %</span>
                                        @else
                                            <span class="no-change">~</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="column">
                                    <span class="aud_deposit">${{ audFormat(remove_site_fee($price['ltc']->ask)) }}</span>
                                    <span class="nzd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','nzd',$price['ltc']->ask))) }}</span>
                                    <span class="usd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','usd',$price['ltc']->ask))) }}</span>
                                    <h4>(Buy)</h4>
                                </div>
                            </div>
                            <div class="columns is-mobile">
                                <!-- Price Change for sell price -->
                                @if ($bccomp_result['ltc']['bid'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['ltc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['ltc']['bid'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['ltc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                                <!-- Price Change for buy price -->
                                @if ($bccomp_result['ltc']['ask'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['ltc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['ltc']['ask'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['ltc'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="columns is-mobile coin-options">
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/withdraw/ltc') }}">Withdraw</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/deposit/ltc') }}">Deposit</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/buy', ['coin' => 'ltc']) }}">Buy</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/sell', ['coin' => 'ltc']) }}">Sell</a></div>
                    </div>
                </div>
            </div>
            <div class="single-coin">
                <div class="wrap">
                    <div class="header">
                        <span class="icon-rpl"></span>
                        <div class="the-coin">
                            <div class="price">{{@ get_crypto_balance('xrp') }} XRP</div>
                            <span class="aud_deposit balance">${{@audFormat(convertFiatCurrency('aud','aud',get_user_crypto_value('xrp')))}}</span>
                            <span class="nzd_deposit balance">${{@audFormat(convertFiatCurrency('aud','nzd',get_user_crypto_value('xrp')))}}</span>
                            <span class="usd_deposit balance">${{@audFormat(convertFiatCurrency('aud','usd',get_user_crypto_value('xrp')))}}</span>
                            @if (strpos(strtolower(request()->path()), 'sell') !== false && get_pending_aud_sell('xrp') != 0)
                                <div class="pendingAmount">
                                    pending
                                    <span class="aud_deposit">AUD ${{@audFormat(get_pending_aud_sell('xrp'))}}</span>
                                    <span class="nzd_deposit">NZD ${{@audFormat(convertFiatCurrency('aud','nzd',get_pending_aud_sell('xrp')))}}</span>
                                    <span class="usd_deposit">USD ${{@audFormat(convertFiatCurrency('aud','usd',get_pending_aud_sell('xrp')))}}</span>
                                </div>
                            @endif

                            @if (strpos(strtolower(request()->path()), 'buy') !== false && get_pending_crypto_buy('xrp') != 0)
                                <div class="pendingAmount">pending {{@ get_pending_crypto_buy('xrp')}}</div>
                            @endif
                        </div>
                        <div class="toggle">
                            <div class="icon-chevron-down"></div>
                        </div>
                    </div>
                </div>
                <div class="button-dropdown">
                    <div class="wrapper">
                            <div class="columns wallet-title">
                                <div class="column part-header">24hr Market Price</div>
                            </div>
                            <div class="columns is-mobile buy-sell">
                                <div class="column price">
                                    <span class="aud_deposit">${{@audFormat($price['xrp']->bid)}}</span>
                                    <span class="nzd_deposit">${{@audFormat(convertFiatCurrency('aud','nzd',$price['xrp']->bid))}}</span>
                                    <span class="usd_deposit">${{@audFormat(convertFiatCurrency('aud','usd',$price['xrp']->bid))}}</span>
                                    <h4>(Sell)</h4>
                                    <!-- <div>
                                        @if ($bccomp_result['xrp']['bid'] == 1)
                                            <div class="icon-long-arrow-down rotate"></div><span class="positive percentage">{{@ $price_change['xrp'] }}
                                                %</span>
                                        @elseif ($bccomp_result['xrp']['bid'] == -1)
                                            <div class="icon-long-arrow-down"></div><span class="negative percentage">{{@ $price_change['xrp'] }}
                                                %</span>
                                        @else
                                            <span class="no-change">~</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="column">
                                    <span class="aud_deposit">${{ audFormat(remove_site_fee($price['xrp']->ask)) }}</span>
                                    <span class="nzd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','nzd',$price['xrp']->ask))) }}</span>
                                    <span class="usd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','usd',$price['xrp']->ask))) }}</span>
                                    <h4>(Buy)</h4>
                                </div>
                            </div>
                            <div class="columns is-mobile">
                                <!-- Price Change for sell price -->
                                @if ($bccomp_result['xrp']['bid'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['xrp'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['xrp']['bid'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['xrp'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                                <!-- Price Change for buy price -->
                                @if ($bccomp_result['xrp']['ask'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['xrp'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['xrp']['ask'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['xrp'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="columns is-mobile coin-options">
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/withdraw/xrp') }}">Withdraw</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/deposit/xrp') }}">Deposit</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/buy', ['coin' => 'xrp']) }}">Buy</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/sell', ['coin' => 'xrp']) }}">Sell</a></div>
                    </div>
                </div>
            </div>
            <div class="single-coin">
                <div class="wrap">
                    <div class="header">
                        <span class="icon-pwr"></span>
                        <div class="the-coin">
                            <div class="price">{{@ get_crypto_balance('pwr') }} POWR</div>
                            <span class="aud_deposit balance">${{@audFormat(convertFiatCurrency('aud','aud',get_user_crypto_value('pwr')))}}</span>
                            <span class="nzd_deposit balance">${{@audFormat(convertFiatCurrency('aud','nzd',get_user_crypto_value('pwr')))}}</span>
                            <span class="usd_deposit balance">${{@audFormat(convertFiatCurrency('aud','usd',get_user_crypto_value('pwr')))}}</span>
                            @if (strpos(strtolower(request()->path()), 'sell') !== false && get_pending_aud_sell('pwr') != 0)
                                <div class="pendingAmount">
                                    pending
                                    <span class="aud_deposit">AUD ${{@audFormat(get_pending_aud_sell('pwr'))}}</span>
                                    <span class="nzd_deposit">NZD ${{@audFormat(convertFiatCurrency('aud','nzd',get_pending_aud_sell('pwr')))}}</span>
                                    <span class="usd_deposit">USD ${{@audFormat(convertFiatCurrency('aud','usd',get_pending_aud_sell('pwr')))}}</span>
                                </div>
                            @endif

                            @if (strpos(strtolower(request()->path()), 'buy') !== false && get_pending_crypto_buy('pwr') != 0)
                                <div class="pendingAmount">pending {{@ get_pending_crypto_buy('pwr')}}</div>
                            @endif
                        </div>
                        <div class="toggle">
                            <div class="icon-chevron-down"></div>
                        </div>
                    </div>
                </div>
                <div class="button-dropdown">
                    <div class="wrapper">
                            <div class="columns wallet-title">
                                <div class="column part-header">24hr Market Price</div>
                            </div>
                            <div class="columns is-mobile buy-sell">
                                <div class="column price">
                                    <span class="aud_deposit">${{@audFormat($price['pwr']->bid)}}</span>
                                    <span class="nzd_deposit">${{@audFormat(convertFiatCurrency('aud','nzd',$price['pwr']->bid))}}</span>
                                    <span class="usd_deposit">${{@audFormat(convertFiatCurrency('aud','usd',$price['pwr']->bid))}}</span>
                                    <h4>(Sell)</h4>
                                    <!-- <div>
                                        @if ($bccomp_result['pwr']['bid'] == 1)
                                            <div class="icon-long-arrow-down rotate"></div><span class="positive percentage">{{@ $price_change['pwr'] }}
                                                %</span>
                                        @elseif ($bccomp_result['pwr']['bid'] == -1)
                                            <div class="icon-long-arrow-down"></div><span class="negative percentage">{{@ $price_change['pwr'] }}
                                                %</span>
                                        @else
                                            <span class="no-change">~</span>
                                        @endif
                                    </div> -->
                                </div>
                                <div class="column">
                                    <span class="aud_deposit">${{ audFormat(remove_site_fee($price['pwr']->ask)) }}</span>
                                    <span class="nzd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','nzd',$price['pwr']->ask))) }}</span>
                                    <span class="usd_deposit">${{ audFormat(remove_site_fee(convertFiatCurrency('aud','usd',$price['pwr']->ask))) }}</span>
                                    <h4>(Buy)</h4>
                                </div>
                            </div>
                            <div class="columns is-mobile">
                                <!-- Price Change for sell price -->
                                @if ($bccomp_result['pwr']['bid'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['pwr'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['pwr']['bid'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['pwr'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                                <!-- Price Change for buy price -->
                                @if ($bccomp_result['pwr']['ask'] == 1)
                                <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p>+<b>{{@ $price_change['pwr'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-gain.png') }}">
                                    @elseif ($bccomp_result['pwr']['ask'] == -1)
                                        <!-- <div class="icon-long-arrow-down"></div> -->
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <p><b>{{@ $price_change['pwr'] }}</b>%</p>
                                        <img class="chart-change" src="{{ asset('img/bg-loss.png') }}">
                                    @else
                                    <div class="column positive percentage chart-bg">
                                        <div class="price-change">
                                        <span class="no-change">No changes</span>
                                    @endif
                                        </div>
                                </div>
                            </div>
                    </div>
                    <div class="columns is-mobile coin-options">
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/withdraw/pwr') }}">Withdraw</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/wallets/deposit/pwr') }}">Deposit</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/buy', ['coin' => 'pwr']) }}">Buy</a></div>
                        <div class="column button btn-trans-blue"><a href="{{ url()->to('/sell', ['coin' => 'pwr']) }}">Sell</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    @if ('nzd' == \Auth::user()->get_setting('default_fiat_currency', 'aud'))
    $(document).ready(function() {
        $('.aud_deposit').hide();
        $('.usd_deposit').hide();
        $('.nzd_deposit').show();
    });

    @elseif ('usd' == \Auth::user()->get_setting('default_fiat_currency', 'aud'))
    $(document).ready(function() {
        $('.aud_deposit').hide();
        $('.nzd_deposit').hide();
        $('.usd_deposit').show();
    });
    @else
    $(document).ready(function() {
        $('.aud_deposit').show();
        $('.nzd_deposit').hide();
        $('.usd_deposit').hide();
    });
@endif
</script>
@endpush