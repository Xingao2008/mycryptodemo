@if (count($breadcrumbs))

    <div class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a> <span class="seperator">/</span>
            @else
                <span>{{ $breadcrumb->title }}</span>
            @endif

        @endforeach
    </div>

@endif