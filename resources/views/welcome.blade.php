<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
    </head>
    <body>
        <div id="chart">
            <piechart></piechart>
            <radialbar></radialbar>
        </div>
    </body>
</html>
<script src=" {{ mix('js/dashboard-widgets.js') }} "></script>