<div class="" style="text-align: center;">
    @extends('errors.layout')
    @section('title', 'Blacklisted')
    @section('message', '')
</div>
<div class="container dashboard sub-page four-oh-four">
    <div class="overlay"></div>
    <div class="main">
        <div class="page-heading"><h2>Sorry, you have been blacklisted from the site.</h2></div>
        <div class="container-fluid">
            <p>Please contact staff if you believe this to be a mistake <a href="mailto:support@mycryptowallet.com.au">support@mycryptowallet.com.au</a></p>
        </div>
    </div>
</div>