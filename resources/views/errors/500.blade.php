<div style="text-align: center;">
	@extends('errors.layout')
	@section('title', 'Service Unavailable')
	@section('message', '')
</div>
<div class="container dashboard sub-page four-oh-four">
  <div class="overlay"></div>
  <div class="main">
    <div class="page-heading"><h2>Looks like we're having some technical difficulties, bare with us!</h2></div>
    <div class="container-fluid">
      @if(app()->bound('sentry') && !empty(\Sentry::getLastEventID()))
        <div class="subtitle">Error ID: {{ \Sentry::getLastEventID() }}</div>

        <!-- Sentry JS SDK 2.1.+ required -->
        <script src="https://cdn.ravenjs.com/3.3.0/raven.min.js"></script>

        <script>
            Raven.showReportDialog({
                eventId: '{{ Sentry::getLastEventID() }}',
                // use the public DSN (dont include your secret!)
                dsn: 'https://076bf0f24a4d4d48ad937d11bed5c32f@sentry.ecdev.com.au/6',
                @if(\Auth::user())
                  user: {
                      'name': "{{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }}",
                      'email': '{{ \Auth::user()->email }}',
                  }
                @endif
            });
        </script>
      @endif
    </div>
  </div>
</div>