<div class="" style="text-align: center;">
	@extends('errors.layout')
	@section('title', 'Page Not Found')
	@section('message', '')
</div>
<div class="container dashboard sub-page four-oh-four">
  <div class="overlay"></div>
  <div class="main">
    <div class="page-heading"><h2>Sorry, the page you are looking for could not be found.</h2></div>
    <div class="container-fluid">
    	<p>Back <a href="/">home?</a></p>
    </div>
  </div>
</div>