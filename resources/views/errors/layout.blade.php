<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 36px;
            padding: 20px;
        }
        .four-oh-four {
            text-align: center;
            height: 100vh;
            text-align: center;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            background: #37517e;
            background: -moz-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: -webkit-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#37517e', endColorstr='#00b5ff',GradientType=1 );
        }
        .overlay {
            background-image: url(../img/large-maze.svg);
            background-position: 0% 100%;
            background-size: cover;
            position: absolute;
            top: 0px;
            left: 0px;
            height: 100%;
            width: 100%;
        }
        .flex-center.position-ref.full-height {
            display: none;
        }
        .main {
            position: relative;
        }
        .four-oh-four a {
            color: #2ED0B5;
            font-weight: 600;
        }
        .four-oh-four h2 {
            color: white;
        }
        .container-fluid p {
            font-weight: 600;
            color: white;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title">
            @yield('message')
        </div>
    </div>
</div>
</body>
</html>
