<div style="text-align: center;">
	@extends('errors.layout')
	@section('title', 'Service Unavailable')
	@section('message', '')
</div>
<div class="container dashboard sub-page four-oh-four">
  <div class="overlay"></div>
  <div class="main">
    <div class="page-heading"><h2>myCryptoWallet is undergoing maintenance, we'll be back soon.</h2></div>
    <div class="container-fluid">
    	<p>Back <a href="/">home?</a></p>
    </div>
  </div>
</div>