@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>Contact</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">

        <div class="wrapper flex-wrap columns">
            <h4>Buy & Sell Through Our Instant Exchange Services</h4>
            <div class="flex-group align-normal">

              <div class="fifty column">
                <p>We’re excited to see how myCryptoWallet can advance your financial efforts and build a dedicated community of investors and businesses. We appreciate the support of all of our users and are always ready to help.</p>
                <br>
                <div class="single-contact-module">
                  <p>If you’re a business or investor looking to do business with myCryptoWallet or get in contact with our staff directly, please feel free to contact us at <a href="mailto:info@mycryptowallet.com.au">info@mycryptowallet.com.au</a>.</p>
                </div>
                <br>
                <div class="single-contact-module">
                  <p>If you’re looking for technical support or have a concern you would like addressed, we encourage you to contact our support team via our customer <a href="https://mycryptowallet.freshdesk.com/support/solutions">support page</a>. Here we will be able to process and resolve your claim quickly thanks to our trained support team available 24/7.</p>
                </div>
              </div>

              <div class="column is-hidden-mobile">
              </div>

            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- @include('public.partials.start-trading') -->

@endsection
