@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="back"><a href="/legal">< Back</a></div>
    <div class="page-heading"><h2>Disclaimer</p></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content" style="justify-content: flex-start;">
                <div class="content">
                  <p><strong>GENERAL </strong></p>
                  <p class="sind"><strong>1.</strong> MyCryptowallet (“the Company”), located at ‘www.mycryptowallet.com.au’ (“the Website”) provides various services as displayed on the Website. </p>

                  <p class="sind"><strong>2.</strong> The Company is not, inter alia, professionals and in no event whatsoever is the Company claiming to be. </p>

                  <p class="sind"><strong>3.</strong> The contents on the Website are not, without limitations, advice and/or professional advice and should not be construed and/or considered to be. </p>

                  <p class="sind"><strong>4.</strong> No warranties</strong></p>

                  <p class="sind"><strong>4.1.</strong> The general information on the Website is provided without any representations or warranties express or implied. </p>

                  <p class="sind"><strong>4.2.</strong> Without limiting the scope of Section 4.1,  we do not warrant and/or represent that the information on the Website :- </p>

                  <p class="lind"><strong>(a)</strong> will be constantly available, or available at all; or </p>

                  <p class="lind"><strong>(b)</strong> is true, accurate, complete, current or non-misleading. </p>

                  <p class="sind"><strong>5. Interactive features</strong></p>

                  <p class="sind"><strong>5.1.</strong> The Website may include interactive features that allow users to communicate with us.</p>

                  <p class="sind"><strong>5.2.</strong> You acknowledge that, because of the limited nature of communication through the Website's interactive features, any assistance you may receive using any such features is likely to be incomplete and may even be misleading.</p>

                  <p class="sind"><strong>5.3.</strong> Any assistance you may receive using any of the Website's interactive features does not constitute specific advice and accordingly should not be relied upon without further independent confirmation.</p>

                  <p><strong>CONTENTS ON THE WEBSITE</strong></p>
                  <p class="sind"><strong>6.</strong> All information contained, posted and/or displayed on the Website is for information purposes only. </p>

                  <p class="sind"><strong>7.</strong> The Company is not and will not be liable, either expressly or in an implied manner, and is not responsible for any physical and/or emotional problems that may and/or could occur from any of the information whether for nutrition or otherwise on the Website.</p>

                  <p><strong>DISCLAIMER OF WARRANTIES </strong></p>
                  <p class="sind"><strong>8.</strong> You agree that use of the Website is at your sole risk. All information and content are provided on an "as is" and "as available" basis.</p>

                  <p class="sind"><strong>9.</strong> The Company, its affiliates, agents and licensors cannot and do not warrant the accuracy, completeness, correctness, non-infringement, merchantability, or fitness for a particular purpose of the information, materials and/or content available through the Website.</p>

                  <p class="sind"><strong>10.</strong> The Company does not guarantee the Website to be error-free, secure, continuously available or free of viruses or other harmful components.</p>

                  <p class="sind"><strong>11.</strong> You also agree that if you rely on any data or information obtained through the Website, you do so at your own risk. You are solely responsible for any damage or loss that results from your use of any material and/or data.</p>

                  <p class="sind"><strong>12.</strong> The Company and the contents and/or information on the Website are provided with the understanding that neither the Company nor its users, while such users are participating in the sites, are engaged in rendering legal, medical, counselling or other professional services or advice. The contents and/or information on the website are not substitute for professional services or advice.</p>

                  <p class="sind"><strong>13.</strong> The Company its affiliates, agents, and licensors make no warranty regarding any goods or services referred to, advertised on, or obtained through the Websites including without limitations, references to websites, other than those express warranties the Company itself specifically makes.</p>

                  <p><strong>LIMITATION OF LIABILITY</strong></p>
                  <p class="sind"><strong>14.</strong> Under no circumstances will the Company or its affiliates, agents or licensors be liable to you or anyone else for including but not limited to any loss and/or damages arising out of your use of any of the Website, including, without limitation, liability for consequential, special, incidental, indirect, or similar damages, even if we are advised beforehand of the possibility of such damages. </p>

                  <p class="sind"><strong>15.</strong> Without prejudice to Clause 14 herein, nothing in this disclaimer will :- </p>

                  <p class="lind"><strong>(a)</strong> limit or exclude any liability for death or personal injury resulting from negligence; </p>
                  <p class="lind"><strong>(b)</strong> limit or exclude any liability for fraud or fraudulent misrepresentation; </p>
                  <p class="lind"><strong>(c)</strong> limit any liabilities in any way that is not permitted under applicable law; or </p>
                  <p class="lind"><strong>(d)</strong> exclude any liability that may not be excluded under applicable law. </p>
                  <p><strong>THIRD PARTY LIABILITY</strong></p>
                  <p class="sind"><strong>16.</strong> By using the Website, you agree and consent that any loss and/or damages arising out of or in connection to, without limitation to the negligence, fault, mistake, misrepresentation and/or fraud of any third party and/or any services provided to you by the third party is the responsibility and liability of the individual and/or group of third party, and you hereby agree that you will only claim against and seek relief against the third party and the Company will be free of any liability/responsibility whatsoever. </p>

                  <p><strong>INDEMNIFICATION</strong></p>
                  <p class="sind"><strong>17.</strong> In the event that the Company becomes a party to the third party proceedings, you agree to indemnify, defend and hold harmless, without limitations, the Company, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys’ fees) relating to or arising out of without limitations, the negligence, fault, mistake, misrepresentation and/or fraud of the third party.</p>

                  <p>Effective as of 27/10/2017.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  @include('public.partials.start-trading')
@endsection
