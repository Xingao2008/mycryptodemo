@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    @if($post->featured_image)
      <div class="single-news-banner-image"><img src="{{ url('') }}/storage/{{ $post->featured_image }}"></div>
    @endif
    <div class="page-heading single-post-heading"><h2>{{ $post->title }}</h2><p class="post-meta"><span class="author">{{ $post->author }}</span> posted on <span class="posted">{{ $post->created_at->format('d M Y') }}</span></p></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content">
                <div class="content">
                  {!! $post->content !!}
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
