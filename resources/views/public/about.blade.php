@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>About myCryptoWallet</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <h4>The Next World Leader in Blockchain Commerce Solutions</h4>
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>myCryptoWallet is a digital currency service and marketplace based in Australia. We provide a number of services to our customers and offer the most control when it comes to the investment of their digital wealth in the online marketplace. These services include fiat deposit and withdrawal wallet services, a live peer-to-peer digital marketplace and instantaneous exchange between Australian, New Zealand and digital currencies. Investors and businesses can participate in the buying and selling of up to four different cryptocurrencies including Bitcoins, Litecoins, Ethereum and Ripple.</p>
                <p>Through rigorous efforts, we’ve created myCryptoWallet to be the safest and most secured blockchain commerce solution available in the world today. Our platform is currently under today’s international AML/CTF and KYC laws, and we maintain constant vigilance when it comes to changes in these laws and how they affect our platform. Our system also incorporates the latest in blockchain distributed ledger technology to secure and trace all transactions made by users while preventing the system from ever being at risk or compromised.</p>
              </div>
              <div class="fifty">
                <p>As is part of our philosophy and mission, we embrace the anonymity of our users as they engage in our live marketplace, but we also respect their need for assurance against fraud and fake accounts. This is why each new member goes through a rigorous third party Electronic Identity Verification process before being allowed to create their Crypto Wallet and buy & sell with other users.</p>
                <p>Our team also dedicates efforts to keeping investors up-to-date on all the latest market trends while giving them access to the best market advice and news available. With a 24/7 support team on standby, there’s nothing to stopping users from maximizing their crypto currency reserves.</p>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
