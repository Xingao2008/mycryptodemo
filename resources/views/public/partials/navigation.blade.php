<!-- navigation -->
<div class="navigation">
          @if (Route::getCurrentRoute()->uri() != '/')
            <div class="overlay"></div>
          @endif
          <div class="inner">
            <div class="logo"><a href="/"><img src="{{ url('img/myCryptoWallet.png') }}"></a></div>
            <div class="links">
              <ul>
                @if (Auth::user())
                <div class="columns user">
                    <li class="column">
                        <a href="{{ route('dashboard') }}"><img src="{{ url('img/nav/login/dashboard.png') }}"></a>
                        <br>
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </li>
                    <li class="column">
                        <a href="{{ route('account-progress') }}"><img src="{{ url('img/nav/login/verified.png') }}"></a>
                        <br>
                        <a href="{{ route('account-progress') }}">Get Verified</a>
                    </li>
                    <li class="column">
                        <a href="{{ route('refer') }}"><img src="{{ url('img/nav/login/refer.png') }}"></a>
                        <br>
                        <a href="{{ route('refer') }}">Refer</a>
                    </li>
                    <li class="column">
                        <a href="{{ route('wallets') }}"><img src="{{ url('img/nav/login/wallet.png') }}"></a>
                        <br>
                        <a href="{{ route('wallets') }}">Wallets</a>
                    </li>
                    <li class="column has-children">
                        <img src="{{ url('img/nav/login/exchange.png') }}"></a>
                        <br>
                        <div class="columns">
                            <div class="column">
                                <a href="{{ route('exchange') }}#/aud/btc" onClick="window.location.reload()">Exchange</a>
                                <ul>
                                    <li>
                                        <a href="exchange#/aud/btc" onClick="window.location.reload()">AUD/BTC</a>
                                        <a href="exchange#/aud/ltc" onClick="window.location.reload()">AUD/LTC</a>
                                        <a href="exchange#/aud/eth" onClick="window.location.reload()">AUD/ETH</a>
                                        <a href="exchange#/aud/xrp" onClick="window.location.reload()">AUD/XRP</a>
                                        <a href="exchange#/aud/pwr" onClick="window.location.reload()">AUD/PWR</a>
                                        <a href="exchange#/btc/ltc" onClick="window.location.reload()">BTC/LTC</a>
                                        <a href="exchange#/btc/eth" onClick="window.location.reload()">BTC/ETH</a>
                                        <a href="exchange#/btc/xrp" onClick="window.location.reload()">BTC/XRP</a>
                                        <a href="exchange#/btc/pwr" onClick="window.location.reload()">BTC/PWR</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    </li>
                    <li class="column has-children">
                        <img src="{{ url('img/nav/login/buysell.png') }}">
                        <br>
                        <div class="columns">
                            <div class="column">
                                <a href="#">Buy/Sell</a>
                                <ul>
                                    <li>
                                        <a href="{{ route('buy') }}">Buy</a>
                                        <a href="{{ route('sell') }}">Sell</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
<!-- {{--
<li class="has-children">
    <a href="#">Marketplace</a>
    <ul>
        <li>
            <a href="{{ route('marketplace') }}/btc"><div class="icon icon-btc"></div>AUD/BTC</a>
            <a href="{{ route('marketplace') }}/eth"><div class="icon icon-eth"></div>AUD/ETH</a>
            <a href="{{ route('marketplace') }}/ltc"><div class="icon icon-ltc"></div>AUD/LTC</a>
            <a href="{{ route('marketplace') }}/xrp"><div class="icon icon-rpl"></div>AUD/XRP</a>
        </li>
    </ul>
</li> --}} -->
                    <li class="column has-children">
                        <img src="{{ url('img/nav/login/user.png') }}">
                        <br>
                        <div class="columns">
                            <div class="column">
                                <a href="#">{{ first_name() }}</a>
                                <ul>
                                    <li><a href="{{ route('account')}}">Account</a></li>
                                    <li><a href="{{ route('orderhistory')}}">Orders</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    @can('admin.dashboard')
                    <li class="column">
                        <a href="{{ url()->to('/admin/dashboard') }}"><img src="{{ url('img/nav/login/user.png') }}"></a>
                        <br>
                        <a href="{{ url()->to('/admin/dashboard') }}">Admin</a>
                    </li>
                </div>
                    @endcan
                @endif
                @if (Auth::guest())
                    <div class="columns guest">
                        <li class="column">
                            <a href="{{ route('about-marketplace') }}">
                                <img src="{{ url('img/nav/marketplace.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('about-marketplace') }}">Marketplace</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('about-buy') }}">
                                <img src="{{ url('img/nav/withdrawls.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('about-buy') }}">Withdrawals<br>Deposits</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('faq') }}">
                                <img src="{{ url('img/nav/faq.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('faq') }}">FAQ</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('about') }}">
                                <img src="{{ url('img/nav/about.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('about') }}">About</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('news') }}">
                                <img src="{{ url('img/nav/news.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('news') }}">News</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('contact') }}">
                                <img src="{{ url('img/nav/contact.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('contact') }}">Contact</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('login') }}">
                                <img src="{{ url('img/nav/login.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('login') }}">Login</a>
                        </li>
                        <li class="column">
                            <a href="{{ route('register') }}">
                                <img src="{{ url('img/nav/register.png') }}">
                            </a>
                            <br>
                            <a href="{{ route('register') }}">Register</a>
                        </li>
                    </div>
                @endif
              </ul>
            </div>
          </div>
          <img class="underlay-bg" src="{{ url('img/nav/HeaderBG.png') }}">
        </div>
        <div class="mobile-navigation">
          <div class="inner">
            <div class="logo"><a href="/"><img src="{{ url('img/myCryptoWallet.png') }}"></a></div>
            <div class="hamburger">
            {{--
              @if (Auth::guest())
                <div class="link-btn"><a href="{{ route('register') }}">Register</a></div>
              @endif
            --}}
              <div class="icon-menu"></div>
            </div>
          </div>
        </div>
        <div class="mobile-menu">
          <div class="inner">
            <ul>
                @if (Auth::user())
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('verify') }}">Get Verified</a></li>
                    <li class="has-children"><a href="{{ route('wallets') }}">Wallets</a>
                        <ul>
                            <li>
                                <a href="{{ route('currency') }}">Fiat Swap</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-children"><a href="{{ route('exchange') }}#/aud/btc" onClick="window.location.reload()">⚡️Exchange⚡️</a>
                      <ul>
                          <li>
                              <a href="exchange#/aud/btc" onClick="window.location.reload()">AUD/BTC</a>
                              <a href="exchange#/aud/ltc" onClick="window.location.reload()">AUD/LTC</a>
                              <a href="exchange#/aud/eth" onClick="window.location.reload()">AUD/ETH</a>
                              <a href="exchange#/aud/xrp" onClick="window.location.reload()">AUD/XRP</a>
                              <a href="exchange#/aud/pwr" onClick="window.location.reload()">AUD/PWR</a>
                              <a href="exchange#/btc/ltc" onClick="window.location.reload()">BTC/LTC</a>
                              <a href="exchange#/btc/eth" onClick="window.location.reload()">BTC/ETH</a>
                              <a href="exchange#/btc/xrp" onClick="window.location.reload()">BTC/XRP</a>
                              <a href="exchange#/btc/pwr" onClick="window.location.reload()">BTC/PWR</a>
                          </li>
                      </ul>
                    </li>
                    <li class="has-children"><a href="#">Buy/Sell</a>
                        <ul>
                            <li>
                                <a href="{{ route('buy') }}">Buy</a>
                                <a href="{{ route('sell') }}">Sell</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-children">
                        <a href="#">{{ first_name() }}</a>
                        <ul>
                            <li><a href="{{ route('account')}}">Account</a></li>
                            <li><a href="{{ route('orderhistory')}}">Orders</a></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
                @if (Auth::guest())
                    <li><a href="{{ route('about-marketplace') }}">Marketplace</a></li>
                    <li><a href="{{ route('about-buy') }}">Withdrawls/Deposits</a></li>
                    <li><a href="{{ route('faq') }}">FAQ</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li><a href="{{ route('news') }}">News</a></li>
                    <li><a href="{{ route('contact') }}">Contact</a></li>
                    <li style="margin-top: 20px;"><a href="{{ route('login') }}">Login</a></li>
                    <li style="margin-top: 20px;"><a href="{{ route('register') }}">Register</a></li>
                @endif
            </ul>
          </div>
        </div>