<section class="home content-body">
    <!-- EFTPOS Content -->
    <div class="container eftpos">
        <h2 class="title">Spend & Exchange Multiple Currencies, Using myCryptoWallet</h2>
        <div class="columns">
            <div class="column is-3 is-hidden-mobile">
                <div class="level">
                    <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/terminal.png') }}" />
                    <p>Works at all EFTPOS</p>
                </div>
                <div class="level">
                    <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/fund.png') }}" />
                    <p>Instantly fund your card by selling your Cryptocurrency</p>
                </div>
            </div>
            <div class="column is-6 card-wrapper">
                <img src="{{ url('img/eftpos/card.png') }}" />
            </div>
            <div class="column is-3">
                <div class="is-hidden-tablet">
                    <div class="level">
                        <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/terminal.png') }}" />
                        <p>Works at all EFTPOS</p>
                    </div>
                    <div class="level">
                        <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/fund.png') }}" />
                        <p>Instantly fund your card by selling your Cryptocurrency</p>
                    </div>
                </div>
                <div class="level">
                    <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/atm.png') }}" />
                    <p>Free AUD Withdraws from ATMs in Australia</p>
                </div>
                <div class="level">
                    <img class="icons is-4 is-offset-8" src="{{ url('img/eftpos/pay.png') }}" />
                    <p>Pay for services using Cryptocurrency</p>
                </div>
            </div>
        </div>
        <div class="button-wrapper">
            <!-- TODO - Link to the Eftpos page to be added -->
            <a href="#" class="btn btn-blue-fading">Order yours now</a>
        </div>
    </div>
    <!-- Cross-platform content -->
    <div class="cross-platform">
        <h2 class="title">myCryptoWallet is your complete cryptocurrency solution to trade, store and track cryptocurrencies.</h2>
        <div class="columns">
            <div class="is-12">
                <img src="{{ url('img/crossplatform/all-platforms.png') }}" />
            </div>
        </div>
    </div>
    <!-- Instant Verification  -->
    <div class="instant-verify">
        <h2 class="title">Instant Verification for <br><span class="is-hidden-mobile">🇦🇺Australia, 🇺🇸USA, 🇨🇦Canada, 🇬🇧U.K and 🇳🇿New Zealand</span></h2>
        <h2 class="title is-hidden-tablet">🇦🇺Australia</h2>
        <h2 class="title is-hidden-tablet">🇺🇸USA</h2>
        <h2 class="title is-hidden-tablet">🇨🇦Canada</h2>
        <h2 class="title is-hidden-tablet">🇬🇧U.K</h2>
        <h2 class="title is-hidden-tablet">🇳🇿New Zealand</h2>
        <div class="container">
            <div class="columns register-flow">
                <div class="column is-4">
                    <img src="{{ url('img/instant-verification/register.png') }}" />
                    <!-- <div class="line"></div> -->
                    <h3>Register</h3>
                </div>
                <div class="column is-4">
                    <img src="{{ url('img/instant-verification/verify.png') }}" />
                    <h3>Verify</h3>
                </div>
                <div class="column is-4">
                    <img src="{{ url('img/instant-verification/buy-sell.png') }}" />
                    <h3>Buy & Sell</h3>
                </div>
            </div>
        </div>
    </div>
    <!-- Trading dashboard and graphs on the homepage -->
    <div class="trading-dashboard" style="max-width:800px; margin:0 auto 50px auto;">
        @include('partials.charts')
    </div>

    <!-- App transactions and email subscribtion  -->
    <div class="app-email">
        <!-- Logo column -->
        <div class="columns is-mobile">
            <div class="column is-3"></div>
            <div class="logo-container column is-6">
                <div class="logo"><a href="/"><img src="{{ url('img/myCryptoWallet.png') }}"></a></div>
            </div>
            <div class="column is-3"></div>
        </div>
        <div class="columns">
            <div class="app-demo column is-7">
                <h3 class="title">Hold and exchange multiple currencies instantly</h3>
                <img class="is-hidden-mobile" src="{{ url('img/hold-currencies/phones-with-bg.png') }}">
                <img class="is-hidden-tablet" src="{{ url('img/hold-currencies/iphones-crypto-trading-app.png') }}">
            </div>
            <div class="email-subscribe column is-5">
            @include ('public.partials.email')
            </div>
        </div>
    </div>

    <div class="stats">
        <div class="columns container">
            <div class="column">
                <h2 class="title is-size-2 lightblue">
                    $20M +
                </h2>
                <p>Digital currency exchanged</p>
            </div>
            <div class="column">
                <h2 class="title is-size-2 lightblue">12</h2>
                <p>Countries supported</p>
            </div>
            <div class="column">
                <h2 class="title is-size-2 lightblue">120K</h2>
                <p>Customers served</p>
            </div>
        </div>
        <img class="planet" src="{{ url('img/planet.png') }}" />
    </div>

</section>