<section class="home hero is-fullheight">
    <div class="columns">
        <div class="column is-1"></div>
        <div class="column is-7">
            <div class="hero-body">
                <div class="container">
                    <h2 class="title">
                        FREE AUD, USD, NZD Deposits and Withdrawals
                    </h2>
                    <h2 class="subtitle">
                        Australia's Bitcoin, Ethereum, Litecoin, Ripple & PowerLedger exchange has launched.
                    </h2>
                    <a href="/register" class="button btn-blue-fading is-medium">Start Now</a>
                </div>
            </div>
        </div>
        <div class="mobile-mockup column is-4">
            <img src="{{ url('img/iPhone-X-Mockup.png') }}" />
        </div>
    </div>
</section>