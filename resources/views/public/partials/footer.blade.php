@if(request()->route()->getName() == 'home')
    <div class="homepage footer">
@else
    <div class="footer">
@endif
        <img class="geometry" src="{{ url('img/footer-top.png') }}">
        <div class="background-wrapper">
            <div class="inner">
                <div class="top columns">
                    <div class="logo column">
                        <img src="{{ url('img/myCryptoWallet.png') }}">
                        <br><br>
                        <p>ABN 14 619 265 548<br>
                        <a style="color: #4084D3; text-decoration: none;font-family: 'greycliff-demibold';" href="/legal">Legal</a>
                        </p><br>
                        <p>PO BOX 2250, VIC, 3429 Australia<br>
                            <a style="color: #4084D3; text-decoration: none;font-family: 'greycliff-demibold';" href="mailto:info@mycryptowallet.com.au">info@mycryptowallet.com.au</a>
                        </p>
                        <span class="socials">
                            <a href="https://www.facebook.com/mycryptowallet/" target="_blank"><div class="icon-facebook"></div></a>
                            <a href="https://twitter.com/mycryptowallet?lang=en" target="_blank"><div class="icon-twitter"></div></a>
                        </span>
                    </div>
                    <div class="links column">
                        <ul>
                            <li><a href="{{ route('about-marketplace') }}">Marketplace</a></li>
                            <li><a href="{{ route('about') }}">About</a></li>
                            <li><a href="{{ route('about-buy') }}">Withdrawls/Deposits</a></li>
                            <li><a href="https://mycryptowallet.freshdesk.com/support/solutions">FAQ</a></li>
                            <li><a href="{{ route('news') }}">News</a></li>
                            <li><a href="{{ route('contact') }}">Contact</a></li>
                            <li><a href="{{ route('contact') }}">Support</a></li>
                            <li><a href="{{ route('fees') }}">Fees</a></li>
                        </ul>
                    </div>
                    <div class="secure column is-hidden-mobile">
                        <img src="{{ url('img/secure.png') }}">
                    </div>
                    <div class="buttons column">
                        @if (Auth::user())
                            <div class="columns">
                                <a class="btn btn-grey" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    Log Out
                                </a>
                            </div>
                        @endif
                        @if (Auth::guest())
                        <div class="columns">
                            <a class="column btn btn-grey" href="{{ route('login') }}">Log In</a>
                            <a class="column btn btn-blue-fading" href="{{ route('register') }}">Register</a>
                        </div>
                        @endif
                        <div class="aica columns">
                            <a href="http://adca.asn.au/" target="_BLANK"><img src="{{ asset('img/adca-white.png') }}"></a>
                        </div>
                    </div>
                </div>
                <!-- <div class="bottom" style="display: none;">
                    <p></p>
                </div> -->
            </div>
            <img class="blue-light" src="{{ url('img/footer-bottom.png') }}">
        </div>
</div>