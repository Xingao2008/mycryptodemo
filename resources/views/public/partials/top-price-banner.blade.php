<!-- Real time pricing banner -->
<div class="top-price-block is-centered">
    <div class="columns wrapper is-mobile">
        <span class="column is-one-quarter-mobile"> BTC:<span class="highlight-blue"> ${{ audFormat(remove_site_fee($price['btc']->ask)) }} <br></span></span>
        <span class="column is-one-quarter-mobile"> LTC:<span class="highlight-blue"> ${{ audFormat(remove_site_fee($price['ltc']->ask)) }} <br></span></span>
        <span class="column is-one-quarter-mobile"> ETH:<span class="highlight-blue"> ${{ audFormat(remove_site_fee($price['eth']->ask)) }} <br></span></span>
        <span class="column is-one-quarter-mobile"> XRP:<span class="highlight-blue"> ${{ audFormat(remove_site_fee($price['xrp']->ask)) }} <br></span></span>
        <span class="column is-one-quarter-mobile"> POWR:<span class="highlight-blue"> ${{ audFormat(remove_site_fee($price['pwr']->ask)) }} <br></span></span>
    </div>
</div>