<section class="start-trading" id="register-interested">
    <div class="inner">
        <h2>Register your free account!</h2>
        <p style="max-width: 640px; margin: auto; margin-top: 20px; margin-bottom: 30px;">myCryptoWallet is Australia and New Zealand's only CryptoCurrency marketplace which offers buying, selling and trading of Bitcoin, Ethereum, Litecoin and Ripple digital currencies.</p>
        <p class="more-info"><a class="btn btn-teal" href="{{ url()->to('register') }}">Register</a></p>
    </div>
</section>