<!-- Todo : Make sure the email subscription function works  -->
<section class="start-trading" id="register-interested">
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='BIRTHDAY';ftypes[3]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
      <div class="inner">
        <h3 class="title">Buy and sell digital currency</h3>
        <p class="copy"><span>myCryptoWallet</span> is the easiest and most trusted place to buy, sell and manage your digital currency.</p>
          {{--<form action="//mycryptowallet.us16.list-manage.com/subscribe/post?u=b97f75ce7d2ee266978e65b14&amp;id=be7ba5943b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>--}}
          {{--<div class="contact-module">--}}
            {{--<div class="input-icon"><img src="{{ url('img/email-icon.png') }}" /></div>--}}
            {{--<input type="email" value="" placeholder="Email Address" name="EMAIL" class="required email" id="mce-EMAIL">--}}
            {{--<div style="position: absolute; left: -5000px;" aria-hidden="true">--}}
                {{--<input type="text" name="b_b97f75ce7d2ee266978e65b14_be7ba5943b" tabindex="-1" value="">--}}
            {{--</div>--}}
          {{--</div>--}}
            {{--<div class="response" id="mce-error-response" style="display:none"></div>--}}
            {{--<div class="response" id="mce-success-response" style="display:none"></div>--}}
          {{--</form>--}}
          {{--<div class="submit-button">--}}
              {{--<input type="submit" value="GET STARTED" name="subscribe" id="mc-embedded-subscribe" class="btn btn-blue-fading">--}}
          {{--</div>--}}
        <p class="more-info" style="display: none;">Need more information? <a href="#">Learn more</a></p>
      </div>
</section>