@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>Marketplace</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <h4>Become an Investor in myCryptoWallet’s Live Marketplace</h4>
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>Come and invest in the ever-growing market of blockchain assets. myCryptoWallet is a digital currency service based in Australia and does money exchanges for both the Australian and New Zealand dollar.</p>
                <p>With our digital marketplace platform, controlling your digital wealth has never been easier. It’s complete with a vast resource of secure and reliable wallet services. myCryptoWallet lets users trade with all the latest high-value blockchain assets available: Bitcoins, Ethereum, Litecoins and Ripple. Users also have access to a 24/7 live marketplace accessible in real-time from your desktop, phone and other mobile devices.</p>
              </div>
              <div class="fifty">

              </div>
            </div>
            <br><br>
        </div>

        <div class="wrapper flex-wrap">
          <div class="flex-group align-normal">
            <div class="fifty">
              <p><strong>Access to Current Market Values</strong></p>
              <p>Our platform stays up-to-date with current market trends so our users can continue to make wise investments options as the market changes.</p>
            </div>
            <div class="fifty">
              <p><strong>Be in Control of Your Wealth</strong></p>
              <p>myCryptoWallet facilitates a peer-to-peer live marketplace with numerous wallet management services. It provides users ultimate control in managing all their blockchain assets.</p>
            </div>
            <div class="fifty">
              <p><strong>Secured & Insured</strong></p>
              <p>We’re current with all international standards and regulations and use the latest technology to offer peace of mind to our users. We’re also insured to protect against hacks, fraud and theft.</p>
            </div>
            <div class="fifty">
              <p><strong>Trade with Verified Investor</strong></p>
              <p>With each new myCryptoWallet user, we ensure they go through a rigorous, third-party electronic identity verification process before they can start trading with other investors.</p>
            </div>
            <div class="fifty">
              <p><strong>Free & Secure Digital Currency Wallet Creation</strong></p>
              <p>Creating your wallet is free and easy to do thanks to our immediate fiat deposit, withdrawal and fix-rate exchange services.</p>
            </div>
          </div>
          <br><br>
        </div>

        <div class="wrapper flex-wrap">
            <h4>Find Confidence in a Digital Marketplace You Can Trust</h4>
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>myCryptoWallet seeks to build a community of verified investors that find value and trust through a single cryptocurrency platform. We continue to advance our services and online marketplace to become a world leader in the digital market. </p>
              </div>
              <div class="fifty">
                <p>Offering the utmost in security and assurance, our support team is always standing by 24/7 to process claims and take care of all client concerns. We do all of this because we understand the importance of having a platform that shares in the concerns and investments of its users.</p>
              </div>
            </div>
        </div>
      
      </div>
    </div>
  </div>
</div>
@endsection
