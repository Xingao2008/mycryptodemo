@extends('layouts.app')

@section('custom_styles')
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/flags.min.css" rel=stylesheet type="text/css">
    <style>
        .my-divider {
            width: 1px;
            margin: 6px 0;
            background: grey;
        }
        .my-padding {
            padding: 2%;
        }
        .my-height {
            height: 200px;
        }
        h2 {
            font-size: 28px;
        }
        /* customize vuetify css*/
        .theme--light {
            background-color: transparent !important;
        }
        .v-text-field__slot,
        .v-menu__activator,
        .v-select__slot{
            height: auto;
        }
        .application--wrap {
            min-height: auto;
            margin-top:50px;
        }
        .inner-content {
            margin: 30px;
        }
        .v-stepper__header {
            overflow:hidden;
        }
        .v-card__title.v-card__title--primary,
	        .v-card__actions {
	            padding-left:0px;
	        }
	        /* v-cloak hide ugly loading messages */
	        [v-cloak] > * { display:none; }
	        [v-cloak]::before { content: "loading…" }

	        .register-wrapper {
	            max-width:800px;
	            margin:0 auto;
	        }
	        .register-wrapper .v-stepper--vertical .v-stepper__content {
	            margin:0px -25px -25px 33px;
	        }
	        .v-date-picker-table {
	            height: auto!important;
	            padding:0!important;
	        }
	        .v-stepper__items {
	            overflow:visible!important;
	        }
    </style>
@endsection

@section('content')
    <v-app v-cloak>
        <v-content>
            <div class="register-wrapper">
                <v-container>
                    <v-alert class="notification is-danger" v-model="general.alert" :type="alert.alertType">
                        @{{ alert.alertMessage }}
                    </v-alert>

                    <v-layout class="text-xs-center" v-show="general.firstpage">
                        <v-flex class="my-padding">
                            <v-container>
                                <v-card flat>
                                    <v-card-title primary-title>
                                        <h2 class="page-title-heading">Registration</h2>
                                    </v-card-title>
                                    <v-form class="my-height">
                                        <v-text-field
                                                v-model="registration.email"
                                                :error-messages="alert.errorMessages"
                                                label="Email"
                                                :rules="[ validEmail ]"
                                        ></v-text-field>
                                        <v-card-actions>
                                            <v-btn color="primary" large block @click.prevent="checkStep" :disabled="general.disabled">Return User <i v-if="general.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                                            <v-btn color="primary" large block @click.prevent="checkStep(1)">New User</v-btn>
                                        </v-card-actions>
                                    </v-form>
                                </v-card>
                            </v-container>
                        </v-flex>

                    </v-layout>

                    <v-stepper v-model="general.step" vertical v-show="general.secondpage">
                        <v-stepper-header>
                            <v-stepper-step step="1" :complete="general.step > 1"><span class="is-hidden-mobile">Information</span></v-stepper-step>
                            <v-divider></v-divider>
                            <v-stepper-step step="2" :complete="general.step > 2"><span class="is-hidden-mobile">Mobile Verification</span></v-stepper-step>
                            <v-divider></v-divider>
                            <v-stepper-step step="3" :complete="general.step > 3"><span class="is-hidden-mobile">Password</span></v-stepper-step>
                            <v-divider></v-divider>
                            <v-stepper-step step="4" :complete="general.step > 4"><span class="is-hidden-mobile">Complete profile</span></v-stepper-step>
                            <v-divider></v-divider>
                            <v-stepper-step step="5"><span class="is-hidden-mobile">Confirm Email</span></v-stepper-step>
                        </v-stepper-header>

                        <v-stepper-items>
                            <v-stepper-content step="1">

                                <v-text-field label="First Name" v-model="registration.firstname" :error-messages="alert.errorMessages"
                                            :rules="[ validFirstName ]"
                                            required></v-text-field>
                                <v-text-field label="Last Name" v-model="registration.lastname" :error-messages="alert.errorMessages"
                                            :rules="[ validLastName ]"
                                            required></v-text-field>
                                <v-text-field label="Email" v-model="registration.email" :error-messages="alert.errorMessages"
                                            :rules="[ validEmail ]"
                                            required></v-text-field>
                                <v-layout row class="text-xs-center">

                                    <v-flex d-flex xs9 sm9 md9><v-select label="Mobile Prefix" :items="mobilePrefix" v-model="registration.mobilePrefix" :error-messages="alert.errorMessages"
                                            :rules="[ validMobilePrefix ]"
                                            required>
                                        </v-select></v-flex>
                                    <v-flex d-flex xs3 sm3 md3><v-subheader><img src="/images/blank.gif" class="flag" v-bind:class="countryFlag" /></v-subheader></v-flex>

                                </v-layout>
                                <v-text-field label="Mobile" v-model="registration.mobile" :error-messages="alert.errorMessages"
                                            :rules="[ validMobile ]"
                                            required></v-text-field>

                                <v-btn color="primary" @click.prevent="register" :disabled="general.disabled">Next <i v-if="general.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                            </v-stepper-content>
                            <v-stepper-content step="2">

                                <v-layout row class="text-xs-center">
                                    <v-flex d-flex xs10 sm10 md10>
                                        <v-text-field label="Enter verification code" v-model="registration.verification" :error-messages="alert.errorMessages"
                                                    :rules="[ validVerification ]"
                                                    required></v-text-field>
                                    </v-flex>
                                    <v-flex d-flex xs2 sm2 md2>
                                        <div><v-btn color="primary" @click.prevent="resendSMS" :disabled="counter.disabled">Resend @{{ counter.countdownMsg }}</v-btn></div>
                                    </v-flex>
                                </v-layout>

                                <v-btn color="primary" @click.prevent="verifySMS" :disabled="general.disabled">Next <i v-if="general.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>

                            </v-stepper-content>
                            <v-stepper-content step="3">

                                <v-text-field
                                        v-model="registration.password"
                                        :append-icon="e2 ? 'visibility' : 'visibility_off'"
                                        :append-icon-cb="() => (e2 = !e2)"
                                        :type="e2 ? 'password' : 'text'"
                                        :error-messages="alert.errorMessages"
                                        hint="At least 8 characters"
                                        min="8"
                                        counter
                                        label="New Password"
                                        :rules="[ validPass ]"
                                        required
                                ></v-text-field>
                                <v-text-field
                                        v-model="registration.confirm_password"
                                        :append-icon="e3 ? 'visibility' : 'visibility_off'"
                                        :append-icon-cb="() => (e3 = !e3)"
                                        :type="e3 ? 'password' : 'text'"
                                        :error-messages="alert.errorMessages"
                                        hint="At least 8 characters"
                                        min="8"
                                        counter
                                        label="Confirm Password"
                                        :rules="[ validPass, matchPass ]"
                                        required
                                ></v-text-field>

                                <v-btn color="primary" @click.prevent="setPassword" :disabled="general.disabled">Next <i v-if="general.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>

                            </v-stepper-content>
                            <v-stepper-content step="4">

                                <v-menu
                                        ref="menu"
                                        :close-on-content-click="false"
                                        v-model="menu"
                                        :nudge-right="40"
                                        lazy
                                        transition="scale-transition"
                                        offset-y
                                        full-width
                                        min-width="290px"
                                >
                                    <v-text-field
                                            slot="activator"
                                            v-model="registration.dob"
                                            label="Birthday date"
                                            prepend-icon="event"
                                            readonly
                                            required
                                            :error-messages="alert.errorMessages"
                                            :rules = "[ over18 ]"
                                    ></v-text-field>
                                    <v-date-picker
                                            ref="picker"
                                            v-model="registration.dob"
                                            :max="new Date().toISOString().substr(0, 10)"
                                            min="1950-01-01"
                                            @change="saveDob"
                                    ></v-date-picker>
                                </v-menu>

                                <v-text-field label="Street" v-model="registration.street"></v-text-field>
                                <v-text-field label="City" v-model="registration.city"></v-text-field>
                                <v-text-field label="State" v-model="registration.state"></v-text-field>
                                <v-text-field label="Post Code" v-model="registration.postCode" :error-messages="alert.errorMessages" :rules="[ validPostCode ]"></v-text-field>

                                <v-layout row class="text-xs-center">
                                    <v-flex d-flex xs9 sm9 md9>
                                        <v-select label="Country" :items="country" item-text="text" item-value="value" v-model="registration.countryId"></v-select>
                                    </v-flex>
                                    <v-flex d-flex xs3 sm3 md3><v-subheader><img src="/images/blank.gif" class="flag" v-bind:class="countryFlag" /></v-subheader></v-flex>
                                </v-layout>

                                <v-btn color="primary" @click.prevent="submit" :disabled="general.disabled">Next <i v-if="general.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>

                            </v-stepper-content>
                            <v-stepper-content step="5">

                                <v-subheader>Congrats! Please Check your email to verify your account</v-subheader>

                            </v-stepper-content>
                        </v-stepper-items>
                    </v-stepper>

                </v-container>
            </div>
        </v-content>
    </v-app>
@endsection

@section('custom_scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.1.5/vuetify.min.js"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                menu: false,
                e1: true,
                e2: true,
                e3: true,
                general: {
                    step: 1, //start from 1
                    spinner: false,
                    firstpage: true,
                    secondpage: false,
                    alert: false,
                    disabled: false
                },
                counter: {
                    countdownMsg: '',
                    timer: 60,
                    counter: false,
                    interval: null,
                    disabled: false
                },
                alert: {
                    alertType: 'error',
                    alertMessage: 'There was a problem, please try again before go next step',
                    errorMessages: []
                },
                registration: {
                    firstname: '',
                    lastname: '',
                    email: '',
                    mobilePrefix: '',
                    mobile: '',
                    verification: '',

                    dob: '',
                    street: '',
                    city: '',
                    state: '',
                    postCode: '',
                    countryId: '',

                    password: '',
                    confirm_password: ''
                },
                mobilePrefix: [
                    {text: 'Australia (61)', value: '61', iso: 'au'},
                    {text: 'New Zealand (64)', value: '64', iso: 'nz'},
                    {text: 'Unit States (1)', value: '1', iso: 'us'}
                ],
                country: [
                    {text: 'Australia', value: '36', iso: 'au'},
                    {text: 'New Zealand', value: '554', iso: 'nz'},
                    {text: 'USA', value: '840', iso: 'us'}
                ],
                countryFlag: ''
            },

            methods: {
                matchPass() {
                    var n = this.registration.password.localeCompare(this.registration.confirm_password)
                    this.alert.errorMessages = []
                    if ( n === 0 ) {
                        return true
                    } else {
                        return false || 'Password needs to be match'
                    }
                },

                setPassword() {
                    let vm = this
                    vm.general.spinner = true
                    if ( vm.matchPass() && vm.validPass(vm.registration.password) === true ) {
                        axios.post('/register/new_password',
                            {
                                email: vm.registration.email,
                                password: vm.registration.password,
                                confirm_password: vm.registration.confirm_password

                            }).then(function(response) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            if ( response.data.state == 200) {
                                vm.alert.alertType = 'success'
                                vm.alert.alertMessage = response.data.msg
                                vm.general.step++
                            } else {
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = response.data.msg
                            }
                        }).catch(function(error) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = error.data.msg
                        })
                    } else {
                        vm.general.alert = true
                        vm.general.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = 'Fail to update password'
                    }
                },

                register() {
                    let vm = this
                    vm.general.spinner = true
                    if (
                        vm.validFirstName(vm.registration.firstname) === true &&
                        vm.validLastName(vm.registration.lastname) === true &&
                        vm.validEmail(vm.registration.email) === true &&
                        vm.validMobilePrefix(vm.registration.mobilePrefix) === true &&
                        vm.validMobile(vm.registration.mobile) === true
                    ) {
                        axios.post('/register',
                            {
                                first_name: vm.registration.firstname,
                                last_name: vm.registration.lastname,
                                email: vm.registration.email,
                                phone_prefix: vm.registration.mobilePrefix,
                                phone: vm.registration.mobile

                            }).then(function(response) {
                                vm.general.alert = true
                                vm.general.spinner = false
                            if ( response.data.state == 200) {
                                vm.alert.alertType = 'success'
                                vm.alert.alertMessage = response.data.msg
                                vm.general.step++
                            } else {
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = response.data.msg
                            }
                        }).catch(function(error) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = error.data.msg
                        })
                    } else {
                        vm.general.alert = true
                        vm.general.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = 'There was a problem, please try again before go next step'
                    }
                },

                resendSMS() {
                    let vm = this
                    vm.counter.disabled = true
                    vm.counter.interval = setInterval(vm.countDown, 1000)
                    axios.post('/register/resend-msg',
                        {
                            email: vm.registration.email
                        }).then(function(response) {
                        if ( response.data.state == 200) {
                            vm.general.alert = true
                            vm.alert.alertType = 'success'
                            vm.alert.alertMessage = response.data.msg
                        } else {
                            vm.general.alert = true
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = response.data.msg
                        }
                    }).catch(function(error) {
                        vm.general.alert = true
                        vm.general.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = error.data.msg
                    })
                },

                countDown() {
                    let vm = this
                    let n = vm.counter.timer
                    if (!vm.counter.counter) {
                        vm.counter.counter = true
                    } else if (n > 0) {
                        n = n - 1
                        vm.counter.timer = n
                        vm.counter.countdownMsg = n + ' sec'
                    } else {
                        clearInterval(vm.counter.interval)
                        vm.counter.counter = false
                        vm.counter.countdownMsg = ''
                        vm.counter.interval = null
                        vm.counter.timer = 60
                        vm.counter.disabled = false
                    }
                },

                verifySMS() {
                    let vm = this
                    vm.general.spinner = true
                    if ( vm.validVerification(vm.registration.verification) === true ) {
                        axios.post('/register/mobile/verify',
                            {
                                code: vm.registration.verification,
                                email: vm.registration.email

                            }).then(function(response) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            if ( response.data.state == 200) {
                                vm.alert.alertType = 'success'
                                vm.alert.alertMessage = response.data.msg
                                vm.general.step++
                            } else {
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = response.data.msg
                            }
                        }).catch(function(error) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = error.data.msg
                        })
                    } else {
                        vm.general.alert = true
                        vm.general.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = 'There was a problem, please try again before go next step'
                    }

                },

                submit() {
                    let vm = this
                    vm.general.spinner = true
                    if ( vm.validPostCode(vm.registration.postCode) === true && vm.over18(vm.registration.dob) ===  true ) {
                        axios.post('/register/addProfile',
                            {
                                birthday: vm.registration.dob,
                                street: vm.registration.street,
                                city: vm.registration.city,
                                state: vm.registration.state,
                                postCode: vm.registration.postCode,
                                countryId: vm.registration.countryId,
                                email: vm.registration.email

                            }).then(function (response) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            if ( response.data.state == 200) {
                                vm.alert.alertType = 'success'
                                vm.alert.alertMessage = response.data.msg
                                vm.general.step++
                            } else {
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = response.data.msg
                            }
                        }).catch(function (error) {
                            vm.general.alert = true
                            vm.general.spinner = false
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = error.data.msg
                        })
                    } else {
                        vm.general.alert = true
                        vm.general.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = 'There was a problem, please try again before go next step'
                    }
                },

                checkStep(step) {
                    let vm = this
                    vm.general.spinner = true
                    vm.general.alert= false

                    if (step === 1) {
                        vm.general.step = 1;
                        vm.general.spinner = false
                        vm.general.secondpage = true
                        vm.general.firstpage = false
                    } else {
                        if (vm.validEmail(vm.registration.email) === true) {
                            axios.post('/register/get-step', {
                                email: vm.registration.email
                            }).then(function(response) {
                                vm.general.alert = true
                                vm.general.spinner = false
                                if (response.data.state === 200) {
                                    if (response.data.step === 'mobile') vm.general.step = 2
                                    if (response.data.step === 'details') vm.general.step = 3
                                    if (response.data.step === 'verify') vm.general.step = 4

                                    if (response.data.step === 'finish') {
                                        vm.general.step = 5
                                        vm.alert.alertMessage = 'You have already complete the registration'
                                    } else {
                                        vm.alert.alertMessage = 'Welcome back, please finish the rest of registration'
                                    }
                                }
                                vm.general.secondpage = true
                                vm.general.firstpage = false
                                vm.alert.alertType = 'success'
                            }).catch(function(error) {
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = error.data.msg
                            })
                        } else {
                            vm.general.alert = true
                            vm.general.spinner = false
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = 'There was a problem, please try again before go next step'
                        }
                    }
                },

                over18(birthday) {
                    this.alert.errorMessages = []
                    var ageDifMs = Date.now() - Date.parse( birthday );
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    var age = Math.abs(ageDate.getUTCFullYear() - 1970);
                    if (age < 18) {
                        return false || 'You have to be over 18 years old'
                    } else {
                        return true
                    }
                },

                validPass(pass) {
                    var re = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,32}$/
                    this.alert.errorMessages = []
                    if (!re.test(pass) || pass === '') {
                        return false || 'Password needs to contain minimum 8 characters, one number, one special character, one low or upper case'
                    } else {
                        return true
                    }
                },

                saveDob (date) {
                    this.$refs.menu.save(date)
                },

                validFirstName(firstname) {
                    this.alert.errorMessages = []
                    if (firstname === '') {
                        return false || 'This field is required'
                    } else {
                        return true
                    }
                },

                validLastName(lastname) {
                    this.alert.errorMessages = []
                    if (lastname === '') {
                        return false || 'This field is required'
                    } else {
                        return true
                    }
                },

                validEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    this.alert.errorMessages = []
                    if (!re.test(email) || email === '') {
                        return false || 'Email is not valid'
                    } else {
                        return true
                    }

                },

                validMobilePrefix(prefix) {
                    this.alert.errorMessages = []
                    if (prefix === '') {
                        return false || 'This field is required'
                    } else {
                        return true
                    }
                },

                validMobile(phone) {
                    var phone = phone.replace(/\s/g,'')  // strip all white space
                    var phone = phone.replace(/\+/g,'')  // strip all +
                    var phone = phone.replace(/\-/g,'')  // strip all -
                    var re= /^\d{9}(?:\d{1})?(?:\d{1})?$/
                    this.alert.errorMessages = []

                    if (!re.test(phone) || phone === '') {
                        return false || 'Mobile number is not valid'
                    } else {
                        return true
                    }
                },

                validVerification(verify) {
                    var re = /^\d{6}$/
                    this.alert.errorMessages = []
                    if (!re.test(verify) || verify === '') {
                        return false || 'Verification code is not valid'
                    } else {
                        return true
                    }
                },

                validPostCode(postcode) {
                    var re = /^(\s*|\d+)$/
                    this.alert.errorMessages = []
                    if (!re.test(postcode)) {
                        return false || 'Post code should only contain digits'
                    } else {
                        return true
                    }
                }
            },

            watch: {
                menu: function(val) {
                    val && this.$nextTick(() => (this.$refs.picker.activePicker = 'YEAR'))
                },

                'general.spinner': function(val) {
                    if (val)
                        this.general.disabled = true
                    else
                        this.general.disabled = false
                },

                'registration.mobilePrefix': function(val) {
                    if (val) {
                        for (i=0; i<this.mobilePrefix.length; i++) {
                            if (this.mobilePrefix[i].value == this.registration.mobilePrefix) {
                                this.countryFlag = 'flag-'+this.mobilePrefix[i].iso
                            }
                        }
                    }
                },

                'registration.countryId': function(val) {
                    if (val) {
                        for (i=0; i<this.country.length; i++) {
                            if (this.country[i].value == this.registration.countryId) {
                                this.countryFlag = 'flag-'+this.country[i].iso
                            }
                        }
                    }
                }
            },
        })
    </script>
@endsection