@extends('layouts.app')

@section('content')
<div class="main">
  <div class="page-heading">
    <h2>News</h2>
  </div>
</div>
<div class="container dashboard sub-page">
  <div class="main">
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal news-list">
            @foreach($posts as $post)
              <a href="/news/{{ $post->slug }}" class="single-news-item">
                <div class="featured-image" style="background-image: url('{{ url('') }}/storage/{{ $post->featured_image }}')"><div class="overlay"></div></div>
                <div class="bottom">
                  <div class="title">{{ $post->title }}</div>
                  <div class="btn btn-grey-full" href="/news/{{ $post->slug }}">Read more</div>
                </div>
              </a>
            @endforeach
            <div class="spacer"></div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
