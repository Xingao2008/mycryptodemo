@extends('layouts.app')

@section('content')

  <div class="main">
    <!-- <section class="home-top">
      <div class="overlay"></div>
      <div class="inner">
        <div class="fifty"><img src="img/mobile.png"></div>
        <div class="fifty">
          <div class="c1">
            <div class="c2">
              <div class="c3">
                <div class="wrapper">
                  <h2>Australia's Bitcoin, Ethereum, Litecoin, Ripple & PowerLedger exchange has launched. Free AUD Withdrawals & Deposits</h2>
                  <p>
                  myCryptoWallet is an Australian-based CryptoCurrency marketplace which offers instant buying, selling and trading of Bitcoin, Ethereum, Litecoin, Ripple & PowerLedger digital currencies, with 2.8% fees</p>
                  <a href="/register" class="btn btn-teal">Get started</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <a href="#benefits" class="go-down"><img src="img/arrows.svg"></a>
    </section> -->
    @include ('public.partials.home-hero')
    @include ('public.partials.home-eftpos')

    <!-- TODO: Remove the below section after the new graph ui is done -->
    <!-- <section class="instant-access">
      <div class="inner">
        <div class="fourty">
          <div class="c1">
            <div class="c2">
              <div class="c3">
                <h2>Instant access and <br>comparison to all <br>markets</h2>
                <p>myCryptoWallet is Australia's only CryptoCurrency marketplace which offers buying, selling and trading of Bitcoin, Ethereum, Litecoin, Ripple and PowerLedger digital currencies.</p>
                <a href="/about/marketplace" class="btn btn-teal">Read more</a>
              </div>
            </div>
          </div>
        </div>
        <div class="fifty home-graph">
                <div class="market-prices">
                    <div class="header">
                        <div class="coin-toggle">
                            <div class="single-toggle active" data-toggle="bitcoin"><div class="icon icon-btc"></div>Bitcoin</div>
                            <div class="single-toggle" data-toggle="litecoin"><div class="icon icon-ltc"></div>Litecoin</div>
                            <div class="single-toggle" data-toggle="ethereum"><div class="icon icon-eth"></div>Ethereum</div>
                            <div class="single-toggle" data-toggle="ripple"><div class="icon icon-rpl"></div>Ripple</div>
                            <div class="single-toggle" data-toggle="power"><div class="icon icon-pwr"></div>PowerLedger</div>
                        </div>
                        <div class="single-benefit">
                            <div class="twenty"><img src="img/computing-cloud.svg"></div>
                            <div class="eighty">
                                <h4>Available anytime, anywhere</h4>
                                <p>myCryptoWallet is here to help. Our services allow you to access your digital assets
                                    to send and receive them across the globe in real time</p>
                            </div>
                        </div>
                        <div class="single-benefit">
                            <div class="twenty"><img src="img/exchange.svg"></div>
                            <div class="eighty">
                                <h4>Instant Exchange</h4>
                                <p>Instantly Buy & Sell cryptocurrencies through our website exchange service</p>
                            </div>
                        </div>
                        <div class="single-benefit">
                            <div class="twenty"><img src="img/maze.svg"></div>
                            <div class="eighty">
                                <h4>Marketplace</h4>
                                <p>Start trading on our live marketplace and place your trades against other verified
                                    users</p>
                            </div>
                        </div>
                        <div class="single-benefit">
                            <div class="twenty"><img src="img/shield.svg"></div>
                            <div class="eighty">
                                <h4>Safe & secure</h4>
                                <p>All wallet storage is 100% insured by us with our off server cold wallet storage</p>
                            </div>
                        </div>
                        <div class="single-benefit">
                            <div class="twenty"><img src="img/certificate.svg"></div>
                            <div class="eighty">
                                <h4>100% certified</h4>
                                <p>Australian owned, 24/7 Support</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="steps">
                <div class="inner">
                    <h2>Getting verified is easy</h2>
                    <p>Unlike other markets and vendors, we have a large, friendly, responsive support team who are
                        excited to help you on your Crypto journey</p>
                    <img src="img/steps.png" class="steps-desktop">
                    <img src="img/steps-tall.png" class="steps-mobile">
                </div>
            </section>
            <section class="instant-access">
                <div class="inner">
                    <div class="fourty">
                        <div class="c1">
                            <div class="c2">
                                <div class="c3">
                                    <h2>Instant access and <br>comparison to all <br>markets</h2>
                                    <p>myCryptoWallet is Australia's only CryptoCurrency marketplace which offers
                                        buying, selling and trading of Bitcoin, Ethereum, Litecoin, Ripple and
                                        PowerLedger digital currencies.</p>
                                    <a href="/about/marketplace" class="btn btn-teal">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fifty home-graph">
                        <div class="market-prices">
                            <div class="header">
                                <div class="coin-toggle">
                                    <div class="single-toggle active" data-toggle="bitcoin">
                                        <div class="icon icon-btc"></div>
                                        Bitcoin
                                    </div>
                                    <div class="single-toggle" data-toggle="litecoin">
                                        <div class="icon icon-ltc"></div>
                                        Litecoin
                                    </div>
                                    <div class="single-toggle" data-toggle="ethereum">
                                        <div class="icon icon-eth"></div>
                                        Ethereum
                                    </div>
                                    <div class="single-toggle" data-toggle="ripple">
                                        <div class="icon icon-rpl"></div>
                                        Ripple
                                    </div>
                                    <div class="single-toggle" data-toggle="power">
                                        <div class="icon icon-pwr"></div>
                                        PowerLedger
                                    </div>
                                </div>
                            </div>
                            <div class="graph">
                                <div id="">
                                    <div id="chartContainer-btc" class="single-graph bitcoin active"
                                         style="height: 400px; width: 100%;"></div>
                                    <div id="chartContainer-ltc" class="single-graph litecoin"
                                         style="height: 400px; width: 100%;"></div>
                                    <div id="chartContainer-eth" class="single-graph ethereum"
                                         style="height: 400px; width: 100%;"></div>
                                    <div id="chartContainer-xrp" class="single-graph ripple"
                                         style="height: 400px; width: 100%;"></div>
                                    <div id="chartContainer-pwr" class="single-graph power"
                                         style="height: 400px; width: 100%;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            <section class="start-trading" id="register-interested">
                <div class="inner">
                    <h2>Sign up to our newsletter!</h2>
                    <p>Keep in the loop and enter your email below:</p>
                    <form action="//mycryptowallet.us16.list-manage.com/subscribe/post?u=b97f75ce7d2ee266978e65b14&amp;id=be7ba5943b"
                          method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                          class="validate" target="_blank" novalidate>
                        <div class="contact-module">
                            <input type="email" value="" placeholder="Enter your email address" name="EMAIL"
                                   class="required email" id="mce-EMAIL">
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                                      name="b_b97f75ce7d2ee266978e65b14_be7ba5943b"
                                                                                                      tabindex="-1"
                                                                                                      value=""></div>
                            <div class="submit-button">
                                <input type="submit" value="Start trading" name="subscribe" id="mc-embedded-subscribe"
                                       class="btn btn-teal">
                            </div>
                        </div>
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                        <script type='text/javascript'
                                src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
                        <script type='text/javascript'>(function ($) {
                                window.fnames = new Array();
                                window.ftypes = new Array();
                                fnames[0] = 'EMAIL';
                                ftypes[0] = 'email';
                                fnames[1] = 'FNAME';
                                ftypes[1] = 'text';
                                fnames[2] = 'LNAME';
                                ftypes[2] = 'text';
                                fnames[3] = 'BIRTHDAY';
                                ftypes[3] = 'birthday';
                            }(jQuery));
                            var $mcj = jQuery.noConflict(true);</script>
                    </form>
                    <p class="more-info" style="display: none;">Need more information? <a href="#">Learn more</a></p>
                </div>
            </div>
      </div>
    </section> -->

    <!-- TODO: Remove the code after the new email subscribe works -->
    <!-- <section class="start-trading" id="register-interested">
      <div class="inner">
        <h2>Sign up to our newsletter!</h2>
        <p>Keep in the loop and enter your email below:</p>
          <form action="//mycryptowallet.us16.list-manage.com/subscribe/post?u=b97f75ce7d2ee266978e65b14&amp;id=be7ba5943b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div class="contact-module">
            <input type="email" value="" placeholder="Enter your email address" name="EMAIL" class="required email" id="mce-EMAIL">
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b97f75ce7d2ee266978e65b14_be7ba5943b" tabindex="-1" value=""></div>
            <div class="submit-button">
              <input type="submit" value="Start trading" name="subscribe" id="mc-embedded-subscribe" class="btn btn-teal">
            </div>
          </div>
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='BIRTHDAY';ftypes[3]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
          </form>
        <p class="more-info" style="display: none;">Need more information? <a href="#">Learn more</a></p>
      </div>
    </section> -->
  </div>
</div>

@endsection