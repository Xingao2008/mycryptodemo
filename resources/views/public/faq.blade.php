@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>Frequently Asked Questions</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="faq-page">
          <div class="wrapper flex-wrap faq-list">
            <p style="font-weight:bold;">Please note, our FAQ page has moved here: <a href="https://mycryptowallet.freshdesk.com/support/solutions">https://mycryptowallet.freshdesk.com/support/solutions</a></p>
            <br>
            <div class="single-faq">
              <div class="title active">What is myCryptoWallet?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>myCryptoWallet is a virtual marketplace based in Australia for the global exchange of blockchain assets between buyers and sellers. Through our platform, you’ll have access to trading with other users and can set the price in which you buy or sell assets. Read more about us on our about page <a href="/about">here</a>.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">What is cryptocurrency?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>Cryptocurrency is a digital asset like Bitcoin, Litecoin, Ethereum and Ripple. They’re designed as a virtual exchange medium for digital marketplace investments and trades.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">Does myCryptoWallet collaborate with other exchanges?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>No, we do not. Our marketplace is for our verified users only to guarantee safe and secure trades through a single system. All blockchain assets are sourced by deposits from our users.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">Can I use this site for illegal activities or the paying of ransomware?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>No. Both activities are not only forbidden by our terms of service, but will result in immediate termination of your account. Our platform abides by current international AML/CTF and KYC laws and will never tolerate any such activities.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">How do I buy cryptocurrency?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>Once you have registered and been verified, you will be able to create your very own Crypto Wallet. From there, you should be able to select from an array of deposit options to exchange either AUD or NZD for any of our four digital assets. Then simply click on “Buy/Sell” and enter the price you’re willing to pay for the currency of choice. Or, select a “Market Order” to make fast and immediate purchases.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">How do I sell cryptocurrency?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>It’s the same as buying except you place a sale order for your specific volume and price. However, the quickest way to do this is to select the market order option.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">What is a Market Order?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>Market orders are a means of buying or selling digital assets immediately at the best available price. These prices can be found in the order book at any time.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">What is a Limit Order?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>A limit order is the opposite of a market order. Instead of letting the order book decide your buying or selling rate, you do. Then the order gets listed on the order book until someone matches it.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">What is the Order Book?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>An order book lists all the available sell and buy orders on the market. Use this to buy and sell digital assets instantly at those prices.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">Can trades be cancelled?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>Yes, you can cancel an order before it’s matched any time by selecting the “Buy/Sell” option and then “Open Orders.” You’re even able to cancel partially matched orders.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">How does my account get verified?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>All newly registered accounts must be verified before they can buy or sell with other verified users. Once you register, you’ll be asked to verify your account through our designated third party service. There, you’ll have to provide a passport and driver’s license and verification may take up to a few days to complete. This is a mandatory security measurement that we require of all our users.</p>
                </div>
              </div>
            </div>
            <div class="single-faq">
              <div class="title">What are the limits on Deposits and Withdrawals?</div>
              <div class="content-faq">
                <div class="inner">
                  <p>The minimum purchase is $50, with a maximum of $1,000 per day. There are no limits for deposits, or withdrawals.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="sidebar">
            <div class="single-contact-module">
            <p>Looking for support? We're here to help, 24/7. Contact <a href="/support">support</a>.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
