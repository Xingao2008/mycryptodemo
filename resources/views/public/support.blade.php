@extends('layouts.app')

@section('content')
<div class="sub-page-banner" style="background-image: url('https://images.pexels.com/photos/210607/pexels-photo-210607.jpeg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb');">
  <div class="overlay"></div>
  <div class="inner">
    <h2>Support</h2>
  </div>
</div>
<div class="container dashboard sub-page">
  <div class="main">
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>We thank you for trusting our <a href="https://mycryptowallet.freshdesk.com/support/home" target="_blank">24/7 support team</a> to help you. We’re standing by to provide you the fasted means to solving any issues or concerns you may be experiencing.</p>
              </div>
              <div class="fifty">
                <div class="single-support-module">
                  <p>Please refer to our <a href="https://mycryptowallet.freshdesk.com/support/solutions">FAQ</a> first for resources that may alleviate common troubleshooting issues and problems you may be experiencing. If you cannot find a solution to your issue in our database, then proceed to contacting our customer service and technical support team. They are available 24/7 for your convenience.</p>
                </div>
                <div class="single-support-module">
                  <p>For contacting <a href="https://mycryptowallet.freshdesk.com/support/home" target="_blank">customer support</a>, please fill out the form with as much detail as you can. Then we’ll immediate go to work and be in contact with a solution as soon as we can.</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('public.partials.start-trading')

@endsection
