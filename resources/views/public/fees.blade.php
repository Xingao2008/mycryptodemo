@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>Fees</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>myCryptoWallet provides a completely free digital wallet for Bitcoin, Litecoin, Ethereum & Ripple. Only the blockchain miner fee applies when making an external withdraw, this amount is shown on withdraw request.</p>
                <br>
                <div class="fees-content">
                  <div class="fees-table">
                    <p class="sub-title">Free Fiat Deposits & Withdraws</p>
                    <ul>
                      <li><p>We do not take any fees for AUD & NZD Fiat transfers for Deposits/Withdraws </p></li>
                      <li><p>Use POLI Payments $3.25 inc GST fee</p></li>
                    </ul>
                  </div>
                  <br>
                  <div class="fees-table">
                    <p class="sub-title">EFT Bank Transfer Free</p>
                    <ul>
                      <li><p>Buy/Sell Exchange: 2.8 % Fee applies</p></li>
                    </ul>
                  </div>
                  <br>
                  <div class="fees-table">
                    <p class="sub-title">Marketplace:</p>
                    <ul>
                      <li><p>0.7% Maker/Taker fee >$5000 30 day Balance</p></li>
                      <li><p>0.6% Maker/Taker fee >$5001 - $25,000 30 day Balance</p></li>
                      <li><p>0.5% Maker/Taker fee >$25,001 - $50,000 30 day balance</p></li>
                      <li><p>0.4% Maker/Taker fee >$50,001 - $100,000</p></li>
                      <li><p>0.3% 100k+</p></li>
                    </ul>
                  </div>
                  <br>
                  <div class="fees-table">
                    <p class="sub-title">Any 2 Any CryptoSwap:</p>
                    <ul>
                      <li><p>Swap between Bitcoin - Ethereum- Litecoin - Ripple in any combination.</p></li>
                      <li><p>3.2% fee applies</p></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="fifty is-hidden-mobile">
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
