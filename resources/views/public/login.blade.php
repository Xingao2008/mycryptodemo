@extends('layouts.app')
@section('custom_styles')
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .my-divider {
            width: 1px;
            margin: 6px 0;
            background: grey;
        }
        .my-padding {
            padding: 2%;
        }
        .my-height {
            height: 200px;
        }
        h2 {
            font-size: 28px;
        }
        /* customize vuetify css*/
        .theme--light {
            background-color: transparent !important;
        }
        .v-text-field__slot,
        .v-menu__activator,
        .v-select__slot{
            height: auto;
        }
        .application--wrap {
            min-height: auto;
            margin-top:50px;
        }
        .inner-content {
            margin: 30px;
        }
        .v-card__title.v-card__title--primary,
        .v-card__actions {
            padding-left:0px;
        }
        [v-cloak] > * { display:none; }
        [v-cloak]::before { content: "loading…" }

        .login-wrapper {
            max-width:800px;
            margin:0 auto;
        }
    </style>
@endsection

@section('content')
    <v-app v-cloak>
        <v-content>
            <div class="login-wrapper">
                <v-container>
                    <v-layout class="text-xs-center">

                        <v-flex class="my-padding column">
                            <v-container>
                                <v-alert class="notification is-danger" v-model="general.alert" :type="alert.alertType">
                                    @{{ alert.alertMessage }}
                                </v-alert>
                                <v-card flat>
                                    <v-card-title primary-title>
                                        <h2 class="page-title-heading">@{{ general.title }}</h2>
                                    </v-card-title>
                                    <v-form class="my-height" v-if="general.form1">
                                        <v-text-field label="Email" :error-messages="alert.errorMessages" :rules="[ validEmail ]" v-model="login.email"></v-text-field>
                                        <v-text-field
                                                v-model="login.password"
                                                :append-icon="e1 ? 'visibility' : 'visibility_off'"
                                                :append-icon-cb="() => (e1 = !e1)"
                                                :type="e1 ? 'password' : 'text'"
                                                :error-messages="alert.errorMessages"
                                                hint="At least 6 characters"
                                                min="6"
                                                counter
                                                label="Password"
                                                :rules="[ validPass ]"
                                        ></v-text-field>
                                        <v-card-actions>
                                            <v-btn color="primary" large block @click.prevent="signin" :disabled="login.disabled">Login <i v-if="login.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                                            <v-btn flat large color="orange" @click.prevent="resetPass">Forget password?</v-btn>
                                        </v-card-actions>
                                    </v-form>

                                    <v-form class="my-height" v-if="general.form2">
                                        <v-text-field label="Two-Factor Auth Code" :error-messages="alert.errorMessages" v-model="login.tfa"></v-text-field>
                                        <v-card-actions>
                                            <v-btn color="primary" large block @click.prevent="signin" :disabled="login.disabled">Enter <i v-if="login.spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                                        </v-card-actions>
                                    </v-form>

                                </v-card>
                            </v-container>
                        </v-flex>
                    </v-layout>
                </v-container>
            </div>
        </v-content>
    </v-app>
@endsection

@section('custom_scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.1.5/vuetify.min.js"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                e1: true,
                general: {
                    title: 'login',
                    alert: false,
                    form1: true,
                    form2: false
                },
                counter: {
                    countdownMsg: '',
                    timer: 60,
                    counter: false,
                    interval: null,
                    disabled: false
                },
                alert: {
                    alertType: 'error',
                    alertMessage: 'There was a problem, please try again before go next step.',
                    errorMessages: []
                },
                login: {
                    spinner: false,
                    email: '{{ session('loginEmail') }}',
                    password: '',
                    disabled: false,
                    token: 0,
                    uid: 0,
                    tfa: '',
                    tfa_status: 0,
                }
            },

            methods: {
                resetPass() {
                    window.location.replace("/password/reset");
                },

                signin() {
                    let vm = this
                    vm.login.spinner = true
                    vm.general.alert= false

                    axios.post('/login/do-login', {
                        email: vm.login.email,
                        password: vm.login.password,
                        tfa: vm.login.tfa
                    }).then(function(response) {
                        vm.login.spinner = false

                        if (response.data.state === 200) {
                            if (response.data.step === 'finish') {

                                vm.login.token = response.data.token
                                vm.login.uid = response.data.uid
                                vm.login.tfa_status = response.data.tfa_status

                                Vue.cookie.remove('api_token')
                                Vue.cookie.set('api_token', vm.login.token, 1)
                                Vue.cookie.remove('uid')
                                Vue.cookie.set('uid',  vm.login.uid, 1)

                                vm.general.alert = true
                                vm.alert.alertType = 'success'
                                vm.alert.alertMessage = response.data.msg
                                vm.goDashboard()

                            } else if (response.data.state === 303) {
                                console.log('send verification email')
                            } else {
                                vm.general.alert = true
                                vm.alert.alertType = 'error'
                                vm.alert.alertMessage = 'Registration haven\'t been done, Please finish the rest of registration'
                            }

                        } else if (response.data.state == 403) {
                            vm.general.title = 'Please enter TFA code'
                            vm.general.form1 = false
                            vm.general.form2 = true
                        } else {
                            vm.general.alert = true
                            vm.alert.alertType = 'error'
                            vm.alert.alertMessage = response.data.msg
                        }

                    }).catch(function (error) {
                        vm.general.alert = true
                        vm.login.spinner = false
                        vm.alert.alertType = 'error'
                        vm.alert.alertMessage = error.data.msg
                    })
                },

                validPass(pass) {
                    var re = /^\S{6,}$/
                    this.alert.errorMessages = []
                    if (!re.test(pass) || pass === '') {
                        return false || 'Password need to contain minimum 6 characters'
                    } else {
                        return true
                    }
                },

                validEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    this.alert.errorMessages = []
                    if (!re.test(email) || email === '') {
                        return false || 'Email is not valid'
                    } else {
                        return true
                    }
                },

                goDashboard() {
                    window.location = "{{ url()->to('dashboard') }}"
                },

                goTfa() {
                    window.location = "{{ url()->to('account/tfa') }}"
                }
            },

            watch: {
                'login.spinner': function(val) {
                    if (val)
                        this.login.disabled = true
                    else
                        this.login.disabled = false
                }
            },
        })
    </script>
@endsection