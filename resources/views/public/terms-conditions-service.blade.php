@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="back"><a href="/legal">< Back</a></div>
    <div class="page-heading"><h2>Terms and Conditions of Service of myCryptoWallet</h2><p>The Agreement was last updated on 27/10/2017.</p></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content" style="justify-content: flex-start;">
                <div class="content">
                  <p><strong>INTRODUCTION</strong></p>

                  <p class="sind"><strong>1.</strong> Thank you for engaging our services on Mycryptowallet (“the Company”) at ‘www.mycryptowallet.com.au’ (“Website”) owned and operated by Mycryptowallet located at PO BOX 2250, VIC 3429 Australia.</p>

                  <p class="sind"><strong>2.</strong> By engaging our services and/or clicking to accept this Agreement or using or accessing any of the Company or related services, you agree to all the terms and conditions of the Agreement.</p>

                  <p class="sind"><strong>3.</strong> If you are using or ordering the Company’s service(s), related service(s) and/or purchasing a services on behalf of a Company or other entity, then “Customer” or “you” means that entity, and you are binding that entity to the Agreement. You represent and warrant that you have the legal power and authority to enter into the Agreement and that, if the Customer is an entity, the Agreement is entered into by an employee or agent with all necessary authority to bind that entity to the Agreement.</p>

                  <p><strong>GENERAL</strong></p>

                  <p class="sind"><strong>4.</strong> The terms and conditions of the Agreement (together with any other terms and conditions agreed in writing between the Company and the Client from time to time) constitute the entire agreement between the parties and supersede any previous agreement(s) or understanding(s) and may not be varied except with notice from the Company. </p>

                  <p class="sind"><strong>5.</strong> No failure or delay by the Company in exercising any of its rights under the Agreement shall be deemed to be a waiver of that right, and no waiver by the Company of any breach of the Contract by the Client shall be considered as a waiver of any subsequent breach of the same or any other provision.</p>

                  <p><strong>DEFINITION AND INTERPRETATION</strong></p>

                  <p class="sind"><strong>6.</strong> The following words used herein have the following definitions and meanings :-</p>

                  <p class="sind"><strong>6.1.</strong> ‘Authorised Users’ refers to the Client’s employees, agents, contractor, third parties, staffs or any entity that is duly authorised to act on behalf of the Client.</p>

                  <p class="sind"><strong>6.2.</strong> ‘Client’ refers to you as the receiver of the Services and will also include inter alia, to your employees, agents, contractor, third parties, staffs or any entity that is duly authorised to act on behalf of you. </p>

                  <p class="sind"><strong>6.3.</strong> ‘Parties’ collectively refers to ‘the Company’ and its employees, agents, contractor, third parties, staffs or any entity that is duly authorised to act on behalf of the Company for the carrying out of the Services as the Service Provider and ‘You’ as the Client and its authorised agents, contractors, employees or any entity duly authorised for and on behalf of you. </p>

                  <p class="sind"><strong>6.4.</strong> ‘Services’ refers to the services to be provided by the Company from time to time as displayed on the Website (as the case may be) (subject to change). </p>

                  <p class="sind"><strong>6.5.</strong> ‘Service Provider’ refers to the Company and/or its employees, agent, contractor, third parties, staff or any entity that is duly authorised to act on behalf of the Company for the carrying out of the Services. </p>

                  <p class="sind"><strong>6.6.</strong> ‘We, Us or Our’ refers to the Company and its employees, agents, contractor, third parties, staffs or any entity that is duly authorised to act on behalf of the Company for the carrying out of the Services.</p>

                  <p class="sind"><strong>6.7.</strong> ‘You or your’ refers to the Client.</p>

                  <p><strong>THE SERVICES</strong></p>

                  <p class="sind"><strong>7.</strong> General terms of the Services</p>

                  <p class="sind"><strong>7.1.</strong> The Company shall provide the Services to the Client subject to the terms and conditions contained herein or any other reference to documents referred to by the Company to the Client or such other terms and conditions as may be agreed in writing between the Company and the Client.</p>

                  <p class="sind"><strong>7.2.</strong> The Company shall provide to the Client all Services as specified on the Company’s website. </p>

                  <p class="sind"><strong>8.</strong> Responsibilities, Obligations and Due Diligence</p>

                  <p class="sind"><strong>8.1.</strong> At the absolute discretion of the Company, it will provide the Client with the Services in its best endeavour to the Client. </p>

                  <p class="sind"><strong>8.2.</strong> In the event that the Company is unable to provide the Services within a reasonable period from the dates(s) and time(s) and the Company have agreed or notified the Client then the Client will have the rights subject to the terms and conditions contained herein to exercise the Client’s option to wait until the Company is available to start performing the Services.</p>

                  <p class="sind"><strong>8.3.</strong> In the event that the Company has begun performance of the Services and the Client has in the course of exercising your right of termination of the Agreement pursuant to the provision contained herein, the Client will be liable to pay for any Services incurred by the Company up to the date of termination of the Agreement.  </p>

                  <p class="sind"><strong>8.4.</strong> Without limitation to any of the rights contained herein and contractual remedies, the Company reserves the right to claim for any loses and damages incurred as a result of the termination. </p>

                  <p class="sind"><strong>8.5.</strong> The Client shall not request the Company to perform Services which are immoral or unlawful in nature.  The decision will be at the absolute discretion of the Company. </p>

                  <p class="sind"><strong>8.6.</strong> The Client shall endeavour to provide the Company with as much detailed information as possible regarding the Services under request in order for the Company to provide excellent services. </p>

                  <p class="sind"><strong>8.7.</strong> The Client shall not request the Company to perform Services to, from and for people or places where the Company’s staffs, employees, agents and any other duly authorised entities of the Company may experience any form of abuse, bodily harm or death. </p>

                  <p class="sind"><strong>9.</strong> Payment </p>

                  <p class="sind"><strong>9.1.</strong> The Company offers various pricing as displayed on the Website  (the “Charge”)</p>

                  <p class="sind"><strong>9.2.</strong> The Company requires payment for Services to be made prior to the performance of the Service.</p>

                  <p class="sind"><strong>9.3.</strong> The Company shall be entitled to vary the Charge from time to time and shall communicate any such changes to the Client before any payment is made.</p>

                  <p class="sind"><strong>9.4.</strong> The Company shall be entitled to invoice the Client for any incidental costs incurred during the facilitation of the Client’s request, including costs from unexpected delays, toll charges etc.</p>

                  <p class="sind"><strong>9.5.</strong> All payments made to the Company via Credit Card transactions are liable to a [*]% handling charge. This handling charge of [*]% is added to the total sum owing to the Company by the Client. </p>

                  <p class="sind"><strong>10.</strong> Termination and Refunds </p>

                  <p class="sind"><strong>10.1.</strong> After the Parties have entered into the Agreement, refunds may only be requested, subject to the final discretion of the Company when performance by the Company has not begun. </p>

                  <p class="sind"><strong>10.2.</strong> Any refunds made by the Company will be with reduction of the direct costs including any handling costs. </p>

                  <p class="sind"><strong>10.3.</strong> Without prejudice to any other rights and remedies available, the Company shall have the right to terminate the Agreement for the provision of all or any of the Services upon written notice if the Client commits a serious breach of the terms and conditions contained herein. The Company reserves the right to claim against the Client including but not limited to losses and damages as a result of the termination by the Client. </p>

                  <p class="sind"><strong>10.4.</strong> On termination for any reason whatsoever, the Client shall immediately make payment to the Company of all and any sums outstanding and owing to the Company.</p>

                  <p class="sind"><strong>10.5.</strong> In the event that a deposit is paid by the Client, at the sole discretion of the Company, the deposit will be retained by the Company and for the necessary deduction of the Company losses and costs without prejudice to its rights to further claim damages against you. </p>

                  <p><strong>REMEDIES</strong></p>

                  <p class="sind"><strong>11.</strong> The Client acknowledges that a breach by him or her obligations hereunder will cause irreparable harm to the Company, by vitiating the intent and purpose of the transaction contemplated hereby. </p>

                  <p class="sind"><strong>12.</strong> Accordingly, the Client acknowledges that the remedy at law for a breach of its obligations herein will be inadequate and agrees, in the event of a breach of threatened breach by the Client of the provisions herein, the Company shall be entitled, in addition to all available remedies at law or in equity the claim for loss and damages and such claim shall be based on the contractual price of the services notwithstanding a discount applied.  </p>

                  <p class="sind"><strong>13.</strong> In the event that the Client breaches this Agreement, it will have forfeited its benefit of the discount provided by the Company (as the case may be) and the Company shall claim in full the original contractual price for the respective services provided or to be provided to the Client.</p>

                  <p><strong>LIABILITY, EXCLUSION AND LIMITATIONS</strong></p>

                  <p class="sind"><strong>14.</strong> The Company warrants to the Client that it shall use all of its reasonable endeavours to provide the Services using reasonable care and skill and as far as reasonably possible, in accordance with the Client’s order. </p>

                  <p class="sind"><strong>15.</strong> The Company will not exclude or limit liability for its negligence or negligent omission which causes personal injury or death. </p>

                  <p class="sind"><strong>16.</strong> The Company shall not be liable for any loss, cost, expense or damage of any nature whatsoever (whether direct or indirect) resulting from the use of Services except where it is expressly determined that a person acting under the direct instruction of the Company has knowingly acted in a negligent manner.</p>

                  <p class="sind"><strong>17.</strong> The Company shall have no liability to the Client for any loss, damage, costs, expenses or other claims for compensation arising from requests or instructions supplied by the Client which are incomplete, incorrect or inaccurate or any other fault of the Client.</p>

                  <p class="sind"><strong>18.</strong> The Company shall not be liable or be deemed to be in breach of the Agreement by reason of any delay in performing, or any failure, any of the Company’s obligations in relation to the Services, if the delay or failure was due to any cause beyond the Company’s reasonable control.</p>

                  <p class="sind"><strong>19.</strong> Subject to the provisions of the terms and conditions contained herein, the maximum liability of the Company to the Client for breach of any of its obligations hereunder shall be limited to the value of the Charge (provided that the Charge has at such time been paid by the Client in full).</p>

                  <p><strong>DISCLOSURE OF INFORMATION </strong></p>

                  <p class="sind"><strong>20.</strong> All private information shall be governed by the Privacy Policy on the Website. </p>

                  <p class="sind"><strong>21.</strong> Unless the Company receives notice from the Client to the contrary, the Company shall from time to time provide to the Client (by post, telephone or email) such information in relation to the Services that the Company considers may be of interest to the Client.</p>

                  <p><strong>AMENDMENTS </strong></p>

                  <p class="sind"><strong>22.</strong> The Company may update or modify this Agreement from time to time. If the Company modifies the Agreement during the Services, the modified version will take effect upon the next Service. </p>

                  <p class="sind"><strong>23.</strong> Client may be required to check the update version from time to time after the modified version takes effect, in any event the continued use of the Services shall constitute acceptance of the modified version. </p>

                  <p><strong>SEVERABILITY</strong></p>

                  <p class="sind"><strong>24.</strong> If any provision of this Agreement is found by any Court of competent jurisdiction to be unenforceable or invalid, that provision will be limited to the minimum extent necessary so that this Agreement may otherwise remain in effect. </p>

                  <p><strong>FORCE MAJEURE </strong></p>

                  <p class="sind"><strong>25.</strong> Neither Party will be liable for any delay or failure to perform its obligations under the Agreement (except payment obligations) if the delay or failure is due to causes beyond its reasonable control, such as a strike, blockade, war, act of terrorism, riot, natural disaster, failure or reduction of power or telecommunications or data networks or services, or government act.</p>

                  <p><strong>SUBPOENAS </strong></p>

                  <p class="sind"><strong>26.</strong> Nothing in the Agreement prevents the Company from disclosing Client information and data to the extent required by law, subpoenas, or court orders, but the Company will use commercially reasonable efforts to notify Client where permitted to do so.</p>

                  <p><strong>ASSIGNMENT </strong></p>

                  <p class="sind"><strong>27.</strong> The Agreement will bind and inure to the benefit of each Party’s permitted successors and assigns. Neither Party may assign the Agreement without the advance written consent of the other party, except that the Company may assign the Agreement without consent to an affiliate or in connection with a merger, reorganization, acquisition or other transfer of all or substantially all of its assets or voting securities. </p>


                  <p><strong>ENTIRE AGREEMENT </strong></p>

                  <p class="sind"><strong>28.</strong> The Agreement represents the parties’ complete and exclusive understanding relating to the Agreement’s subject matter. It supersedes all prior or contemporaneous oral communications, proposals and representations with respect to the Company or any other subject matter covered by this Agreement.</p>

                  <p><strong>INCORPORATION</strong></p>

                  <p class="sind"><strong>29.</strong> The Agreement, shall unless otherwise suggested, incorporate all terms and conditions contained and set out in the <a href="/privacy-policy">Privacy Policy</a> and other written documents deemed appropriate by the Company. </p>

                  <p><strong>GOVERNING LAW, JURISDICTION AND VENUE</strong></p>

                  <p class="sind"><strong>30.</strong> This Agreement is governed by the laws in Australia without regard to choice or conflict of law rules thereof.</p>

                  <p><strong>CONTACT US</strong></p>

                  <p class="sind"><strong>31.</strong> The Company welcomes your questions or comments regarding the foregoing Terms.</p>

                  <p>Email : info@mycryptowallet.com.au</p>

                  <p>Effective as of 27/10/2017</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('public.partials.start-trading')

@endsection
