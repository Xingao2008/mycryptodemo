@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="back"><a href="/legal">< Back</a></div>
    <div class="page-heading"><h2>Privacy Policy</h2><p>Updated as at 27/10/2017</p></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content" style="justify-content: flex-start;">
                <div class="content">
                  <p><strong>INTRODUCTION</strong></p>

                  <p class="sind"><strong>1.</strong> This site is owned and operated by MyCryptowallet and MyCryptowallet (“the Company”) is located at 'www.mycryptowallet.com.au' (“the Website”). For the purposes of this Privacy Policy, unless otherwise specified, all references to MyCryptowallet include the Company and the Website.</p>

                  <p class="sind"><strong>2.</strong> The Company’s provides the following services :-</p>

                  <p class="sind"><strong>2.1.</strong> Secure and free digital currency wallet creation and management services for four different cryptocurrencies including Bitcoin, Ethereum, Litecoin and Ripple; </p>

                  <p class="sind"><strong>2.2.</strong> Fixed price instant buying/selling exchange service for Australian dollars (AUD) and New Zealand dollars (NZD) to digital currency pairs such as Bitcoin/AUD & NZD, Litecoin/AUD & NZD, Ethereum/AUD & NZD and Ripple/AUD & NZD;  </p>

                  <p class="sind"><strong>2.3.</strong> Digital currency marketplace, facilitating peer-to-peer trading using a 'maker taker' model;</p>

                  <p class="sind"><strong>2.4.</strong> Swapping services between any two types of cryptocurrencies like Bitcoin – Litecoin, Litecoin – Bitcoin, Bitcoin – Ripple, Ripple – Bitcoin etc.; and</p>

                  <p class="sind"><strong>2.5.</strong> Free AUD/NZD fiat withdrawals to bank accounts.</p>

                  <p class="sind"><strong>3.</strong> Your privacy on the Internet is of the utmost importance to us. We will never share your personal information with any third party, but because we may gather certain types of information about our users, we feel you should fully understand our policy and the terms and conditions surrounding the capture and use of that information.  This privacy statement discloses what information we gather and how we use it. </p>

                  <p class="sind"><strong>4.</strong> The private information you provide on the Website will be used only for its intended purpose. </p>

                  <p class="sind"><strong>5.</strong> By using the Website, you consent to the data practices described hereinafter. </p>

                  <p><strong>PERSONAL INFORMATION</strong></p>

                  <p class="sind"><strong>6.</strong> As a general rule, the Company does not collect personal information about you when you visit the Website, unless you choose to provide such information to us.  Submitting personal information through the Website is voluntary. By doing so, you are giving the Company your permission to use the information for the stated purpose.  </p>

                  <p class="sind"><strong>7.</strong> If you choose to provide us with personal information on the Website, through methods such as sending us an email either directly or at info@mycryptowallet.com.au, we may use that information to help us provide you with information (collectively referred to as (“the Information”)).</p>

                  <p class="sind"><strong>8.</strong> We may also use the personal information to respond to any of your message(s) and/or feedback(s). The information we may receive from you varies based on what you do when visiting the Website. </p>

                  <p><strong>COLLECTION OF PERSONAL INFORMATION</strong></p>

                  <p class="sind"><strong>9.</strong> By using the Website and its functions, you may provide basic information such as including but not limited to your name, phone number, address and email address which allows us to send information, provide updates and/or process the type of Information you so desire. </p>

                  <p class="sind"><strong>10.</strong> The Company encourages you to review the privacy statements of websites you choose to link to from the Company so that you can understand how those websites collect, use and share your information (“Third Party Website”).  The Company is not responsible for the privacy statements or other contents on the Third Party Website outside of the Company’s website. </p>

                  <p><strong>THE NATURE OF PERSONAL INFORMATION WE COLLECT</strong></p>

                  <p class="sind"><strong>11.</strong> Personal information we collect which you may provide to us includes, inter alia:-</p>

                  <p class="sind"><strong>11.1.</strong> ‘Identifying information’ (i.e. name, date and place of birth, gender, age); and</p>

                  <p class="sind"><strong>11.2.</strong> ‘Contact information’ (i.e. home / office address, home / mobile / office phone numbers).</p>

                  <p class="sind"><strong>12.</strong> As a general rule, you have the right not to provide the foregoing information when dealing with the Company and/or when we provide the Information to you. </p>

                  <p><strong>AUTOMATICALLY COLLECTED INFORMATION</strong></p>

                  <p class="sind"><strong>13.</strong> We collect and temporarily store certain information about your visit for use in site management and security purposes only.  We collect and analyse this information because it helps us to better design the Website to suit your needs.  We may also automatically collect information about the web content you view in the event of a known security or virus threat. </p>

                  <p><strong>The information includes, inter alia :-</strong></p>

                  <p class="sind"><strong>13.1.</strong> The internet domain name from which you access our website (for example, “xcompany.com” if you use a private internet access account, or “yourschool.edu” if you connect from an educational domain);</p>

                  <p class="sind"><strong>13.2.</strong> The internet protocol (“IP”) address (a unique number for each computer connected to the internet) from which you access our Website; </p>

                  <p class="sind"><strong>13.3.</strong> The type of browser used to access our Website; </p>

                  <p class="sind"><strong>13.4.</strong> The operating system used to access our Website; </p>

                  <p class="sind"><strong>13.5.</strong> The date and time you access our Website; </p>

                  <p class="sind"><strong>13.6.</strong> The Universal Resource Locators (“URLs”) or address of the pages you visit; </p>

                  <p class="sind"><strong>13.7.</strong> Your username, if it was used to log in to the Website; and </p>

                  <p class="sind"><strong>13.8.</strong> If you visited this Website from another website, the URL of the forwarding site. </p>

                  <p><strong>INFORMATION COLLECTED FOR TRACKING AND CUSTOMIZATION (COOKIES)</strong></p>

                  <p class="sind"><strong>14.</strong> A ‘cookie’ is a small file that a website transfers to your computer to allow it to remember specific information about your session while you are connected. Your computer will only share the information in the cookie with the website that provided it, and no other website can request it. There are two types of cookies:</p>

                  <p class="sind"><strong>14.1.</strong> ‘Session’: Session cookies last only as long as your web browser is open. Once you close your browser, the cookie is deleted. Websites may use session cookies for technical purposes such as to enable better navigation through the site, or to allow you to customize your preferences for interacting with the site.</p>

                  <p class="sind"><strong>14.2.</strong> ‘Persistent’: Persistent cookies are saved on a user’s hard drive in order to determine which users are new to the site or are returning.</p>

                  <p><strong>USE OF YOUR PERSONAL INFORMATION</strong></p>

                  <p class="sind"><strong>15.</strong> Personal information submitted to us through our website will be used for the purposes specified in this policy or on the relevant pages of the website.</p>

                  <p class="sind"><strong>16.</strong> We may use your personal information to: </p>
                  <p class="sind"><strong>16.1.</strong> administer our website and business;</p>
                  <p class="sind"><strong>16.2.</strong> personalize our website for you;</p>
                  <p class="sind"><strong>16.3.</strong> send you non-marketing commercial communications;</p>
                  <p class="sind"><strong>16.4.</strong> send you email notifications that you have specifically requested;</p>
                  <p class="sind"><strong>16.5.</strong> send you our email newsletter, if you have requested it (you can inform us at any time if you no longer require the newsletter);</p>
                  <p class="sind"><strong>16.6.</strong> send you marketing communications relating to our business which we think may be of interest to you, by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications);</p>
                  <p class="sind"><strong>16.7.</strong> deal with enquiries and complaints made by or about you relating to our website;</p>
                  <p class="sind"><strong>16.8.</strong> keep our website secure and prevent fraud; and</p>
                  <p class="sind"><strong>16.9.</strong> verify compliance with the terms and conditions governing the use of our website (including monitoring private messages sent through our website private messaging service).</p>
                  <p class="sind"><strong>17.</strong> If you submit personal information for publication on our website, we will publish and otherwise use that information in accordance with the licence you grant to us.</p>

                  <p class="sind"><strong>18.</strong> Your privacy settings can be used to limit the publication of your information on our website, and can be adjusted using privacy controls on the website.</p>

                  <p class="sind"><strong>19.</strong> We will not, without your express consent, supply your personal information to any third party for the purpose of their or any other third party's direct marketing.</p>

                  <p><strong>DISCLOSURE OF YOUR PERSONAL INFORMATION</strong></p>

                  <p class="sind"><strong>20.</strong> The personal information you provide to us whether voluntarily or automatically as the foregoing paragraphs describes, may be used and disclosed without limitations, to our employees, staffs, insurers, professionals, agents and/or other parties we deem fit for the purpose and in the manner reasonably necessary for purposes set out in this Privacy Policy.</p>

                  <p class="sind"><strong>21.</strong> Personal information may be disclosed and/or supplied between the third parties. When personal information is disclosed and/or supplied between the third parties all terms and conditions contained herein shall have effect for the purpose of protecting your personal information. </p>

                  <p class="sind"><strong>22.</strong> Compliance with legal requirements for the enforcement of law, regulations, court orders, subpoena, warrant during the course of a legal proceedings or otherwise may render our need to disclose personal information. </p>

                  <p class="sind"><strong>23.</strong> Personal information may also be used to protect and safeguard the copyright, trademarks, legal rights, intellectual property rights or safety of the Company.</p>

                  <p><strong>SECURITY OF YOUR PERSONAL INFORMATION</strong></p>

                  <p class="sind"><strong>24.</strong> The Company secures your personal information from unauthorized access, use or disclosure.</p>

                  <p><strong>CHILDREN UNDER THIRTEEN</strong></p>

                  <p class="sind"><strong>25.</strong> The Company does not knowingly collect personal information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use this Website.</p>

                  <p><strong>OPT-OUT & UNSUBSCRIBE</strong></p>

                  <p class="sind"><strong>26.</strong> The Company respects your privacy and gives you an opportunity to opt-out of receiving information in respect of the Information by contacting us at info@mycryptowallet.com.au.</p>

                  <p><strong>CHANGES</strong></p>

                  <p class="sind"><strong>27.</strong> The terms contained in this Privacy Policy may change and subject to modification and amendments. All modifications and the time of the changes and modification will be subject to the sole direction of the Company and such changes will have immediate effect. No notice will be given to you prior to such changes and/or modifications; you are therefore advised to review our Privacy Policy from time to time.</p>

                  <p><strong>CONTACT</strong></p>

                  <p class="sind"><strong>28.</strong> The Company welcomes your questions or comments regarding the foregoing Terms.</p>

                  <p class="sind"><strong>29.</strong> Please find us at info@mycryptowallet.com.au.  You could also contact us at PO BOX 2250, VIC 3429 Australia.</p>

                  <p>Effective as of 27/10/2017.</p>

                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('public.partials.start-trading')

@endsection
