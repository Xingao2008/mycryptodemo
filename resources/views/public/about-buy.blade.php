
@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <h2 class="page-title-heading">Withdrawals / Deposits</h2>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <h4>Buy & Sell Through Our Instant Exchange Services</h4>
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>myCryptoWallet eliminates long exchange periods thanks to our instant buying and selling exchange service. Our platform allows verified users to instantaneously deposit and withdraw funds between their personal accounts into their secure crypto wallet.</p>
                <p>We offer fixed-rate prices for both Australian dollars (AUD) and New Zealand dollars (NZD) exchanges into four cryptocurrencies options. Diversify your digital portfolio with Bitcoins, Ethereum, Litecoins and Ripple.</p>
                <br>
              </div>
              <div class="fifty is-hidden-mobile"></div>
              <div class="fifty">
                <p><strong>Take Advantage of Our <span style="color: #2ED0B5;display: inline-block;line-height: 1;">Free</span> Fiat Withdrawals</strong></p>
                <p>As a customary service to our users, we offer free fiat withdrawals (AUD/NZD) to verified users’ bank accounts. You’ll never have to worry about constant withdrawal fees that other blockchain marketplaces apply on to your exchanges.</p>
                <br>
              </div>
              <div class="fifty is-hidden-mobile"></div>
              <div class="fifty">
                <p><strong>Swap between Different Currencies Anytime</strong></p>
                <p>Our exchange services don’t just stop at transferring between AUD & NZD to our four available cryptocurrencies. Our platform also allows for swapping between the various blockchain assets. Switch from Bitcoin to Litecoin, Ripple to Ethereum, and any other combinations of exchanges to stay on top of the market.</p>
                <br>
              </div>
              <div class="fifty is-hidden-mobile"></div>
              <div class="fifty">
                <p><strong>All Transactions Are Safe and Secure</strong></p>
                <p>myCryptoWallet guarantees the safety and security of your assets by integrating the latest in blockchain distributed ledger technology into our marketplace platform. This technology offers the highest level of security so your assets never get compromised.</p>
                <br>
              </div>
            </div>
        </div>
      </div>
      <!-- <div class="dashboard-content white">
        <div class="wrapper flex-wrap">
          <div class="flex-group align-normal">
            <div class="fifty">
              <p><strong>Take Advantage of Our <span style="color: #2ED0B5;display: inline-block;line-height: 1;">Free</span> Fiat Withdrawals</strong></p>
              <p>As a customary service to our users, we offer free fiat withdrawals (AUD/NZD) to verified users’ bank accounts. You’ll never have to worry about constant withdrawal fees that other blockchain marketplaces apply on to your exchanges.</p>
            </div>
            <div class="fifty">
              <p><strong>Swap between Different Currencies Anytime</strong></p>
              <p>Our exchange services don’t just stop at transferring between AUD & NZD to our four available cryptocurrencies. Our platform also allows for swapping between the various blockchain assets. Switch from Bitcoin to Litecoin, Ripple to Ethereum, and any other combinations of exchanges to stay on top of the market.</p>
            </div>
            <div class="fifty">
              <p><strong>All Transactions Are Safe and Secure</strong></p>
              <p>myCryptoWallet guarantees the safety and security of your assets by integrating the latest in blockchain distributed ledger technology into our marketplace platform. This technology offers the highest level of security so your assets never get compromised.</p>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>

@endsection