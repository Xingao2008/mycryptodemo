@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="back"><a href="/legal">< Back</a></div>
    <div class="page-heading"><h2>Terms and Conditions of Use of
myCryptoWallet (“the Website”)</h2><p>[Updated as at 27/10/2017] </p></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content" style="justify-content: flex-start;">
                <div class="content">
                  <p><strong>INTRODUCTION</strong></p>
                  <p class="sind"><strong>1.</strong> Welcome to Mycryptowallet a website provided by Mycryptowallet (“the Company”). </p>

                  <p class="sind"><strong>2.</strong> The Website is comprised of various web pages operated by the Company and is offered to you conditioned on your acceptance without modification of the terms, conditions and notices contained herein (“the Terms”). Your use of the Website constitutes your agreement to all such Terms. Please read these terms carefully and keep a copy of them for your reference. </p>

                  <p class="sind"><strong>3.</strong> The Company at 'www.mycryptowallet.com.au' provides, various services as specified on the Company’s Website. </p>

                  <p><strong>PRIVACY</strong></p>
                  <p class="sind"><strong>4.</strong> Your use of the Website is subject to the Company’s Privacy Policy. Please review our Privacy Policy, which also governs the Website and informs users of our data collection practices. </p>

                  <p><strong>ELECTRONIC COMMUNICATIONS</strong></p>
                  <p class="sind"><strong>5.</strong> Visiting the Website, subscribing to the Company and/or sending emails to the Company constitute electronic communications. You consent to receive electronic communications and you agree that all agreements, notices disclosures and other communications that we provide to you electronically via email and/or on the Website satisfy the legal requirement that such communications be in writing. </p>

                  <p class="sind"><strong>6.</strong> The Company does not knowingly collect either online or offline, personal information from persons under the age of thirteen.  If you are under 18, you may use the Website only with permission of a parent or guardian. </p>


                  <p><strong>NO UNLAWFUL OR PROHIBITED USE / INTELLECTUAL PROPERTY</strong></p>
                  <p class="sind"><strong>7.</strong> You are granted a non-exclusive, non-transferable, recoverable license to access and use of the Website strictly in accordance with these terms of use.  As condition of your use of the Website, you warrant to the Company that you will not use the Website for any purpose that is unlawful or prohibited by these Terms. </p>

                  <p class="sind"><strong>8.</strong> You may not use the Website in any manner which could damage, disable, overburden, or impair the Website or interfere with any other party’s use and enjoyment of the Site. You may not obtain or attempt to obtain any material or information through any means not intentionally made available through the Website. </p>

                  <p class="sind"><strong>9.</strong> All content included herein, such as inter alia, text, graphics, logos, images, videos, as well as the compilation thereof, and any software used on the Website, is the property of the Company and/or its suppliers and protected by copyright and other laws that protect intellectual property and proprietary rights. You agree to observe and abide by all copyright and other proprietary notices, legends or other restrictions contained in any such content and will not make any changes thereto. </p>

                  <p class="sind"><strong>10.</strong> You will not modify, publish, transmit, reverse engineer, participate in the transfer or sale, create derivative works, or in any way exploit any of the content, in whole or in part, found on the Website. </p>

                  <p class="sind"><strong>11.</strong> The Company content is not for resale. </p>

                  <p class="sind"><strong>12.</strong> Your use of the Site does not entitle you to make any unauthorised use of any protected content, and in particular you will not delete or alter any proprietary rights or attribution notices in any content.  You will use protected content solely for your personal use, and will make no other use of the content without the express written permission of the Company and the copyright owner.  You agree that you do not acquire any ownership rights in any protected content.  We do not grant you any licenses, express or implied, to the intellectual property of the Company or our licensors except as expressly authorised by these Terms. </p>

                  <p><strong>INTERNATIONAL USERS</strong></p>
                  <p class="sind"><strong>13.</strong> All information and content on the Website are controlled, operated and administered by the Company from our offices in Australia.  If you access the information and content from a location outside of Australia, you are responsible for compliance with all local laws. </p>

                  <p class="sind"><strong>14.</strong> Your agree that you will not use the Company Content accessed through the Website in any country or in any manner prohibited by any applicable laws, restrictions or regulations. </p>


                  <p><strong>INDEMNIFICATION</strong></p>
                  <p class="sind"><strong>15.</strong> You agree to indemnify, defend and hold harmless, without limitations, the Company, its officers, directors, employees, agents and third parties, for any losses, costs, liabilities and expenses (including reasonable attorneys’ fees) relating to or arising out of your use of or inability to use the Website, your violation of any terms of this Agreement or your violation of any rights of a third party, or your violation of any applicable laws, rules or regulations.</p>

                  <p class="sind"><strong>16.</strong> The Company reserves the right, at its own cost, to assume the exclusive defence and control of any matter otherwise subject to indemnification by you, in which event you will fully cooperate with the Company in asserting any available defences. </p>

                  <p><strong>LIABILITY DISCLAIMER</strong></p>
                  <p class="sind"><strong>17.</strong> The information, software, products and services included in or available through the Website may include inaccuracies or typographical errors. Changes are periodically added to the information herein. The Company and/or its suppliers may make improvements and/or changes in the Website at any time.</p>

                  <p class="sind"><strong>18.</strong> The Company and/or its suppliers make no representations about the suitability, reliability, availability, timeliness and accuracy of the information, software, products, services and related graphics contained on the site for any purpose to the maximum extent permitted by applicable law, all such information, software, products, services and related graphics are provided “as is” without warranty or conditions of any kind.  The Company and/or its suppliers hereby disclaim all warranties and conditions with regard to this information, software, products, services and related graphics, including all implied warranties or conditions of merchantability, fitness for a particular purpose, title and non-infringement. </p>

                  <p class="sind"><strong>19.</strong> To the maximum extent permitted by applicable law, in no event shall the Company and/or its suppliers be liable for any direct, indirect, punitive, incidental, special, consequential damages or any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the site, with the delay or inability to use the site or related services, the provision of any failure to provide services, or for any information, software, products, services and related graphics obtained through the Website, or otherwise arising out of the use of the Website, whether based on contract, tort, negligence, strict liability or otherwise, even if the Company or any of its Suppliers has been advised of the possibility of damages. </p>

                  <p class="sind"><strong>20.</strong> The above limitation may not apply to you. If you are dissatisfied with any portion of the Website, or with any of these terms of use, your sole and exclusive remedy is to discontinue using the Website. </p>

                  <p><strong>TERMINATION / ACCESS RESTRICTION</strong></p>
                  <p class="sind"><strong>21.</strong> The Company reserves the right, in its sole discretion, to terminate your access to the Website and the related services of any portion thereof at any time, without notice.</p>


                  <p><strong>JURISDICTION</strong></p>
                  <p class="sind"><strong>22.</strong> To the maximum extent permitted by law, this Agreement is governed by the laws in Australia all disputes arising out of or relating to the use of the Website.  Use of the Website is unauthorised in any jurisdiction that does not give effect to all provision of these Terms, including, without limitation, this section.</p>

                  <p><strong>SEVERABILITY</strong></p>
                  <p class="sind"><strong>23.</strong> If any part of this Agreement is determined to be invalid or unenforceable pursuant to applicable law including but not limited to the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid enforceable provision that most closely matches the intent of the original provision and the remainder of the Agreement shall continue in effect. </p>

                  <p><strong>ENTIRE AGREEMENT</strong></p>
                  <p class="sind"><strong>24.</strong> Unless otherwise specified herein, this Agreement constitutes the entire agreement between the user and the Company with respect to the Website and it supersedes all prior contemporaneous communication and proposals, whether electronic, oral or written between the user and the Company with respect to the Website.  A printed version of this Agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.  It is the express wish to the parties that this Agreement and all related documents be written in English.</p>

                  <p><strong>CHANGES TO TERMS</strong></p>
                  <p class="sind"><strong>25.</strong> The Company reserves the right, in its sole discretion, to change the Terms. The most current version of the Terms will supersede all previous versions.  The Company encourages you to periodically review the Terms to stay informed of our updates.</p>

                  <p><strong>CONTACT</strong></p>
                  <p class="sind"><strong>26.</strong> The Company welcomes your questions or comments regarding the foregoing Terms.
                  You may contact us at info@mycryptowallet.com.au.</p>

                  <p>Effective as of 27/10/2017.</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('public.partials.start-trading')

@endsection
