@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="back"><a href="/legal">< Back</a></div>
    <div class="page-heading"><h2>Copyright</h2></div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal single-news-item-page">
              <div class="single-news-item-page-content" style="justify-content: flex-start;">
                <div class="content">
<p><strong>GENERAL</strong></p>
<p class="sind"><strong>1.</strong> Mycryptowallet (“the Company”) with its website at ‘www.mycryptowallet.com.au’ (“the Website”).</p>

<p><strong>COPYRIGHT NOTICE</strong></p>
<p class="sind"><strong>2.</strong> Subject to the express provisions of this notice :- </p>

<p class="lind"><strong>(a)</strong> We, together with our licensors, own and control all the copyright and other intellectual property rights on our Website; and </p>

<p class="lind"><strong>(b)</strong> All the copyright and other intellectual property rights on our Website are reserved.</p>

<p><strong>COPYRIGHT LICENCE </strong></p>
<p class="sind"><strong>3.</strong> Except as expressly permitted by the other provisions of this Policy and the approval of the Company, you must not download any material from our Website or save any such material to your computer. </p>

<p class="sind"><strong>4.</strong> You may only use the Website for your own personal and/or business purposes and you must not use the Website for any other purposes. </p>

<p class="sind"><strong>5.</strong> Except as expressly permitted by this Policy, you must not edit or otherwise modify any material on the Website. </p>

<p class="sind"><strong>6.</strong> Unless you own or control the rights in the material, you must not :- </p>

<p class="lind"><strong>(a)</strong> Republish material from our Website (including republication on other social media); </p>

<p class="lind"><strong>(b)</strong> Sell, rent, or sub-license material from the Website; </p>

<p class="lind"><strong>(c)</strong> Show any material from the Website in public; </p>

<p class="lind"><strong>(d)</strong> Exploit material from the Website for a commercial purpose; or </p>

<p class="lind"><strong>(e)</strong> Redistribute material from our Website, save to the extent expressly permitted by this Notice. </p>

<p><strong>ACCEPTABLE USE </strong></p>
<p class="sind"><strong>7.</strong> You must not:-</p>

<p class="lind"><strong>(a)</strong> use the Website in any way or take any action that causes, or may cause, damage to Website and/or Company or impairment of the performance, availability or accessibility of the Website;</p>

<p class="lind"><strong>(b)</strong> use the Website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;</p>

<p class="lind"><strong>(c)</strong> use the Website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software; or</p>

<p class="lind"><strong>(d)</strong> conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent.</p>

<p><strong>REPORT ABUSE AND COPYRIGHT INFRINGEMENT NOTIFICATION</strong></p>
<p class="sind"><strong>8.</strong> If you learn of any unlawful material or activity on our Website, or any material or activity that breaches this Policy, please let us know. </p>

<p class="sind"><strong>9.</strong> You can let us know about any such material or activity by email at 'info@mycryptowallet.com.au'. </p>

<p class="sind"><strong>10.</strong> The email should contain the following materials :- </p>

<p class="lind"><strong>(a)</strong> An email address, physical address or phone number to enable the Company to be in contact with you; </p>

<p class="lind"><strong>(b)</strong> The description of the unlawful material; </p>

<p class="lind"><strong>(c)</strong> A statement verifying that you have reasonable belief in good faith that the unlawful material is not authorised by the copyright owner and/or any entities associated or known to be associated to the copyright owner; </p>

<p class="lind"><strong>(d)</strong> In the event that the material belongs to you, and you are the rightful legal owner to the material, you shall deliver to us a statement verifying that you are the copyright owner or duly authorised by the copyright owner to submit such application on his/her behalf together with a duly signed e-form specifying you full legal name. </p>

<p><strong>ENFORCEMENT OF COPYRIGHT</strong></p>
<p class="sind"><strong>11.</strong> We take the protection of our copyright very seriously. </p>

<p class="sind"><strong>12.</strong> If we discover that you have used our copyright materials in contravention of the licence set out in this Policy, we may bring legal proceedings against you, seeking monetary damages and/or an injunction to stop you using those materials. You could also be ordered to pay legal costs. </p>

<p><strong>PERMISSIONS</strong></p>
<p class="sind"><strong>13.</strong> You may request permission to use the copyright materials on our Website by writing to us by email and submitting it to ‘info@mycryptowallet.com.au’.  </p>

<p><strong>CONTACT</strong></p>
<p class="sind"><strong>14.</strong> The Company welcomes your views in respect of the foregoing terms. </p>

<p>Email : info@mycryptowallet.com.au<br>
Address: PO BOX 2250, VIC 3429 Australia</p>

<p>Effective as of 27/10/2017.</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

    @include('public.partials.start-trading')
@endsection
