@extends('layouts.app')

@section('content')
<div class="container dashboard sub-page">
  <div class="main">
    <div class="page-heading">
      <h2>Legal</h2>
    </div>
    <div class="container-fluid">
      @include('flash::message')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="flex-group align-normal">
              <div class="fifty">
                <p>myCryptoWallet eliminates long exchange periods thanks to our instant buying and selling exchange service. Our platform allows verified users to instantaneously deposit and withdraw funds between their personal accounts into their secure crypto wallet.</p>
                <br>
                <div class="single-contact-module legal-module">
                    <p><a href="/privacy-policy"><span class="icon-download2"></span> Privacy Policy</a></p>
                    <p><a href="/terms-conditions-use"><span class="icon-download2"></span> Terms and Conditions of Use</a></p>
                    <p><a href="/terms-conditions-service"><span class="icon-download2"></span> Terms and Conditions of Service</a></p>
                    <p><a href="/disclaimer"><span class="icon-download2"></span> Disclaimer</a></p>
                    <p><a href="/copyright"><span class="icon-download2"></span> Copyright</a></p>
                </div>
              </div>
              <div class="fifty">
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
