@extends('layouts.private')

@section('page-heading', 'Buy Order Successful')
@section('title', 'Buy Order Successful')
@section('page-sub-heading', '')

@section('content')
 <div class="wrapper sub-flex">
    <div class="general-content-area">
      <div class="top-note">
        <h4>Congratulations, your buy order was successful!<br>The final details are below:</h4>
      </div>
      <div class="body buy-order-confirm">
        <ul>
          <li>Coin: {{strtoupper($type)}}</li>
          <li>Market price: ${{$baseline_price}} + {{setting('public_fee')}}% fee</li>
          <li>{{strtoupper($currency)}} Spent: <b>${{$amount}}</b></li>
          <li>Coins received: <b>{{$quantity}}</b></li>
        </ul>
        <br>
        <p style="max-width: 500px; margin: auto;">You can see your past orders by clicking your name in the top-right and clicking Orders.</p>
        <p class="fee">Your funds are available immediately.</p>
      </div>
    </div>
    @include('partials.mywallet')
</div>
@endsection
