@extends('layouts.private')

@section('page-heading', 'Two Factor Authentication')
@section('title', 'Two Factor Authentication')
@section('page-sub-heading', '')
@section('breadcrumbs', Breadcrumbs::render('account.tfa'))
@section('content')
<div class="wrapper flex-wrap tfa-page">
    <div class="general-content-area">
        @if (session()->has('status'))
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">×</a>
                {!! session()->get('status') !!}
            </div>
        @endif
        @if (Auth::user()->tfa_status)
            You have successfully enabled TFA on your account!
        @else
            <p class="tfa-message">Once you enter the code below, Two-Factor Authentication will be enabled instantly on your account. You'll need to download the Google Authenticator on your mobile to proceed.</p>
            <div style="height: 30px;"></div>
            <div class="center-content">
                <img src="{{ $google2fa_url }}" alt="">
                <center>Secret: {{ $tfasecret }}</center>
                <div style="height: 30px;"></div>
                <form role="form" method="POST" action="/account/tfa">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="tfa">TFA Code</label>
                        <input type="text" class="form-control" style="max-width: 200px; margin: auto;" id="tfa" name="tfa" >
                    </div>
                    <div class="center-content">
                        <button type="submit" class="btn btn-teal">Update</button>
                    </div>
                    @if (count($errors))
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </form>
            </div>
        @endif
    </div>
</div>
@endsection
