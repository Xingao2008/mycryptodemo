@extends('layouts.private')

@section('page-heading', 'Deposit')

@section('title', 'Deposit')

@section('content')

        <div class="dashboard-content">
            <div class="wrapper sub-flex">
                <div class="deposit general-content-area" style="text-align: left;">
                  @if($path == "aud")
                    <div class="panel-header columns">
                        <div class="column">
                            <p>In order to deposit AUD, you can make a <b>Bank Transfer or use Poli Payments</b>.</p>
                            <br>
                            <p><b>Bank transfers are free</b>: you'll receive into your account exactly what you send.</p>
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                    <div class="panel-body">
                     <?php /*
                        <p>In order to deposit AUD, you can make a Bank Transfer or use Poli Payments.</p>

                        <p>Bank transfers are <b>free</b>: you'll receive into your account exactly what you send.</p>

                        <h2>Bank Transfer</h2>
                        <p>If using Bank Transfer, <b>you MUST include your UBC (unique bank code) code</b> (located in <a href="/account">your account</a>) in the description field of the transfer, otherwise we won't be able to match your deposit to your account. <b>Please note, if your UBC is missing in the transfer description, it will incur a 2% admin fee.</b></p>
                        <p>Please transfer to:</p>
                        <u>BSB:</u> 083 184<br>
                        <u>Account #:</u> 35 208 4906<br>
                        <u>Account Name:</u> My WALLET<br>
                        <u>Reference/Description:</u> <b>{{Auth::user()->ubc}}</b> (this is your UBC)<Br>
                        <u>Amount:</u> Should be more than $50 due to minimum buy orders.<br>
                        <b>Please read:</b> If your bank doesn't allow dashes/hyphens in the Reference/Statement Descriptor/Details field, please use a space - as long as we have the numbers and letters, we can match your deposit up.<br><br>
                        */ ?>

                        <p>Payments made with POLi are instant. If there is an issue, please contact support.</p>


                        @if(!\Auth::user()->canPoliPay())
                            <img src="https://transaction.apac.paywithpoli.com/images/logo.png" alt="polipayments" />
                            <p>Unfortunately you don't meet the minimum requirements to use POLi Pay.</p>
                            <table>
                                <thead>
                                    <tr>
                                        <th width="60%">Requirement</th>
                                        <th width="30%">Value</th>
                                        <th>Pass</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <strong>Transaction Volume more than $1,000</strong>
                                        </td>
                                        <td>
                                            ${{ audFormat(\Auth::user()->transaction_volume()) }}
                                        </td>
                                        <td>
                                            {!!  \Auth::user()->transaction_volume() >= 1000 ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Account age older than 5</strong>
                                        </td>
                                        <td>
                                            {{ \Auth::user()->created_at->diffInDays() }} days old
                                        </td>
                                        <td>
                                            {!! \Auth::user()->created_at->diffInDays() >= 5 ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Recommended (Silver) Tier or Above</strong>
                                        </td>
                                        <td>
                                            {{ ucfirst(tier()) }}
                                        </td>
                                        <td>
                                            {!! tier() != 'unverified' ? '<i class="fa fa-check" aria-hidden="true"></i>' : '<i class="fa fa-times" aria-hidden="true"></i>' !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        @elseif(!setting('enable_polipay'))
                            <img src="https://transaction.apac.paywithpoli.com/images/logo.png" alt="polipayments" />
                            <p>POLi Payments are currently disabled.</p>
                        @else
                        <img src="https://transaction.apac.paywithpoli.com/images/logo.png" alt="polipayments" />
                        <div class="poly-list">
                            <ul>
                                <li>Payments made with<b>POLI are instant</b> if there is an issue, please <a href="{{ route('contact') }}">Contact support</a>.</li>
                                <li><b>POLi Payments</b> allow for bank transfers with instant fund transfer, so you don't have to wait!</li>
                                <li>Not all banks are available, so please <a href="https://transaction.apac.paywithpoli.com/POLiFISupported.aspx?merchantcode=S6102972">Click here</a> to see if your bank is supported!</li>
                                <li>Amount should be more than <b>$50</b> due to minimum buy orders. Limit of <b>${{ \App\Models\UserLimits::get_limit('poli_transaction_limit') }}</b> per transaction</li>
                            </ul>
                        </div>

                        <!-- <div class="columns">
                            <p>
                                Not all banks are available, so please <a href="https://transaction.apac.paywithpoli.com/POLiFISupported.aspx?merchantcode=S6102972" target="_blank">Click Here</a> to see if your bank is supported!
                            </p>
                        </div>
                        <div class="columns">
                            <p>Amount should be more than $50 due to minimum buy orders. Limit of ${{ \App\Models\UserLimits::get_limit('poli_transaction_limit') }} per transaction</p>
                        </div> -->

                            <form class="deposit-form" role="form" method="POST" action="{{ url()->route('polipay.index') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="container-fluid">
                                    <label for="amount"><b>Amount to deposit:</b></label>
                                    <div class="form-group columns">
                                        <div class="column is-size-3"><input type="text" class="form-control" id="amount" name="amount" value="{{ request()->old('amount') }}" min="49" required="" /></div>
                                        <div class="column is-size-3"><button type="submit" class="btn btn-blue-fading">Deposit</button></div>
                                    </div>
                                </div>
                            </form>
                        @endif

                    </div>
                    @elseif($path == "nzd")
                        <div class="panel-header columns">
                            <div class="column">
                                <p>In order to deposit <b>NZD</b>, you can make a <b>Bank Transfer</b>.</p>
                                <br>
                                <p><b>Bank transfers are free</b>: you'll receive into your account exactly what you send.</p>
                            </div>
                            <div class="column is-hidden-mobile is-5">
                                <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                            </div>
                        </div>

                        <div class="panel-body">
                            <h2>Bank Transfer</h2>
                            <p>If using Bank Transfer, <b>you MUST include your UBC (unique bank code) code</b> (located in <a href="/account">your account</a>) in the description field of the transfer, otherwise we won't be able to match your deposit to your account.<b>Please note, if your UBC is missing in the transfer description, it will incur a 2% admin fee.</b></p>
                            <p>Please transfer to:</p>
                            <u>BSB:</u> 030 394<br>
                            <u>Account #:</u> 0011 043 000<br>
                            <u>Account Name:</u> MYCRYPTOWALLET<br>
                            <u>Reference/Description:</u> <b>{{Auth::user()->ubc}}</b> (this is your UBC)<Br>
                            <u>Amount:</u> Should be more than $50 due to minimum buy orders.<br>
                            <b>Please read:</b> If account name doesn't fit, just put MYCRYPTOWALLET. If your bank doesn't allow dashes/hyphens in the Reference/Statement Descriptor/Details field, please use a space - as long as we have the numbers and letters, we can match your deposit up.<br><br>
                        </div>
                    @elseif($path == "usd")
                        <div class="panel-header columns">
                            <div class="column">
                                <p>In order to deposit <b>USD</b>, you can make a <b>Bank Transfer</b>.</p>
                                <br>
                                <p><b>Bank transfers are free</b>: you'll receive into your account exactly what you send.</p>
                            </div>
                            <div class="column is-hidden-mobile is-5">
                                <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                            </div>
                        </div>
                        <div class="panel-body">
                            <h2>Bank Transfer</h2>
                            <p>If using Bank Transfer, <b>you MUST include your UBC (unique bank code) code</b> (located in <a href="/account">your account</a>) in the description field of the transfer, otherwise we won't be able to match your deposit to your account.<b>Please note, if your UBC is missing in the transfer description, it will incur a 2% admin fee.</b></p>
                            <p>Please transfer to:</p>
                            <u>Account #:</u> 8310 0500 85<br>
                            <u>Wire Routing Number #:</u> 0260 7300 8<br>
                            <u>ACH Routing Number #:</u> 0260 7315 0<br>
                            <u>Account Name:</u> myCryptoWallet<br>
                            <u>Reference/Description:</u> <b>{{Auth::user()->ubc}}</b> (this is your UBC)<Br>
                            <u>Amount:</u> Should be more than $50 due to minimum buy orders.<br>
                            <b>Please read:</b> If account name doesn't fit, just put MYCRYPTOWALLET. If your bank doesn't allow dashes/hyphens in the Reference/Statement Descriptor/Details field, please use a space - as long as we have the numbers and letters, we can match your deposit up.<br><br>
                        </div>
                  @elseif($path == "btc")
                    <div class="panel-header columns">
                        <div class="column">
                            <h2 style="font-size:25px;">To deposit <b>Bitcoin</b></h2>
                            <br>
                            <p>Simply generate an address and send your Bitcoin to that address.</p>
                            <br>
                            <p><b>Warning:</b> You <b>MUST</b> deposit more than 0.005 BTC otherwise your deposit may be lost.</p>
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                    <!-- <div class="panel-body">
                    </div> -->
                  @elseif($path == "eth")
                    <div class="panel-header columns">
                        <div class="column">
                            <h2 style="font-size:25px;">To deposit <b>Ethereum</b></h2>
                            <br>
                            <ol>
                                <li>Generate an address on myCryptoWallet</li>
                                <li>Send your Ethereum to that address</li>
                                <li>Click "check for deposit" after 15 minutes has passed</li>
                            </ol>
                            <br>
                            <p><b>Warning:</b> You <b>MUST</b> deposit more than <b>0.05 ETH</b> otherwise your deposit may be lost.</p>
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                  @elseif($path == "ltc")
                    <div class="panel-header columns">
                        <div class="column">
                            <h2 style="font-size:25px;">To deposit <b>Litecoin</b></h2>
                            <br>
                            <p>Simply generate an address and send your Litecoin to that address.</p>
                            <br>
                            <b>Warning:</b> You <b>MUST</b> deposit more than <b>0.5 LTC</b> otherwise your deposit may be lost.
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                  @elseif($path == "xrp")
                    <div class="panel-header columns">
                        <div class="column">
                            <h2 style="font-size:25px;">To deposit <b>Ripple</b></h2>
                            <br>
                            <p>Simply generate an address and send your Ripple to that address.</p>
                            <br>
                            <p><b>Please note:</b> Ripple requires a new wallet/address to reserve 20 to 25 Ripple in order to activate. Your first deposit will be deducted around 20 to 25 Ripple for this. Read more on the official Ripple website: <a href="https://ripple.com/build/reserves/">https://ripple.com/build/reserves/</a></p>
                            <br>
                            <p><b>Warning:</b> You <b>MUST</b> deposit more than <b>30 XRP</b> otherwise your deposit may be lost.</p>
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                  @elseif($path == "pwr")
                    <div class="panel-header columns">
                        <div class="column">
                            <h2 style="font-size:25px;">To deposit <b>Power Ledger</b></h2>
                            <br>
                            <p>Simply generate an Ethereum address and send your PowerLedger to that address.</p>
                            <br>
                            <p><b>Warning:</b> You <b>MUST</b> deposit more than <b>50 POWR</b> otherwise your deposit may be lost.</p>
                        </div>
                        <div class="column is-hidden-mobile is-5">
                            <img class="bank-clipper" src="{{ url('img/deposit/Bank_Clipart.png') }}">
                        </div>
                    </div>
                  @endif
                </div>
                @include('partials.mywallet')
            </div>
        </div>

@endsection
