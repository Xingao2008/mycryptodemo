@extends('layouts.private')

@section('page-heading')
    Withdraw <span class="capitalise">
    @if ($path == 'pwr')
    POWR
    @else
        {{strtoupper($path)}}
    @endif
    </span>
@endsection

@section('title')
    Withdraw  
    @if ($path == 'pwr')
        POWR
    @else
        {{strtoupper($path)}}
    @endif
@endsection

@section('page-sub-heading', '')

@section('content')
    <div class="container-fluid account-page">
        <div class="dashboard-content">
            <div class="wrapper sub-flex">
                <div class="general-content-area" id="app" v-cloak>
                    @if($path == "aud")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw AUD, simply enter the amount you'd like to withdraw. The funds will be sent to the approved bank account you registered.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/aud/go">
                                {{ csrf_field() }}
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total AUD balance:</label>
                                    <div class="amount">{{ formatted_user_balance() }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" v-model="amount_aud" id="amount" name="amount" value="{{old('amount')}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                        <div class="form-group">
                                            <label for="phone">TFA Code:</label>
                                            <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                        </div>
                                    @endif
                                </div>
                                <p class="fee">Minimum withdraw is $10 AUD.</p>
                                <button type="button" @click.prevent="maxAmount()" class="btn btn-red">Max</button>
                                <br><br>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>
                    @elseif($path == "nzd")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw NZD, simply enter the amount you'd like to withdraw. The funds will be sent to the approved bank account you registered.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/nzd/go">
                                {{ csrf_field() }}
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total NZD balance:</label>
                                    <div class="amount">{{ formatted_user_balance('nzd') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" v-model="amount_aud" id="amount" name="amount" value="{{old('amount')}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                        <div class="form-group">
                                            <label for="phone">TFA Code:</label>
                                            <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                        </div>
                                    @endif
                                </div>
                                <p class="fee">Minimum withdraw is $10 NZD.</p>
                                <button type="button" @click.prevent="maxAmount()" class="btn btn-red">Max</button>
                                <br><br>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>
                    @elseif($path == "usd")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw USD, simply enter the amount you'd like to withdraw. The funds will be sent to the approved bank account you registered.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/usd/go">
                                {{ csrf_field() }}
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total USD balance:</label>
                                    <div class="amount">{{ formatted_user_balance('usd') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" v-model="amount_aud" id="amount" name="amount" value="{{old('amount')}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                        <div class="form-group">
                                            <label for="phone">TFA Code:</label>
                                            <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                        </div>
                                    @endif
                                </div>
                                <p class="fee">Minimum withdraw is $10 USD.</p>
                                <button type="button" @click.prevent="maxAmount()" class="btn btn-red">Max</button>
                                <br><br>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>
                    @elseif($path == "btc")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw BTC, enter your external wallet address (<b>NOT</b> your myCryptoWallet address) and the amount you'd like to withdraw.<br><br>Please note, the minimum withdrawal is 0.01 Bitcoin and your maximum is {{$withdrawLimit}} Bitcoin.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/btc/go">
                                {{ csrf_field() }}
                                <input type="hidden" name="convertion_rate" value="{{$price['btc']->bid}}">
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total BTC balance:</label>
                                    <div class="amount">{{ formatted_user_balance('btc') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="name">External BTC Wallet Address:</label>
                                                <input type="text" class="form-control" id="external" name="external" value="{{old('external')}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="{{old('amount')}}" required>
                                            </div>
                                            @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                                <div class="form-group">
                                                    <label for="phone">TFA Code:</label>
                                                    <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="fee">+0.00011 withdraw fee taken from above</p>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>


                    @elseif($path == "eth")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw ETH, enter your external wallet address (<b>NOT</b> your myCryptoWallet address) and the amount you'd like to withdraw.

                                    <br><br><b>Do not withdraw Ethereum to a token sale</b> as your tokens will be lost.<br><br>Please note, the minimum withdrawal is 0.05 Ethereum and your maximum is {{$withdrawLimit}} Ethereum.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/eth/go">
                                {{ csrf_field() }}
                                <input type="hidden" name="convertion_rate" value="{{$price['eth']->bid}}">
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total ETH balance:</label>
                                    <div class="amount">{{ formatted_user_balance('eth') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="name">External ETH Wallet Address:</label>
                                                <input type="text" class="form-control" id="external" name="external" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="" required>
                                            </div>
                                            @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                                <div class="form-group">
                                                    <label for="phone">TFA Code:</label>
                                                    <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="fee">+0.005 withdraw fee</p>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>


                    @elseif($path == "ltc")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw LTC, enter your external LTC wallet address (<b>NOT</b> your myCryptoWallet address) and the amount you'd like to withdraw.<br><br>Please note, the minimum withdrawal is 0.1 LTC and your maximum is {{$withdrawLimit}} Litecoin.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/ltc/go">
                                {{ csrf_field() }}
                                <input type="hidden" name="convertion_rate" value="{{$price['ltc']->bid}}">
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total LTC balance:</label>
                                    <div class="amount">{{ formatted_user_balance('ltc') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="name">External LTC Wallet Address:</label>
                                                <input type="text" class="form-control" id="external" name="external" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="" required>
                                            </div>
                                            @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                                <div class="form-group">
                                                    <label for="phone">TFA Code:</label>
                                                    <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="fee">+0.011 withdraw fee</p>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>


                    @elseif($path == "xrp")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw XRP, enter your external wallet address (<b>NOT</b> your myCryptoWallet address) and the amount you'd like to withdraw.<br><br>Please note, the minimum withdrawal is 10 Ripple. <b>Sending to a NEW Ripple Address?</b> You <b>MUST</b> send more than 30 Ripple or it will fail - Ripple developers have mandated a 20~ Ripple cost to create a new Ripple address.</h4>
                                <br>Please note, the minimum withdrawal is 10 Ripple and your maximum is {{$withdrawLimit}} Ripple.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/xrp/go">
                                {{ csrf_field() }}
                                <input type="hidden" name="convertion_rate" value="{{$price['xrp']->bid}}">
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total XRP balance:</label>
                                    <div class="amount">{{ formatted_user_balance('xrp') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="name">External XRP Wallet Address:</label>
                                                <input type="text" class="form-control" id="external" name="external" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="" required>
                                            </div>
                                            @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                                <div class="form-group">
                                                    <label for="phone">TFA Code:</label>
                                                    <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="fee">+{{getTxFees('xrp')}} withdraw fee</p>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>
                        @elseif($path == "pwr")
                        <div class="panel-body">
                            <div class="top-note">
                                <h4>To withdraw POWR, enter your external Ethereum wallet address (<b>NOT</b> your myCryptoWallet address) and the amount you'd like to withdraw.<br><br>Please note, the minimum withdrawal is 50 POWR.</h4>
                                <br>Please note, the maximum withdrawal is {{$withdrawLimit}} POWR.</h4>
                            </div>
                            <form role="form" method="POST" action="/wallets/withdraw/pwr/go">
                                {{ csrf_field() }}
                                <input type="hidden" name="convertion_rate" value="{{$price['pwr']->bid}}">
                                <div class="form-group balance">
                                    <label for="name" class="heading">Total POWR balance:</label>
                                    <div class="amount">{{ formatted_user_balance('pwr') }}</div>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sx-12">
                                            <div class="form-group">
                                                <label for="name">External ETH Wallet Address:</label>
                                                <input type="text" class="form-control" id="external" name="external" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Amount to withdraw:</label>
                                                <input type="text" class="form-control" id="amount" name="amount" value="" required>
                                            </div>
                                            @if(\Auth::user()->get_setting('withdraw_tfa', 0))
                                                <div class="form-group">
                                                    <label for="phone">TFA Code:</label>
                                                    <input type="text" class="form-control" id="tfa" name="tfa" value="" required>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="fee">+15 POWR withdraw fee</p>
                                <p class="fee" style="font-size: 12px;">We apologise for the high withdrawal fee, however this is unavoidable due to a combination of our source costs, token transfer costs and the complexity of tokens. This will be reduced in the future.</p>
                                <button type="submit" class="btn btn-teal">Withdraw</button>
                                @if (count($errors))
                                    <ul class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </form>
                        </div>
                    @endif
                </div>
                @include('partials.mywallet')
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                selected_currency: "{{ $coin ?: \Auth::user()->get_setting('default_currency', 'btc') }}",
                amount_aud: "{{ old('amount_aud', 0) }}",
                aud_limit: {{ \App\Models\UserLimits::get_limit('buy_transaction_limit') }},
                amount: {
                    btc: 0,
                    ltc: 0,
                    eth: 0,
                    xrp: 0,
                    pwr: 0
                },
                ask: {
                    btc: "{{ $price['btc']->ask * (1+(setting('public_fee') / 100)) }}",
                    ltc: "{{ $price['ltc']->ask * (1+(setting('public_fee') / 100)) }}",
                    eth: "{{ $price['eth']->ask * (1+(setting('public_fee') / 100)) }}",
                    xrp: "{{ $price['xrp']->ask * (1+(setting('public_fee') / 100)) }}",
                    pwr: "{{ $price['pwr']->ask * (1+(setting('public_fee') / 100)) }}"
                },
                miner_fee: {
                    btc: 0.001,
                    ltc: 0.01,
                    eth: 0.006,
                    xrp: 1.0,
                    pwr: 15
                },
                wallet: {
                    aud: "{{ number_format($funds['aud']['0']->balance, 6, '.', '') }}",
                    btc: "{{ number_format($funds['btc']['0']->balance, 6, '.', '') }}",
                    ltc: "{{ number_format($funds['ltc']['0']->balance, 6, '.', '') }}",
                    eth: "{{ number_format($funds['eth']['0']->balance, 6, '.', '') }}",
                    xrp: "{{ number_format($funds['xrp']['0']->balance, 6, '.', '') }}",
                    pwr: "{{ number_format($funds['pwr']['0']->balance, 6, '.', '') }}"
                }
            },
            methods: {
                updateAud: function () {
                    this.amount_aud = this.amount_aud.replace(/[^0-9\.]+/g,'');
                    this.amount[this.selected_currency] = parseFloat( (this.amount_aud / this.ask[this.selected_currency]) + this.miner_fee[this.selected_currency] ).toFixed(6);
                },
                maxAmount: function () {
                    if(this.wallet.aud > 0)
                    {
                        this.amount_aud = parseFloat((this.wallet.aud > this.aud_limit) ? this.aud_limit : this.wallet.aud).toFixed(2);
                        this.updateAud()
                    }
                    else
                    {
                        this.amount[this.selected_currency] = 0;
                        this.amount_aud = 0;
                    }
                }
            }
        })
    </script>
@endpush
