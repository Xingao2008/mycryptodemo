@extends('layouts.private')

@section('page-heading', 'Verify SMS')
@section('title', 'Verify SMS')
@section('page-sub-heading', '')

@section('content')
    <div class="general-content-area">
                  <div class="top-note">
                    <h4>Congratulations!</h4>
                  </div>
                  <div class="body">
                    @if ($verification->sms == 'approved')
                        You've successfully verified your mobile!
                    @else
                        @if (!isset(Auth::user()->phone))
                          You haven't set a mobile number yet! Go <a href="/account">here</a> to set one.
                        @else
                          @if (isset($sms_verified))
                            Your mobile number is verified!
                          @else
                            <form action="/account/verify_sms" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="sms" value="send" />
                                <br />
                                    Your mobile number is not yet verified.<br>
                                <br /><br />
                                <input type="submit" value="Send SMS to {{Auth::user()->phone}}" />
                            </form>
                          @endif
                        @endif
                        @if (isset($show_form))
                            <form action="/account/verify_sms" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="sms" value="verify" />
                                <br />
                                    Please enter the code you received:<br>

                                    <input type="text" name="code" value="">
                                <br /><br />
                                <input type="submit" value="Submit Code" />
                            </form>
                        @endif
                    @endif
                  </div>
                </div>
@endsection
