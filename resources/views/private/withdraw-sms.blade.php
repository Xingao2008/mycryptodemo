@extends('layouts.private')

@section('title', 'Authorize Withdraw Request')
@section('page-heading', 'Authorize Withdraw Request')

@section('content')
<div class="container dashboard withdraw-deposit">
    <div class="container-fluid account-page">

        <div class="dashboard-content">
            <div class="wrapper sub-flex">
                <div class="general-content-area">
                  <div class="top-note">
                    <h4>Additional actions required</h4>
                  </div>
                  <div class="body">

                    @if ($status != 'started')
                        You've successfully verified your mobile!
                    @else
                        @if (!isset(Auth::user()->phone))
                          You haven't set a mobile number yet! Go <a href="/account">here</a> to set one.
                        @else
                          @if (isset($sms_verified))
                            Your withdrawal request is verified! It will be processed shortly.
                          @else
                            <form action="/wallets/verify/withdraw/{{$path}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="sms" value="send" />
                                <input type="hidden" name="withdraw_id" value="{{$path}}" />
                                <br />
                                    Your withdrawal request is not yet authorised. Please confirm it via SMS.<br>
                                <br /><br />
                                <input type="submit" value="Send SMS to {{Auth::user()->phone}}" />
                            </form>
                          @endif
                        @endif
                        @if (isset($show_form))
                            <form action="/wallets/verify/withdraw/{{$path}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="sms" value="verify" />
                                <input type="hidden" name="withdraw_id" value="{{$path}}" />
                                <br />
                                    Please enter the code you received:<br>

                                    <input type="text" name="code" value="">
                                <br /><br />
                                <input type="submit" value="Submit Code" />
                            </form>
                        @endif
                    @endif
                  </div>
                </div>
                <div class="my-wallet">
                    <div class="top">
                        <div class="header">
                            <h4>myWallet</h4>
                        </div>
                        <div class="deposit">
                            <div>${{$coin_balances['aud']}} AUD</div>
                        </div>
                    </div>
                    <div class="bottom">
                        <div class="coin-feed">
                            <div class="single-coin">
                                <div class="wrap">
                                    <div class="header">
                                        <span class="icon-btc"></span>
                                        <div>
                                            <div class="price">{{$coin_balances['btc']}} BTC</div>
                                            <div class="balance">${{@audFormat(latest_price('btc') * $coin_balances['btc'])}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-coin">
                                <div class="wrap">
                                    <div class="header">
                                        <span class="icon-eth"></span>
                                        <div>
                                            <div class="price">{{$coin_balances['eth']}} ETH</div>
                                            <div class="balance">${{@audFormat(latest_price('eth') * $coin_balances['eth'])}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-coin">
                                <div class="wrap">
                                    <div class="header">
                                        <span class="icon-ltc"></span>
                                        <div>
                                            <div class="price">{{$coin_balances['ltc']}} LTC</div>
                                            <div class="balance">${{@audFormat(latest_price('ltc') * $coin_balances['ltc'])}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single-coin">
                                <div class="wrap">
                                    <div class="header">
                                        <span class="icon-rpl"></span>
                                        <div>
                                            <div class="price">{{$coin_balances['xrp']}} XRP</div>
                                            <div class="balance">${{@audFormat(latest_price('xrp') * $coin_balances['xrp'])}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

@endsection
