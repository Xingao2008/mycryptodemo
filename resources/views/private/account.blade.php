@extends('layouts.private')

@section('page-heading', 'Account')
@section('title', 'Account')

@section('breadcrumbs', Breadcrumbs::render('account'))

@section('content')
<div class="wrapper flex-wrap">
<div class="small-container">
    <div class="col-md-12">
        <section class="content account-page">
            <div class="panel-body">
                @if (session()->has('status'))
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert">×</a>
                        <strong>Success!</strong> {{ session()->get('status') }}<br>
                        click <a href="/dashboard">here</a> to continue to myCryptowallet
                    </div>
                @endif
                <div class="columns buttons-wrapper">
                    <a href="{{route('bank')}}" class="column btn btn-ghost-inverse">Bank Details</a>
                    <a href="{{route('tfa')}}" class="column btn btn-ghost-inverse">Two-factor Auth</a>
                    <a href="{{route('settings')}}" class="column btn btn-ghost-inverse">Settings</a>
                    <a href="{{route('password')}}" class="column btn btn-ghost-inverse">Password</a><br><br>
                </div>
                <form id="account_profile_form" role="form" method="POST" action="/account">
                    {{ csrf_field() }}
                    <div class="form-group general-container">
                        <label for="name">Unique Bank Code (UBC)</label>
                        {{ Auth::user()->ubc }}
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-sx-12">
                                <div class="form-group">
                                    <label for="name">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{Auth::user()->first_name}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="name">Middle Name (optional)</label>
                                    <input type="text" class="form-control" id="middle_name" name="middle_name" value="{{Auth::user()->middle_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{Auth::user()->last_name}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Mobile</label>
                                    <select style="width: 18%; display: inline-block; margin-right: 11px;" name="country_calling_code">
                                    <option value="">Country code</option>
                                    @foreach ($countries as $c)
                                        <option {{ (isset(Auth::user()->country_calling_code) && Auth::user()->country_calling_code == $c->calling_code ? 'selected="selected"' : '')}}
                                            value="{{$c->calling_code}}">+{{$c->calling_code}}</option>
                                    @endforeach
                                    </select>
                                    <input style="width: 80%; display: inline-block;" type="text" class="form-control" id="phone" name="phone" value="{{Auth::user()->phone}}" >
                                </div>
                                <div class="form-group">
                                    <label for="dob">Date of Birth</label>
                                    <input placeholder="DD/MM/YYYY" type="text" class="form-control" id="datepicker" name="dob" value="{{Auth::user()->dob}}" >
                                </div>
                            </div>
                            <div class="col-md-6 col-sx-12">
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <label for="address">Street</label>
                                    <input type="text" class="form-control" id="street" name="street" value="{{@Auth::user()->address->street}}" >

                                    <label for="address">City</label>
                                    <input type="text" class="form-control" id="city" name="city" value="{{@Auth::user()->address->city}}" >

                                    @if (isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == 36)
                                        <label for="address">State</label>
                                        <select id="state" class="form-control" name="state" id="state" autocomplete="off">
                                            <?php if(isset(Auth::user()->address->state)) {$state = Auth::user()->address->state;} else {$state = "";} ?>
                                            <option> -- Please select -- </option>    
                                            @foreach(get_states_for_country(Auth::user()->address->country_id) as $key => $longForm)
                                            <option value='{{$key}}' 
                                            @if($state == $key) {
                                                selected="selected"
                                            @endif
                                                >{{$key}}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <label for="address">State</label>
                                        <input type="text" class="form-control" id="state" name="state" value="{{@Auth::user()->address->state}}" >
                                    @endif

                                    @if (isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == 36)
                                        <label for="address">Post Code</label>
                                    @else
                                        <label for="address">Zip Code</label>
                                    @endif

                                    <input type="text" class="form-control" id="post_code" name="post_code" value="{{@Auth::user()->address->post_code}}" >


                                    <label for="address">Country</label>
                                    <select name="country_id" style="width: 100%;" autocomplete="off">
                                        @foreach ($countries as $country)
                                            <option value="{{$country->id}}"

                                                    @if(isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == $country->id)
                                                    selected="selected"
                                                    @elseif (empty(Auth::user()->address->country_id) && $country->id == 36)
                                                    selected="selected"
                                                    @endif
                                            >{{$country->name}}</option>

                                        @endforeach
                                    </select>
                                    <div class="form-group">
                                        <label for="dob">ABN/ACN if relevant (optional)</label>
                                        <input type="text" class="form-control" id="abn" name="abn" value="{{Auth::user()->abn}}" >
                                    </div>
                                    @if(isset(Auth::user()->address->country_id) && (Auth::user()->address->country_id == 840 || Auth::user()->address->country_id == 124))
                                     <div class="form-group">
                                        @if(@Auth::user()->address->country_id == 840)
                                        <label for="GBGIdentityID">Social Security Number (optional)</label>
                                        
                                        @elseif(@Auth::user()->address->country_id == 124)
                                        <label for="GBGIdentityID">SocialInsuranceNumber (optional)</label>
                                        @endif
                                        <input type="text" class="form-control" id="GBGIdentityID" name="GBGIdentityID" value="{{Auth::user()->verification->GBGIdentityID}}" >
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="center-content">
                        <button type="submit" class="btn btn-teal">Update</button>
                    </div>


                </form>
            </div>
        </section>
    </div>
</div>
</div>
@endsection

