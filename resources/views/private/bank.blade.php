@extends('layouts.private')

@section('page-heading', 'Bank Details')

@section('title', 'Bank Details')

@section('content')

    <div class="wrapper">
        {{--@if(isset($bank->bank_name))--}}
            <form role="form" method="POST" action="/account/bank" class="small-container">
                {{ csrf_field() }}
                <p>Please note, support will review these details as soon as possible. <b>You can only submit this form once</b> so please check over the details carefully.</p>
                <div class="form-group">
                    <label for="name">Account Name Must Match: <strong>{{Auth::user()->name}}</strong></label>
                </div>
                <div style="height: 30px;"></div>
                <div class="form-group">
                    <label for="bank_name">Bank Name*</label>
                    <input type="text" class="form-control" id="bank_name" name="bank_name" value="{{$bank->bank_name or '' }}" >
                </div>

                <div class="form-group">
                    <label for="bank_branch">Branch</label>
                    <input type="text" class="form-control" id="bank_branch" name="bank_branch" value="{{$bank->bank_branch or '' }}" >
                    <p>Leave blank if unsure</p>
                </div>

                    <div class="form-group" 
                    @if(isset(Auth::user()->address->country_id) && Auth::user()->address->country_id != 36)
                        style="display:none"
                    @endif
                    >
                        <label for="bank_bsb">BSB*</label>
                        <input type="text" class="form-control" id="bank_bsb" name="bank_bsb" value="{{$bank->bank_bsb or '' }}" maxlength="6">
                        <p>Only use numbers (e.g. 123456)</p>
                    </div>


                <div class="form-group">
                    <label for="bank_number">Account Number*
                    </label>
                    <input type="text" class="form-control" id="bank_number" name="bank_number" value="{{$bank->bank_number or '' }}" 
                    @if(isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == 554)
                        maxlength="16"
                    @else
                        maxlength="10"
                    @endif
                    >
                    @if(isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == 554)
                        <p>Only use numbers (e.g. 0000001111111112)</p>
                    @else
                        <p>Only use numbers (e.g. 1212121212)</p>
                    @endif
                </div>

                <div class="center-content">
                    <p style="text-align:left;"><b>PLEASE NOTE:</b> After you submit this, you will <b>NOT</b> be able to change it. If these details change, you’ll need to email support@mycryptowallet.com.au with the new details.</p>
                    <br>
                    <button type="submit" class="btn btn-teal">Update</button>
                </div>
            </form>
        {{--@else--}}
            {{--<div class="form-group">--}}
                {{--<label for="name"><strong>Account Name:</strong> {{Auth::user()->name}}</label>--}}
            {{--</div>--}}
            {{--<div style="height: 30px;"></div>--}}
            {{--<div class="form-group">--}}
                {{--<label for="bank_name"><strong>Bank Name: </strong></label>--}}
                {{--{{$bank->bank_name or '' }}--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="bank_branch"><strong>Branch: </strong></label>--}}
                {{--{{$bank->bank_branch or '' }}--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="bank_bsb"><strong>BSB: </strong></label>--}}
                {{--{{$bank->bank_bsb or '' }}--}}
            {{--</div>--}}

            {{--<div class="form-group">--}}
                {{--<label for="bank_number"><strong>Account Number: </strong></label>--}}
                {{--{{$bank->bank_number or '' }}--}}
            {{--</div>--}}

            {{--<p>To change these details, you’ll need to email support@mycryptowallet.com.au</p>--}}
        {{--@endif--}}
    </div>

@endsection
