@extends('layouts.app')

@section('content')


    <div class="container dashboard">
      <div class="main">
          <div class="page-heading with-breadcrumb">
            <h2>Marketplace</h2>
              <!-- <div class="breadcrumb">
              <a href="/">Home</a> <span>Marketplace</span>
            </div> -->
          </div>
      <div class="dashboard-content">
        <div class="wrapper">

          <div class="market-graph">
            <div class="header">
              <div class="controls">
                <div class="single-control"><div class="label">Current market prices</div></div>
                <div class="single-control">
                  <div class="toggle">
                    <span>
                      <img src="/img/bitcoin-icon.png">
                    </span>
                    <div class="name">AUD->BTC</div>
                  </div>
                </div>
                <div class="single-control">
                  <div class="standing">
                    <span>Your AUD: </span>
                    <span class="number">$20.34</span>
                  </div>
                </div>
                <div class="single-control">
                  <div class="standing">
                    <span>Your BTC: </span>
                    <span class="number">$31.49689</span>
                  </div>
                </div>
              </div>
              <div class="buy-sell-toggle">
                <div class="single-toggle active">Buy BTC</div>
                <div class="single-toggle">Sell BTC</div>
              </div>
            </div>
            <div class="single-graph">
              <div>
                <canvas id="myChart"></canvas>
                <script>
                  var ctx = document.getElementById('myChart').getContext("2d");

                  var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
                  gradientStroke.addColorStop(0, '#2ccfb7');

                  gradientStroke.addColorStop(1, '#00b5fe');

                  var myChart = new Chart(ctx, {
                      type: 'line',
                      data: {
                          labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
                          datasets: [{
                              label: "Data",
                              borderColor: gradientStroke,
                              pointBorderColor: gradientStroke,
                              pointBackgroundColor: gradientStroke,
                              pointHoverBackgroundColor: gradientStroke,
                              pointHoverBorderColor: gradientStroke,
                              pointBorderWidth: 1,
                              pointHoverRadius: 10,
                              pointHoverBorderWidth: 1,
                              pointRadius: 3,
                              fill: false,
                              borderWidth: 4,
                              data: [100, 120, 150, 170, 180, 170, 160]
                          }]
                      },
                      options: {
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      fontColor: "rgba(0,0,0,0.5)",
                                      fontStyle: "bold",
                                      beginAtZero: true,
                                      maxTicksLimit: 5,
                                      padding: 20
                                  },
                              }],
                              xAxes: [{
                                  gridLines: {
                                      display: false
                                  },
                                  ticks: {
                                      padding: 20,
                                      fontColor: "rgba(0,0,0,0.5)",
                                      fontStyle: "bold"
                                  }
                              }]
                          }
                      }
                  });
                </script>
              </div>
            </div>
          </div>

          <div class="purchase-module">
            <div class="header">
              <div class="coin-toggle">
                <div class="single-toggle">Buy BTC <span class="icon-caret"><img src="/img/carret.svg"></span></div>
              </div>
              <div class="menu">
                <a href="#">View myWallet</a>
              </div>
            </div>
            <div class="purchase-form">
              <div class="single-input">
                <div class="image"><div class="icon-btc"></div></div>
                <div class="input">
                  <input type="text" placeholder="">
                  <div class="label">Bitcoin</div>
                </div>
              </div>
              <div class="single-input">
                <div class="image"><img src="/img/multipel.svg"></div>
                <div class="input">
                  <input type="text" placeholder="">
                  <div class="label">$230 AUD / BTC</div>
                </div>
              </div>
              <div class="single-input total">
                <div class="image"><img src="/img/equal.svg"></div>
                <div class="input">
                  <input type="text" placeholder="">
                  <div class="label">Total AUD</div>
                </div>
              </div>
              <div class="single-input total">
                <div class="image"><img src="/img/minus.svg"></div>
                <div class="input">
                  <input type="text" placeholder="">
                  <div class="label">1% surcharge</div>
                </div>
              </div>
              <div class="single-input total">
                <a href="#" class="btn btn-grey">Place order</a>
              </div>
            </div>
            <div class="bottom">
              <div class="checkbox-group">
                <input type="checkbox">
                <div class="label">Buy at best market rate</div>
              </div>
            </div>
          </div>

          <div class="purchase-history">
            <div class="orders">
              <div class="purchase-channel">
                <h4>Purchase orders</h4>
                <div class="group">
                  <div class="single-order">
                    <div class="icon"><div class="icon-btc"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 BTC</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-ltc"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 LTC</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-eth"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 ETH</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-rpl"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 XRP</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                </div>
              </div>
              <div class="purchase-channel">
                <h4 class="title">Sale orders</h4>
                <div class="group">
                  <div class="single-order">
                    <div class="icon"><div class="icon-btc"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 BTC</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-ltc"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 LTC</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-eth"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 ETH</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                  <div class="single-order">
                    <div class="icon"><div class="icon-rpl"></div></div>
                    <div class="price">
                      <div class="price-top">5.234 XRP</div>
                      <div class="price-bottom">$11,250 AUD ($2,250 / BTC)</div>
                    </div>
                    <div class="view"><a href="#">View</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="history">
              <div class="header">
                <h4 class="title">Recent history</h4>
                <div class="view-all">
                  <a href="#">View all</a>
                </div>
              </div>
              <div class="history-feed">
                <table class="mobile-table">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Digital Currency</th>
                      <th>Order Total</th>
                      <th>AUD / BTC</th>
                    </tr>
                  </thead>
                  <tr>
                    <td>7 Jun 2016</td>
                    <td>04:00 UTC</td>
                    <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                    <td>$11,250 AUD</td>
                    <td>$2,250 / BTC</td>
                  </tr>
                  <tr>
                    <td>7 Jun 2016</td>
                    <td>04:00 UTC</td>
                    <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                    <td>$11,250 AUD</td>
                    <td>$2,250 / BTC</td>
                  </tr>
                  <tr>
                    <td>7 Jun 2016</td>
                    <td>04:00 UTC</td>
                    <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                    <td>$11,250 AUD</td>
                    <td>$2,250 / BTC</td>
                  </tr>
                  <tr>
                    <td>7 Jun 2016</td>
                    <td>04:00 UTC</td>
                    <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                    <td>$11,250 AUD</td>
                    <td>$2,250 / BTC</td>
                  </tr>
                  <tr>
                    <td>7 Jun 2016</td>
                    <td>04:00 UTC</td>
                    <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                    <td>$11,250 AUD</td>
                  <td>$2,250 / BTC</td>
                </tr>
                <tr>
                  <td>7 Jun 2016</td>
                  <td>04:00 UTC</td>
                  <td><img src="/img/bitcoin-icon.png"><span>5.234 BTC</span></td>
                  <td>$11,250 AUD</td>
                  <td>$2,250 / BTC</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
        </div>
      </div>

    </div>
</div>




@endsection
