@extends('layouts.private')

@section('page-heading', 'Buy')
@section('title', 'Buy')
@section('page-sub-heading', '')
@section('container', 'withdraw-deposit')
@section('content')

        <div class="dashboard-content" >
            <div class="wrapper sub-flex">
                <div class="general-content-area" id="app" v-cloak>
                  <div class="top-note">
                    <h4 id="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>Your available AUD balance is <b>${{$coin_balances['aud']}}</b>.<br>The minimum purchase is $50, with a maximum of @{{ aud_limit | currency }} per day.<br>Do not enter a dollar sign ($) in the text box.</h4>
                    <h4 id="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>Your available NZD balance is <b>${{$coin_balances['nzd']}}</b>.<br>The minimum purchase is $50, with a maximum of @{{ aud_limit | currency }} per day.<br>Do not enter a dollar sign ($) in the text box.</h4>
                    <h4 id="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>Your available USD balance is <b>${{$coin_balances['usd']}}</b>.<br>The minimum purchase is $50, with a maximum of @{{ aud_limit | currency }} per day.<br>Do not enter a dollar sign ($) in the text box.</h4>
                  </div>
                  <form role="form" method="POST" action="/buy/process" onsubmit="myButton.disabled = true; return true;">
                  {{ csrf_field() }}
                    @if (count($errors))
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                  <div class="row currency-buttons">
                          <a href="#" :class="getClass(currency)" @click.prevent="changeCurrency(currency)" v-for="currency in currencies"><i :class="icons[currency]"></i> @{{ currency.toUpperCase() }}</a>
                  </div>
                  <input type="hidden" name="currency" :value="selected_currency" />
                  <input type="hidden" id="selected_fiat" name="fiat_currency" value="{{ Auth::user()->get_setting('default_fiat_currency', 'aud')}}" />
                  <div class="form-group">
                    <label>Buy @{{ selected_currency.toUpperCase() }}</label>
                    <div class="row">
                        <span style="display:inline;">$</span>
                        <input :style="audTextBox" id="amount" name="amount" type="text" v-model="amount_aud" @keyup="updateAud()" />
                        <div class="help-block" v-if="error_message">@{{ error_message }}</div>
                    </div>
                      <div class="buysell-buttons">
                          <a href="#" @click.prevent="setAmount('50')" class="btn btn-blue">$50</a>
                          <a href="#" @click.prevent="setAmount('100')" class="btn btn-blue">$100</a>
                          <a href="#" @click.prevent="setAmount('250')" class="btn btn-blue">$250</a>
                          <a href="#" @click.prevent="maxAmount()" class="btn btn-blue">MAX</a>
                      </div>
                      <span>- OR -</span>
                      <div v-if="selected_currency == currency" v-for="currency in currencies">

                          <div>
                              <span :class="icons[currency]"></span>
                              <input style="display:inline;" :id="'amount_' + currency" :name="'amount_' + currency" type="text" v-model="amount[currency]" @keyup="updateCurrency(currency)" />
                          </div>

                          <p :id="currency + '_fee'" :class="currency + '_fee fee'">
                            <span style="font-size:12px;"> <br>
                                Exchange fee @{{ site_fee | currency  }} ({{ setting('public_fee') }}% site fee) & @{{ currency.toUpperCase() }} )
                            </span>
                          </p>

                      </div>


                  </div>
                  <!--<p class="fee">+{{setting('public_fee')}}% fee</p>-->
                  <a type="submit" href="#" v-if="!confirmButton"  :disabled="!validateFunds" @click.prevent="confirmDiag()" class="btn btn-teal">Buy</a>
                  <div v-if="confirmButton">
                      <p class="notification is-danger" style="margin:0 6% 10px;">Are you sure you want to buy $@{{ amount_aud }} worth of @{{ selected_currency.toUpperCase() }}?</p>
                      <button type="submit" class="btn btn-teal" name="myButton">Yes</button>
                      <a href="#" type="submit" class="btn btn-red" @click.prevent="cancelDiag()">No</a>
                  </div>
                  </form>
                  <div class="sub-note">
                    <p class="fee">Please note that prices are updated every minute, you must place and confirm your order within 60 seconds of the last update.</p>
                  </div>
                </div>
                @include('partials.mywallet')
            </div>
        </div>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                selected_currency: "{{ $coin ?: \Auth::user()->get_setting('default_currency', 'btc') }}",
                amount_aud: "{{ old('amount_aud', 0) }}",
                aud_limit: {{ \App\Models\UserLimits::get_limit('buy_transaction_limit') }},
                amount: {
                    @foreach(config('currencies.crypto') as $crypto)
                        {{$crypto}}: 0,
                    @endforeach
                },
                wallet: {
                    @foreach(config('currencies.allAvailableCurrencies') as $cur)
                        {{$cur}}: "{{ number_format(($funds[$cur]['0']->balance??0), 6, '.', '') }}",
                    @endforeach
                },
                ask: {
                    @foreach(config('currencies.crypto') as $crypto)
                        {{$crypto}}: "{{ $price[$crypto]->ask }}", //already applied the fee in price
                    @endforeach
                },
                currencies: [
                    @foreach(config('currencies.crypto') as $crypto)
                        '{{$crypto}}',
                    @endforeach

                ],
                icons: {
                    @foreach(config('currencies.crypto') as $crypto)
                        {{$crypto}}: 'icon-{{$crypto}}',
                    @endforeach
                },
                confirmButton: false,
                error_message: ''
            },
            methods: {
                updateAud: function () {
                    this.amount_aud = this.amount_aud.replace(/[^0-9\.]+/g,'');
                    this.amount[this.selected_currency] = parseFloat( this.amount_aud / this.ask[this.selected_currency] ).toFixed(6);
                },

                updateCurrency: function(currency)
                {
                    this.amount_aud = this.ask[currency] * this.amount[currency];
                },

                confirmDiag: function () {
                    this.confirmButton = (this.validateFunds)
                },

                cancelDiag: function () {
                    this.confirmButton = false
                },

                maxAmount: function () {
                    if(this.wallet.aud > 0)
                    {
                        this.amount_aud = parseFloat((this.wallet.aud > this.aud_limit) ? this.aud_limit : this.wallet.aud).toFixed(2);
                        this.updateAud()
                    }
                    else
                    {
                        this.amount[this.selected_currency] = 0;
                        this.amount_aud = 0;
                    }
                },

                getMax: function(currency)
                {
                    return this.aud_limit
                },

                setAmount: function (amount) {
                    this.amount_aud = amount
                    this.updateAud()
                },

                getClass: function(currency) {
                    if(currency == this.selected_currency)
                    {
                        return ['btn', 'btn-teal'];
                    }
                    else
                    {
                        return ['btn', 'btn-ghost'];
                    }
                },

                changeCurrency: function(currency) {
                    this.selected_currency = currency;
                }
            },
            watch: {
                selected_currency: function (val) {
                    for(currency in this.currencies)
                    {
                        if(currency == val)
                        {
                            this.amount[currency] = this.amount_aud / this.ask[currency]
                        }
                        else
                        {
                            this.amount[currency] = 0
                        }
                    }
                }
            },
            computed: {
                calc: function() {
                    if(this.amount[this.selected_currency] == 0)
                    {
                        return 0;
                    }

                    return parseFloat(this.amount[this.selected_currency]) - (this.amount[this.selected_currency] * this.site_fee);
                },

                site_fee: function () {
                    return parseFloat("{{ setting('public_fee') }}" / 100) * this.amount_aud;
                },

                audTextBox: function () {
                    return {
                        display: 'inline',
                        'border-color': this.validateFunds ? '' : 'red'
                    };

                },
                validateFunds: function () {
                    if (this.amount_aud == 0) {
                        this.error_message = "Amount can't be 0";
                        return false;
                    }

                    if (this.amount_aud < 50) {
                        this.error_message = "Amount must be more than $50";
                        return false;
                    }

                    if (this.amount_aud > this.aud_limit)
                    {
                        this.error_message = "Amount cannot be more than $" + this.aud_limit;
                        return false;
                    }

                    if(parseFloat(this.wallet.aud) >= parseFloat(this.amount_aud)){
                        this.error_message = "";
                        return true;
                    }
                    else
                    {
                        this.error_message = "Amount can't be more than your wallet";
                        return false;
                    }
                }
            },
            filters: {
                currency: function(val){
                    return accounting.formatMoney(val)
                }
            }
        })
    </script>
@endpush
