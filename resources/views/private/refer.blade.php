@extends('layouts.private')

@section('page-heading', 'Referrals')
@section('title', 'Referrals')

@section('content')
        <div class="dashboard-content">
          <div class="wallet-items wrapper">
            <p>myCryptoWallet now offers rewards for referring your friends! The <b>high earning</b> potential and <b>unlimited lifetime commissions</b> even makes it possible to do this full-time and refer strangers via ads, too.</p>

            <p>It's super-simple. We pay you 30% of all fees we earn. You simply share your unique link, which stores a "cookie" or "unique ID" in the person's browser for 60 days, which tells us you referred them. Once they sign up, their account is linked to yours permanently, and you earn a commission from all their activity.</p>
            <p>You'll get 30% of the following:</p>
            <ul>
              <li>2.8% instant buy/sell fee</li>
              <li>~1% Exchange Fee (when launched)</li>
              <li>2% any-to-any swap fee (when launched)</li>
            </ul>

            <p>For illustration, a user's monthly buy volume of $10,000 with a 2.8% fee, depending on miner fees and other variables, would result in $280~. If a user bought <i>and</i> sold that quantity in one month, and you had referred 10 users doing this, it would result in $5,600~ per month. You're charged no fees on your income, there's no withdrawal cost, and you can use this income however you like.</p>

            <p>Commissions are payable <b>immediately</b> when the month completes; simply go to this page and click Release Payment when it's ready. You can immediately withdraw, or, use it to buy crypto!</p>
            <br>
            <h2>Unique invite link</h2>
            <p><b>Your unique invite link is: <a href="https://mycryptowallet.com.au/refer/id/<?php print Auth::user()->id + 100000; ?>">https://mycryptowallet.com.au/refer/id/<?php print Auth::user()->id + 100000; ?></a></b></p>
            <p>You can share this on Facebook, forums, in your signature, anywhere you like.</p>
            <br>
            <h2>Earnings</h2>
            <p>All referred users (whether they transacted or not) and earnings per user are listed below.</p>
            <p><b>Note:</b> "Past month" is counted as the whole past month.</p>

            <table>
              <tr>
                <td>Unique User Hash</td>
                <td>Earnings Total</td>
                <td>Earnings Past Month</td>
              </tr>
              @if(isset($fees))
                @foreach($fees as $key=>$referee)
                  <tr>
                    <td>{{ md5($key.'j4x389as89udfj') }}</td>
                    <td>${{$referee['all_fees']}}</td>
                    <td>${{$referee['past_month_fees']}}</td>
                  </tr>
                @endforeach
              @else
                  <tr>
                    <td>No referrals yet!</td>
                    <td>n/a</td>
                    <td>n/a</td>
                  </tr>
              @endif
            </table>

            <br>
            <h2>Payouts</h2>
            <p>Payouts are calculated on a monthly basis, and are claimable once the month fully concludes.</p>

            <p>Past payouts:</p>
            @if(isset($monthly_fees))
              @foreach($monthly_fees as $year=>$monthly_fee)
              Year: {{$year}}
              <table>
                <tr>
                  <td>Date</td>
                  <td>Payout</td>
                  <td>Claim Payment</td>
                </tr>
                @foreach($monthly_fee as $month=>$value)
                  <tr>
                    <td>{{ $month }}</td>
                    <td>${{$value['amount']}}</td>
                    <td>
                      @if($value['status'])
                        Payment already claimed.
                      @else
                        <a type="submit" href="/refer/claim/{{$year}}/{{$month}}" class="btn btn-teal">Release Payment</a></td>
                      @endif
                  </tr>
                @endforeach
              </table>
              @endforeach
            @else
            <p>No income yet!</p>
            @endif
          </div>
        </div>



@endsection
