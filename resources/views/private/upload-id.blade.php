@extends('layouts.private')

@section('page-heading', 'Upload ID')
@section('title', 'Upload ID')
@section('page-sub-heading', 'Please upload your ID')

@section('content')
            <div class="wrapper">
                <div class="general-content-area">
                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form action="/account/upload_id" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @if (isset($passport_location))
                            <div class="single-input">
                                <label>Passport:</label>
                                <img width="200" src="/storage/{{ $passport_location }}">
                            </div>
                        @else
                            <div class="single-input">
                                <label>Passport:</label>
                                <label for="filespassport" class="drop-label">Select file</label>
                                <input type="file" id="filespassport" name="passport" />
                            </div>
                        @endif


                        @if (isset($drivers_location))
                            <div class="single-input">
                                <label>Drivers License:</label>
                                <img width="200" src="/storage/{{ $drivers_location }}">
                            </div>
                        @else
                            <div class="single-input">
                                <label>Drivers License:</label>
                                <label for="filesdriver" class="drop-label">Select file</label>
                                <input type="file" id="filesdriver" name="drivers" />
                            </div>
                        @endif


                        @if (isset($bank_location))
                            <div class="single-input">
                                <label>Bank Statement:</label>
                                <img width="200" src="/storage/{{ $bank_location }}">
                            </div>
                        @else
                            <div class="single-input">
                                <label>Bank Statement:</label>
                                <label for="filesbank" class="drop-label">Select file</label>
                                <input type="file" id="filesbank" name="bank" />
                            </div>
                        @endif

                        <input type="submit" class="btn btn-teal" value="Upload" />
                    </form>
                </div>
            </div>
@endsection
