@extends('layouts.private')

@section('page-heading', 'Password')
@section('title', 'Password')

@section('breadcrumbs', Breadcrumbs::render('account.password'))

@section('content')

      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
          <div class="small-container">
             <div class="col-md-12">
               <section class="content">
                 <div class="panel-body">
                      @if (session()->has('status'))
                          <div class="alert alert-info">
                              <a class="close" data-dismiss="alert">×</a>
                              {{ session()->get('status') }}
                          </div>
                      @endif

                      <form id="account_profile_form" role="form" method="POST" action="/account/password/go">
                       {{ csrf_field() }}
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-6 col-sx-12">
                                <div class="form-group">
                                  <label for="name">New Password:</label>
                                  <input type="password" class="form-control" id="password" name="password" required>
                                </div>
                                <div class="form-group">
                                  <label for="name">Confirm Password:</label>
                                  <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="center-content">
                            <button type="submit" class="btn btn-teal">Update</button>
                          </div>
                          @if (count($errors))
                            <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                          @endif

                      </form>
                    </div>
                 </section>
              </div>
            </div>
          </div>

@endsection

