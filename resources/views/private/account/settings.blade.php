@extends('layouts.private')

@section('page-heading', 'Settings')
@section('title', 'Settings')
@section('breadcrumbs', Breadcrumbs::render('account.settings'))

@section('content')
      <div class="dashboard-content">
        <div class="wrapper flex-wrap">
          <div class="small-container">
             <div class="col-md-12">
               <section class="content">
                 <div class="panel-body">
                      @if (session()->has('status'))
                          <div class="alert alert-info">
                              <a class="close" data-dismiss="alert">×</a>
                              {{ session()->get('status') }}
                          </div>
                      @endif
                      <form id="account_profile_form" role="form" method="POST" class="form-horizontal" action="{{ url()->to('account/settings') }}">
                       {{ csrf_field() }}
                          <div class="container-fluid">
                              <table style="border:0; box-shadow:none;">
                                  <tr>
                                      <td><label for="withdraw_tfa">Enable Withdrawal TFA:</label></td>
                                      <td>
                                          <input type="hidden" name="withdraw_tfa" value="0"/>
                                          <label class="switch">
                                              <input type="checkbox" name="withdraw_tfa" id="withdraw_tfa"
                                                     value="1" {{ \Auth::user()->get_setting('withdraw_tfa', 0) ? 'checked' : '' }} />
                                              <span class="slider"></span>
                                          </label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td><label for="default_fiat_currency">Default Currency</label></td>
                                      <td>
                                          <select name="default_fiat_currency" style="background:none;">
                                            @foreach(config('currencies.fiatCurrencies') as $currency)
                                              <option value="{{$currency}}" {{ (\Auth::user()->get_setting('default_fiat_currency') == $currency) ? 'selected' : '' }}>{{ strtoupper($currency) }}</option>
                                            @endforeach
                                          </select>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td><label for="default_currency">Default Crypto Currency</label></td>
                                      <td>
                                          <select name="default_currency" style="background:none;">
                                            @foreach(config('currencies.crypto') as $currency)
                                              <option value="{{$currency}}" {{ (\Auth::user()->get_setting('default_currency') == $currency) ? 'selected' : '' }}>{{ strtoupper($currency) }}</option>
                                            @endforeach
                                          </select>
                                      </td>
                                  </tr>
                                  @can('admin.users.index')
                                      <tr>
                                          <td><label for="freshdesk_api_key">Freshdesk API Key</label></td>
                                          <td>
                                              <input type="text" name="freshdesk_api_key" id="freshdesk_api_key" value="{{ Auth::user()->get_setting('freshdesk_api_key') }}" />
                                              <div class="help-block">
                                                  <a href="https://support.freshdesk.com/support/solutions/articles/215517-how-to-find-your-api-key">Where is my API Key?</a>
                                              </div>
                                          </td>
                                      </tr>
                                  @endcan
                              </table>
                          </div>

                          <div class="center-content">
                            <button type="submit" class="btn btn-teal">Update</button>
                          </div>
                          @if (count($errors))
                            <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                          @endif

                      </form>
                    </div>
                 </section>
              </div>
            </div>
          </div>
        </div>
@endsection

