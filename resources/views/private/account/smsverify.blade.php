@extends('layouts.private')

@section('page-heading', 'Verify the account change via sms')
@section('title', 'Verify the account change via sms')

@section('content')

<div class="container dashboard withdraw-deposit">
    <div class="container-fluid account-page">

        <div class="dashboard-content">
            <div class="wrapper sub-flex">
                <div class="general-content-area">
                    <div class="top-note">
                        <h4>Additional actions required</h4>
                    </div>
                    <div class="body">

                        @if ($version['status'] != 'pending')
                            You've successfully verified your mobile!
                        @else
                            @if (!isset(Auth::user()->phone))
                                You haven't set a mobile number yet! Go <a href="/account">here</a> to set one.
                            @else
                                @if (isset($sms_verified))
                                    Your mobile number is verified!
                                @else
                                    <form action="account/sms_bank_addr" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="sms" value="send" />
                                        <input type="hidden" name="version_id" value="{{$version['id']}}" />
                                        <input type="hidden" name="version_status" value="{{$version['status']}}" />
                                        <input type="hidden" name="dob" value="{{$dob}}" />
                                        <br />
                                        Your mobile number is not yet verified.<br>
                                        <br /><br />
                                        <input type="submit" value="Send SMS to {{Auth::user()->phone}}" />
                                    </form>
                                @endif
                            @endif
                            @if (isset($show_form))
                                <form action="account/sms_bank_addr/verify" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="sms" value="verify" />
                                    <input type="hidden" name="version_id" value="{{$version['id']}}" />
                                    <input type="hidden" name="version_status" value="{{$version['status']}}" />
                                    <input type="hidden" name="dob" value="{{$dob}}" />
                                    <br />
                                    Please enter the code you received:<br>

                                    <input type="text" name="code" value="">
                                    <br /><br />
                                    <input type="submit" value="Submit Code" />
                                </form>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection