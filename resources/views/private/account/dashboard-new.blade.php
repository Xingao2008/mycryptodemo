<html>
<head>
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .theme--light {
            background-color: transparent !important;
        }
        .v-text-field__slot,
        .v-menu__activator,
        .v-select__slot{
            height: auto;
        }
    </style>
</head>
<body>
<div id="app">
    <v-app>
        <v-content>
            <v-container>

                <v-alert class="notification is-danger" v-model="alert" :type="alertType">
                    @{{ alertMessage }}
                </v-alert>

                <v-layout row class="text-xs-center">
                    <v-flex d-flex xs12 sm12 md12 class="my-padding">

                        <v-container>
                            <v-card flat>
                                <v-card-title primary-title>
                                    <h2>Dashboard</h2>
                                </v-card-title>
                                <v-card-text>
                                    <p class="text-lg-left"></p>
                                </v-card-text>

                                <v-card-actions>
                                    <v-btn color="primary" large block @click.prevent="logout">logout</v-btn>
                                </v-card-actions>
                            </v-card>
                        </v-container>

                    </v-flex>
                </v-layout>

            </v-container>
        </v-content>
    </v-app>
</div>
</body>
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vuetify/dist/vuetify.js"></script>
<script>
    new Vue({
        el: '#app',
        data: {

        },
        methods: {
            logout() {

            },

            loadUser() {
                axios.post('/loadUser')
                    .then(function(response) {

                    })
                    .catch(function(response) {

                    })
            }
        },
        mounted: {
            this.loadUser()
        }
    })
</script>
</html>