@extends('layouts.app')

@section('custom_styles')
    <style>
        .modal-mask {
            position: fixed;
            z-index: 9998;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, .5);
            display: table;
            transition: opacity .3s ease;
        }

        .modal-wrapper {
            display: table-cell;
            vertical-align: middle;
        }

        .modal-container {
            width: 400px;
            height: 80px;
            margin: 0px auto;
            padding: 20px 30px;
            background-color: #fff;
            border-radius: 2px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
            transition: all .3s ease;
            font-family: Helvetica, Arial, sans-serif;
        }

        .modal-header h3 {
            margin-top: 0;
            color: #42b983;
        }

        .modal-body {
            margin: 20px 0;
        }

        .modal-default-button {
            background-color: #f52e18;
            float: right;
            padding: 10px 25px;
            border: 2px solid #f52e18;
            color: white;
            text-decoration: none;
            padding: 12px 36px;
            margin-right: 10px;
            border-radius: 3px;
            font-family: 'greycliff-heavy';
            font-size: 16px;
            line-height: 1;
            display: inline-block;
            -webkit-transition: ease all 0.2s;
            transition: ease all 0.2s;
        }

        .green {
            background-color: #28a745;
            border: 2px solid #28a745;
        }

        /*
         * The following styles are auto-applied to elements with
         * transition="modal" when their visibility is toggled
         * by Vue.js.
         *
         * You can easily play with the modal transition by editing
         * these styles.
         */

        .modal-enter {
            opacity: 0;
        }

        .modal-leave-active {
            opacity: 0;
        }

        .modal-enter .modal-container,
        .modal-leave-active .modal-container {
            -webkit-transform: scale(1.1);
            transform: scale(1.1);
        }

        .spinner-next {
            margin-left: 10px;
            display: inline-block;
        }
    </style>
@endsection

@section('content')

    <div class="main" id="app">
        <div class="container-fluid">
            <div class="row">
                <h1 style="display: inline-block;">Manage my orders</h1><div class="spinner-next"><i v-show="loading" class="fa fa-spinner fa-spin"></i></div>
                <table>
                    <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Price</th>
                            <th>Currency</th>
                            <th>Buy/Sell</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody v-for="(myOrder, index) in my_order">
                        <tr>
                            <td>@{{ myOrder.amount }}</td>

                            <td v-if="myOrder.currency.toUpperCase() === 'AUD'">$ @{{ myOrder.price }}</td>
                            <td v-else>@{{ myOrder.price }}</td>

                            <td>@{{ myOrder.currency.toUpperCase() }} / @{{ myOrder.currency_pair.toUpperCase() }}</td>
                            <td>@{{ myOrder.market_type.toUpperCase() }}</td>

                            <td v-if="myOrder.currency.toUpperCase() === 'AUD'">$ @{{ myOrder.total }}</td>
                            <td v-else>@{{ myOrder.total }}</td>

                            <td>@{{ myOrder.status }}</td>
                            <td v-if="myOrder.status.toLowerCase() === 'active'">
                                <a href="#" class="btn btn-red btn-sm" @click.prevent="cancelPron( myOrder.id, index )">Cancel <i v-show="myOrder.canceling" class="fa fa-spinner fa-spin"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <my-component v-if="showModel" @close="showModel = false"></my-component>

        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script type="application/javascript">
        // register modal component
        Vue.component('my-component', {
            template: `
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-container">

                            <div class="modal-header">
                                <h3><slot name="header">Are you sure to cancel the order ?</slot></h3>
                            </div>

                            <div class="modal-footer">
                                <slot name="footer">
                                    <button class="modal-default-button" @click.prevent="$parent.cancel()">Yes</button>
                                    <button class="modal-default-button green" @click.prevent="$emit('close')">No</button>
                                </slot>
                            </div>

                        </div>
                    </div>
                </div>
            `
        })

        let app = new Vue({
            el: '#app',
            data: {
                myToken: '',
                my_order: [],
                showModel: false,
                cancelId: '',
                cancelIndex: '',
                loading: false
            },
            methods: {
                getMyorder() {
                    this.my_order = [];
                    this.loading = true;
                    axios.get(
                        'api/my-recent-orders'
                    )
                        .then((rsp) => {
                    let data = rsp.data.data;
                    for (var i in data) {
                        this.my_order.push({
                            id: data[i].id,
                            market_type: data[i].market_type,
                            date: data[i].updated_at,
                            amount: data[i].volume,
                            status: data[i].status.toUpperCase(),
                            price: data[i].price,
                            currency: data[i].currency,
                            currency_pair: data[i].currency_pair,
                            total: data[i].volume * data[i].price,
                            canceling: false
                        })
                    }

                    this.loading = false;
                    })
                    .catch((err) => {
                        if (err.response.status === 401) {

                        }

                        this.loading = false;
                    })
                },
                cancelPron (id, index) {
                    this.showModel = true
                    this.cancelId = id
                    this.cancelIndex = index
                },
                cancel () {
                    this.showModel = false
                    this.my_order[this.cancelIndex].canceling = true
                    axios.patch(
                        'api/cancel-exchange-order/' + this.cancelId, {
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: this.myToken
                            }
                        })
                        .then((rsp) => {
                        this.my_order[this.cancelIndex].status = 'CANCELLED'
                        this.my_order[this.cancelIndex].canceling = false
                    })
                }
            },
            mounted () {
                let token = this.$cookie.get('api_token')
                this.myToken = token
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`

                this.getMyorder()
            }
        })
    </script>
@endpush