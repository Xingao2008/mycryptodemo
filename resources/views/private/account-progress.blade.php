@extends('layouts.private')

@section('page-heading', 'Account Progress')

@section('title', 'Account Progress')

@section('container', 'flex-wrap wrapper')

@section('content')
    <div class="dialog-wrapper account-progress">
        <article class="message">
            <div class="message-header">
                <p>COMPLETE YOUR ACCOUNT</p>
            </div>
            <div class="message-body">
                <!-- email verify? -->
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/email-yes.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Confirm your email address</h4>
                        <p>Your email address has been verified</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-yes.png') }}">
                    </div>
                </div>
                <!-- phone number added? -->
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/phone-yes.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Add your phone number</h4>
                        <p>Your account is associated with a phone number</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-yes.png') }}">
                    </div>
                </div>
                <!-- ID verified? -->
                @if($verification->account == 'approved')
                    <div class="columns is-mobile">
                        <div class="column is-1 is-hidden-mobile"></div>
                        <div class="column is-3 logo-step">
                            <img class="progress-icon" src="{{ url('img/progress/id-yes.png') }}">
                        </div>
                        <div class="column is-6 info">
                            <h4>Verify your identity</h4>
                            <p>Upload your ID to get the account verified</p>
                        </div>
                        <div class="column is-2 logo-check">
                            <img class="check" src="{{ url('img/progress/check-yes.png') }}">
                        </div>
                    </div>
                @else
                    <div class="columns is-mobile">
                        <div class="column is-1 is-hidden-mobile"></div>
                        <div class="column is-3 logo-step">
                            <img class="progress-icon" src="{{ url('img/progress/id-no.png') }}">
                        </div>
                        <div class="column is-6 info">
                            <h4>Verify your identity</h4>
                            <p>Upload your ID to get the account verified</p>
                        </div>
                        <div class="column is-2 logo-check">
                            <img class="check" src="{{ url('img/progress/check-no.png') }}">
                        </div>
                    </div>
                @endif
                <!-- Payment method? -->
                @if($verification->bank == 'approved')
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/account-yes.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Add a payment method</h4>
                        <p>Use a bank account or card to make your first purchase</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-yes.png') }}">
                    </div>
                </div>
                @else
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/account-no.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Add a payment method</h4>
                        <p>Use a bank account or card to make your first purchase</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-no.png') }}">
                    </div>
                </div>
                @endif
                <!-- 2FA Enabled? -->
                @if($verification->tfa != 'approved')
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/safety-no.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Two factor authentication</h4>
                        <p>An extra layer of security to keep your account safe</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-no.png') }}">
                    </div>
                </div>
                @else
                <div class="columns is-mobile">
                    <div class="column is-1 is-hidden-mobile"></div>
                    <div class="column is-3 logo-step">
                        <img class="progress-icon" src="{{ url('img/progress/safety-yes.png') }}">
                    </div>
                    <div class="column is-6 info">
                        <h4>Two factor authentication</h4>
                        <p>An extra layer of security to keep your account safe!</p>
                    </div>
                    <div class="column is-2 logo-check">
                        <img class="check" src="{{ url('img/progress/check-yes.png') }}">
                    </div>
                </div>
                @endif
            </div>
            <div class="message-footer">
                <a class="button btn-blue-fading" href="/verify">Complete your account</a>
            </div>
        </article>
    </div>

@endsection
