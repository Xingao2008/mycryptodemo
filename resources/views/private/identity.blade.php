@extends('layouts.private')

@section('page-heading', 'Identity Verification')

@section('title', 'Identity Verification')

@section('content')

  <div class="small-container">
     <div class="col-md-12">
       <section class="content">
         <div id="greenid-div"></div>
         <div class="panel-body">
              @if (session()->has('status'))
                  <div class="alert alert-info">
                      <a class="close" data-dismiss="alert">×</a>
                      <strong>Success!</strong> {{ session()->get('status') }}
                  </div>
              @endif

              @if ($user->verify_count > 3)

                <div class="error alert alert-danger">
                  <button class="close" data-close="alert"></button>
                  <span>You've exceeded the number of attempts to get verified. Please email your ID documents (passport, driver's license and proof of address) to support@mycryptowallet.com.au</span>
                </div>

              @elseif (isset(Auth::user()->address->country_id) && (Auth::user()->address->country_id != 36 && Auth::user()->address->country_id != 554))
                  <div class="error alert alert-danger">
                  <button class="close" data-close="alert"></button>
                  <span>Status: incomplete</span> <br />
                  <span>Are your ID Documents from outside of Australia or New Zealand? Please email <a href="mailto:support@mycryptowallet.com.au?subject=ID Documents for User ID: {{Auth::user()->id}}">support@mycryptowallet.com.au</a> with the subject 'ID Documents for User ID {{Auth::user()->id}}'.
                  <br /><br />
                  To ensure no delays, please provide some government-issued ID documents that include your address, full name, photo and date of birth - this doesn't have to be one document, it can be multiple documents (e.g. passport, social security number and driver's license for the address and photo).</span>
                  <br /><br />
                  Alternatively, submit your documents through our <a href="https://mycryptowallet.freshdesk.com/support/tickets/new">support ticket system</a>.
                </div>
              @else


                @if (!$user->verified)

                  <form id="vixverify_form" role="form" method="POST" action="/account">
                   {{ csrf_field() }}
                      <div class="form-group general-container">
                          <label for="name">Unique Bank Code (UBC)</label>
                          {{ Auth::user()->ubc }}
                      </div>
                      <div class="container-fluid">
                        <div class="row">
                          <div class="col-md-6 col-sx-12">
                            <div class="form-group">
                              <label for="givenNames">First Name</label>
                              <input type="text" class="form-control" id="givenNames" name="givenNames" value="{{Auth::user()->first_name}}" required>
                            </div>
                            <div class="form-group">
                              <label for="middleNames">Middle Name (optional)</label>
                              <input type="text" class="form-control" id="middleNames" name="middleNames" value="{{Auth::user()->middle_name}}">
                            </div>
                            <div class="form-group">
                              <label for="surname">Last Name</label>
                              <input type="text" class="form-control" id="surname" name="surname" value="{{Auth::user()->last_name}}" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Mobile</label>
                                <select style="width: 18%; display: inline-block; margin-right: 11px;" name="country_calling_code">
                                    <option value="">Country code</option>
                                    @foreach ($countries as $c)
                                        <option {{ (isset(Auth::user()->country_calling_code) && Auth::user()->country_calling_code == $c->calling_code ? 'selected="selected"' : '')}}
                                            value="{{$c->calling_code}}">+{{$c->calling_code}}</option>
                                    @endforeach
                                </select>
                                <input style="width: 80%; display: inline-block;" type="text" class="form-control" id="phone" name="phone" value="{{Auth::user()->phone}}" >
                            </div>
                            <div class="form-group">
                              <label for="dob">Date of Birth</label>
                              <input placeholder="DD/MM/YYYY" type="text" class="form-control" id="dob" name="dob" value="{{Auth::user()->dob}}" >
                            </div>
                          </div>
                          <div class="col-md-6 col-sx-12">
                           <div class="form-group">
                              <label for="address">Address</label>
                              <label for="address">Street</label>
                              <input type="text" class="form-control" id="street" name="street" value="{{@Auth::user()->address->street}}" >

                              <label for="address">City</label>
                              <input type="text" class="form-control" id="city" name="city" value="{{@Auth::user()->address->city}}" >
                              <?php if(isset(Auth::user()->address->state)) {$state = Auth::user()->address->state;} else {$state = "";} ?>
                              @if (isset(Auth::user()->address->country_id) && @Auth::user()->address->country_id == 36)
                              <label for="address">State</label>
                              <select id="state" class="form-control" name="state" autocomplete="off">
                               
                                @foreach(get_states_for_country(@Auth::user()->address->country_id) as $key => $longForm)
                                  <option value='{{$key}}' 
                                  @if($state == $key) {
                                    selected
                                  @endif
                                    >{{$key}}</option>
                                @endforeach
                              </select>
                               @else
                                  <label for="address">State</label>
                                  <input type="text" class="form-control" id="state" name="state" value="{{@Auth::user()->address->state}}" >
                              @endif
                              {{-- <input type="text" class="form-control" id="state" name="state" value="{{@Auth::user()->address->state}}" > --}}

                              @if (isset(Auth::user()->address->country_id) && @Auth::user()->address->country_id == 36)
                                  <label for="address">Post Code</label>
                              @else
                                  <label for="address">Zip Code</label>
                              @endif
                              <input type="text" class="form-control" id="post_code" name="post_code" value="{{@Auth::user()->address->post_code}}" >

                              <label for="address">Country</label>
                              <select name="country_id" style="width: 100%;" autocomplete="off">
                                @foreach ($countries as $country)
                                  <option value="{{$country->id}}"

                                  @if(isset(Auth::user()->address->country_id) && @Auth::user()->address->country_id == $country->id)
                                    selected
                                  @elseif (empty(@Auth::user()->address->country_id) && $country->id == 36)
                                    selected
                                  @endif
                                  >{{$country->name}}</option>

                                @endforeach
                              </select>
                              {{-- @if(Auth::user()->address->country_id == 840 || Auth::user()->address->country_id == 124)
                              <div class="form-group">
                                @if(@Auth::user()->address->country_id == 840)
                                  <label for="GBGIdentityID">Social Security Number</label>
                                  
                                @elseif(@Auth::user()->address->country_id == 124)
                                  <label for="GBGIdentityID">SocialInsuranceNumber</label>
                                @endif
                                <input type="text" class="form-control" id="GBGIdentityID" name="GBGIdentityID" value="{{Auth::user()->verification->GBGIdentityID}}" >
                              </div>
                              @endif --}}
                              {{--
                              <div class="form-group">
                                <label for="passbookNumber">Passbook No.</label>
                                <input type="text" class="form-control" id="passbookNumber" name="passbookNumber" value="{{Auth::user()->abn}}" >
                              </div> --}}
                              {{-- <div class="form-group">
                                <label for="dob">ABN/ACN if relevant (optional)</label>
                                <input type="text" class="form-control" id="abn" name="abn" value="{{Auth::user()->abn}}" >
                              </div> --}}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="center-content">
                        <button type="submit" class="btn btn-teal" id="verifyBtn">Verify</button>
                        {{-- <span id="connectingBtn">Connecting to external verification service...</span> --}}
                      </div>
                      @if (count($errors))
                        <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                      @endif

                      <input type="hidden" name="accountId" id="accountId" value="{{ config('vix.account_id') }}">
                      <input type="hidden" name="apiCode" id="apiCode" value="{{ config('vix.apikey') }}">
                      <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                      @if (isset(Auth::user()->address->country_id) && @Auth::user()->address->country_id == 554)
                        <input type="hidden" id="ruleId" name="ruleId" value="NZ" />
                      @else
                        <input type="hidden" id="ruleId" name="ruleId" value="AU" />
                      @endif
                      <input type="hidden" id="usethiscountry" value="{{ isset(Auth::user()->address)?Auth::user()->address->country->iso_3166_2:'AU' }}" name="country"/>

                  </form>

                @else

                  <div class="error alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span>Congratulations, you are now successfully verified! Please now <a href="/verify">complete any final verifications required.</a></span>
                  </div>

                @endif

              @endif

              <div class="error alert alert-danger" style="display:none;">
                  <button class="close" data-close="alert"></button>
                  <span></span>
              </div>

            </div>
         </section>
      </div>
    </div>


@endsection

@section('custom_scripts')

<script src="https://simpleui-au.vixverify.com/df/javascripts/greenidConfig.js" type="text/javascript"></script>
@if (isset(Auth::user()->address->country_id) && @Auth::user()->address->country_id == 36 || @Auth::user()->address->country_id == 554)
<script src="https://simpleui-au.vixverify.com/df/javascripts/greenidui.min.js" type="text/javascript"></script>
@endif
{{-- <script src="https://simpleui-test-au.vixverify.com/df/javascripts/greenidConfig.js" type="text/javascript"></script>
<script src="https://simpleui-test-au.vixverify.com/df/javascripts/greenidui.min.js" type="text/javascript"></script> --}}



<script type="text/javascript">

  $(document).ready(function() {

      const base_url = '<?= url('/'); ?>';
      const vixVerifyAttempts = function () {
          $.ajax({
              type: 'POST',
              url: base_url + '/api/user/verify/attempts',
              data: {'user_id': <?= auth()->id(); ?>}
          });
      };

      @if (isset(Auth::user()->address->country_id) && Auth::user()->address->country_id == 36 || Auth::user()->address->country_id == 554)
      greenidUI.setup({
          environment: "prod",
          formId: "vixverify_form",
          frameId: "greenid-div",
          country: "usethiscountry",
          registerCallback: function(verificationId, userData){
              $.ajax({
                  type: 'POST',
                  url: base_url + '/api/verification',
                  data: {
                      'id': verificationId,
                      'user': <?= auth()->id(); ?>
                  }
              });
          },
          submitCallback: function(verificationToken, overallState){
              $.ajax({
                  type: 'POST',
                  url: base_url + '/api/verification',
                  data: {
                      'token': verificationToken,
                      'state': overallState,
                      'user': <?= auth()->id(); ?>
                  },
                  success: function(data){
                      vixVerifyAttempts();
                    if (data.success){
                        window.location.href = base_url + '/account/identity';
                    } else {
                        $(".error span").html(data.message);
                        $(".error").show();
                    }
                  },
                  error: function(xhr, status, error) {
                      $(".error span").html(data.message);
                      $(".error").show();
                  }
              });
          },
          // errorCallback: function(veritificationToken, errorName){
          //     console.log('ERROR');
          //     console.log(veritificationToken);
          //     console.log(errorName);
          // },
          // sessionCompleteCallback: function(veritificationToken, overallState){
          //     console.log('SESSION COMPLETE');
          //     console.log(veritificationToken);
          //     console.log(overallState);
          // },
          // sessionCancelledCallback: function(veritificationToken, overallState){
          //     console.log('SESSION CANCELLED');
          //     console.log(veritificationToken);
          //     console.log(overallState);
          // }
      });
      @elseif (1 == 2)
        $('#vixverify_form').submit(function(event){
          event.preventDefault();
          $('#verifyBtn').hide();
          $('#connectingBtn').show();
          $(".error").hide();
          (".error span").html('');
          $.ajax({
                  type: 'POST',
                  url: base_url + '/api/verification',
                  data: $('#vixverify_form').serialize(),
                  success: function(data){
                    
                    if (data.success){
                        $(".error span").html('Congratulations, you are now successfully verified! Please now <a href="/verify">complete any final verifications required</a>.');
                        $(".error").show();
                        $('#connectingBtn').hide();
                    } else {
                        $(".error span").html(data.message);
                        $(".error").show();
                        $('#verifyBtn').show();
                        $('#connectingBtn').hide();
                    }
                  },
                  error: function(xhr, status, error) {
                    $('#verifyBtn').show();
                      $(".error span").html('Extneral verification service is currently unavailable. Please try again later.');
                      $(".error").show();
                      $('#connectingBtn').hide();
                  }
              });
      });
      @endif


  });

</script>

@endsection

@section('custom_styles')
  <link rel="stylesheet" type="text/css" media="screen" href="https://simpleui-test-au.vixverify.com/df/assets/stylesheets/greenid.css"/>
@endsection
