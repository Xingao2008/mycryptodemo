@extends('layouts.private')

@section('page-heading', 'Upoload Selfie')
@section('title', 'Upload Selfie')
@section('page-sub-heading', 'To withdraw, you must upload a selfie first.')

@section('content')
    <div class="wrapper flex-wrap">
        <div class="box box-primary">
            <div class="box-body" id="app">
                <p>In order to unlock Withdrawals of any type, you must upload a single selfie picture containing:</p>
                <ul>
                    <li>Your unobstructed face</li>
                    <li>A piece of paper detailing the current date, your signature, and your UBC</li>
                    <li>You should be holding the photo ID you used to get verified as well</li>
                    <li>You can upload as many images as needed below</li>
                </ul>

                <form action="{{ route('selfie.upload') }}" class="dropzone" id="my-awesome-dropzone">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/dropzone.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/basic.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/min/dropzone.min.js"></script>
    <style>
        .dropzone .dz-preview .dz-error-message {
            color: #fffefe;
            display: none;
        }
    </style>
    <script>
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2, // MB
            acceptedFiles: 'image/*',
        };
    </script>
@endpush
