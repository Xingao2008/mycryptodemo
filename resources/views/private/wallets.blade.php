@extends('layouts.private')

@section('page-heading', 'Wallet')
@section('title', 'Wallet')

@section('content')
        <div class="dashboard-content">
          <div class="wallet-items wrapper">
            <div class="notification is-link">
              <p>When depositing or withdrawing crypto, please only send to the same type of currency; e.g. <b>do not</b> send Bitcoin to a Litecoin wallet.</p>
              <p><b>Please note:</b> Assets are based on myCryptoWallet's instant sell price.</p>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-dollar"></div><h4 class="title">Australian Dollar - AUD</h4></div>
              <div class="content">
                <div class="balance">
                    <span class="label">Balance:</span> ${{@audFormat($wallet['aud'])}}<br>
                </div>
                <div class="options">
                  <a class="btn btn-grey" href="{{ route('currency') }}">Fiat Swap</a>
                  <a class="btn btn-blue" href="{{ route('deposit') }}/aud">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/aud">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-dollar"></div><h4 class="title">New Zealand Dollar - NZD</h4></div>
              <div class="content">
                <div class="balance">
                    <span class="label">Balance:</span> ${{@audFormat($wallet['nzd'])}}<br>

                </div>
                <div class="options">
                  <a class="btn btn-grey" href="{{ route('currency') }}">Fiat Swap</a>
                  <a class="btn btn-blue" href="{{ route('deposit') }}/nzd">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/nzd">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-dollar"></div><h4 class="title">U.S. dollar - USD</h4></div>
              <div class="content">
                <div class="balance">
                    <span class="label">Balance:</span> ${{@audFormat($wallet['usd'])}}<br>

                </div>
                <div class="options">
                  <a class="btn btn-grey" href="{{ route('currency') }}">Fiat Swap</a>
                  <a class="btn btn-blue" href="{{ route('deposit') }}/usd">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/usd">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-btc"></div><h4 class="title">Bitcoin - BTC</h4></div>
              <div class="content">
                <div class="balance">
                  <div class="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['btc']['get_crypto_balance'] }} (${{@$wallet['btc']['get_user_crypto_value']}}) in AUD</div>
                  <div class="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'nzd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['btc']['get_crypto_balance'] }} (${{@audFormat($wallet['btc']['get_user_nzd_crypto_value'])}}) in NZD</div>
                  <div class="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['btc']['get_crypto_balance'] }} (${{@audFormat($wallet['btc']['get_user_usd_crypto_value'])}}) in USD</div>
                  <br>
                  @if (!isset($btc))
                    <p><a class="" href="{{ route('generate') }}/btc">Generate Wallet</a></p>
                  @else
                    <span class="label">Address:</span> {{$btc->address}}
                  @endif
                </div>
                <div class="options">
                  <a class="btn btn-blue" href="{{ route('deposit') }}/btc">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/btc">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-eth"></div><h4 class="title">Ethereum - ETH</h4></div>
              <div class="content">
                <div class="balance">
                    <div class="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['eth']['get_crypto_balance'] }} (${{@$wallet['eth']['get_user_crypto_value']}}) in AUD</div>
                     <div class="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'nzd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['eth']['get_crypto_balance'] }} (${{@audFormat($wallet['eth']['get_user_nzd_crypto_value'])}}) in NZD</div>
                     <div class="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'usd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['eth']['get_crypto_balance'] }} (${{@audFormat($wallet['eth']['get_user_usd_crypto_value'])}}) in USD</div>
                    <br>
                    @if (!isset($eth))
                      <p><a class="" href="{{ route('generate') }}/eth">Generate Wallet</a></p>
                    @else
                      <span class="label">Address:</span> {{$eth->address}}<br>
                      <p><a href="/wallets/ethCheckForDeposit">Check For Deposit</a></p>
                    @endif
                </div>
                <div class="options">
                  <a class="btn btn-blue" href="{{ route('deposit') }}/eth">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/eth">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-ltc"></div><h4 class="title">Litecoin - LTC</h4></div>
              <div class="content">
                <div class="balance">
                    <div class="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['ltc']['get_crypto_balance'] }} (${{@$wallet['ltc']['get_user_crypto_value']}}) in AUD</div>
                     <div class="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'nzd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['ltc']['get_crypto_balance'] }} (${{@audFormat($wallet['ltc']['get_user_nzd_crypto_value'])}}) in NZD</div>
                     <div class="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'usd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['ltc']['get_crypto_balance'] }} (${{@audFormat($wallet['ltc']['get_user_usd_crypto_value'])}}) in USD</div>
                    <br>
                    @if (!isset($ltc))
                      <p><a class="" href="{{ route('generate') }}/ltc">Generate Wallet</a></p>
                    @else
                      <span class="label">Address:</span> {{$ltc->address}}
                    @endif
                </div>
                <div class="options">
                  <a class="btn btn-blue" href="{{ route('deposit') }}/ltc">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/ltc">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-rpl"></div><h4 class="title">Ripple - XRP</h4></div>
              <div class="content">
                <div class="balance">
                  <div class="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['xrp']['get_crypto_balance'] }} (${{@$wallet['xrp']['get_user_crypto_value']}}) in AUD</div>
                   <div class="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'nzd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['xrp']['get_crypto_balance'] }} (${{@audFormat($wallet['xrp']['get_user_nzd_crypto_value'])}}) in NZD</div>
                   <div class="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'usd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['xrp']['get_crypto_balance'] }} (${{@audFormat($wallet['xrp']['get_user_usd_crypto_value'])}}) in USD</div>
                  <br>
                  @if (!isset($xrp))
                    <p><a class="" href="{{ route('generate') }}/xrp">Generate Wallet</a></p>
                  @else
                    <span class="label">Address:</span> {{$xrp->address}}
                  @endif
                </div>
                <div class="options">
                  <a class="btn btn-blue" href="{{ route('deposit') }}/xrp">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/xrp">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="header"><div class="icon icon-pwr"></div><h4 class="title">PowerLedger - POWR</h4></div>
              <div class="content">
                <div class="balance">
                    <div class="aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['pwr']['get_crypto_balance'] }} (${{@$wallet['pwr']['get_user_crypto_value']}}) in AUD</div>
                     <div class="nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'nzd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['pwr']['get_crypto_balance'] }} (${{@audFormat($wallet['pwr']['get_user_nzd_crypto_value'])}}) in NZD</div>
                     <div class="usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'usd') ? 'style=display:none':'')}}><span class="label">Balance:</span> {{@ $wallet['pwr']['get_crypto_balance'] }} (${{@audFormat($wallet['pwr']['get_user_usd_crypto_value'])}}) in USD</div>
                    <br>
                    @if (!isset($eth))
                      <p><a class="" href="{{ route('generate') }}/eth">Generate Wallet</a></p>
                    @else
                      <span class="label">Address:</span> {{$eth->address}}<br>
                      <p><a href="/wallets/pwrCheckForDeposit">Check For Deposit</a></p>
                    @endif
                </div>
                <div class="options">
                  <a class="btn btn-blue" href="{{ route('deposit') }}/pwr">Deposit</a>
                  <a class="btn btn-teal" href="{{ route('withdraw') }}/pwr">Withdraw</a>
                </div>
              </div>
            </div>
            <div class="single-wallet-item">
              <div class="aud_balance header" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency') ? (is_null(\Auth::user()->get_setting('default_fiat_currency'))?'':'style=display:none'):'')}}><div class="icon icon-dollar"></div><h4 class="title">Total Assets - AUD</h4></div>
              <div class="header nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><div class="icon icon-dollar"></div><h4 class="title">Total Assets - NZD</h4></div>
              <div class="header usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}><div class="icon icon-dollar"></div><h4 class="title">Total Assets - USD</h4></div>
              <div class="content">
                <div class="balance aud_balance" {{( 'aud' != \Auth::user()->get_setting('default_fiat_currency') ? (is_null(\Auth::user()->get_setting('default_fiat_currency'))?'':'style=display:none'):'')}}>
                  <span class="label">Balance:</span> ${{@audFormat($wallet['total'])}}<br>
                </div>
                <div class="balance nzd_balance" {{( 'nzd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>
                  <span class="label">Balance:</span> ${{@audFormat($wallet['nzd_total'])}}<br>
                </div>
                <div class="balance usd_balance" {{( 'usd' != \Auth::user()->get_setting('default_fiat_currency', 'aud') ? 'style=display:none':'')}}>
                  <span class="label">Balance:</span> ${{@audFormat($wallet['usd_total'])}}<br>
                </div>
                <div class="switch-field">
                    <input type="radio" id="switch_aud" name="switch_3" value="yes" {{( 'aud' == \Auth::user()->get_setting('default_fiat_currency','aud') ? 'checked':'')}}/>
                    <label for="switch_aud">AUD</label>
                    <input type="radio" id="switch_nzd" name="switch_3" value="maybe" {{( 'nzd' == \Auth::user()->get_setting('default_fiat_currency') ? 'checked':'')}}/>
                    <label for="switch_nzd">NZD</label>
                    <input type="radio" id="switch_usd" name="switch_3" value="maybe" {{( 'usd' == \Auth::user()->get_setting('default_fiat_currency') ? 'checked':'')}}/>
                    <label for="switch_usd">USD</label>
                </div>
              </div>
            </div>
          </div>
        </div>



@endsection

@push('scripts')
<script>
  $('#switch_aud').on('click', function() {
        $('.aud_balance').show();
        $('.nzd_balance').hide();
        $('.usd_balance').hide();
    });
    $('#switch_nzd').on('click', function() {
        $('.aud_balance').hide();
        $('.nzd_balance').show();
        $('.usd_balance').hide();
    });
    $('#switch_usd').on('click', function() {
        $('.aud_balance').hide();
        $('.nzd_balance').hide();
        $('.usd_balance').show();
    });
</script>
@endpush