@extends('layouts.private')

@section('page-heading', 'Fiat Swap')
@section('title', 'Fiat Swap')
@section('container', 'withdraw-deposit')

@section('content')

        <div class="dashboard-content" >
            <div class="wrapper sub-flex">
                <div class="general-content-area" id="app" v-cloak>
                  <div class="top-note">
                    <h4>Your available AUD balance is ${{$coin_balances['aud']}}.<br></h4>
                    <h4>Your available NZD balance is ${{$coin_balances['nzd']}}.<br></h4>
                    <h4>Your available USD balance is ${{$coin_balances['usd']}}.<br></h4>
                    <h4>Minimum currency convert amount: ${{config('fee.fiat_convert.min_convert_fee')}}<br></h4>
                  </div>
                  <form role="form" method="POST" action="/currency/convert" @submit="validateForm">
                  {{ csrf_field() }}
                    
                    <div class="row">
                        <div class="help-block" v-if="errors" style="font-size:unset">
                            <ul>
                            <li v-for="error in errors">@{{ error }}
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        <label for="inputCurrencyFrom">I want to convert from</label>
                        <select id="inputCurrencyFrom" class="form-control currency-select" name="inputCurrencyFrom" v-model="selectedInputCurrencyFrom">
                            
                            <option v-for="co in currency_options" v-bind:value="co.value">
                                @{{ co.name.toUpperCase()}}
                            </option>
                        </select>
                        <input type="text" class="form-control" id="inputZip" placeholder="$0" value="" name="inputCurrencyFromValue" v-model="inputCurrencyFromValue"> 
                    </div>
                    <div class="form-group">
                        
                        <label for="inputCurrencyTo">I want to convert to</label>
                        <select id="inputCurrencyTo" class="form-control currency-select" name="inputCurrencyTo" v-model="selectedInputCurrencyTo">
                            
                            <option v-for="co in currency_options" v-bind:value="co.value">
                                @{{ co.name.toUpperCase()}}
                            </option>
                        </select>
                    
                        <input type="text" class="form-control" id="inputZip" placeholder="$0" value="" name="inputCurrencyToValue" v-model="inputCurrencyToValue" disabled>
                            
                    </div>
                   
                  <!--<p class="fee">+{{setting('public_fee')}}% fee</p>-->
                  <input type="submit" href="#" class="btn btn-teal" value="Convert">
                  {{-- <div v-if="showConfirmBox">
                      <p>Are you sure you want to convert $@{{ inputCurrencyFromValue }} (@{{selectedInputCurrencyTo}}) to $@{{ inputCurrencyToValue }} (@{{selectedInputCurrencyTo}})?</p>
                      <button type="submit" class="btn btn-teal">Yes</button>
                      <a href="#" type="submit" class="btn btn-red" @click.prevent="cancelDiag()">No</a>
                  </div> --}}
                  </form>
                  <div class="sub-note">
                    
                  </div>
                </div>
             
                
                @include('partials.mywallet')
            </div>
        </div>
@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                currency_options: [
                    @foreach (config('currencies.fiatCurrencies') as $c)
                    {name: '{{$c}}', value: '{{$c}}'},
                    @endforeach
                ],
                currency_limits: {
                    @foreach (config('currencies.fiatCurrencies') as $c)
                            '{{$c}}': {{ str_replace(',','', $coin_balances[$c])}},
                    @endforeach
                },
                currency_rate: {
                    @foreach (config('currencies.fiatCurrencies') as $c)
                            '{{$c}}': {{$currency_rate[$c]}},
                    @endforeach
                },
                inputCurrencyToValue: '',
                inputCurrencyFromValue: '',
                confirmButton: false,
                errors:[],
                selectedInputCurrencyFrom: 'aud',
                selectedInputCurrencyTo: 'nzd',
                minAmount: {{config('fee.fiat_convert.min_convert_fee')}}
            },
            methods: {
                validateForm: function (e) {
                    
                    this.errors = [];
                    if (this.selectedInputCurrencyFrom == this.selectedInputCurrencyTo) {
                        this.errors.push("You cannot convert the same currency");
                        
                    }

                    if (parseFloat(this.inputCurrencyFromValue) === 0) {
                        this.errors.push("Amount to convert cannot be zero");
                        
                    }
                    if (!this.inputCurrencyFromValue.length || this.inputCurrencyFromValue.length == ' ') {
                        this.errors.push("Amount cannot be empty");
                        
                    }
                    
                    if (parseFloat(this.currency_limits[this.selectedInputCurrencyFrom]) < parseFloat(this.inputCurrencyFromValue)) {
                        this.errors.push("Amount cannot be more than your wallet");
                    }
                    if (!this.isNumeric(this.inputCurrencyFromValue)) {
                        this.errors.push("Only digit is accepted for the amount");
                    }

                    if ( this.minAmount > parseFloat(this.inputCurrencyFromValue)) {
                        this.errors.push("Amount must be greater than " + this.minAmount);
                    }
                    
                    if (!this.errors.length) {
                        
                    } else {
                        e.preventDefault();
                    }
                }, 

                updateCurrency: function(currency)
                {
                    this.amount_aud = this.ask[currency] * (this.amount[currency] - this.miner_fee[currency]);
                },

                confirmDiag: function () {
                    this.confirmButton = (this.validateFunds)
                },

                cancelDiag: function () {
                    this.confirmButton = false
                },
                isNumeric: function(n) {
                    return !isNaN(parseFloat(n)) && isFinite(n)
                }

                
            },
            watch: {
                inputCurrencyFromValue: function (val) {
                    var selectedCurrencyRate = this.currency_rate[this.selectedInputCurrencyFrom]; //selected currency rate
                    var ToCurrencyRate = this.currency_rate[this.selectedInputCurrencyTo]; //selected currency rate
                    var returnValue = parseFloat(val) / parseFloat(selectedCurrencyRate) * parseFloat(ToCurrencyRate);
                    this.inputCurrencyToValue = returnValue.toFixed(2);
                }
            },
            computed: {
                {{-- calc: function() {
                    if(this.amount[this.selected_currency] == 0)
                    {
                        return 0;
                    }

                    return parseFloat(this.amount[this.selected_currency]) - this.miner_fee[this.selected_currency] - (this.amount[this.selected_currency] * this.site_fee);
                },

                site_fee: function () {
                    return parseFloat("{{ setting('public_fee') }}" / 100) * this.amount_aud;
                },

                currency_miner_fee: function() {
                    return parseFloat(this.miner_fee[this.selected_currency] * this.ask[this.selected_currency]);
                },

                audTextBox: function () {
                    return {
                        display: 'inline',
                        'border-color': this.validateFunds ? '' : 'red'
                    };

                }, --}}
                validateForm: function () {
                    if (this.selectedInputCurrencyFrom == this.selectedInputCurrencyTo) {
                        this.error_message = "You cannot convert the same currency";
                        return false;
                    }

                    {{-- if (this.inputCurrencyToValue < 50) {
                        this.error_message = "Amount must be more than $50";
                        return false;
                    }

                    if (this.amount_aud > this.aud_limit)
                    {
                        this.error_message = "Amount cannot be more than $" + this.aud_limit;
                        return false;
                    }

                    if(parseFloat(this.wallet.aud) >= parseFloat(this.amount_aud)){
                        this.error_message = "";
                        return true;
                    }
                    else
                    {
                        this.error_message = "Amount can't be more than your wallet";
                        return false;
                    } --}}
                } 
            },
            filters: {
                {{-- currency: function(val){
                    return accounting.formatMoney(val)
                } --}}
            }
        })
    </script>
@endpush
