@extends('layouts.private')

@section('page-heading', 'Get Verified')

@section('page-sub-heading', 'To begin trading, you must get verified first.')

@section('title', 'Get Verified')

@section('container', 'flex-wrap wrapper')

@section('content')
    <div class="flex-wrap wrapper">
        <div class="account-status-feed verify">
            <div class="single-account-status">
                <h2>Minimum Account Status (Level 1)</h2>
                <p>By completing the below requirements, you'll be able to use your account <b>immediately</b>.</p>
                <table class="table table-responsive mobile-table">
                    <tr>
                        <th><b>Requirement</b></th>
                        <th><b>Action</b></th>
                        <th><b>Status</b></th>
                        <th><b>Notes</b></th>
                    </tr>
                    <tr>
                        <td>Account Profile</td>
                        <td>
                            @if($verification->account != 'approved')
                                <a href="{{ route('account') }}">Complete now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>
                            {!! $status['account'] !!}
                        </td>
                        <td>{{ $verification->account_notes }}</td>
                    </tr>
                    <tr>
                        <td>Verify Identity</td>
                        <td>
                            @if($verification->id_drivers == 'approved' || $verification->id_bank == 'approved')
                                No Action Required
                            @elseif($status['account_completed'])
                                <a href="{{ route('identity') }}">Complete now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                Complete Account details first
                            @endif
                        </td>
                        <td>{!! $status['id'] !!}</td>
                        <td>{{ $verification->id_bank_notes }} {{ $verification->id_passport_notes }} {{ $verification->id_drivers_notes }}</td>
                    </tr>
                    <tr>
                        <td>Verify Phone</td>
                        <td>
                            @if($verification->sms != 'approved')
                                <a href="{{ route('sms') }}">Send SMS now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>{!! $status['sms'] !!}</td>
                        <td>n/a</td>
                    </tr>
                </table>
                <br>
                Minimum account status:
                @if ($status['overall'] == 'silver' || $status['overall'] == 'gold' || $status['overall'] == 'platinum')
                    <i class="fa fa-check" aria-hidden="true"></i> complete
                @else
                    <i class="fa fa-times" aria-hidden="true"></i> incomplete
                @endif
            </div>
            <div class="single-account-status">
                <h2>Recommended Account Status (Level 2)</h2>
                <p>The below verification helps improve security on your account, and allows you to make FIAT withdrawals.</p>
                <table class="table table-responsive mobile-table" style="width:100%;">
                    <thead>
                    <tr>
                        <th><b>Requirement</b></th>
                        <th><b>Action</b></th>
                        <th><b>Status</b></th>
                        <th><b>Notes</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Silver Complete</td>
                        <td>
                            @if ($status['overall'] != 'silver' && $status['overall'] != 'gold' && $status['overall'] != 'platinum')
                                Please complete Silver requirements
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>
                            @if ($status['overall'] == 'silver' || $status['overall'] == 'gold' || $status['overall'] == 'platinum')
                                <i class="fa fa-check" aria-hidden="true"></i> complete
                            @else
                                <i class="fa fa-times" aria-hidden="true"></i> incomplete
                            @endif
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Add Bank Account</td>
                        <td>
                            @if($verification->bank != 'approved')
                                <a href="{{ route('account') }}">Add now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>{!! $status['bank'] !!}</td>
                        <td>{{ $verification->bank_notes }}</td>
                    </tr>
                    <tr>
                        <td>2FA Enabled</td>
                        <td>
                            @if($verification->tfa != 'approved')
                                <a href="{{ route('tfa') }}">Enable now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>{!! $status['tfa'] !!}</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                Recommended Account Status:
                @if ($status['overall'] == 'gold' || $status['overall'] == 'platinum')
                    <i class="fa fa-check" aria-hidden="true"></i> complete
                @else
                    <i class="fa fa-times" aria-hidden="true"></i> incomplete
                @endif
            </div>
            <div class="single-account-status">
                <h2>Corporate Account Status (Level 3)</h2>
                <p>The below account status unlocks the corporate account level.</p>
                <table class="table table-responsive mobile-table" style="width:100%;">
                    <tr>
                        <th><b>Requirement</b></th>
                        <th><b>Action</b></th>
                        <th><b>Status</b></th>
                    </tr>
                    <tr>
                        <td>Level 2 Complete</td>
                        <td>
                            @if ($status['overall'] != 'gold' && $status['overall'] != 'platinum')
                                Please complete Level 2 requirements
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>
                            @if ($status['overall'] == 'gold' || $status['overall'] == 'platinum')
                                <i class="fa fa-check" aria-hidden="true"></i> complete
                            @else
                                <i class="fa fa-times" aria-hidden="true"></i> incomplete
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Add your ABN/ACN</td>
                        <td>
                            @if($verification->abn != 'approved')
                                <a href="{{ route('account') }}">Add now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>{!! $status['abn'] !!}</td>
                    </tr>
                    <tr>
                        <td>Maintain $5k to $25k Account Balance</td>
                        <td>
                            @if($verification->balance1 != 'approved')
                                <a href="/wallets/deposit/aud">Deposit now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                            @else
                                No Action Required
                            @endif
                        </td>
                        <td>{!! $status['balance1'] !!}</td>
                    </tr>

                </table>
                <br>
                Corporate Verification overall status:
                @if ($status['overall'] == 'platinum')
                    <i class="fa fa-check" aria-hidden="true"></i> complete
                @else
                    <i class="fa fa-times" aria-hidden="true"></i> incomplete
                @endif
            </div>
        </div>
    </div>

@endsection
