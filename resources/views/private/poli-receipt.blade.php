@extends('layouts.private')

@section('page-heading', 'Receipt')
@section('title', '')
@section('page-sub-heading', 'Receipt')

@section('content')

<div class="poli-receipt">
    <div id="header">
        <h1>
            <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 24 24" height="24" width="24" xml:space="preserve"
                 version="1.1" id="receipt-header-image" class="injected-svg inject-me"
                 data-fallback="/Images/ap/tick.png"><defs>
                    <clipPath clipPathUnits="userSpaceOnUse">
                        <path d="M0 0 85 0 85 85 0 85Z"></path>
                    </clipPath>
                </defs>
                <g transform="matrix(0.52863436,0,0,-0.52863436,-10.4603,34.440687)">
                    <g clip-path="url(#clipPath18)">
                        <g transform="translate(55.4874,48.4503)">
                            <path d="m0 0-16.2-16.1c-0.6-0.6-1.3-0.9-2.1-0.9-0.1 0-0.2 0-0.3 0-0.9 0.1-1.7 0.6-2.2 1.3l-5.5 8.2c-0.9 1.4-0.6 3.2 0.8 4.2 1.4 0.9 3.2 0.6 4.2-0.8l3.5-5.2 13.6 13.6C-3.1 5.4-1.2 5.4 0 4.2 1.2 3.1 1.2 1.2 0 0m-13 16.7c-12.5 0-22.7-10.2-22.7-22.7 0-12.5 10.2-22.7 22.7-22.7 12.5 0 22.7 10.2 22.7 22.7 0 12.5-10.2 22.7-22.7 22.7"
                                  fill="#73c050"></path>
                        </g>
                    </g>
                </g>
                <path d="M8.5 17.6C8 17.3 4.9 12.7 4.8 12.2 4.6 10.8 6.2 9.8 7.2 10.7c0.2 0.1 0.7 0.9 1.2 1.6 0.5 0.8 1 1.4 1 1.4 0 0 1.7-1.7 3.8-3.7 4.1-4 4-4 4.8-3.8 0.4 0.1 1 0.6 1.1 1.1 0.2 0.8 0.3 0.8-4.6 5.6-2.5 2.5-4.6 4.6-4.8 4.7-0.4 0.2-1 0.2-1.4 0z"
                      fill="#fff"></path></svg>
            Transaction receipt
        </h1>
    </div>
    <div id="paymentInformation">
        <div id="merchantInformation">
            <div id="merchantInformationRight">
                <h3>MyCrytoWallet Pty. Ltd.</h3>
            </div>
        </div>
        <div id="amountInformation">
            <span id="amountLabel">Amount: </span>
            <span id="amountText">${{ $polidata->PaymentAmount }}</span>
        </div>
    </div>

    <div id="receiptBody">
        <ul>
            <li>
                <span class="receiptItemLabel">Amount paid</span>
                <span class="receiptItemContent">${{ $polidata->PaymentAmount }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">Paid from</span>
                <span class="receiptItemContent">{{ $polidata->FinancialInstitutionName }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">To account name</span>
                <span class="receiptItemContent">{{ $polidata->MerchantAccountName }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">To BSB</span>
                <span class="receiptItemContent">{{ $polidata->MerchantAccountSortCode }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">To account number</span>
                <span class="receiptItemContent">{{ $polidata->MerchantAccountNumber }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">POLi ID</span>
                <span class="receiptItemContent">{{ $polidata->TransactionRefNo }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">myCryptoWallet Transaction ID</span>
                <span class="receiptItemContent">{{ $record->reference }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">Bank receipt</span>
                <span class="receiptItemContent">{{ $polidata->BankReceipt }}</span>
            </li>
            <li>
                <span class="receiptItemLabel">Transaction time</span>
                <span class="receiptItemContent">{{ $polidata->BankReceiptDateTime }}</span>
            </li>
        </ul>
    </div>
</div>

@endsection
