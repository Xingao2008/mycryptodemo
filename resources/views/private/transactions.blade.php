@extends('layouts.private')

@section('page-heading', 'Transactions')
@section('title', 'Transactions')
@section('page-sub-heading', '')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @include('flash::message')
            <div class="panel panel-default" style="background-color:#F6F7F9;">
                <div class="panel-body">
                @if ($tier == 'unverified')
                    <a class="btn btn-success" style="float:right;" href="{{ route('verify') }}">Verify Now</a>
                    <p><b>One more thing before you get started...</b></p>
                    <p>Thanks for creating a myCryptoWallet Account. We just need you to <a href="{{ route('verify') }}">verify your account</a> and you'll be ready to go!</p>
                @elseif ($tier == 'silver')
                    <a class="btn btn-success" style="float:right;" href="{{ route('verify') }}">Enhance your verification</a>
                    <p><b>You're now verified!</b></p>
                    <p>Congratulations on getting verified! <a href="{{ route('verify') }}">Now enhance your account</a> security by adding Two-Factor Authentication, add your bank account and unlock lower fees by depositing $5,000 in Fiat or Crypto.</p>
                @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">myWallet</div>
                <div class="panel-body">
                    <div class="">$0 AUD</div>
                    <div class="">0 BTC</div>
                    <div class="">0 ETH</div>
                    <div class="">0 LTC</div>
                    <div class="">0 XRP</div>

                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">Market Prices</div>
                <div class="panel-body">
                    Market Prices Graph goes here
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
