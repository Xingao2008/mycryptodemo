@extends('layouts.private')

@section('page-heading', 'myDashboard')

@section('title', 'myDashboard')

@section('breadcrumbs', Breadcrumbs::render('dashboard'))

@section('content')

        <div class="wrapper wrap-flex">

            <!-- Warning message for 2FA -->
            @if($tfa_status === 0)
                <div class="notification is-danger level">
                    <p class="level-left">You do not have <b>Two Factor Authentication</b> set up and your account is at risk.</p>
                    <a class="level-right" href="/account/tfa"><p>Set up now</p></a>
                </div>
            @endif

            @if($tier == 'silver' || $tier == 'unverified')
                <div class="system-message">
                    <div class="inner">
                        @if ($tier == 'unverified')
                            <div class="verified">
                                <div class="left">
                                    <img src="img/id-card.svg">
                                </div>
                                <div class="right">
                                    <div class="flex-group">
                                        <div class="first">
                                            <p class="highlighted">One more thing before you get started...</p>
                                            <p class="general">Thanks for creating a myCryptoWallet Account. We just need you to <a href="{{ route('verify') }}">verify your account</a> and you'll be ready to go!</p>
                                        </div>
                                        <div class="second">
                                            <a class="btn btn-teal" href="{{ route('account-progress') }}">Verify Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif ($tier == 'silver')
                            <div class="verified">
                                <div class="left"><img src="img/shield.svg"></div>
                                <div class="right">
                                    <p class="highlighted">Perfect! Your account is verified.</p>
                                    <p class="general">Thanks for veryfiying your account. You can now top up your <a href="{{ route('wallets') }}">myWallet</a>, or go for the <a href="{{ route('verify') }}">Recommended tier</a> for enhanced account security.
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
        <div class="dashboard-content" id="chart">
            <div class="wrapper flex-wrap">
                {{--@include('partials.blockfolio')--}}
            </div>
            <div class="wrapper flex-wrap">
                @include('partials.mywallet')
                @include('partials.charts')
            <div style="height: 50px; width: 100%;"></div>
{{--         <div class="user-fees wrapper">
            <h4>Transactions & Fees</h4>
            <div class="content">
              <div class="right">
                <table>
                    <tr>
                        <th>Transaction Volume</th>
                        <th>Fees</th>
                    </tr>
                    <tr>
                        <td>$0 - $5,000</td>
                        <td>1% Fees</td>
                    </tr>
                    <tr>
                        <td>$5,001 - $25,000</td>
                        <td>0.75% Fees</td>
                    </tr>
                    <tr>
                        <td>$25,001+</td>
                        <td>0.50% Fees</td>
                    </tr>
                </table>
              </div>
              <div class="left">
                <p>Each transaction you make in any given month brings you closer to reduced fees.</p>
                <p>You've made ${{userVolume30(Auth::user()->id, true)}} worth of transactions this month.</p>
              </div>
            </div>
        </div>--}}
        </div>
    </div>

@endsection
@push('scripts')
    <script src=" {{ mix('js/dashboard-widgets.js') }} "></script>
@endpush
