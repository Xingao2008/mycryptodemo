@extends('layouts.private')

@section('page-heading', 'Order History')

@section('title', 'Order History')

@section('content')
<div class="wrapper">
    <div class="single-grouping">
        <h4 class="sub-title">Deposits:</h4>
        @if(!$user_deposits->isEmpty())
        <table>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>To Address</th>
                <th>TXID</th>
                <th>Amount</th>
                <th>Status</th>
                <th>Confirmations</th>
            </tr>
            @foreach($user_deposits as $deposit)
            <tr>
                <td>{{date('Y-m-d h:i a', strtotime($deposit->created_at))}}</td>
                <td>{{strtoupper($deposit->type)}}</td>
                <td>{{$deposit->to}}</td>
                <td>{{$deposit->transaction_id}}</td>
                <td>{{$deposit->amount}}</td>
                <td>{{$deposit->status}}</td>
                <td>{{$deposit->confirmations}}</td>
            </tr>
            @endforeach
        </table>
        @else
        <p style="font-family: 'greycliff-regular';font-size: 18px;">There are no deposits to show.</p>
        @endif
    </div>
    <div class="single-grouping">
        <h4 class="sub-title">Withdraws:</h4>
        @if(!$user_withdraws->isEmpty())
        <table>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>To Address</th>
                <th>Amount (before fees)</th>
                <th>Status</th>
                <th>Confirmations</th>
                <th>Fees</th>
            </tr>
            @foreach($user_withdraws as $withdraw)
            <tr>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $withdraw->created_at, 'Australia/Melbourne')}}</td>
                <td>{{strtoupper($withdraw->currency)}}</td>
                <td>{{$withdraw->address_to}}</td>
                <td>{{$withdraw->amount}}</td>
                @if($withdraw->status == "started")
                    <td>Needs verification. <a href="/wallets/verify/withdraw/{{$withdraw->verifyid}}">Authorise Now</a></td>
                @else
                    <td>{{$withdraw->status}}</td>
                @endif
                <td>{{$withdraw->confirmations}}</td>
                <td>{{$withdraw->actual_fee}}</td>
            </tr>
            @endforeach
        </table>
        @else
        <p style="font-family: 'greycliff-regular';font-size: 18px;">There are no withdraws to show.</p>
        @endif
    </div>

    <div class="single-grouping">
        <h4 class="sub-title">Buys:</h4>
        @if(!$user_buys->isEmpty())
        <table>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Amount (before fees)</th>
                <th>Price (time of sale)</th>
                <th>Purchased</th>
                <th>Status</th>
                <th>Fees</th>
            </tr>
            @foreach($user_buys as $user_buy)
            <tr>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_buy->created_at, 'Australia/Melbourne')}}</td>
                <td>{{strtoupper($user_buy->type)}}</td>
                <td>{{$user_buy->amount}}</td>
                <td>${{bcdiv($user_buy->price,(1+(setting('public_fee')/100)),2)}}</td>
                <td>${{$user_buy->total}}</td>
                <td>{{$user_buy->status}}</td>
                <td>${{number_format(($user_buy->amount * $user_buy->price)*(setting('public_fee') / 100),2)}}</td>
            </tr>
            @endforeach
        </table>
        @else
        <p style="font-family: 'greycliff-regular';font-size: 18px;">There are no buy orders to show.</p>
        @endif
    </div>
    <div class="single-grouping">
        <h4 class="sub-title">Sells:</h4>
        @if(!$user_sells->isEmpty())
        <table>
            <tr>
                <th>Date</th>
                <th>Type</th>
                <th>Amount (before fees)</th>
                <th>Price (time of sale)</th>
                <th>Received</th>
                <th>Status</th>
                <th>Fees</th>
            </tr>
            @foreach($user_sells as $user_sell)
            <tr>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user_sell->created_at, 'Australia/Melbourne')}}</td>
                <td>{{strtoupper($user_sell->type)}}</td>
                <td>{{$user_sell->amount}}</td>
                <td>${{bcmul($user_sell->price,(1+(setting('public_fee')/100)),2)}}</td>
                <td>${{$user_sell->total}}</td>
                <td>{{$user_sell->status}}</td>
                <td>${{number_format(($user_sell->amount * $user_sell->price)*(setting('public_fee') / 100),2)}}</td>
            </tr>
            @endforeach
        </table>
        @else
        <p style="font-family: 'greycliff-regular';font-size: 18px;">There are no sell orders to show.</p>
        @endif
    </div>
    </div>
@endsection
