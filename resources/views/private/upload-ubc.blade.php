@extends('layouts.private')

@section('page-heading', 'Upload UBC')
@section('title', 'Upload UBC')
@section('page-sub-heading', '')

@section('content')
<div class="general-content-area">
    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <form action="/account/upload_ubc" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if (isset($ubc_location))
            <label>You've successfully uploaded your selfie.</label><br>
            <img width="200" src="/storage/{{ $ubc_location }}">
        @else
            <p>Please upload a selfie with you holding your UBC.</p>
            <label>Selfie of UBC:</label>
            <label for="ubc" class="drop-label">Select file</label>
            <input type="file" id="ubc" name="ubc" />
            <br /><br />
            <input type="submit" class="btn btn-teal" value="Upload" />
        @endif


    </form>
</div>
@endsection
