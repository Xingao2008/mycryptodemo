@extends('layouts.app')

@section('content')



    <div class="dashboard-content">
        <div class="wrapper flex-wrap">
            <div class="container" id="root">

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script src="{{ mix('/js/exchange.js') }}"></script>
@endpush

@section('custom_styles')
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/css/exchange.css') }}">
@endsection