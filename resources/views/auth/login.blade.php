@extends('layouts.app')

@section('content')
    <div class="container login">
        <div class="main" id="login-form" v-cloak>
            <div class="page-heading"><h2>Login</h2></div>
            @include('flash::message')
            <div class="alert alert-danger" v-if="has_error">
                @{{ error_message }}
            </div>
            <div class="alert alert-success" v-if="is_successful">
               @{{ success_message }}
            </div>
            <form class="form-login" @submit.prevent="loginForm()" v-if="!is_successful">

                <div class="form-group" v-if="form_mode === 1">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <input id="email" type="email" class="form-control" name="email" v-model="email" required autofocus>
                </div>
                <div class="form-group" v-if="form_mode === 1">
                    <label for="password" class="col-md-4 control-label">Password</label>
                    <input id="password" type="password" class="form-control" name="password" v-model="password"
                           required>
                </div>
                <div class="tfa-input form-group" v-if="form_mode === 2">
                    <label for=tfa class="col-md-4 control-label">Two-Factor Auth Code</label>
                    <input id="text" type="tfa" class="form-control" name="tfa" style="border: 1px solid;"
                           :required="tfa" v-model="tfa">
                </div>

                <div class="form-group" v-if="form_mode === 1">
                    <div class="checkbox">
                        <label>
                            <!--<input type="checkbox" name="remember" v-model="remember"> Remember Me-->
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div v-if="form_mode === 1 || form_mode === 2">
                        <button class="button is-primary is-medium" :disabled="!loginButton" v-if="!processing">
                            Login
                        </button>
                        <button v-if="processing" class="button is-primary is-medium is-loading" disabled>
                            Loading
                            <!-- <div class="spinner">
                                <div class="rect1"></div>
                                <div class="rect2"></div>
                                <div class="rect3"></div>
                                <div class="rect4"></div>
                                <div class="rect5"></div>
                            </div> -->
                        </button>
                    </div>
                    <div v-if="form_mode === 3">
                        <div class="alert alert-danger">
                            <b>Warning:</b> You do not have Two-Factor-Authentication set up and your account is at risk.
                        </div>
                        <button class="btn btn-teal" @click.prevent="goDashboard()">
                            Continue
                        </button>
                        <button class="btn btn-red" @click.prevent="goTfa()">
                            Setup TFA Now!
                        </button>
                    </div>
                    <div class="center" v-if="form_mode === 1">
                        <a class="minor-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('custom_scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#login-form',
            data: {
                email: "{{ old('email') }}",
                password: "{{ old('password') }}",
                tfa: "{{ old('tfa') }}",
                csrf_token: "{{ csrf_token() }}",
                processing: false,
                form_mode: 1,
                remember: "{{ old('remember', 0) }}",
                has_error: false,
                error_message: "",
                is_successful: false,
                success_message: "",
                tfa_status: 0,
                token: 0
            },
            methods: {
                loginForm: function () {

                    if (!this.loginButton) {
                        return;
                    }

                    var vm = this;

                    vm.processing = true;
                    vm.has_error = false;

                    axios.post("{{ url()->to('login') }}", {
                            email: vm.email,
                            password: vm.password,
                            remember: vm.remember,
                            tfa: vm.tfa
                        })
                        .then(function (response) {
                            vm.processSuccess(response);
                        })
                        .catch(function (error) {
                            vm.processErrors(error);
                        });
                },

                processErrors: function (error) {
                    error_code = error.response.data.error;
                    switch(error_code){
                        case 'invalid_login':
                        case 'invalid_input':
                            this.error_message = "Invalid email and/or password, please try again";
                            break;
                        case 'tfa_failed':
                            this.error_message = "Invalid TFA Token, please try again. If you continue to have issues, contact staff.";
                            break;
                        case 'login_lockout':
                            this.error_message = "You have been locked out of this account due to too many unsuccessful login attempts. Please contact staff if you're having issues.";
                            break;

                    }
                    this.has_error = true;
                    this.processing = false;
                },

                processSuccess: function (rsp) {
                    this.has_error = false;
                    switch (rsp.data.message) {
                        case 'login_successful':
                            // If tfa_status = 0, we tell them to enable it
                            this.tfa_status = rsp.data.tfa_status;
                            this.token = rsp.data.token;
                            this.uid = rsp.data.uid;
                            this.$cookie.remove('api_token');
                            this.$cookie.set('api_token', this.token, 1);
                            this.$cookie.remove('uid');
                            this.$cookie.set('uid', this.uid, 1);
                            if(this.tfa_status === 1)
                            {
                                this.goDashboard();
                            }
                            else
                            {
                                this.form_mode = 3;
                            }
                            break;
                        case 'tfa_required':
                            this.form_mode = 2;
                            break;
                    }

                    this.processing = false;
                },

                goDashboard: function() {
                    this.is_successful = true;
                    this.success_message = "You have successfully logged in! We will redirect you now.";
                    window.location = "{{ url()->to('dashboard') }}";
                },

                goTfa: function() {
                    this.is_successful = true;
                    this.success_message = "You have successfully logged in! We will redirect you now.";
                    window.location = "{{ url()->to('account/tfa') }}";
                }
            },
            computed: {
                loginButton: function () {
                    if (this.form_mode === 1) {
                        return (this.password !== "" && this.email !== "");
                    }
                    else {
                        return (this.password !== "" && this.email !== "" && this.tfa !== "");
                    }
                }
            },
            watch: {}
        });
    </script>
@endsection