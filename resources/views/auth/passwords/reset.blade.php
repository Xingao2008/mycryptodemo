@extends('layouts.app')

@section('custom_styles')
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .my-padding {
            padding: 2%;
        }
        /* customize vuetify css*/
        .theme--light {
            background-color: transparent !important;
        }
        .v-text-field__slot,
        .v-menu__activator,
        .v-select__slot{
            height: auto;
        }
        .application--wrap {
            min-height: auto;
        }
        .inner-content {
            margin: 30px;
        }
        h2 {
            font-size: 28px;
        }
    </style>
@endsection

@section('content')
    <div class="inner-content">

        <v-app>
            <v-content>
                <v-container>

                    <v-alert class="notification is-danger" v-model="alert" :type="alertType">
                        @{{ alertMessage }}
                    </v-alert>

                    <v-layout row class="text-xs-center">
                        <v-flex d-flex xs12 sm12 md12 class="my-padding">

                            <v-container>
                                <v-card flat>
                                    <v-card-title primary-title>
                                        <h2>Reset your password</h2>
                                    </v-card-title>
                                    <v-form class="my-height">
                                        <v-text-field
                                                v-model="email"
                                                :error-messages="errorMessages"
                                                label="Email"
                                                :rules="[ validEmail ]"
                                        ></v-text-field>
                                        <v-text-field
                                                v-model="password"
                                                :append-icon="e1 ? 'visibility' : 'visibility_off'"
                                                :append-icon-cb="() => (e1 = !e1)"
                                                :type="e1 ? 'password' : 'text'"
                                                :error-messages="errorMessages"
                                                hint="At least 8 characters"
                                                min="8"
                                                counter
                                                label="New Password"
                                                :rules="[ validPass ]"
                                                required
                                        ></v-text-field>
                                        <v-text-field
                                                v-model="confirm_password"
                                                :append-icon="e2 ? 'visibility' : 'visibility_off'"
                                                :append-icon-cb="() => (e2 = !e2)"
                                                :type="e2 ? 'password' : 'text'"
                                                :error-messages="errorMessages"
                                                hint="At least 8 characters"
                                                min="8"
                                                counter
                                                label="Confirm Password"
                                                :rules="[ validPass, matchPass ]"
                                                required
                                        ></v-text-field>
                                        <v-card-actions>
                                            <v-btn color="primary" large block @click.prevent="updatePassword" :disabled="disabled">Update Password <i v-if="spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                                        </v-card-actions>
                                    </v-form>
                                </v-card>
                            </v-container>

                        </v-flex>
                    </v-layout>

                </v-container>
            </v-content>
        </v-app>

    </div>
@endsection

@section('custom_scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.1.5/vuetify.min.js"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                e1: true,
                e2: true,

                alert: false,
                alertType: 'error',
                alertMessage: 'Password update fails.',

                email: '',
                password: '',
                confirm_password: '',
                errorMessages: [],
                spinner: false,
                disabled: false
            },
            methods: {
                validEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    this.errorMessages = []
                    if (!re.test(email) || email === '') {
                        return false || 'Email is not valid'
                    } else {
                        return true
                    }

                },

                validPass(pass) {
                    var re = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,32}$/
                    this.errorMessages = []
                    if (!re.test(pass) || pass === '') {
                        return false || 'Password needs to contain minimum 8 characters, one number, one special character, one low or upper case'
                    } else {
                        return true
                    }
                },

                matchPass() {
                    var n = this.password.localeCompare(this.confirm_password)
                    this.errorMessages = []
                    if ( n === 0 ) {
                        return true
                    } else {
                        return false || 'Password needs to be match'
                    }
                },

                updatePassword() {
                    let vm = this
                    vm.spinner = true
                    if ( vm.matchPass() && vm.validPass(vm.password) === true ) {
                        axios.post('/password/new_reset',
                            {
                                email: vm.email,
                                password: vm.password,
                                confirm_password: vm.confirm_password

                            }).then(function(response) {
                            vm.alert = true
                            vm.spinner = false
                            if ( response.data.state == 200) {
                                vm.alertType = 'success'
                                vm.alertMessage = response.data.msg
                                window.location.href = '/login'
                            } else {
                                vm.alertType = 'error'
                                vm.alertMessage = response.data.msg
                            }
                        }).catch(function(error) {
                            vm.alert = true
                            vm.spinner = false
                            vm.alertType = 'error'
                            vm.alertMessage = error.data.msg
                        })
                    } else {
                        vm.alert = true
                        vm.spinner = false
                        vm.alertType = 'error'
                        vm.alertMessage = 'Password update fails.'
                    }
                }
            },
            watch: {
                spinner: function(val) {
                    if (val)
                        this.disabled = true
                    else
                        this.disabled = false
                },
            }
        })
    </script>
@endsection
