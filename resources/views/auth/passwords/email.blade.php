@extends('layouts.app')

@section('custom_styles')
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .my-padding {
            padding: 2%;
        }
        /* customize vuetify css*/
        .theme--light {
            background-color: transparent !important;
        }
        .v-text-field__slot,
        .v-menu__activator,
        .v-select__slot{
            height: auto;
        }
        .application--wrap {
            min-height: auto;
        }
        .inner-content {
            margin: 30px;
        }
        h2 {
            font-size: 28px;
        }
        /* v-cloak hide ugly loading messages */
        [v-cloak] > * { display:none; }
        [v-cloak]::before { content: "loading…" }
        .password-wrapper {
            max-width:800px;
            margin:0 auto;
        }
        .v-input.v-text-field {
            padding: 0 10px;
        }
    </style>
@endsection

@section('content')
    <div class="inner-content">

        <div class="password-wrapper">
            <v-app v-cloak>
                <v-content>
                    <v-container>
                        <v-layout row class="text-xs-center">
                            <v-flex d-flex xs12 sm12 md12 class="my-padding">

                                <v-container>
                                <v-alert class="notification is-danger" v-model="alert" :type="alertType">
                                    @{{ alertMessage }}
                                </v-alert>
                                    <v-card flat>
                                        <v-card-title primary-title>
                                            <h2 class="page-title-heading">Reset Password</h2>
                                        </v-card-title>
                                        <v-form class="my-height">
                                            <v-text-field
                                                    v-model="email"
                                                    :error-messages="errorMessages"
                                                    label="Email"
                                                    :rules="[ validEmail ]"
                                            ></v-text-field>
                                            <v-card-actions>
                                                <v-btn color="primary" large block @click.prevent="sentLink" :disabled="disabled">Send Password Reset Link <i v-if="spinner" class="fa fa-spinner fa-spin" style="font-size:24px"></i></v-btn>
                                            </v-card-actions>
                                        </v-form>
                                    </v-card>
                                </v-container>

                            </v-flex>
                        </v-layout>

                    </v-container>
                </v-content>
            </v-app>
        </div>

    </div>
@endsection

@section('custom_scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuetify/1.1.5/vuetify.min.js"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                email: '',
                errorMessages: [],
                spinner: false,
                disabled: false,
                alert: false,
                alertType: 'error',
                alertMessage: 'There was a problem, please try again before go next step.'
            },
            methods: {
                validEmail(email) {
                    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    this.errorMessages = []
                    if (!re.test(email) || email === '') {
                        return false || 'Email is not valid'
                    } else {
                        return true
                    }

                },

                sentLink() {
                    let vm = this
                    vm.spinner = true
                    axios.post('password/email',
                        {
                            email: vm.email
                        })
                        .then(function(response) {
                            vm.alert = true
                            vm.spinner = false
                            if ( response.data.state == 200) {
                                vm.alertType = 'success'
                                vm.alertMessage = response.data.msg
                            } else {
                                vm.alertType = 'error'
                                vm.alertMessage = response.data.msg
                            }
                        })
                        .catch(function(error) {
                            vm.alert = true
                            vm.spinner = false
                            vm.alertType = 'error'
                            vm.alertMessage = error.data.msg
                        })
                }
            }
        })
    </script>
@endsection
