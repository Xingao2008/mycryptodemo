@extends('layouts.app')
@section('content')

    <div class="register-content">
        <div class="wrapper flex-wrap">
            <div class="container" id="root">

            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('/js/register_flow.js') }}"></script>
@endpush

@section('custom_styles')
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/css/register_flow.css') }}">
@endsection
