<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <base href="{{ url()->to('/') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ url('img/favicon.ico') }}" type="images/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', '') :: myCryptoWallet</title>

    <!-- Styles -->
    {!! Assets::css() !!}
    <!-- Latest compiled and minified CSS -->
    <link href="{{ get_app_css() }}" rel="stylesheet">

    @section('custom_styles')

    <!-- JS -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108557919-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-108557919-1');
    </script>
</head>
<body @if (Route::getCurrentRoute()->uri() != '/')
        class="not-home"
      @endif>
    <div id="app">
        <!-- Real time pricing banner -->
        @include ('public.partials.top-price-banner')
        @if(!Auth::guest())
            <div class="notification is-primary" style="padding: 15px;font-size: 14px;margin-bottom:0;text-align:center;">
                <div class="overlay">Welcome to myCryptoWallet! The 🔥EXCHANGE🔥 has launched! <a href="/exchange">Click here to start trading!</a> Email issues to <a href="mailto:support@mycryptowallet.com.au">support@mycryptowallet.com.au</a>
                </div>
            </div>
            @if(setting('banner_message'))
                <div class="notification is-primary" style="padding: 15px;font-size: 14px;margin-bottom:0;text-align:center;">
                    <div class="overlay">
                        {!! setting('banner_message') !!}
                    </div>
                </div>
            @endif
            @impersonating
                <div class="notification is-primary" style="padding: 15px;font-size: 14px;margin-bottom:0;text-align:center;">
                    <div class="overlay">
                        You are impersonating {{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }} -
                        <a href="{{ route('admin.impersonate.leave', Auth::user()->id) }}" style="color:#16168e;">Leave impersonation</a>
                    </div>
                </div>
            @endImpersonating
        @endif
        @include('public.partials.navigation')
        @if(!setting('deposits_active') && Auth::user())
            <div class="alert alert-damger alert-dismissible" style="padding: 10px;margin-bottom: 0px; z-index:100;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 style="display:inline;"><i class="icon fa fa-warning"></i> Warning!</h3>
                <h4 style="display:inline;">Deposits are disabled for maintenance..</h4>
            </div>
        @endif
        @if(!setting('withdraws_active') && Auth::user())
            <div class="alert alert-damger alert-dismissible" style="padding: 10px;margin-bottom: 0px; z-index:100;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 style="display:inline;"><i class="icon fa fa-warning"></i> Warning!</h3>
                <h4 style="display:inline;">Withdrawals are disabled for maintenance..</h4>
            </div>
        @endif
        @if(!setting('sells_active') && Auth::user())
            <div class="alert alert-damger alert-dismissible" style="padding: 10px;margin-bottom: 0px; z-index:100;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 style="display:inline;"><i class="icon fa fa-warning"></i> Warning!</h3>
                <h4 style="display:inline;">Selling functionalities are disabled for maintenance..</h4>
            </div>
        @endif
        @if(!setting('buys_active') && Auth::user())
            <div class="alert alert-damger alert-dismissible" style="padding: 10px;margin-bottom: 0px; z-index:100;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 style="display:inline;"><i class="icon fa fa-warning"></i> Warning!</h3>
                <h4 style="display:inline;">Buying functionalities are disabled for maintenance..</h4>
            </div>
        @endif
        @if(setting('maintenance_active'))
            <div class="alert alert-damger alert-dismissible" style="padding: 10px;margin-bottom: 0px; z-index:100; display:block;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h3 style="display:inline;"><i class="icon fa fa-warning"></i> Heads up!</h3>
                <h4 style="display:inline;">The website is in maintenance, so logins, deposits, withdrawals and buy/sells are temporarily unavailable.</h4>
            </div>
        @endif
        <div class="dashboard @yield('container')">
            <div class="main">
                <div class="page-heading with-breadcrumb">
                    <h2>@yield('page-heading', '')</h2>
                     <br/><p>@yield('page-sub-heading', '')</p>
                    <!-- <div class="breadcrumb">
                        @yield('breadcrumbs')
                    </div> -->
                </div>
                <div class="container-fluid account-page">
                    @include('flash::message')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="dashboard-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- include footer from partials folder -->
    @include ('public.partials.footer')
    <!-- Scripts -->
    {!! Assets::js() !!}
    <script>
      $(function() {
        var date = new Date();
        $( "#datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
            dateFormat: 'dd/mm/yy',
            yearRange: "1900:1999"
        });
      });
    </script>
    @yield('custom_scripts')
    @stack('scripts')
</body>
</html>
