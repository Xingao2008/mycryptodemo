<form role="form" method="POST" action="{{ url()->to('admin/settings/store') }}">
    {{ csrf_field() }}
    <input type="hidden" name="hiddentype" value="settings" required>
    <div class="container-fluid">
        <div class="row">
                @foreach($settings as $setting)
                    @if($setting->is_boolean)
                        <input type="hidden" name="{{ $setting->id }}" value="0" />
                        {!! BootForm::checkbox($setting->id, $setting->name, 1, $setting->value) !!}
                    @else
                        <div class="form-group">
                            <label for="name">{{ $setting->name }}</label>
                            <input type="text" class="form-control" id="{{ $setting->id }}" name="{{ $setting->id }}" value="{{ $setting->value }}" required>
                        </div>
                    @endif
                @endforeach
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>