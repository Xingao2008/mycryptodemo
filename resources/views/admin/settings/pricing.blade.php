<div class="row">
    <div class="col-md-6">
        <div><p><b>Note:</b> Prices on the right <b>include</b> any markup saved here previously, <b>plus</b> the hidden fee,
                but <u>does <b>not</b> include the public fee.</u></p></div>
        <form role="form" method="POST" action="/admin/settings/store">
            {{ csrf_field() }}
            <input type="hidden" name="hiddentype" value="pricing" required>
            <div class="form-group">
                <label for="name">BTC mark-up fee (%):</label>
                <input style="width:70px;" type="text" class="form-control" id="btc_fee" name="btc_fee"
                       value="{{ $pricing['btc_fee'] or 0 }}" required>
            </div>

            <div class="form-group">
                <label for="name">ETH mark-up fee (%):</label>
                <input style="width:70px;" type="text" class="form-control" id="eth_fee" name="eth_fee"
                       value="{{ $pricing['eth_fee'] or 0 }}" required>
            </div>

            <div class="form-group">
                <label for="name">LTC mark-up fee (%):</label>
                <input style="width:70px;" type="text" class="form-control" id="ltc_fee" name="ltc_fee"
                       value="{{ $pricing['ltc_fee'] or 0 }}" required>
            </div>

            <div class="form-group">
                <label for="name">XRP mark-up fee (%):</label>
                <input style="width:70px;" type="text" class="form-control" id="xrp_fee" name="xrp_fee"
                       value="{{ $pricing['xrp_fee'] or 0 }}" required>
            </div>
            <div class="form-group">
                <label for="name">POWR mark-up fee (%):</label>
                <input style="width:70px;" type="text" class="form-control" id="pwr_fee" name="pwr_fee"
                       value="{{ $pricing['pwr_fee'] or 0 }}" required>
            </div>
                <input type="hidden" name="btc_id" value="0">
                <input type="hidden" name="ltc_id" value="0">
                <input type="hidden" name="eth_id" value="0">
                <input type="hidden" name="xrp_id" value="0">
                <input type="hidden" name="pwr_id" value="0">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>

        <h2>Modify previous entries</h2>
        @if(isset($price_history))
            @foreach ($price_history as $key=>$history)
            <?php if(isset($nextkey)){print $key . " - " . $nextkey; $disabled = "required";} else {print $key." - now"; $disabled = "disabled";} ?>
            <form role="form" method="POST" action="/admin/settings/store">
                {{ csrf_field() }}
                <input type="hidden" name="hiddentype" value="pricing" >
                <input type="hidden" name="subpricing" value="subpricing" >
                <div class="form-group" style="float:left;">
                    <label for="name">BTC (%):</label>
                    <input type="hidden" name="btc_id" value="{{$history['btc']['id']}}">
                    <input style="width:70px;" type="text" class="form-control" id="btc_fee" name="btc_fee"
                           value="{{ $history['btc']['value']}}"  {{$disabled}}>
                </div>

                <div class="form-group" style="float:left;">
                    <label for="name">ETH (%):</label>
                    <input type="hidden" name="eth_id" value="{{$history['eth']['id']}}">
                    <input style="width:70px;" type="text" class="form-control" id="eth_fee" name="eth_fee"
                           value="{{ $history['eth']['value']}}"  {{$disabled}}>
                </div>

                <div class="form-group" style="float:left;">
                    <label for="name">LTC (%):</label>
                    <input type="hidden" name="ltc_id" value="{{$history['ltc']['id']}}">
                    <input style="width:70px;" type="text" class="form-control" id="ltc_fee" name="ltc_fee"
                           value="{{ $history['ltc']['value']}}" {{$disabled}}>
                </div>

                <div class="form-group" style="float:left;">
                    <label for="name">XRP (%):</label>
                    <input type="hidden" name="xrp_id" value="{{$history['xrp']['id']}}">
                    <input style="width:70px;" type="text" class="form-control" id="xrp_fee" name="xrp_fee"
                           value="{{ $history['xrp']['value']}}" {{$disabled}}>
                </div>

                <div class="form-group" style="float:left;">
                    <label for="name">POWR (%):</label>
                    <input type="hidden" name="pwr_id" value="{{$history['pwr']['id']}}">
                    <input style="width:70px;" type="text" class="form-control" id="pwr_fee" name="pwr_fee"
                           value="{{ $history['pwr']['value']}}" {{$disabled}}>
                </div>

                <div class="form-group" style="display:inline;">
                    <button type="submit" class="btn btn-primary" style="margin-left: 10px; margin-top: 20px;" {{$disabled}}>Update</button>
                </div>
            </form>
            <br><br>
            <?php $nextkey = $key; ?>
            @endforeach
        @endif
    </div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td>
                    <b>BTC Buy Price</b> <br>
                    Original: ${{ number_format($prices_original['btc']->ask, 2, '.', '')}}<br>
                    Now: $<span id="btc_price">{{ number_format($prices_original['btc']->ask, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24btc']->ask, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24btc_price">{{ number_format($prices_original['24btc']->ask, 2, '.', '') }}</span><br><br>

                    <b>ETH Buy Price</b> <br>
                    Original: ${{number_format($prices_original['eth']->ask, 2, '.', '')}}<br>
                    Now: $<span id="eth_price">{{ number_format($prices_original['eth']->ask, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24eth']->ask, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24eth_price">{{ number_format($prices_original['24eth']->ask, 2, '.', '') }}</span><br><br>

                    <b>LTC Buy Price</b> <br>
                    Original: ${{number_format($prices_original['ltc']->ask, 2, '.', '')}}<br>
                    Now: $<span id="ltc_price">{{ number_format($prices_original['ltc']->ask, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24ltc']->ask, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24ltc_price">{{ number_format($prices_original['24ltc']->ask, 2, '.', '') }}</span><br><br>

                    <b>XRP Buy Price</b> <br>
                    Original: ${{number_format($prices_original['xrp']->ask, 2, '.', '')}}<br>
                    Now: $<span id="xrp_price">{{ number_format($prices_original['xrp']->ask, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24xrp']->ask, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24xrp_price">{{ number_format($prices_original['24xrp']->ask, 2, '.', '') }}</span><br><br>

                    <b>POWR Buy Price</b> <br>
                    Original: ${{number_format($prices_original['pwr']->ask, 2, '.', '')}}<br>
                    Now: $<span id="pwr_price">{{ number_format($prices_original['pwr']->ask, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24pwr']->ask, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24pwr_price">{{ number_format($prices_original['24pwr']->ask, 2, '.', '') }}</span><br><br>
                </td>
                <td>
                    <b>BTC Sell Price</b> <br>
                    Original: ${{number_format($prices_original['btc']->bid, 2, '.', '')}}<br>
                    Now: $<span id="btc_price_sell">{{ number_format($prices_original['btc']->bid, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24btc']->bid, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24btc_price_sell">{{ number_format($prices_original['24btc']->bid, 2, '.', '') }}</span><br><br>

                    <b>ETH Sell Price</b> <br>
                    Original: ${{number_format($prices_original['eth']->bid, 2, '.', '')}}<br>
                    Now: $<span id="eth_price_sell">{{ number_format($prices_original['eth']->bid, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24eth']->bid, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24eth_price_sell">{{ number_format($prices_original['24eth']->bid, 2, '.', '') }}</span><br><br>

                    <b>LTC Sell Price</b> <br>
                    Original: ${{number_format($prices_original['ltc']->bid, 2, '.', '')}}<br>
                    Now: $<span id="ltc_price_sell">{{ number_format($prices_original['ltc']->bid, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24ltc']->bid, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24ltc_price_sell">{{ number_format($prices_original['24ltc']->bid, 2, '.', '') }}</span><br><br>

                    <b>XRP Sell Price</b> <br>
                    Original: ${{number_format($prices_original['xrp']->bid, 2, '.', '')}}<br>
                    Now: $<span id="xrp_price_sell">{{ number_format($prices_original['xrp']->bid, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24xrp']->bid, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24xrp_price_sell">{{ number_format($prices_original['24xrp']->bid, 2, '.', '') }}</span><br><br>

                    <b>POWR Sell Price</b> <br>
                    Original: ${{number_format($prices_original['pwr']->bid, 2, '.', '')}}<br>
                    Now: $<span id="pwr_price_sell">{{ number_format($prices_original['pwr']->bid, 2, '.', '') }}</span><br>
                    Original 24hrs ago: ${{ number_format($prices_original['24pwr']->bid, 2, '.', '') }}<br>
                    24hrs ago: $<span
                            id="24pwr_price_sell">{{ number_format($prices_original['24pwr']->bid, 2, '.', '') }}</span><br><br>
                </td>
            </tr>
        </table>
    </div>
</div>




<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        var btc_fee = document.getElementById('btc_fee');
        var eth_fee = document.getElementById('eth_fee');
        var ltc_fee = document.getElementById('ltc_fee');
        var xrp_fee = document.getElementById('xrp_fee');
        var pwr_fee = document.getElementById('pwr_fee');

        document.getElementById('btc_price').innerHTML = (((btc_fee.value / 100) * {{ $prices['btc']->ask }}) + {{ $prices['btc']->ask }}).toFixed(2);
        document.getElementById('24btc_price').innerHTML = (((btc_fee.value / 100) * {{ $prices['24btc']->ask }}) + {{ $prices['24btc']->ask }}).toFixed(2);
        document.getElementById('eth_price').innerHTML = (((eth_fee.value / 100) * {{ $prices['eth']->ask }}) + {{ $prices['eth']->ask }}).toFixed(2);
        document.getElementById('24eth_price').innerHTML = (((eth_fee.value / 100) * {{ $prices['24eth']->ask }}) + {{ $prices['24eth']->ask }}).toFixed(2);
        document.getElementById('ltc_price').innerHTML = (((ltc_fee.value / 100) * {{ $prices['ltc']->ask }}) + {{ $prices['ltc']->ask }}).toFixed(2);
        document.getElementById('24ltc_price').innerHTML = (((ltc_fee.value / 100) * {{ $prices['24ltc']->ask }}) + {{ $prices['24ltc']->ask }}).toFixed(2);
        document.getElementById('xrp_price').innerHTML = (((xrp_fee.value / 100) * {{ $prices['xrp']->ask }}) + {{ $prices['xrp']->ask }}).toFixed(2);
        document.getElementById('24xrp_price').innerHTML = (((xrp_fee.value / 100) * {{ $prices['24xrp']->ask }}) + {{ $prices['24xrp']->ask }}).toFixed(2);
        document.getElementById('pwr_price').innerHTML = (((pwr_fee.value / 100) * {{ $prices['pwr']->ask }}) + {{ $prices['pwr']->ask }}).toFixed(2);
        document.getElementById('24pwr_price').innerHTML = (((pwr_fee.value / 100) * {{ $prices['24pwr']->ask }}) + {{ $prices['24pwr']->ask }}).toFixed(2);

        document.getElementById('btc_price_sell').innerHTML = (({{ $prices['btc']->bid }} -(btc_fee.value / 100) * {{ $prices['btc']->bid }})).toFixed(2);
        document.getElementById('24btc_price_sell').innerHTML = (({{ $prices['24btc']->bid }} -(btc_fee.value / 100) * {{ $prices['24btc']->bid }})).toFixed(2);
        document.getElementById('eth_price_sell').innerHTML = (({{ $prices['eth']->bid }} -(eth_fee.value / 100) * {{ $prices['eth']->bid }})).toFixed(2);
        document.getElementById('24eth_price_sell').innerHTML = (({{ $prices['24eth']->bid }} -(eth_fee.value / 100) * {{ $prices['24eth']->bid }})).toFixed(2);
        document.getElementById('ltc_price_sell').innerHTML = (({{ $prices['ltc']->bid }} -(ltc_fee.value / 100) * {{ $prices['ltc']->bid }})).toFixed(2);
        document.getElementById('24ltc_price_sell').innerHTML = (({{ $prices['24ltc']->bid }} -(ltc_fee.value / 100) * {{ $prices['24ltc']->bid }})).toFixed(2);
        document.getElementById('xrp_price_sell').innerHTML = (({{ $prices['xrp']->bid }} -(xrp_fee.value / 100) * {{ $prices['xrp']->bid }})).toFixed(2);
        document.getElementById('24xrp_price_sell').innerHTML = (({{ $prices['24xrp']->bid }} -(xrp_fee.value / 100) * {{ $prices['24xrp']->bid }})).toFixed(2);
        document.getElementById('pwr_price_sell').innerHTML = (({{ $prices['pwr']->bid }} -(pwr_fee.value / 100) * {{ $prices['pwr']->bid }})).toFixed(2);
        document.getElementById('24pwr_price_sell').innerHTML = (({{ $prices['24pwr']->bid }} -(pwr_fee.value / 100) * {{ $prices['24pwr']->bid }})).toFixed(2);

        btc_fee.onkeyup = function () {
            document.getElementById('btc_price').innerHTML = (((btc_fee.value / 100) * {{ $prices['btc']->ask }}) + {{ $prices['btc']->ask }}).toFixed(2);
            document.getElementById('24btc_price').innerHTML = (((btc_fee.value / 100) * {{ $prices['24btc']->ask }}) + {{ $prices['24btc']->ask }}).toFixed(2);
            document.getElementById('btc_price_sell').innerHTML = (({{ $prices['btc']->bid }} -(btc_fee.value / 100) * {{ $prices['btc']->bid }})).toFixed(2);
            document.getElementById('24btc_price_sell').innerHTML = (({{ $prices['24btc']->bid }} -(btc_fee.value / 100) * {{ $prices['24btc']->bid }})).toFixed(2);
        }

        eth_fee.onkeyup = function () {
            document.getElementById('eth_price').innerHTML = (((eth_fee.value / 100) * {{ $prices['eth']->ask }}) + {{ $prices['eth']->ask }}).toFixed(2);
            document.getElementById('24eth_price').innerHTML = (((eth_fee.value / 100) * {{ $prices['24eth']->ask }}) + {{ $prices['24eth']->ask }}).toFixed(2);
            document.getElementById('eth_price_sell').innerHTML = (({{ $prices['eth']->bid }} -(eth_fee.value / 100) * {{ $prices['eth']->bid }})).toFixed(2);
            document.getElementById('24eth_price_sell').innerHTML = (({{ $prices['24eth']->bid }} -(eth_fee.value / 100) * {{ $prices['24eth']->bid }})).toFixed(2);
        }

        ltc_fee.onkeyup = function () {
            document.getElementById('ltc_price').innerHTML = (((ltc_fee.value / 100) * {{ $prices['ltc']->ask }}) + {{ $prices['ltc']->ask }}).toFixed(2);
            document.getElementById('24ltc_price').innerHTML = (((ltc_fee.value / 100) * {{ $prices['24ltc']->ask }}) + {{ $prices['24ltc']->ask }}).toFixed(2);
            document.getElementById('ltc_price_sell').innerHTML = (({{ $prices['ltc']->bid }} -(ltc_fee.value / 100) * {{ $prices['ltc']->bid }})).toFixed(2);
            document.getElementById('24ltc_price_sell').innerHTML = (({{ $prices['24ltc']->bid }} -(ltc_fee.value / 100) * {{ $prices['24ltc']->bid }})).toFixed(2);
        }

        xrp_fee.onkeyup = function () {
            document.getElementById('xrp_price').innerHTML = (((xrp_fee.value / 100) * {{ $prices['xrp']->ask }}) + {{ $prices['xrp']->ask }}).toFixed(2);
            document.getElementById('24xrp_price').innerHTML = (((xrp_fee.value / 100) * {{ $prices['24xrp']->ask }}) + {{ $prices['24xrp']->ask }}).toFixed(2);
            document.getElementById('xrp_price_sell').innerHTML = (({{ $prices['xrp']->bid }} -(xrp_fee.value / 100) * {{ $prices['xrp']->bid }})).toFixed(2);
            document.getElementById('24xrp_price_sell').innerHTML = (({{ $prices['24xrp']->bid }} -(xrp_fee.value / 100) * {{ $prices['24xrp']->bid }})).toFixed(2);
        }

        pwr_fee.onkeyup = function () {
            document.getElementById('pwr_price').innerHTML = (((pwr_fee.value / 100) * {{ $prices['pwr']->ask }}) + {{ $prices['pwr']->ask }}).toFixed(2);
            document.getElementById('24pwr_price').innerHTML = (((pwr_fee.value / 100) * {{ $prices['24pwr']->ask }}) + {{ $prices['24pwr']->ask }}).toFixed(2);
            document.getElementById('pwr_price_sell').innerHTML = (({{ $prices['pwr']->bid }} -(pwr_fee.value / 100) * {{ $prices['pwr']->bid }})).toFixed(2);
            document.getElementById('24pwr_price_sell').innerHTML = (({{ $prices['24pwr']->bid }} -(pwr_fee.value / 100) * {{ $prices['24pwr']->bid }})).toFixed(2);
        }

    });
</script>

