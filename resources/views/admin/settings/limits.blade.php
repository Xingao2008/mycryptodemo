<form role="form" method="POST" action="{{ url()->to('/admin/settings/store') }}">
    {{ csrf_field() }}
    <input type="hidden" name="hiddentype" value="limits" required>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sx-12">
                <div class="form-group">
                    <label for="name">Buy Transaction Limit:</label>
                    <input type="text" class="form-control" id="buy_transaction_limit" name="buy_transaction_limit" value="{{ $limits->get('buy_transaction_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">Buy Daily Limit:</label>
                    <input type="text" class="form-control" id="buy_daily_limit" name="buy_daily_limit" value="{{ $limits->get('buy_daily_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">Sell Transaction Limit:</label>
                    <input type="text" class="form-control" id="sell_transaction_limit" name="sell_transaction_limit" value="{{ $limits->get('sell_transaction_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">Sell Daily Limit:</label>
                    <input type="text" class="form-control" id="sell_daily_limit" name="sell_daily_limit" value="{{ $limits->get('sell_daily_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">POLi Transaction Limit:</label>
                    <input type="text" class="form-control" id="poli_transaction_limit" name="poli_transaction_limit" value="{{ $limits->get('poli_transaction_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">POLi Daily Limit:</label>
                    <input type="text" class="form-control" id="poli_daily_limit" name="poli_daily_limit" value="{{ $limits->get('poli_daily_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">BTC Withdraw Limit:</label>
                    <input type="text" class="form-control" id="btc_withdraw_limit" name="btc_withdraw_limit" value="{{ $limits->get('btc_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">BTC Daily Withdraw Limit:</label>
                    <input type="text" class="form-control" id="btc_daily_withdraw_limit" name="btc_daily_withdraw_limit" value="{{ $limits->get('btc_daily_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">LTC Withdraw Limit:</label>
                    <input type="text" class="form-control" id="ltc_withdraw_limit" name="ltc_withdraw_limit" value="{{ $limits->get('ltc_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">LTC Daily Withdraw Limit:</label>
                    <input type="text" class="form-control" id="ltc_daily_withdraw_limit" name="ltc_daily_withdraw_limit" value="{{ $limits->get('ltc_daily_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">ETH Withdraw Limit:</label>
                    <input type="text" class="form-control" id="eth_withdraw_limit" name="eth_withdraw_limit" value="{{ $limits->get('eth_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">ETH Daily Withdraw Limit:</label>
                    <input type="text" class="form-control" id="eth_daily_withdraw_limit" name="eth_daily_withdraw_limit" value="{{ $limits->get('eth_daily_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">XRP Withdraw Limit:</label>
                    <input type="text" class="form-control" id="xrp_withdraw_limit" name="xrp_withdraw_limit" value="{{ $limits->get('xrp_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">XRP Daily Withdraw Limit:</label>
                    <input type="text" class="form-control" id="xrp_daily_withdraw_limit" name="xrp_daily_withdraw_limit" value="{{ $limits->get('xrp_daily_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">POWR Withdraw Limit:</label>
                    <input type="text" class="form-control" id="pwr_withdraw_limit" name="pwr_withdraw_limit" value="{{ $limits->get('pwr_withdraw_limit', 0) }}" required>
                </div>

                <div class="form-group">
                    <label for="name">POWR Daily Withdraw Limit:</label>
                    <input type="text" class="form-control" id="pwr_daily_withdraw_limit" name="pwr_daily_withdraw_limit" value="{{ $limits->get('pwr_daily_withdraw_limit', 0) }}" required>
                </div>

            </div>
        </div>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
</form>
