@extends('admin.layouts.app')

@section('contentheader_title', 'Settings')

@section('content')
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Settings</a></li>
        <li><a href="#tab_2" data-toggle="tab">Limits</a></li>
        <li><a href="#tab_3" data-toggle="tab">Pricing</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            @include('admin.settings.settings')
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            @include('admin.settings.limits')
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
            @include('admin.settings.pricing')
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
@endsection
