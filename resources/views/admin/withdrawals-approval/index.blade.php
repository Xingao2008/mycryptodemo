@extends('admin.layouts.app')

@section('contentheader_title', 'Withdrawals Approval')

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>${{ number_format($withdraws->sum('amount'), 2) }}</h3>
                    <p>Sum of Withdraws</p>
                </div>
                <div class="icon">
                    <i class="ion ion-social-usd"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>${{ number_format($withdraws_unconfirmed->sum('amount'), 2) }}</h3>
                    <p>Sum of Uncomfirmed</p>
                </div>
                <div class="icon">
                    <i class="ion ion-social-usd-outline"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $withdraws->count() }}</h3>
                    <p>Pending Withdraws</p>
                </div>
                <div class="icon">
                    <i class="ion ion-play"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $withdraws_unconfirmed->count() }}</h3>
                    <p>Pending Unconfirmed Withdraws</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pause"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Withdrawals Approval</h3>
            <div style="float: right;">
                <a href="#" id="export" class="btn btn-primary btn-md">Export</a>
                <a href="#" id="approveAll" class="btn btn-info btn-md"><i style="display: none;" class="fa fa-spinner fa-spin" id="spinner"></i> Approve All</a>
            </div>
        </div>
        <div class="box-body">
            <div id="dvData" style="display: none;">
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>UBC</th>
                        <th>DOB</th>
                        <th>Address</th>
                        <th>Bank Name</th>
                        <th>Bank Branch</th>
                        <th>BSB</th>
                        <th>Account</th>
                        <th>Amount</th>
                        <th>Withdraw request date</th>
                    </tr>
                    @foreach ($withdraws as $withdraw)
                    <tr>
                        <td>{{ $withdraw->id }}</td>
                        <td>{{ $withdraw->user->name }}</td>
                        <td>{{ $withdraw->user->ubc }}</td>
                        <td>{{ $withdraw->user->dob }}</td>
                        <td>{{ $withdraw->address }}</td>
                        <td>{{ $withdraw->user->ubc }}</td>
                        <td>{{ $withdraw->user_bank->bank_name }}</td>
                        <td>{{ $withdraw->user_bank->bank_branch }}</td>
                        <td>{{ format_bsb($withdraw->user_bank->bank_bsb) }}</td>
                        <td>{{ number_format($withdraw->amount, 2, '.', '') }}</td>
                        <td>{{ $withdraw->amount }}</td>
                        <td>{{ $withdraw->created_at }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>

            <table id="js-withdrawals-approval-table" class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Bank Details</th>
                    <th>Amount</th>
                    <th>Withdraw request date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($withdraws as $withdraw)
                    @include('admin.withdrawals-approval.table-partial')
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Unconfirmed Withdrawals</h3>
        </div>
        <div class="box-body">
            <p>These withdrawal requests are from users who did not verify via SMS the withdrawal request. This usually
                means they ignored it, however, it may indicate a security breach in the account by someone trying to
                withdraw their FIAT, so treat carefully.</p>
            <table id="js-withdrawals-unconfirmed-table" class="table table-responsive table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Bank Details</th>
                    <th>Amount</th>
                    <th>Withdraw request date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($withdraws_unconfirmed as $withdraw)
                    @include('admin.withdrawals-approval.table-partial')
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var clipboard = new Clipboard('.clipboard', {
            text: function (trigger) {
                return $(trigger).parent().parent().find('input').val();
            }
        });

        clipboard.on('success', function (e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });

        $('button').tooltip({
            trigger: 'click',
            placement: 'bottom'
        });

        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
                .attr('data-original-title', message)
                .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function () {
                $(btn).tooltip('hide');
            }, 1000);
        }

        $('.user-link').click(function () {
            location.href = $(this).attr('href');
        });

        $("#export").on('click', function (event) {
            // CSV
            // event.preventDefault();
            var args = [$('#dvData>table'), 'export.csv'];

            exportTableToCSV.apply(this, args);

            // If CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });

        $("#approveAll").on('click', function(e) {

            e.preventDefault();
            var ids = [];
            $("#spinner").show();

            $('#dvData td:first-child').each(function() {
                ids.push($(this).text());
            });

            $.ajax({
                method: "POST",
                url: "/admin/withdrawals-approval/addAll",
                data: { ids: ids,  _token: '{{csrf_token()}}' }
            })
                .done(function( msg ) {
                    if (msg === 'success') {
                        $("#spinner").hide();
                        window.location = '/admin/withdrawals-approval?changes-saved=true';
                    }
                });
        });

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td,th)'),

                // Temporary delimiter characters unlikely to be typed by keyboard
                // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

                // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',

                // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row),
                        $cols = $row.find('td,th');

                    return $cols.map(function (j, col) {
                        var $col = $(col),
                            text = $col.text();

                        return text.replace(/"/g, '""'); // escape double quotes

                    }).get().join(tmpColDelim);

                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"';

            // Deliberate 'false', see comment below
            if (false && window.navigator.msSaveBlob) {

                var blob = new Blob([decodeURIComponent(csv)], {
                    type: 'text/csv;charset=utf8'
                });

                // Crashes in IE 10, IE 11 and Microsoft Edge
                // See MS Edge Issue #10396033: https://goo.gl/AEiSjJ
                // Hence, the deliberate 'false'
                // This is here just for completeness
                // Remove the 'false' at your own risk
                window.navigator.msSaveBlob(blob, filename);

            } else if (window.Blob && window.URL) {
                // HTML5 Blob
                var blob = new Blob([csv], { type: 'text/csv;charset=utf8' });
                var csvUrl = URL.createObjectURL(blob);

                $(this)
                    .attr({
                        'download': filename,
                        'href': csvUrl
                    });
            } else {
                // Data URI
                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

                $(this)
                    .attr({
                        'download': filename,
                        'href': csvData,
                        'target': '_blank'
                    });
            }
        }
    </script>
@endpush