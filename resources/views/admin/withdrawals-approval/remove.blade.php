@extends('admin.layouts.app')

@section('contentheader_title', 'Withdrawals Approval')


@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Withdrawals Approval for <strong>{{ $user->name }}</strong></h3>
    </div>
    <form id="withdrawal-approval-form" method="get" action="{{ url()->route('admin.withdrawalsApproval.doRemove', $withdraw->id) }}" >
        <div class="box-body">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$withdraw->id}}">
            <p><h3 style="display:inline;">WARNING:</h3> You are about to delete this withdraw request and return the funds (${{$withdraw->amount}}) to the user's balance.</p>
            <p>The user's ID is <strong>{{$user->id}}</strong>, the withdraw request's ID is <strong>{{$withdraw->id}}</strong> and the user's name is <strong>{{$user->name}}</strong>.</p>
            <div class="form-group">
                <label>Email user?</label><br />
                <input type="checkbox" data-toggle="toggle" name="send_email" id="send_email" value="1" />
            </div>
            <div class="form-group" id="reason_box" style="display:none;">
                <label for="reason">Message to use: </label>
                <textarea id="reason" class="form-control" name="reason"></textarea>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-danger">Delete and return funds to user</button>
            <a class="btn btn-info" href="{{ route('admin.withdrawalsApproval') }}">Cancel</a>
        </div>
    </form>
</div>
@endsection

@push('scripts')
    <script>
        $('#send_email').change(function(){
            if($(this).prop('checked')) {
                $('#reason_box').show().removeAttr('disabled');
            }
            else
            {
                $('#reason_box').hide().attr('disabled', 'disabled');
            }
        });
    </script>
@endpush