<tr>
    <td>{{ $withdraw->id }}</td>
    <td>
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user->name }}" type="text">
                <div class="input-group-btn">
                    <button class="btn user-link" href="{{ url()->route('admin.users.edit', $withdraw->user_id) }}" type="button"><i class="fa fa-user"></i></button>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">UBC</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user->ubc }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">DOB</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user->dob }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Address</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->address }}" type="text">
            </div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user_bank->bank_name }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Branch</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user_bank->bank_branch }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">BSB</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ format_bsb($withdraw->user_bank->bank_bsb) }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Account</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->user_bank->bank_number }}" type="text">
            </div>
        </div>
    </td>
    <td>
        <div class="form-group">
            <label for="name" class="control-label">Amount</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ number_format($withdraw->amount, 2, '.', '') }}" type="text">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label">Currency</label>
            <div class="input-group">
                <div class="input-group-btn">
                    <button  class="clipboard btn" type="button"><i class="fa fa-clipboard"></i></button>
                </div>
                <input class="form-control" readonly value="{{ $withdraw->currency }}" type="text">
            </div>
        </div>
    </td>
    <td>{{ $withdraw->created_at }} ({{ \Carbon\Carbon::parse($withdraw->created_at)->diffForHumans() }})</td>
    <td>
        <a href="{{ url()->to("admin/withdrawals-approval/{$withdraw->id}/add") }}" class="btn btn-primary">Approve</a> &nbsp;
        <a class="btn btn-danger" href="{{ url()->to("admin/withdrawals-approval/{$withdraw->id}/remove") }}">Return funds to user</a>
    </td>
</tr>