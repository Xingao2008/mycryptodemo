@extends('admin.layouts.app')

@section('contentheader_title', 'Withdrawals Approval')


@section('content')
<form id="fund-approval-form" method="post" class="form-horizontal">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Withdrawals Approval for <strong>{{ $user->name }}</strong></h3>
            </div>
            <div class="box-body">
                <p>You should only complete this once you've finished transferring the user their funds. Upon completion, an email will be sent to the user telling them their funds are on the way.</p>

                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="transaction_date" class="col-sm-2 control-label">Transfer Date</label>
                        <div class="col-sm-10">
                            <input name="transaction_date" type="date" id="date" class="form-control"  value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount" class="col-sm-2 control-label">Amount</label>
                        <div class="col-sm-10">
                            <input name="amount" type="text" class="form-control"  value="{{$withdraw->amount}}">
                            <p>Do not enter the dollar sign ($). Only enter the numbers (e.g. 19.45)</p>
                        </div>
                    </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-danger" href="{{ route('admin.withdrawalsApproval') }}">Cancel</a>
            </div>
        </div>
    </div>
</form>
<div class="col-md-4">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">FIAT Withdrawals</h3>
        </div>
        <div class="box-body">
            <p>Note: dates displayed are the date a myCryptoWallet administrator used the Funds Deposit form on the left.</p>
            @if(!$fiat_withdrawals->isEmpty())
                @foreach ($fiat_withdrawals as $fiat_withdrawal)
                    Withdrew ${{$fiat_withdrawal->amount}} via {{$fiat_withdrawal->source}} on {{$fiat_withdrawal->created_at}} | Notes: {{$fiat_withdrawal->notes}}<br>
                @endforeach
            @else
                The user doesn't have any deposits yet.
            @endif
        </div>
    </div>
</div>
@endsection
