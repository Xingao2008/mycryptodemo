@extends('admin.layouts.app')

@section('contentheader_title', 'TFA Resets')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">TFA Resets</h3>
    </div>
    <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th>Staff</th>
                    <th>Active</th>
                    <th>Complete</th>
                    <th>Evidence</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($tfa_resets as $reset)
                    <tr>
                        <td>{{ $reset->id }}</td>
                        <td>{{ $reset->user->name }}</td>
                        <td>{{ $reset->staff->name }}</td>
                        <td>{{ $reset->active ? 'Yes' : 'No' }}</td>
                        <td>{{ $reset->complete ? 'Yes' : 'No' }}</td>
                        <td>{{ $reset->getMedia()->count() }}</td>
                        <td>
                            <a href="{{ url()->to('admin/tfa_reset/' . $reset->id) }}" class="btn btn-primary">Evidence</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('table').DataTable();
    </script>
@endpush