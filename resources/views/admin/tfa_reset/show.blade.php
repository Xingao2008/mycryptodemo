@extends('admin.layouts.app')

@section('contentheader_title', 'TFA Resets')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"></h3>
    </div>
    <div class="box-body">

        <div class="row">
            <div class="col-md-6">
                <strong>Reason: </strong> {{ $tfa_reset->reason }}
            </div>

            <div class="col-md-2">
                <strong>User: </strong> {{ $tfa_reset->user->name }}
            </div>

            <div class="col-md-2">
                <strong>Staff: </strong> {{ $tfa_reset->staff->name }}
            </div>

            <div class="col-md-2">
                <strong>When: </strong> {{ $tfa_reset->created_at }}
            </div>
        </div>
    <hr/>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Filename</th>
                    <th>Size</th>
                    <th>Type</th>
                    <th>Link</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tfa_reset->getMedia() as $media)
                    <tr>
                        <td>{{ $media->id }}</td>
                        <td>{{ $media->file_name }}</td>
                        <td>{{ $media->size }}</td>
                        <td>{{ $media->mime_type }}</td>
                        <td>
                            <a href="{{ $media->disk == 's3' ? $media->getTemporaryUrl(\Carbon\Carbon::now()->addMinutes(5)) : $media->getUrl() }}" class="btn btn-info" target="_blank">
                                View
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection