@extends('admin.layouts.app')

@section('contentheader_title', 'Reset TFA')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Reset TFA for {{ $user->name }}</h3>
    </div>
    <div class="box-body">
        <p>NOTE: Upon submitting, the user will receive an SMS with a reset link that they have 1 Hour to open.</p>
        {!! BootForm::horizontal(['url' => url()->to('admin/tfa_reset/' . $tfa_reset->id)]) !!}

            {!! BootForm::textarea('reason') !!}


        {!! BootForm::file('evidence', 'Evidence (Attach multiple files)', ['id' => 'file-upload', 'multiple']) !!}

        {!! BootForm::submit() !!}

        {!! BootForm::close() !!}
    </div>
</div>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Uploaded Evidence</h3>
    </div>
    <div class="box-body">
        @foreach($tfa_reset->getMedia() as $media)
            <div class="col-md-2">
                <a href="{{ $media->disk == 's3' ? $media->getTemporaryUrl(\Carbon\Carbon::now()->addMinutes(5)) : $media->getUrl() }}" class="btn btn-info" target="_blank">
                    <img src="{{ $media->disk == 's3' ? $media->getTemporaryUrl(\Carbon\Carbon::now()->addMinutes(5)) : $media->getUrl() }}" class="img-responsive img-thumbnail">
                </a>
            </div>
        @endforeach
    </div>
</div>
@endsection

@push('scripts')

    <script>
        $("#file-upload").fileinput({
            theme: "explorer",
            uploadUrl: "{{ url()->route('admin.tfa_reset.upload', $tfa_reset->id) }}",
            preferIconicPreview: true, // this will force thumbnails to display icons for following file extensions
            previewFileIconSettings: { // configure your icon file extensions
            'doc': '<i class="fa fa-file-word-o text-primary"></i>',
                'xls': '<i class="fa fa-file-excel-o text-success"></i>',
                'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
                'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
                'htm': '<i class="fa fa-file-code-o text-info"></i>',
                'txt': '<i class="fa fa-file-text-o text-info"></i>',
                'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
                // note for these file types below no extension determination logic
                // has been configured (the keys itself will be used as extensions)
                'jpg': '<i class="fa fa-file-photo-o text-danger"></i>',
                'gif': '<i class="fa fa-file-photo-o text-muted"></i>',
                'png': '<i class="fa fa-file-photo-o text-primary"></i>'
        },
        previewFileExtSettings: { // configure the logic for determining icon file extensions
            'doc': function(ext) {
                return ext.match(/(doc|docx)$/i);
            },
            'xls': function(ext) {
                return ext.match(/(xls|xlsx)$/i);
            },
            'ppt': function(ext) {
                return ext.match(/(ppt|pptx)$/i);
            },
            'zip': function(ext) {
                return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
            },
            'htm': function(ext) {
                return ext.match(/(htm|html)$/i);
            },
            'txt': function(ext) {
                return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
            },
            'mov': function(ext) {
                return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
            },
            'mp3': function(ext) {
                return ext.match(/(mp3|wav)$/i);
            }
        }
        });

        $('textarea').wysihtml5();
    </script>
@endpush