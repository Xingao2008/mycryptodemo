@extends('admin.layouts.app')

@section('contentheader_title', 'User Limits')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">User Limits</h3>
    </div>
    <div class="box-body">
        <table id="user_limits" class="table table-responsive table-striped display">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        var base_url = '{{ url()->to("admin/user_limits") }}';
        $(function() {
            $('#user_limits').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.users.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'status', name: 'status' },
                    {
                        "mRender": function ( data, type, row ) {
                            return '<a href="' + base_url + '/' + row.id + '/edit" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                        }
                    }
                ]
            });
        });
    </script>
@endpush