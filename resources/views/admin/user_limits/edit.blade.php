@extends('admin.layouts.app')

@section('contentheader_title', 'Edit User Limits')

@section('content')

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">User Limits</h3>
    </div>
    <div class="box-body"  id="app">
        <form role="form" method="POST" action="{{ url()->to('/admin/user_limits/' . $user->id) }}">
            {{ csrf_field() }}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-sx-12">

                        <div class="form-group">
                            <label for="name">Buy Transaction Limit:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <input type="checkbox" v-model="buy_transaction_limit_checkbox">
                                </span>
                                <input type="text" class="form-control" id="buy_transaction_limit" name="buy_transaction_limit" placeholder="Default: {{ setting('buy_transaction_limit') }}" :readonly="!buy_transaction_limit_checkbox" v-model="buy_transaction_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">Buy Daily Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="buy_daily_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="buy_daily_limit" name="buy_daily_limit" placeholder="Default: {{ setting('buy_daily_limit') }}" :readonly="!buy_daily_limit_checkbox" v-model="buy_daily_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">Sell Transaction Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="sell_transaction_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="sell_transaction_limit" name="sell_transaction_limit" placeholder="Default: {{ setting('sell_transaction_limit') }}" :readonly="!sell_transaction_limit_checkbox" v-model="sell_transaction_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">Sell Daily Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="sell_daily_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="sell_daily_limit" name="sell_daily_limit" placeholder="Default: {{ setting('sell_daily_limit') }}" :readonly="!sell_daily_limit_checkbox" v-model="sell_daily_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">POLi Transaction Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="poli_transaction_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="poli_transaction_limit" name="poli_transaction_limit" placeholder="Default: {{ setting('poli_transaction_limit') }}" :readonly="!poli_transaction_limit_checkbox" v-model="poli_transaction_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">POLi Daily Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="poli_daily_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="poli_daily_limit" name="poli_daily_limit" placeholder="Default: {{ setting('poli_daily_limit') }}" :readonly="!poli_daily_limit_checkbox" v-model="poli_daily_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">BTC Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="btc_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="btc_withdraw_limit" name="btc_withdraw_limit" placeholder="Default: {{ setting('btc_withdraw_limit') }}" :readonly="!btc_withdraw_limit_checkbox" v-model="btc_withdraw_limit_value" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">BTC Daily Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="btc_daily_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="btc_daily_withdraw_limit" name="btc_daily_withdraw_limit" placeholder="Default: {{ setting('btc_daily_withdraw_limit') }}" :readonly="!btc_daily_withdraw_limit_checkbox" v-model="btc_daily_withdraw_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">LTC Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="ltc_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="ltc_withdraw_limit" name="ltc_withdraw_limit" placeholder="Default: {{ setting('ltc_withdraw_limit') }}" :readonly="!ltc_withdraw_limit_checkbox" v-model="ltc_withdraw_limit_value" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">LTC Daily Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="ltc_daily_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="ltc_daily_withdraw_limit" name="ltc_daily_withdraw_limit" placeholder="Default: {{ setting('ltc_daily_withdraw_limit') }}" :readonly="!ltc_daily_withdraw_limit_checkbox" v-model="ltc_daily_withdraw_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">ETH Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="eth_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="eth_withdraw_limit" name="eth_withdraw_limit" placeholder="Default: {{ setting('eth_withdraw_limit') }}" :readonly="!eth_withdraw_limit_checkbox" v-model="eth_withdraw_limit_value" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">ETH Daily Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="eth_daily_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="eth_daily_withdraw_limit" name="eth_daily_withdraw_limit" placeholder="Default: {{ setting('eth_daily_withdraw_limit') }}" :readonly="!eth_daily_withdraw_limit_checkbox" v-model="eth_daily_withdraw_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">XRP Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="xrp_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="xrp_withdraw_limit" name="xrp_withdraw_limit" placeholder="Default: {{ setting('xrp_withdraw_limit') }}" :readonly="!xrp_withdraw_limit_checkbox" v-model="xrp_withdraw_limit_value" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">XRP Daily Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="xrp_daily_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="xrp_daily_withdraw_limit" name="xrp_daily_withdraw_limit" placeholder="Default: {{ setting('xrp_daily_withdraw_limit') }}" :readonly="!xrp_daily_withdraw_limit_checkbox" v-model="xrp_daily_withdraw_limit_value" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">POWR Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="pwr_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="pwr_withdraw_limit" name="pwr_withdraw_limit" placeholder="Default: {{ setting('pwr_withdraw_limit') }}" :readonly="!pwr_withdraw_limit_checkbox" v-model="pwr_withdraw_limit_value" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">POWR Daily Withdraw Limit:</label>
                            <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" v-model="pwr_daily_withdraw_limit_checkbox">
                                                    </span>
                                <input type="text" class="form-control" id="pwr_daily_withdraw_limit" name="pwr_daily_withdraw_limit" placeholder="Default: {{ setting('pwr_daily_withdraw_limit') }}" :readonly="!pwr_daily_withdraw_limit_checkbox" v-model="pwr_daily_withdraw_limit_value" required>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>

    </div>
</div>
@endsection

@push('scripts')
    <script>
        let vue = new Vue({
            el: '#app',
            data: {
                buy_daily_limit_value: '{{ $limits->get('buy_daily_limit', '') }}',
                buy_daily_limit_checkbox: parseInt('{{ $limits->get('buy_daily_limit', 0) ? 1 : 0 }}'),
                buy_transaction_limit_value: '{{ $limits->get('buy_transaction_limit', '') }}',
                buy_transaction_limit_checkbox: parseInt('{{ $limits->get('buy_transaction_limit', 0) ? 1 : 0 }}'),
                sell_daily_limit_value: '{{ $limits->get('sell_daily_limit', '') }}',
                sell_daily_limit_checkbox: parseInt('{{ $limits->get('sell_daily_limit', 0) ? 1 : 0 }}'),
                sell_transaction_limit_value: '{{ $limits->get('sell_transaction_limit', '') }}',
                sell_transaction_limit_checkbox: parseInt('{{ $limits->get('sell_transaction_limit', 0) ? 1 : 0 }}'),
                poli_daily_limit_value: '{{ $limits->get('poli_daily_limit', '') }}',
                poli_daily_limit_checkbox: parseInt('{{ $limits->get('poli_daily_limit', 0) ? 1 : 0 }}'),
                poli_transaction_limit_value: '{{ $limits->get('poli_transaction_limit', '') }}',
                poli_transaction_limit_checkbox: parseInt('{{ $limits->get('poli_transaction_limit', 0) ? 1 : 0 }}'),

                btc_daily_withdraw_limit_value: '{{ $limits->get('btc_daily_withdraw_limit', '') }}',
                btc_daily_withdraw_limit_checkbox: parseInt('{{ $limits->get('btc_daily_withdraw_limit', 0) ? 1 : 0 }}'),
                btc_withdraw_limit_value: '{{ $limits->get('btc_withdraw_limit', '') }}',
                btc_withdraw_limit_checkbox: parseInt('{{ $limits->get('btc_withdraw_limit', 0) ? 1 : 0 }}'),

                ltc_daily_withdraw_limit_value: '{{ $limits->get('ltc_daily_withdraw_limit', '') }}',
                ltc_daily_withdraw_limit_checkbox: parseInt('{{ $limits->get('ltc_daily_withdraw_limit', 0) ? 1 : 0 }}'),
                ltc_withdraw_limit_value: '{{ $limits->get('ltc_withdraw_limit', '') }}',
                ltc_withdraw_limit_checkbox: parseInt('{{ $limits->get('ltc_withdraw_limit', 0) ? 1 : 0 }}'),

                eth_daily_withdraw_limit_value: '{{ $limits->get('eth_daily_withdraw_limit', '') }}',
                eth_daily_withdraw_limit_checkbox: parseInt('{{ $limits->get('eth_daily_withdraw_limit', 0) ? 1 : 0 }}'),
                eth_withdraw_limit_value: '{{ $limits->get('eth_withdraw_limit', '') }}',
                eth_withdraw_limit_checkbox: parseInt('{{ $limits->get('eth_withdraw_limit', 0) ? 1 : 0 }}'),

                xrp_daily_withdraw_limit_value: '{{ $limits->get('xrp_daily_withdraw_limit', '') }}',
                xrp_daily_withdraw_limit_checkbox: parseInt('{{ $limits->get('xrp_daily_withdraw_limit', 0) ? 1 : 0 }}'),
                xrp_withdraw_limit_value: '{{ $limits->get('xrp_withdraw_limit', '') }}',
                xrp_withdraw_limit_checkbox: parseInt('{{ $limits->get('xrp_withdraw_limit', 0) ? 1 : 0 }}'),

                pwr_daily_withdraw_limit_value: '{{ $limits->get('pwr_daily_withdraw_limit', '') }}',
                pwr_daily_withdraw_limit_checkbox: parseInt('{{ $limits->get('pwr_daily_withdraw_limit', 0) ? 1 : 0 }}'),
                pwr_withdraw_limit_value: '{{ $limits->get('pwr_withdraw_limit', '') }}',
                pwr_withdraw_limit_checkbox: parseInt('{{ $limits->get('pwr_withdraw_limit', 0) ? 1 : 0 }}'),
            },
            watch: {
                buy_daily_limit_checkbox: function(val) {
                    if(!val) this.buy_daily_limit_value = '';
                    console.log(this.$data);
                },
                buy_transaction_limit_checkbox: function(val) {
                    if(!val) this.buy_transaction_limit_value = '';
                },
                sell_daily_limit_checkbox: function(val) {
                    if(!val) this.sell_daily_limit_value = '';
                },
                sell_transaction_limit_checkbox: function(val) {
                    if(!val) this.sell_transaction_limit_value = '';
                },
                poli_daily_limit_checkbox: function(val) {
                    if(!val) this.poli_daily_limit_value = '';
                },
                poli_transaction_limit_checkbox: function(val) {
                    if(!val) this.poli_transaction_limit_value = '';
                },

                btc_daily_withdraw_limit_checkbox: function(val) {
                    if(!val) this.btc_daily_withdraw_limit_value = '';
                },
                btc_withdraw_limit_checkbox: function(val) {
                    if(!val) this.btc_withdraw_limit_value = '';
                },

                ltc_daily_withdraw_limit_checkbox: function(val) {
                    if(!val) this.ltc_daily_withdraw_limit_value = '';
                },
                ltc_withdraw_limit_checkbox: function(val) {
                    if(!val) this.ltc_withdraw_limit_value = '';
                },

                eth_daily_withdraw_limit_checkbox: function(val) {
                    if(!val) this.eth_daily_withdraw_limit_value = '';
                },
                eth_withdraw_limit_checkbox: function(val) {
                    if(!val) this.eth_withdraw_limit_value = '';
                },

                xrp_daily_withdraw_limit_checkbox: function(val) {
                    if(!val) this.xrp_daily_withdraw_limit_value = '';
                },
                xrp_withdraw_limit_checkbox: function(val) {
                    if(!val) this.xrp_withdraw_limit_value = '';
                },

                pwr_daily_withdraw_limit_checkbox: function(val) {
                    if(!val) this.pwr_daily_withdraw_limit_value = '';
                },
                pwr_withdraw_limit_checkbox: function(val) {
                    if(!val) this.pwr_withdraw_limit_value = '';
                },
            }
        });
    </script>
@endpush

