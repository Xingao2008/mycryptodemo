<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en" />
    <title>YubiKey 2FA Registration</title>
    <script src="{!! secure_asset('vendor/u2f/u2f.js') !!}"></script>
    <script src="{!! secure_asset('vendor/u2f/app.js') !!}"></script>
    <style>
        body {
            background: #37517e;
            background: -moz-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: -webkit-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#37517e', endColorstr='#00b5ff',GradientType=1 );
            height: 100vh;
            width: 100%;
        }

        .panel, .panel-default>.panel-heading {
            background-color: #00000061;
            color: white;
            border-color: #00000061;
        }
    </style>
</head>
<body>



<div class="container" style="margin-top:50px">
    <div class="col-md-8 col-md-offset-2">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">{{ trans('u2f::messages.register.title') }}</h1>
            </div>
            <div class="panel-body" style="padding: 5px">

                <div class="alert alert-danger" role="alert" id="error" style="display: none"></div>
                <div class="alert alert-success" role="alert" id="success" style="display: none">
                    {{ trans('u2f::messages.success') }}
                </div>

                <div align="center">
                    <img src="https://ssl.gstatic.com/accounts/strongauth/Challenge_2SV-Gnubby_graphic.png" alt=""/>
                </div>

                <h3>
                    {{ trans('u2f::messages.insertKey') }}
                </h3>

                <p>
                    {{ trans('u2f::messages.buttonAdvise') }}
                    <br>
                    {{ trans('u2f::messages.noButtonAdvise') }}
                </p>
            </div>
        </div>
        <img src="{{ asset('img/logo.svg') }}">
    </div>
</div>



{!! Form::open(array('route' => 'u2f.register', 'id' => 'form')) !!}
    {!! Form::hidden('register', '', ['id' => 'register']) !!}
{!! Form::close() !!}

<script type="text/javascript">
    var sigs = {!! json_encode($currentKeys) !!};
    var req = {!! json_encode($registerData) !!};

    var errors = {
        1: "{{ trans('u2f::errors.other_error') }}",
        2: "{{ trans('u2f::errors.bad_request') }}",
        3: "{{ trans('u2f::errors.configuration_unsupported') }}",
        4: "{{ trans('u2f::errors.device_ineligible') }}",
        5: "{{ trans('u2f::errors.timeout') }}"
    };

    u2fClient.register(req, sigs, errors);
</script>
</body>
</html>
