@extends('admin.layouts.app')

@section('contentheader_title', 'Balances')

@section('content')
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Alter Balances</h3>
    </div>
    <div class="box-body">
        @if (session('changes-saved'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                Changes saved.
            </div>
        @endif
        <table id="balances-table" class="table display dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th></th>
                </tr>
            </tfoot>

        </table>
    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->
@endsection


@push('scripts')
    <script>
        var base_url = '{{ url()->to("admin/balances/") }}';
        $(function() {
            $('#balances-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.users.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'ubc', name: 'ubc' },
                    {
                        "mRender": function ( data, type, row ) {
                            return '<a href="' + base_url + '/' + row.id + '/list" class="btn btn-sm btn-primary">Edit</a>';
                        }
                    }
                ]
            });
        });
    </script>
@endpush