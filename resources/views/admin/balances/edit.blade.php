@extends('admin.layouts.app')

@section('contentheader_title', 'Balance Edit for ' . $user->name)

@section('content')
<!-- Default box -->
<form id="withdrawal-approval-form" method="post" >
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Balance Edit for <span>{{ $user->name }}</span></h3>
    </div>
    <div class="box-body">

            {{ csrf_field() }}
            <input name="currency" type="hidden" value="{{$balance->currency}}"/>
            <input name="locked" type="hidden" value="{{$balance->locked}}"/>
            <p>Current balance: {{$balance->balance}}</p>

            <p>Enter new balance for {{strtoupper($balance->currency)}}</p>
            {!! BootForm::text('balance', null, $balance->balance) !!}
        <p>Do not enter the dollar sign ($). Only enter the numbers (e.g. 19.45)</p>

    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Save</button>
        <a class="btn btn-danger" href="{{ route('admin.balances') }}">Cancel</a>
    </div>
</div>
</form>

@endsection
