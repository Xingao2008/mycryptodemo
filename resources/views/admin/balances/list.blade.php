@extends('admin.layouts.app')

@section('contentheader_title', 'Balances of ' . $user->name)

@section('content')
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Balances of {{ $user->name }}</h3>
    </div>
    <div class="box-body">
        <table id="js-balances-table" class="table display dataTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Currency</th>
                <th>Balance</th>
                <th>IsLocked</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($balances as $balance)
                <tr>
                    <td>{{ $balance->id }}</td>
                    <td>{{ $balance->currency }}</td>
                    <td>{{ $balance->balance }}</td>
                    <td>{{ $balance->locked }}</td>
                    <td><a href="{{ url('admin/balances/'. $balance->id . '/edit') }}" class="btn btn-primary">edit</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection