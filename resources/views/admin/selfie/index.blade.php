@extends('admin.layouts.app')

@section('contentheader_title', 'Users')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Users</h3>
        </div>
        <div class="box-body">
            <table id="users-table" class="table datatable display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>UBC</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->ubc }}</td>
                            <td>{{ $user->selfie }}</td>
                            <td>
                                <a href="{{ url()->to('admin/selfies/' . $user->id) }}" class="btn btn-primary">View</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#users-table').DataTable({});
    </script>
@endpush