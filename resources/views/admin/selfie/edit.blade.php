@extends('admin.layouts.app')

@section('contentheader_title', 'Verify ' . $user->name)

@section('content')
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Details</h3>
            </div>
            <div class="box-body">
                <table class="table table-responsive table-striped">
                    <tr>
                        <td>Full Name</td>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>{{ $user->phone }}</td>
                    </tr>
                    <tr>
                        <td>UBC</td>
                        <td>{{ $user->ubc }}</td>
                    </tr>
                    <tr>
                        <td>DOB</td>
                        <td>
                            @if(preg_match('/\d+\/\d+\/\d+/', $user->dob))
                                {{ $user->dob }} ({{ \Carbon\Carbon::createFromFormat('d/m/Y', $user->dob)->age }} years old)
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
                            {{ $user->address->street ?? '' }} <br />
                            {{ $user->address->city ?? '' }} <br />
                            {{ $user->address->state ?? '' }}, {{ $user->address->post_code ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>Member Since</td>
                        <td>
                            {{ $user->created_at->toFormattedDateString() }} ({{ $user->created_at->diffForHumans() }})
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                {!! BootForm::open(['id' => 'form']) !!}

                {!! BootForm::textarea('notes', null, $user->verification->selfie_notes, ['rows'  => 9]) !!}

                <input type="hidden" name="approve" id="approve" value="0" />

                <button type="submit" id="approveBtn" class="btn btn-success btn-block">Approve</button>
                <button type="submit" id="denyBtn" class="btn btn-danger btn-block">Deny</button>

                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Selfies</h3>
            </div>
            <div class="box-body">
                @foreach($user->getMedia('selfie') as $image)
                    @php
                        $img_url = $image->getTemporaryUrl(\Carbon\Carbon::now()->addMinutes(10));
                    @endphp
                    <div class="col-md-2">
                        <a href="{{ $img_url }}" target="_blank">
                            <img src="{{ $img_url }}" class="img-responsive img-thumbnail">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#denyBtn').click(function(e) {
            var currentForm = $('#form');
            e.preventDefault();
            bootbox.confirm("Are you sure you want to <strong>DENY</strong> this selfie?", function(result) {
                if (result) {
                    $('#approve').val(0);
                    currentForm.submit();
                    $('#box-body').html('<h2>Processing</h2>');
                }
            });
        });

        $('#approveBtn').click(function(e) {
            var currentForm = $('#form');
            e.preventDefault();
            bootbox.confirm("Are you sure you want to <strong>APPROVE</strong> this selfie?", function(result) {
                if (result) {
                    $('#approve').val(1);
                    currentForm.submit();
                    $('#box-body').html('<h2>Processing</h2>');
                }
            });
        });
    </script>
@endpush
