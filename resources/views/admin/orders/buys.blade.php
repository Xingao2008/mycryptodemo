@extends('admin.layouts.app')

@section('contentheader_title', 'Orders Buys')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Orders (Buys)</h3>
        </div>
        <div class="box-body">
            <p>BTCMarkets "Buy" ID is the ID BTCMarkets gave us at the moment we bought/sold the Crypto for the user.</p>
            <p>BTCMarkets "Transfer" ID is the ID we were given when we made a transfer from their wallet to ours or vice versa.</p>
            <table id="js-ordersbuy-table" class="table display ordersbuy-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User ID</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Bittrex Withdraw ID</th>
                        <th>Bittrex Order ID</th>
                        <th>Bittrex TXID</th>
                        <th>Bittrex Rate</th>
                        <th>Date Started</th>
                        <th>Last Change</th>
                        <th>Approve</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>User ID</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Bittrex Withdraw ID</th>
                        <th>Bittrex Order ID</th>
                        <th>Bittrex TXID</th>
                        <th>Bittrex Rate</th>
                        <th>Date Started</th>
                        <th>Last Change</th>
                        <th>Approve</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let base_url = '{{ url()->to("admin/orders/buys") }}';
        $(function() {
            $('#js-ordersbuy-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.orders.buy.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'type', name: 'type' },
                    { data: 'amount', name: 'amount' },
                    { data: 'price', name: 'total' },
                    { data: 'total', name: 'total' },
                    { data: 'status', name: 'status' },
                    { data: 'bittrex_withdraw_id', name: 'bittrex_withdraw_id' },
                    { data: 'bittrex_order_id', name: 'bittrex_order_id' },
                    { data: 'bittrex_txid', name: 'bittrex_txid'},
                    { data: 'bittrex_rate', name: 'bittrex_rate'},
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    {
                       "mRender": function ( data, type, row ) {
                        if(!row.status.includes('complete')) {
                            return '<a href="' + base_url + '/' + row.id + '/approve" class="btn btn-sm btn-success" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-check" aria-hidden="true"></i> Reattempt</a>';
                        } else {
                            return '';
                        }
                       }
                    }
                ]
            });
        });
    </script>
@endpush
