@extends('admin.layouts.app')

@section('contentheader_title', 'Edit News')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit News Post</h3>
        </div>
        <div class="box-body">
            {!! BootForm::open(["method"=>"POST", "url"=> url()->to('admin/news/save'), "enctype"=>"multipart/form-data", "model" => $post]) !!}

            {!! BootForm::text('title') !!}

            {!! BootForm::text('author') !!}

            {!! BootForm::checkbox('published') !!}

            {!! BootForm::textarea('content', null, $post->content) !!}

            @if (isset($post->featured_image))
                <div class="featured-image-uploaded">
                    <img width="300" src="/storage/{{ $post->featured_image }}">
                </div>
            @endif

            {!! BootForm::file('featured_image') !!}

            {!! BootForm::hidden('id') !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#content').wysihtml5();
    </script>
@endpush