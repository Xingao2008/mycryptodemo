@extends('admin.layouts.app')

@section('contentheader_title', 'Create News')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Create News Post</h3>
        </div>
        <div class="box-body">
            {!! BootForm::open(["method"=>"POST", "url"=> url()->to('admin/news/save'), "enctype"=>"multipart/form-data"]) !!}

            {!! BootForm::text('title') !!}

            {!! BootForm::text('author') !!}

            {!! BootForm::checkbox('published') !!}

            {!! BootForm::textarea('content') !!}

            {!! BootForm::file('featured_image') !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('#content').wysihtml5();
    </script>
@endpush