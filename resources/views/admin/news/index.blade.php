@extends('admin.layouts.app')

@section('contentheader_title', 'News')

@section('content')

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">News</h3>
        <a href="{{ url()->to('admin/news/new') }}" class="pull-right btn btn-success btn-sm">Create News</a>
    </div>
    <div class="box-body">
        <table id="js-news-table" class="table display dataTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th>Published</th>
                <th>Created Date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->author }}</td>
                    <td>{{ $post->published }}</td>
                    <td>{{ $post->created_at }}</td>
                    <td><a class="btn btn-primary" href="{{ url()->to("/admin/news/$post->id/edit") }}">Edit</a></td>
                    <td><a class="btn btn-danger" href="{{ url()->to("/admin/news/$post->id/delete") }}">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
