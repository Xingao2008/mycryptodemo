@extends('admin.layouts.app')

@section('contentheader_title', 'Dashboard')

@section('content')
    <style>
        .content-wrapper {
            height: 1500px;
        }
    </style>
    @can('admin.dashboard.stats')
        <div class="row">
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ \App\Models\User::count() }}</h3>
                        <p>Users</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            ${{ audFormat(round($balance['aud'] +
                              (latest_price('btc') * $balance['btc']) +
                              (latest_price('eth') * $balance['eth']) +
                              (latest_price('ltc') * $balance['ltc']) +
                              (latest_price('xrp') * $balance['xrp']),2)) }}
                        </h3>
                        <p>In Assets</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ \App\Models\Ledger::count() }}</h3>
                        <p>Transactions</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-arrow-swap"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ \App\Models\Firewall::count() }}</h3>
                        <p>Firewall Blocks</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-flame"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            ${{ audFormat($profits['all']) }}
                        </h3>
                        <p>All Time Profit from fees</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            ${{ audFormat($profits['30']) }}
                        </h3>
                        <p>30 day Profit from fees</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>$ {{ $usdt_balance }}</h3>
                        <p>USDT Balance</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-cash"></i>
                    </div>
                </div>
            </div>
        </div>

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="col-md-4">
                            <h5>Whole-of-site all-time Volume</h5>
                            <strong>AUD:</strong> ${{ audFormat($volume['all']['aud']) }}<br>
                            <strong>BTC:</strong> {{ formatCrypto($volume['all']['btc']) }}
                            <i>(${{ audFormat(latest_price('btc') * $volume['all']['btc']) }})</i><br>
                            <strong>ETH:</strong> {{ formatCrypto($volume['all']['eth']) }}
                            <i>(${{ audFormat(latest_price('eth') * $volume['all']['eth']) }})</i><br>
                            <strong>LTC:</strong> {{ formatCrypto($volume['all']['ltc']) }}
                            <i>(${{ audFormat(latest_price('ltc') * $volume['all']['ltc']) }})</i><br>
                            <strong>XRP:</strong> {{ formatCrypto($volume['all']['xrp']) }}
                            <i>(${{ audFormat(latest_price('xrp') * $volume['all']['xrp']) }})</i><br>
                            <hr>
                            <strong>Sum:</strong> ${{ audFormat(round($volume['all']['aud'] +
                                  (latest_price('btc') * $volume['all']['btc']) +
                                  (latest_price('eth') * $volume['all']['eth']) +
                                  (latest_price('ltc') * $volume['all']['ltc']) +
                                  (latest_price('xrp') * $volume['all']['xrp']),2)) }}
                        </div>
                        <div class="col-md-4">
                            <h5>Whole-of-site 30 day Volume</h5>
                            <strong>AUD:</strong> {{ audFormat($volume['30']['aud']) }}<br>
                            <strong>BTC:</strong> {{ formatCrypto($volume['30']['btc']) }}
                            <i>(${{ audFormat(latest_price('btc') * $volume['30']['btc']) }})</i><br>
                            <strong>ETH:</strong> {{ formatCrypto($volume['30']['eth']) }}
                            <i>(${{ audFormat(latest_price('eth') * $volume['30']['eth']) }})</i><br>
                            <strong>LTC:</strong> {{ formatCrypto($volume['30']['ltc']) }}
                            <i>(${{ audFormat(latest_price('ltc') * $volume['30']['ltc']) }})</i><br>
                            <strong>XRP:</strong> {{ formatCrypto($volume['30']['xrp']) }}
                            <i>(${{ audFormat(latest_price('xrp') * $volume['30']['xrp']) }})</i><br>
                            <hr>
                            <strong>Sum:</strong> ${{ audFormat(round($volume['30']['aud'] +
                                  (latest_price('btc') * $volume['30']['btc']) +
                                  (latest_price('eth') * $volume['30']['eth']) +
                                  (latest_price('ltc') * $volume['30']['ltc']) +
                                  (latest_price('xrp') * $volume['30']['xrp']),2)) }}
                        </div>
                        <div class="col-md-4">
                            <h5>Total user-owned Balances</h5>
                            <strong>AUD:</strong> {{ audFormat($balance['aud']) }}<br>
                            <strong>BTC:</strong> {{ formatCrypto($balance['btc']) }}
                            <i>(${{ audFormat(latest_price('btc') * $balance['btc']) }})</i><br>
                            <strong>ETH:</strong> {{ formatCrypto($balance['eth']) }}
                            <i>(${{ audFormat(latest_price('eth') * $balance['eth']) }})</i><br>
                            <strong>LTC:</strong> {{ formatCrypto($balance['ltc']) }}
                            <i>(${{ audFormat(latest_price('ltc') * $balance['ltc']) }})</i><br>
                            <strong>XRP:</strong> {{ formatCrypto($balance['xrp']) }}
                            <i>(${{ audFormat(latest_price('xrp') * $balance['xrp']) }})</i><br>
                            <hr>
                            <strong>Sum:</strong> ${{ audFormat(round($balance['aud'] +
                                  (latest_price('btc') * $balance['btc']) +
                                  (latest_price('eth') * $balance['eth']) +
                                  (latest_price('ltc') * $balance['ltc']) +
                                  (latest_price('xrp') * $balance['xrp']),2)) }}
                        </div>

                    </div>
                </div>
            </div>


        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Holding/Hot Wallet Balances</h3>
                </div>
                <div class="box-body">
                    <strong>BTC:</strong> {{ formatCrypto($holdingbalances['btc']) }}
                    ({{ config('crypto.holding.btc') }})<br>
                    <strong>ETH:</strong> {{ formatCrypto($holdingbalances['eth']) }}
                    ({{ config('crypto.holding.eth') }})<br>
                    <strong>LTC:</strong> {{ formatCrypto($holdingbalances['ltc']) }}
                    ({{ config('crypto.holding.ltc') }})<br>
                    <strong>XRP:</strong> {{ formatCrypto($holdingbalances['xrp']) }}
                    ({{ config('crypto.holding.xrp') }})<br>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">User Signups</h3>
                </div>
                <div class="box-body">
                    <div id="user-signups"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Ledger Transactions</h3>
                </div>
                <div class="box-body">
                    <div id="ledger-transactions"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Fee Profits</h3>
                </div>
                <div class="box-body">
                    <div id="profits"></div>
                </div>
            </div>
        </div>

    @endcan
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.7/raphael.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script>
        new Morris.Area({
            element: 'user-signups',
            resize: true,
            data: JSON.parse('{!! json_encode($user_signups) !!}'),
            xkey: 'x',
            ykeys: ['y'],
            labels: ['Users'],
            lineColors: ['#a0d0e0', '#3c8dbc'],
            hideHover: 'auto'
        });

        new Morris.Area({
            element: 'ledger-transactions',
            resize: true,
            data: JSON.parse('{!! json_encode($transactions) !!}'),
            xkey: 'x',
            ykeys: ['y'],
            labels: ['Transactions'],
            lineColors: ['#a0d0e0', '#3c8dbc'],
            hideHover: 'auto'
        });

        new Morris.Area({
            element: 'profits',
            resize: true,
            data: JSON.parse('{!! json_encode($profits_graph) !!}'),
            xkey: 'x',
            ykeys: ['y'],
            labels: ['Profit'],
            lineColors: ['#a0d0e0', '#3c8dbc'],
            hideHover: 'auto'
        });
    </script>
@endpush