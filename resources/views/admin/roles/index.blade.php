@extends('admin.layouts.app')

@section('contentheader_title', 'Roles')

@section('content')
    <p>
        <a href="{{ url()->to('admin/roles/create') }}" class="btn btn-primary">Create Role</a>
    </p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Roles</h3>
        </div>
        <div class="box-body">
            <table class="table display">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            @if($role->name == "user")
                            not editable
                            @else
                            <a href="{{ url()->to('admin/roles/' . $role->id) }}" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
