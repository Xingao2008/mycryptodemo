@extends('admin.layouts.app')

@section('contentheader_title', 'Edit Role')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Edit Role</h3>
    </div>
    <div class="box-body">
        {!! BootForm::open(['model' => $role]) !!}

        {!! BootForm::text('name') !!}

        @foreach(\Spatie\Permission\Models\Permission::all() as $permission)
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="{{ $permission->name }}" {{ $role->hasPermissionTo($permission->name) ? 'checked' : '' }} value="1" {{ $role->name == 'user' ? 'disabled' : '' }} class="minimal">
                    </label>
                    <label>
                        &nbsp; {{ $permission->title }}
                    </label>
                    <p><i>{{ $permission->description }}</i></p>
                </div>
            </div>
        @endforeach

        <div class="col-md-12">
            {!! BootForm::submit() !!}
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    </script>
@endpush
