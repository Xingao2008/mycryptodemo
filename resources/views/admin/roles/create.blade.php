@extends('admin.layouts.app')

@section('contentheader_title', 'Create Role')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Create Role</h3>
    </div>
    <div class="box-body">
        {!! BootForm::open(['model']) !!}

        {!! BootForm::text('name') !!}

        @foreach(\Spatie\Permission\Models\Permission::all() as $permission)
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="{{ $permission->name }}" value="1" class="minimal">
                    </label>
                    <label>
                        &nbsp; {{ ucwords(str_replace('.', ' ', $permission->name)) }}
                    </label>
                </div>
            </div>
        @endforeach

        <div class="col-md-12">
            {!! BootForm::submit() !!}
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    </script>
@endpush
