@extends('admin.layouts.app')
@section('contentheader_title', 'Ledgers')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">
            Ledgers Database
        </h3>

        <div class="pull-right">
            <a href="{{ route('admin.ledgers.csv') }}" target="_blank" class="btn btn-sm btn-primary">Export CSV</a>
        </div>
        <div class="clearfix"></div>

    </div>
    <div class="box-body">
        <table id="ledgers-table" class="table table-responsive table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Ref ID</th>
                    <th>User to</th>
                    <th>Address to</th>
                    <th>Address from</th>
                    <th>Transaction Type</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Transaction ID</th>
                    <th>Fee</th>
                    <th>Fee Type</th>
                    <th>Notes</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <th>Old Balance</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection


@push('scripts')
    <script>
        $(function() {
            $('#ledgers-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.ledgers.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'reference_id', name: 'reference_id' },
                    { data: 'user_to', name: 'user_to' },
                    { data: 'address_to', name: 'address_to' },
                    { data: 'address_from', name: 'address_from' },
                    { data: 'transaction_type', name: 'transaction_type' },
                    { data: 'currency', name: 'currency' },
                    { data: 'amount', name: 'amount' },
                    { data: 'transaction_id', name: 'transaction_id' },
                    { data: 'fee', name: 'fee' },
                    { data: 'fee_type', name: 'fee_type' },
                    { data: 'notes', name: 'notes' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'oldbalance', name: 'oldbalance' },
                ]
            });
        });
    </script>
@endpush
