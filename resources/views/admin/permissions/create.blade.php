@extends('admin.layouts.app')

@section('contentheader_title', 'Create Permission')

@section('content')
    <p>Upon creation, developers will have to implement the new permission into the code.</p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Create Permission</h3>
        </div>
        <div class="box-body">
            {!! BootForm::open([]) !!}

            {!! BootForm::text('name') !!}

            {!! BootForm::text('title') !!}

            {!! BootForm::textarea('description') !!}

            {!! BootForm::hidden('guard_name', 'web') !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection