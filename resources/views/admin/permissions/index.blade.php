@extends('admin.layouts.app')

@section('contentheader_title', 'Permissions')

@section('content')
    <p>
        <a href="{{ url()->to('admin/permissions/create') }}" class="btn btn-primary">Create Permission</a>
    </p>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Permissions</h3>
        </div>
        <div class="box-body">
            <table class="table display">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->title }}</td>
                        <td>{{ $permission->description }}</td>
                        <td>
                            <a href="{{ url()->to('admin/permissions/' . $permission->id) }}" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
