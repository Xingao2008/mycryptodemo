@extends('admin.layouts.app')

@section('contentheader_title', 'Edit Permission')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Edit Permission</h3>
    </div>
    <div class="box-body">
        <p><strong>WARNING:</strong> Changing the name will break the site. Please be careful.</p>
        {!! BootForm::open(['model' => $permission]) !!}

        {!! BootForm::text('name') !!}

        {!! BootForm::text('title') !!}

        {!! BootForm::textarea('description') !!}

        {!! BootForm::hidden('guard_name', 'web') !!}

        {!! BootForm::submit() !!}

        {!! BootForm::close() !!}
    </div>
</div>
@endsection