
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="{{ url()->to('/') }}">
    <title>@yield('contentheader_title', 'Page Header here') :: myCryptoWallet</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    {!! Assets::group('admin')->css() !!}

    <style>
        .skin-blue .main-header .navbar{
            background: #37517E;
            background-size: cover;
            background-position: 50% 50%;
            background: #37517e;
            background: -moz-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: -webkit-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#37517e', endColorstr='#00b5ff',GradientType=1 );
            position: relative;
        }

        .skin-blue .main-header .logo {
            background-color: #37517e;
            color: #fff;
            border-bottom: 0 solid transparent;
        }

    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url()->to('/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>m</b>CW</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <img src="{{ asset('img/logo.svg') }}" height="35px">
            </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get(\Auth::user()->email) }}">
                </div>
                <div class="pull-left info">
                    <p>{{ \Auth::user()->first_name }} {{ \Auth::user()->last_name }}</p>
                    <i>
                        {{ ucwords(\Auth::user()->getRoleNames()->implode(', ')) }}
                    </i>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            {!! \Menu::main() !!}

        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('contentheader_title', 'Page Header here')
                <small>@yield('contentheader_description')</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url()->to('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">TODO: Setup Breadcrumbs</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
        @if (session('changes-saved') || Request::get('changes-saved'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Success! Changes saved.</h4>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @include('flash::message')

            @yield('content')

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2016-{{ date('Y') }} <a href="{{ url()->to('/') }}">myCryptoWallet</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

{!! Assets::group('admin')->js() !!}

<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree();
    })
</script>

@stack('scripts')

</body>
</html>
