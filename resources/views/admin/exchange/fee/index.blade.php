@extends('admin.layouts.app')

@section('contentheader_title', 'Exchange fees')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Fees (surcharge)</h3>
    </div>
    <div class="box-body">
        <p>Below are the exchange transaction fees based on the volume. We only charge when it is a credit transactions.</p>
        
        <table id="js-failedwithdraw-table" class="table display failedwithdraw-table">
            <thead>
            <tr>
                <th>Min volume</th>
                <th>Max volume</th>
                <th>Fees</th>
                <th>Tier</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($fees as $fee)
                <tr>
                    <td>{{$fee->min}}</td>
                    <td>{{$fee->max}}</td>
                    <td>{{$fee->fee}}</td>
                    <td>{{$fee->exchange_tier}}</td>
                    
                    <td><a href="{{ url('admin/exchange/fees/' . $fee->id.'/edit') }}" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
