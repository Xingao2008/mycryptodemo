@extends('admin.layouts.app')

@section('contentheader_title', 'Edit fee')

@section('content')
<div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit Fee</h3>
        </div>
        <div class="box-body">
            {!! BootForm::open(["method"=>"PATCH", "url"=> url()->to('admin/exchange/fees'),  "model" => $fee]) !!}

            {!! BootForm::hidden('id') !!}
            {!! BootForm::text('min') !!}
            {!! BootForm::text('max') !!}
            {!! BootForm::text('exchange_tier') !!}
            {!! BootForm::text('fee') !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>



@endsection
