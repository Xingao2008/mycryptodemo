@extends('admin.layouts.app')

@section('contentheader_title', 'Exchange orders')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Orders</h3>
        </div>
        <div class="box-body" id="app">
            <form role="form" method="GET" action="{{route('admin.exchange.fees.convert.report')}}">
                <div class="input-group custom-search-form" style="width: 40%; display: inline-flex;">
                    <select class="form-control" name="currency">
                        <option value="">Choose currency</option>
                        @php
                            $cryptos = Config::get('currencies.allAvailableCurrencies');
                            array_unshift($cryptos, 'aud');
                        @endphp
                        @foreach(Config::get('crypto.available_currency') as $crypto)
                            @if ($request->currency === $crypto)
                                <option value="{{ $crypto }}" selected>{{ $crypto }}</option>
                            @else
                                <option value="{{ $crypto }}">{{ $crypto }}</option>
                            @endif
                        @endforeach
                    </select>
                    <input type="text" class="form-control datepicker" name="search_datetime" placeholder="Enter Date" v-model="date"/>
                    <span class="input-group-btn" margin-left: 5px;>
                        <button class="btn btn-default-sm" type="submit"> <i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

            <table class="table display failedwithdraw-table">
                <thead>
                <tr>
                    <th>Currency</th>
                    <th>Amount</th>
                    {{-- <th>Original balance</th> --}}
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($records as $record)
                    <tr>
                        <td>{{$record->currency}}</td>
                        <td>{{$record->amount}}</td>
                        <td>{{$record->created_at}}</td>
                        
                    </tr>
                @endforeach
                </tbody>
            </table>
            <hr />
            <table class="table">
                
                <tbody>
                    <tr>
                        <td>Current Surcharge Balance:</td>
                        @foreach ($balance as $b)
                            <td>{{ $b->currency }}: {{$b->amount}}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <hr />
            {{-- <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Currency to sell</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {!! BootForm::open(["method"=>"POST", "url"=> url()->to('admin/exchange/fees/convert/cashout')]) !!}
                    <tr>
                        <td>
                            <select class="form-control" name="currency">
                                <option value="">Choose currency</option>
                                @foreach (Config::get('currencies.allAvailableCurrencies') as $c)
                                        <option value="{{ $c }}">{{ $c }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="amount" placeholder="Enter amount"/>
                        </td>
                        <td>
                            <input type="submit" name="submit" value="Submit" class="btn btn-success">
                        </td>
                    </tr>
                    </form>
                </tbody>
            </table> --}}
            
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                date: '',
                isActive: false
            },
            mounted: function() {
                var args = {
                    format: 'DD/MM/YYYY'
                };
                this.$nextTick(function() {
                    $('.datepicker').datetimepicker(args)
                });
            },
            methods: {
                
            }
        })
    </script>
@endpush
