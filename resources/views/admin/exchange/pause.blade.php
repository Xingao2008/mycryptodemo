@extends('admin.layouts.app')

@section('contentheader_title', 'Exchange Pause Control')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Exchange Pause Control</h3>
    </div>
    <div class="box-body">
        <p>Current exchange status: 
            @if($isPause)
                <span style="color:red">Inactive</span>
            @else
                <span style="color:green">Active</span>
            @endif
        </p>
        {!! BootForm::open(["method"=>"POST", "url"=> url()->to('admin/exchange/pause')]) !!}
            @if($isPause)
                <input type="hidden" name="pauseStatus" value="0">
                <input type="submit" class="btn btn-success" value="Enable Exchange">
            @else
                <input type="hidden" name="pauseStatus" value="1">
                <input type="submit" class="btn btn-danger" value="Freeze Exchange">
            @endif
            
            

            {!! BootForm::close() !!}
    </div>
</div>
@endsection
