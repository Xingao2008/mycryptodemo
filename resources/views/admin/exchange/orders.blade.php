@extends('admin.layouts.app')

@section('contentheader_title', 'Exchange orders')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Orders</h3>
        </div>
        <div class="box-body" id="app">
            <form role="form" method="GET" action="{{route('admin.exchange.orders.search')}}">
                <div class="input-group custom-search-form" style="width: 40%; display: inline-flex;">
                    <input type="text" class="form-control" name="user_name" placeholder="User Name" value="{{ $req->user_name }}"/>
                    <select class="form-control" name="currency">
                        <option value="">Choose currency</option>
                        @php
                            $cryptos = Config::get('crypto.available_currency');
                            array_unshift($cryptos, 'aud');
                        @endphp
                        @foreach($cryptos as $crypto)
                            @if ($req->currency === $crypto)
                                <option value="{{ $crypto }}" selected>{{ $crypto }}</option>
                            @else
                                <option value="{{ $crypto }}">{{ $crypto }}</option>
                            @endif
                        @endforeach
                    </select>
                    <input type="text" class="form-control datepicker" name="search_datetime" v-model="date"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default-sm" type="submit"> <i class="fa fa-search"></i></button>
                    </span>
                </div>

                <div style="float: right;"><a href="#" class="btn btn-primary btn-sm" @click.prevent="showTrans()">Show transactions</a></div>
            </form>

            <table class="table display failedwithdraw-table">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Order type</th>
                    <th>Currency</th>
                    <th>Currency Pair</th>
                    <th>Status</th>
                    <th>Volume</th>
                    <th>Buy/Sell</th>
                    <th>Price</th>
                    <th>Remaining</th>
                    <th>Date Time</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($orders as $order)
                    <tr>
                        {{--'+ 0' used for removing useless 0--}}
                        <td>{{$order->name}}</td>
                        <td>{{$order->order_type}}</td>
                        <td>{{$order->currency}}</td>
                        <td>{{$order->currency_pair}}</td>
                        <td>{{$order->status}}</td>
                        <td>{{$order->volume + 0}}</td>
                        <td>{{$order->market_type}}</td>
                        <td>{{$order->price + 0}}</td>
                        <td>{{$order->remaining + 0}}</td>
                        <td>{{$order->datetime}}</td>
                    </tr>
                    <tr v-show="isActive">
                        <td colspan="9">
                            @foreach ($order->transactions as $tra)
                            <table class="table display failedwithdraw-table">
                                <thead>
                                    <tr>
                                        <th>Transaction Status</th>
                                        <th>Transaction Volume</th>
                                        <th>Transaction Datetime</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $tra->t_status }}</td>
                                        <td>{{ $tra->t_volume + 0 }}</td>
                                        <td>{{ $tra->t_datetime }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            @endforeach
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $orders->appends(request()->input())->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
    <script type="application/javascript">
        var app = new Vue({
            el: '#app',
            data: {
                date: '',
                isActive: false
            },
            mounted: function() {
                var args = {
                    format: 'DD/MM/YYYY'
                };
                this.$nextTick(function() {
                    $('.datepicker').datetimepicker(args)
                });
            },
            methods: {
                showTrans() {
                    this.isActive = !this.isActive
                }
            }
        })
    </script>
@endpush
