@extends('admin.layouts.app')

@section('contentheader_title', 'Verifications')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Verifications</h3>
    </div>
    <div class="box-body">
        <p>Changes here will send the user a generic email telling them that there's an update to a pending verification, and telling them to log in to review what's changed.</p>
        <table id="users-table" class="table display">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Role</th>
                <th>Email</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        let base_url = '{{ url()->to("admin/") }}';
        $(function () {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.users.datatables') !!}',
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'role', name: 'role', searchable: false, sortable: false},
                    {data: 'email', name: 'email'},
                    {data: 'status', name: 'status'},
                    {
                        "mRender": function (data, type, row) {
                            html = '<a href="' + base_url + '/verifications/' + row.id + '/edit" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
                            html += '<a href="' + base_url + '/verifications/' + row.id + '/approve" class="btn btn-success">Approve</a>&nbsp;';
                            html += '<a href="' + base_url + '/verifications/' + row.id + '/reject" class="btn btn-danger">Reject</a>&nbsp;';
                            html += '&nbsp; <a href="' + base_url + '/verifications/' + row.id + '/manual" class="btn btn-sm btn-danger reset-tfa">Manual</a>';
                            return html;
                        }
                    }
                ]
            });
        });
    </script>
@endpush