@extends('admin.layouts.app')

@section('contentheader_title', 'Verifications')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Pending Resolution</h3>
    </div>
    <div class="box-body">
        {!!
            BootForm::horizontal([
                'model' => $user,
                'left_column_class' => 'col-md-2',
                'left_column_offset_class' => '',
                'right_column_class' => 'col-md-5',
                'method' => 'POST',
                'id' => 'verify_form',
                'url' => url()->route('admin.verifications.manual', $user->id)
            ])
        !!}
            {!! BootForm::select('profile', 'GBG Profile', ['US'=>'US','UK'=>'UK', 'CA'=>'CA']) !!}
            {{ csrf_field() }}
            {!! BootForm::text('first_name', null, null) !!}

            {!! BootForm::text('middle_name', null, null) !!}

            {!! BootForm::text('last_name', null, null) !!}

            {!! BootForm::email('email', null, null) !!}

            {!! BootForm::text('phone', null, null) !!}

            {!! BootForm::text('dob', null, $user->dob) !!}

            {!! BootForm::text('country', null, $user->address->country->name) !!}
            {!! BootForm::text('street', null, $user->address->street) !!}
            {!! BootForm::text('city', null, $user->address->city) !!}
            {!! BootForm::text('ZipPostcode', null, $user->address->post_code) !!}
            {!! BootForm::text('StateDistrict', null, $user->address->state) !!}
            <hr />
            
            <div class="form-group "><label for="id" class="control-label col-md-5"><b>Identiy Card: UK (driver license), US (SocialSecurity nunber), Canada (SocialInsuranceNumber)</b></label></div>
            {!! BootForm::text('idCard', 'ID card', null, ['placeholder'=>'This can be Social security number or social insurance number']) !!}
            <div id="us_div">
                 {!! BootForm::text('us_drivier_num', 'US driver license nunber', null) !!}
                 {!! BootForm::text('us_drivier_state', 'US driver license state', null) !!}
            </div>
            <hr />
            <div id="uk_div" style="display:none">

                 <div class="form-group "><label for="id" class="control-label col-md-5"><b>EuropeanIdentityCard (UK only)</b></label></div>
                 {!! BootForm::text('eid_line1', 'Line1', null, ['placeholder'=>'Example: IDGBR1234567897<<<<<<<<<<<<<<<']) !!}
                 {!! BootForm::text('eid_line2', 'Line2', null, ['placeholder'=>'Example: 4609011F2201110GBR<<<<<<<<<<<6']) !!}
                 {!! BootForm::text('eid_line3', 'Line3', null, ['placeholder'=>'Example: Shenol<<hoines<<<<<<<<<<<<']) !!}
                 {!! BootForm::text('eid_day', 'ExpiryDay', null, ['placeholder'=>'With zero in front if single digit: 03']) !!}
                {!! BootForm::text('eid_month', 'ExpiryMonth', null, ['placeholder'=>'With zero in front if single digit: 09']) !!}
                {!! BootForm::text('eid_year', 'ExpiryYear', null, ['placeholder'=>'4 digit full year like: 2019']) !!}
                {!! BootForm::text('eid_CountryOfNationality', 'CountryOfNationality', null, ['placeholder'=>'Example: GBR']) !!}
                {!! BootForm::text('eid_CountryOfIssue', 'CountryOfIssue', null, ['placeholder'=>'Example: GBR']) !!}
                <hr />
            </div>

            

            <div class="form-group "><label for="id" class="control-label col-md-2"><b>Passport (optional)</b></label></div>
            {!! BootForm::text('passport_Number', 'Passport Number', null, ['placeholder'=>'Long passport barcode number: for exmplae: 9876543213GBR7310065M080310806']) !!}
            {!! BootForm::text('passport_ExpiryDay', 'ExpiryDay', null, ['placeholder'=>'With zero in front if single digit: 03']) !!}
            {!! BootForm::text('passport_ExpiryMonth', 'ExpiryMonth', null, ['placeholder'=>'With zero in front if single digit: 09']) !!}
            {!! BootForm::text('passport_ExpiryYear', 'ExpiryYear', null, ['placeholder'=>'4 digit full year like: 2019']) !!}
            {!! BootForm::text('passport_CountryOfOrigin', 'CountryOfOrigin', null, ['placeholder'=>'Example: United Kingdom']) !!}
            {!! BootForm::text('passport_IssueDay', 'IssueDay', null) !!}
            {!! BootForm::text('passport_IssueMonth', 'IssueMonth', null) !!}
            {!! BootForm::text('passport_IssueYear', 'IssueYear', null) !!}
            {!! BootForm::text('passport_ShortPassportNumber', 'ShortPassportNumber', null) !!}
            <div class="form-group ">
                <div class="col-md-4">
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success" id="sendBtn">Send</button>
                    <a href="{{ url()->to('/admin/verifications/' . $user->id.'/approve?verify=gbg') }}"  class="btn btn-warning" id="approveBtn" style="display:none">Approve Now!</a>
                    </a>
                    <a href="{{ url()->to('/admin/verifications/' . $user->id.'/reject?verify=gbg') }}" class="btn btn-danger" id="rejectBtn" style="display:none">Reject</a>
                    </a>
                    <a class="btn btn-danger" href="{{ route('admin.verifications') }}">Cancel</a>
                </div>
            </div>
        
        </form>
        
        
            <div class="form-group" id="resultDiv">
                <div class="col-md-2">
                </div>
                <div class="col-md-5">
                    <label for="result">Feedback:</label>
                    <div  disabled>
                       <pre id="result"></pre>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        var full_url = '{{ url()->route('admin.verifications.manual', $user->id) }}';
        $('#profile').on('change', function(){
            
            if ($(this).val() == 'US') {
                $('#us_div').show();
                $('#uk_div').hide();
            } else if ($(this).val() == 'UK') {
                $('#us_div').hide();
                $('#uk_div').show();
            } else if ($(this).val() == 'CA') {
                $('#us_div').hide();
                $('#uk_div').hide();
            }

        })
        $('#verify_form').submit(function(event){
          event.preventDefault();
          $('#sendBtn').hide();
          $(".resultDiv").show();
          $('#approveBtn').hide();
          $("#result").html('Sending request......');
          $.ajax({
                  type: 'POST',
                  url: full_url,
                  data: $('#verify_form').serialize(),
                  success: function(data){
                    $("#result").html('');
                    $("#result").html(JSON.stringify(data.data, null, 2));
                    if (data.success){
                        $('#approveBtn').show();
                        $('#rejectBtn').show();
                    } else {
                        $('#sendBtn').show();
                        $('#approveBtn').hide();
                        $('#rejectBtn').show();
                    }
                  },
                  error: function(xhr, status, error) {
                    $('#sendBtn').show();
                    $('#approveBtn').hide();
                    $('#rejectBtn').show();
                    $("#result").html('Error sending data to GBG or some coding went wrong. Error has been saved to database. Contact dev team to investigate further');
                  }
              });
      });
       
    </script>
@endpush