@extends('admin.layouts.app')

@section('contentheader_title', 'Verifications')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Pending Resolution</h3>
    </div>
    <div class="box-body">
        <form id="pending-resolution-form" method="post" class="form-horizontal">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="account" class="col-sm-2 control-label">Account</label>
                    <div class="col-sm-10">
                        <select id="account" class="form-control" name="account">
                            @foreach(['incomplete', 'approved', 'pending'] as $account)
                                <option value="{{ $account }}" {{ $account == $verification->account ? 'selected' : '' }}>{{ strtoupper($account) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_passport" class="col-sm-2 control-label">Id Passport Status</label>
                    <div class="col-sm-10">
                        <select id="id_passport" class="form-control" name="id_passport">
                            @foreach(['incomplete', 'approved', 'pending'] as $id_passport)
                                <option value="{{ $id_passport }}" {{ $id_passport == $verification->id_passport ? 'selected' : '' }}>{{ strtoupper($id_passport) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_drivers" class="col-sm-2 control-label">Id Driver Status</label>
                    <div class="col-sm-10">
                        <select id="id_drivers" class="form-control" name="id_drivers">
                            @foreach(['incomplete', 'approved', 'pending'] as $id_drivers)
                                <option value="{{ $id_drivers }}" {{ $id_drivers == $verification->id_drivers ? 'selected' : '' }}>{{ strtoupper($id_drivers) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="id_bank" class="col-sm-2 control-label">Id Bank Status</label>
                    <div class="col-sm-10">
                        <select id="id_bank" class="form-control" name="id_bank">
                            @foreach(['incomplete', 'approved', 'pending'] as $id_bank)
                                <option value="{{ $id_bank }}" {{ $id_bank == $verification->id_bank ? 'selected' : '' }}>{{ strtoupper($id_bank) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ubc" class="col-sm-2 control-label">UBC Status</label>
                    <div class="col-sm-10">
                        <select id="ubc" class="form-control" name="ubc">
                            @foreach(['incomplete', 'approved', 'pending'] as $ubc)
                                <option value="{{ $ubc }}" {{ $ubc == $verification->ubc ? 'selected' : '' }}>{{ strtoupper($ubc) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sms" class="col-sm-2 control-label">SMS Status</label>
                    <div class="col-sm-10">
                        <select id="sms" class="form-control" name="sms">
                            @foreach(['incomplete', 'approved', 'pending'] as $sms)
                                <option value="{{ $sms }}" {{ $sms == $verification->sms ? 'selected' : '' }}>{{ strtoupper($sms) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tfa" class="col-sm-2 control-label">TFA Status</label>
                    <div class="col-sm-10">
                        <select id="tfa" class="form-control" name="tfa">
                            @foreach(['incomplete', 'approved', 'pending'] as $tfa)
                                <option value="{{ $tfa }}" {{ $tfa == $verification->tfa ? 'selected' : '' }}>{{ strtoupper($tfa) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="balance1" class="col-sm-2 control-label">Balance1 Status</label>
                    <div class="col-sm-10">
                        <select id="balance1" class="form-control" name="balance1">
                            @foreach(['incomplete', 'approved', 'pending'] as $balance1)
                                <option value="{{ $balance1 }}" {{ $balance1 == $verification->balance1 ? 'selected' : '' }}>{{ strtoupper($balance1) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="balance2" class="col-sm-2 control-label">Balance2 Status</label>
                    <div class="col-sm-10">
                        <select id="balance2" class="form-control" name="balance2">
                            @foreach(['incomplete', 'approved', 'pending'] as $balance2)
                                <option value="{{ $balance2 }}" {{ $balance2 == $verification->balance2 ? 'selected' : '' }}>{{ strtoupper($balance2) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="abn" class="col-sm-2 control-label">ABN Status</label>
                    <div class="col-sm-10">
                        <select id="abn" class="form-control" name="abn">
                            @foreach(['incomplete', 'approved', 'pending'] as $abn)
                                <option value="{{ $abn }}" {{ $abn == $verification->abn ? 'selected' : '' }}>{{ strtoupper($abn) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="abn" class="col-sm-2 control-label">Selfie Status</label>
                    <div class="col-sm-10">
                        <select id="selfie" class="form-control" name="selfie">
                            @foreach(['incomplete', 'approved', 'pending', 'legacy'] as $selfie)
                                <option value="{{ $selfie }}" {{ $selfie == $verification->selfie ? 'selected' : '' }}>{{ strtoupper($selfie) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <input type="hidden" name="user_id" value="{{ $verification->user_id }}" />
                <button type="submit" class="btn btn-success">Save</button>
                <a class="btn btn-danger" href="{{ route('admin.verifications') }}">Cancel</a>
        </form>
    </div>
</div>
@endsection