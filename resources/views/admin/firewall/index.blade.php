@extends('admin.layouts.app')

@section('contentheader_title', 'Firewall')

@section('content')
    <p>
        <a href="{{ url()->to('admin/firewall/new') }}" class="btn btn-primary">Create Entry</a>
    </p>
    <div class="box box-danger box-solid box-border">
        <div class="box-header">
            <h2 class="box-title">Blacklist</h2>
        </div>
        <div class="box-body">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th width="10%">ID</th>
                    <th width="20%">IP</th>
                    <th>Reason</th>
                    <th>Expiry</th>
                    <th>Added</th>
                    <th width="10%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($blacklist as $entry)
                    <tr>
                        <td>{{ $entry->id }}</td>
                        <td>{{ $entry->ip_address }} [<a href="{{ url()->to('https://geoiptool.com/en/?ip=' . $entry->ip_address) }}" target="_blank">lookup</a>]</td>
                        <td>{{ $entry->reason }}</td>
                        <td>
                            @if($entry->expiry)
                                {{ $entry->expires->diffForHumans() }} ({{ $entry->expires }})
                            @else
                                Permanent
                            @endif
                        </td>
                        <td>{{ $entry->created_at }}</td>
                        <td>
                            <!--<a href="#" class="btn btn-table btn-teal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
                            <a href="{{ url()->to('admin/firewall/' . $entry->id . '/delete') }}" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="box box-success box-solid box-border">
        <div class="box-header">
            <h2 class="box-title">Whitelist</h2>
        </div>
        <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th width="10%">ID</th>
                    <th width="20%">IP</th>
                    <th>Reason</th>
                    <th>Expiry</th>
                    <th>Added</th>
                    <th width="10%"></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($whitelist as $entry)
                    <tr>
                        <td>{{ $entry->id }}</td>
                        <td>{{ $entry->ip_address }} [<a href="{{ url()->to('https://geoiptool.com/en/?ip=' . $entry->ip_address) }}" target="_blank">lookup</a>]</td>
                        <td>{{ $entry->reason }}</td>
                        <td>
                            @if($entry->expiry)
                                {{ $entry->expires->diffForHumans() }} ({{ $entry->expires }})
                            @else
                                Permanent
                            @endif
                        </td>
                        <td>{{ $entry->created_at }}</td>
                        <td>
                            <!--<a href="#" class="btn btn-table btn-teal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
                            <a href="{{ url()->to('admin/firewall/' . $entry->id . '/delete') }}" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('table').DataTable();
    </script>
@endpush