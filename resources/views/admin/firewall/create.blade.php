@extends('admin.layouts.app')

@section('contentheader_title', 'Firewall')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Create Firewall</h3>
        </div>
        <div class="box-body">
            {!! BootForm::horizontal() !!}

            {!! BootForm::text('ip_address') !!}

            {!! BootForm::text('reason') !!}

            {!! BootForm::date('expires') !!}

            {!! BootForm::radios('whitelist', 'List', ['whitelist' => '&nbsp;Whitelist', 'blacklist' => '&nbsp;Blacklist']) !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('input[type="radio"]').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        })
    </script>
@endpush