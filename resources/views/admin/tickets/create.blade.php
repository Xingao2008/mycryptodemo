@extends('admin.layouts.app')

@section('contentheader_title', 'Create Ticket')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Create Ticket</h3>
        </div>
        <div class="box-body">
            {!! BootForm::open(['route' => 'admin.ticket.postCreate']) !!}

            @if($user)
                {!! BootForm::select('user_id', 'User', [[$user->id => $user->name]], $user->id) !!}
            @else
                {!! BootForm::select('user_id', 'User') !!}
            @endif

            {!! BootForm::text('subject') !!}

            <div class="form-group ">
                <label for="body" class="control-label">Body</label>
                <div>
                    <textarea class="form-control" id="body" name="body" cols="50" rows="10">
                        Hello <span id="user-name">{{ $user->first_name ?? '[name]' }}</span>, <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        Regards,<br/>
                        {{ \Auth::user()->first_name }}<br/>
                        <br/>
                        <p style="color:blue;">⌘ myCryptoWallet</p>
                    </textarea>
                </div>
            </div>

            {!! BootForm::select('priority', null, ['1' => 'Low', '2' => 'Medium', '3' => 'High', '4' => 'Urgent']) !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $('#user_id').select2({
            ajax: {
                url: '{{ url()->route('admin.users.select2') }}',
                dataType: 'json'
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        })
            .on("select2:select", function(e) {
                let name = e.params.data.text.split(' ')[0];
                $('#user-name').text(name);
        });

        $('#body').summernote();
    </script>
@endpush