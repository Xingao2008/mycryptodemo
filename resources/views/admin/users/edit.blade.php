@extends('admin.layouts.app')

@section('contentheader_title', 'Edit ' . $user->name)

@section('content')
<div id="main">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab">View</a></li>
            <li><a href="#tab2" data-toggle="tab">Edit</a></li>
            <li><a href="#tab9" data-toggle="tab">Notes</a></li>
            <li><a href="#tab6" data-toggle="tab">Address</a></li>
            <li><a href="#tab7" data-toggle="tab">Events</a></li>
            <li><a href="#tab3" data-toggle="tab">Buys/Sells</a></li>
            <li><a href="#tab4" data-toggle="tab">Deposits/Withdawals</a></li>
            <li><a href="#tab5" data-toggle="tab">Wallets/Balances/Banks</a></li>
            <li><a href="#tab8" data-toggle="tab">POLi Pay</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                @include('admin.users.panels.view')
            </div>
            <div class="tab-pane" id="tab2">
                @include('admin.users.panels.edit')
            </div>
            <div class="tab-pane" id="tab9">
                @include('admin.users.panels.notes')
            </div>
            <div class="tab-pane" id="tab7">
                @include('admin.users.panels.events')
            </div>
            <div class="tab-pane" id="tab3">
                @include('admin.users.panels.buys-sells')
            </div>
            <div class="tab-pane" id="tab4">
                @include('admin.users.panels.deposits-withdrawals')
            </div>
            <div class="tab-pane" id="tab5">
                @include('admin.users.panels.wallets-balances-banks')
            </div>
            <div class="tab-pane" id="tab6">
                @include('admin.users.panels.address')
            </div>
            <div class="tab-pane" id="tab8">
                @include('admin.users.panels.polipay')
            </div>
        </div>
    </div>


    @canImpersonate
        <a href="{{ route('admin.impersonate.take', $user->id) }}" class="btn btn-warning">Impersonate</a>
    @endCanImpersonate
    @can('admin.userlimits.index')
        <a href="{{ route('admin.userlimits.edit', $user->id) }}" class="btn btn-info">User Limits</a>
    @endcan
    @can('admin.selfie.index')
        <a href="{{ route('admin.selfies.edit', $user->id) }}" class="btn btn-primary">User Selfies</a>
    @endcan
</div>



@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        let app = new Vue({
            el: '#main',

            data: {
                fiatEntries: {},
                date: ''
            },

            methods: {
                loadFiatRows: function () {
                    let vm = this;
                    axios.get('{{ url()->to('/admin/users/' . $user->id . '/fiat') }}')
                        .then(function (rsp) {
                            vm.fiatEntries = rsp.data.map(function (e) {
                                e.edit = false;
                                e.notes = (e.notes === '' || e.notes === null) ? 'N/A' : e.notes;
                                return e;
                            })
                        })
                        .catch(function (err) {

                        });
                },
                toggleEditFiat: function(ev, entry) {
                    entry.edit = true;

                    if(entry.notes === 'N/A' || entry.notes === '' || entry.notes === null)
                    {
                        entry.notes = '';
                    }

                    this.$nextTick(() => {
                        this.$refs['input-' + entry.id][0].focus()
                    });

                },
                saveFiatEdit: function(ev, entry) {
                    entry.edit = false;

                    if(entry.notes === null || entry.notes === '')
                    {
                        entry.notes = 'N/A';
                    }

                    axios.post('{{ url()->to('/admin/users/fiat/notes') }}', {notes: entry.notes, type: entry.type, id: entry.id})
                },
                reverseFiat: function(entry) {
                    let vm = this;
                    axios.post('{{ url()->to('/admin/users/fiat/reverse') }}', {id: entry.id, type: entry.type})
                        .then(function(rsp){
                            vm.loadFiatRows();
                        });
                }
            },
            mounted: function () {
                this.loadFiatRows();

                var args = {
                    format: 'DD/MM/YYYY'
                };
                this.$nextTick(function() {
                    $('.datepicker').datetimepicker(args)
                });
            }
        });

        $('.select2').select2();
        $('.datatable').DataTable();

        var clipboard = new Clipboard('.clipboard', {
            text: function(trigger) {
                return $(trigger).parent().parent().find('input').val();
            }
        });

        clipboard.on('success', function(e) {
            setTooltip(e.trigger, 'Copied!');
            hideTooltip(e.trigger);
        });

        $('button').tooltip({
            trigger: 'click',
            placement: 'bottom'
        });

        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
                .attr('data-original-title', message)
                .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                $(btn).tooltip('hide');
            }, 1000);
        }

        $('textarea').summernote();
    </script>
@endpush