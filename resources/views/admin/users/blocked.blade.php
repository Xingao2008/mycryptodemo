@extends('admin.layouts.app')

@section('contentheader_title', 'Unblock users')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Unblock users</h3>
        </div>
        @include('flash::message')
        <div class="box-body" id="app">
            <form role="form" method="POST" action="{{route('admin.blocked.search')}}">
                {{ csrf_field() }}
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" name="user_name" placeholder="User Name"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default-sm" type="submit"> <i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>

            <table class="table display failedwithdraw-table">
                <thead>
                <tr>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($blocked_users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->ubc}}</td>
                        <td><a href="admin/users_unblock/{{$user->id}}" class="btn btn-sm btn-primary">Unblock</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $blocked_users->appends(request()->input())->links() }}
        </div>
    </div>
@endsection