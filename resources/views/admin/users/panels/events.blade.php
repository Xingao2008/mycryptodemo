<table class="table datatable">
    <thead>
    <tr>
        <th>IP</th>
        <th>Event</th>
        <th>Time</th>
    </tr>
    </thead>
    <tbody>
    @if(!$ips->isEmpty())
        @foreach($ips as $ip)
            <tr>
                <td>{{ $ip->ip }}</td>
                <td>{{ $ip->event }}</td>
                <td>{{ $ip->created_at }}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan="3">None available.</td></tr>
    @endif
    </tbody>

</table>