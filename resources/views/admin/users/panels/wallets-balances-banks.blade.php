<div class="row">
    <div class="col-md-7 col-md-offset-1">
        {{--<h3>Bank</h3>--}}
        {{--<b>Bank Name</b>: {{ $bank->bank_name or ''}}<br>--}}
        {{--<b>Branch</b>: {{ $bank->bank_branch or ''}}<br>--}}
        {{--<b>BSB</b>: {{ $bank->bank_bsb or ''}}<br>--}}
        {{--<b>Account Number</b>: {{ $bank->bank_number or ''}}--}}
        <h3>Modify Bank</h3>
        <form method="post" class="form-horizontal">
            {{ csrf_field() }}
            <input type="hidden" name="userfieldtype" value="bank">
                <div class="form-group">
                    <label for="id" class="col-sm-2 control-label">Bank Name</label>
                    <div class="col-sm-10">
                        <input name="bank_name" type="text" class="form-control" value="{{ $bank->bank_name or ''}}">
                    </div>
                    <label for="id" class="col-sm-2 control-label">Branch</label>
                    <div class="col-sm-10">
                        <input name="bank_branch" type="text" class="form-control" value="{{ $bank->bank_branch or ''}}">
                    </div>
                    <label for="id" class="col-sm-2 control-label">BSB</label>
                    <div class="col-sm-10">
                        <input name="bank_bsb" type="text" class="form-control" value="{{ $bank->bank_bsb or ''}}">
                    </div>
                    <label for="id" class="col-sm-2 control-label">Account Number</label>
                    <div class="col-sm-10">
                        <input name="bank_number" type="text" class="form-control" value="{{ $bank->bank_number or ''}}">
                    </div>

                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success">Save</button>
                        <a class="btn btn-danger" href="{{ route('admin.users') }}">Cancel</a>
                    </div>
            </div>
        </form>

        <h3>History</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Bank Name</th>
                    <th>Branch</th>
                    <th>BSB</th>
                    <th>Account Number</th>
                    <th>Version</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($banks_arr as $banks)
                <tr>
                    <td>{{ $banks['data']->bank_name }}</td>
                    <td>{{ $banks['data']->bank_branch }}</td>
                    <td>{{ $banks['data']->bank_bsb }}</td>
                    <td>{{ $banks['data']->bank_number }}</td>
                    <td>{{ $banks['version'] }}</td>
                    <td>{{ $banks['status'] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col-md-4">
        <h3>Wallets</h3>
        @if(!$wallets->isEmpty())
            @foreach ($wallets as $wallet)
                {{strtoupper($wallet->type)}} : {{$wallet->address}} <br>
            @endforeach
        @else
            The user hasn't generated any wallets.
        @endif
        <h3>Balances</h3>
        @foreach ($balances as $balance)
            {{strtoupper($balance->currency)}} : {{$balance->balance}} <br>
        @endforeach
    </div>

    <div class="col-md-12">
        <div style="margin-top: 20px;">
            <h4>FIAT Deposits/Withdrawals</h4>
            <table class="table">
                <tr>
                    <th>Type</th>
                    <th>Amount</th>
                    <th>Via</th>
                    <th>Status</th>
                    <th>When</th>
                    <th>Notes</th>
                    <th></th>
                </tr>
                <tr v-if="!fiatEntries.length">
                    <td colspan="7">No transactions found!</td>
                </tr>
                <tr v-for="entry in fiatEntries">
                    <td>@{{ entry.type.toUpperCase() }}</td>
                    <td>$@{{ entry.amount }}</td>
                    <td>@{{ entry.source }}</td>
                    <td>@{{ entry.status }}</td>
                    <td>@{{ entry.created_at }}</td>
                    <td>
                        <span v-show="!entry.edit" @click="toggleEditFiat(this, entry)">@{{ entry.notes }}</span>

                        <textarea type="text"
                                  v-model="entry.notes"
                                  v-show="entry.edit"
                                  @blur="saveFiatEdit(this, entry)"
                                  :ref="'input-' + entry.id"
                        ></textarea>
                        <br>
                    </td>
                    <td>
                        <a href="#" class="btn btn-table btn-red" @click.prevent="reverseFiat(entry)"><i class="fa fa-undo" aria-hidden="true"></i></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>