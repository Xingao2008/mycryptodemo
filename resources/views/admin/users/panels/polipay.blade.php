<table class="table datatable">
    <thead>
    <tr>
        <th>User IP</th>
        <th>Amount</th>
        <th>Status</th>
        <th>Transaction ID</th>
        <th>Transaction Ref No.</th>
        <th>When</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
        @if(!$polipay->isEmpty())
            @foreach($polipay as $transaction)
                <tr>
                    <td>{{ $transaction->user_ip }}</td>
                    <td>${{ audFormat($transaction->amount) }}</td>
                    <td>{{ $transaction->status }}</td>
                    <td>{{ $transaction->transaction_id }}</td>
                    <td>{{ $transaction->transaction_ref_no }}</td>
                    <td>{{ $transaction->created_at }}</td>
                    <td>
                        <a href="{{ url()->route('admin.polipay.edit', $transaction->id) }}" class="btn btn-sm btn-primary">Edit</a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr><td colspan="3">None available.</td></tr>
        @endif
    </tbody>
</table>