
{!!
    BootForm::horizontal([
        'model' => $user,
        'left_column_class' => 'col-md-2',
        'left_column_offset_class' => '',
        'right_column_class' => 'col-md-10',
        'method' => 'POST',
        'url' => url()->route('admin.users.postedit', $user->id)
    ])
!!}
    {{ csrf_field() }}
    <input type="hidden" name="userfieldtype" value="user">

    {!! BootForm::staticField('id', null, $user->id) !!}

    {!! BootForm::text('first_name', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::text('middle_name', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::text('last_name', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::email('email', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::select('country_calling_code', 'Country code', [null=>'Please Select'] + $countrie_calling_codes) !!}

    {!! BootForm::text('phone', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::text('dob', null, $user->dob, ['pattern' => '[0-9]{2}/[0-9]{2}/[0-9]{4}', 'title' => 'Please use the format dd/mm/yyyy', 'class' => 'datepicker' ]) !!}

    {!! BootForm::select('tfa_status', null, ['Disabled', 'Enabled']) !!}

    {!! BootForm::text('ubc', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::text('verify_count') !!}

    {!! BootForm::text('abn', null, null, ['prefix' => BootForm::addonButton('<i class="fa fa-clipboard"></i>', ['class' => 'clipboard']) ]) !!}

    {!! BootForm::select('status', 'Status Code', ['inactive' => 'Inactive', 'active' => 'Active', 'blocked' => 'Blocked']) !!}

    @if(auth()->user()->hasRole('admin'))
    <div class="form-group">
        <label for="roles[]" class="col-sm-2 control-label">Role(s)</label>
        <div class="col-sm-10">
            <select class="form-control select2" multiple="multiple" data-placeholder="Select Role(s)" name="roles[]" style="width: 100%;">
                @foreach($roles as $role)
                    <option value="{{ $role->id }}" {{ $user->hasRole($role->name) ? 'selected' : '' }}>{{ $role->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    @endif

    <button type="submit" class="btn btn-success">Save</button>
    <a class="btn btn-danger" href="{{ route('admin.users') }}">Cancel</a>

{!! BootForm::close() !!}
