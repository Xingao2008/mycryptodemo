<div class="row">
    <div class="col-md-6">
        <h3>Buys</h3>
        @if(!$buys->isEmpty())
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th style="width: 35px;">Type</th>
                    <th>Amount</th>
                    <th>Price (time of sale)</th>
                    <th>Value (after fees)</th>
                    <th style="width: 70px;">Status</th>
                </tr>

                @foreach ($buys as $buy)
                    <tr>
                        <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $buy->created_at, 'Australia/Melbourne')}}</td>
                        <td>{{strtoupper($buy->type)}}</td>
                        <td>{{$buy->amount}}</td>
                        <td>${{$buy->price}}</td>
                        <td>${{bcmul($buy->price,$buy->amount,2)}}
                        <td>{{$buy->status}}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="none">No buy orders yet.</p>
        @endif
    </div>

    <div class="col-md-6">
        <h3>Sells</h3>
        @if(!$sells->isEmpty())
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th style="width: 35px;">Type</th>
                    <th>Amount</th>
                    <th>Price (time of sale)</th>
                    <th>Value (after fees)</th>
                    <th style="width: 70px;">Status</th>
                </tr>

                @foreach ($sells as $sell)
                    <tr>
                        <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sell->created_at, 'Australia/Melbourne')}}</td>
                        <td>{{strtoupper($sell->type)}}</td>
                        <td>{{$sell->amount}}</td>
                        <td>${{$sell->price}}</td>
                        <td>${{bcmul($sell->price,$sell->amount,2)}}
                        <td>{{$sell->status}}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="none">No sell orders yet.</p>
        @endif
    </div>

</div>