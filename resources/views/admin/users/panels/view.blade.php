<div class="row" id="view">
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td colspan="2" style="font-weight: bold; text-align: center;">User Information</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">ID</td>
                <td>{{ $user->id }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Name</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Email</td>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">TFA Status</td>
                <td>{{ $user->tfa_status ? 'Enabled' : 'Disabled' }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">UBC</td>
                <td>{{ $user->ubc }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">ABN</td>
                <td>{{ $user->abn }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Phone</td>
                <td>{{ $user->country_calling_code }} {{ $user->phone }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td colspan="2" style="font-weight: bold; text-align: center;">User Status</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Status</td>
                <td>{{ $user->status }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">D.O.B</td>
                <td>{{ $user->dob }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Verification Attempts</td>
                <td>{{ $user->verify_count }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Verified</td>
                <td>{{ $user->verified ? 'Yes' : 'No' }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Mobile Verified</td>
                <td>{{ ($verifications->sms ?? false) ? 'Yes' : 'No' }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Bank Verified</td>
                <td>{{ ($verifications->bank ?? false) ? 'Yes' : 'No' }}</td>
            </tr>
            <tr>
                <td style="font-weight:bold;">Drivers ID Verified</td>
                <td>{{ ($verifications->id_drivers ?? false) ? 'Yes' : 'No' }}</td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td colspan="2" style="font-weight: bold; text-align: center;">User Limits</td>
            </tr>
            @foreach(\App\Models\UserLimits::whereUserId($user->id)->get() as $limit)
                <tr>
                    <td style="font-weight:bold;">
                        {{ ucwords(str_replace('_', ' ', $limit->key)) }}
                    </td>
                    <td>{{ $limit->value }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>