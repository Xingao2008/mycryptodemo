
<h3>Deposits</h3>
@if(!$deposits->isEmpty())
    <table class="table table-responsive table-bordered table-striped">
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Amount</th>
            <th>To</th>
            <th>TXID</th>
            <th style="width: 70px;">Status</th>
        </tr>

        @foreach ($deposits as $deposit)
            <tr>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $deposit->created_at, 'Australia/Melbourne')}}</td>
                <td>{{strtoupper($deposit->type)}}</td>
                <td>{{$deposit->amount}}</td>
                <td>{{$deposit->to}}</td>
                <td>{{$deposit->transaction_id}}</td>
                <td>{{$deposit->status}}</td>
            </tr>
        @endforeach
    </table>
@else
    <p class="none">No deposits yet.</p>
@endif


<h3>Withdrawals</h3>
@if(!$withdrawals->isEmpty())
    <table class="table table-responsive table-bordered table-striped">
        <tr>
            <th>Date</th>
            <th>Type</th>
            <th>Amount</th>
            <th>To</th>
            <th>TXID</th>
            <th style="width: 70px;">Status</th>
        </tr>

        @foreach ($withdrawals as $withdraw)
            <tr>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $withdraw->created_at, 'Australia/Melbourne')}}</td>
                <td>{{strtoupper($withdraw->type)}}</td>
                <td>{{$withdraw->amount}}</td>
                <td>{{$withdraw->to}}</td>
                <td>{{$withdraw->transaction_id}}</td>
                <td>{{$withdraw->status}}</td>
            </tr>
        @endforeach
    </table>
@else
    <p class="none">No withdrawals yet.</p>
@endif