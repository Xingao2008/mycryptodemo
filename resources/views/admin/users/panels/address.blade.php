<div class="row">
    <div class="col-md-7 col-md-offset-1">
        <h3>Address</h3>
        <form method="post" class="form-horizontal" action="admin/users/{{$user->id}}/address">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="id" class="col-sm-2 control-label">Street</label>
                <div class="col-sm-10">
                    <input name="street" type="text" class="form-control" value="{{ $user->address->street or '' }}">
                </div>
                <label for="id" class="col-sm-2 control-label">City</label>
                <div class="col-sm-10">
                    <input name="city" type="text" class="form-control" value="{{ $user->address->city or '' }}">
                </div>
                <label for="id" class="col-sm-2 control-label">State</label>
                <div class="col-sm-10">
                    <select name="state" class="form-control">
                        @foreach(get_states_array() as $state)
                            <option value="{{$state}}"

                                    @if(isset($user->address->state) && $user->address->state == $state)
                                    selected="selected"
                                    @elseif (empty($user->address->state) && $state == 'VIC')
                                    selected="selected"
                                    @endif

                            >{{$state}}</option>
                        @endforeach
                    </select>
                </div>
                <label for="id" class="col-sm-2 control-label">Post Code</label>
                <div class="col-sm-10">
                    <input name="post_code" type="text" class="form-control" value="{{ $user->address->post_code or '' }}">
                </div>
                <label for="id" class="col-sm-2 control-label">Country</label>
                <div class="col-sm-10">
                    <select name="country_id" class="form-control">
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}"

                                    @if(isset($user->address->country_id) && $user->address->country_id == $country->id)
                                    selected="selected"
                                    @elseif (empty($user->address->country_id) && $country->id == 36)
                                    selected="selected"
                                    @endif

                            >{{$country->name}}</option>

                        @endforeach
                    </select>
                </div>

                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success">Save</button>
                    <a class="btn btn-danger" href="{{ route('admin.users') }}">Cancel</a>
                </div>
            </div>
        </form>

        <h3>History</h3>
        <table class="table">
            <thead>
                <tr>
                    <th>Street</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Post Code</th>
                    <th>Country</th>
                    <th>Version</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($addrs_arr as $addr)
                <tr>
                    <td>{{ $addr['data']->street }}</td>
                    <td>{{ $addr['data']->city }}</td>
                    <td>{{ $addr['data']->state }}</td>
                    <td>{{ $addr['data']->post_code }}</td>
                    <td>{{ $addr['data']->country_id }}</td>
                    <td>{{ $addr['version'] }}</td>
                    <td>{{ $addr['status'] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>