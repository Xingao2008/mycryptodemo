<table class="table datatable">
    <thead>
    <tr>
        <th>Admin</th>
        <th>Note</th>
        <th>Time</th>
    </tr>
    </thead>
    <tbody>
    @if(!$user->notes->isEmpty())
        @foreach($user->notes()->orderBy('created_at', 'DESC')->get() as $note)
            <tr>
                <td>{{ $note->admin->name }}</td>
                <td>{!! $note->note !!}</td>
                <td>{{ $note->created_at }}</td>
            </tr>
        @endforeach
    @else
        <tr><td colspan="3">None available.</td></tr>
    @endif
    </tbody>
</table>
<hr/>
<div class="row">
    <div class="col-md-12">
        <h4>Add note(s)</h4>
        {!! BootForm::open(['url' => route('admin.users.notes', $user->id)]) !!}

        {!! BootForm::textarea('note') !!}

        {!! BootForm::submit() !!}

        {!! BootForm::close() !!}
    </div>
</div>