@extends('admin.layouts.app')

@section('contentheader_title', 'Users')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Users</h3>
    </div>
    <div class="box-body">
        <table id="users-table" class="table datatable display">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Role(s)</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        let base_url = '{{ url()->to("admin/") }}';
        $(document).ready(function() {
            var table = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.users.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'role', name: 'role', searchable: false, sortable: false },
                    { data: 'email', name: 'email' },
                    { data: 'ubc', name: 'ubc' },
                    { data: 'status', name: 'status' },
                    {
                        "mRender": function ( data, type, row ) {
                            html = '<a href="' + base_url + '/users/' + row.id + '/edit" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>';

                            @canImpersonate
                                html += '&nbsp; <a href="' + base_url + '/users/loginas/' + row.id +'" class="btn btn-sm btn-warning">Impersonate</a>';
                            @endCanImpersonate

                            @can('admin.resettfa')
                                html += '&nbsp; <a href="' + base_url + '/tfa_reset/user/' + row.id + '" class="btn btn-sm btn-danger reset-tfa">Reset 2FA</a>';
                            @endcan

                            html += '&nbsp <a href="' + base_url + '/tickets/create/' + row.id + '" class="btn btn-sm btn-info">Message User</a>';

                            return html;
                        }
                    }
                ]
            });

            addExtraButtons();
            $('#users-table').on("draw.dt", function(e) {
                addExtraButtons();
            })

            function addExtraButtons() {

                $("#users-table_previous").after(
                    "<li class='paginate_button'>" +
                    "<a class='ten_previous' style='cursor: pointer'>10<</a>" +
                    "</li>"
                )
                $("#users-table_next").before(
                    "<li class='paginate_button'>" +
                    "<a class='ten_next' style='cursor: pointer'>>10</a>" +
                    "</li>"
                )

                $("#users-table_previous").after(
                    "<li class='paginate_button'>" +
                    "<a class='hundred_previous' style='cursor: pointer'>100<</a>" +
                    "</li>"
                )
                $("#users-table_next").before(
                    "<li class='paginate_button'>" +
                    "<a class='hundred_next' style='cursor: pointer'>>100</a>" +
                    "</li>"
                )

                $("#users-table_previous").after(
                    "<li class='paginate_button'>" +
                    "<a class='thousand_previous' style='cursor: pointer'>1000<</a>" +
                    "</li>"
                )
                $("#users-table_next").before(
                    "<li class='paginate_button'>" +
                    "<a class='thousand_next' style='cursor: pointer'>>1000</a>" +
                    "</li>"
                )

                var currentPage = table.page.info();

                // disable btn if first or last
                // if (currentPage.pages - 1 == currentPage.page) {
                //     $(".quick_next").parent().addClass("disabled")
                // } else if (currentPage.page == 0) {
                //     $(".quick_previous").parent().addClass("disabled")
                // }



                $(".ten_next").on("click", tenNext)
                $(".ten_previous").on("click", tenPrevious)
                $(".hundred_next").on("click", hundredNext)
                $(".hundred_previous").on("click", hundredPrevious)
                $(".thousand_next").on("click", thousandNext)
                $(".thousand_previous").on("click", thousandPrevious)

                function tenNext(e) {
                    var pageToGoTo = (currentPage.page + 10) >= currentPage.pages ? currentPage.pages - 1 : (currentPage.page + 10);
                    table.page(pageToGoTo).draw(false);
                }

                function tenPrevious(e) {
                    var pageToGoTo = (currentPage.page - 10) <= 0 ? 0 : (currentPage.page - 10);
                    table.page(pageToGoTo).draw(false);
                }

                function hundredNext(e) {
                    var pageToGoTo = (currentPage.page + 100) >= currentPage.pages ? currentPage.pages - 1 : (currentPage.page + 100);
                    table.page(pageToGoTo).draw(false);
                }

                function hundredPrevious(e) {
                    var pageToGoTo = (currentPage.page - 100) <= 0 ? 0 : (currentPage.page - 100);
                    table.page(pageToGoTo).draw(false);
                }

                function thousandNext(e) {
                    var pageToGoTo = (currentPage.page + 1000) >= currentPage.pages ? currentPage.pages - 1 : (currentPage.page + 1000);
                    table.page(pageToGoTo).draw(false);
                }

                function thousandPrevious(e) {
                    var pageToGoTo = (currentPage.page - 1000) <= 0 ? 0 : (currentPage.page - 1000);
                    table.page(pageToGoTo).draw(false);
                }
            }
        });

        $(document).on('click', '.reset-tfa', function(e) {
            e.preventDefault();
            let btn = $(this);
            bootbox.confirm("Are you sure?", function(result) {
                if (result) {
                     window.location = btn.attr('href');
                }
            });
        });
    </script>
@endpush