@extends('admin.layouts.app')

@section('contentheader_title', 'POLi Pay')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Users</h3>
            <h5>
                The following users have their withdrawal ability blocked until you manually approve / verify their PoliPay deposits
            </h5>
            <button class="btn btn-large btn-danger" onclick="toggleFraud();" style="float: right;" id="fraud">Fraud Records</button>
        </div>
        <div class="box-body" id="poliFraudRecords">
                <table id="users-table" class="table datatable display">
                    <thead>
                        <tr>
                            <th style="width: 5%;">uID</th>
                            <th style="width: 10%;">User Name</th>
                            <th style="width: 10%;">Email</th>
                            <th style="width: 10%;">DOB</th>
                            <th style="width: 10%;">Phone</th>
                            <th style="width: 10%;">UBC</th>
                            <th style="width: 30%;">Address</th>
                            <th style="width: 5%;">Poli Reference</th>
                            <th style="width: 10%;">Date</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($frauds as $fraud)
                        <tr>
                            <td>{{ $fraud->id }}</td>
                            <td>{{ $fraud->username }}</td>
                            <td>{{ $fraud->email }}</td>
                            <td>{{ $fraud->dob }}</td>
                            <td>{{ $fraud->phone }}</td>
                            <td>{{ $fraud->ubc }}</td>
                            <td>{{ $fraud->street }} {{ $fraud->city }} {{ $fraud->state }} {{ $fraud->post_code }} {{ $fraud->country_id }}</td>
                            <td>{{ $fraud->poli_transaction_id }}</td>
                            <td>{{ $fraud->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
        </div>

        <div class="box-body" id="poliApprove">
            @foreach($reps as $rep)
                <table id="users-table" class="table datatable display">
                    <thead>
                        <tr>
                            <th style="width: 20%;">ID</th>
                            <th style="width: 20%;">User Name</th>
                            <th style="width: 20%;">Email</th>
                            <th style="width: 20%;">UBC</th>
                            <th style="width: 20%;">Expand / Collapse</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 20%;">{{ $rep[0]->id }}</td>
                            <td style="width: 20%;">{{ $rep[0]->first_name }} {{ $rep[0]->last_name }}</td>
                            <td style="width: 20%;">{{ $rep[0]->email }}</td>
                            <td style="width: 20%;">{{ $rep[0]->ubc }}</td>
                            <td style="width: 20%;" colspan="3"><button class="btn btn-info btn-sm user_area" id="{{ $rep[0]->id }}">Expand</button></td>
                        </tr>

                        <table id="table-{{ $rep[0]->id }}" class="child-table table datatable display" style="border: 1px solid #000;">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">User IP</th>
                                    <th style="width: 10%;">Status</th>
                                    <th style="width: 10%;">Amount</th>
                                    <th style="width: 20%;">Transaction time</th>
                                    <th style="width: 20%;">Transaction id</th>
                                    <th style="width: 20%;">Transaction Ref No</th>
                                    <th style="width: 10%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($rep as $item)
                                <tr>
                                    <td style="width: 10%;">{{ $item->user_ip }}</td>
                                    <td style="width: 10%;">{{ $item->status }}</td>
                                    <td style="width: 10%;">{{ $item->amount }}</td>
                                    <td style="width: 20%;">{{ date('Y/m/d H:m:s', $item->transaction_time) }}</td>
                                    <td style="width: 20%;">{{ $item->transaction_id }}</td>
                                    <td style="width: 20%;">{{ $item->transaction_ref_no }}</td>
                                    <td style="width: 10%;">
                                        <a href="/admin/polipay-deposit/{{$item->id}}/remove" class="btn btn-danger btn-sm">Fraud</a>
                                        <a href="/admin/polipay-deposit/{{$item->id}}/approve" class="btn btn-success btn-sm">Approve</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#poliFraudRecords').hide();

            $(".user_area").click(function(e){
                e.preventDefault();
                var tid = $(this).attr('id');
                $("#table-"+tid).toggle();
            });
        });

        function toggleFraud() {
            $('#poliFraudRecords').toggle();
            $('#poliApprove').toggle();
            if ($('#fraud').text() === 'Fraud Records') {
                $('#fraud').removeClass('btn-danger');
                $('#fraud').addClass('btn-primary');
                $('#fraud').text('Back');
            } else {
                $('#fraud').removeClass('btn-primary');
                $('#fraud').addClass('btn-danger');
                $('#fraud').text('Fraud Records');
            }
        }
    </script>
@endpush