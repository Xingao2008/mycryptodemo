@extends('admin.layouts.app')

@section('contentheader_title', 'POLi Pay')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Users</h3>
        </div>
        <div class="box-body">
            <table id="users-table" class="table datatable display">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>User</th>
                    <th>User IP</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Transaction ID</th>
                    <th>Transaction Ref No.</th>
                    <th>When</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var base_url = '{{ url()->to("admin/polipay/") }}';
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.polipay.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'users.name' },
                    { data: 'user_ip', name: 'user_ip' },
                    { data: 'amount', name: 'amount' },
                    { data: 'status', name: 'status' },
                    { data: 'transaction_id', name: 'transaction_id' },
                    { data: 'transaction_ref_no', name: 'transaction_ref_no' },
                    { data: 'created_at', name: 'created_at' },
                    {
                        "mRender": function ( data, type, row ) {
                            html = '<a href="' + base_url + '/' + row.id + '/edit" class="btn btn-sm btn-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
                            return html;
                        }, orderable: false, searchable: false
                    }
                ]
            });
        });
    </script>
@endpush