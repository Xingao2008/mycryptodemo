@extends('admin.layouts.app')

@section('contentheader_title', 'Edit POLi')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Edit Transaction</h3>
        </div>
        <div class="box-body">
            <p>WARNING: Make sure you know what you're doing when changing these settings. Changing user could result in giving funds out.</p>
            {!! BootForm::horizontal(['model' => $transaction]) !!}


            <div class="form-group ">
                <label for="status" class="control-label col-sm-2 col-md-3">User</label>
                <div class="col-sm-10 col-md-9">
                    <select class="select2 form-control">
                        @if($transaction->user_id)
                            <option value="{{ $transaction->user_id }}" selected="selected">{{ $transaction->user->name }}</option>
                        @endif
                    </select>
                </div>
            </div>

            {!! BootForm::staticField('user_ip') !!}

            {!! BootForm::select('status', null, $statuses) !!}

            {!! BootForm::text('amount') !!}

            {!! BootForm::number('transaction_id') !!}

            {!! BootForm::number('transaction_ref_no') !!}

            {!! BootForm::text('transaction_token') !!}

            {!! BootForm::submit() !!}

            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('.select2').select2({
            ajax: {
                url: '{{ url()->route('admin.users.select2') }}',
                dataType: 'json'
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endpush