@extends('admin.layouts.app')

@section('contentheader_title', 'Emails')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Email Logs</h2>
            <a href="admin/emails/import-email" class="btn btn-success btn-sm">Import emails</a>
            <a href="admin/emails/email-list" class="btn btn-success btn-sm">Emails list</a>
        </div>
        <div class="box-body">
            <table id="emails-table" class="table table-responsive">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User ID</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Subject</th>
                        <th>Data</th>
                        <th>Date</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection



@push('scripts')
    <script>
        $(function() {
            $('#emails-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.emails.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'name', name: 'users.name' },
                    { data: 'to', name: 'to' },
                    { data: 'subject', name: 'subject' },
                    { data: 'data', name: 'data' },
                    { data: 'created_at', name: 'created_at' },
                ]
            });
        });
    </script>
@endpush