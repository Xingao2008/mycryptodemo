@extends('admin.layouts.app')

@section('contentheader_title', 'Emails')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Send bulk emails</h2>
        </div>
        <div class="box-body">
            <form action="/admin/emails/send-bulk-email" method="post">
                {{ csrf_field() }}
                Sender: <textarea name="email_sender"></textarea>
                Subject: <input name="email_subject" type="text" class="form-control" placeholder="Enter Subject" />
                body:
                <textarea name="email_body"></textarea>
                <button type="submit" class="btn btn-primary btn-lg">Send</button>
            </form>
        </div>
    </div>
@endsection
