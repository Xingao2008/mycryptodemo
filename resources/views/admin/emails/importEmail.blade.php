@extends('admin.layouts.app')

@section('contentheader_title', 'Emails')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Import emails</h2>
        </div>
        <div class="box-body" id="app">
            <textarea name="emails" style="width: 100%; height: 300px" v-model="emails"></textarea>
            <input type="text" name="list_name" style="width: 100%;" v-model="list_name"/>
            <a href="#" class="btn btn-primary btn-lg" @click.prevent="importEmail()">Import <i v-show="loading" class="fa fa-spinner fa-spin"></i></a>
            <span>@{{ message }}</span>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                myToken: '',
                loading: false,
                emails: [],
                list_name: '',
                message: ''
            },
            methods: {
                importEmail() {
                    this.loading = true
                    axios.post(
                        'api/importEmail',
                        {
                            emails: this.emails,
                            list_name: this.list_name
                        }
                    )
                    .then((rsp) => {
                        this.loading = false
                        this.message = rsp.data.message + ', Import ' + rsp.data.data['countOfEmails'] + ' emails, There are ' + rsp.data.data['countOfValidateEmails'] + ' valid emails.'
                        this.emails = ''
                        this.list_name = ''
                    })
                    .catch((err) => {
                        if (err.response.status === 401) {
                            this.message = err.message
                        }

                        if (err.response.status === 500) {
                            this.message = err.message
                        }

                        this.loading = false
                    })
                }
            },
            mounted () {
                let token = this.$cookie.get('api_token')
                this.myToken = token
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
            }
        })
    </script>
@endpush
