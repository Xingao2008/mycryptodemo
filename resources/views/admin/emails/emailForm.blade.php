@extends('admin.layouts.app')

@section('contentheader_title', 'Emails')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Email Form</h2>
        </div>
        <div class="box-body" id="app">
            <div class="container">
                <div class="raw">
                    <div>Subject</div>
                    <div><input type="text" name="subject" v-model="subject" style="width: 100%;"/></div>
                    <div>Content</div>
                    <div><textarea style="width: 100%; height: 300px;" name="content" v-model="content"></textarea></div>
                    <div><a href="#" @click.prevent="send()" class="btn btn-primary btn-md">Send <i v-show="loading" class="fa fa-spinner fa-spin"></i></a></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                subject: '',
                content: '',
                senders: [],
                loading: false
            },
            methods: {
                send() {
                    var title = "{{ $title }}"
                    this.loading = true
                    axios.post(
                       'api/sendBulkEmail',
                       {
                           subject: this.subject,
                           body: this.content,
                           title: title
                       }
                    )
                       .then((rsp) => {
                           let data = rsp.data.data
                           this.loading = false
                           console.log(data)
                   })
                }
            }
        })
    </script>
@endpush