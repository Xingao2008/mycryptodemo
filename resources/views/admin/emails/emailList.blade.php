@extends('admin.layouts.app')

@section('contentheader_title', 'Emails')

@section('content')
    <div class="box box-primary">
        <div class="box-header">
            <h2 class="box-title">Email list</h2>
        </div>
        <div class="box-body" id="app">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Option</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="list in lists">
                    <th scope="row"></th>
                    <td>@{{ list.title }}</td>
                    <td>@{{ list.amount }}</td>
                    <td><a :href="'/admin/emails/email-form/'+list.title" class="btn btn-primary btn-sm">Pick me</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.0/js.cookie.min.js"></script>
    <script src="https://unpkg.com/vue-js-cookie@2.1.0/vue-js-cookie.js"></script>
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                myToken: '',
                loading: [],
                lists: []
            },
            methods: {
                loadEmailList() {
                    axios.get(
                        'api/getEmailList'
                    )
                        .then((rsp) => {
                        let data = rsp.data.data
                        for (var i in data) {
                            this.lists.push({
                                title: data[i].list_name,
                                amount: data[i].user_count,
                                loading: false
                            })
                        }
                    })
                }
            },
            mounted () {
                let token = this.$cookie.get('api_token')
                this.myToken = token
                axios.defaults.headers.common['Authorization'] = `Bearer ${token}`

                this.loadEmailList()
            }
        })
    </script>
@endpush
