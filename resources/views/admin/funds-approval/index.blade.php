@extends('admin.layouts.app')

@section('contentheader_title', 'Deposits Approval')

@section('content')
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Deposits History</h3>
    </div>
    <div class="box-body">
        <table class="table" id="deposits-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Amount</th>
                <th>Source</th>
                <th>Status</th>
                <th>When</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <hr/>
        <a href="#" class="btn btn-block btn-primary" id="loadMore">See More</a>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Users</h3>
    </div>
    <div class="box-body">
        <table id="funds-table" class="table display datatable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>UBC</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection


@push('scripts')
    <script>
        let base_url = '{{ url()->to("admin/funds-approval") }}';
        $(function() {
            $('#funds-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: '{!! route('admin.users.datatables') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'ubc', name: 'ubc' },
                    {
                        "mRender": function ( data, type, row ) {
                            return '<a href="' + base_url + '/' + row.id + '/add" class="btn btn-sm btn-success">Add Deposit</a>';
                        }
                    }
                ]
            });

            let table = $('#deposits-table').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                pageLength: 4,
                dom: "frt",
                searching: false,
                ajax: '{!! route('admin.fundsApproval.dataTable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'user.name', name: 'user.name' },
                    { data: 'amount', name: 'amount' },
                    { data: 'source', name: 'source' },
                    { data: 'status', name: 'status' },
                    { data: 'created_at', name: 'created_at' }
                ]
            });

           // table.page.len(4).draw();


            $(document).on('click', '#loadMore', function(){
                length = table.page.len();
                table.page.len(length * 2).draw();
                return false;
            })
        });
    </script>
@endpush