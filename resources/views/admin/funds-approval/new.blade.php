@extends('admin.layouts.app')

@section('contentheader_title', 'Deposits Approval for ' . $user->name)

@section('content')
<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Deposits Approval for <strong>{{ $user->name }}</strong></h3>
        </div>
        {!! BootForm::horizontal() !!}
            <div class="box-body" id="box-body">
                    {{ csrf_field() }}

                    {!! BootForm::date('transaction_date') !!}

                    {!! BootForm::text('amount', null, null, ['required']) !!}
                    <label for="currency" class="control-label col-sm-2 col-md-3">Currency</label>
                    <div class="col-sm-10 col-md-9">
                        <select class="form-control" name="currency" id="currency" class="form-control">
                            <option value="">Choose currency</option>
                            @foreach(config('currencies.fiatCurrencies') as $c)
                                @if ($c === 'aud')
                                    <option value="{{ $c }}" selected>{{ $c }}</option>
                                @else
                                    <option value="{{ $c }}">{{ $c }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-danger" href="{{ route('admin.fundsApproval') }}">Cancel</a>
            </div>
        </form>
    </div>
</div>
<div class="col-md-4">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">FIAT Deposits</h3>
        </div>
        <div class="box-body">
            <p>Note: dates displayed are the date a myCryptoWallet administrator used the Funds Deposit form on the left.</p>
            @if($fiat_deposits->isNotEmpty())
                <table class="table">
                    <thead>
                        <tr>
                            <th>Amount</th>
                            <th>Source</th>
                            <th>When</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($fiat_deposits as $fiat_deposit)
                            <tr>
                                <td>${{ audFormat($fiat_deposit->amount) }}</td>
                                <td>{{ $fiat_deposit->source }}</td>
                                <td>{{ $fiat_deposit->created_at }} ({{ $fiat_deposit->created_at->diffForHumans() }})</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                The user doesn't have any deposits yet.
            @endif
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('form').submit(function(e) {
            var currentForm = this;
            e.preventDefault();
            let amount = $('#amount').val();
            bootbox.confirm("Are you sure you want to deposit $" + amount + ' into {{$user->name}}\'s account?', function(result) {
                if (result) {
                    currentForm.submit();
                    $('.box-footer').html('');
                    $('#box-body').html('<center><h2>Depositing $' + amount + ' into {{$user->name}}\'s account.</h2><i class="fa fa-spinner fa-spin" style="font-size: 50px"></i></center>');
                }
            });
        });
    </script>
@endpush