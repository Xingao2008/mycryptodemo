<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <title>2FA Authentication</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en" />
    <style>
        body {
            background: #37517e;
            background: -moz-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: -webkit-linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            background: linear-gradient(45deg, #37517e 40%, #00b5ff 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#37517e', endColorstr='#00b5ff',GradientType=1 );
            height: 100vh;
            width: 100%;
        }

        .panel, .panel-default>.panel-heading {
            background-color: #00000061;
            color: white;
            border-color: #00000061;
        }
    </style>
</head>
<body>

<div class="container" style="margin-top:50px">
    <div class="col-md-8 col-md-offset-2">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title">2FA Authentication</h1>
            </div>
            <div class="panel-body" style="padding: 5px">
                {!! BootForm::open() !!}

                {!! BootForm::text('tfa') !!}

                {!! BootForm::submit() !!}

                {!! BootForm::close() !!}
            </div>
        </div>
        <img src="{{ asset('img/logo.svg') }}">
    </div>
</div>

</body>
</html>
