@extends('admin.layouts.app')

@section('contentheader_title', 'Failed Transactions')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Failed Transactions</h3>
    </div>
    <div class="box-body">
        <p>Below are both failed and processing transactions. Failed transactions are usually safe to revert, however processing transactions may not be. Always check the blockchain (including the user's wallet and the hot/holding wallet) to make sure no funds left or arrived in the account to prevent doubling up.</p>
        <p>This feature will only: <br>
            1. return the crypto to the user<br>
            2. remove any trace of a failed transaction in the database<br>
            (in other words, it won't check if a "processing" or "failed" transaction already sent funds), so please use with caution!</p>


        <table id="js-failedwithdraw-table" class="table display failedwithdraw-table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Type</th>
                <th>User ID</th>
                <th>Amount</th>
                <th>To</th>
                <th>TXID</th>
                <th style="width: 80px;">Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($failed_withdrawals as $withdraw)
                <tr>
                    <td>{{$withdraw->id}}</td>
                    <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $withdraw->created_at, 'Australia/Melbourne')}}</td>
                    <td>{{strtoupper($withdraw->type)}}</td>
                    <td>{{$withdraw->user_id}}</td>
                    <td>{{$withdraw->amount}}</td>
                    <td>{{$withdraw->to}}</td>
                    <td>{{$withdraw->transaction_id}}</td>
                    <td>{{$withdraw->status}}</td>
                    <td><a class="btn btn-danger" href="/admin/failed-transactions/withdraw-removal/{{$withdraw->id}}" onclick="return confirm('Are you sure you want to delete all traces of this withdrawal attempt and refund the user {{$withdraw->amount}} {{$withdraw->type}}?')">Delete all records & Refund User</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
