// Import System requirements
import Vue from 'vue'
import VueFlashMessage from 'vue-flash-message'
import VueRouter from 'vue-router'
import routes from './routes'
import AppView from './components/App'

import loadStock from 'highcharts/modules/stock'
import VueHighcharts from 'vue-highcharts'
import Highcharts from 'highcharts'
import axios from 'axios'
import moment from 'moment'



//Charts
loadStock(Highcharts);
Vue.use(VueHighcharts, { Highcharts });


// Global variables
window.axios = axios;
window.moment = moment;
//window.moment.locale('en-au');



// Currency
import VueCurrencyFilter from 'vue-currency-filter'
Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: false
})

//Use
Vue.use(VueRouter);
Vue.use(require('vue-moment'));

//Use cookie
var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

//Sockets
// import VueSocketio from 'vue-socket.io';
// Vue.use(VueSocketio, 'http://localhost:6001');
import Echo from "laravel-echo"

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.origin + ':8443'
});



//Use flash
Vue.use(VueFlashMessage);

// Routing logic
let router = new VueRouter({
    routes: routes,
    linkExactActiveClass: 'active'
});

// register modal component
Vue.component('my-component', {
    template: `
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-container">

                            <div class="modal-header">
                                <h3><slot name="header">Are you sure to cancel the order ?</slot></h3>
                            </div>

                            <div class="modal-footer">
                                <slot name="footer">
                                    <button class="modal-default-button" @click.prevent="$parent.cancel()">Yes</button>
                                    <button class="modal-default-button green" @click.prevent="$emit('close')">No</button>
                                </slot>
                            </div>

                        </div>
                    </div>
                </div>
            `
})


// Start Vue
new Vue({
    el: '#root',
    router: router,
    render: h => h(AppView)
});

// Vue.config.devtools = true;
// Vue.config.debug = true;
