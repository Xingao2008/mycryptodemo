import DashboardView from './components/views/Dashboard'
// Routes
const routes = [
    {
        path: '/:currency1/:currency2',
        name: 'Dashboard',
        component: DashboardView
    }
];

export default routes