export const currencies = new Vue({
    data: {
        currency: ['AUD', 'BTC'],
        currencyPair: ['AUD', 'BTC', 'LTC', 'ETH', 'XRP', 'PWR']
    },
    methods: {
        getCryptoPair: function() {
            var returnPairs = [];
            this.currency.forEach(function(thisV) {
                this.currencyPair.forEach(function(thisPV) {
                    if (thisV === thisPV) {
                        return;
                    }
                    returnPairs.push(thisV + '/' + thisPV);
                });
            }, this);

            var i = returnPairs.indexOf("BTC/AUD");
            returnPairs.splice(i, 1);
            return returnPairs;
        }
    }
})