//require('bootstrap-sass');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    $('.coin-toggle .single-toggle').on('click', function() {
        $('.coin-toggle .single-toggle').removeClass('active');
        $(this).addClass('active');
    });



    $('#fund-approval-form').submit(function(event) {

        event.preventDefault();
        var amount = parseFloat($("input[name=amount]").val()).toFixed(2);

        if (isNaN(amount))
            amount = "0.00";

        swal({
                title: "Are you sure you want to credit " + $('.page-heading span').text() + " the amount of " + amount,
                // text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal("Submit", "Item has been successfully processed.", "success");
                    $("#fund-approval-form").off("submit").submit();
                } else {
                    swal("Cancelled", "Cancel Operation.", "error");
                    $("#fund-approval-form").reset();
                }
            });

        return false;

    });

    $('#withdraw-approval-form').submit(function(event) {

        event.preventDefault();
        var amount = parseFloat($("input[name=amount]").val()).toFixed(2);

        if (isNaN(amount))
            amount = "0.00";

        swal({
                title: "Are you sure you want to credit " + $('.page-heading span').text() + " the amount of " + amount,
                // text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    swal("Submit", "Item has been successfully processed.", "success");
                    $("#withdraw-approval-form").off("submit").submit();
                } else {
                    swal("Cancelled", "Cancel Operation.", "error");
                    $("#withdraw-approval-form").reset();
                }
            });

        return false;

    });
});


$(document).ready(function() {
    var ctx = document.getElementById("myChart");
});


$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

$(document).ready(function() {
    $('.hamburger').on('click', function() {
        console.log('yo');
        $('.mobile-menu').slideToggle();
        $(this).toggleClass('active');
        $('body').toggleClass('nil-overflow');
    });
});


// GRAPH TOGGLE //
$(document).ready(function() {
    $('.market-prices .single-toggle').click(function() {
        var boxes = $('.single-graph');
        var filter = $(this).data("toggle");
        boxes.css('opacity', '0');
        boxes.removeClass('active');
        filter = '.' + filter;
        boxes.filter(filter).css('opacity', '1');
        boxes.filter(filter).addClass('active');
    });

    $('.single-faq .title').on('click', function() {
        $(this).next().slideToggle();
        $(this).toggleClass('active');
    })

    $('.toggle').on('click', function() {
        $(this).parent().parent().parent().find('.button-dropdown').slideToggle();
        $(this).toggleClass('active');
    });

    $('#switch_aud').on('click', function() {
        $('.aud_deposit').show();
        $('.nzd_deposit').hide();
        $('.usd_deposit').hide();
        $('#aud_balance').show();
        $('#nzd_balance').hide();
        $('#usd_balance').hide();
        $('#selected_fiat').val('aud');
    });
    $('#switch_nzd').on('click', function() {
        $('.aud_deposit').hide();
        $('.nzd_deposit').show();
        $('.usd_deposit').hide();
        $('#aud_balance').hide();
        $('#nzd_balance').show();
        $('#usd_balance').hide();
        $('#selected_fiat').val('nzd');
    });
    $('#switch_usd').on('click', function() {
        $('.aud_deposit').hide();
        $('.nzd_deposit').hide();
        $('.usd_deposit').show();
        $('#aud_balance').hide();
        $('#nzd_balance').hide();
        $('#usd_balance').show();
        $('#selected_fiat').val('usd');
    });
});
