import Vue from 'vue'
import piechart from './components/piechart'
import radialbar from './components/radialbar'
import stockchart from './components/stockchart'

new Vue({
    el: '#chart',
    components: {
        piechart,
        radialbar,
        stockchart
    }
})