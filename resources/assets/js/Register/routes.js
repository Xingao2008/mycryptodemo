import RegisterSignUp from './components/views/RegisterSignUp'
import RegisterMobile from './components/views/RegisterMobile'
import RegisterPersonalDetails from './components/views/RegisterPersonalDetails'
// Routes
const routes = [
    {
        path: '/register',
        name: 'RegisterSignUp',
        component: RegisterSignUp
    },
    {
        path: '/register/mobile',
        name: 'RegisterMobile',
        component: RegisterMobile
    },
    {
        path: '/register/details',
        name: 'RegisterPersonalDetails',
        component: RegisterPersonalDetails
    }
];

export default routes