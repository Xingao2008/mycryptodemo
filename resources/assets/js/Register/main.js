// Import System requirements
import Vue from 'vue'
import VueFlashMessage from 'vue-flash-message'
import VueRouter from 'vue-router'
import routes from './routes'
import AppView from './components/App'

import axios from 'axios'
import moment from 'moment'


// Global variables
window.axios = axios;
window.moment = moment;
//window.moment.locale('en-au');

//Use
Vue.use(VueRouter);
Vue.use(require('vue-moment'));

//Use flash
Vue.use(VueFlashMessage);

// Routing logic
let router = new VueRouter({
    routes: routes,
    linkExactActiveClass: 'active'
});

// Start Vue
new Vue({
    el: '#root',
    router: router,
    render: h => h(AppView)
});

